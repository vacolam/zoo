// firebase_subscribe.js
firebase.initializeApp({
    messagingSenderId: '480584252642'
});


// браузер поддерживает уведомления
// вообще, эту проверку должна делать библиотека Firebase, но она этого не делает
if ('Notification' in window) {
    var messaging = firebase.messaging();

    // пользователь уже разрешил получение уведомлений
    // подписываем на уведомления если ещё не подписали
    if (Notification.permission === 'granted') {
        subscribe();
    }

    // по клику, запрашиваем у пользователя разрешение на уведомления
    // и подписываем его
    $(function(){
        $('#subscribe').on('click', function () {
            subscribe();
        });
    })
}


function subscribe() {

    // запрашиваем разрешение на получение уведомлений
    messaging.requestPermission()
        .then(function () {
            // получаем ID устройства
            messaging.getToken()
                .then(function (currentToken) {
                    console.log(currentToken);

                    if (currentToken) {
                        sendTokenToServer(currentToken);
                    } else {
                        console.warn('Не удалось получить токен.');
                        $('#subscribe').addClass('red');
                        setTokenSentToServer(false);
                    }
                })
                .catch(function (err) {
                    console.warn('При получении токена произошла ошибка.', err);
                    $('#subscribe').addClass('red');
                    setTokenSentToServer(false);
                });
    })
    .catch(function (err) {
        $('#subscribe').addClass('red');
        console.warn('Не удалось получить разрешение на показ уведомлений.', err);

    });
}

// отправка ID на сервер
function sendTokenToServer(currentToken) {
    if (!isTokenSentToServer(currentToken)) {
        console.log('Отправка токена на сервер...');

        var url = 'https://sakhalinzoo.ru/zoo.php?r=site/webpushsubscribe'; // адрес скрипта на сервере который сохраняет ID устройства
        $.post(url, {
            token: currentToken
        });

        TokenChangeText();

        setTokenSentToServer(currentToken);
    } else {
        console.log('Токен уже отправлен на сервер.');
    }
}

// используем localStorage для отметки того,
// что пользователь уже подписался на уведомления
function isTokenSentToServer(currentToken) {
    TokenChangeText();
    return window.localStorage.getItem('sentFirebaseMessagingToken') == currentToken;
}

function setTokenSentToServer(currentToken) {
    window.localStorage.setItem(
        'sentFirebaseMessagingToken',
        currentToken ? currentToken : ''
    );
}

function TokenChangeText() {
    //$('#subscribe').text('Уведомления включены');
    $('.sbscrb').css('display','none');
    $('.subscribed').css('display','inline');
}