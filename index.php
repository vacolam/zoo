<?php
header('Content-Type:text/html; charset=UTF-8');
//error_reporting(E_ALL);
error_reporting(-1);

$yii    = __DIR__.'/yii/framework/yii.php';
$config = require __DIR__.'/protected/config/frontend.php';

defined('YII_DEBUG') or define('YII_DEBUG',true);

require_once($yii);

Yii::createWebApplication($config)->runEnd('frontend');

