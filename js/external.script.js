var is_safari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
    navigator.userAgent && !navigator.userAgent.match('CriOS') ||
    /iPad|iPhone|iPod/.test(navigator.userAgent) &&
    !window.MSStream;


var globheight = 480;


// запрос на наличие авторизационных кук
if (is_safari) {
    /*
	var GetNeedFixXHR = new XMLHttpRequest();
	GetNeedFixXHR.withCredentials = true;

	GetNeedFixXHR.open('GET', get_url() + '/CookiesFix/NeedFix', false);
	GetNeedFixXHR.send(false);

	if (GetNeedFixXHR.status == 200) {

		var response = JSON.parse(GetNeedFixXHR.responseText);
		console.log(response);

		if (response && response.Result == 1) {

			console.info('Web Platform: Нет авторизационных cookies, необходимо форсированно их вручную');
			document.location = get_url() + '/CookiesFix/Set/?url=' + document.location.href;
		}
		else if (response && response.Result == 0) {

			console.info('Web Platform: Есть авторизационные cookies, всё в порядке');
		}
	}
	else {
		console.error('Web Platform: Запрос CookiesFix/NeedFix не сработал');
	}
    */
}

function GetGuid() {
    var RegGuid = /^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$/;
    var guid = localStorage.getItem('wp-window-guid');

    if (guid !== '' && RegGuid.test(guid)) {
        return guid;
    }
    else {
        return false;
    }
}
function SetGuid() {
    var guid = GenerateGuid();
    localStorage.setItem('wp-window-guid', guid);
    return guid;
}
function GenerateGuid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}
function insertBefore(el, referenceNode) {
    referenceNode.parentNode.insertBefore(el, referenceNode);
}

var WPStatus = false;
var WPId = false;
var WPType = false;
var WPParams = '';
var WPIsInitialized = false;

var WPModalMode = false;
var WPMode = false;
var WPObject = false;
var WPFrameObj = false;
var WPIsPreorder = false;
var WPSessionId = '';

var WPOrder = false;

var WidgetParams = '';
if (!GetGuid()) {
    var guid = SetGuid();
}

var GetSession = new XMLHttpRequest();
//GetSession.withCredentials = true;

GetSession.open('GET', get_url() + '/Widget/GetSession?guid=' + GetGuid() + '&rand' + Math.random(), false);
GetSession.send(false);

if (GetSession.status == 200) {

    var response = JSON.parse(GetSession.responseText);
    //console.log(response);
    if (response !== null && 'Data' in response && typeof response !== 'undefined' && response.Data !== null && response.Data !== '') {
        WPSessionId = response.Data;  
    }
    else {
        location.href = get_url() + '/Widget/SetSession?guid=' + GetGuid() + '&returnUrl=' + location.href;
    }
}

document.addEventListener('DOMContentLoaded', function () { // Аналог $(document).ready(function(){
    var GetStateXHR = new XMLHttpRequest();
    GetStateXHR.open('GET', get_url() + '/widget/GetState?key=' + WPSessionId);
    GetStateXHR.onload = function () {
        var response = JSON.parse(GetStateXHR.responseText);

        if (response.Result === 0 && response.Data === 'open') {
            var params = JSON.parse(localStorage.getItem('wp-init-params'));
            var openParams = JSON.parse(localStorage.getItem('wp-open-modal-params'));
            if (!WPIsInitialized)
                WPInit(params);
            WPOpenModalReal(openParams);
        }
    }
    GetStateXHR.send();
});

function WPInit(params) {
    WPIsInitialized = true;
    params['GUID'] = GetGuid();
    params['SessionId'] = WPSessionId;
    WidgetParams = params;
    localStorage.setItem('wp-init-params', JSON.stringify(params));
    //console.log(get_url() + '/Widget/GetSession?guid=' + GetGuid());


    // повесим эвент, который будет передавать статус
    var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
    var eventer = window[eventMethod];
    var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

    if (WPFrameObj) {
        var wrap = document.getElementsByClassName('wp-modal-wrap')[0];
        if (wrap)
            wrap.remove();

        var modal = document.getElementsByClassName('wp-modal')[0];
        if (modal)
            modal.remove();

        var style = document.getElementById('wp-style-container');
        if (style)
            style.remove();

        WPFrameObj.remove();
        WPFrameObj = false;
    }

    eventer(messageEvent, function (e) {

        // закрытие модального окна на крестик, после отмены
        if (e.data.status === 'cancel_and_close') {
            WPIsPreorder = false;
            WPCloseModal();
        }

        // закрытие модального окна на внутреннюю кнопку отмены
        if (e.data.status === 'cancel') {
            WPIsPreorder = false;
            WPCloseModal();
        }

        WPChange(e.data);
        if (e.data.status === 'preordered') {
            WPObject.setAttribute('style', '');
            WPObject.setAttribute('class', 'wp-modal');
        }

        if (e.data.status === 'reserved' || e.data.status === 'completed') {
            WPOrder = e.data.data.order_id;
        }
    }, false);

    console.log(params);


    // определим ID виджета
    if ('id' in params) { WPId = params.id }
    else { console.error('Ошибка: не указан ID виджета') }

    // первичная инициализация виджета
    // определим тип виджета
    var type = 'instant';
    if ('type' in params) { type = params.type; WPType = type; }

    // определим параметры, есть ли высота ширина?
    var width = false;
    var height = false;
    if ('width' in params) { width = params.width; }
    if ('height' in params) { height = params.height; globheight = height; }
    //console.log(params, width, height);

    // создадим цсс
    var WPStyle = WPGetCss(params.type, width, height);

    // создадим вёрстку
    var WPHtml = WPGetHtml(type, params);

    var WPPlace = false;
    if ('place' in params) { WPPlace = params.place }

    // положим цсс и вёрстку непосредственно в документ
    // если тип модальный, вставляем в конец документа, если инстант —
    // если place, кладём в place, если нет — кладёт где тэг
    if (type == 'instant') {
        if (WPPlace) {
            var WPPlaceElement = document.querySelector(WPPlace);
            //console.log(WPPlaceElement);
            WPPlaceElement.appendChild(WPStyle);
            WPPlaceElement.appendChild(WPHtml);
        }
        else {
            tag = document.currentScript || (function () {
                var scripts = document.getElementsByTagName('script');
                return scripts[scripts.length - 1];
            })();

            insertBefore(WPStyle, tag);
            insertBefore(WPHtml, tag);
        }
    }
    else {
        document.body.appendChild(WPStyle);
        document.body.appendChild(WPHtml);
    }

    return false;
}
function WPSetUrl(url) {
    WPFrameObj.setAttribute('src', url);
}
function WPOpenModal(params) {
    if (params === undefined)
        params = {};

    localStorage.setItem('wp-open-modal-params', JSON.stringify(params));
    WPParams = params;
    if (!WPIsInitialized) {
        WPModalMode = true;
        return;
    }

    if (WPModalMode === false) {
        WPModalMode = true;
        WPOpenModalReal(params);
    }
}

function WPOpen(params) {
    WPParams = params;
    if (!WPIsInitialized) {
        WPMode = true;
        return;
    }

    if (WPMode === false) {
        WPMode = true;
        WPOpenReal(params);
    }
}

function WPOpenModalReal(params) {
    var WPModal = document.getElementsByClassName('wp-modal');
    WPModal[0].setAttribute('class', 'wp-modal');

    var hashline = '';

    var browser = get_browser();
    if (browser.name !== "MSIE" && browser.name !== "IE") {

        copyWidgetParams = Object.assign({}, WidgetParams);
        Object.assign(copyWidgetParams, params);

        hashline = '#' + JSON.stringify(copyWidgetParams);
    }
    else {
        hashline = '#' + JSON.stringify(WidgetParams);
    }
    WPSetUrl(get_url() + hashline);
    WPModalMode = false;
}
function WPForceCancel() {
    //console.log(WPOrder, WPId);
    //WPFrameObj.setAttribute('src', '/order/cancel/' + WPOrder + '?siteid=' + WPId);
    /*
    WPFrameObj.onload = function () {
        WPIsPreorder = false;
    }
    */
    /*
    if (WPType === 'instant') {
        WPObject.innerHTML = '';
    }
    else {
        var ForceCancelXHR = new XMLHttpRequest();

        ForceCancelXHR.open('GET', '/order/cancel/' + WPOrder + '?siteid=' + WPId);
        ForceCancelXHR.onload = function () {

            var response = JSON.parse(ForceCancelXHR.responseText);

            if (response.Result === 0) {
                //WPFrameObj.innerHTML = '';
                console.log(WPFrameObj);
                WPCloseModal();
            }

        }
        ForceCancelXHR.send();
    }*/
}

function WPOpenReal(params) {
    var hashline = '';

    var browser = get_browser();
    if (browser.name !== "MSIE" && browser.name !== "IE") {

        copyWidgetParams = Object.assign({}, WidgetParams);
        Object.assign(copyWidgetParams, params);

        hashline = '#' + JSON.stringify(copyWidgetParams);
    }
    else {
        hashline = '#' + JSON.stringify(WidgetParams);
    }
    WPSetUrl(get_url() + hashline);
    WPMode = false;
}

function WPCloseModal() {

    if (WPIsPreorder) {
        WPFrameObj.contentWindow.postMessage({ status: 'cancel_and_close' }, '*');
    }
    var WPModal = document.getElementsByClassName('wp-modal');
    WPModal[0].setAttribute('class', 'wp-modal wp-hidden');
    /*
    if (WPIsPreorder) {
        WPIsPreorder = false;
        WPForceCancel();
    }
    else {
    */
    //var WPModal = document.getElementsByClassName('wp-modal');
    //WPModal[0].setAttribute('class', 'wp-modal wp-hidden');
    /*}*/
}
function WPGetHtml(type, params) {

    if (type === 'modal') {
        var wp_modal = document.createElement('div');
        wp_modal.setAttribute('class', 'wp-modal wp-hidden');
        wp_modal.onclick = function (e) {
            if (e.target.className === "wp-modal") {
                WPCloseModal();
            }
        };

        var wp_wrap = document.createElement('div');
        wp_wrap.setAttribute('class', 'wp-modal-wrap');

        var wp_frame = document.createElement('iframe');
        WPFrameObj = wp_frame;

        wp_frame.setAttribute('class', 'wp-frame');
        wp_frame.onload = function () {
            //console.log('detected');
            /*
            var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
            if (iOS) {
                if ('height' in params) {
                    this.parentNode.setAttribute("style", 'overflow-y: auto; -webkit-overflow-scrolling: touch; height: ' + params.height + 'px');
                }
                else {
                    this.parentNode.setAttribute("style", 'overflow-y: auto; -webkit-overflow-scrolling: touch; height: 640px');
                }

                var HtmlBodyStyle = 'height:100%;overflow:auto;-webkit-overflow-scrolling: touch;';
                document.getElementsByTagName("html")[0].setAttribute("style", HtmlBodyStyle);
                document.getElementsByTagName("body")[0].setAttribute("style", HtmlBodyStyle);
            }
            */
        }
        wp_frame.name = 'widget_' + GetGuid();

        var wp_close = document.createElement('div');
        wp_close.setAttribute('class', 'wp-close');
        wp_close.innerHTML = '×';
        wp_close.onclick = function (e) {
            WPCloseModal();
        };

        wp_wrap.appendChild(wp_frame);
        wp_wrap.appendChild(wp_close);

        wp_modal.appendChild(wp_wrap);

        return wp_modal;

    }
    else {

        var wp_frame = document.createElement('iframe');
        WPFrameObj = wp_frame;
        wp_frame.setAttribute('class', 'wp-frame');
        wp_frame.onload = function () {
            /*
            var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
            if (iOS) {
                if ('height' in params) {
                    this.parentNode.setAttribute("style", 'overflow-y: auto; -webkit-overflow-scrolling: touch; height: ' + params.height + 'px');
                }
                else {
                    this.parentNode.setAttribute("style", 'overflow-y: auto; -webkit-overflow-scrolling: touch; height: 640px');
                }
                this.style.height = '100%';

                var HtmlBodyStyle = 'height:100%;overflow:auto;-webkit-overflow-scrolling: touch;';
                document.getElementsByTagName("html")[0].setAttribute("style", HtmlBodyStyle);
                document.getElementsByTagName("body")[0].setAttribute("style", HtmlBodyStyle);
            }
            */
        }
        wp_frame.name = 'widget_' + GetGuid();


        return wp_frame;
        /*
        if (selector !== null) {
            selector.appendChild(wp_style);
            selector.appendChild(wp_frame);
        }
        else {
            insertBefore(wp_style, this.tag);
            insertBefore(wp_frame, this.tag);
        }
        */
    }
}
function WPGetCss(type, width, height) {
    var styleDiv = document.createElement('div');
    styleDiv.setAttribute("id", "wp-style-container");

    var WPStyleTag = document.createElement('style');
    var css = '';

    if (type === 'modal') {
        css += '\
        .wp-frame { height:100%; width:100%; border: 0; } \
        .wp-modal.wp-hidden { display:none; } \
        .wp-modal { display:block; position:fixed; left:0; top:0; height:100%; width:100%; background:rgba(0,0,0,.5); z-index: 9999; } \
        .wp-frame { border:0  } \
        .wp-close { position:absolute; top:0; left:0px; font-size:36px; line-height:36px; height:36px; width:36px; cursor:pointer; color:#000; text-align:center; font-family: sans-serif !important,  } \
        .wp-modal-wrap { position:absolute; left:50%; top:50%; transform:translate(-50%,-50%); max-width:95%; max-height:95%; background-color:#fff;';

        if (height) { css += 'height: ' + height + 'px;'; }
        else { css += 'height: 480px;'; }

        if (width) { css += 'width: ' + width + 'px;'; }
        else { css += 'width: 640px;'; }

        css += '}';
    }
    else {

        css += '.wp-frame { border: 0; min-width: 320px; min-height: 480px; ';

        if (height) { css += 'height: ' + height + 'px;'; }
        else { css += 'height: 100%;'; }

        if (width) { css += 'width: ' + width + 'px;'; }
        else { css += 'width: 100%;'; } if (this.border !== '') { css += 'border:none;'; }

        css += '}';
    }
    WPStyleTag.innerHTML = css;
    styleDiv.appendChild(WPStyleTag);
    return styleDiv;
}

function get_url() {
    var url = document.querySelector('script[src*="external.script.js"]').getAttribute('src');
    return url.toString().replace(/^(.*\/\/[^\/?#]*).*$/, "$1");
}

function WPChange(data) {
    return data;
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function get_browser() {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return { name: 'IE', version: (tem[1] || '') };
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) { return { name: 'Opera', version: tem[1] }; }
    }
    if (window.navigator.userAgent.indexOf("Edge") > -1) {
        tem = ua.match(/\Edge\/(\d+)/)
        if (tem != null) { return { name: 'Edge', version: tem[1] }; }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
    return {
        name: M[0],
        version: +M[1]
    };
}

