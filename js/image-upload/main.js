/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload, .fileupload-comment').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        //url: 'server/php/'
        previewMaxWidth: 155,
        previewMaxHeight: 96,
        previewCrop: true,
        imageMaxWidth: 1400,
        imageMaxHeight: 800,
        disableImageResize: false
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload, .fileupload-comment').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    // Load existing files:
    $('#fileupload, .fileupload-comment').addClass('fileupload-processing');

    var addedCount = 0;
    var doneCount = 0;

    var originalAdd = $.blueimp.fileupload.prototype.options.add;

    $('.fileupload-comment').fileupload({
        dataType: 'json',
        add: function (e, data) {
            addedCount++;
            $('.upload-button').click(function () {
                data.submit();
            });
            originalAdd.call(this, e, data);
        },
        done: function () {
            doneCount++;
            if (doneCount >= addedCount) {
                window.location.href = window.location.href;
            }
        }
    }).on('fileuploadprocessalways', function (e, data) {
        console.log(data);
        var index = data.index,
            file = data.files[index];
        if (file.preview) {
            var $image = $('<img>').attr('src', file.preview.toDataURL());
            data.form.find('.comment-previews').append($image);
        }
    });

});
