function InfiniteScroll(url)
{
    this.init(url);
}

InfiniteScroll.prototype = {
    url: null,
    currentPage: 1,
    handler: null,
    list: null,
    postData: null,
    updatePoint: 0,
    init: function(url) {
        this.url = url;
    },
    update: function(container, postData) {
        this.currentPage = 1;
        this.list = $(container);
        this.postData = postData;
        this.registerHandlers();
    },
    registerHandlers: function() {
        var obj = this;
        if (this.handler !== null) {
            $(window).off('scroll', obj.handler);
        }
        this.handler = this.scrollHandler;
        this.updatePoint = this.list.scrollTop() + this.list.height() - $(window).height();
        $(window).on('scroll', $.proxy(obj.handler, obj));
    },
    scrollHandler: function() {
        if ($(document).scrollTop() > this.updatePoint) {
            var obj = this;
            $(window).off('scroll', this.handler);
            this.handler = null;
            var limiter = obj.url.indexOf("?") === -1 ? '?' : '&';

            $.ajax({
                type: "POST",
                url: obj.url + limiter + 'page=' + (obj.currentPage + 1),
                dataType: 'json',
                data: obj.postData,
                success: function(data){
                    if (!data.empty)
                    {
                        obj.list.append(data.list);
                        obj.currentPage++;
                        obj.registerHandlers();
                    }
                }
            });
        }
    }
}