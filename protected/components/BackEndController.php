<?php
class BackEndController extends CController
{

    /**
     * @var array
     */
    public $menu;
    private $sitemenu;
    public $razdel_id;
    public $razdel_parent;
    public $razdel;
    public $active_menu;
    public $active_user;

    public function filters()
    {
        return array(
            'accessControl',
        );
    }


    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'users'=>array('@'),
            ),
            // всем остальным разрешаем посмотреть только на страницу авторизации
            array(
                'allow',
                'actions'=>array('login','checkemail'),
                'users'=>array('?'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }

	protected function beforeAction($action)
	{
		parent::beforeAction($action);
        $this->razdel = $this->getRazdel();

        if(Yii::app()->controller->id != 'site')
        {
            if (User::openAllowedCat(Yii::app()->controller->id) == false)
            {
                $this->render('//layouts/error', array(

                ));
                die;
            }
        }
		return true;
	}


    public function getRazdel()
    {
        $razdel = array();
        $razdel['name'] = '';
        $razdel['id'] = 0;

        $plugin = Cats::model()->findAll(array('condition'=>'plugin="'.Yii::app()->controller->id.'"'));

        if (isset($plugin[0]))
        {
            $active_razdel = $plugin[0];
            $razdel['id'] =  $active_razdel->id;
            $razdel['name'] = $active_razdel->name;
            $razdel['cats_id'] = $active_razdel->cats_id;
            return $razdel;
        }

    }

   public function getSiteMenu()
   {
        if ($this->sitemenu === null)
        {
            $allowed_cats      =   User::getAllowedCats();
            $this->sitemenu = array(

                );
            /*
                            array(
                    'id' => '99999',
                    'label' => 'Главная',
                    'url' => Yii::app()->createUrl('home/index'),
                    'active' => $this->route === 'home/index',
                    'visible' => 1,
                ),
            */

            $menu = Cats::getMenu();
            $menuSub = Cats::getSubCats();

            // Теперь надо сделать массив из меню и подменю
            /*
            $menu_array = array();

            foreach ($menuSub as $index => $item)
            {
                $menu_array[$item->cats_id][] = array(
                    'id'=>$item->id,
                    'name'=>$item->name,
                    'link'=>$item->link,
                );
            }
            */

            //1. формируем ссылки для меню
            foreach ($menu as $index => $item)
            {
                    $link = Yii::app()->createUrl('pages/index',array('id'=>$item->id));
                    $active = false;

                    if ($item->plugin != '')
                    {
                        $link = Yii::app()->createUrl($item->link);
                    }

                    //$plgn = $item->plugin;
                    //if ($item->plugin == ''){$plgn = 'pages';}

                    if (User::checkCat($item->id,$allowed_cats) == true)
                    {
                        $this->sitemenu[] =
                        array(
                            'id' => $item->id,
                            'label' => $item->name,
                            'url' => $link,
                            'visible' => $item->visible,
                            'plugin' => $item->plugin,
                        );
                    }
            }


            //2. Находим активный раздел
            $razdel['name'] = '';
            $razdel['id'] = 0;
            $razdel['cats_id'] = 0;
            if (Yii::app()->controller->id != 'pages' AND $this->route != 'cats/update')
            {
                $plugin = Cats::model()->findAll(array('condition'=>'plugin="'.Yii::app()->controller->id.'"'));

                if (isset($plugin[0])){
                    $active_razdel = $plugin[0];
                    if ($active_razdel->cats_id == 0)
                    {
                        $razdel['id'] =  $active_razdel->id;
                        $razdel['name'] = $active_razdel->name;
                        $razdel['cats_id'] = $active_razdel->cats_id;
                    }else{
                        $active_razdel = Cats::model()->findByPk($active_razdel->cats_id);
                        if ($active_razdel)
                        {
                            $razdel['id'] =  $active_razdel->id;
                            $razdel['name'] = $active_razdel->name;
                            $razdel['cats_id'] = 0;
                        }
                    }
                }
            }else{
                if (isset($_GET['id']))
                {
                        $active_razdel = Cats::model()->findByPk((int)$_GET['id']);
                        if ($active_razdel->cats_id == 0)
                        {
                            $razdel['id'] =  $active_razdel->id;
                            $razdel['name'] = $active_razdel->name;
                            $razdel['cats_id'] = $active_razdel->cats_id;
                        }else{
                            $active_razdel = Cats::model()->findByPk($active_razdel->cats_id);
                            if ($active_razdel)
                            {
                                $razdel['id'] =  $active_razdel->id;
                                $razdel['name'] = $active_razdel->name;
                                $razdel['cats_id'] = 0;
                            }
                        }
                }
            }

            /*
            if (Yii::app()->controller->id == 'home'){
                 $razdel['id'] =  '99999';
                 $razdel['name'] = 'Главная';
                 $razdel['cats_id'] = 0;
            }
            */

            $this->active_menu = $razdel;

            if (Yii::app()->user->role == 'user')
            {
                //unset($this->sitemenu);
                $this->sitemenu = array();
                $this->sitemenu[] =
                array(
                    'label' => 'Конференции',
                    'url' => Yii::app()->createUrl('conf/jury'),
                    'active' => true,
                    'visible'  => true,
                    'id'=>0
                );
            }
        }
            return $this->sitemenu;
    }



	public function phoneFormat($phoneNumber){

            $phoneNumber = preg_replace('/[^0-9]/','',$phoneNumber);

		    if(strlen($phoneNumber) >= 10) {
		        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
		        $areaCode = substr($phoneNumber, -10, 3);
		        $nextThree = substr($phoneNumber, -7, 3);
		        $lastFour = substr($phoneNumber, -4, 4);

		        $phoneNumber = $countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
		    }
		    else if(strlen($phoneNumber) == 7) {
		        $first3 = substr($phoneNumber, 0, 3);
		        $middle2 = substr($phoneNumber, 3, 2);
		        $last2 = substr($phoneNumber, 5, 2);

		        $phoneNumber = $first3.'-'.$middle2.'-'.$last2;
		    }
			else if(strlen($phoneNumber) == 6) {
		        $nextThree = substr($phoneNumber, 0, 3);
		        $lastFour = substr($phoneNumber, 3, 4);

		        $phoneNumber = $nextThree.'-'.$lastFour;
		    }
			else if(strlen($phoneNumber) == 5) {
		        $first3 = substr($phoneNumber, 0, 1);
		        $middle2 = substr($phoneNumber, 1, 2);
		        $last2 = substr($phoneNumber, 3, 2);

		        $phoneNumber = $first3.'-'.$middle2.'-'.$last2;
		    }

		    return $phoneNumber;

	}

	public function date_makeLongStringName($date){
			if ($date != '0000-00-00' and $date != ''){
			$date_longName = array(
			    "01" => "Января",
			    "02" => "Февраля",
			    "03" => "Марта",
			    "04" => "Апреля",
			    "05" => "Мая",
			    "06" => "Июня",
			    "07" => "Июля",
			    "08" => "Августа",
			    "09" => "Сентября",
			    "10" => "Октября",
			    "11" => "Ноября",
			    "12" => "Декабря"
			);
			     $getdate=explode('-',$date);
			     $result = $getdate[2]." ".$date_longName[$getdate [1]]." ".$getdate [0];
			}else{
				$result = '';
			}
			   return $result;
	}


        public function crop_str_word($text, $max_words, $append = ' …')
    	{
    	        $max_words = $max_words+1;
                $count = count(explode(' ', $text));

                if ($count > $max_words)
    		   {
    		       $words = explode(' ', $text, $max_words);
    			   if (is_array($words)==true && count($words) > 1){

    			       array_pop($words);

    			       $text = implode(' ', $words) . $append;

    		       }
    		   }

    	       return $text;
    	}

}
