<?php

/**
 * Загрузчик фоток
 */
class FileUploader
{

	/**
	 * @var CDbConnection подключение к базе данных
	 */
	private $db;

	/**
	 * PhotoUploader constructor.
	 */
	public function __construct()
	{
		$this->db = Yii::app()->db;
	}

	/**
	 * Сохраняет и привязывает фотку к объекту
	 * @param CUploadedFile $file загруженный файл с фоткой
	 * @param PhotosInterface $object объект, к которому добавляется фотография
	 * @param array $info поля с дополнительной информацией о фотке (title, author, description)
	 * @return int айди загруженной фотки или false, если что-то пошло не так
	 */
	public function addPhoto(CUploadedFile $file, $X_SIZE, $Y_SIZE, $path)
	{
		$filename = uniqid().'_'.time() . '.' . $file->extensionName;
		$success = false;

	    foreach (array('normal','small') as $size)
		{
		    //echo $size." - ".$filename."<br>";
		    $s = '';
            if ($size == 'small'){$s = 's_';}
			$newFile = $path . $s.$filename;
			$success = $file->saveAs($newFile, false);

			if ($success)
            {
				chmod($newFile, 0644);
                //echo 'next is try<br>';
			   try{

				    //echo "try";
				    $img = new SimpleImage($newFile);

                    
                    //ресайзим
					switch ($size)
					{
						case 'normal':
                            $img->best_fit($X_SIZE, $Y_SIZE);
							break;
						case 'small':
                            //$img->thumbnail(360,null);
                            $img->fit_to_width(360);
                            //$img->fitToWidth(360, null);
							break;
					}
      				$img->save();
                }
			    catch (Exception $e)
                {
				    //echo 'error'.$e;
					@unlink($newFile);
					$success = false;
                }
			}
        }

		return $filename;
	}

	/**
	 * Сохраняет документ
	 * @param CUploadedFile $file загруженный файл с фоткой
	 */
	public function addFile(CUploadedFile $file, $path)
	{
		    $filename = uniqid().'_'.time() . '.' . $file->extensionName;
		    $success = false;

			$newFile = $path . $filename;
			$success = $file->saveAs($newFile, false);
    		return $filename;
	}
}
