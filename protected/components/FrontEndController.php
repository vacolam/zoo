<?php
class FrontEndController extends CController
{

    /**
     * @var array
     */
    public $menu;
	public $cart;
	public $pageTitle = 'Сахалинский Зооботанический парк.';
	public $siteDescription = 'Зоопарк в городе Южно-Сахалинск Сахалинской области.';
	public $siteKeywords = 'Сахалин, зоопарк, ботанический парк, животные, звери, птицы, аквариум, приматник, змеи, ящерицы';
	public $activeKeywords;
	public $activeDescription;
    public $razdel_id;
    public $razdel;

	protected function beforeAction($action)
	{
		parent::beforeAction($action);
        $this->menu = $this->getMenu();
		return true;
	}

    public function getRazdel($razdel_id = 0)
    {
        $razdel = array();
        $razdel['name'] = '';
        $razdel['id'] = 0;
        $razdel['cats_id'] = 0;
        if ($razdel_id > 0)
        {
            $razdel = Cats::model()->findByPk($razdel_id);
            $razdel['name'] = $razdel->name;
            $razdel['id'] = $razdel->id;
            $razdel['cats_id'] = $razdel->cats_id;

            if ($razdel->cats_id == 0){
                $razdel['cats_id'] = $razdel->id;
            }
        }

        return $razdel;
    }    
    
    /**
     * Возвращает массив с элементами меню для сайта
     * @return array
     */
    public function getMenu()
    {

        if (Yii::app()->controller->id != 'eng'){
        if ($this->menu === null)
        {

        $this->menu = array();
        /*
            $this->menu[] =
                array(
                    'id' => '0',
                    'label' => 'Главная',
                    'url' => Yii::app()->createUrl('home/index'),
                    'active' => $this->route === 'home/index',
                );
            */

            $menu = Cats::getMenu();
            $menuSub = Cats::getSubCats();

            // Теперь надо сделать массив из меню и подменю
            $menu_array = array();

            foreach ($menuSub as $index => $item)
            {

                if ($item->visible == 0){continue;}
                if ($item->backend == 1){continue;}
                $active_link = 'pages/view';
                $link = Yii::app()->createUrl('pages/view',array('id'=>$item->id));

                if ($item->plugin != '')
                {
                    $link = Yii::app()->createUrl($item->link);
                }

                $menu_array[$item->cats_id][] = array(
                    'id'=>$item->id,
                    'name'=>$item->name,
                    'link'=>$link,
                );
            }

            //1. формируем ссылки для меню
            foreach ($menu as $index => $item)
            {
                    if ($item->visible == 0){continue;}
                    $active_link = 'pages/view';
                    $link = Yii::app()->createUrl('pages/view',array('id'=>$item->id));
                    $active = false;

                    if ($item->plugin != '')
                    {
                        $link = Yii::app()->createUrl($item->link);
                    }
                    else if ($item->plugin == '')
                    {
                        $subs = array();
                        if (isset($menu_array[$item->id])){
                            $subs = $menu_array[$item->id];
                        }

                        if (isset($subs[0]))
                        {
                            if ($subs[0]['link'] != '')
                            {
                                $link = $subs[0]['link'];
                            }
                            else
                            {
                                $link = Yii::app()->createUrl('pages/view',array('id'=>$subs[0]['id']));
                            }
                        }else{
                            $link = Yii::app()->createUrl('pages/view',array('id'=>$item->id));
                        }
                    }

                    $submenu = array();
                    if (isset($menu_array[$item->id]))
                    {
                        $submenu = $menu_array[$item->id];
                    }

                    $this->menu[] =
                    array(
                        'id' => $item->id,
                        'label' => $item->name,
                        'url' => $link,
                        'active' => $active,
                        'keywords' => $item->keywords,
                        'description' => $item->description,
                        'submenu'   => $submenu
                    );
            }


            //2. Находим активный раздел
            $razdel['name'] = '';
            $razdel['id'] = 0;
            $razdel['cats_id'] = 0;
            if (Yii::app()->controller->id != 'pages')
            {
                $plugin = Cats::model()->findAll(array('condition'=>'plugin="'.Yii::app()->controller->id.'"'));

                if (isset($plugin[0])){
                    $active_razdel = $plugin[0];
                    if ($active_razdel->cats_id == 0)
                    {
                        $razdel['id'] =  $active_razdel->id;
                        $razdel['name'] = $active_razdel->name;
                        $razdel['cats_id'] = $active_razdel->cats_id;
                    }else{
                        $active_razdel = Cats::model()->findByPk($active_razdel->cats_id);
                        if ($active_razdel)
                        {
                            $razdel['id'] =  $active_razdel->id;
                            $razdel['name'] = $active_razdel->name;
                            $razdel['cats_id'] = 0;
                        }
                    }
                }
            }else{
                if (isset($_GET['id']))
                {
                        $active_razdel = Cats::model()->findByPk((int)$_GET['id']);
                        if ($active_razdel)
                        {
                            if ($active_razdel->cats_id == 0)
                            {
                                $razdel['id'] =  $active_razdel->id;
                                $razdel['name'] = $active_razdel->name;
                                $razdel['cats_id'] = $active_razdel->cats_id;
                            }else{
                                $active_razdel = Cats::model()->findByPk($active_razdel->cats_id);
                                if ($active_razdel)
                                {
                                    $razdel['id'] =  $active_razdel->id;
                                    $razdel['name'] = $active_razdel->name;
                                    $razdel['cats_id'] = 0;
                                }
                            }
                        }
                }


                //3. Прописываем title, устанавливаем description и keywords
                if (isset($active_razdel))
                {
                    if ($active_razdel->keywords != ''){
                        $this->activeKeywords = $active_razdel->keywords;
                    }else{
                        $this->activeKeywords = $this->siteKeywords.", раздел ".$active_razdel->name;
                    }

                    if ($active_razdel->description != ''){
                        $this->activeDescription = $active_razdel->description;
                    }else{
                        $this->activeDescription = "Раздел ".$active_razdel->name.". ".$this->siteDescription;
                    }

                    $this->pageTitle = $active_razdel->name.". ".$this->pageTitle;
                    $this->siteDescription = $this->activeDescription;
                    $this->siteKeywords = $this->activeKeywords;
                }

            }
            $this->razdel = $razdel;
        }
        }else{
        $menu = Eng::getMenu();

        $menuSub = array();

        // Теперь надо сделать массив из меню и подменю
        $menu_array = array();

        foreach ($menuSub as $index => $item)
        {
            $menu_array[$item->cats_id][] = array(
                'id'=>$item->id,
                'name'=>$item->name,
                'link'=>$item->link,
            );
        }

        foreach ($menu as $index => $item)
        {
                if ($item->visible == 0){continue;}
                $active_link = 'eng/view';
                $link = Yii::app()->createUrl('eng/view',array('id'=>$item->id));
                $active = false;

                $subs = array();
                if (isset($menu_array[$item->id])){
                    $subs = $menu_array[$item->id];
                }

                $subs_ids = array();
                if (count($subs) > 0){
                    foreach ($subs as $s_item)
                    {
                        $subs_ids[] = $s_item['id'];
                    }
                }

                if (count($subs) > 0){
                    if (isset($subs[0]))
                    {
                        $link = Yii::app()->createUrl('eng/view',array('id'=>$subs[0]['id']));
                        if ($this->route == 'eng/view' AND in_array((int)$_GET['id'],$subs_ids)){
                            $active = true;
                        }
                    }
                }else if (count($subs) == 0){
                        $link = Yii::app()->createUrl('eng/view',array('id'=>$item->id));
                        if ($this->route == 'eng/view' AND $item->id == (int)$_GET['id']){
                            $active = true;
                        }
                }

                $this->menu[] =
                array(
                    'label' => $item->name,
                    'url' => $link,
                    'active' => $active,
                    'keywords' => $item->keywords,
                    'description' => $item->description,
                );


                //Прописываем title, устанавливаем description и keywords
                if ($active == true){
                    if ($item->keywords != ''){
                        $this->activeKeywords = $item->keywords;
                    }else{
                        $this->activeKeywords = $this->siteKeywords.", раздел ".$item->name;
                    }

                    if ($item->description != ''){
                        $this->activeDescription = $item->description;
                    }else{
                        $this->activeDescription = "Раздел ".$item->name.". ".$this->siteDescription;
                    }

                    $this->pageTitle = $item->name.". ".$this->pageTitle;
                    $this->siteDescription = $this->activeDescription;
                    $this->siteKeywords = $this->activeKeywords;
                }
        }

        }
        return $this->menu;
    }

	public function date_makeLongStringName($date){
		if ($date != '0000-00-00' and $date != ''){
		$date_longName = array(
		    "01" => "Января",
		    "02" => "Февраля",
		    "03" => "Марта",
		    "04" => "Апреля",
		    "05" => "Мая",
		    "06" => "Июня",
		    "07" => "Июля",
		    "08" => "Августа",
		    "09" => "Сентября",
		    "10" => "Октября",
		    "11" => "Ноября",
		    "12" => "Декабря"
		);
		     $getdate=explode('-',$date);
		     $result = $getdate[2]." ".$date_longName[$getdate [1]]." ".$getdate [0];
		}else{
			$result = 'Дата не указана';
		}
		   return $result;
		}

        public function crop_str_word($text, $max_words, $append = ' …')
    	{
    	       $max_words = $max_words+1;
        $count = count(explode(' ', $text));

        if ($count > $max_words)
    		   {
    		       $words = explode(' ', $text, $max_words);
    			   if (is_array($words)==true && count($words) > 1){

    			       array_pop($words);

    			       $text = implode(' ', $words) . $append;

    		       }
    		   }

    	       return $text;
    	}
}
