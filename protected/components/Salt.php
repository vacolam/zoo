<?php
/**
 * Солит строки для дальнешйного хэширования
 */
class Salt
{
	
	/**
	 * Возвращает подсоленный пароль
	 * @param string $string пароль
	 * @return string
	 */
	public static function password($string)
	{
		return 'зубочистка'.$string.'UNLimited';
	}
}

