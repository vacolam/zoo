<?php
/**
 * @file
 * Файл с классом для отправки почты через SMTP.
 *
 */

/**
 * Класс для отправки почты через SMTP.
 *
 * Используется так:
 * <pre>

 * </pre>
 *
 * Класс написан на основе этой статьи. Там можно найти что, куда, как и зачем происходит...
 * @link http://webi.ru/webi_articles/6_10_f.html
 */
class SmtpMail
{
    /**
     * @var string smtp сервер для подключения.
     */
    protected $server;

    /**
     * @var int порт подключения. По умолчанию - 25.
     */
    protected $port;

    /**
     * @var string логин для подключения к smtp. Обычно это адрес электронной почты.
     */
    protected $login;

    /**
     * @var string пароль для подключения к smtp.
     */
    protected $password;

    /**
     * @var type соеденение с smtp-сервером.
     */
    protected $smtp_conn;

    public $auto_close = true;

    /**
     * @var string кодировка письма. По умолчанию UTF-8.
     */
    protected $encoding = "UTF-8";


    /**
     * Констрктор. Устанавливает значения для подключению к SMTP-серверу.
     * @param string $server адрес сервера, например smtp.mail.ru
     * @param string $login логин для подключения, например samel@bk.ru.
     * @param string $password пароль для подключения.
     */
    public function __construct($server, $login, $password, $port=25, $auto_close = true)
    {
        $this->server = $server;
        $this->login = $login;
        $this->password = $password;
        $this->port = $port;
        $this->auto_close = $auto_close;
    }

    /**
     * Устанавливает кодировку для письма.
     * @param string $value Новая кодировка
     */
    public function setEncoding($value)
    {
        $this->encoding = $value;
    }

    /**
     *	Возвращает текущий ответ сервера.
     * @return string ответ сервера.
     */
    private function getData()
    {
        $data="";
        while($str = fgets($this->smtp_conn,515))
        {
            $data .= $str;
            if(substr($str,3,1) == " ")
            {
                break;
            }
        }
        return $data;
    }

    /**
     * Возвращает код ответа сервера.
     * @return string Код ответа.
     */
    private function getCode()
    {
        return substr($this->getData(), 0, 3);
    }

    /**
     * Отпарвляет команду на сервер.
     * Возвращает код ответа сервера.
     * @param string $command Команда.
     * @return string код ответа сервера.
     */
    private function sendCommand($command)
    {
        fputs($this->smtp_conn, $command."\r\n");
        return $this->getCode();
    }

    /**
     * Отпаравляет письмо.
     * @param string $to адрес получателя.
     * @param string $subject тема письма.
     * @param string $text текст письма.
     * @param string $additional_headers дополнительные заголовки для письма.
     * @return mix true если письмо отправлено успешно, иначе сообщени об ошибке.
     */
    public function sendMail($to, $subject, $text, $additional_headers="", $from_label="sakhalinzoo", $attach="")
    {
        //обрабатываем заголовки
        $header="Date: ".date("D, j M Y G:i:s")." +1100\r\n";
        $header.="From: =?".$this->encoding."?Q?".str_replace("+","_",str_replace("%","=",urlencode($from_label)))."?= <".$this->login.">\r\n";
        $header .= "To: ".$to."\r\n";
        $header.="Subject: =?".$this->encoding."?Q?".str_replace("+","_",str_replace("%","=",urlencode($subject)))."?=\r\n";
        //$header.="Content-Type: text/plain; charset=".$this->encoding."\r\n";
        if ($attach == "")
        {
            $header.="Content-Type: text/html; charset=".$this->encoding."\r\n";
            $header.="Content-Transfer-Encoding: 8bit\r\n";
        }
        else
        {
            //Прикрепляем файлы к письму
            $header.="MIME-Version: 1.0\r\n";
            $header.="Content-Type: multipart/mixed; boundary=\"----------A4D921C2D10D7DB\"\r\n\r\n";

        /*	$inf = pathinfo($attach);
            $name = $inf['basename'];
            //echo $name;*/

            $fp = fopen($attach['full_name'], "rb");
            $file = chunk_split(base64_encode(fread($fp, filesize($attach['full_name']))));
            fclose($fp);
            //$ext = FileHelper::getExtension($attach);

            $name = $attach['name'];
            //$name = basename($attach);
            $new_text = "------------A4D921C2D10D7DB\r\n";
            $new_text .= "Content-Type: text/html; charset=".$this->encoding."\r\n";
            $new_text .= "Content-Transfer-Encoding: 8bit\r\n\r\n";
            $new_text .= $text."\r\n\r\n";
            $new_text .= "------------A4D921C2D10D7DB\r\n";
            $new_text .= "Content-Type: application/octet-stream; name=\"=?".$this->encoding."?B?".base64_encode($name)."?=\"\r\n";
            $new_text .= "Content-Transfer-Encoding: base64\r\n";
            $new_text .= "Content-Disposition: attachment; filename=\"=?".$this->encoding."?B?".base64_encode($name)."?=\"\r\n\r\n";
            $new_text .= $file."\r\n";
            $new_text .= "------------A4D921C2D10D7DB--";
            $text = $new_text;
        }
        if ($additional_headers == "")
        {
            //$header.="X-Mailer: The Bat! (v3.99.3) Professional\r\n";
        //	$header.="Reply-To: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode('Максим')))."?= <samel@bk.ru>\r\n";
        //	$header.="X-Priority: 3 (Normal)\r\n";
        //	$header.="Message-ID: <172562218.".date("YmjHis")."@mail.ru>\r\n";
            //$header.="To: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode('')))."?= <".$to.">\r\n";
        //	$header.="To: ".$to."\r\n";
        //	$header.="Subject: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode('проверка')))."?=\r\n";
        //	$header.="MIME-Version: 1.0\r\n";
        //	$header.="Content-Type: text/plain; charset=utf-8\r\n";
        //	$header.="Content-Transfer-Encoding: 8bit\r\n";
            ////echo $header;
        }
        else
        {
            $header .= $additional_headers;
        }

        //$text="привет, проверка связи.";

        return $this->_send($to, $text, $header);

    }


    public function _open()
    {
        if (is_null($this->smtp_conn))
        {
            $this->smtp_conn = fsockopen($this->server, $this->port, $errno, $errstr, 10);
        	//echo 'Соеденение открыто <br>';
            if (!$this->smtp_conn)
            {
                $result = "соединение с серверов не прошло";
                $this->_close();
                //echo $result;
                return $result;
            }

            $data = $this->getData();

            if ($this->sendCommand("EHLO ".$this->login) != 250)
            {
                $result = "ошибка приветсвия EHLO";
                $this->_close();
                //echo $result;
                return $result;
            }

            if ($this->sendCommand("AUTH LOGIN") != 334)
            {
                $result = "сервер не разрешил начать авторизацию";
                $this->_close();
                //echo $result;
                return $result;
            }

            if ($this->sendCommand(base64_encode($this->login)) != 334)
            {
                $result = "ошибка доступа к такому юзеру";
                $this->_close();
                //echo $result;
                return $result;
            }

            if ($this->sendCommand(base64_encode($this->password)) != 235)
            {
                $result = "не правильный пароль";
                $this->_close();
                //echo $result;
                return $result;
            }
        }
    }

    /**
     *
     */
    public function _mail($to, $text, $header)
    {
        $size_msg = mb_strlen($header."\r\n".$text. "UTF-8");

        if ($this->sendCommand("MAIL FROM:<".$this->login."> SIZE=".$size_msg) != 250)
        {
            $result = "сервер отказал в команде MAIL FROM";
            $this->_close();
                //echo $result;
            return $result;
        }

        $to_array = explode(", ", $to);
        foreach ($to_array as $mail)
        {
            $code = $this->sendCommand("RCPT TO:<".$mail.">");
            if ($code != 250 AND $code != 251)
            {
                $result = "Сервер не принял команду RCPT TO для ".$mail;
                $this->_close();
                //echo $result;
                return $result;
            }
        }

        if ($this->sendCommand("DATA") != 354)
        {
            $result = "сервер не принял DATA";
            $this->_close();
                //echo $result;
            return $result;
        }

        if ($this->sendCommand($header."\r\n".$text."\r\n.") != 250)
        {
            $result = "ошибка отправки письма";
            $this->_close();
                //echo $result;
            return $result;
        }
    }

    public function _close($quit = false)
    {
        if (!is_null($this->smtp_conn))
        {
            if ($quit)
            {
                $this->sendCommand("QUIT");
            }
            fclose($this->smtp_conn);
            $this->smtp_conn = null;
        //	//echo 'Соеденение закрыто <br>';
        }
    }


    public function _send($to, $text, $header)
    {
        $result_open = $this->_open();

        ////echo "результат отправки ".$result_open;

        $this->_mail($to, $text, $header);



        if ($this->auto_close)
        {

            $this->_close(true);
        }
        return true;
    }
}

?>
