<?php
class UserIdentity extends CUserIdentity
{
    private $_id;

    private $usePassword = true;

    public function authenticate()
    {
        $query = "SELECT id, email, password, role FROM {{users}} WHERE email = :email";
        $params = array(':email' => $this->username);
        $user_data = Yii::app()->db->createCommand($query)->queryRow(true, $params);

        if ($user_data === false)
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        elseif (!CPasswordHelper::verifyPassword(Salt::password($this->password),
                $user_data['password']) && $this->usePassword)
        {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        else
        {
            $this->_id = $user_data['id'];
            $this->errorCode = self::ERROR_NONE;
        }
        //echo $this->errorCode;
        return !$this->errorCode;
    }

    public function usePassword($value)
    {
        $this->usePassword = (bool)$value;
    }

    public function getId()
    {
        return $this->_id;
    }


    /*
    public function authenticate()
    {
        $password = '123456';

        if (!CPasswordHelper::verifyPassword(
            $this->password,
            CPasswordHelper::hashPassword($password)
        )) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
    */
}