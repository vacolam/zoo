<?php
class WebUser extends CWebUser implements UserInterface 
{

    public $model;
    public $role = 'guest';
    public $email;
    public $name;
    public $accesss;
    public $notify;

    public function init()
    {
        parent::init();
        if (!$this->isGuest)
        {
            $this->model = User::model()->findByPk($this->id);
            $data = $this->model->getAttributes(array('email', 'name', 'role','accesss','notify'));
            foreach ($data as $field => $value)
            {
                $this->$field = $value;
            }
        }

    }

    public function checkAccess($operation, $params = array())
    {
        if (empty($this->id))
        {
            // Not identified => no rights
            return false;
        }
        if ($this->role === 'admin')
        {
            return true; // admin role has access to everything
        }
        // allow access if the operation request is the current user's role
        return ($operation === $this->role);
    }

    public function setRegion($region)
    {
        $this->setState('region', $region);
    }
    
    public function isInKhabarovsk()
    {
        return $this->getState('region', 'khv') === 'khv';
    }
}