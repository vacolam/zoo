<?php
$mainConfig = require __DIR__ . '/main.php';
$envConfig  = file_exists(__DIR__ . '/env.php')
    ? require __DIR__ . '/env.php' : array();
$backConfig = array(
    'defaultController' => 'dashboard',
    'components' => array(
        'clientScript' => array(
            'scriptMap' => array(
                'jquery.js' => 'js/jquery.min.js',
            )
        ),
        'user' => array(
            'loginUrl' => array('site/login'),
            'allowAutoLogin' => true,
            'class'          => 'WebUser',
        )
    ),
);
return array_replace_recursive($mainConfig, $backConfig, $envConfig);