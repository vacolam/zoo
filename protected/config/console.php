<?php
$base = array(
	'basePath'   => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'       => 'zoo-console',
	'commandMap' => array(
		'migrate' => array(
			'class' => 'system.cli.commands.MigrateCommand',
			'migrationTable' => '{{migrations}}'
		)
	)
);

$env = file_exists(dirname(__FILE__).'/env.php') ? require_once 'env.php' : array();

return array_merge($base, $env);