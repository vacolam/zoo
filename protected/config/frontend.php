<?php
$mainConfig = require __DIR__ . '/main.php';
$envConfig  = file_exists(__DIR__ . '/env.php')
    ? require __DIR__ . '/env.php' : array();
$frontConfig = array(
    'defaultController' => 'home',
);
return array_replace_recursive($mainConfig, $frontConfig, $envConfig);