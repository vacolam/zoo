<?php

class AfishaController extends BackEndController
{

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }

    public $razdel_id = 2;

    public function actionIndex($tag = 0, $month = 0)
    {

        $tag_sql = '';
        if ((int)$tag > 0)
        {
            $tag_sql = " AND tags like '%\"".$tag."\"%'";
        }


        $year = date('Y');
        $month = (int)$month;
        $month_condition = '';
        if ($month > 0)
        {
            $month_condition = ' AND ( ("'.$month.'" = MONTH(date) AND end_date ="0000-00-00" AND YEAR(date) = "'.$year.'" ) OR ("'.$month.'" >= MONTH(date) AND "'.$month.'" <= MONTH(end_date)   AND YEAR(date) = "'.$year.'" )  )';
        }


        $dataProvider = new CActiveDataProvider('Afisha', array(
            'criteria' => array(
                'condition'=>'id > 0'.$tag_sql.$month_condition,
                'order' => 'date DESC, id DESC',
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));

        $query = "SELECT * FROM {{tags}} WHERE id > 0 and type = 'afisha' ORDER BY name";
            $params = array();
            $tags = Yii::app()->db->createCommand($query)->query($params)->readAll();

        $this->render('index', array(
            'provider' => $dataProvider,
            'tags' => $tags,
            'month' => $month,
        ));
    }

    public function actionOrders()
    {


        $dataProvider = new CActiveDataProvider('Groups', array(
            'criteria' => array(
                'condition'=>'',
                'order' => 'id desc',
            ),
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));

        $this->render('orders', array(
            'provider' => $dataProvider,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        $this->saveFromPost($model);

            $query = "SELECT * FROM {{tags}} WHERE id > 0 and type = 'afisha' ORDER BY name";
            $params = array();
            $tags = Yii::app()->db->createCommand($query)->query($params)->readAll();

        	$checked_tags = array();
            if ($model->tags != ''){
            $checked_tags = json_decode($model->tags);
            }

        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "afisha" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "afisha" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

                $timetable = array();
                $timetable['mn'] = GroupsTime::model()->findAll(array('condition'=>'week_day="mn" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
                $timetable['tu'] = GroupsTime::model()->findAll(array('condition'=>'week_day="tu" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
                $timetable['we'] = GroupsTime::model()->findAll(array('condition'=>'week_day="we" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
                $timetable['th'] = GroupsTime::model()->findAll(array('condition'=>'week_day="th" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
                $timetable['fr'] = GroupsTime::model()->findAll(array('condition'=>'week_day="fr" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
                $timetable['sa'] = GroupsTime::model()->findAll(array('condition'=>'week_day="sa" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
                $timetable['su'] = GroupsTime::model()->findAll(array('condition'=>'week_day="su" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));

                $ordersProvider = new CActiveDataProvider('GroupsOrders', array(
                        'criteria' => array(
                                'condition'=>'groups_id="'.$id.'"',
                                'order' => 'id desc',
                        ),
                        'pagination' => array(
                                'pageSize' => 50,
                        ),
                ));

        $this->render('form', array(
            'model' => $model,
            'photos' => $photos,
            'docs' => $docs,
            'tags' => $tags,
            'checked_tags' => $checked_tags,
            'timetable' => $timetable,
            'ordersProvider' => $ordersProvider,
        ));
    }
    
    public function actionCreate()
    {
        $model = new Afisha();
        $model->date = date('Y-m-d');
        $this->saveFromPost($model);
            $query = "SELECT * FROM {{tags}} WHERE id > 0 and type = 'afisha' ORDER BY name";
            $params = array();
            $tags = Yii::app()->db->createCommand($query)->query($params)->readAll();

        	$checked_tags = array();
            if ($model->tags != ''){
            $checked_tags = json_decode($model->tags);
            }

        $this->render('form', array(
            'model' => $model,
            'tags' => $tags,
            'checked_tags' => $checked_tags,
        ));
    }
    
    private function saveFromPost(Afisha $model)
    {
        if (isset($_POST['Afisha'])) {
            $model->has_groups = 0;
            $model->attributes = $_POST['Afisha'];
            $model->image = CUploadedFile::getInstance($model, 'image');

           	$date = new DateTime($_POST['Afisha']['date']);
			$model->date = $date->format('Y-m-d');

            if ($_POST['Afisha']['end_date'])
            {
        	$date = new DateTime($_POST['Afisha']['end_date']);
            $model->end_date = $date->format('Y-m-d');
            }

            $isNew = $model->isNewRecord;

            if ($model->short_text == ''){
                $model->short_text = trim(strip_tags($this->crop_str_word($model->text, 30 )));
            }

            if (isset($_POST['Tags'])) {
                $tags = json_encode($_POST['Tags']);
                $model->tags = $tags;
            }

            if ($model->save()) {
                if ($model->image) {
                    //$file_name = uniqid().'_'.time() . '.' . $model->image->extensionName;
                    //$path = Yii::getPathOfAlias('webroot').'/upload/news/'.$file_name;
                    //$model->image->saveAs($path);

                    //$image = new SimpleImage($path);
                    //$image->thumbnail(News::IMAGE_WIDTH, News::IMAGE_HEIGHT);
                    //$image->save($path, 80);

                    $uploader = new FileUploader();
                    $file_name = $uploader->addPhoto($model->image, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/afisha/');

                    if ($model->filename != '' AND $file_name != ''){
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/afisha/'.$size.$model->filename;
                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                            }
                        }
                    }

                    $model->filename = $file_name;
                    $model->save();


                } elseif (isset($_POST['Afisha']['removeImage'])) {
                    if ($model->filename != ''){
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/afisha/'.$size.$model->filename;

                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                                 //echo $deleted_path."<br>";
                                 $model->filename = '';
                                 $model->save();
                            }
                        }
                    }
                }
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Мероприятие добавлено' : 'Изменения сохранены'
                );

                //$this->redirect(array('index'));
                $this->redirect(array('update','id'=>$model->id));
            }
        }
    }
    
    public function actionDelete($id)
    {
        $model = $this->getModel($id);
        $photos = $model->photos;
        if ($photos)
        {
            foreach($photos as $photo_model){
                Photos::deleteImage($photo_model);
            }
        }

        $docs = $model->docs;
        if ($docs)
        {
            foreach($docs as $doc_model){
                Docs::deleteDoc($doc_model);
            }
        }

        if ($model->filename != '')
        {
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/afisha/'.$size.$model->filename;

                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                            }
                        }
        }


        $model->delete();
        $this->redirect(array('index'));
    }
    
    protected function getModel($id)
    {
        $model = Afisha::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    public function actionFilesAdd($post_id)
    {
        $post_item = Afisha::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $afisha_photos = new Photos;
                $afisha_photos->post_id = $post_id;
                $afisha_photos->date = new CDbExpression('NOW()');
                $afisha_photos->filename = $uploaded_file;
                $afisha_photos->type = 'afisha';
                $afisha_photos->save();

                if ($post_item->cover_id == 0 || $post_item->cover_id == '')
                {
                    $post_item->cover_id = $afisha_photos->id;
                    $post_item->save();
                }
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $afisha_docs = new Docs;
                $afisha_docs->post_id = $post_id;
                $afisha_docs->date = new CDbExpression('NOW()');
                $afisha_docs->filename = $uploaded_file;
                $afisha_docs->filetitle = $file->getName();
                $afisha_docs->type = 'afisha';
                $afisha_docs->save();
            }
        }
        }
    }

    public function actionDeletePhotos($id){
        $result = false;
        $photo = Photos::model()->findByPk($id);
        if ($photo)
        {
            $post_id = $photo->post_id;
            Photos::deleteImage($photo);

            $post = Afisha::model()->findByPk($post_id);
            if ($post){
                if ($post->cover_id == $id){
                    $post->cover_id = 0;
                    $post->save();

                    $result = Afisha::autoCover($post);
                }
            }
        }
        echo CJSON::encode($result);
    }
    public function actionDeleteDocs($id){
        $model = Docs::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Docs::deleteDoc($model);
            //$this->redirect(array('update','id'=>$post_id));
        }
    }

    public function actionSetCover($news_id, $cover_id){
        $news_item = Afisha::model()->findByPk($news_id);
        if ($news_item)
        {
            $news_item->cover_id = $cover_id;
            $news_item->save();

            $new_cover_adress = $news_item->getThumbnailUrl();

            echo json_encode($new_cover_adress);
        }
    }

    public function actionAddTag($tag_name){
        if ($tag_name != '')
        {
            $tag = new Tags();
            $tag->name = $tag_name;
            $tag->type = 'afisha';
            $tag->save();
            echo CJSON::encode($tag->id);
        }
    }

    public function actionDeleteTag($id){
        if ((int)$id != 0)
        {
            $tag = Tags::model()->findByPk($id);
            if ($tag)
            {
                $tag->delete();
            }
        }
    }

    public function actionAddTime($time,$week_day,$groups_id){
        if ($time != '' and $week_day != '' and $groups_id != '')
        {
            $other_times =  GroupsTime::model()->findAll(array('condition'=>'week_day="'.$week_day.'" and groups_id="'.$groups_id.'" and deleted=0','order'=>'number asc, id asc'));
            $count = count($other_times) + 1;
            $tag = new GroupsTime();
            $tag->time = $time;
            $tag->week_day = $week_day;
            $tag->groups_id = $groups_id;
            $tag->number = $count;
            $tag->save();
            echo CJSON::encode($tag->id);
        }
    }

    public function actionChangeTime($id,$time){
            $timetabel = GroupsTime::model()->findByPk($id);
            if ($timetabel){
                $timetabel->time = $time;
                $timetabel->save();
            }
    }

    public function actionDeleteTime($id){
            $timetabel = GroupsTime::model()->findByPk($id);
            if ($timetabel){
                $timetabel->deleted = 1;
                $timetabel->save();
            }
    }

        /*
     Просмотр работы
    */
    public function actionOrder($id)
    {
        $group = Groups::model()->findByPk($id);
        if ($group){
               $this->render('order', array(
                'group' => $group,
            ));
        }
    }

    public function actionOrderStatus($status=0,$id)
    {
        $model = GroupsOrders::model()->findByPk($id);
        //$model = GroupsOrders::model()->findByPk($id);
        if ($model){
            $model->status = $status;
            $model->save();
            Yii::app()->user->setFlash('success',
                    'Статус изменен'
            );
        }
        $this->redirect(array('order','id'=>$model->groups_id));
    }
}