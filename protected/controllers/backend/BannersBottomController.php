<?php

class BannersbottomController extends BackEndController
{
    public $razdel_parent = 'home';   
    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $banners = BannersBottom::model()->findAll(array('order'=>'position asc'));
        $this->render('index', array(
            'banners' => $banners,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        $this->saveFromPost($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $banners = BannersBottom::model()->findAll(array('order'=>'position asc'));
        $model = new BannersBottom();
        $model->position = count($banners)+1;
        $this->saveFromPost($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }

    private function saveFromPost(BannersBottom $model)
    {
        if (isset($_POST['BannersBottom'])) {
            $model->attributes = $_POST['BannersBottom'];

            $model->image = CUploadedFile::getInstance($model, 'image');

            $isNew = $model->isNewRecord;
            if ($model->save()) {
                if ($model->image) {
                    $file_name = uniqid().'_'.time() . '.' . $model->image->extensionName;
                    $path = Yii::getPathOfAlias('webroot').'/upload/banners/'.$file_name;
                    $model->image->saveAs($path);
                    $image = new SimpleImage($path);
                    //$image->thumbnail(BannersBottom::WIDTH, BannersBottom::HEIGHT);
                    $image->fit_to_width(BannersBottom::WIDTH);
                    $image->save($path, 80);

                    if ($model->filename != '' AND $file_name != ''){
                        $deleted_path = Yii::getPathOfAlias('webroot').'/upload/banners/'.$model->filename;
                        if (file_exists($deleted_path)) {
                            unlink($deleted_path);
                        }
                    }
                    $model->filename = $file_name;
                    $model->save();
                }
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Баннер добавлен' : 'Изменения сохранены'
                );
                $this->redirect(array('index'));
            }
        }
    }

    public function actionDelete($id)
    {
        $model = $this->getModel($id);
        $model->delete();
        $this->redirect(array('index'));
    }

    protected function getModel($id)
    {
        $model = BannersBottom::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}
