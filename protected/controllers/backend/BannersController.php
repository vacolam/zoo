<?php

class BannersController extends BackEndController
{

    public $razdel_parent = 'home';   

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }
    
    public function actionIndex()
    {
        $banners = Banner::model()->findAll();
        $this->render('index', array(
            'banners' => $banners,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        $this->saveFromPost($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }
    
    public function actionCreate()
    {
        $model = new Banner();
        $this->saveFromPost($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }
    
    private function saveFromPost(Banner $model)
    {
        if (isset($_POST['Banner'])) {
            $model->attributes = $_POST['Banner'];

            $model->image = CUploadedFile::getInstance($model, 'image');

            $isNew = $model->isNewRecord;
            if ($model->save()) {
                if ($model->image) {
                    $file_name = uniqid().'_'.time() . '.' . $model->image->extensionName;
                    $path = Yii::getPathOfAlias('webroot').'/upload/banners/'.$file_name;
                    $model->image->saveAs($path);
                    $image = new SimpleImage($path);
                    $image->thumbnail(Banner::WIDTH, Banner::HEIGHT);
                    $image->save($path, 80);

                    if ($model->filename != '' AND $file_name != ''){
                        $deleted_path = Yii::getPathOfAlias('webroot').'/upload/banners/'.$model->filename;
                        if (file_exists($deleted_path)) {
                            unlink($deleted_path);
                        }
                    }
                    $model->filename = $file_name;
                    $model->save();
                }
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Баннер добавлен' : 'Изменения сохранены'
                );
                $this->redirect(array('index'));
            }
        }
    }
    
    public function actionDelete($id)
    {
        $model = $this->getModel($id);
        $model->delete();
        $this->redirect(array('index'));
    }
    
    protected function getModel($id)
    {
        $model = Banner::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}