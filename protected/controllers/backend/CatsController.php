<?php

class CatsController extends BackEndController
{


    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index','update','preview','create','delete','filesadd','deletephotos','deletedocs','addtag','deletetag','setcover'),
                'roles'=>array('admin','manager'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }


    public function actionUpdate($id)
    {
        $model = $this->getModel($id);

        $this->saveFromPost($model);

        $this->render('form', array(
            'model' => $model,
        ));

    }


    
    public function actionCreate()
    {
        $model = new Cats();
        $cats = Cats::model()->findAll(array('condition'=>'cats_id = 0'));
        $model->number = count($cats) + 1;

        $this->saveFromPost($model);

        $this->render('form', array(
            'model' => $model,
        ));
    }

    private function saveFromPost(Cats $model)
    {
        $is_new =  $model->isNewRecord;
        if (isset($_POST['Cats'])) {
            $model->visible = 0;
            $model->attributes = $_POST['Cats'];
            $model->save();

            if ($is_new){
                $this->redirect(array('pages/index','id'=>$model->id));
            }
        }
    }
    
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->cats_id == 0 and $model->link == ''){
            $submenu = Cats::getSubmenu($model->id);
            foreach ($submenu as $sub){
                //������� �����, ����, ���
                $this->deleteSub($sub->id);
            }
        }

        $model->delete();
        
        $this->redirect(array('dashboard/index'));
    }
    
    protected function getModel($id)
    {
        $model = Cats::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }


    protected function deleteSub($id)
    {
        $model = $this->getModel($id);
        $cat_id = $model->cats_id;
        $photos = $model->photos;
        if ($photos)
        {
            foreach($photos as $photo_model){
                Photos::deleteImage($photo_model);
            }
        }

        $docs = $model->docs;
        if ($docs)
        {
            foreach($docs as $doc_model){
                Docs::deleteDoc($doc_model);
            }
        }

        if ($model->filename != '')
        {
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/pages/'.$size.$model->filename;

                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                            }
                        }
        }

        if ($model->link == ''){
        $model->delete();
        }
    }



}