<?php

class ConfController extends BackEndController
{

    public $typeList = array(
            0 => 'Выбрать тип',
            'conf' => 'Конференция',
            'festival' => 'Фестиваль',
            'konkurs' => 'Конкурс',
            'opros' => 'Опрос',
        );

    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index','orders','jury','confupdate','confcreate','confdelete','confview','nomcreate','nomupdate','nomdelete','nomaddjury','nomdeletejury','work','workmark','jurylist','juryupdate','jurypass','jurydelete','jurycreate','juryaddnom','jurydeletenom','chatsend','filesadd','deletephotos','deletedocs','oproscreate','oprosupdate','oprosdelete',),
                'roles'=>array('admin','manager'),
            ),
            array('allow',
                'actions'=>array('jury','confview','work','workmark','chatsend'),
                'roles'=>array('user'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    protected function beforeAction($action)
    {
        parent::beforeAction($action);
        //$this->typeList
        return true;
    }

    /*
    Список конференций
    */
    public function actionIndex($type = '0')
    {
        $type_sql = array();
        if ($type != '0'){
            $type_sql[] = 'type="'.$type.'"';
        }

        $plugins = User::getAllowedPlugins();
        if(count($plugins)>0){
            $type_sql[] = 'type IN ("'.implode('","',$plugins).'")';
        }

        $sql = '';
        if(count($type_sql)>0){
        $sql = implode(' and ',$type_sql);
        }

        $dataProvider = new CActiveDataProvider('Conf', array(
            'criteria' => array(
                'condition'=>$sql,
                'order' => 'date DESC, id DESC',
            ),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));

        $this->render('index', array(
            'provider' => $dataProvider,
            'typeList' => $this->typeList,
        ));
    }

    /*
    Список всех заявок
    */
    public function actionOrders($type = '0',$city='',$school='',$sort='')
    {
        $type_sql = '';
        $search = array();
        if ($type != '0'){
            $search[] = 'type="'.$type.'"';
        }

        if ($city != ''){
            $search[] = 'city like "%'.$city.'%"';
        }
        if ($school != ''){
            $search[]  = 'shcool like "%'.$school.'%"';
        }

        $plugins = User::getAllowedPlugins();
        if(count($plugins)>0){
           $rr = 'type IN ("'.implode('","',$plugins).'")';
           $allTypes_confs = Conf::model()->findAll(array('condition'=>$rr));
           $allowed_conf_array = array();
           if ($allTypes_confs)
           {
                foreach($allTypes_confs as $c)
                {
                $allowed_conf_array[] = $c->id;
                }
           }

           if (count($allowed_conf_array)>0){
            $search[] = 'conf_id IN ('.implode(',',$allowed_conf_array).')';
           }else{
            $search[] = 'conf_id=0';
            }
        }else{
            $search[] = 'conf_id=0';
        }

        $searc_sql = '';
        if (count($search) > 0){
            $searc_sql = implode(' and ',$search);
        }

        $sort_sql = 'date DESC, id DESC';
        if ($sort == 'city_down'    ){$sort_sql = 'city ASC, date DESC, id DESC';}
        if ($sort == 'city_up'      ){$sort_sql = 'city DESC, date DESC, id DESC';}
        if ($sort == 'school_down'  ){$sort_sql = 'shcool ASC, date DESC, id DESC';}
        if ($sort == 'school_up'    ){$sort_sql = 'shcool DESC, date DESC, id DESC';}

        $dataProvider = new CActiveDataProvider('ConfWorks', array(
            'criteria' => array(
                'condition'=>$searc_sql,
                'order' => $sort_sql,
            ),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));

        $this->render('orders', array(
            'provider' => $dataProvider,
            'typeList' => $this->typeList,
            'city'     => $city,
            'school'   => $school,
            'sort'     => $sort,
        ));
    }

    public function actionJury()
    {
        $noms = Yii::app()->user->model->nominations;
        $confs_ids = array();
        if ($noms){
            foreach($noms as $nom)
            {
            $confs_ids[] = $nom->conf_id;
            }
        }

        if (count($confs_ids) == 0){$confs_ids[] = 0;}

        $dataProvider = new CActiveDataProvider('Conf', array(
            'criteria' => array(
                'condition'=>'id IN ('.implode(',',$confs_ids).')',
                'order' => 'date DESC, id DESC',
            ),
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));

        $this->render('jury', array(
            'provider' => $dataProvider,
        ));
    }

    /*
    =========================================================
    УПРАВЛЕНИЕ КОНФЕРЕНЦИЯМИ
    ======================
    */


    /*
     ПРОСМОТР КОНФЫ
    */
    public function actionConfView($id, $nomination_filter = '0', $city='', $school='', $sort='')
    {
        $model = $this->getConf($id);

        if ($model->type != 'opros')
        {
            if (Yii::app()->user->role != 'user')
            {
                $nominations = $model->nominations;
            }else{
                $noms = ConfNomJury::model()->findAll(array('condition'=>'user_id="'.Yii::app()->user->id.'" and conf_id="'.$id.'"'));

                $noms_ids = array();
                if ($noms)
                {
                    foreach($noms as $nom)
                    {
                    $noms_ids[] = $nom->nom_id;
                    }
                }

                if (count($noms_ids) == 0){$noms_ids[] = 0;}
                $nominations = ConfNominations::model()->findAll(array('condition'=>'id IN('.implode(',',$noms_ids).')'));
            }

            $works_array = array();
            if ($nominations){
                foreach($nominations as $nom){
                    $works = $nom->works;
                    if ($works){
                        foreach ($works as $work){
                            $works_array[] = $work->id;
                        }
                    }

                }
            }
            if (count($works_array) == 0){$works_array[] = 0;}

            $your_marks = array();
            $marks_array = ConfWorksMarks::model()->findAll(array('condition'=>'user_id="'.Yii::app()->user->id.'" AND work_id IN ('.implode(',',$works_array).')'));
            if ($marks_array){
                foreach($marks_array as $mark){
                    $your_marks[$mark->work_id] = $mark->mark;
                }
            }



        ///заявки
            $type_sql = '';
            $search = array();
            $search[] = 'conf_id="'.$id.'"';
            if ($nomination_filter != '0'){
                $search[] = 'nom_id="'.$nomination_filter.'"';
            }

            if ($city != ''){
                $search[] = 'city like "%'.$city.'%"';
            }
            if ($school != ''){
                $search[]  = 'shcool like "%'.$school.'%"';
            }


            $searc_sql = '';
            if (count($search) > 0){
                $searc_sql = implode(' and ',$search);
            }

            $sort_sql = 'date DESC, id DESC';
            if ($sort == 'city_down'    ){$sort_sql = 'city ASC, date DESC, id DESC';}
            if ($sort == 'city_up'      ){$sort_sql = 'city DESC, date DESC, id DESC';}
            if ($sort == 'school_down'  ){$sort_sql = 'shcool ASC, date DESC, id DESC';}
            if ($sort == 'school_up'    ){$sort_sql = 'shcool DESC, date DESC, id DESC';}

            $dataProvider = new CActiveDataProvider('ConfWorks', array(
                'criteria' => array(
                    'condition'=>$searc_sql,
                    'order' => $sort_sql,
                ),
                'pagination' => array(
                    'pageSize' => 1000,
                ),
            ));

            $this->render('conf_view', array(
                'model'         => $model,
                'nominations'   => $nominations,
                'your_marks'    => $your_marks,
                'provider'      => $dataProvider,
                'city'          => $city,
                'school'        => $school,
                'nomination_filter'          => $nomination_filter,
                'sort'          => $sort,
            ));
        }else{

            $nominations = $model->nominations;
            $this->render('opros/conf_view', array(
                    'model' => $model,
                    'nominations' => $nominations,
            ));
        }
    }

    /*
     РЕДАКТИРОВАНИЕ КОНФЫ
    */
    public function actionConfUpdate($id)
    {
        $model = $this->getConf($id);
        $this->saveConf($model);
        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "conf" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "conf" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));


        $this->render('conf_form', array(
            'model' => $model,
            'photos' => $photos,
            'docs' => $docs,
            'typeList' => $this->typeList,
        ));
    }

    /*
     СОЗДАНИЕ КОНФЫ
    */
    public function actionConfCreate($type='conf')
    {
        $model = new Conf();
        $model->type = $type;
        $model->date = date('Y-m-d');
        $model->form_is_open = 1;
        $this->saveConf($model);


        $this->render('conf_form', array(
            'model' => $model,
            'typeList' => $this->typeList,
        ));
    }

    /*
     СОХРАНЕНИЕ КОНФЫ
    */
    private function saveConf(Conf $model, $action = 'update')
    {
        if (isset($_POST['Conf'])) {
            $isNew =  $model->isNewRecord;
            $model->form_is_open = 0;
            $model->golosovanie_is_closed = 0;
            $model->attributes = $_POST['Conf'];

            if ($model->short_text == ''){
               $model->short_text = trim(strip_tags($this->crop_str_word($model->text, 30 )));
            }
           	$date = new DateTime($_POST['Conf']['date']);
			$model->date = $date->format('Y-m-d');
            $model->save();

            //Для новых опросов создаем номинацию
            if ($model->type == 'opros' and $isNew)
            {
                $nom = new ConfNominations();
                $nom->title = 'Номинация для опроса';
                $nom->conf_id = $model->id;
                $nom->save();
            }

            if (!$model->hasErrors())
            {
                if (Yii::app()->controller->action->id == 'confcreate'){
                    if ($model->type != 'summer')
                    {
                        $this->redirect(array('conf/confview','id'=>$model->id));
                    }else{
                        $this->redirect(array('conf/confupdate','id'=>$model->id));
                    }
                }
            }
        }
    }

    /*
     УДАЛЕНИЕ КОНФЫ
    */
    public function actionConfDelete($id)
    {

        $model = $this->getConf($id);
        $nominations = $model->nominations;

        if ($nominations)
        {
            foreach($nominations as $nomination)
            {
                $nomination->delete();
            }
        }

        $photos = $model->photos;
        if ($photos)
        {
            foreach($photos as $photo_model){
                Photos::deleteImage($photo_model);
            }
        }


        $docs = $model->docs;
        if ($docs)
        {
            foreach($docs as $doc_model){
                Docs::deleteDoc($doc_model);
            }
        }

        if ($model->type == 'opros')
        {
            if ($model->opros)
            {
                foreach($model->opros as $question)
                {
                    Opros::deleteQuestion($question);
                }
            }
        }

        $model->delete();
        $this->redirect(array('index'));
    }

    /*
     ПОЛУЧЕНИЕ КОНФЫ
    */
    protected function getConf($id)
    {
        $model = Conf::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }





    /*
    ==================================================================
    УПРАВЛЕНИЕ НОМИНАЦИЯМИ
    ==================
    */

    /*
     РЕДАКТИРОВАНИЕ НОМИНАЦИИ
    */
    public function actionNomUpdate($id)
    {
        $model = $this->getNom($id);
        $conf = $this->getConf($model->conf_id);
        $this->saveNom($model,$model->conf_id);

        $users_list = User::model()->findAll(array('condition'=>'role="user"'));

        $this->render('nom_form', array(
            'model' => $model,
            'conf' => $conf,
            'users_list' => $users_list,
        ));
    }

    /*
     СОЗДАНИЕ НОМИНАЦИИ
    */
    public function actionNomCreate($conf_id)
    {
        $conf = $this->getConf($conf_id);
        $model = new ConfNominations();
        $this->saveNom($model,$conf_id);
        $this->render('nom_form', array(
            'model' => $model,
            'conf' => $conf,
        ));
    }

    /*
     СОХРАНЕНИЕ НОМИНАЦИИ
    */
    private function saveNom(ConfNominations $model, $conf_id)
    {
        if (isset($_POST['ConfNominations'])) {
            $model->attributes = $_POST['ConfNominations'];
            $model->conf_id = $conf_id;
            if ($model->save())
            {
                $this->redirect(array('conf/confview','id'=>$conf_id));
            }
        }
    }

    /*
     УДАЛЕНИЕ НОМИНАЦИИ
    */
    public function actionNomDelete($id)
    {

        $model = $this->getNom($id);
        /*
        $nominations = $model->nominations;
        if ($nominations)
        {
            foreach($nominations as $nomination)
            {
                $nomination->delete();
            }
        }
        */
        $conf_id = $model->conf_id;
        $model->delete();
        $this->redirect(array('conf/confview','id'=>$conf_id));
    }

    /*
     ПОЛУЧЕНИЕ НОМИНАЦИИ
    */
    protected function getNom($id)
    {
        $model = ConfNominations::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }


    /*
     ДОБАВЛЕНИЕ ЖЮРИ В НОМИНАЦИИ
    */
     public function actionNomAddJury($nom_id,$user_id){
        $nom = ConfNominations::model()->findByPk($nom_id);
        $user = User::model()->findByPk($user_id);
        if ($user and $nom)
        {
            $ConfNomJury = new ConfNomJury();
            $ConfNomJury->user_id = $user_id;
            $ConfNomJury->nom_id = $nom_id;
            $ConfNomJury->conf_id = $nom->conf_id;
            $ConfNomJury->save();
            echo CJSON::encode($user_id);
        }
    }


    /*
    ==================================================================
    УПРАВЛЕНИЕ РАБОТАМИ
    ==================
    */

    /*
     Просмотр работы
    */
    public function actionWork($id)
    {
        $work = $this->getWork($id);
        $nom = ConfNominations::model()->findByPk($work->nom_id);
        $conf = $this->getConf($work->conf_id);
        $marks = $work->marks;
        $marks_array = array();
        if ($marks){
            foreach($marks as $mark){
                $marks_array[$mark->user_id] = array(
                    'date'=>$mark->date,
                    'mark'=>$mark->mark,
                );
            }
        }

        $your_mark = ConfWorksMarks::model()->findAll(array('condition'=>'work_id="'.$id.'" and user_id="'.Yii::app()->user->id.'"'));
        if (isset($your_mark[0])){
            $your_mark = $your_mark[0];
        }

        if ($conf->type == 'konkurs'){
            $this->render('konkurs/work', array(
                'work' => $work,
                'conf' => $conf,
                'nom' => $nom,
                'marks' => $marks,
            ));
        }
        elseif ($conf->type == 'conf'){
            $this->render('conf/work', array(
                'work' => $work,
                'conf' => $conf,
                'nom' => $nom,
                'marks' => $marks,
            ));
        }
        else{
            $this->render('work', array(
                'work' => $work,
                'conf' => $conf,
                'nom' => $nom,
                'marks_array' => $marks_array,
                'your_mark' => $your_mark,
            ));
        }
    }


    

    public function actionWorkMark($id,$mark)
    {
        $work = ConfWorks::model()->findByPk($id);
        if ($work)
        {
            if (User::userNomPermission($work->nom_id,Yii::app()->user->id))
            {
                    //1.удаляем все отметки этого пользователя и ставим новые
                    $query = 'DELETE FROM {{conf_works_marks}} where user_id = "'.Yii::app()->user->id.'" AND work_id="'.$id.'"';
                    Yii::app()->db->createCommand($query)->execute();
                    //2.сохраняем новое значение
                    $newmark = new ConfWorksMarks();
                    $newmark->user_id  = Yii::app()->user->id;
                    $newmark->work_id  = $id;
                    $newmark->mark     = $mark;
                    $newmark->date     = new CDbExpression('NOW()');
                    $newmark->save();

                    $allMarks = ConfWorksMarks::model()->findAll(array('condition'=>'work_id="'.$id.'"'));
                    $summ = 0;
                    $t = 0;
                    if($allMarks){
                        foreach($allMarks as $item)
                        {
                            //3.считаем сумму всех отметок. обновляем
                            $summ = $summ + $item->mark;

                            //4.считаем количество голосов. обновляем
                            $t = $t + 1;
                        }
                    }

                    //5.Обновляем
                    $work->mark = $summ;
                    $work->marks_count = $t;
                    $work->save();
            }
            $this->redirect(array('conf/work','id'=>$id));
        }
    }

    protected function getWork($id)
    {
        $model = ConfWorks::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }


/*
    =========================================================
    УПРАВЛЕНИЕ СПИСКОМ ЖЮРИ
    ======================
    */


    /*
     ПРОСМОТР СПИСКА ЖЮРИ
    */
    public function actionJuryList()
    {
        $jury_list = User::model()->findAll(array('condition'=>'role="user"'));
        $nominations = ConfNominations::model()->findAll(array('condition'=>''));
        $nominations_list = array();
        if ($nominations){
            foreach($nominations as $nomination)
            {
                $nominations_list[$nomination->id] = $nomination->title;
            }
        }



        $this->render('jury_list', array(
            'jury_list' => $jury_list,
            'nominations_list' => $nominations_list,
        ));
    }

    /*
     РЕДАКТИРОВАНИЕ ЖЮРИ
    */
     /*
     ПРОСМОТР СПИСКА ЖЮРИ
    */
    public function actionJuryUpdate($id = 'new')
    {
        if ((int)$id) $users = User::model()->findByPk((int)$id);
        if (!isset($users) || !$users) $users = new User;

        if (isset($_POST['User'])) {
            if ($_POST['User']['name'] != '' && $_POST['User']['email'] != '') {
                $users->attributes = $_POST['User'];
                if ($users->validate()) {

                $emailCopy = User::model()->count(array(
                'condition'	=>	'id != "'.(int)$id.'" AND email="'.$_POST['User']['email'].'"',
                ));

                if ($emailCopy > 0){
                 Yii::app()->user->setFlash('userUpdateError', 'Такой электронный адрес уже используется');
                }else{
                    if ($users->save()){
                        Yii::app()->user->setFlash('userUpdateSuccess', 'Данные успешно изменены');
                        $this->redirect(array('conf/juryupdate','id'=>$users->id));
                    }else{
                        Yii::app()->user->setFlash('userUpdateError', 'Изменения не сохранились');
                    }
                }
                }else{
                    Yii::app()->user->setFlash('userUpdateError', 'Проверьте формат e-mail и заполненность других полей');
                }
            }
            else{
                    Yii::app()->user->setFlash('userUpdateError', 'Поля Имя и E-mail должны быть заполнены');
                }
        }

        $nom_list = array();

        $confs = Conf::model()->findAll();
        if ($confs){
            foreach($confs as $conf){
                $noms = $conf->nominations;
                if ($noms)
                {
                    foreach($noms as $nom)
                    {
                        $nom_list[$nom->id] = $conf->title.":: ".$nom->title;
                    }
                }
            }
        }

        $this->render('jury_form', array(
            'users'			=>	$users,
            'nom_list'			=>	$nom_list,
        ));
    }

     /**
     * изменение пароля у просматриваемого сотрудника
     */
    public function actionJuryPass($id)
    {
    if ((int)$id) $users = User::model()->findByPk((int)$id);
        if (isset($_POST['User'])) {
            if ($_POST['User']['password'] != ''){
                    $users->attributes = $_POST['User'];
                    if ($users->validate()) {
                        if ($users->password){
                           $users->password = CPasswordHelper::hashPassword(Salt::password($users->password));
                        }
                        if($users->save()){
                            Yii::app()->user->setFlash('passSuccess', 'Новый пароль "'.$_POST['User']['password'].'" сохранен');
                        }

                    }
            }else{
                Yii::app()->user->setFlash('passError', 'Пароль не может быть пустым');
            }
        }

        $this->redirect(array('conf/juryupdate','id'=>$users->id));
    }


    public function actionJuryCreate()
    {
        $users = new User();

        if (isset($_POST['User'])) {
        	if ($_POST['User']['name'] != '' && $_POST['User']['email'] != '' && $_POST['User']['password'] != '' ){
                	$user = new User;
                    $user->attributes = $_POST['User'];
                    $user->role = 'user';
                    if ($user->validate()) {

                        $emailCopy = User::model()->count(array(
                        'condition'	=>	'email="'.$_POST['User']['email'].'"',
                        ));

                        if ($emailCopy > 0){
                           Yii::app()->user->setFlash('newUserError', 'Такой электронный адрес уже используется');
                        }else{
                                if ($user->password){
                                   $user->password = CPasswordHelper::hashPassword(Salt::password($user->password));
                                }
                                if ($user->save()){
                                    Yii::app()->user->setFlash('success', 'Пользователь "'.$user->name.'" успешно создан');
                                    $this->redirect(array('juryupdate','id'=>$user->id));
                                }
                        }
                    }else{
                        Yii::app()->user->setFlash('newUserError', 'Проверьте формат E-mail и заполненность других полей');
                    }
            }else{
                Yii::app()->user->setFlash('newUserError', 'Поля "Имя", "E-mail", "Пароль" должны быть заполнены');
            }
        }

        $this->render('jury_form', array(
             'users'			=>	$users,
        ));
    }

    public function actionJuryAddNom($id,$user_id){
        $nom = ConfNominations::model()->findByPk($id);
        if ($nom)
        {
            $ConfNomJury = new ConfNomJury();
            $ConfNomJury->user_id = $user_id;
            $ConfNomJury->nom_id = $id;
            $ConfNomJury->conf_id = $nom->conf_id;
            $ConfNomJury->save();
            echo CJSON::encode($id);
        }
    }

    public function actionJuryDeleteNom($nom_id,$user_id){
        if ((int)$nom_id != 0)
        {
                $query = 'DELETE FROM {{conf_nom_jury}} where user_id = "'.$user_id.'" AND nom_id="'.$nom_id.'"';
                Yii::app()->db->createCommand($query)->execute();
        }
    }


    /*-------------------------------------------------------------------*/

    public function actionChatsend()
    {
        $response = array(
           'result' => false,
           'text' => '',
        );

        $new_msg = ConfChat::msgsNew();
        $response['result'] = true;
        $response['text'] = $this->renderPartial('_chat_item',array(
            'date'					=>  date('Y-m-d'),
            'user_photo'			=>	'',
            'user_name'				=>	Yii::app()->user->name,
            'user_id'				=>	Yii::app()->user->id,
            'text'					=>	$new_msg->text,
            'creator_id'			=>	$new_msg->user_id,
            'receiver_id'			=>	'',
            'watched_by_receiver'	=>	0,
        ),true);


       	echo CJSON::encode($response);
    }


/*
     СОХРАНЯЕМ ФОТКИ
    */
    public function actionFilesAdd($post_id, $type='conf')
    {
        $type_save = 'conf';
        if ($type == 'conf'){$type_save = 'conf';}

        $post_item = Conf::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $news_photos = new Photos;
                $news_photos->post_id = $post_id;
                $news_photos->date = new CDbExpression('NOW()');
                $news_photos->filename = $uploaded_file;
                $news_photos->type = $type_save;
                $news_photos->save();

                if ($post_item->cover_id == 0 || $post_item->cover_id == '')
                {
                    $post_item->cover_id = $news_photos->id;
                    $post_item->save();
                }
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $news_docs = new Docs;
                $news_docs->post_id = $post_id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $uploaded_file;
                $news_docs->filetitle = $file->getName();
                $news_docs->type = $type_save;
                $news_docs->save();
            }
        }
        }
    }

    public function actionDeletePhotos($id){
        $result = false;
        $photo = Photos::model()->findByPk($id);
        if ($photo)
        {
            $post_id = $photo->post_id;
            Photos::deleteImage($photo);

            /*
            $post = News::model()->findByPk($post_id);
            if ($post){
                if ($post->cover_id == $id){
                    $post->cover_id = 0;
                    $post->save();

                    $result = News::autoCover($post);
                }
            }
            */
        }
        echo CJSON::encode($result);
    }

    public function actionDeleteDocs($id){
        $model = Docs::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Docs::deleteDoc($model);
        }
    }

/*
    ==================================================================
    УПРАВЛЕНИЕ ОПРОСАМИ
    ==================
    */

    /*
     РЕДАКТИРОВАНИЕ ОПРОСА
    */
    public function actionOprosUpdate($id)
    {
        $model = $this->getOpros($id);
        $conf = $this->getConf($model->conf_id);
        $this->saveOpros($model,$model->conf_id);

        $this->render('opros/opros_form', array(
            'model' => $model,
            'conf' => $conf,
        ));
    }

    /*
     СОЗДАНИЕ ОПРОСА
    */
    public function actionOprosCreate($conf_id)
    {
        $conf = $this->getConf($conf_id);
        $model = new Opros();
        $count = count($conf->opros) + 1;
        $model->number =  $count;
        $this->saveOpros($model,$conf_id);
        $this->render('opros/opros_form', array(
            'model' => $model,
            'conf' => $conf,
        ));
    }

    /*
     СОХРАНЕНИЕ ОПРОСА
    */
    private function saveOpros(Opros $model, $conf_id)
    {
        if (isset($_POST['Opros'])) {
            $model->attributes = $_POST['Opros'];
            $model->conf_id = $conf_id;

            $model->image = CUploadedFile::getInstance($model, 'image');
            $isNew = $model->isNewRecord;
            if ($model->save()) {
                if ($model->image) {
                    $file_name = uniqid().'_'.time() . '.' . $model->image->extensionName;
                    $path = Yii::getPathOfAlias('webroot').'/upload/opros/'.$file_name;
                    $model->image->saveAs($path);
                    $image = new SimpleImage($path);
                    $image->thumbnail(Opros::WIDTH, Opros::HEIGHT);
                    $image->save($path, 80);

                    if ($model->filename != '' AND $file_name != ''){
                        $deleted_path = Yii::getPathOfAlias('webroot').'/upload/opros/'.$model->filename;
                        if (file_exists($deleted_path)) {
                            unlink($deleted_path);
                        }
                    }
                    $model->filename = $file_name;
                    $model->save();
                }
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Опрос добавлен' : 'Изменения сохранены'
                );
                $this->redirect(array('conf/confview','id'=>$conf_id));
            }
        }
    }

    /*
     УДАЛЕНИЕ ОПРОСА
    */
    public function actionOprosDelete($id)
    {

        $model = $this->getOpros($id);
        $conf_id = $model->conf_id;

        Opros::deleteQuestion($model);


        $model->delete();
        $this->redirect(array('conf/confview','id'=>$conf_id));
    }

    /*
     ПОЛУЧЕНИЕ ОПРОСА
    */
    protected function getOpros($id)
    {
        $model = Opros::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

}
