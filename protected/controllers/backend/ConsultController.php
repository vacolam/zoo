<?php
Yii::import('application.controllers.backend.PageController');



/**
 * Управление разделом "Консультации"
 */
class ConsultController extends PageController
{


    public $razdel_id = 65;

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }

    protected function getPageId()
    {
        return 'consult';
    }

    public function actionIndex()
    {
        $consult = $this->getModel('consult');

        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "consult"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "consult"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

        $this->render('index', array(
            'consult' => $consult,
            'photos' => $photos,
            'docs' => $docs,
        ));

    }


    public function actionSave()
    {
        $consult = $this->getModel('consult');
        if (isset($_POST['Page']))
        {
            $consult->content = $_POST['Page']['content'];
            $consult->save();
        }

        Yii::app()->user->setFlash('success', "Изменения сохранены");
        $this->redirect(array('consult/index'));
    }



    public function actionFilesAdd()
    {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $Cats_photos = new Photos;
                $Cats_photos->post_id = 0;
                $Cats_photos->date = new CDbExpression('NOW()');
                $Cats_photos->filename = $uploaded_file;
                $Cats_photos->type = 'consult';
                $Cats_photos->save();
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $Cats_docs = new Docs;
                $Cats_docs->post_id = 0;
                $Cats_docs->date = new CDbExpression('NOW()');
                $Cats_docs->filename = $uploaded_file;
                $Cats_docs->filetitle = $file->getName();
                $Cats_docs->type = 'consult';
                $Cats_docs->save();
            }
        }
    }

    public function actionDeletePhotos($id){
        $model = Photos::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Photos::deleteImage($model);
            //$this->redirect(array('update','id'=>$post_id));
        }
    }
    public function actionDeleteDocs($id){
        $model = Docs::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Docs::deleteDoc($model);
            //$this->redirect(array('update','id'=>$post_id));
        }
    }

}
