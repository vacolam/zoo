<?php
Yii::import('application.controllers.backend.PageController');



/**
 * Управление разделом "Контакты"
 */
class ContactsController extends PageController
{


    public $razdel_id = 7;

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }

    protected function getPageId()
    {
        return 'contacts';
    }

    public function actionIndex()
    {
        /*
        if (isset($_POST['COMPANY'])){
            echo 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzsssssssszzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz<br>';
            foreach($_POST['COMPANY'] as $index => $value)
            {
                $t = $this->getModel($index);
                if ($t)
                {
                    $t->content = $value;
                }
                $t->save();
            }
        }

        if (isset($_POST['COMPANY_ENG'])){
            foreach($_POST['COMPANY_ENG'] as $index => $value)
            {
                $t = $this->getModel($index);
                if ($t)
                {
                    $t->content_eng = $value;
                }
                $t->save();
            }
        }




        echo 'ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd<br>';
         if (isset($_POST['Page'])) {
            echo 'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy<br>';
            $contacts->attributes = $_POST['Page'];
            if ($contacts->save()) {
                $map->content = $_POST['Map'];
                $map->save();
                //Yii::app()->user->setFlash('success', "Изменения сохранены");
                //$this->redirect(array('contacts/index'));
            }
        }
        */

        $map = $this->getModel('map');
        $contacts = $this->getModel('contacts');
        $contacts_table = $this->getModel('contacts_ta');
        $company_contacts = Page::getContatcs();
        $company_contacts_eng = Page::getEngContatcs();



        $this->render('index', array(
            'map' => $map,
            'company_contacts' => $company_contacts,
            'company_contacts_eng' => $company_contacts_eng,
            'contacts' => $contacts,
            'contacts_table' => $contacts_table,
        ));
    }



    public function actionSave()
    {
        if (isset($_POST['COMPANY'])){
            foreach($_POST['COMPANY'] as $index => $value)
            {
                $t = $this->getModel($index);
                if ($t)
                {
                    $t->content = $value;
                }
                $t->save();
            }
        }

        if (isset($_POST['COMPANY_ENG'])){
            foreach($_POST['COMPANY_ENG'] as $index => $value)
            {
                $t = $this->getModel($index);
                if ($t)
                {
                    $t->content_eng = $value;
                }
                $t->save();
            }
        }


        $map = $this->getModel('map');
        $contacts = $this->getModel('contacts');
        $contacts_table = $this->getModel('contacts_ta');
        $company_contacts = Page::getContatcs();
        $company_contacts_eng = Page::getEngContatcs();

         if (isset($_POST['Page'])) {
            $contacts->content = $_POST['Page']['content'];
            if ($contacts->save()) {
                $map->content = $_POST['Map'];
                $map->save();

            }
        }




        Yii::app()->user->setFlash('success', "Изменения сохранены");
        $this->redirect(array('contacts/index'));
    }
}
