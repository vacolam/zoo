<?php

class ConvertdataController extends BackEndController
{
    
    public function actionText()
    {
        //Переносим новости в новости
        $query = "SELECT * FROM {{b_iblock_element}} where IBLOCK_ID = 9 ORDER BY ID ASC";
        $params = array();
        $zoo_news = Yii::app()->db->createCommand($query)->query($params)->readAll();


        foreach ($zoo_news as $index => $item)
        {
            echo $item['ID']." - ".$item['NAME']."<br>";
           $news = new News();
           $news->id            = $item['ID'];
           $news->date          = $item['TIMESTAMP_X'];
           $news->title         = $item['NAME'];
           $news->text          = $item['DETAIL_TEXT'];
           $news->pre_text      = $item['DETAIL_TEXT'];
           $news->short_text    = $item['PREVIEW_TEXT'];
           $news->cover_id      = $item['PREVIEW_PICTURE'];
           $news->save();
        }
    }

    public function actionCovers()
    {
        //Переносим обложки в файлы
        $query = "SELECT * FROM {{exposure_item}} ORDER BY ID ASC";
        $params = array();
        $news = Yii::app()->db->createCommand($query)->query($params)->readAll();


        foreach ($news as $index => $item)
        {
            $query = "SELECT * FROM {{b_file}} where ID='".$item['cover_id']."'";
            $params = array();
            $photo = Yii::app()->db->createCommand($query)->query($params)->readAll();

            if (isset($photo[0])){
                echo $photo[0]['ID']."<br>";

               echo $item['id']." - ".$item['title']." - ".$item['cover_id']."<br>";
               $news = new Photos();
               $news->id            = $photo[0]['ID'];
               $news->type          = 'exposure_item';
               $news->post_id       = $item['id'];
               $news->filename      = $photo[0]['FILE_NAME'];
               $news->subdir        = $photo[0]['SUBDIR'];
               $news->save();

           }
        }
    }

    public function actionFiles()
    {
        //Переносим файлы новостей
        $query = "SELECT * FROM {{b_iblock_element_prop_m10}} ORDER BY ID ASC";
        $params = array();
        $data = Yii::app()->db->createCommand($query)->query($params)->readAll();

        $image_types = array('image/jpeg','image/png','image/bmp','image/gif');

        foreach ($data as $index => $item)
        {
            $query = "SELECT * FROM {{b_file}} where ID='".$item['VALUE']."'";
            $params = array();
            $file = Yii::app()->db->createCommand($query)->query($params)->readAll();

            if (isset($file[0])){

                echo $file[0]['ID']."<br>";

                if (in_array($file[0]['CONTENT_TYPE'],$image_types)){
                    $file_model = new Photos();
                }else{
                    $file_model = new Docs();
                }
               $file_model->id            = $file[0]['ID'];
               $file_model->type          = 'exposure_item';
               $file_model->post_id       = $item['IBLOCK_ELEMENT_ID'];
               $file_model->filename      = $file[0]['FILE_NAME'];
               $file_model->subdir        = $file[0]['SUBDIR'];
               $file_model->save();
           }
        }
    }

    public function actionSaveFilesToServer()
    {

        $photo = Photos::model()->findAll(array('condition'=>'parsed_from_server=0','order'=>'id desc','limit'=>1));

        if (isset($photo[0]))
        {
                echo $photo[0]->id." - ".$photo[0]->post_id."<br>";

                $path = Yii::getPathOfAlias('webroot').'/upload/photos_exposure/';
                $newFile = $path.$photo[0]->filename;
                $newFile_small = $path.'s_'.$photo[0]->filename;

                $zoo_server_img = 'http://sakhalinzoo.ru/upload/'.$photo[0]->subdir."/".$photo[0]->filename;

                echo $zoo_server_img."<br>";

                // Проверяем есть ли файл на сайте
                $Headers = @get_headers($zoo_server_img);

                echo "headers";
                print_r($Headers);
                // проверяем ли ответ от сервера с кодом 200 - ОК

                $file_exists = false;
                if(preg_match("|200|", $Headers[0])) {
                    $file_exists = true;
                }

                $copy_big = false;
                if ($file_exists == true)
                {
                    echo "true";
                    $copy_big = copy ( $zoo_server_img , $newFile );
                }else{
                    echo "false";
                    $photo[0]->deleted = 1;
                    $photo[0]->parsed_from_server = 1;
                    $photo[0]->save();
                }

                if ($copy_big == true AND $file_exists==true)
                {
                    echo "<br>lets make small copy";
                    copy ( $newFile , $newFile_small );

                    foreach (array('normal','small') as $size)
            		{

                        if ($size == 'small'){$newFile = $newFile_small;}

                        if (file_exists($newFile))
                        {
            				chmod($newFile, 0644);

            				try
            				{
            				    echo "try";
            				    $img = new SimpleImage($newFile);


                                //ресайзим
            					switch ($size)
            					{
            						case 'normal':
                                        //$img->best_fit(800, 600);
            							break;
            						case 'small':
                                        $img->thumbnail(200, 200);
            							break;
            					}
                  				$img->save();

            				}
            				catch (Exception $e)
            				{
            				    //echo 'error'.$e;
            					@unlink($newFile);
            					$success = false;
            				}

            			}
                    }
                    $photo[0]->parsed_from_server = 1;
                    $photo[0]->save();
                }
        }
        $query = "SELECT COUNT( id ) AS count_id FROM `photos` WHERE `parsed_from_server` =0";
        $params = array();
        $count = Yii::app()->db->createCommand($query)->query($params)->readAll();

        echo "<br><br><br>Осталось еще:".$count[0]['count_id']."<br>";


        $this->render('index', array(
            'photo' => $photo,
        ));
    }
}