<?php

class CropImageController extends BackEndController
{

    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('crop'),
                'roles'=>array('admin','manager'),
            ),
            array('allow',
                'actions'=>array(),
                'roles'=>array('user'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    protected function beforeAction($action)
    {
        parent::beforeAction($action);
        //$this->typeList
        return true;
    }


    public function actionCrop()
    {
        $crop = new CropAvatar(
          isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
          isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
          isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
        );

        $response = array(
          'state'  => 200,
          'message' => $crop -> getMsg(),
          'result' => $crop -> getResult()
        );

        echo json_encode($response);
    }

}
