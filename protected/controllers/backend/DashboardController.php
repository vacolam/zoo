<?php
class DashboardController extends BackEndController
{
    public function actionIndex($type = 'none')
    {
        if (Yii::app()->user->role == 'user'){
            $this->redirect(array('conf/jury'));
        }else{


        $type_sql = array();
        if ($type != 'none'){
            $type_sql[] = 'type="'.$type.'"';
        }

        $plugins = User::getAllowedPlugins();
        if(count($plugins)>0){
            $type_sql[] = 'type IN ("'.implode('","',$plugins).'")';
        }

        $sql = '';
        if(count($type_sql)>0){
        $sql = implode(' and ',$type_sql);
        }

        $dataProvider = new CActiveDataProvider('Orders', array(
            'criteria' => array(
                'condition'=>$sql,
                'order' => 'date DESC, id DESC',
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
        $notifications = array();
        $notifications_sql = Notifications::model()->findAll(array('condition'=>'user_id="'.Yii::app()->user->id.'"'));
        if (count($notifications_sql) > 0){
            foreach ($notifications_sql as $not){
                $notifications[] = $not->type.'_'.$not->order_id;
            }
        }

        $this->render('index', array(
            'dataProvider'=>$dataProvider,
            'type'=>$type,
            'notifications'=>$notifications,
        ));

        }
    }

    public function actionDeleteNotify($type,$order_id){
        Notifications::deleteNotification($type,$order_id);
    }

    public function actionRedirectNotify($type,$order_id,$redirect){
        Notifications::deleteNotification($type,$order_id);
        $this->redirect($redirect);
    }

}