<?php

class DocumentsController extends BackEndController
{

    public $razdel_id = 40;


    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index','update','preview','create','delete','filesadd','deletephotos','deletedocs','addtag','deletetag','setcover','updatedocname'),
                'roles'=>array('admin','manager'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {

        $cats = DocumentsCats::model()->findAll(array('condition'=>'','order'=>'number asc,id asc'));

        $this->render('index', array(
            'cats' => $cats,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);

        $this->saveFromPost($model);

        $this->render('form', array(
            'model' => $model,
        ));

    }


    public function actionCreate()
    {
        $model = new DocumentsCats();
        $this->saveFromPost($model);

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionUpdateDocName($id)
    {
        $model = Docs::model()->findByPk($id);
        if ($model){
            if (isset($_POST['name'])){
            $model->filetitle = $_POST['name'];
            $model->save();
            }
        }
    }
    
    private function saveFromPost(DocumentsCats $model)
    {
        $is_new =  $model->isNewRecord;
        if (isset($_POST['DocumentsCats'])) {
            $model->attributes = $_POST['DocumentsCats'];
            $model->save();

            $this->redirect(array('documents/index'));
        }
    }
    
    public function actionDelete($id)
    {

        $model = $this->getModel($id);

        $docs = $model->docs;
        if ($docs)
        {
            foreach($docs as $doc_model){
                Docs::deleteDoc($doc_model);
            }
        }

        $model->delete();
        
        $this->redirect(array('index'));
    }
    
    protected function getModel($id)
    {
        $model = DocumentsCats::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    public function actionFilesAdd($post_id)
    {
        $post_item = DocumentsCats::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {

            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $news_docs = new Docs;
                $news_docs->post_id = $post_id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $uploaded_file;
                $news_docs->filetitle = $file->getName();
                $news_docs->type = 'documents';
                $news_docs->save();
            }
        }
        }
    }

    public function actionDeletePhotos($id){

    }

    public function actionDeleteDocs($id){
        $model = Docs::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Docs::deleteDoc($model);
        }
    }

}