<?php

class EngController extends BackEndController
{
    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index','update','preview','create','delete','filesadd','deletephotos','deletedocs'),
                'roles'=>array('admin','manager'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex($id = 0)
    {
        $dataProvider = new CActiveDataProvider('Eng', array(
            'criteria' => array(
                'condition' => 'cats_id = "'.(int)$id.'"',
                'order' => 'number ASC, id ASC',
            ),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));
        $this->render('index', array(
            'provider' => $dataProvider,
            'id' => $id,
        ));
    }

 public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        $this->saveFromPost($model);

        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "eng" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "eng" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

        $this->render('form', array(
            'model' => $model,
            'photos' => $photos,
            'docs' => $docs,
        ));
    }

    public function actionCreate($id = 0)
    {
        $model = new Eng();
        $model->cats_id = (int)$id;
        $this->saveFromPost($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }

    private function saveFromPost(Eng $model)
    {
        if (isset($_POST['Eng'])) {
            $model->show_raspisanie = 0;
            $model->attributes = $_POST['Eng'];

            $model->image = CUploadedFile::getInstance($model, 'image');

            $isNew = $model->isNewRecord;
            if ($model->save()) {
                if ($model->image) {
                    $uploader = new FileUploader();
                    $file_name = $uploader->addPhoto($model->image, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/eng/');

                    if ($model->filename != '' AND $file_name != ''){
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/eng/'.$size.$model->filename;
                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                            }
                        }
                    }

                    $model->filename = $file_name;
                    $model->save();


                } elseif (isset($_POST['Eng']['removeImage'])) {
                    if ($model->filename != ''){
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/eng/'.$size.$model->filename;
                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                                $model->filename = '';
                                $model->save();
                            }
                        }
                    }
                }
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Подраздел добавлен' : 'Изменения сохранены'
                );

                $this->redirect(array('update','id'=>$model->id));
            }
        }
    }

    public function actionDelete($id)
    {
        $model = $this->getModel($id);
        $cat_id = $model->cats_id;
        $photos = $model->photos;
        if ($photos)
        {
            foreach($photos as $photo_model){
                Photos::deleteImage($photo_model);
            }
        }

        $docs = $model->docs;
        if ($docs)
        {
            foreach($docs as $doc_model){
                Docs::deleteDoc($doc_model);
            }
        }

        if ($model->filename != '')
        {
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/eng/'.$size.$model->filename;

                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                            }
                        }
        }
        $model->delete();
        $this->redirect(array('index','id'=>$cat_id));
    }

    protected function getModel($id)
    {
        return Eng::getPage($id);
    }

    public function actionFilesAdd($cats_id)
    {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $Cats_photos = new Photos;
                $Cats_photos->post_id = $cats_id;
                $Cats_photos->date = new CDbExpression('NOW()');
                $Cats_photos->filename = $uploaded_file;
                $Cats_photos->type = 'eng';
                $Cats_photos->save();
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $Cats_docs = new Docs;
                $Cats_docs->post_id = $cats_id;
                $Cats_docs->date = new CDbExpression('NOW()');
                $Cats_docs->filename = $uploaded_file;
                $Cats_docs->filetitle = $file->getName();
                $Cats_docs->type = 'eng';
                $Cats_docs->save();
            }
        }
    }

    public function actionDeletePhotos($id){
        $model = Photos::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Photos::deleteImage($model);
            //$this->redirect(array('update','id'=>$post_id));
        }
    }
    public function actionDeleteDocs($id){
        $model = Docs::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Docs::deleteDoc($model);
            //$this->redirect(array('update','id'=>$post_id));
        }
    }
}
