<?php

class ExposureController extends BackEndController
{

    public $cats_list   = array();
    public $type        = 'exposure';
    public $razdel_id = 5;

    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index','update','preview','create','delete','filesadd','deletephotos','deletedocs','list','setcover','cats','catsupdate','catscreate','catsdelete',),
                'roles'=>array('admin','manager'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }




    public function actionIndex($cats_id = 0, $exposure_id = 0)
    {
        $condition = '';
        if ((int)$cats_id > 0){
            $condition = 'cats_id="'.(int)$cats_id.'"';
            $exposure_id = 0;
        }elseif ((int)$exposure_id > 0){
            $condition = 'exposure_id="'.(int)$exposure_id.'"';
        }

        //echo Yii::app()->user->role;
        $dataProvider = new CActiveDataProvider('ExposureItems', array(
            'criteria' => array(
                'condition'=>$condition,
                'order' => 'id ASC',
            ),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));


        //Формируем массив-дерево категорий и подкатегорий
        $cats = ExposureCats::model()->findAll(array('order'=>'id asc, parent_id asc'));
        $cats_array = array();
        foreach($cats as $cat){
            $cats_array[] = array('id'=>$cat->id, 'title'=>$cat->title, 'parent_id' => $cat->parent_id);
        }
        $c = ExposureCats::restore_tree($cats_array);
        $this->formCatsArray($c);
        //$this->cats_list[0] = array_merge(array(0=>'Все категории'), $this->cats_list);


        //Получаем массив названий экспозиций
        $exposureModel=Exposure::model()->findAll();
        $exposurelist= CHtml::listData($exposureModel,'id','title');//array_merge(array(0=>'Выберите экспозицию'), );

        $current_exposure = false;
        if ((int)$exposure_id > 0){
            $current_exposure = Exposure::model()->findByPk((int)$exposure_id);
        }


        $this->render('index', array(
            'provider' => $dataProvider,
            'cats_id' => $cats_id,
            'exposure_id' => $exposure_id,
            'exposurelist' => $exposurelist,
            'current_exposure' => $current_exposure,
        ));

    }

    public function actionList()
    {
        //echo Yii::app()->user->role;
        $dataProvider = new CActiveDataProvider('Exposure', array(
            'criteria' => array(
                'condition'=>'',
                'order' => 'id ASC',
            ),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));

        $this->render('list', array(
            'provider' => $dataProvider,
        ));
    }




    public function formCatsArray($data, $level = -1) {
        $this->cats_list[$data['id']] = ExposureCats::show_array($data, $level);
        foreach ($data['child'] as $item) {
            $this->formCatsArray($item, $level + 1);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        $this->saveFromPost($model);

        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "exposure" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "exposure" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));


        $this->render('form', array(
            'model' => $model,
            'photos' => $photos,
            'docs' => $docs,
        ));
    }


    public function actionCreate()
    {
        $model = new Exposure();
        $model->date = date('Y-m-d');
        $this->saveFromPost($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }

    private function saveFromPost(Exposure $model, $action = 'update')
    {
        if (isset($_POST['Exposure'])) {

            $model->attributes = $_POST['Exposure'];

            if ($model->short_text == ''){
                $model->short_text = trim(strip_tags($this->crop_str_word($model->text, 30 )));
            }

            $isNew = $model->isNewRecord;

            if ($model->save())
            {
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Экспозиция добавлена' : 'Изменения сохранены'
                );

                if ($action == 'update')
                {
                $this->redirect(array('update','id'=>$model->id));
                }
            }
        }
    }

    public function actionDelete($id)
    {

        $model = $this->getModel($id);
        $photos = $model->photos;
        if ($photos)
        {
            foreach($photos as $photo_model){
                Photos::deleteImage($photo_model);
            }
        }


        $docs = $model->docs;
        if ($docs)
        {
            foreach($docs as $doc_model){
                Docs::deleteDoc($doc_model);
            }
        }

        if ($model->filename != '')
        {
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/exposure/'.$size.$model->filename;

                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                            }
                        }
        }


        $model->delete();

        $this->redirect(array('index'));
    }

    protected function getModel($id)
    {
        $model = Exposure::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    /////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////


    public function actionCats()
    {

        //Формируем массив-дерево категорий и подкатегорий
        $cats = ExposureCats::model()->findAll(array('order'=>'id asc, parent_id asc'));
        $cats_array = array();
        foreach($cats as $cat){
            $cats_array[] = array('id'=>$cat->id, 'title'=>$cat->title, 'parent_id' => $cat->parent_id);
        }
        $c = ExposureCats::restore_tree($cats_array);
        $this->formCatsArray($c);

        $this->render('cats', array(

        ));

    }


    public function actionCatsUpdate($id)
    {
        $model = $this->getCatsModel($id);
        $this->saveFromPostCats($model);

        //Формируем массив-дерево категорий и подкатегорий
        $cats = ExposureCats::model()->findAll(array('order'=>'id asc, parent_id asc'));
        $cats_array = array();
        foreach($cats as $cat){
            $cats_array[] = array('id'=>$cat->id, 'title'=>$cat->title, 'parent_id' => $cat->parent_id);
        }
        $c = ExposureCats::restore_tree($cats_array);
        $this->formCatsArray($c);
        //$this->cats_list[0] = array_merge(array(0=>'Все категории'), $this->cats_list);

        $this->render('catsform', array(
            'model' => $model,
        ));
    }


    public function actionCatsCreate()
    {
        $model = new ExposureCats();
        $this->saveFromPostCats($model);

        //Формируем массив-дерево категорий и подкатегорий
        $cats = ExposureCats::model()->findAll(array('order'=>'id asc, parent_id asc'));
        $cats_array = array();
        foreach($cats as $cat){
            $cats_array[] = array('id'=>$cat->id, 'title'=>$cat->title, 'parent_id' => $cat->parent_id);
        }
        $c = ExposureCats::restore_tree($cats_array);
        $this->formCatsArray($c);

        $this->render('catsform', array(
            'model' => $model,
        ));
    }

    private function saveFromPostCats(ExposureCats $model, $action = 'catsupdate')
    {
        if (isset($_POST['ExposureCats'])) {

            $model->attributes = $_POST['ExposureCats'];
            $isNew = $model->isNewRecord;

            if ($model->save())
            {
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Категория добавлена' : 'Изменения сохранены'
                );

                if ($action == 'catsupdate')
                {
                $this->redirect(array('catsupdate','id'=>$model->id));
                }
            }
        }
    }

    public function actionCatsDelete($id)
    {
        $model = $this->getCatsModel($id);
        $model->delete();
        $this->redirect(array('cats'));
    }

    protected function getCatsModel($id)
    {
        $model = ExposureCats::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }






    /////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    public function actionFilesAdd($post_id)
    {
        $post_item = Exposure::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $news_photos = new Photos;
                $news_photos->post_id = $post_id;
                $news_photos->date = new CDbExpression('NOW()');
                $news_photos->filename = $uploaded_file;
                $news_photos->type = $this->type;
                $news_photos->save();

                if ($post_item->cover_id == 0 || $post_item->cover_id == '')
                {
                    $post_item->cover_id = $news_photos->id;
                    $post_item->save();
                }
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $news_docs = new Docs;
                $news_docs->post_id = $post_id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $uploaded_file;
                $news_docs->filetitle = $file->getName();
                $news_docs->type = $this->type;
                $news_docs->save();
            }
        }
        }
    }

    public function actionDeletePhotos($id){
        $result = false;
        $photo = Photos::model()->findByPk($id);
        if ($photo)
        {
            $post_id = $photo->post_id;
            Photos::deleteImage($photo);

            $post = Exposure::model()->findByPk($post_id);
            if ($post){
                if ($post->cover_id == $id){
                    $post->cover_id = 0;
                    $post->save();

                    $result = Exposure::autoCover($post);
                }
            }
        }
        echo CJSON::encode($result);
    }


    public function actionDeleteDocs($id){
        $model = Docs::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Docs::deleteDoc($model);
        }
    }

    public function actionSetCover($news_id, $cover_id){
        $news_item = Exposure::model()->findByPk($news_id);
        if ($news_item)
        {
            $news_item->cover_id = $cover_id;
            $news_item->save();

            $new_cover_adress = $news_item->getThumbnailUrl();

            echo json_encode($new_cover_adress);
        }
    }

}
