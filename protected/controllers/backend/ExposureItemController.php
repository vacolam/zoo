<?php

class ExposureitemController extends BackEndController
{

    public $cats_list = array();
    public $razdel_id = 5;

    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index','update','preview','create','delete','filesadd','deletephotos','deletedocs','addtag','deletetag','setcover','copy'),
                'roles'=>array('admin','manager'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }


    public function actionUpdate($id)
    {
        $model = $this->getModel($id);

            $query = "SELECT * FROM {{tags}} WHERE id > 0 and type = 'exposure_item' ORDER BY name";
            $params = array();
            $tags = Yii::app()->db->createCommand($query)->query($params)->readAll();

        	$checked_tags = array();
            if ($model->tags != ''){
            $checked_tags = json_decode($model->tags);
            }


        $this->saveFromPost($model);

        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "exposure_item" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "exposure_item" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));


        //Формируем массив-дерево категорий и подкатегорий
        $cats = ExposureCats::model()->findAll(array('order'=>'id asc, parent_id asc'));
        $cats_array = array();
        foreach($cats as $cat){
            $cats_array[] = array('id'=>$cat->id, 'title'=>$cat->title, 'parent_id' => $cat->parent_id);
        }
        $c = ExposureCats::restore_tree($cats_array);
        $this->formCatsArray($c);


        //Получаем массив названий экспозиций
        $exposureModel=Exposure::model()->findAll();
        $exposurelist = array();
        $exposurelist[0] = 'Выберите экспозицию';
        foreach($exposureModel as $index => $item)
        {
            $exposurelist[$item->id] = $item->title;
        }


        $this->render('form', array(
            'model' => $model,
            'photos' => $photos,
            'docs' => $docs,
            'tags' => $tags,
            'checked_tags' => $checked_tags,
            'exposurelist' => $exposurelist,
        ));

    }

    public function formCatsArray($data, $level = -1) {
        $this->cats_list[$data['id']] = ExposureCats::show_array($data, $level);
        foreach ($data['child'] as $item) {
            $this->formCatsArray($item, $level + 1);
        }
    }



    public function actionCreate()
    {
        $model = new ExposureItems();
        $this->saveFromPost($model);

            $query = "SELECT * FROM {{tags}} WHERE id > 0 and type = 'exposure_item' ORDER BY name";
            $params = array();
            $tags = Yii::app()->db->createCommand($query)->query($params)->readAll();

        	$checked_tags = array();

        //Формируем массив-дерево категорий и подкатегорий
        $cats = ExposureCats::model()->findAll(array('order'=>'id asc, parent_id asc'));
        $cats_array = array();
        foreach($cats as $cat){
            $cats_array[] = array('id'=>$cat->id, 'title'=>$cat->title, 'parent_id' => $cat->parent_id);
        }
        $c = ExposureCats::restore_tree($cats_array);
        $this->formCatsArray($c);


        //Получаем массив названий экспозиций
        $exposureModel=Exposure::model()->findAll();
        $exposurelist=array_merge(array(0=>'Выберите экспозицию'), CHtml::listData($exposureModel,'id','title'));


        $this->render('form', array(
            'model' => $model,
            'tags' => $tags,
            'checked_tags' => $checked_tags,
            'exposurelist' => $exposurelist,
        ));
    }

    private function saveFromPost(ExposureItems $model, $action = 'update')
    {
        if (isset($_POST['ExposureItems'])) {
            $model->attributes = $_POST['ExposureItems'];

            if ($model->short_text == ''){
               $model->short_text = trim(strip_tags($this->crop_str_word($model->text, 30 )));
            }

            $isNew = $model->isNewRecord;


            if (isset($_POST['Tags'])) {
                $tags = json_encode($_POST['Tags']);
                $model->tags = $tags;
            }
            if ($model->save()) {

                Yii::app()->user->setFlash('success',
                    $isNew ? 'Животное добавлено' : 'Изменения сохранены'
                );

                //$this->redirect(array('index'));
                if ($action == 'update')
                {
                $this->redirect(array('update','id'=>$model->id));
                }
            }
        }
    }

    public function actionDelete($id)
    {

        $model = $this->getModel($id);
        $photos = $model->photos;
        if ($photos)
        {
            foreach($photos as $photo_model){
                Photos::deleteImage($photo_model);
            }
        }


        $docs = $model->docs;
        if ($docs)
        {
            foreach($docs as $doc_model){
                Docs::deleteDoc($doc_model);
            }
        }

        $model->delete();

        $this->redirect(array('exposure/index'));
    }

    protected function getModel($id)
    {
        $model = ExposureItems::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    public function actionFilesAdd($post_id)
    {
        $post_item = ExposureItems::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $news_photos = new Photos;
                $news_photos->post_id = $post_id;
                $news_photos->date = new CDbExpression('NOW()');
                $news_photos->filename = $uploaded_file;
                $news_photos->type = 'exposure_item';
                $news_photos->save();

                if ($post_item->cover_id == 0 || $post_item->cover_id == '')
                {
                    $post_item->cover_id = $news_photos->id;
                    $post_item->save();
                }
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $news_docs = new Docs;
                $news_docs->post_id = $post_id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $uploaded_file;
                $news_docs->filetitle = $file->getName();
                $news_docs->type = 'exposure_item';
                $news_docs->save();
            }
        }
        }
    }

    public function actionDeletePhotos($id){
        $result = false;
        $photo = Photos::model()->findByPk($id);
        if ($photo)
        {
            $post_id = $photo->post_id;
            Photos::deleteImage($photo);

            $post = ExposureItems::model()->findByPk($post_id);
            if ($post){
                if ($post->cover_id == $id){
                    $post->cover_id = 0;
                    $post->save();

                    $result = ExposureItems::autoCover($post);
                }
            }
        }
        echo CJSON::encode($result);
    }
    public function actionDeleteDocs($id){
        $model = Docs::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Docs::deleteDoc($model);
            //$this->redirect(array('update','id'=>$post_id));
        }
    }

    public function actionSetCover($news_id, $cover_id){
        $news_item = ExposureItems::model()->findByPk($news_id);
        if ($news_item)
        {
            $news_item->cover_id = $cover_id;
            $news_item->save();

            $new_cover_adress = $news_item->getThumbnailUrl();

            echo json_encode($new_cover_adress);
        }
    }

    public function actionAddTag($tag_name){
        if ($tag_name != '')
        {
            $tag = new Tags();
            $tag->name = $tag_name;
            $tag->type = 'exposure_item';
            $tag->save();
            echo CJSON::encode($tag->id);
        }
    }

    public function actionDeleteTag($id){
        if ((int)$id != 0)
        {
            $tag = Tags::model()->findByPk($id);
            if ($tag)
            {
                $tag->delete();
            }
        }
    }

    public function actionCopy($id){
        $model_old = $this->getModel($id);

        $model_new = new ExposureItems();
        $model_new->cats_id = $model_old->cats_id;
        $model_new->title = "(КОПИЯ) ".$model_old->title;
        $model_new->cover_id = $model_old->cover_id;
        $model_new->short_text = $model_old->short_text;
        $model_new->text = $model_old->text;
        $model_new->tags = $model_old->tags;
        $model_new->exposure_id = $model_old->exposure_id;
        $model_new->redbook = $model_old->redbook;
        $model_new->opeka = $model_old->opeka;
        $model_new->save();

        $photos_old = $model_old->photos;
        if ($photos_old)
        {
            foreach($photos_old as $photo_model)
            {
                $copy_filename_txt = "c_".$photo_model->filename;
                $copy_filename_stxt = "s_c_".$photo_model->filename;
                $photo_old_file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$photo_model->filename;
                $photo_new_file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$copy_filename_txt;
                if (file_exists($photo_old_file)){
                    $copy_result = copy($photo_old_file, $photo_new_file);
                }

                $s_photo_old_file = Yii::getPathOfAlias('webroot').'/upload/photos/s_'.$photo_model->filename;
                $s_photo_new_file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$copy_filename_stxt;
                if (file_exists($s_photo_old_file)){
                    copy($s_photo_old_file, $s_photo_new_file);
                }

                $news_photos                = new Photos;
                $news_photos->post_id       = $model_new->id;
                $news_photos->date          = new CDbExpression('NOW()');
                $news_photos->filename      = $copy_filename_txt;
                $news_photos->type          = 'exposure_item';
                $news_photos->save();

                //Сохраняем обложку
                if ($model_old->cover_id == $photo_model->id)
                {
                    $model_new->cover_id = $news_photos->id;
                    $model_new->save();
                }

            }
        }

        $docs_old = $model_old->docs;
        if ($docs_old)
        {
            foreach($docs_old as $docs_model)
            {
                $copy_filename_txt = "c_".$docs_model->filename;
                $photo_old_file = Yii::getPathOfAlias('webroot').'/upload/docs/'.$docs_model->filename;
                $photo_new_file = Yii::getPathOfAlias('webroot').'/upload/docs/'.$copy_filename_txt;
                if (file_exists($photo_old_file)){
                    $copy_result = copy($photo_old_file, $photo_new_file);
                }

                $news_docs = new Docs;
                $news_docs->post_id = $model_new->id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $copy_filename_txt;
                $news_docs->filetitle = $docs_model->filetitle;
                $news_docs->type = 'exposure_item';
                $news_docs->save();
            }
        }

        Yii::app()->user->setFlash('success',
            'Копирование завершено'
        );

        $this->redirect(array('update','id'=>$model_new->id));

    }

}
