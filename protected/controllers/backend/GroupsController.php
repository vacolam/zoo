<?php

class GroupsController extends BackEndController
{

    public $razdel_id = 51;


    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index','orders','view','update','preview','create','delete','filesadd','deletephotos','deletedocs','addtag','deletetag','setcover','addtime','deletetime','changetime','order','orderstatus'),
                'roles'=>array('admin','manager'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex($tag = 0)
    {
        $tag_sql = '';
        if ((int)$tag > 0)
        {
            $tag_sql = "tags like '%\"".$tag."\"%'";
        }

        $dataProvider = new CActiveDataProvider('Groups', array(
            'criteria' => array(
                'condition'=>$tag_sql,
                'order' => 'date DESC, id DESC',
            ),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));



        $this->render('index', array(
            'provider' => $dataProvider,
        ));
    }

    public function actionOrders()
    {


        $dataProvider = new CActiveDataProvider('GroupsOrders', array(
            'criteria' => array(
                'condition'=>'',
                'order' => 'id desc',
            ),
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));

        $this->render('orders', array(
            'provider' => $dataProvider,
        ));
    }

    public function actionView($id)
    {
        $model = $this->getModel($id);

        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "groups" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));


        $timetable = array();
        $timetable['mn'] = GroupsTime::model()->findAll(array('condition'=>'week_day="mn" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
        $timetable['tu'] = GroupsTime::model()->findAll(array('condition'=>'week_day="tu" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
        $timetable['we'] = GroupsTime::model()->findAll(array('condition'=>'week_day="we" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
        $timetable['th'] = GroupsTime::model()->findAll(array('condition'=>'week_day="th" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
        $timetable['fr'] = GroupsTime::model()->findAll(array('condition'=>'week_day="fr" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
        $timetable['sa'] = GroupsTime::model()->findAll(array('condition'=>'week_day="sa" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));
        $timetable['su'] = GroupsTime::model()->findAll(array('condition'=>'week_day="su" and groups_id="'.$model->id.'" and deleted=0','order'=>'number asc, id asc'));

        $dataProvider = new CActiveDataProvider('GroupsOrders', array(
            'criteria' => array(
                'condition'=>'groups_id="'.$id.'"',
                'order' => 'id desc',
            ),
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));


        $this->render('view', array(
            'model' => $model,
            'photos' => $photos,
            'timetable' => $timetable,
            'dataProvider' => $dataProvider,
        ));

    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);

        $this->saveFromPost($model);

        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "groups" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "groups" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));


        $this->render('form', array(
            'model' => $model,
            'photos' => $photos,
            'docs' => $docs,
        ));

    }


    public function actionCreate()
    {
        $model = new Groups();
        $model->date = date('Y-m-d');
        $this->saveFromPost($model);

        $this->render('form', array(
            'model' => $model,
        ));
    }
    
    private function saveFromPost(Groups $model, $action = 'update')
    {
            $isNew = $model->isNewRecord;
        if (isset($_POST['Groups'])) {
            $model->attributes = $_POST['Groups'];

            if ($model->short_text == ''){
               $model->short_text = trim(strip_tags($this->crop_str_word($model->text, 30 )));
            }

            if ($model->save()) {
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Групповое мероприятие добавлено' : 'Изменения сохранены'
                );
                 if ($action == 'update')
                {
                $this->redirect(array('view','id'=>$model->id));
                }
            }
        }
    }
    
    public function actionDelete($id)
    {

        $model = $this->getModel($id);
        $photos = $model->photos;
        if ($photos)
        {
            foreach($photos as $photo_model){
                Photos::deleteImage($photo_model);
            }
        }


        $docs = $model->docs;
        if ($docs)
        {
            foreach($docs as $doc_model){
                Docs::deleteDoc($doc_model);
            }
        }

        $model->delete();
        
        $this->redirect(array('index'));
    }
    
    protected function getModel($id)
    {
        $model = Groups::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    public function actionFilesAdd($post_id)
    {
        $post_item = Groups::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $news_photos = new Photos;
                $news_photos->post_id = $post_id;
                $news_photos->date = new CDbExpression('NOW()');
                $news_photos->filename = $uploaded_file;
                $news_photos->type = 'groups';
                $news_photos->save();

                if ($post_item->cover_id == 0 || $post_item->cover_id == '')
                {
                    $post_item->cover_id = $news_photos->id;
                    $post_item->save();
                }
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $news_docs = new Docs;
                $news_docs->post_id = $post_id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $uploaded_file;
                $news_docs->filetitle = $file->getName();
                $news_docs->type = 'groups';
                $news_docs->save();
            }
        }
        }
    }

    public function actionDeletePhotos($id){
        $result = false;
        $photo = Photos::model()->findByPk($id);
        if ($photo)
        {
            $post_id = $photo->post_id;
            Photos::deleteImage($photo);

            $post = Groups::model()->findByPk($post_id);
            if ($post){
                if ($post->cover_id == $id){
                    $post->cover_id = 0;
                    $post->save();

                    $result = Groups::autoCover($post);
                }
            }
        }
        echo CJSON::encode($result);
    }
    public function actionDeleteDocs($id){
        $model = Docs::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Docs::deleteDoc($model);
            //$this->redirect(array('update','id'=>$post_id));
        }
    }

    public function actionSetCover($news_id, $cover_id){
        $news_item = Groups::model()->findByPk($news_id);
        if ($news_item)
        {
            $news_item->cover_id = $cover_id;
            $news_item->save();

            $new_cover_adress = $news_item->getThumbnailUrl();

            echo json_encode($new_cover_adress);
        }
    }

    public function actionAddTime($time,$week_day,$groups_id){
        if ($time != '' and $week_day != '' and $groups_id != '')
        {
            $other_times =  GroupsTime::model()->findAll(array('condition'=>'week_day="'.$week_day.'" and groups_id="'.$groups_id.'" and deleted=0','order'=>'number asc, id asc'));
            $count = count($other_times) + 1;
            $tag = new GroupsTime();
            $tag->time = $time;
            $tag->week_day = $week_day;
            $tag->groups_id = $groups_id;
            $tag->number = $count;
            $tag->save();
            echo CJSON::encode($tag->id);
        }
    }

    public function actionChangeTime($id,$time){
            $timetabel = GroupsTime::model()->findByPk($id);
            if ($timetabel){
                $timetabel->time = $time;
                $timetabel->save();
            }
    }

    public function actionDeleteTime($id){
            $timetabel = GroupsTime::model()->findByPk($id);
            if ($timetabel){
                $timetabel->deleted = 1;
                $timetabel->save();
            }
    }

        /*
     Просмотр работы
    */
    public function actionOrder($id)
    {
        $model = GroupsOrders::model()->findByPk($id);
        if ($model){
               $group = Groups::model()->findByPk($model->groups_id);
               $this->render('order', array(
                'model' => $model,
                'group' => $group,
            ));
        }
    }

    public function actionOrderStatus($status=0,$id)
    {
        $model = GroupsOrders::model()->findByPk($id);
        if ($model){
            $model->status = $status;
            $model->save();
            Yii::app()->user->setFlash('success',
                    'Статус изменен'
            );
        }
        $this->redirect(array('view','id'=>$model->groups_id));
    }
}