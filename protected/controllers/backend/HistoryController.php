<?php
class HistoryController extends BackEndController
{
    public $razdel_id = 38;

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }


    public function actionIndex()
    {
        $model = Cats::model()->findByPk($this->razdel_id);
        $this->savePage($model);

        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "pages" And post_id = "'.$this->razdel_id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "pages" And post_id = "'.$this->razdel_id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

        $charts = Charts::model()->findAll();

        $this->render('index', array(
            'model'     => $model,
            'photos'    => $photos,
            'docs'      => $docs,
            'charts'    => $charts,
        ));

    }


    private function savePage(Cats $model)
    {
        if (isset($_POST['Cats'])) {
            $model->attributes = $_POST['Cats'];
            $model->save();
        }
    }



     public function actionChartUpdate($id)
    {
        $model = $this->getModel($id);
        $this->saveChart($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionChartCreate()
    {
        $model = new Charts();
        $this->saveChart($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }


    private function saveChart(Charts $model)
    {
        if (isset($_POST['Charts']))
        {

            //формируем массив данных
            $MegaArray = array();
            if (isset($_POST['data_x'])){
                $MegaArray[0] = array
                (
                    'type' => 'x',
                    'name' => '',
                    'data' => $_POST['data_x']
                );
            }

            if (isset($_POST['data_y'])){
                foreach ($_POST['data_y'] as $index => $array_y)
                {
                    $name = '';
                    $data = array();
                    if (isset($array_y['name'])){$name = $array_y['name'];}
                    if (isset($array_y['data'])){
                        $data = $array_y['data'];
                    }
                    $MegaArray[] = array
                    (
                        'type' => 'y',
                        'name' => $name,
                        'data' => $data
                    );
                }
            }


            $data = CJSON::encode($MegaArray);

            $model->attributes = $_POST['Charts'];
            $model->data = $data;
            $model->save();

            Yii::app()->user->setFlash('success', "Изменения сохранены");
            $this->redirect(array('history/index'));

            //echo "<pre>";
            //print_r($MegaArray);
            //echo "////////////////////////////<br><br><br><br><br><br><br>";
            //print_r($_POST['data_x']);
            //print_r($_POST['data_y']);
            //echo "</pre>";
        }
    }

    public function actionChartDelete($id = 0)
    {
        $model = Charts::model()->findByPk($id);
        if ($model)
        {
            $model->delete();
        }

        Yii::app()->user->setFlash('success', "Диаграмма удалена");
        $this->redirect(array('history/index'));
    }

    protected function getModel($id)
    {
        $model = Charts::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }




}
