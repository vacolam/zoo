<?php

class InfoblocksController extends BackEndController
{

    public $razdel_parent = 'home';   

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }
    
    public function actionIndex()
    {
        $banners = InfoBlocks::model()->findAll();
        $this->render('index', array(
            'banners' => $banners,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        $this->saveFromPost($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }
    
    public function actionCreate()
    {
        $model = new InfoBlocks();
        $this->saveFromPost($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }

    private function saveFromPost(InfoBlocks $model)
    {
        if (isset($_POST['InfoBlocks'])) {
            $model->attributes = $_POST['InfoBlocks'];

            $isNew = $model->isNewRecord;
            if ($model->save()) {
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Блок добавлен' : 'Изменения сохранены'
                );
                $this->redirect(array('index'));
            }
        }
    }
    
    public function actionDelete($id)
    {
        $model = $this->getModel($id);
        $model->delete();
        $this->redirect(array('index'));
    }
    
    protected function getModel($id)
    {
        $model = InfoBlocks::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}