<?php

class NewsController extends BackEndController
{

    public $razdel_id = 1;
    public $razdel_parent = 'news';


    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index','update','preview','create','delete','filesadd','deletephotos','deletedocs','addtag','deletetag','setcover'),
                'roles'=>array('admin','manager'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex($tag = 0)
    {

        $tag_sql = '';
        if ((int)$tag > 0)
        {
            $tag_sql = "tags like '%\"".$tag."\"%'";
        }

        $dataProvider = new CActiveDataProvider('News', array(
            'criteria' => array(
                'condition'=>''.$tag_sql,
                'order' => 'date DESC, id DESC',
            ),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));

        $query = "SELECT * FROM {{tags}} WHERE id > 0 and type = 'news' ORDER BY name";
        $params = array();
        $tags = Yii::app()->db->createCommand($query)->query($params)->readAll();

        $this->render('index', array(
            'provider' => $dataProvider,
            'tags' => $tags,
        ));

    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);

            $query = "SELECT * FROM {{tags}} WHERE id > 0 and type = 'news' ORDER BY name";
            $params = array();
            $tags = Yii::app()->db->createCommand($query)->query($params)->readAll();

        	$checked_tags = array();
            if ($model->tags != ''){
            $checked_tags = json_decode($model->tags);
            }


        $this->saveFromPost($model);

        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "news" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "news" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));

        $exposureItems = ExposureItems::model()->findAll(array('condition'=>'','order'=>'title ASC'));
        $exposureItemsList[0] = 'Привязать новость к животному';
        if ($exposureItems){
            foreach($exposureItems as $item){
                $exposureItemsList[$item->id] = $item->title;
            }
        }


        $this->render('form', array(
            'model' => $model,
            'photos' => $photos,
            'docs' => $docs,
            'tags' => $tags,
            'checked_tags' => $checked_tags,
            'exposureItemsList' => $exposureItemsList,
        ));

    }

    public function actionPreview($id)
    {
        $model = $this->getModel($id);
        $this->saveFromPost($model,'preview');
        $this->render('preview', array(
            'model' => $model,
        ));
    }


    public function actionCreate()
    {
        $model = new News();
        $model->date = date('Y-m-d');
        $model->closed_till_time = '00:00:00';

        $this->saveFromPost($model);

            $query = "SELECT * FROM {{tags}} WHERE id > 0 and type = 'news' ORDER BY name";
            $params = array();
            $tags = Yii::app()->db->createCommand($query)->query($params)->readAll();

        	$checked_tags = array();
            if ($model->tags != ''){
            $checked_tags = json_decode($model->tags);
            }

        $exposureItems = ExposureItems::model()->findAll(array('condition'=>'','order'=>'title ASC'));
        $exposureItemsList[0] = 'Привязать новость к животному';
        if ($exposureItems){
            foreach($exposureItems as $item){
                $exposureItemsList[$item->id] = $item->title;
            }
        }

        $this->render('form', array(
            'model' => $model,
            'tags' => $tags,
            'checked_tags' => $checked_tags,
            'exposureItemsList' => $exposureItemsList,
        ));
    }

    private function saveFromPost(News $model, $action = 'update')
    {
        if (isset($_POST['News'])) {
            $model->attributes = $_POST['News'];
            if ($action == 'update' || $action == 'create')
            {
               $model->text = $model->pre_text;
            }


            if ($model->short_text == ''){
               $model->short_text = trim(strip_tags($this->crop_str_word($model->pre_text, 30 )));
            }

            $model->image = CUploadedFile::getInstance($model, 'image');
           	$date = new DateTime($_POST['News']['date']);
			$model->date = $date->format('Y-m-d');

            if ($_POST['News']['closed_till'] != '' AND $_POST['News']['closed_till'] != '0000-00-00')
            {
                $date_clt = new DateTime($_POST['News']['closed_till']);
    			$model->closed_till = $date_clt->format('Y-m-d');
                $time = '00:00';
                if (isset($_POST['News']['closed_till_hour']) AND isset($_POST['News']['closed_till_minute']))
                {
                    $time = $_POST['News']['closed_till_hour'].":".$_POST['News']['closed_till_minute'];
                }
                $model->closed_till_time = $time;
            }

            $isNew = $model->isNewRecord;


            if (isset($_POST['Tags'])) {
                $tags = json_encode($_POST['Tags']);
                $model->tags = $tags;
            }
            if ($model->save()) {
                if ($model->image) {
                    //$file_name = uniqid().'_'.time() . '.' . $model->image->extensionName;
                    //$path = Yii::getPathOfAlias('webroot').'/upload/news/'.$file_name;
                    //$model->image->saveAs($path);

                    //$image = new SimpleImage($path);
                    //$image->thumbnail(News::IMAGE_WIDTH, News::IMAGE_HEIGHT);
                    //$image->save($path, 80);

                    $uploader = new FileUploader();
                    $file_name = $uploader->addPhoto($model->image, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/news/');

                    if ($model->filename != '' AND $file_name != ''){
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/news/'.$size.$model->filename;
                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                            }
                        }
                    }

                    $model->filename = $file_name;
                    $model->save();


                } elseif (isset($_POST['News']['removeImage'])) {
                    if ($model->filename != ''){
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/news/'.$size.$model->filename;

                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                                 //echo $deleted_path."<br>";
                                 $model->filename = '';
                                 $model->save();
                            }
                        }
                    }
                }
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Новость добавлена' : 'Изменения сохранены'
                );

                //$this->redirect(array('index'));
                if ($action == 'update')
                {
                $this->redirect(array('update','id'=>$model->id));
                }
            }
        }
    }

    public function actionDelete($id)
    {

        $model = $this->getModel($id);
        $photos = $model->photos;
        if ($photos)
        {
            foreach($photos as $photo_model){
                Photos::deleteImage($photo_model);
            }
        }


        $docs = $model->docs;
        if ($docs)
        {
            foreach($docs as $doc_model){
                Docs::deleteDoc($doc_model);
            }
        }

        if ($model->filename != '')
        {
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/news/'.$size.$model->filename;

                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                            }
                        }
        }


        $model->delete();

        $this->redirect(array('index'));
    }

    protected function getModel($id)
    {
        $model = News::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    public function actionFilesAdd($post_id)
    {
        $post_item = News::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $news_photos = new Photos;
                $news_photos->post_id = $post_id;
                $news_photos->date = new CDbExpression('NOW()');
                $news_photos->filename = $uploaded_file;
                $news_photos->type = 'news';
                $news_photos->save();

                if ($post_item->cover_id == 0 || $post_item->cover_id == '')
                {
                    $post_item->cover_id = $news_photos->id;
                    $post_item->save();
                }
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $news_docs = new Docs;
                $news_docs->post_id = $post_id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $uploaded_file;
                $news_docs->filetitle = $file->getName();
                $news_docs->type = 'news';
                $news_docs->save();
            }
        }
        }
    }

    public function actionDeletePhotos($id){
        $result = false;
        $photo = Photos::model()->findByPk($id);
        if ($photo)
        {
            $post_id = $photo->post_id;
            Photos::deleteImage($photo);

            $post = News::model()->findByPk($post_id);
            if ($post){
                if ($post->cover_id == $id){
                    $post->cover_id = 0;
                    $post->save();

                    $result = News::autoCover($post);
                }
            }
        }
        echo CJSON::encode($result);
    }
    public function actionDeleteDocs($id){
        $model = Docs::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Docs::deleteDoc($model);
            //$this->redirect(array('update','id'=>$post_id));
        }
    }

    public function actionSetCover($news_id, $cover_id){
        $news_item = News::model()->findByPk($news_id);
        if ($news_item)
        {
            $news_item->cover_id = $cover_id;
            $news_item->save();

            $new_cover_adress = $news_item->getThumbnailUrl();

            echo json_encode($new_cover_adress);
        }
    }

    public function actionAddTag($tag_name){
        if ($tag_name != '')
        {
            $tag = new Tags();
            $tag->name = $tag_name;
            $tag->type = 'news';
            $tag->save();
            echo CJSON::encode($tag->id);
        }
    }

    public function actionDeleteTag($id){
        if ((int)$id != 0)
        {
            $tag = Tags::model()->findByPk($id);
            if ($tag)
            {
                $tag->delete();
            }
        }
    }

}
