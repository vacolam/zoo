<?php

class OptionsController extends BackEndController
{

	/*
	 * УПРАВЛЕНИЕ ЮЗЕРАМИ
	 *
	 */
    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('userlist','userview','userpass','userdelete','create'),
                'roles'=>array('admin'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

     /**
	 * Список юзеров
	 */
	public function actionUserList()
	{

		$users = User::model()->findAll(array(
				'condition'	=>	'',
				'order'		=>	'id',
			));

		$this->render('userlist', array(
			'users'=>$users,
		));

	}



     /**
	 * Просмотр/редактирование юзера
	 */
	public function actionUserView($id='new')
	{

	    if ((int)$id) $users = User::model()->findByPk((int)$id);
		if (!isset($users) || !$users) $users = new User;

        //Уведомления
        if (isset($_POST['notifications']))
        {
            $n_ar = array();
            if (count($_POST['notifications']) > 0){
                foreach($_POST['notifications'] as $n_i => $val){
                    $n_ar[] = $val;
                }
            }
            $users->notify = json_encode($n_ar);
            $users->save();
        }

        //Доступы
        $accesss_post_array = array();
        $json_new = '{}';
        $json_old = $users->accesss;
        if (isset($_POST['accesss_array']))
        {
            $accesss_post_array = $_POST['accesss_array'];
        }

        if (count($accesss_post_array) > 0)
        {
            $t=array();
            $subcats = array();
            if (isset($accesss_post_array['subcats']))
            {
                $subcats = $accesss_post_array['subcats'];
            }

            foreach($accesss_post_array as $index => $ar)
            {
                if ($index === 'subcats'){continue;}
                if ($index === 'notifications'){continue;}
                $t[$ar] = array();
                if (count($subcats) > 0)
                {
                    if (isset($subcats[$ar]))
                    {
                        $t[$ar] = $subcats[$ar];
                    }
                }
            }
            $json_new = json_encode($t);

            if ($json_new != $json_old){
                $users->accesss = $json_new;
                $users->save();
            }
        }
        ///


		if (isset($_POST['User'])) {
			if ($_POST['User']['name'] != '' && $_POST['User']['email'] != '') {
	            $users->attributes = $_POST['User'];
				if ($users->validate()) {

				$emailCopy = User::model()->count(array(
				'condition'	=>	'id != "'.(int)$id.'" AND email="'.$_POST['User']['email'].'"',
				));

				if ($emailCopy > 0){
                   Yii::app()->user->setFlash('userUpdateError', 'Такой электронный адрес уже используется');
				}else{
					if ($users->save()){
						Yii::app()->user->setFlash('userUpdateSuccess', 'Данные успешно изменены');
						$this->redirect(array('options/userview','id'=>$users->id));
					}else{
	                    Yii::app()->user->setFlash('userUpdateError', 'Изменения не сохранились');
					}
				}
				}else{
					Yii::app()->user->setFlash('userUpdateError', 'Проверьте формат e-mail и заполненность других полей');
				}
			}
			else{
					Yii::app()->user->setFlash('userUpdateError', 'Поля Имя и E-mail должны быть заполнены');
				}
		}

		$this->render('userview', array(
			'users'			=>	$users,
		));

	}


	 /**
	 * изменение пароля у просматриваемого сотрудника
	 */
	public function actionUserPass($id)
	{
       if ((int)$id) $users = User::model()->findByPk((int)$id);
		if (isset($_POST['User'])) {
			if ($_POST['User']['password'] != ''){
		            $users->attributes = $_POST['User'];
					if ($users->validate()) {
						if ($users->password){
		                   $users->password = CPasswordHelper::hashPassword(Salt::password($users->password));
						}
						if($users->save()){
							Yii::app()->user->setFlash('passSuccess', 'Новый пароль "'.$_POST['User']['password'].'" сохранен');
						}

					}
			}else{
				Yii::app()->user->setFlash('passError', 'Пароль не может быть пустым');
			}
		}

		$this->redirect(array('options/userview','id'=>$users->id));
	}


    /**
	 * Удаляем сотрудника. Насовсем из базы не удаляется,т.к. слетят имена в архиве.
	 */
	public function actionUserDelete($id)
	{
	   	if ((int)$id) $model = User::model()->findByPk((int)$id);
		if ($model) {
		if ($model->role != 'admin') {
				$deleteName = $model->name;
                if($model->delete()){
					Yii::app()->user->setFlash('deleteUserSuccess', 'Пользователь "'.$deleteName.'" успешно удален');
				}else{
                    Yii::app()->user->setFlash('deleteUserError', 'Пользователь "'.$deleteName.'" не удалился');
				}
		}
		}
		$this->redirect(array('options/userlist'));
	}


    public function actionCreate()
    {
        $new = new User();

	    if (isset($_POST['User'])) {
	    	if ($_POST['User']['name'] != '' && $_POST['User']['email'] != '' && $_POST['User']['password'] != '' ){
			    	$user = new User;
		            $user->attributes = $_POST['User'];
					if ($user->validate()) {

						$emailCopy = User::model()->count(array(
						'condition'	=>	'email="'.$_POST['User']['email'].'"',
						));

						if ($emailCopy > 0){
		                   Yii::app()->user->setFlash('newUserError', 'Такой электронный адрес уже используется');
						}else{
								if ($user->password){
				                   $user->password = CPasswordHelper::hashPassword(Salt::password($user->password));
								}
								if ($user->save()){
									//Yii::app()->user->setFlash('newUserSuccess', 'Пользователь "'.$user->name.'" успешно создан');
                                    $this->redirect(array('userview','id'=>$user->id));
								}
						}
					}else{
		                Yii::app()->user->setFlash('newUserError', 'Проверьте формат E-mail и заполненность других полей');
					}
			}else{
                Yii::app()->user->setFlash('newUserError', 'Поля "Имя", "E-mail", "Пароль" должны быть заполнены');
			}
		}

        $this->render('create', array(
            'new'=>$new
        ));
    }



}
