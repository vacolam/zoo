<?php

class OtherinfoController extends BackEndController
{

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('OtherInfo', array(
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
        $this->render('index', array(
            'provider' => $dataProvider,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        $this->saveFromPost($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }
    
    public function actionCreate()
    {
        $model = new OtherInfo();
        $this->saveFromPost($model);
        $this->render('form', array(
            'model' => $model,
        ));
    }
    
    private function saveFromPost(OtherInfo $model)
    {
        if (isset($_POST['OtherInfo'])) {
            $model->attributes = $_POST['OtherInfo'];
            $isNew = $model->isNewRecord;
            if ($model->save()) {
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Информация добавлена' : 'Изменения сохранены'
                );
                $this->redirect(array('index'));
            }
        }
    }
    
    public function actionDelete($id)
    {
        $model = $this->getModel($id);
        $model->delete();
        $this->redirect(array('index'));
    }
    
    protected function getModel($id)
    {
        $model = OtherInfo::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}