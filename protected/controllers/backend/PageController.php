<?php

abstract class PageController extends BackEndController
{
    abstract protected function getPageId();
    
    public function actionUpdate()
    {
        $pageId = $this->getPageId();
        $page = $this->getModel($pageId);
        if (isset($_POST['Page'])) {
            $page->attributes = $_POST['Page'];
            if ($page->save()) {
                Yii::app()->user->setFlash('success', "Изменения сохранены");
                $this->redirect(array($pageId . '/update'));
            }
        }
        $this->render('//pages/form', array(
            'model' => $page,
        ));
    }

    /**
     * Возвращает модель страницы
     */
    protected function getModel($id)
    {
        $page = Page::model()->findByPk($id);
        if ($page === null) {
            throw new CHttpException(404);
        }
        return $page;
    }
}