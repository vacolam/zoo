<?php

class PagesController extends BackEndController
{

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }
    
    public function actionIndex($id)
    {
        $model = Cats::getPage($id);
        $dataProvider = new CActiveDataProvider('Cats', array(
            'criteria' => array(
                'condition' => 'cats_id = "'.(int)$id.'"',
                'order' => 'number ASC, id ASC',
            ),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));
        $this->render('index', array(
            'provider' => $dataProvider,
            'id' => $id,
            'model' => $model,
        ));
    }
    
 public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        $parent = $this->getModel($model->cats_id);
        $this->saveFromPost($model);

        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "pages" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "pages" And post_id = "'.$id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

        $this->render('form', array(
            'model' => $model,
            'photos' => $photos,
            'docs' => $docs,
            'parent' => $parent,
        ));
    }

    public function actionCreate($id)
    {
        $parent = $this->getModel($id);
        $model = new Cats();
        $cats = Cats::model()->findAll(array('condition'=>'cats_id = "'.$parent->id.'"'));
        $model->number = count($cats) + 1;

        $model->cats_id = (int)$id;
        $this->saveFromPost($model);
        $this->render('form', array(
            'model' => $model,
            'parent' => $parent,
        ));
    }

    private function saveFromPost(Cats $model)
    {
        if (isset($_POST['Cats'])) {
            $model->attributes = $_POST['Cats'];
            $model->image = CUploadedFile::getInstance($model, 'image');

            $isNew = $model->isNewRecord;
            if ($model->save()) {
                if ($model->image) {
                    $uploader = new FileUploader();
                    $file_name = $uploader->addPhoto($model->image, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/pages/');

                    if ($model->filename != '' AND $file_name != ''){
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/pages/'.$size.$model->filename;
                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                            }
                        }
                    }

                    $model->filename = $file_name;
                    $model->save();


                } elseif (isset($_POST['Cats']['removeImage'])) {
                    //$model->removeImage();
                    if ($model->filename != ''){
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/pages/'.$size.$model->filename;

                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                                 $model->filename = '';
                                 $model->save();
                            }
                        }
                    }
                }
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Подраздел добавлен' : 'Изменения сохранены'
                );

                $this->redirect(array('update','id'=>$model->id));
            }
        }
    }

    public function actionDelete($id)
    {
        $model = $this->getModel($id);
        $cat_id = $model->cats_id;
        $photos = $model->photos;
        if ($photos)
        {
            foreach($photos as $photo_model){
                Photos::deleteImage($photo_model);
            }
        }

        $docs = $model->docs;
        if ($docs)
        {
            foreach($docs as $doc_model){
                Docs::deleteDoc($doc_model);
            }
        }

        if ($model->filename != '')
        {
                        foreach(array('','s_') as $size)
                        {
                            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/pages/'.$size.$model->filename;

                            if (file_exists($deleted_path)) {
                                unlink($deleted_path);
                            }
                        }
        }

        $model->delete();
        $this->redirect(array('index','id'=>$cat_id));
    }

    protected function getModel($id)
    {
        return Cats::getPage($id);
    }

    public function actionFilesAdd($cats_id)
    {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $Cats_photos = new Photos;
                $Cats_photos->post_id = $cats_id;
                $Cats_photos->date = new CDbExpression('NOW()');
                $Cats_photos->filename = $uploaded_file;
                $Cats_photos->type = 'pages';
                $Cats_photos->save();
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $Cats_docs = new Docs;
                $Cats_docs->post_id = $cats_id;
                $Cats_docs->date = new CDbExpression('NOW()');
                $Cats_docs->filename = $uploaded_file;
                $Cats_docs->filetitle = $file->getName();
                $Cats_docs->type = 'pages';
                $Cats_docs->save();
            }
        }
    }

    public function actionDeletePhotos($id){
        $model = Photos::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Photos::deleteImage($model);
            //$this->redirect(array('update','id'=>$post_id));
        }
    }
    public function actionDeleteDocs($id){
        $model = Docs::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Docs::deleteDoc($model);
            //$this->redirect(array('update','id'=>$post_id));
        }
    }
}
