<?php
class PartnersController extends BackEndController
{

    public $razdel_id = 44;

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }    

    public function actionIndex()
    {
        $banners = Partners::model()->findAll(array('condition'=>'type="partner"'));
        $custodians = Partners::model()->findAll(array('condition'=>'type="custodian"'));
        $exposureItems = ExposureItems::model()->findAll(array('condition'=>'','order'=>'title ASC'));
        $exposureItemsList[0] = 'Выберите животное';
        if ($exposureItems){
            foreach($exposureItems as $item){
                $exposureItemsList[$item->id] = $item->title;
            }
        }
        $this->render('index', array(
            'banners' => $banners,
            'custodians' => $custodians,
            'exposureItemsList' => $exposureItemsList,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        $this->saveFromPost($model);

        $exposureItems = ExposureItems::model()->findAll(array('condition'=>'','order'=>'title ASC'));
        $exposureItemsList[0] = 'Выберите животное';
        if ($exposureItems){
            foreach($exposureItems as $item){
                $exposureItemsList[$item->id] = $item->title;
            }
        }

        $this->render('form', array(
            'model' => $model,
            'exposureItemsList' => $exposureItemsList,
        ));
    }
    
    public function actionCreate()
    {
        $model = new Partners();
        $this->saveFromPost($model);
        $exposureItems = ExposureItems::model()->findAll(array('condition'=>'','order'=>'title ASC'));
        $exposureItemsList[0] = 'Выберите животное';
        if ($exposureItems){
            foreach($exposureItems as $item){
                $exposureItemsList[$item->id] = $item->title;
            }
        }

        $this->render('form', array(
            'model' => $model,
            'exposureItemsList' => $exposureItemsList,
        ));
    }
    
    private function saveFromPost(Partners $model)
    {
        if (isset($_POST['Partners'])) {
            $model->attributes = $_POST['Partners'];

            $model->image = CUploadedFile::getInstance($model, 'image');

            $isNew = $model->isNewRecord;
            if ($model->save()) {
                if ($model->image) {
                    $file_name = uniqid().'_'.time() . '.' . $model->image->extensionName;
                    $path = Yii::getPathOfAlias('webroot').'/upload/partners/'.$file_name;
                    $model->image->saveAs($path);
                    $image = new SimpleImage($path);
                    $image->thumbnail(Partners::WIDTH, Partners::HEIGHT);
                    $image->save($path, 80);

                    if ($model->filename != '' AND $file_name != ''){
                        $deleted_path = Yii::getPathOfAlias('webroot').'/upload/partners/'.$model->filename;
                        if (file_exists($deleted_path)) {
                            unlink($deleted_path);
                        }
                    }
                    $model->filename = $file_name;
                    $model->save();
                }
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Партнер добавлен' : 'Изменения сохранены'
                );
                $this->redirect(array('index'));
            }
        }
    }
    
    public function actionDelete($id)
    {
        $model = $this->getModel($id);
        $model->delete();
        $this->redirect(array('index'));
    }
    
    protected function getModel($id)
    {
        $model = Partners::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}