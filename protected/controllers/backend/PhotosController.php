<?php

class PhotosController extends BackEndController
{

    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('view', 'crop','save'),
                'roles'=>array('admin','manager'),
            ),
            array('allow',
                'actions'=>array(),
                'roles'=>array('user'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    protected function beforeAction($action)
    {
        parent::beforeAction($action);
        //$this->typeList
        return true;
    }

    public function actionView($id,$crop_type = 'image')
    {
        $photo = Photos::model()->findByPk($id);
        $this->render('view', array(
            'photo' => $photo,
            'crop_type' => $crop_type,
        ));

    }

    public function actionCrop()
    {
        $crop = new CropAvatar(
          isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
          isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
          isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
        );

        $response = array(
          'state'  => 200,
          'message' => $crop -> getMsg(),
          'result' => $crop -> getResult()
        );

        echo json_encode($response);
    }

    public function actionSave($id, $crop_type = 'image')
    {
        $X_SIZE = 1200;
        $Y_SIZE = 800;
        $photo = Photos::model()->findByPk($id);
        if ($photo)
        {
            if (isset($_POST['canvas']))
            {
            $photos_info = explode('.',$photo->filename);
            if (is_array($photos_info)){
                if (count($photos_info)>1)
                {

                        $extension_name = $photos_info[count($photos_info)-1];
                        if ($extension_name != ''){

                        if ($crop_type == 'image')
                        {
                        $filename = uniqid().'_'.time() . '.' . $extension_name;
                        $success = false;

                        //define('UPLOAD_DIR', 'upload/photos/');
                    	$img = $_POST['canvas'];
                    	$img = str_replace('data:image/png;base64,', '', $img);
                    	$img = str_replace('data:image/jpeg;base64,', '', $img);
                    	$img = str_replace('data:image/gif;base64,', '', $img);
                    	$img = str_replace(' ', '+', $img);
                    	$data = base64_decode($img);

                        $path = Yii::getPathOfAlias('webroot').'/upload/photos/';

                        $size_array = array('normal','small');
                        if ($crop_type == 'preview'){
                            $size_array = array('small');
                        }

                        foreach ($size_array as $size)
                        {
                            //echo $size." - ".$filename."<br>";
                            $s = '';
                            if ($size == 'small'){$s = 's_';}
                            $newFile = $path . $s.$filename;

                        	$success = file_put_contents($newFile, $data);
                            if ($success)
                            {
                                $photo->filename = $filename;
                                $photo->save();

                                chmod($newFile, 0644);
                                //echo 'next is try<br>';
                               try{

                                    //echo "try";
                                    $img = new SimpleImage($newFile);


                                    //ресайзим
                                    switch ($size)
                                    {
                                        case 'normal':
                                            $img->best_fit($X_SIZE, $Y_SIZE);
                                            break;
                                        case 'small':
                                            //$img->thumbnail(360,null);
                                            $img->fit_to_width(360);
                                            //$img->fitToWidth(360, null);
                                            break;
                                    }
                                    $img->save();
                                }
                                catch (Exception $e)
                                {
                                    //echo 'error'.$e;
                                    @unlink($newFile);
                                    $success = false;
                                }
                            }
                        }
                        }

                        if ($crop_type == 'preview')
                        {
                        $filename = uniqid().'_'.time() . '.' . $extension_name;
                        $success = false;

                        //define('UPLOAD_DIR', 'upload/photos/');
                    	$img = $_POST['canvas'];
                    	$img = str_replace('data:image/png;base64,', '', $img);
                    	$img = str_replace('data:image/jpeg;base64,', '', $img);
                    	$img = str_replace('data:image/gif;base64,', '', $img);
                    	$img = str_replace(' ', '+', $img);
                    	$data = base64_decode($img);

                        $path = Yii::getPathOfAlias('webroot').'/upload/photos/';

                            $s = 's_';
                            $newFile = $path . $s.$filename;

                        	$success = file_put_contents($newFile, $data);
                            rename($path.$photo->filename,$path.$filename);
                            if ($success)
                            {
                                $photo->filename = $filename;
                                $photo->save();

                                chmod($newFile, 0644);
                                //echo 'next is try<br>';
                               try{
                                    $img = new SimpleImage($newFile);
                                    $img->fit_to_width(360);
                                    $img->save();
                                }
                                catch (Exception $e)
                                {
                                    //echo 'error'.$e;
                                    @unlink($newFile);
                                    $success = false;
                                }
                            }

                        }
                    }
                }
            }
        }
        }

        echo json_encode(true);
    }



}
