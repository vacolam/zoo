<?php
Yii::import('application.controllers.backend.PageController');

/**
 * Управление разделом "Контакты"
 */
class RaspisanieController extends PageController
{

    public $razdel_parent = 'home';   

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }

    protected function getPageId()
    {
        return 'raspisanie';
    }

    public function actionIndex()
    {

        if (isset($_POST['COMPANY_ENG'])){
            foreach($_POST['COMPANY_ENG'] as $index => $value)
            {
                $t = $this->getModel($index);
                if ($t)
                {
                    $t->content_eng = $value;
                    $t->save();
                }
            }
        }

        if (isset($_POST['COMPANY'])){
            foreach($_POST['COMPANY'] as $index => $value)
            {
                $t = $this->getModel($index);
                if ($t)
                {
                    $t->content = $value;
                    $t->save();
                }
            }

            Yii::app()->user->setFlash('success', "Изменения сохранены");
            $this->redirect(array('raspisanie/index'));
        }

        $raspisanie = Page::getRaspisanie();
        $raspisanie_eng = Page::getEngRaspisanie();
        $tickets = Page::getTickets();


        $this->render('index', array(
            'raspisanie' => $raspisanie,
            'raspisanie_eng' => $raspisanie_eng,
            'tickets' => $tickets,
        ));


    }
}
