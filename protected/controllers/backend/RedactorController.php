<?php

/**
 * Загрузка картинок
 */
class RedactorController extends BackEndController
{

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }
    
    public function actionUploadImages()
    {
        // This is a simplified example, which doesn't cover security of uploaded images.
        // This example just demonstrate the logic behind the process.

        // files storage folder
        $dir = '/upload/redactor/';
        $files = array();
        $types = array('image/png', 'image/jpg', 'image/gif', 'image/jpeg', 'image/pjpeg');

        if (isset($_FILES['file']))
        {
            foreach ($_FILES['file']['name'] as $key => $name)
            {
                $type = strtolower($_FILES['file']['type'][$key]);
                if (in_array($type, $types))
                {
                    // setting file's mysterious name
                    $ext = pathinfo($name, PATHINFO_EXTENSION);
                    $id = uniqid();
                    $filename = $id.'.'.$ext;
                    $path = $dir.$filename;

                    // copying
                    move_uploaded_file($_FILES['file']['tmp_name'][$key],  Yii::getPathOfAlias('webroot').$path);


                    //Меняем размер изображения
                    chmod(Yii::getPathOfAlias('webroot').$path, 0644);
                                //echo 'next is try<br>';
                    try
                    {
                        //echo "try";
                        $img = new SimpleImage(Yii::getPathOfAlias('webroot').$path);
                        $size = 'normal';
                        $img->best_fit(1200, 1000);                    //ресайзим
             			$img->save();
                    }
                    catch (Exception $e)
                    {

                    }

                    $files['file-'.$key] = array(
                        'url' => $path, 'id' => $id
                    );
                }
            }
        }

        echo stripslashes(json_encode($files));
    }

    public function actionUploadFiles()
    {
        $files = array();
        if (isset($_FILES['file']))
        {
            foreach ($_FILES['file']['name'] as $key => $name)
            {
                $dir = '/upload/redactor/';
                // setting file's mysterious name
                $ext = pathinfo($name, PATHINFO_EXTENSION);
                $id = uniqid();
                $filename = $id.'.'.$ext;
                $path = $dir.$filename;

                move_uploaded_file($_FILES['file']['tmp_name'][$key], Yii::getPathOfAlias('webroot').$path);

                $files['file-'.$key] = array(
                    'url' => $path,
                    'name' => $name,
                    'id' => $id
                );
            }
        }

        echo stripslashes(json_encode($files));

    }
}