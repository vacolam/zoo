<?php

class RewardsController extends BackEndController
{

    public $razdel_id = 49;


    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index','update','preview','create','delete','filesadd','deletephotos','deletedocs','addtag','deletetag','setcover','updatedocname'),
                'roles'=>array('admin','manager'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {

        $cats = RewardsCats::model()->findAll(array('condition'=>'','order'=>'number asc,id asc'));

        $this->render('index', array(
            'cats' => $cats,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);

        $this->saveFromPost($model);

        $this->render('form', array(
            'model' => $model,
        ));

    }


    public function actionCreate()
    {
        $model = new RewardsCats();
        $this->saveFromPost($model);

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionUpdateDocName($id)
    {
        $model = Docs::model()->findByPk($id);
        if ($model){
            if (isset($_POST['name'])){
            $model->filetitle = $_POST['name'];
            $model->save();
            }
        }
    }
    
    private function saveFromPost(RewardsCats $model)
    {
        $is_new =  $model->isNewRecord;
        if (isset($_POST['RewardsCats'])) {
            $model->attributes = $_POST['RewardsCats'];
            $model->save();

            $this->redirect(array('rewards/index'));
        }
    }
    

    protected function getModel($id)
    {
        $model = RewardsCats::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    public function actionDelete($id)
    {

        $model = $this->getModel($id);
        $photos = $model->photos;
        if ($photos)
        {
            foreach($photos as $photo_model){
                Photos::deleteImage($photo_model);
            }
        }

        $model->delete();

        $this->redirect(array('index'));
    }

    public function actionFilesAdd($post_id)
    {
        $post_item = RewardsCats::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $news_photos = new Photos;
                $news_photos->post_id = $post_id;
                $news_photos->date = new CDbExpression('NOW()');
                $news_photos->filename = $uploaded_file;
                $news_photos->type = 'rewards';
                $news_photos->save();
            }else
            {

            }
        }
        }
    }

    public function actionDeletePhotos($id){
        $result = false;
        $photo = Photos::model()->findByPk($id);
        if ($photo)
        {
            $post_id = $photo->post_id;
            Photos::deleteImage($photo);
         }
        echo CJSON::encode($result);
    }



}