<?php

class SearchController extends BackEndController
{


    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',   
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Подписка на рассылку
     */
    public function actionIndex($search_text='')
    {
        /*
        exposure
        exposure_items

        */
        $news = '';
        $afisha = '';
        $pages = '';
        $conf = '';
        $exposure = '';
        $exposure_items = '';
        if ($search_text != ''){
        if (mb_strlen($search_text) >= 0){
            $query = "SELECT * FROM {{news}} WHERE title like :t OR text like :txt OR id = :id ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%', "id"=>$search_text);
            $news = Yii::app()->db->createCommand($query)->query($params)->readAll();

            $query = "SELECT * FROM {{afisha}} WHERE title like :t OR text like :txt OR id = :id ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%', "id"=>$search_text);
            $afisha = Yii::app()->db->createCommand($query)->query($params)->readAll();

            $query = "SELECT * FROM {{cats}} WHERE (cats_id > 0 and plugin = '' and name like :t OR text like :txt)  OR (name like :t) ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%');
            $pages = Yii::app()->db->createCommand($query)->query($params)->readAll();

            $query = "SELECT * FROM {{conf}} WHERE title like :t OR text like :txt OR id = :id ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%', "id"=>$search_text);
            $conf = Yii::app()->db->createCommand($query)->query($params)->readAll();

            $query = "SELECT * FROM {{exposure}} WHERE title like :t OR text like :txt OR id = :id ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%', "id"=>$search_text);
            $exposure = Yii::app()->db->createCommand($query)->query($params)->readAll();

            $query = "SELECT * FROM {{exposure_item}} WHERE title like :t OR text like :txt  OR id = :id ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%', "id"=>$search_text);
            $exposure_items = Yii::app()->db->createCommand($query)->query($params)->readAll();
        }
        }

        $this->render('index', array(
            'news'=>$news,
            'afisha'=>$afisha,
            'pages'=>$pages,
            'conf'=>$conf,
            'exposure'=>$exposure,
            'exposure_items'=>$exposure_items,
        ));

    }


}