<?php

class ShopController extends BackEndController
{

    public $cats_list   = array();
    public $type        = 'shop';
    public $razdel_id = 10;

    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('productsearch','ordersearch','orders','order','orderdelete','index','update','preview','create','delete','filesadd','deletephotos','deletedocs','list','setcover','cats','catsupdate','catscreate','catsdelete',),
                'roles'=>array('admin','manager'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    protected function beforeAction($action)
    {
        parent::beforeAction($action);

        //User::openAllowedShop();

        return true;
    }


    public function actionIndex($cats_id = 0)
    {
        $current_cat = false;
        if ((int)$cats_id > 0){
            $current_cat = ShopCats::model()->findByPk((int)$cats_id);
        }

        $submenu = ShopCats::getSubmenu();

        if (User::openAllowedShop() == true)
        {

            $condition = 'deleted=0';
            if ($current_cat)
            {
                $condition_array = array();
                $condition_array[] = $cats_id;
                $sub_cats = ShopCats::model()->findAll(array('condition'=>'parent_id='.$cats_id,'order'=>'id asc, parent_id asc'));
                if ($sub_cats)
                {
                    foreach($sub_cats as $sub){
                        $condition_array[] = $sub->id;
                    }
                }
                $condition = 'deleted=0 AND cats_id IN ('.implode($condition_array,',').') ';
            }else{
                $AllowedShopArray = User::getAllowedShop();
                if (count($AllowedShopArray)>0)
                {
                    $condition = 'cats_id IN ('.implode(',',$AllowedShopArray).') and deleted = 0';
                }


            }


            $t = false;
            if (isset($_POST))
            {
                if (isset($_POST['s']))
                {
                if ($_POST['s'] != '')
                {
                    $t = $_POST['s'];
                    $condition = '(id="'.$t.'" OR title like "%'.$t.'%") AND deleted=0';

                    $AllowedShopArray = User::getAllowedShop();
                    if (count($AllowedShopArray)>0)
                    {
                        $condition = $condition.' AND cats_id IN ('.implode(',',$AllowedShopArray).')';
                    }
                }
                }
            }

            //echo Yii::app()->user->role;
            $dataProvider = new CActiveDataProvider('ShopItems', array(
                'criteria' => array(
                    'condition'=>$condition,
                    'order' => 'id DESC',
                ),
                'pagination' => array(
                    'pageSize' => 50,
                ),
            ));


            $cats = ShopCats::model()->findAll(array('order'=>'id asc, parent_id asc'));
            $catslist= CHtml::listData($cats,'id','title');



            $this->render('index', array(
                'provider' => $dataProvider,
                'cats_id' => $cats_id,
                'current_cat' => $current_cat,
                'submenu' => $submenu,
                'catslist' => $catslist,
                't' => $t,
            ));

        }else{
            $this->render('cats_error', array(
                'cats_id' => $cats_id,
                'current_cat' => $current_cat,
                'submenu' => $submenu,
            ));

        }

    }


    /////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////


    public function actionCatsUpdate($id)
    {
        $model = $this->getCatsModel($id);
        $allowed_cats      =   User::getAllowedShop();
        $submenu = ShopCats::getSubmenu();

        if (in_array($model->id,$allowed_cats))
        {
            $this->saveFromPostCats($model);

            //Формируем массив-дерево категорий и подкатегорий
            $catslist = ShopCats::getArray();



            $this->render('catsform', array(
                'model' => $model,
                'catslist' => $catslist,
                'submenu' => $submenu,
                'cats_id' => $id,
            ));
        }else{
            $this->render('cats_error', array(
                'cats_id' => $id,
                'current_cat' => $model,
                'submenu' => $submenu,
            ));

        }
    }


    public function actionCatsCreate($cats_id=0)
    {
        $model = new ShopCats();
        $this->saveFromPostCats($model);

        //Формируем массив-дерево категорий и подкатегорий
        $catslist = ShopCats::getArray();

        $submenu = ShopCats::getSubmenu();

        $this->render('catsform', array(
            'model' => $model,
            'catslist' => $catslist,
            'cats_id' => $cats_id,
            'submenu' => $submenu,
        ));
    }

    private function saveFromPostCats(ShopCats $model, $action = 'catsupdate')
    {
        if (isset($_POST['ShopCats'])) {

            $model->attributes = $_POST['ShopCats'];
            $isNew = $model->isNewRecord;

            if ($model->save())
            {
                Yii::app()->user->setFlash('success',
                    $isNew ? 'Категория добавлена' : 'Изменения сохранены'
                );

                if ($action == 'catsupdate')
                {
                $this->redirect(array('catsupdate','id'=>$model->id));
                }
            }
        }
    }

    public function actionCatsDelete($id)
    {
        $allowed_cats      =   User::getAllowedShop();
        if (in_array($id,$allowed_cats))
        {
            $model = $this->getCatsModel($id);
            $model->delete();
        }
        $this->redirect(array('index'));
    }

    protected function getCatsModel($id)
    {
        $model = ShopCats::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }



    public function actionOrders($status='50000',$y=0,$m=0,$start_date='',$end_date='')
    {

        $filter_condition = array();
        $filter_condition[] = 'deleted=0';

        $allowedOrders = ShopOrders::getAllowedOrdersIds();

        if (count($allowedOrders)>0)
        {
            $allowedOrders = array_unique($allowedOrders);
            $filter_condition[] = 'id IN ('.implode(',',$allowedOrders).')';
        }


        $t = false;
        if (isset($_POST))
        {
            if (isset($_POST['s']))
            {
            if ($_POST['s'] != '')
            {
                $t = $_POST['s'];
                $filter_condition[] = '(id="'.$t.'" OR name like "%'.$t.'%"  OR phone like "%'.$t.'%" OR adress like "%'.$t.'%")';
            }
            }
        }


        if (isset($_POST['status']))
        {
        $status = $_POST['status'];
        }
        if ((int)$status <> 50000)
        {
            $filter_condition[] = 'status="'.$status.'"';
        }


        $current_start_date = '';
        if (isset($_POST['start_date'])){
            $start_date=$_POST['start_date'];
        }
        if ($start_date != ''){
            $tw = explode('.',$start_date);
            if (is_array($tw) == true){
                if (count($tw) > 2){
                    $current_start_date = $tw[2].'-'.$tw[1].'-'.$tw[0];
                }
            }
        }

        $current_end_date = '';
        if (isset($_POST['end_date'])){
            $end_date=$_POST['end_date'];
        }
        if ($end_date != ''){
            $tw = explode('.',$end_date);
            if (is_array($tw) == true){
                if (count($tw) > 2){
                    $current_end_date = $tw[2].'-'.$tw[1].'-'.$tw[0];
                }
            }
        }

        if ($current_start_date != '' AND $current_end_date == '')
        {
        $filter_condition[] = 'date="'.$current_start_date.'"';
        }

        if ($current_start_date != '' AND $current_end_date != '')
        {
        $filter_condition[] = 'date>="'.$current_start_date.'" AND date<="'.$current_end_date.'"';
        }




        if ((int)$m <> 0)
        {
            $filter_condition[] = 'MONTH(date)="'.$m.'"';
        }

        if ((int)$y <> 0){
            $filter_condition[] = 'YEAR(date)="'.$y.'"';
        }

        $condition = '';
        if (count($filter_condition) > 0){
            $condition = implode(' AND ',$filter_condition);
        }



        $dataProvider = new CActiveDataProvider('ShopOrders', array(
            'criteria' => array(
                'condition' => $condition,
                'order' => 'status ASC, id DESC',
            ),
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
        $this->render('orders', array(
            'dataProvider' => $dataProvider,
            't' => $t,
            'status' => $status,
            'm' => $m,
            'y' => $y,
            'start_date' => $start_date,
            'end_date' => $end_date,
        ));
    }

    public function actionOrder($id = 0)
    {
        $data = ShopOrders::model()->findByPk($id);

        $this->saveOrder($data);

        $this->render('order', array(
            'data' => $data,
        ));
    }


    private function saveOrder(ShopOrders $model)
    {
        if (isset($_POST['ShopOrders'])) {

            if ($model->status != $_POST['ShopOrders']['status'])
            {
                $this->notification($model,$_POST['ShopOrders']['status']);
            }

            $model->attributes = $_POST['ShopOrders'];
            if ($model->save())
            {
                if (isset($_POST['ShopOrdersitem']))
                {
                    if (isset($_POST['ShopOrdersitem']))
                    {
                        foreach($_POST['ShopOrdersitem'] as $index => $ar)
                        {
                            $data = ShopOrdersitems::model()->findByPk($index);
                            $data->price    = $ar['price'];
                            $data->quontity = $ar['quontity'];
                            $data->deleted  = $ar['deleted'];
                            $data->save();
                        }
                    }

                }

                Yii::app()->user->setFlash('success',
                    'Изменения сохранены'
                );


            }
            $this->redirect(array('shop/order','id'=>$model->id));
        }
    }

    private function notification(ShopOrders $model, $new_status)
    {

        //Отправляем юзеру смс
        ShopSms::addSms($model, $new_status);
    }


    public function actionOrderDelete($id)
    {
        $order = ShopOrders::model()->findByPk($id);
        $order->deleted = 1;
        $order->status = 10000;
        $order->save();

        $this->redirect(array('shop/orders'));
    }

    public function actionProductSearch()
    {
        if (isset($_POST['s']))
        {
            if ($_POST['s'] != '')
            {

            }
        }

        $this->redirect(array('shop/orders'));
    }


}
