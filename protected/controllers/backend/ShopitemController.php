<?php

class ShopitemController extends BackEndController
{

    public $cats_list = array();
    public $type        = 'shop';
    public $razdel_id = 10;

    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index','update','preview','create','delete','filesadd','deletephotos','deletedocs','addtag','deletetag','setcover','copy'),
                'roles'=>array('admin','manager'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }




    public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        if (User::openAllowedProduct() == true)
        {
            $this->saveFromPost($model);

            $photos = new CActiveDataProvider('Photos', array(
                'criteria' => array(
                    'condition' => 'type = "shop" And post_id = "'.$id.'"',
                    'order' => 'date, id',
                ),
                'pagination' => array(
                    'pageSize' => 500,
                ),
            ));

            $docs = new CActiveDataProvider('Docs', array(
                'criteria' => array(
                    'condition' => 'type = "shop" And post_id = "'.$id.'"',
                    'order' => 'date, id',
                ),
                'pagination' => array(
                    'pageSize' => 500,
                ),
            ));

            $catslist = ShopCats::getArray();

            $this->render('form', array(
                'model' => $model,
                'photos' => $photos,
                'docs' => $docs,
                'catslist' => $catslist,
                'cats_id' => $model->cats_id,
            ));

        }else{
            $this->render('error', array(
                'model' => $model,
                'cats_id' => $model->cats_id,
            ));
        }

    }


    public function actionCreate($cats_id = 0)
    {

            $model = new ShopItems();
            $this->saveFromPost($model);

            $catslist = ShopCats::getArray();

            $this->render('form', array(
                'model' => $model,
                'catslist' => $catslist,
                'cats_id' => (int)$cats_id,
            ));

    }

    private function saveFromPost(ShopItems $model, $action = 'update')
    {
        if (isset($_POST['ShopItems'])) {
            $model->attributes = $_POST['ShopItems'];

            if ($model->short_text == ''){
               $model->short_text = trim(strip_tags($this->crop_str_word($model->text, 30 )));
            }

            $model->hidden = 0;
            if ( isset($_POST['ShopItems']['hidden']) ){
            $model->hidden = $_POST['ShopItems']['hidden'];
            }

            $isNew = $model->isNewRecord;

            if ($model->save()) {

                Yii::app()->user->setFlash('success',
                    $isNew ? 'Товар добавлен' : 'Изменения сохранены'
                );

                //$this->redirect(array('index'));
                if ($action == 'update')
                {
                $this->redirect(array('update','id'=>$model->id));
                }
            }
        }
    }

    public function actionDelete($id)
    {

        /*
        $model = $this->getModel($id);
        $photos = $model->photos;
        if ($photos)
        {
            foreach($photos as $photo_model){
                Photos::deleteImage($photo_model);
            }
        }


        $docs = $model->docs;
        if ($docs)
        {
            foreach($docs as $doc_model){
                Docs::deleteDoc($doc_model);
            }
        }

        $model->delete();
        */

        $model = $this->getModel($id);
        $AllowedShopArray = User::getAllowedShop();
        if (in_array($model->cats_id,$AllowedShopArray))
        {
        $model->deleted = 1;
        $model->save();
        }

        $this->redirect(array('shop/index'));
    }

    protected function getModel($id)
    {
        $model = ShopItems::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    public function actionFilesAdd($post_id)
    {
        $post_item = ShopItems::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $news_photos = new Photos;
                $news_photos->post_id = $post_id;
                $news_photos->date = new CDbExpression('NOW()');
                $news_photos->filename = $uploaded_file;
                $news_photos->type = 'shop';
                $news_photos->save();

                if ($post_item->cover_id == 0 || $post_item->cover_id == '')
                {
                    $post_item->cover_id = $news_photos->id;
                    $post_item->save();
                }
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $news_docs = new Docs;
                $news_docs->post_id = $post_id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $uploaded_file;
                $news_docs->filetitle = $file->getName();
                $news_docs->type = 'shop';
                $news_docs->save();
            }
        }
        }
    }

    public function actionDeletePhotos($id){
        $result = false;
        $photo = Photos::model()->findByPk($id);
        if ($photo)
        {
            $post_id = $photo->post_id;
            Photos::deleteImage($photo);

            $post = ShopItems::model()->findByPk($post_id);
            if ($post){
                if ($post->cover_id == $id){
                    $post->cover_id = 0;
                    $post->save();

                    $result = ShopItems::autoCover($post);
                }
            }
        }
        echo CJSON::encode($result);
    }
    public function actionDeleteDocs($id){
        $model = Docs::model()->findByPk($id);
        if ($model)
        {
            $post_id = $model->post_id;
            Docs::deleteDoc($model);
            //$this->redirect(array('update','id'=>$post_id));
        }
    }

    public function actionSetCover($news_id, $cover_id){
        $news_item = ShopItems::model()->findByPk($news_id);
        if ($news_item)
        {
            $news_item->cover_id = $cover_id;
            $news_item->save();

            $new_cover_adress = $news_item->getThumbnailUrl();

            echo json_encode($new_cover_adress);
        }
    }

    public function actionCopy($id){
        $model_old = $this->getModel($id);

        $model_new = new ShopItems();
        $model_new->cats_id = $model_old->cats_id;
        $model_new->title = "(КОПИЯ) ".$model_old->title;
        $model_new->cover_id = $model_old->cover_id;
        $model_new->short_text = $model_old->short_text;
        $model_new->text = $model_old->text;

        $model_new->save();

        $photos_old = $model_old->photos;
        if ($photos_old)
        {
            foreach($photos_old as $photo_model)
            {
                $copy_filename_txt = "c_".$photo_model->filename;
                $copy_filename_stxt = "s_c_".$photo_model->filename;
                $photo_old_file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$photo_model->filename;
                $photo_new_file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$copy_filename_txt;
                if (file_exists($photo_old_file)){
                    $copy_result = copy($photo_old_file, $photo_new_file);
                }

                $s_photo_old_file = Yii::getPathOfAlias('webroot').'/upload/photos/s_'.$photo_model->filename;
                $s_photo_new_file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$copy_filename_stxt;
                if (file_exists($s_photo_old_file)){
                    copy($s_photo_old_file, $s_photo_new_file);
                }

                $news_photos                = new Photos;
                $news_photos->post_id       = $model_new->id;
                $news_photos->date          = new CDbExpression('NOW()');
                $news_photos->filename      = $copy_filename_txt;
                $news_photos->type          = 'shop';
                $news_photos->save();

                //Сохраняем обложку
                if ($model_old->cover_id == $photo_model->id)
                {
                    $model_new->cover_id = $news_photos->id;
                    $model_new->save();
                }

            }
        }

        $docs_old = $model_old->docs;
        if ($docs_old)
        {
            foreach($docs_old as $docs_model)
            {
                $copy_filename_txt = "c_".$docs_model->filename;
                $photo_old_file = Yii::getPathOfAlias('webroot').'/upload/docs/'.$docs_model->filename;
                $photo_new_file = Yii::getPathOfAlias('webroot').'/upload/docs/'.$copy_filename_txt;
                if (file_exists($photo_old_file)){
                    $copy_result = copy($photo_old_file, $photo_new_file);
                }

                $news_docs = new Docs;
                $news_docs->post_id = $model_new->id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $copy_filename_txt;
                $news_docs->filetitle = $docs_model->filetitle;
                $news_docs->type = 'shop';
                $news_docs->save();
            }
        }

        Yii::app()->user->setFlash('success',
            'Копирование завершено'
        );

        $this->redirect(array('update','id'=>$model_new->id));

    }

}
