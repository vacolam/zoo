<?php

class SiteController extends BackEndController
{


    /**
     * Выводит страницу с ошибкой
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error)
        {
            $this->render('error', $error);
        }
    }
    
    public function actionLogin()
    {
        $result = array();
        $error = false;
        $location = '';

        if (isset($_POST['RegistrationForm'])) {

        //Проверяем наличие пары почта-токен и время жизни токена
        if (isset($_POST['RegistrationForm']['email']) AND isset($_POST['RegistrationForm']['tkn'])) {

            $query = "SELECT mail, token, datetime FROM {{tokens}} WHERE mail = :eml and token = :tkn and (datetime + INTERVAL 2 minute) >= NOW() order by id desc LIMIT 1";
    		$params = array(
                ':eml' => $_POST['RegistrationForm']['email'],
                ':tkn' => $_POST['RegistrationForm']['tkn'],
                );
    		$data = Yii::app()->db->createCommand($query)->queryRow(true, $params);
            if ($data !== false){

                //Если токен найден, то начинаем авторизацию
                if (isset($_POST['RegistrationForm']['email']) AND isset($_POST['RegistrationForm']['password'])) {

                    $identity = new UserIdentity($_POST['RegistrationForm']['email'], $_POST['RegistrationForm']['password']);
                    if ($identity->authenticate()) {
                        Yii::app()->user->login($identity,3600*24*60);
                        //echo 'user_role'.Yii::app()->user->role;
                        //if (Yii::app()->user->role == 'user'){
                        //    $this->redirect(array('conf/jury'));
                        //}else{
                        //$location = Yii::app()->createUrl('dashboard/index');
                        //}
                       $this->redirect(array('dashboard/index'));
                    } else {
                        $error = $this->getErrorMessage($identity->errorCode);
                    }
                }

            }else{
                $error = 'Истекло время ожидания авторизации';
            }
        }
        }

        $this->render('login', array('error' => $error));
    }

    public function actionCheckEmail($email)
    {
        if (Yii::app()->request->isAjaxRequest AND $email != '')
        {

            $adressIsIsset = false; //По умолчанию ставим, что адреса не существует.
            $query = "SELECT email FROM {{users}} WHERE email = :email LIMIT 1";
    		$params = array(':email' => $email);
    		$data = Yii::app()->db->createCommand($query)->queryRow(true, $params);
    		if ($data !== false)
    		{
                //УДАЛЯЕМ ВСЕ СТАРЫЕ ТОКЕНЫ
                $query = 'DELETE FROM {{tokens}} where mail= :eml';
        		$params = array(
                    ':eml' => $email,
                    );
                Yii::app()->db->createCommand($query)->execute($params);

                //СОЗДАЕМ ТОКЕН АВТОРИЗАЦИИ
                $token = $this->getToken();
                $query = "INSERT INTO {{tokens}} (datetime, mail, token)"
                        ." VALUES (NOW(), :m, :t)";
                    $params = array(
                        'm' => $email,
                        't' => $token,
                    );
                Yii::app()->db->createCommand($query)->execute($params);

                $adressIsIsset = $token; //Адрес существует

    		}else{
                $adressIsIsset = false; //Адрес не существует
    		}
            echo CJSON::encode($adressIsIsset);
        }
    }    

    private function getErrorMessage($code)
    {
        switch ($code)
        {
            case UserIdentity::ERROR_USERNAME_INVALID:
                return 'Такой пользователь не существует или не активирован';
            case UserIdentity::ERROR_PASSWORD_INVALID:
                return 'Ошибка: Неправильный пароль';
            default:
                return 'Ошибка: попробуйте войти позже'; //неизвестная ошибка авторизации
        }
    }
    
    public function actionLogout()
    {
        if (!Yii::app()->user->isGuest) {
            Yii::app()->user->logout();
            $this->redirect(array('site/login'));
        }
    }


    public function actionWebPushSubscribe()
    {
        $result = false;
        if (isset($_POST['token'])){
            $result = $_POST['token'];
            if ($result != '' and $result != false){
                    $query = "INSERT INTO {{webpush_users}} (user_id, token_id)"
                        ." VALUES (:user, :token)";
                    $params = array(
                        'user' => Yii::app()->user->id,
                        'token' => $result,
                    );
                    Yii::app()->db->createCommand($query)->execute($params);
            }
        }
    	echo CJSON::encode($result);
    }

    public function actionWebPushUnsubscribe()
    {
        $result = true;
    	echo CJSON::encode($result);

    }


    public function getToken()
    {
        $number = 10;
        $param = 3;
        $arr = array('a','b','c','d','e','f',
        'g','h','i','j','k','l',
        'm','n','o','p','r','s',
        't','u','v','x','y','z',
        'A','B','C','D','E','F',
        'G','H','I','J','K','L',
        'M','N','O','P','R','S',
        'T','U','V','X','Y','Z',
        '1','2','3','4','5','6',
        '7','8','9','0','.',',',
        '(',')','[',']','!','?',
        '&amp;','^','%','@','*','$',
        '&lt;','&gt;','/','|','+','-',
        '{','}','`','~');
        // Генерируем пароль
        $pass = "";
        for($i = 0; $i < $number; $i++)
        {
        if ($param > count($arr)-1)$param=count($arr) - 1;
        if ($param==1) $param=48;
        if ($param==2) $param=58;
        if ($param==3) $param=count($arr) - 1;
        // Вычисляем случайный индекс массива
        $index = rand(0, $param);
        $pass .= $arr[$index];
        }

        $token = md5(microtime() . $pass . time());
        return $token;

    }
}