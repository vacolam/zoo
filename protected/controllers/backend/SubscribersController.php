<?php

class SubscribersController extends BackEndController
{

    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider(Subscriber::class);
        $this->render('index', array(
            'provider' => $dataProvider,
        ));
    }
}