<?php

class YarmarkaController extends BackEndController
{

    public $razdel_id = 41;

    public function accessRules()
    {
        return array(
            // даем доступ только авторизованным
            array(
                'allow',
                'roles'=>array('admin','manager'),
            ),
            // запрещаем все остальное
            array(
                'deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {

        $dataProvider = new CActiveDataProvider('Yarmarka', array(
            'criteria' => array(
                'condition'=>'',
                'order' => 'id desc',
            ),
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));

        $model = Cats::model()->findByPk($this->razdel_id);
        $this->savePage($model);

        $photos = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "pages" And post_id = "'.$this->razdel_id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "pages" And post_id = "'.$this->razdel_id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 200,
            ),
        ));

        $this->render('index', array(
            'model'             => $model,
            'docs'              => $docs,
            'photos'            => $photos,
            'dataProvider'            => $dataProvider,
        ));
    }


    private function savePage(Cats $model)
    {
        if (isset($_POST['Cats'])) {
            $model->attributes = $_POST['Cats'];
            $model->save();
        }
    }
    
    /*
     Просмотр заявки
    */
    public function actionView($id)
    {
        $model = Yarmarka::model()->findByPk($id);

        $this->render('view', array(
            'model'=>$model
        ));
    }

    public function actionStatus($status=0,$id)
    {
        $model = Yarmarka::model()->findByPk($id);
        if ($model){
            $model->status = $status;
            $model->save();
            Yii::app()->user->setFlash('success',
                    'Статус изменен'
            );
        }
        $this->redirect(array('view','id'=>$id));
    }

    public function actionDelete($id)
    {

        $model = Yarmarka::model()->findByPk($id);
        if ($model)
        {
            $model->delete();
        }

        $this->redirect(array('index'));
    }
}