<?php

class AfishaController extends FrontEndController
{

    public function actionIndex($tag = 0, $month = 0)
    {

        $tag_sql = '';
        if ((int)$tag > 0)
        {
            $tag_sql = " AND tags like '%\"".$tag."\"%'";
        }


        $year = date('Y');
        $month = (int)$month;
        $month_condition = '';
        if ($month > 0)
        {
            $month_condition = ' AND ( ("'.$month.'" = MONTH(date) AND end_date ="0000-00-00" AND YEAR(date) = "'.$year.'" ) OR ("'.$month.'" >= MONTH(date) AND "'.$month.'" <= MONTH(end_date)   AND YEAR(date) = "'.$year.'" )  )';
        }


        $dataProvider = new CActiveDataProvider('Afisha', array(
            'criteria' => array(
                'condition'=>'id > 0'.$tag_sql.$month_condition,
                'order' => 'date DESC, id DESC',
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));

        $query = "SELECT * FROM {{tags}} WHERE id > 0 and type = 'afisha' ORDER BY name";
            $params = array();
            $tags = Yii::app()->db->createCommand($query)->query($params)->readAll();

        
        $this->render('index', array(
            'provider' => $dataProvider,
            'tags' => $tags,
            'month' => $month,
        ));
    }
    
    public function actionView($id)
    {
        $model = $this->getModel($id);

        $groups =   Afisha::model()->findAll(array('condition'=>'has_groups = 1'));
        $groups_array = CHtml::listData($groups,'id','title');
        $this->render('view', array(
            'model' => $model,
            'groups_array' => $groups_array,
        ));
    }

    protected function getModel($id)
    {
        $model = Afisha::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }


   /*
     ПРОСМОТР КОНФЫ
    */
    public function actionAddOrder()
    {
        $group = new Groups();
        $group->school_name     = $_POST['school_name'];
        $group->name            = $_POST['name'];
        $group->phone           = $_POST['phone'];
        $group->order_date      = new CDbExpression('NOW()');
        $group->save();


        if (isset($_POST['Orders']))
        {
            foreach($_POST['Orders'] as $index => $order)
            {
                $result = array();
                $afisha = $this->getModel($order['afisha_id']);
                $groupOrders = new GroupsOrders();

                if ($afisha->has_groups == 1)
                {
                    $groupOrders->groups_id       = $group->id;
                    $groupOrders->afisha_id       = $order['afisha_id'];
                    $groupOrders->date            = $order['start_date'];
                    $groupOrders->time_id         = $order['time_id'];
                    $groupOrders->kolichestvo     = $order['kolichestvo'];
                    $groupOrders->vozrast         = $order['vozrast'];
                    $groupOrders->week_day        = $order['week_day'];
                    $groupOrders->bus_need        = $order['bus_need'];
                    $groupOrders->bus_time        = $order['bus_time'];
                    $groupOrders->bus_route       = $order['bus_route'];
                    $groupOrders->order_date      = new CDbExpression('NOW()');

                    if ($groupOrders->time_id != ''){
                        $time = GroupsTime::model()->findByPk($groupOrders->time_id);
                        if ($time){
                           $groupOrders->time_text = $time->time;
                        }
                    }
                    $groupOrders->save();
                }
            }
        }

        Orders::createOrder('groups',$group->id);

        $result['id'] = $group->id;
        echo json_encode($result);


        //$t = json_decode($_POST['form1']);

        //echo $t;

        //echo "<pre>";
        //print_r($t);
        //echo "</pre>";
    }

    public function actionFilesAdd($post_id)
    {
        $post_item = Groups::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $news_photos = new Photos;
                $news_photos->post_id = $post_id;
                $news_photos->date = new CDbExpression('NOW()');
                $news_photos->filename = $uploaded_file;
                $news_photos->type = 'groups_orders';
                $news_photos->save();
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $news_docs = new Docs;
                $news_docs->post_id = $post_id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $uploaded_file;
                $news_docs->filetitle = $file->getName();
                $news_docs->type = 'groups_orders';
                $news_docs->save();
            }
        }
        }
    }

    public function actionGetTimeTable($groups_id, $date)
    {
        $result = false;
        $group = $this->getModel($groups_id);
        if ($group)
        {
            $week_day = $number = date('N', strtotime($date));
            $week_day = $week_day - 1;
            if ($week_day < 0){$week_day = 6;}
            $week_names = array('mn','tu','we','th','fr','sa','su');
            if (isset($week_names[$week_day]))
            {

                $timeTable = GroupsTime::model()->findAll(array('condition'=>'week_day="'.$week_names[$week_day].'" and groups_id="'.$group->id.'" and deleted=0','order'=>'number asc, id asc'));

                if (count($timeTable) > 0)
                {
                    $orders = GroupsOrders::model()->findAll(array('condition'=>'status=1 and groups_id="'.$group->id.'" and date = "'.$date.'"','order'=>''));
                    $ordered_times = array();
                    if ($orders){
                        foreach($orders as $order){
                            $ordered_times[] = $order->time_id;
                        }
                    }
                    if (count($ordered_times)==0){$ordered_times[]=0;}

                    $result = "<select name='GroupsOrders[time_id]' size='1' class='time_id' style='width:185px; padding:0px 10px; height:40px; line-height:40px; border:2px solid #ccc; border-radius:4px;'>";
                    $result .= "<option value='0' selected='selected'>Выберите время</option>";
                    foreach($timeTable as $time)
                    {
                        $r = '';
                        $disabled = '';
                        if (in_array($time->id,$ordered_times)){
                                $r = ' ( Забронировано )';
                                $disabled = 'disabled="disabled"';
                        }
                        $result .= "<option value='".$time->id."' ".$disabled.">".$time->time.$r."</option>";
                    }
                    $result .= "</select>";
                    $result .= "<input type='hidden' name='GroupsOrders[week_day]' class='week_day' value='".$week_names[$week_day]."'>";

                }else{
                    $result = '<div style="width:165px; padding:0px 10px; height:40px; line-height:40px; border-radius:4px; font-size:13px; background:#EA8690; color:#fff;">В этот день нет групп</div>';
                }
            }
        }

        echo json_encode($result);
    }

    public function actionGetAfishaItem()
    {
        $groups =   Afisha::model()->findAll(array('condition'=>'has_groups = 1'));
        $groups_array = CHtml::listData($groups,'id','title');
        $result = $this->renderPartial('_form_item', array(
            'groups_array' => $groups_array,
        ),true);

        echo json_encode($result);
    }
}
