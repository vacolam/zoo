<?php

class ConfController extends FrontEndController
{

    public $razdel_id = 8;

    public $typeList = array(
            'conf' => 'Конференции',
            'festival' => 'Фестивали',
            'konkurs' => 'Конкурсы',
            'opros' => 'Опросы',
        );


    public function accessRules() {
        return array(
        );
    }

    /*
    Список конференций
    */
    public function actionIndex($type = '0')
    {
        $type_sql = '';
        if ($type != '0'){
            $type_sql = 'type="'.$type.'"';
        }
        $dataProvider = new CActiveDataProvider('Conf', array(
            'criteria' => array(
                'condition'=>$type_sql,
                'order' => 'date DESC, id DESC',
            ),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ));

        $this->render('index', array(
            'provider' => $dataProvider,
            'active_type' => $type,
        ));
    }


    /*
     ПРОСМОТР КОНФЫ
    */
    public function actionView($id)
    {
        $model = $this->getConf($id);
        $nominations = $model->nominations;
          $this->render('view', array(
            'model' => $model,
            'nominations' => $nominations,
        ));
    }


    /*
     ПОЛУЧЕНИЕ КОНФЫ
    */
    protected function getConf($id)
    {
        $model = Conf::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    /*
     ПРОСМОТР КОНФЫ
    */
    public function actionAddWork($id)
    {
        $result = array();
        $conf = $this->getConf($id);
        $nominations = $conf->nominations;

        if ($conf->form_is_open == 1){
        $model = new ConfWorks();
        if (isset($_POST['ConfWorks'])) {
            $model->attributes = $_POST['ConfWorks'];
            $model->conf_id =$conf->id;
            $model->date = new CDbExpression('NOW()');
            $model->save();
            $result['id'] = $model->id;

            Orders::createOrder($conf->type,$model->id);
            /*
            $model->image = CUploadedFile::getInstance($model, 'image');

            $isNew = $model->isNewRecord;
            if ($model->save()) {
                if ($model->image) {
                    $file_name = uniqid().'_'.time() . '.' . $model->image->extensionName;
                    $path = Yii::getPathOfAlias('webroot').'/upload/conf/'.$file_name;
                    $model->image->saveAs($path);
                    $image = new SimpleImage($path);
                    $image->best_fit(ConfWorks::WIDTH, ConfWorks::HEIGHT);
                    $image->save($path, 80);

                    $small_path = Yii::getPathOfAlias('webroot').'/upload/conf/s_'.$file_name;
                    $image = new SimpleImage($path);
                    $image->thumbnail(400, 400);
                    $image->save($small_path, 80);


                    $model->filename = $file_name;
                    $model->save();
                }

                Yii::app()->user->setFlash('success',
                    'Работа отправлена'
                );

                $this->redirect(array('conf/view','id'=>$conf->id));
            }
            */
        }
        }
        echo json_encode($result);
    }

    public function actionFilesAdd($post_id)
    {
        $post_item = ConfWorks::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $news_photos = new Photos;
                $news_photos->post_id = $post_id;
                $news_photos->date = new CDbExpression('NOW()');
                $news_photos->filename = $uploaded_file;
                $news_photos->type = 'confwork';
                $news_photos->save();

                if ($post_item->cover_id == 0 || $post_item->cover_id == '')
                {
                    $post_item->cover_id = $news_photos->id;
                    $post_item->save();
                }
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $news_docs = new Docs;
                $news_docs->post_id = $post_id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $uploaded_file;
                $news_docs->filetitle = $file->getName();
                $news_docs->type = 'confwork';
                $news_docs->save();
            }
        }
        }
    }


    /*
    Список конференций
    */
    public function actionWork($id)
    {
        $work = ConfWorks::model()->findByPk($id);
        if ($work)
        {
            $conf = Conf::model()->findByPk($work->conf_id);
            if ($conf->type != 'konkurs'){
                throw new CHttpException(404);
            }else{
                $nom = ConfNominations::model()->findByPk($work->nom_id);
                $this->render('konkurs/work', array(
                    'work' => $work,
                    'conf' => $conf,
                    'nom'  => $nom,
                ));
            }
        }
    }

    /*
     ЕСЛИ ЧЕЛОВЕК ПРАВИЛЬНО ЗАПОЛНИЛ ОПРОС
    */
    public function actionAddOpros($id)
    {
        $result = array();
        $conf = $this->getConf($id);
        $nominations = ConfNominations::model()->findAll(array('condition'=>'conf_id="'.$id.'"'));
        if ($conf->form_is_open == 1){
        if (isset($nominations[0]))
        {
            $nom = $nominations[0];
            $model = new ConfWorks();
            if (isset($_POST['ConfWorks'])) {
                $model->attributes = $_POST['ConfWorks'];
                $model->conf_id =$conf->id;
                $model->nom_id =$nom->id;
                $model->date = new CDbExpression('NOW()');
                $model->save();
                Orders::createOrder('opros',$model->id);
                $result['id'] = $model->id;
            }
        }
        }

        echo json_encode($result);
    }

}