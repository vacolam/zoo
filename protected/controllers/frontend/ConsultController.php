<?php
Yii::import('application.controllers.frontend.PageController');

/**
 * Страница "Контакты"
 */
class ConsultController extends PageController
{
    protected function getPageId()
    {
        return 'consult';
    }
    
    public function actionIndex()
    {
        $consult = $this->getModel('consult');

        $photos = Photos::model()->findAll(array('condition'=>'type = "consult"','order'=>'date, id'));
        $docs = Docs::model()->findAll(array('condition'=>'type = "consult"','order'=>'date, id'));

        $this->render('index', array(
            'consult' => $consult,
            'photos' => $photos,
            'docs' => $docs,
        ));
    }
}