<?php
Yii::import('application.controllers.frontend.PageController');

/**
 * Страница "Контакты"
 */
class ContactsController extends PageController
{
    protected function getPageId()
    {
        return 'contacts';
    }
    
    public function actionIndex()
    {
        $map = $this->getModel('map');
        $contacts = $this->getModel('contacts');
        $this->render('index', array(
            'map' => $map->content,
            'contacts' => $contacts->content,
        ));
    }
}