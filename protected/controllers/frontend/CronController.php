<?php
require Yii::app()->basePath.'/extensions/SmsaeroApiV2/SmsaeroApiV2.php';
use SmsaeroApiV2\SmsaeroApiV2;
/**
 * Все кроны здесь
 */
class CronController extends FrontEndController
{

    /*
	 * Отправляет уведомления для админа в хром об оплатах
	 */

    public function actionSentWebPush(){
        $url = 'https://fcm.googleapis.com/fcm/send';
        $YOUR_API_KEY = 'AAAAb-UNvOI:APA91bG0qX8-Pn-MXBZu4vqftyEZZDOscPke-Bzv2F_xDWClSKyBJw676bfPEA9oRUGmC5Q0zUVmmrfi0Mm1lzOQLmg29v-FbDCOxOL-pHiEqsVcH8qbB18leOp2pNK4po7pnA12UztC'; // Server key
        $YOUR_TOKEN_ID = ''; // Client token id

        $query = "SELECT * FROM {{webpush_messages}} limit 1";
		$data = Yii::app()->db->createCommand($query)->queryAll();

		if (count($data)>0){
        	foreach($data as $d)
			{
            	$message = $d['message'];
            	$title = $d['title'];
            	$user_id = $d['user_id'];
            	$link = $d['link'];
                if ($message != '' and $user_id>0){
                    $query = "SELECT * FROM {{webpush_users}} where user_id = '".$user_id."'";
            		$data2 = Yii::app()->db->createCommand($query)->queryAll();

            		if (count($data2)>0){
                    	foreach($data2 as $d2)
            			{
                            $YOUR_TOKEN_ID = $d2['token_id'];
                            if ($YOUR_TOKEN_ID != '')
                            {
                                //echo $YOUR_TOKEN_ID."<br>";
                                //echo $message."<br>";
                                $request_body = array(
                                    'to' => $YOUR_TOKEN_ID,
                                    'notification' => array(
                                        'title' => $title,
                                        'body' => $message,
                                        'icon' => 'https://sakhalinzoo.ru/logo_100.png',
                                        'click_action' => $link,
                                    ),
                                );
                                $fields = json_encode($request_body);

                                $request_headers = array(
                                    'Content-Type: application/json',
                                    'Authorization: key=' . $YOUR_API_KEY,
                                );

                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                $response = curl_exec($ch);
                                curl_close($ch);

                                //echo $response;
                                //echo "--------------<br><br>";

                            }
                        }
                    }

                    $query = 'DELETE FROM {{webpush_messages}} where id="'.$d['id'].'"';
            		Yii::app()->db->createCommand($query)->execute();
                }
            }
		}
    }

    /*
     * Отправляет уведомления для админа в хром об оплатах
     */

    public function actionSentEmail(){
        $query = "SELECT * FROM {{webpush_emails}} limit 1";
        $data = Yii::app()->db->createCommand($query)->queryAll();

        if (count($data)>0)
        {
        	foreach($data as $d)
            {
            	$message = $d['message'];
            	$title = $d['title'];
            	$user_id = $d['user_id'];
            	$link = $d['link'];
                if ($message != '' and $user_id>0)
                {
                    $query = "SELECT * FROM {{users}} where id = '".$user_id."'";
            		$user = Yii::app()->db->createCommand($query)->queryAll();

            		if (isset($user[0]))
                    {
                                //Отправляем письмо клиенту с информацией о новом статусе
                                $text = $this->renderPartial('//email/mail', array(
                                    'title' => $title,
                                    'link' => $link,
                                    'message' => $message,
                                ), true);

                               	//$mail_connection = Yii::app()->params['smtp'];

                              	//echo "<pre>";
                                //print_r($mail_connection);
                                //echo "</pre>";

                                //echo "email: ".$user[0]['email']."<br>";
                                //echo "$title: ".$title."<br>";
                                //echo "$text: ".$text."<br>";

                                //$smtp = new SmtpMail($mail_connection['host'], $mail_connection['login'], $mail_connection['password'], $mail_connection['port'], false);
																//print_r($smtp);
                                //$smtp->sendMail($user[0]['email'], $title, $text, "", 'sakhalinzoo.ru');
								mail($user[0]['email'], $title, $text, "Content-Type:text/html;charset=utf-8"."\r\n"."From: sakhalinzoo <no-reply@sakhalinzoo.ru>");
                    }

                    $query = 'DELETE FROM {{webpush_emails}} where id="'.$d['id'].'"';
            		Yii::app()->db->createCommand($query)->execute();
                }
            }
        }
    }

    /*
     * Отправляет sms уведомления о статусе заказа
     */

    public function actionSmsSent(){
        $query = "SELECT * FROM {{shop_sms}} where send=0 order by id ASC limit 1";
        $data = Yii::app()->db->createCommand($query)->queryAll();

        if (count($data)>0)
        {
        	foreach($data as $d)
            {
                $ShopSms = ShopSms::model()->findByPk($d['id']);
                if ($ShopSms)
                {
                $ShopSms->send = 1;
                $ShopSms->save();

            	$text = $d['text'];
            	$phone = $d['phone'];

                $smsaero_api = new SmsaeroApiV2('zamdir@sakhalinzoo.ru', '6RDP0zgsv5yRPPCMstmMAqWttbed', 'SMS Aero'); // api_key из личного кабинета
                $response =  $smsaero_api->send($d['phone'],$d['text'],'','https://sakhalinzoo.ru?r=cron/smsstatus'); // Отправка сообщений
                if(isset($response['success']) )
                {
                    if (isset($response['data']['id'])){
                        $ShopSms->sms_id = $response['data']['id'];
                    }

                    if (isset($response['data']['extendStatus'])){
                        $ShopSms->send_status = $response['data']['extendStatus'];
                    }

                    $ShopSms->save();
                }
                }
            }
        }
    }


    public function actionSmsStatus(){
        $post = file_get_contents("php://input");
        if ($post != '')
        {
            parse_str($post,$url_array);

            if (isset($url_array['id']))
            {
                if ($url_array['id'] != '')
                {
                    $ShopSms = ShopSms::model()->findAll(array('condition'=>'sms_id="'.$url_array['id'].'"'));
                    foreach($ShopSms as $index => $sms)
                    {
                        $sms->send_status = $url_array['extendStatus'];
                        $sms->save();
                    }
                }
            }
        }

        header('HTTP/1.0 200 OK');
    }

    /*
     * публикуем закрытые новости
     */

    public function actionPublishNews(){
        $dateTime = date('Y-m-d H:i:s');
        $moscow_time = date("Y-m-d H:i:s", strtotime('+8 hours', strtotime($dateTime)));

        $explode_ar = explode(' ',$moscow_time);
        $date = $explode_ar[0];
        $time = $explode_ar[1];

        echo $date."<br>";
        echo $time."<br>";

        $query = "SELECT is_closed, closed_till, id, closed_till_time FROM {{news}} where is_closed = 2 and closed_till <= '".$date."' and closed_till_time <= '".$time."' ";
        $data = Yii::app()->db->createCommand($query)->queryAll();

        if (count($data)>0)
        {
            foreach($data as $d)
            {
                $news = News::model()->findByPk($d['id']);
                if ($news){
                    $news->is_closed = 0;
                    $news->closed_till = '0000-00-00';
                    $news->closed_till_time = '00:00:00';
                    $news->save();
                }
            }
        }

    }

    /*
     CURL запихали в функцию
    */
    private static function file_get_contents_curl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Устанавливаем параметр, чтобы curl возвращал данные, вместо того, чтобы выводить их в браузер.
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

}
