<?php

class CustodiansController extends FrontEndController
{

    public $razdel_id = 17;

    public function actionIndex()
    {
        $custodians = Partners::model()->findAll(array('condition'=>'type="custodian"'));
        $exposureItems = ExposureItems::model()->findAll(array('condition'=>'','order'=>'title ASC'));
        if ($exposureItems){
            foreach($exposureItems as $item){
                $exposureItemsList[$item->id] = $item->title;
            }
        }

        $model = Cats::model()->findByPk($this->razdel_id);

        if ($model->cats_id >0){
        $submenu = Cats::getSubmenu($model->cats_id);
        }else{
        $submenu = array();
        }


        $this->render('index', array(
            'custodians'        => $custodians,
            'exposureItemsList' => $exposureItemsList,
            'model'             => $model,
            'submenu'             => $submenu,
        ));
    }

}