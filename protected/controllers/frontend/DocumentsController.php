<?php

class DocumentsController extends FrontEndController
{
    public $razdel_id = 40;

    public function actionIndex()
    {
        $model = Cats::getPage($this->razdel_id);
        if ($model->cats_id >0){
        $submenu = Cats::getSubmenu($model->cats_id);
        }else{
        $submenu = array();
        }

        $cats = DocumentsCats::model()->findAll(array('condition'=>'','order'=>'number asc,id asc'));

        $this->render('index', array(
            'cats' => $cats,
            'model' => $model,
            'submenu' => $submenu,
        ));
    }
}