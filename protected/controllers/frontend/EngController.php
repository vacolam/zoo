<?php

class EngController extends FrontEndController
{

    public function actionIndex()
    {    
        $homePageNumber = Eng::getHomePage();
        if ($homePageNumber > 0){
            $this->redirect(array('eng/view', 'id' => $homePageNumber));
        }
        else{
            $this->render('index', array(
            ));
        }
    }
    public function actionView($id)
    {
        $model = Eng::getPage($id);
        $this->render('view', array(
            'model' => $model,
        ));
    }
}
