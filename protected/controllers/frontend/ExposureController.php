<?php

class ExposureController extends FrontEndController
{

    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Exposure', array(
            'criteria' => array(
                'order' => 'id ASC',
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
        $this->render('index', array(
            'provider' => $dataProvider,
        ));
    }
    
    public function actionView($id)
    {
        $model = $this->getModel($id);
        $animals= ExposureItems::getExposureAnimals($id);

        //�������� ������ �������� ����������
        $cats = ExposureCats::model()->findAll(array('order'=>'id asc, parent_id asc'));
        $catslist= CHtml::listData($cats,'id','title');

        $this->render('view', array(
            'model' => $model,
            'animals' => $animals,
            'catslist' => $catslist,
        ));
    }

    public function actionAnimal($id)
    {
        $animal = ExposureItems::model()->findByPk($id);
        if ($animal){
            $exposure = Exposure::model()->findByPk($animal->exposure_id);

            $this->render('animal', array(
            'model' => $animal,
            'exposure' => $exposure,
        ));
        }
   }

    protected function getModel($id)
    {
        $model = Exposure::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}
