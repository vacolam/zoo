<?php

class GroupsController extends FrontEndController
{

    public $razdel_id = 51;

    public function actionIndex($tag = 0)
    {
        $tag_sql = '';
        if ((int)$tag > 0)
        {
            $tag_sql = "tags like '%\"".$tag."\"%'";
        }

        $dataProvider = new CActiveDataProvider('Groups', array(
            'criteria' => array(
                'condition'=>$tag_sql,
                'order' => 'date DESC, id DESC',
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));

        $model = Cats::model()->findByPk($this->razdel_id);

        if ($model->cats_id >0){
        $submenu = Cats::getSubmenu($model->cats_id);
        }else{
        $submenu = array();
        }

        $this->render('index', array(
            'provider' => $dataProvider,
            'submenu' => $submenu,
        ));
    }
    
    public function actionView($id)
    {

        $cats = Cats::model()->findByPk($this->razdel_id);

        if ($cats->cats_id >0){
        $submenu = Cats::getSubmenu($cats->cats_id);
        }else{
        $submenu = array();
        }

        $model = $this->getModel($id);
        $this->render('view', array(
            'model' => $model,
            'submenu' => $submenu,
        ));
    }

    protected function getModel($id)
    {
        $model = Groups::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

   /*
     ПРОСМОТР КОНФЫ
    */
    public function actionAddOrder($id)
    {
        $result = array();
        $group = $this->getModel($id);

        $model = new GroupsOrders();
        if (isset($_POST['GroupsOrders'])) {
            $model->attributes = $_POST['GroupsOrders'];
            $model->groups_id = $id;
            $model->order_date = new CDbExpression('NOW()');

            if ($model->time_id != ''){
                $time = GroupsTime::model()->findByPk($model->time_id);
                if ($time){
                   $model->time_text = $time->time;
                }
            }
            $model->save();

            Orders::createOrder('groups',$model->id);

            $result['id'] = $model->id;
        }

        echo json_encode($result);
    }

    public function actionFilesAdd($post_id)
    {
        $post_item = GroupsOrders::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $news_photos = new Photos;
                $news_photos->post_id = $post_id;
                $news_photos->date = new CDbExpression('NOW()');
                $news_photos->filename = $uploaded_file;
                $news_photos->type = 'groups_orders';
                $news_photos->save();
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $news_docs = new Docs;
                $news_docs->post_id = $post_id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $uploaded_file;
                $news_docs->filetitle = $file->getName();
                $news_docs->type = 'groups_orders';
                $news_docs->save();
            }
        }
        }
    }

    public function actionGetTimeTable($groups_id, $date)
    {
        $result = false;
        $group = $this->getModel($groups_id);
        if ($group)
        {
            $week_day = $number = date('N', strtotime($date));
            $week_day = $week_day - 1;
            if ($week_day < 0){$week_day = 6;}
            $week_names = array('mn','tu','we','th','fr','sa','su');
            if (isset($week_names[$week_day]))
            {
                $timeTable = GroupsTime::model()->findAll(array('condition'=>'week_day="'.$week_names[$week_day].'" and groups_id="'.$group->id.'" and deleted=0','order'=>'number asc, id asc'));

                if (count($timeTable) > 0)
                {
                    $orders = GroupsOrders::model()->findAll(array('condition'=>'status=1 and groups_id="'.$group->id.'" and date = "'.$date.'"','order'=>''));
                    $ordered_times = array();
                    if ($orders){
                        foreach($orders as $order){
                            $ordered_times[] = $order->time_id;
                        }
                    }
                    if (count($ordered_times)==0){$ordered_times[]=0;}



                    $result = "<select name='GroupsOrders[time_id]' size='1' class='time_id' style='width:310px; padding:0px 10px; height:40px; line-height:40px; border:2px solid #ccc; border-radius:4px;'>";
                    $result .= "<option value='0' selected='selected'>Выберите время</option>";
                    foreach($timeTable as $time)
                    {
                        $r = '';
                        $disabled = '';
                        if (in_array($time->id,$ordered_times)){
                                $r = ' ( Забронировано )';
                                $disabled = 'disabled="disabled"';
                        }
                        $result .= "<option value='".$time->id."' ".$disabled.">".$time->time.$r."</option>";
                    }
                    $result .= "</select>";
                    $result .= "<input type='hidden' name='GroupsOrders[week_day]' value='".$week_names[$week_day]."'>";
                }else{
                    $result = '<div style="width:290px; padding:0px 10px; height:40px; line-height:40px; border-radius:4px; font-size:13px; background:#EA8690; color:#fff;">В этот день нет групп</div>';
                }
            }
        }

        echo json_encode($result);
    }

}
