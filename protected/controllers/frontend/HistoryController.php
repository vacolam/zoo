<?php

class HistoryController extends FrontEndController
{

    public $razdel_id = 38;

    public function actionIndex()
    {

        $model = Cats::model()->findByPk($this->razdel_id);

        if ($model->cats_id >0){
        $submenu = Cats::getSubmenu($model->cats_id);
        }else{
        $submenu = array();
        }

        $charts = Charts::model()->findAll();


        $this->render('index', array(
            'model'             => $model,
            'submenu'             => $submenu,
            'charts'             => $charts,
        ));
    }



}