<?php
class HomeController extends FrontEndController
{
    public function actionIndex()
    {

        $banners = Banner::model()->findAll();

        $this->render('index', array(
            'banners' => $banners,
        ));
    }
}