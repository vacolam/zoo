<?php

class NewsController extends FrontEndController
{

    public function actionIndex($tag = 0)
    {
        $tag_sql = '';
        if ((int)$tag > 0)
        {
            $tag_sql = " and tags like '%\"".$tag."\"%'";
        }

        $dataProvider = new CActiveDataProvider('News', array(
            'criteria' => array(
                'condition'=>'is_closed = 0'.$tag_sql,
                'order' => 'date DESC, id DESC',
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
        $this->render('index', array(
            'provider' => $dataProvider,
        ));
    }

    public function actionView($id)
    {
        $model = $this->getModel($id);
        if ($model->is_closed == 0)
        {
            $this->render('view', array(
                'model' => $model,
            ));
        }else{
            throw new CHttpException(404);
        }
    }

    protected function getModel($id)
    {
        $model = News::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}
