<?php

class OcenkaController extends FrontEndController
{

    public $razdel_id = 43;

    public function actionIndex()
    {
        $model = Cats::model()->findByPk($this->razdel_id);

        if ($model->cats_id >0){
        $submenu = Cats::getSubmenu($model->cats_id);
        }else{
        $submenu = array();
        }


        $this->render('index', array(
            'model'             => $model,
            'submenu'             => $submenu,
        ));
    }

    public function actionVote()
    {
      if (isset($_POST['Ocenka'])){

            $ocenka = $_POST['Ocenka'];
            $model = new Ocenka();
            $model->date = new CDbExpression('NOW()');
            if (isset($ocenka['name'])){$model->name = $ocenka['name'];}
            if (isset($ocenka['phone'])){$model->phone = $ocenka['phone'];}
            if (isset($ocenka['city'])){$model->city = $ocenka['city'];}
            if (isset($ocenka['email'])){$model->email = $ocenka['email'];}

            $model->result = json_encode($ocenka);
            $model->save();

            Orders::createOrder('ocenka',$model->id);            

            $result = true;
            echo json_encode($result);
      }
    }

}