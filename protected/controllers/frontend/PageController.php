<?php


abstract class PageController extends FrontEndController
{
    
    abstract protected function getPageId();
    
    /**
     * Страница раздела
     */
    public function actionIndex()
    {
        $page = $this->getModel($this->getPageId());
        $text = $page->content;
        $this->render('//pages/text', array('text' => $text));
    }

    /**
     * Возвращает модель страницы
     */
    protected function getModel($id)
    {
        $page = Page::model()->findByPk($id);
        if ($page === null) {
            throw new CHttpException(404);
        }
        return $page;
    }
}