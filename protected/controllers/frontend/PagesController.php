<?php

class PagesController extends FrontEndController
{

    public function actionIndex()
    {

    }
    
    public function actionView($id)
    {
        $model = Cats::getPage($id);
        if (!$model){
            $this->redirect(array('home/index'));
        }

        if ($model->link != '')
        {
            $this->redirect(array($model->link));
        }
        else
        {
            if ($model->cats_id >0){
            $submenu = Cats::getSubmenu($model->cats_id);
            }else{
            $submenu = array();
            }
            if ($model)
            {
                $this->render('view', array(
                    'model' => $model,
                    'submenu' => $submenu,
                ));
            }
        }
    }
}
