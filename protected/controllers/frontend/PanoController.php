<?php

class PanoController extends FrontEndController
{

    public $razdel_id = 63;

    public function actionIndex()
    {
        $model = Cats::model()->findByPk($this->razdel_id);

        if ($model->cats_id >0){
        $submenu = Cats::getSubmenu($model->cats_id);
        }else{
        $submenu = array();
        }


        $this->render('index', array(
            'model'             => $model,
            'submenu'             => $submenu,
        ));
    }
}