<?php

class PartnersController extends FrontEndController
{

    public $razdel_id = 44;

    public function actionIndex()
    {
        $partners = Partners::model()->findAll(array('condition'=>'type="partner"'));
        $custodians = Partners::model()->findAll(array('condition'=>'type="custodian"'));
        $exposureItems = ExposureItems::model()->findAll(array('condition'=>'','order'=>'title ASC'));
        if ($exposureItems){
            foreach($exposureItems as $item){
                $exposureItemsList[$item->id] = $item->title;
            }
        }

        $model = Cats::model()->findByPk($this->razdel_id);

        if ($model->cats_id >0){
        $submenu = Cats::getSubmenu($model->cats_id);
        }else{
        $submenu = array();
        }


        $this->render('index', array(
            'custodians'        => $custodians,
            'partners'          => $partners,
            'exposureItemsList' => $exposureItemsList,
            'model'             => $model,
            'submenu'           => $submenu,
        ));
    }

}