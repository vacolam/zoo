<?php

class ShopController extends FrontEndController
{

    public function actionIndex()
    {
        $submenu = ShopCats::getSubmenu();
        //Получаем массив названий экспозиций
        $cats = ShopCats::model()->findAll(array('order'=>'id asc, parent_id asc', 'limit'=>'50'));

        $items_array = array();
        if ($cats)
        {
            foreach($cats as $index => $cat)
            {
                $items = ShopItems::model()->findAll(array('condition'=>'hidden=0 AND deleted=0 AND cats_id='.$cat->id,'order'=>'cats_id ASC, id DESC', 'limit'=>'5'));
                if ($items){
                $items_array[$cat->id] = array(
                    'id' => $cat->id,
                    'title' => $cat->title,
                    'items' => $items
                    );
                }
            }
        }

        $catslist= CHtml::listData($cats,'id','title');

        $this->render('index', array(
            'items_array'   => $items_array,
            'submenu'       => $submenu,
            'catslist'      => $catslist,
        ));
    }
    
    public function actionView($id)
    {
        $model = $this->getModel($id);
        $items= ShopItems::getItems($model,$id);

        //Получаем массив названий экспозиций
        $cats = ShopCats::model()->findAll(array('order'=>'id asc, parent_id asc'));
        $catslist= CHtml::listData($cats,'id','title');
        $submenu = ShopCats::getSubmenu();

        $this->render('view', array(
            'model' => $model,
            'items' => $items,
            'catslist' => $catslist,
            'submenu' => $submenu,
        ));
    }

    public function actionItem($id)
    {
        $item = ShopItems::model()->findByPk($id);
        if ($item)
        {
            if ($item->deleted == 0 AND $item->hidden == 0)
            {
                $cats = ShopCats::model()->findByPk($item->cats_id);
                $submenu = ShopCats::getSubmenu();


                $active_cat = false;
                $active_subcat = false;
                $active_cat =  ShopCats::model()->findByPk($item->cats_id);
                if ($active_cat){
                    if ($active_cat->parent_id > 0){
                        $parent_cat_id = $active_cat->parent_id;
                        $active_subcat =  $active_cat;
                        $active_cat = ShopCats::model()->findByPk($parent_cat_id);
                    }
                }

                $this->render('item', array(
                'model' => $item,
                'cats' => $cats,
                'submenu' => $submenu,
                'active_cat' => $active_cat,
                'active_subcat' => $active_subcat,
                ));
            }else
            {
                throw new CHttpException(404);
            }
        }
   }

    protected function getModel($id)
    {
        $model = ShopCats::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    public function actionCart($id=0)
    {

        $cookie = Yii::app()->request->cookies['cart_products'];
        $json = "";

        if ($cookie !== null) {
            $json =  $cookie->value;
        }

        $cart_array = array();
        if ($json != ''){
            $cart_array =  json_decode($json, true);
        }

        $cart_ids = array();
        if (count($cart_array) > 0){
            foreach($cart_array as $index => $quontity)
            {
                $cart_ids[] = $index;
            }
        }

        $items = false;
        if (count($cart_ids) > 0)
        {
            $items = ShopItems::model()->findAll(array('condition'=>'id IN ('.implode(',',$cart_ids).')'));

        }

        $this->render('cart', array(
            'items' => $items,
            'cart_array' => $cart_array,
        ));
   }

    public function actionCreateOrder()
    {
        $result = 0;
        $order = new ShopOrders();

        if (isset($_POST['ShopOrders']))
        {
            $order->attributes  = $_POST['ShopOrders'];

            $chars = array('+','(',')',' ','-'); // символы для удаления
            $phone = str_replace($chars, '', $order->phone); // PHP код
            $order->phone = $phone;

            $order->date        = new CDbExpression('NOW()');
            if ($order->save()){
                $order->secret      = md5($order->date."_".$order->id);
                $order->save();
                $result = $order->id;

                //Отправляем юзеру смс
                ShopSms::addSms($order, $new_status=0);
                }
        }

        echo CJSON::encode(array(
            'id'        =>  $order->id,
            'secret'    =>  $order->secret
        ));
   }

    public function actionOrderItems($order_id=0,$secret='')
    {
        $order = ShopOrders::model()->findByPk($order_id);
        if ($order)
        {
            if ($order->secret == $secret AND $secret != '')
            {
                $items_cats_id = array();
                    if (isset($_POST))
                    {
                        if (count($_POST)>0)
                        {
                            foreach($_POST as $index => $quontity)
                            {
                                    $ITEM = ShopItems::model()->findByPk((int)$index);
                                    $order_item = new ShopOrdersitems();
                                    $order_item->order_id = (int)$order_id;
                                    $order_item->item_id = (int)$index;
                                    $order_item->quontity = (int)$quontity;
                                    $order_item->price = $ITEM->price;
                                    $order_item->save();

                                    $items_cats_id[] = $ITEM->cats_id;
                            }
                        }
                    }

                    //Создаем запись на странице заказов
                    $order_dashboard = new Orders();
                    $order_dashboard->date = new CDbExpression('NOW()');
                    $order_dashboard->time = new CDbExpression('DATE_ADD(NOW(), INTERVAL 8 HOUR)');
                    $order_dashboard->type = 'shopOrder';
                    $order_dashboard->order_id = $order->id;
                    $order_dashboard->save();



                //echo '1<br>';
                if (count($items_cats_id)>0){
                    //echo '2<br>';
                   $parent_cats_id = array();
                   $cats = ShopCats::model()->findAll(array('condition'=>'id IN ('.implode(',',$items_cats_id).')') );
                   if ($cats){
                      // echo '3<br>';
                       foreach($cats as $cat)
                       {
                        if ($cat->parent_id == 0){$parent_cats_id[] = $cat->id;}
                        else{$parent_cats_id[] = $cat->parent_id;}
                       }
                   }

                   if(count($parent_cats_id)>0)
                   {
                      // echo '4<br>';
                    $parent_cats_id = array_unique($parent_cats_id);
                    $sql = '(notify like "%shop_'.implode('%" OR notify like "%shop_',$parent_cats_id).'%")';

                   // echo 'assssssssssss'.$sql;

                    //добавляем уведомление для админов сайта
                    $query = "SELECT * FROM {{users}} where role in ('admin','manager') and ".$sql;
                	$users = Yii::app()->db->createCommand($query)->queryAll();
                    if ($users){
                        foreach($users as $user)
                        {
                            $type = 'shopOrder';

                            $notification = new Notifications();
                            $notification->user_id = $user['id'];
                            $notification->order_id = $order->id;
                            $notification->type = $type;
                            $notification->save();


                            $titleMessage = Orders::messageType($type);

                            //$link = 'https://sakhalinzoo.ru/zoo.php?r=dashboard/index&type='.$type;
                            $link = 'https://sakhalinzoo.ru/zoo.php?r=shop/order&id='.$order->id;

                                 $query = "INSERT INTO {{webpush_emails}} (user_id, message, title, link)"
            						." VALUES (:user, :message, :title, :link)";
            					$params = array(
            						'user' => $user['id'],
            						'message' => $titleMessage['message'],
            						'title' => $titleMessage['title'],
            						'link' => $link,
            					);
            					Yii::app()->db->createCommand($query)->execute($params);
                        }
                    }
                   }


                }








            }
        }
        $result = 0;
        echo CJSON::encode($result);
   }

    public function actionOrder($id='')
    {

        $data = ShopOrders::model()->findAll(array('condition'=>'secret="'.$id.'"'));

        $order = false;
        if (isset($data[0])){
        $order = $data[0];
        }

        $r = false;
        if ($order)
        {
            if ($order->status < 1000)
            {
                $r = true;
                $this->render('order', array(
                    'data' => $order,
                ));
            }
        }

        if ($r == false)
        {
             throw new CHttpException(404);
        }
   }



}
