<?php

class SiteController extends FrontEndController
{

    /**
     * Выводит страницу с ошибкой
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error)
        {
            $this->render('error', $error);
        }
    }

    /**
     * Смена региона
     */
    public function actionRegion()
    {
        if (isset($_POST['region']) && in_array($_POST['region'], array('khv', 'other'))) {
            Yii::app()->user->setRegion($_POST['region']);
        }
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : '/' );
    }

    /**
     * Подписка на рассылку
     */
    public function actionSubscribe()
    {
        $result = array(
            'success' => false,
            'errors' => array(),
        );
        $subscriber = new Subscriber();
        $subscriber->date = date('Y-m-d H:i:s');
        if (isset($_POST['Subscriber'])) {
            $subscriber->attributes = $_POST['Subscriber'];
            if ($subscriber->save()) {
                $result['success'] = true;
            } else {
                $result['errors'] = $subscriber->getErrors();
            }
        }
        echo CJSON::encode($result);
        exit;
    }

    /**
     * Подписка на рассылку
     */
    public function actionSearch($search_text = '')
    {
        /*
        exposure
        exposure_items

        */
        $news = '';
        $afisha = '';
        $pages = '';
        $conf = '';
        $exposure = '';
        $exposure_items = '';
        if ($search_text != ''){
        if (mb_strlen($search_text) >= 8){
            $query = "SELECT * FROM {{news}} WHERE title like :t OR text like :txt ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%');
            $news = Yii::app()->db->createCommand($query)->query($params)->readAll();

            $query = "SELECT * FROM {{afisha}} WHERE title like :t OR text like :txt ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%');
            $afisha = Yii::app()->db->createCommand($query)->query($params)->readAll();

            $query = "SELECT * FROM {{cats}} WHERE (cats_id > 0 and plugin = '' and name like :t OR text like :txt)  OR (name like :t) ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%');
            $pages = Yii::app()->db->createCommand($query)->query($params)->readAll();

            $query = "SELECT * FROM {{conf}} WHERE title like :t OR text like :txt ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%');
            $conf = Yii::app()->db->createCommand($query)->query($params)->readAll();

            $query = "SELECT * FROM {{exposure}} WHERE title like :t OR text like :txt ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%');
            $exposure = Yii::app()->db->createCommand($query)->query($params)->readAll();

            $query = "SELECT * FROM {{exposure_item}} WHERE title like :t OR text like :txt ORDER BY id DESC";
            $params = array("t"=>'%'.$search_text.'%', "txt"=>'%'.$search_text.'%');
            $exposure_items = Yii::app()->db->createCommand($query)->query($params)->readAll();
        }
        }
        $this->render('//search/index', array(
            'news'=>$news,
            'afisha'=>$afisha,
            'pages'=>$pages,
            'conf'=>$conf,
            'exposure'=>$exposure,
            'exposure_items'=>$exposure_items,
        ));

    }


}