<?php

/**
 * Страница "Контакты"
 */
class SitemapController extends FrontEndController
{


    public function actionIndex()
    {
        $typeList = Conf::typeList();

        $query = "SELECT * FROM {{tags}} WHERE id > 0 and type = 'afisha' ORDER BY name";
        $params = array();
        $afisha_tags = Yii::app()->db->createCommand($query)->query($params)->readAll();

        $expo = Exposure::getAll();

        $this->render('index', array(
             'typeList'=>$typeList,
             'afisha_tags'=>$afisha_tags,
             'expo'=>$expo,
        ));
    }

}