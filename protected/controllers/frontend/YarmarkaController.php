<?php

class YarmarkaController extends FrontEndController
{

    public $razdel_id = 41;

    public function actionIndex()
    {

        $model = Cats::model()->findByPk($this->razdel_id);

        if ($model->cats_id >0){
        $submenu = Cats::getSubmenu($model->cats_id);
        }else{
        $submenu = array();
        }


        $this->render('index', array(
            'model'             => $model,
            'submenu'             => $submenu,
        ));
    }

    public function actionOrder()
    {
        $result = array();

        $model = new Yarmarka();
        if (isset($_POST['Yarmarka'])) {
            $model->attributes = $_POST['Yarmarka'];
            $model->date = new CDbExpression('NOW()');
            $model->time = new CDbExpression('DATE_ADD(NOW(), INTERVAL 8 HOUR)');
            $model->save();
            $result['id'] = $model->id;

            Orders::createOrder('yarmarka',$model->id);
        }

        echo json_encode($result);
    }

    public function actionFilesAdd($post_id)
    {
        $post_item = Yarmarka::model()->findByPk($post_id);
        if ($post_item)
        {
        $uploadedFiles = CUploadedFile::getInstancesByName('files');
        $uploader = new FileUploader();
        foreach ($uploadedFiles as $file)
        {
            if (in_array($file->extensionName,array('jpg','jpeg','tiff','gif','png','webp','bmp','JPG','JPEG','TIFF','GIF','PNG','WEBP','BMP')))
            {
                $uploaded_file = $uploader->addPhoto($file, 800, 600, $path = Yii::getPathOfAlias('webroot').'/upload/photos/');
                $news_photos = new Photos;
                $news_photos->post_id = $post_id;
                $news_photos->date = new CDbExpression('NOW()');
                $news_photos->filename = $uploaded_file;
                $news_photos->type = 'yarmarka_order';
                $news_photos->save();
            }else
            {
                $uploaded_file = $uploader->addFile($file, $path = Yii::getPathOfAlias('webroot').'/upload/docs/');
                $news_docs = new Docs;
                $news_docs->post_id = $post_id;
                $news_docs->date = new CDbExpression('NOW()');
                $news_docs->filename = $uploaded_file;
                $news_docs->filetitle = $file->getName();
                $news_docs->type = 'yarmarka_order';
                $news_docs->save();
            }
        }
        }
    }

}