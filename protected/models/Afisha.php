<?php

/**
 * Модель статьи в блоге
 */
class Afisha extends CActiveRecord
{
    
    const IMAGE_WIDTH = 200;
    const IMAGE_HEIGHT = 100;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'afisha';
    }

    public function rules()
    {
        return array(
            array('title, date', 'required'),
            array('date', 'date', 'format' => 'yyyy-MM-dd'),
            array('short_text, filename, text, tags, cover_id, has_groups, end_date, time', 'safe'),
            array('image', 'file', 'types' => 'jpg', 'allowEmpty' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'orders' => array(self::HAS_MANY, 'GroupsOrders', 'groups_id', 'on' => "", 'order' => 'id desc'),
            'photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{photos}}.type = 'afisha'", 'order' => 'id'),
            'docs' => array(self::HAS_MANY, 'Docs', 'post_id', 'on' => "{{docs}}.type = 'afisha'", 'order' => 'id'),
            'some_photo' => array(self::HAS_ONE, 'Photos', 'post_id', 'on' => "{{some_photo}}.type = 'afisha' and deleted=0", 'order' => 'id'),
            'cover' => array(self::BELONGS_TO, 'Photos', 'cover_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'date' => 'Дата начала',
            'title' => 'Заголовок',
            'text' => 'Текст статьи',
            'short_text' => 'Краткое описание',
            'image' => 'Картинка',
            'filename' => 'Имя файла',
            'end_date' => 'Дата окончания',
            'time' => 'Время проведения',
        );
    }

    public function getImageUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return '';
        }
    }
    
    public function hasImage()
    {
        if ($this->cover)
        {
            $file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$this->cover->filename;
            return file_exists($file);
        }else{
            return false;
        }
    }

    public function getThumbnailUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/s_' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/s_' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return $this->getImageUrl();
        }
    }
    
    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/photos/'.$size.$this->cover->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }

    public static function getMainList()
    {
        return Afisha::model()->findAll(array('condition'=>'','order'=>'date desc','limit'=>18));
    }

    protected function afterDelete()
    {
        //parent::afterDelete();
        //$this->removeImage();
    }

    public static function autoCover(Afisha $post)
    {
        if ($post){
            if ($post->cover_id == 0){
                if ($post->some_photo)
                {
                    $post->cover_id = $post->some_photo->id;
                    $post->save();
                    return array(
                        'photo_id' => $post->cover_id,
                        'photo_adress' => $post->getThumbnailUrl(),
                    );
                }
            }
        }
    }
}