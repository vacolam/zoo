<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class BannersBottom extends CActiveRecord
{

    CONST WIDTH = 148;
    CONST HEIGHT = 148;

    public $image;

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'banners_bottom';
    }

    public function rules()
    {
        return array(
            array('link', 'required'),
            array('image', 'file', 'types' => 'jpg,jpeg,png,gif', 'on' => 'insert'),
            array('image', 'file', 'types' => 'jpg,jpeg,png,gif', 'on' => 'update', 'allowEmpty' => true),
            array('title, description, filename, position', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'link' => 'Ссылка',
            'image' => 'Картинка',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'filename' => 'Имя файла',
            'position' => 'Позиция',
        );
    }

    public function getImageUrl()
    {
        return Yii::app()->getBaseUrl() . '/upload/banners/' . $this->filename;
    }

    protected function afterDelete()
    {
        parent::afterDelete();
        $file = Yii::getPathOfAlias('webroot').'/upload/banners/'.$this->filename;
        if (file_exists($file)) {
            unlink($file);
        }
    }

    public static function getMainBanners(){
        $banners = BannersBottom::model()->findAll(array('order'=>'position asc'));
        return $banners;
    }
}
