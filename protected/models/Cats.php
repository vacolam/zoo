<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class Cats extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'cats';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('link,id,number,id,text,plugin,filename,visible,keywords,description,backend', 'safe'),
            array('image', 'file', 'types' => 'jpg', 'allowEmpty' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{photos}}.type = 'pages'", 'order' => 'id'),
            'docs' => array(self::HAS_MANY, 'Docs', 'post_id', 'on' => "{{docs}}.type = 'pages'", 'order' => 'id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'name' => 'Заголовок',
            'text' => 'Текст',
            'image' => 'Обложка',
            'filename' => 'Имя файла',
            'number' => 'Порядковый номер раздела',
        );
    }

    public static function getMenu()
    {
        $menu = Cats::model()->findAll(array('condition'=>'cats_id=0','order'=>'number asc,id asc',));
        return $menu;
    }

    public static function getSubCats()
    {
        $submenu = Cats::model()->findAll(array('condition'=>'cats_id > 0','order'=>'number asc,id asc',));
        return $submenu;
    }

    public static function getPage($id)
    {
        $page = Cats::model()->findByPk($id);
        return $page;
    }

    public static function getSubmenu($id)
    {
        if ((int)$id > 0){
        $submenu = Cats::model()->findAll(array('condition'=>'cats_id = "'.(int)$id.'" and visible=1 and backend=0','order'=>'number asc, id asc'));
        }else{
        $submenu = array();
        }
        return $submenu;
    }

    public static function getParentID($razdel='',$podrazdel=0)
    {
        $result = 0;
        $cat = false;
        if ($razdel != 'pages')
        {
            $cat = Cats::model()->findAll(array('condition'=>'plugin="'.$razdel.'"'));
        }

        if ($razdel == 'pages' AND $podrazdel > 0)
        {
            $cat = Cats::model()->findByPk($podrazdel);
        }

        if ($cat){
                if ($cat->cats_id == 0){$result = $cat->id;}
                if ($cat->cats_id > 0){$result = $cat->cats_id;}
        }

        return $result;
    }

    public static function getPluginsArray($plugin)
    {
        if ($plugin == ''){$plugin = 'pages';}
        return array(
            $plugin."/index",
            $plugin."/view",
            $plugin."/create",
            $plugin."/update",
            $plugin."/delete",
            $plugin."/list",
            $plugin."/animal",
        );
    }

    public function getImageUrl()
    {
        return Yii::app()->getBaseUrl() . '/upload/pages/' . $this->filename;
    }

    public function hasImage()
    {
        $file = Yii::getPathOfAlias('webroot').'/upload/pages/'.$this->filename;
        return file_exists($file);
    }

    public function getThumbnailUrl()
    {
        $path = Yii::getPathOfAlias('webroot') . '/upload/pages/s_' . $this->filename;
        if (file_exists($path))
        {
            return Yii::app()->getBaseUrl() . '/upload/pages/s_' . $this->filename;;
        }else{
            return $this->getImageUrl();
        }
    }

    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/pages/'.$size.$model->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }

    protected function afterDelete()
    {
        //parent::afterDelete();
        //$this->removeImage();
    }



    public static function getKeywordsDescription($route)
    {
        //$menu = Cats::model()->findAll(array('condition'=>'cats_id=0','order'=>'number,id',));
        //return $menu;
    }


    public static function getCatIdByPlugin($plugin)
    {
        $result = 0;
        $menu = Cats::model()->findAll(array('condition'=>'plugin="'.$plugin.'"','order'=>'number asc,id asc',));
        if ($menu){
            if (isset($menu[0]))
            {
                $result = $menu[0]->id;
            }
        }
        return $result;
    }

    public static function getParentCatIdByPlugin($plugin)
    {
        $result = 0;
        $menu = Cats::model()->findAll(array('condition'=>'plugin="'.$plugin.'"','order'=>'number asc,id asc',));
        if ($menu){
            if (isset($menu[0]))
            {
                if ($menu[0]->cats_id == 0){
                $result = $menu[0]->id;
                }else{
                $result = $menu[0]->cats_id;
                }
            }
        }
        return $result;
    }

}