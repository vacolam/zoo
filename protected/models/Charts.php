<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class Charts extends CActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'charts';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('id, name, description, data', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'name' => 'Название',
            'description' => 'Описание',
            'data' => 'Данные диаграммы',
        );
    }


}
