<?php

/**
 * Модель статьи в блоге
 */
class Conf extends CActiveRecord
{
    
    const IMAGE_WIDTH = 500;
    const IMAGE_HEIGHT = 500;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'conf';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('short_text, date, text, cover_id, type, form_is_open, golosovanie_is_closed', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'nominations' => array(self::HAS_MANY, 'ConfNominations', 'conf_id', 'on' => "", 'order' => 'id asc'),
            'opros' => array(self::HAS_MANY, 'Opros', 'conf_id', 'on' => "", 'order' => 'number asc, id asc'),
            'some_photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{some_photos}}.type = 'conf' and deleted=0", 'order' => 'id', 'limit'=>7),
            'some_works' => array(self::HAS_MANY, 'ConfWorks', 'conf_id', 'on' => "", 'order' => 'id asc','limit'=>7),
            'jury' => array(self::HAS_MANY, 'ConfNomJury', 'conf_id', 'on' => "", 'order' => 'id asc'),
            'photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{photos}}.type = 'conf' and deleted=0", 'order' => 'id'),
            'docs' => array(self::HAS_MANY, 'Docs', 'post_id', 'on' => "{{docs}}.type = 'conf'", 'order' => 'id'),
            'cover' => array(self::BELONGS_TO, 'Photos', 'cover_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'date' => 'Дата',
            'title' => 'Заголовок',
            'text' => 'Описание',
            'short_text' => 'Краткое описание',
            'type' => 'Тип события',
        );
    }

    public static function typeList()
    {
        return array(
            'conf' => 'Конференции',
            'festival' => 'Фестивали',
            'konkurs' => 'Конкурсы',
            'opros' => 'Опросы',
        );
    }

    public function getImageUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return '';
        }
    }
    
    public function hasImage()
    {
        if ($this->cover)
        {
            $file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$this->cover->filename;
            return file_exists($file);
        }else{
            return false;
        }
    }

    public function getThumbnailUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/s_' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/s_' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return $this->getImageUrl();
        }
    }
    
    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/photos/'.$size.$this->cover->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }

}