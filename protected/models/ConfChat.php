<?php

/**
 * Модель статьи в блоге
 */
class ConfChat extends CActiveRecord
{
    
    const IMAGE_WIDTH = 500;
    const IMAGE_HEIGHT = 500;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'conf_chat';
    }

    public function rules()
    {
        return array(
            array('user_id', 'required'),
            array('id,date,text,user_id,work_id', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'user', 'user_id', 'on' => "", 'order' => 'id asc'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'date' => 'Дата',
            'text' => 'Описание',
            'user_id' => 'Пользователь',
            'work_id' => 'Работа',
        );
    }


    public static function msgsNew()
	{
        if (isset($_POST['text']) and isset($_POST['work_id'])){

        	$new_msg = new ConfChat();
        	$new_msg->user_id = Yii::app()->user->id;
        	$new_msg->work_id = $_POST['work_id'];
        	$new_msg->text = $links_text = $_POST['text'];
        	$new_msg->date = new CDbExpression('NOW()');
			$new_msg->save();

            $work = ConfWorks::model()->findByPk($_POST['work_id']);
            if ($work){
                $count = count($work->chat);
                $work->chat_messages = $count;
                $work->save();
            }
		}

       	return $new_msg;
	}
}