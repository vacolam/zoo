<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class ConfNomJury extends CActiveRecord
{

    CONST WIDTH = 1100;
    CONST HEIGHT = 600;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'conf_nom_jury';
    }

    public function rules()
    {
        return array(
            array('user_id', 'required'),
            array('user_id,nom_id,conf_id', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'nom' => array(self::BELONGS_TO, 'ConfNominations', 'nom_id'),
            'conf' => array(self::BELONGS_TO, 'Conf', 'conf_id'),
        );
    }



    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'user_id' => 'Пользователь',
            'conf_id' => 'Конференция',
            'nom_id' => 'Номинация',

        );
    }


}