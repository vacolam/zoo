<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class ConfWorks extends CActiveRecord
{

    CONST WIDTH = 1100;
    CONST HEIGHT = 600;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'conf_works';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name,email,phone,conf_id,nom_id,filename,mark,date,marks_count,chat_messages', 'safe'),
            array('city, birth, shcool, class, teachers_name, teachers_email, teachers_occupation, teachers_cell, work_name, cover_id, photos_link', 'safe'),
        );
    }


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'nomination' => array(self::BELONGS_TO, 'ConfNominations', 'nom_id'),
            'marks' => array(self::HAS_MANY, 'ConfWorksMarks', 'work_id'),
            'chat' => array(self::HAS_MANY, 'ConfChat', 'work_id'),
            'photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{photos}}.type = 'confwork' and deleted=0", 'order' => 'id'),
            'one_photo' => array(self::HAS_ONE, 'Photos', 'post_id', 'on' => "{{one_photo}}.type = 'confwork' and deleted=0", 'order' => 'id'),
            'docs' => array(self::HAS_MANY, 'Docs', 'post_id', 'on' => "{{docs}}.type = 'confwork'", 'order' => 'id'),
            'cover' => array(self::BELONGS_TO, 'Photos', 'cover_id'),
            'conf' => array(self::BELONGS_TO, 'Conf', 'conf_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'name' => 'Имя',
            'email' => 'e-mail',
            'phone' => 'Телефон',
            'nom_id' => 'Номинация',
            'conf_id' => 'Конференция',
            'filename' => 'Имя файла',
            'mark' => 'Сумма оценок',
            'date' => 'Дата',
        );
    }

    public function getImageUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return '';
        }
    }

    public function hasImage()
    {
        if ($this->cover)
        {
            $file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$this->cover->filename;
            return file_exists($file);
        }else{
            return false;
        }
    }

    public function getThumbnailUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/s_' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/s_' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return $this->getImageUrl();
        }
    }

    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/photos/'.$size.$this->cover->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }

    public function checkSiteAdress($url){
        //Сначала убираем http https //, если пользователь его вводил
        $domen = preg_replace('/(http\:\/\/|https\:\/\/|\/\/)/', '', trim($url));


        //Теперь заменяем множество точек на одну sait..5.....ru => sait.5.ru - это пригодится в дальнейшем
        $domen = preg_replace('/\.+/', '.', $domen);
        $domen = str_replace('www.', '', $domen);
        /*Принудительно добавляем к домену http, чтобы выделить из ссылки домен за счет функции parse_url().
        http://sa.ru/page => sa.ru
        еда.рф/index.php?template=access => еда.рф*/
        //if(parse_url('https://'.$domen)) {$domen_ar = parse_url('https://'.$domen); $domen = $domen_ar['host']; }

        //удаляем www, если оно есть в домене
        $domen_ar = 'https://'.$domen;

        //После всех этих преобразований мы можем вывести наш очищенный домен
        return $domen_ar;

    }
}