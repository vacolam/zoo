<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class ConfWorksMarks extends CActiveRecord
{

    CONST WIDTH = 1100;
    CONST HEIGHT = 600;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'conf_works_marks';
    }

    public function rules()
    {
        return array(
            array('work_id', 'required'),
            array('work_id, date, user_id, mark', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }



    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'user_id' => 'Пользователь',
            'date' => 'Дата',
            'work_id' => 'Работа',
            'mark' => 'Оценка',

        );
    }


}