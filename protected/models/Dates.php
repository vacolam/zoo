<?php

class Dates
{


  public static function getName($date){
		if ($date != '0000-00-00' and $date != ''){
		$date_longName = array(
		    "01" => "Января",
		    "02" => "Февраля",
		    "03" => "Марта",
		    "04" => "Апреля",
		    "05" => "Мая",
		    "06" => "Июня",
		    "07" => "Июля",
		    "08" => "Августа",
		    "09" => "Сентября",
		    "10" => "Октября",
		    "11" => "Ноября",
		    "12" => "Декабря"
		);
		     $getdate=explode('-',$date);
		     $result = $getdate[2]." ".$date_longName[$getdate [1]]." ".$getdate [0];
		}else{
			$result = '';
		}
		   return $result;
  }


  	public static function getDatetimeName($date){
		if ($date != '0000-00-00 00:00:00' and $date != ''){
		$date_longName = array(
		    "01" => "января",
		    "02" => "февраля",
		    "03" => "марта",
		    "04" => "апреля",
		    "05" => "мая",
		    "06" => "июня",
		    "07" => "июля",
		    "08" => "августа",
		    "09" => "сентября",
		    "10" => "октября",
		    "11" => "ноября",
		    "12" => "декабря"
		);
			 $first=explode(' ',$date);
		     $getdate=explode('-',$first[0]);
		     $result = $getdate[2]." ".$date_longName[$getdate [1]]." ".$getdate [0];
		}else{
			$result = '';
		}
		   return $result;
		}

    public static function showDatetime($date){
		if ($date != '0000-00-00 00:00:00' and $date != ''){
		$date_longName = array(
		    "01" => "января",
		    "02" => "февраля",
		    "03" => "марта",
		    "04" => "апреля",
		    "05" => "мая",
		    "06" => "июня",
		    "07" => "июля",
		    "08" => "августа",
		    "09" => "сентября",
		    "10" => "октября",
		    "11" => "ноября",
		    "12" => "декабря"
		);
			 $first=explode(' ',$date);
		     $getdate=explode('-',$first[0]);
		     $result = $getdate[2]." ".$date_longName[$getdate [1]]." ".$getdate [0]." - ".$first[1]."";
		}else{
			$result = '';
		}
		   return $result;
	}

    public static function getFormat($date){
		if ($date != '0000-00-00' and $date != ''){
		     $getdate=explode('-',$date);
		     $result = $getdate[2].".".$getdate [1].".".$getdate [0];
		}else{
			$result = '';
		}
		   return $result;
  }

  public static function getMonthName($m){
		$months = array(
				'1'		=>	'Янв',
				'2'		=>	'Фев',
				'3'		=>	'Март',
				'4'		=>	'Апр',
				'5'		=>	'Май',
				'6'		=>	'Июн',
				'7'		=>	'Июл',
				'8'		=>	'Авг',
				'9'		=>	'Сeнт',
				'10'	=>	'Окт',
				'11'	=>	'Нбр',
				'12'	=>	'Дек',
				);
        if (isset($months[$m])){
        	$result = $months[$m];
		}
		else{
			$result = '';
		}
		return $result;
  }

  public static function getMonthNameLong($m){
		$months = array(
				'1'		=>	'Январь',
				'2'		=>	'Февраль',
				'3'		=>	'Март',
				'4'		=>	'Апрель',
				'5'		=>	'Май',
				'6'		=>	'Июнь',
				'7'		=>	'Июль',
				'8'		=>	'Август',
				'9'		=>	'Сeнтябрь',
				'10'	=>	'Октябрь',
				'11'	=>	'Ноябрь',
				'12'	=>	'Декабрь',
				);
        if (isset($months[$m])){
        	$result = $months[$m];
		}
		else{
			$result = '';
		}
		return $result;
  }

    public static function getMonth($date){
		$result = '';
		if ($date != '0000-00-00')
		{
			$getdate=explode('-',$date);
			$result = Dates::getMonthName((int)$getdate [1]);
		}
		return $result;
  }

  public static function getDayNumber($date){
		$result = '';
		if ($date != '0000-00-00')
		{
			$getdate=explode('-',$date);
			$result = $getdate [2];
			if ((int)$result < 10){$result = '0'.(int)$result;}
		}
		return $result;
  }

   public static function getYearNumber($date){
		$result = '';
		if ($date != '0000-00-00')
		{
			$getdate=explode('-',$date);
			$result = $getdate [0];
		}
		return $result;
  }

  public static function days_ago_old($date){
	    $diff = array();

	    //Если вторая дата не задана принимаем ее как текущую
	        $cd = getdate();
	        $date2 = $cd['year'].'-'.$cd['mon'].'-'.$cd['mday'].' '.$cd['hours'].':'.$cd['minutes'].':'.$cd['seconds'];


		$days = abs(floor((strtotime($date2) - strtotime($date)) / 86400));
        $time = date("H:i",strtotime($date));
		//Значение по умолчанию
		$result = Yii::app()->dateFormatter->formatDateTime($date, 'medium');
		//Сегодня
        if ($days == 0){$result = 'Сегодня в '.$time;}
		//Вчера
        if ($days == 1){$result = 'Вчера в '.$time;}
		//Дней назад
        if ($days > 1 AND $days <= 4){$result = $days. ' дня назад';}
        if ($days == 5){$result = $days. ' дней назад';}
		//Неделю
        if ($days > 5 AND $days <= 9){$result = 'Неделю назад';}
		//Две недели
        if ($days > 9 AND $days <= 16){$result = '2 недели назад';}
		//Три недели
        if ($days > 16 AND $days <= 23){$result = '3 недели назад';}
		//Месяц
        if ($days > 23 AND $days <= 40){$result = 'Месяц назад';}
		//Месяц
        if ($days > 40){
			$td = explode(' ',$date);
			$result = $this->date_makeLongStringName($td[0]);
		}


		return $result;
	}

	public static function days_ago($time) { // преобразовываем время в нормальный вид
		$a = strtotime($time);
		 date_default_timezone_set('Europe/Moscow');
		 $ndate = date('d.m.Y', $a);
		 $ndate_time = date('H:i', $a);
		 $ndate_exp = explode('.', $ndate);
		 $nmonth = array(
		  1 => 'января',
		  2 => 'февраля',
		  3 => 'марта',
		  4 => 'апреля',
		  5 => 'мая',
		  6 => 'июня',
		  7 => 'июля',
		  8 => 'августа',
		  9 => 'сентября',
		  10 => 'октября',
		  11 => 'ноября',
		  12 => 'декабря'
		 );

		 foreach ($nmonth as $key => $value) {
		  if($key == intval($ndate_exp[1])) $nmonth_name = $value;
		 }

		 if($ndate == date('d.m.Y')) return 'Сегодня в '.$ndate_time;
		 elseif($ndate == date('d.m.Y', strtotime('-1 day'))) return 'Вчера в '.$ndate_time;
		 else return $ndate_exp[0].' '.$nmonth_name.' '.$ndate_exp[2];
	}

	public static function getCheckinName($date){
		if ($date != '0000-00-00' and $date != ''){
		$date_longName = array(
		    "01" => "Января",
		    "02" => "Февраля",
		    "03" => "Марта",
		    "04" => "Апреля",
		    "05" => "Мая",
		    "06" => "Июня",
		    "07" => "Июля",
		    "08" => "Августа",
		    "09" => "Сентября",
		    "10" => "Октября",
		    "11" => "Ноября",
		    "12" => "Декабря"
		);
		$date_longName2 = array(
		    "01" => "Январь",
		    "02" => "Февраль",
		    "03" => "Март",
		    "04" => "Апрель",
		    "05" => "Май",
		    "06" => "Июнь",
		    "07" => "Июль",
		    "08" => "Август",
		    "09" => "Сентябрь",
		    "10" => "Октябрь",
		    "11" => "Ноябрь",
		    "12" => "Декабрь"
		);
		$date_longName3 = array(
		    "01" => "январе",
		    "02" => "феврале",
		    "03" => "марте",
		    "04" => "апреле",
		    "05" => "мае",
		    "06" => "июне",
		    "07" => "июле",
		    "08" => "августе",
		    "09" => "сентябре",
		    "10" => "октябре",
		    "11" => "ноябре",
		    "12" => "декабре"
		);
		     $getdate=explode('-',$date);

			 if ($getdate [0] == '1900')
			 {
				 if ($getdate [0] == '1900' AND $getdate [1] == '01'){
	             $result = 'Давным-давно';
				 }

				 if ($getdate [0] == '1900' AND $getdate [1] != '01'){
	             	$result = 'Давным-давно в '.$date_longName3[$getdate [1]];
				 }
			 }else{
                 if ($getdate[2] == '00' || $getdate[2] == '0' || $getdate[2] == 0)
				 {
                 	$result = $date_longName2[$getdate [1]]." ".$getdate [0];
				 }else{
                 	$result = $getdate[2]." ".$date_longName[$getdate [1]]." ".$getdate [0];
				 }
			 }
		}else{
			$result = '';
		}
		   return $result;
  }


  public static function getSeason(){
      $dayOfTheYear = date('z') + 1;
      if($dayOfTheYear < 80 || $dayOfTheYear > 356){
          return 'Winter';
      }
      if($dayOfTheYear < 173){
          return 'Spring';
      }
      if($dayOfTheYear < 266){
          return 'Summer';
      }
      return 'Fall';
  }


}
