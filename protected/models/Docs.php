<?php

/**
 * Модель статьи в блоге
 */
class Docs extends CActiveRecord
{
    

    public $image;
    public $doc;

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'docs';
    }

    public function rules()
    {
        return array(
            array('post_id', 'required'),
            array('id, filename, date, type, filetitle, subdir', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'post_id' => 'ID поста',
            'filename' => 'Имя файла',
            'filetitle' => 'название файла до сохраненеия',
            'date' => 'дата добавления',
            'type' => 'тип',
        );
    }

    public function getDocUrl()
    {
        return Yii::app()->getBaseUrl() . '/upload/docs/' . $this->filename;
    }


    public function hasDoc()
    {
        $file = Yii::getPathOfAlias('webroot').'/upload/docs/'.$this->filename;
        return file_exists($file);
    }
    
    public function removeDoc()
    {
        if ($this->hasDoc()) {
            unlink(Yii::getPathOfAlias('webroot').'/upload/docs/'.$this->filename);
        }
    }

    protected function afterDelete()
    {

    }

    public static function deleteDoc(Docs $model)
    {
        $deleted_path = Yii::getPathOfAlias('webroot').'/upload/docs/'.$model->filename;
        if (file_exists($deleted_path)) {
                unlink($deleted_path);
        }
        $model->delete();
    }
}