<?php

/**
 * Модель статьи в блоге
 */
class DocumentsCats extends CActiveRecord
{
    
    const IMAGE_WIDTH = 200;
    const IMAGE_HEIGHT = 100;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'documents_cats';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            /*array('date', 'date', 'format' => 'yyyy-MM-dd'),*/
            array('id,title,number', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'docs' => array(self::HAS_MANY, 'Docs', 'post_id', 'on' => "{{docs}}.type = 'documents'", 'order' => 'id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'title' => 'Заголовок',
            'number' => 'Порядковый номер',

        );
    }

    public static function getDocs($cat_id = 0){

        $docs = new CActiveDataProvider('Docs', array(
            'criteria' => array(
                'condition' => 'type = "documents" And post_id = "'.$cat_id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));

        return $docs;
    }

    public function getImageUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return '';
        }
    }
    
    public function hasImage()
    {
        if ($this->cover)
        {
            $file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$this->cover->filename;
            return file_exists($file);
        }else{
            return false;
        }
    }

    public function getThumbnailUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/s_' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/s_' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return $this->getImageUrl();
        }
    }
    
    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/photos/'.$size.$this->cover->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }

    public static function getMainList()
    {
        return News::model()->findAll(array('condition'=>'','order'=>'date desc','limit'=>6));
    }

    protected function afterDelete()
    {
        //parent::afterDelete();
        //$this->removeImage();
    }

    public static function autoCover(News $post)
    {
        if ($post){
            if ($post->cover_id == 0){
                if ($post->some_photo)
                {
                    $post->cover_id = $post->some_photo->id;
                    $post->save();
                    return array(
                        'photo_id' => $post->cover_id,
                        'photo_adress' => $post->getThumbnailUrl(),
                    );
                }
            }
        }
    }
}