<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class Eng extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'eng';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('link,id,number,id,text,plugin,filename,visible,keywords,description, show_raspisanie', 'safe'),
            array('image', 'file', 'types' => 'jpg', 'allowEmpty' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{photos}}.type = 'eng'", 'order' => 'id'),
            'docs' => array(self::HAS_MANY, 'Docs', 'post_id', 'on' => "{{docs}}.type = 'eng'", 'order' => 'id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'name' => 'Заголовок',
            'text' => 'Текст',
            'image' => 'Обложка',
            'filename' => 'Имя файла',
            'number' => 'Порядковый номер раздела',
            'show_raspisanie' => 'Показывать расписание',
        );
    }

    public static function getMenu()
    {
        $menu = Eng::model()->findAll(array('condition'=>'cats_id=0','order'=>'number,id',));
        return $menu;
    }

    public static function getHomePage()
    {
        $number = 0;
        $menu = Eng::model()->findAll(array('condition'=>'cats_id=0','order'=>'number,id','limit'=>1));
        if (isset($menu[0])){
            $number = $menu[0]->id;
        }
        return $number;
    }

    public static function getSubeng()
    {
        $submenu = Eng::model()->findAll(array('condition'=>'cats_id > 0','order'=>'number,id',));
        return $submenu;
    }

    public static function getPage($id)
    {
        $page = Eng::model()->findByPk($id);
        if ($page){
            return $page;
        }else{
            throw new CHttpException(404);
        }
    }

    public static function getSubmenu($id)
    {
        $submenu = Eng::model()->findAll(array('condition'=>'cats_id = "'.(int)$id.'"'));
        return $submenu;
    }

    public static function getPluginsArray($plugin)
    {
        if ($plugin == ''){$plugin = 'pages';}
        return array(
            $plugin."/index",
            $plugin."/view",
            $plugin."/create",
            $plugin."/update",
            $plugin."/delete",
            $plugin."/list",
        );
    }

    public function getImageUrl()
    {
        return Yii::app()->getBaseUrl() . '/upload/eng/' . $this->filename;
    }

    public function hasImage()
    {
        $file = Yii::getPathOfAlias('webroot').'/upload/eng/'.$this->filename;
        return file_exists($file);
    }

    public function getThumbnailUrl()
    {
        $path = Yii::getPathOfAlias('webroot') . '/upload/eng/s_' . $this->filename;
        if (file_exists($path))
        {
            return Yii::app()->getBaseUrl() . '/upload/eng/s_' . $this->filename;;
        }else{
            return $this->getImageUrl();
        }
    }

    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/eng/'.$size.$model->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }

    protected function afterDelete()
    {
        //parent::afterDelete();
        //$this->removeImage();
    }


}
