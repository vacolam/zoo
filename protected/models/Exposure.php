<?php

/**
 * Модель статьи в блоге
 */
class Exposure extends PostModelInterface
{

    public $type = 'exposure';

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'exposure';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            /*array('date', 'date', 'format' => 'yyyy-MM-dd'),*/
            array('short_text, filename, pre_text,text,exposure_year,panorama_link,webcam_link', 'safe'),
            array('image', 'file', 'types' => 'jpg,jpeg,gif', 'allowEmpty' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{photos}}.type = '".$this->type."'", 'order' => 'id'),
            'some_photo' => array(self::HAS_ONE, 'Photos', 'post_id', 'on' => "{{some_photo}}.type = '".$this->type."' and deleted=0", 'order' => 'id'),
            'docs' => array(self::HAS_MANY, 'Docs', 'post_id', 'on' => "{{docs}}.type = '".$this->type."'", 'order' => 'id'),
            'cover' => array(self::BELONGS_TO, 'Photos', 'cover_id'),

        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'date' => 'Дата',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'short_text' => 'Краткое описание',
            'pre_text' => 'Текст',
            'image' => 'Обложка',
            'filename' => 'Имя файла',
            'exposure_year' => 'Год создания экспозиции',
            'panorama_link' => 'Ссылка на страницу панорамы',
            'webcam_link' => 'Ссылка на страницу веб-камеры',
        );
    }

    public static function getMainList()
    {
        return Exposure::model()->findAll(array('condition'=>'','order'=>'id asc','limit'=>8));
    }

    public static function getAll()
    {
        return Exposure::model()->findAll(array('condition'=>'','order'=>'id asc'));
    }

    public static function getSomeAnimalsPhotos($id)
    {
        $someAnimals = ExposureItems::model()->findAll(array('condition'=>'exposure_id="'.$id.'"','order'=>'id asc','limit'=>10));



        $animals_ids = array();
        if ($someAnimals){
            foreach ($someAnimals as $animal)
            {
                $animals_ids[] = $animal->id;
            }
        }

        if (count($animals_ids) == 0){$animals_ids[] = 0;}

        $somephotos = Photos::model()->findAll(array('condition'=>'post_id IN ('.implode(',',$animals_ids).') and deleted=0','order'=>'rand()','limit'=>20));
        return $somephotos;
    }
}