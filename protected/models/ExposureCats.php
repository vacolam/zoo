<?php

/**
 * Модель статьи в блоге
 */
class ExposureCats extends CActiveRecord
{

    const IMAGE_WIDTH = 200;
    const IMAGE_HEIGHT = 100;

    public $image;
    public $exposure_cats_array;

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'exposure_cats';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('id,title,text,parent_id,cover_id', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'date' => 'Дата',
            'title' => 'Заголовок',
            'text' => 'Описание',
            'short_text' => 'Краткое описание',
            'pre_text' => 'Текст',
            'image' => 'Обложка',
            'parent_id' => 'К какой категории относится',
        );
    }

    public static function getCats()
    {
       $cats = ExposureCats::model()->findAll(array('order'=>'id asc, parent_id asc'));

       $cats_array = array();
       foreach($cats as $cat){
        $cats_array[] = array('id'=>$cat->id, 'title'=>$cat->title, 'parent_id' => $cat->parent_id);
       }

            $root = ExposureCats::restore_tree($cats_array);

            ExposureCats::print_tree($root);

       return $cats;
    }


    /**восстанавливает дерево по таблице связей
     * @param $data array
     * @return array
     */
    public static function restore_tree($data)
    {
        $lst = ExposureCats::prepare_parents($data);

        foreach ($lst as &$parent) {
            ExposureCats::restore_tree_impl($parent, $lst);
        }
        return $lst[null];
    }

    /** восстановление связей родитель->дети
     * @param $parent array
     * @param $data array
     * @return void
     */
    public static function restore_tree_impl(&$parent, &$data) {
        foreach ($parent['child'] as &$child) {
            // Если текущий ребенок сам является родителем
            if (array_key_exists($child['id'], $data)) {
                // восстановить связь
                $child['child'] = $data[$child['id']]['child'];
                // проверить детей
                ExposureCats::restore_tree_impl($child, $data);
            } else {
                $child['child'] = array();
            }
        }
    }
    public static function make($id) {
        return array('id' => $id, 'title' => '[root]', 'child' => array());
    }
    /**Создает список узлов, имеющих датей
     * @param $data $data
     * @return array
     */
    public static function prepare_parents($data) {
        $res = array();
        foreach ($data as $item) {
            $parent = $item['parent_id'];
            if (!array_key_exists($parent, $res)) {
                $res[$parent] = ExposureCats::make($parent);
                if (array_key_exists($parent, $data)) {
                    $res[$parent]['title'] = $data[$parent]['title'];
                }
            }
            unset($item['parent_id']);
            array_push($res[$parent]['child'], $item);
        }
        $data = $res;
        return $data;
    }

    public static function show($item, $level) {
        if ($item['id'] === null) return;
        $style = '';
        if ($level == 0){$style='padding-top:20px; font-weight:bold;';}


        $margin_left = 0;
        while ($level --> 0) {
            $margin_left = $margin_left + 40;
        }

        $style = $style." margin-left:".$margin_left."px;";

        $color = '';
        if (isset($_GET['cats_id'])){
            if ((int)$_GET['cats_id'] == $item['id'])
            {
                $color = "color:#ff0000;";
            }
        }

        echo "<div style='".$style."'><a href='".Yii::app()->createUrl('exposure/index',array('cats_id'=>$item['id']))."' style='".$color."'>".$item['title']."</a></div>";
    }

    public static function print_tree($data, $level = -1) {
        ExposureCats::show($data, $level);
        foreach ($data['child'] as $item) {
            ExposureCats::print_tree($item, $level + 1);
        }
    }



    public static function show_array($item, $level) {
        if ($item['id'] === null) return;
        $t = '';
        while ($level --> 0) {
            $t = $t.'....';
        }
        return $t.$item['title'];
    }

    public static function array_tree($data, $level = -1) {
        ExposureCats::show_array($data, $level);
        foreach ($data['child'] as $item) {
            ExposureCats::array_tree($item, $level + 1);
        }
    }

    public function getCatsArray(){
        /*
        $cats = ExposureCats::model()->findAll(array('order'=>'id asc, parent_id asc'));

        $cats_array = array();
        foreach($cats as $cat){
            $cats_array[] = array('id'=>$cat->id, 'title'=>$cat->title, 'parent_id' => $cat->parent_id);
        }
        $c = ExposureCats::restore_tree($cats_array);
        ExposureCats::array_tree($c);

        print_r($this->exposure_cats_array);
        */

        return $this->exposure_cats_array;
    }


}
