<?php

/**
 * Модель статьи в блоге
 */
class ExposureItems extends CActiveRecord
{

    const IMAGE_WIDTH = 200;
    const IMAGE_HEIGHT = 100;

    public $image;

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'exposure_item';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('id,title,text,cover_id,short_text,cats_id,tags,exposure_id,news_id,redbook,opeka', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'news' => array(self::HAS_MANY, 'News', 'exposureitem_id', 'on' => "is_closed=0", 'order' => 'date desc'),
            'photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{photos}}.type = 'exposure_item' and deleted=0", 'order' => 'id'),
            'some_photo' => array(self::HAS_ONE, 'Photos', 'post_id', 'on' => "{{some_photo}}.type = 'exposure_item' and deleted=0", 'order' => 'id'),
            'docs' => array(self::HAS_MANY, 'Docs', 'post_id', 'on' => "{{docs}}.type = 'exposure_item'", 'order' => 'id'),
            'cover' => array(self::BELONGS_TO, 'Photos', 'cover_id'),
            'exposure' => array(self::BELONGS_TO, 'Exposure', 'exposure_id'),
            'exposurecats' => array(self::BELONGS_TO, 'ExposureCats', 'cats_id'),
            'custodian' => array(self::HAS_ONE, 'Partners', 'animals_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'title' => 'Заголовок',
            'text' => 'Полное описание',
            'short_text' => 'Краткое описание',
            'cats_id' => 'Каталог',
            'exposure_id' => 'Эскпозиция',
            'redbook' => 'Красная Книга',
            'opeka' => 'Опека',
        );
    }

    public function getImageUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return '';
        }
    }

    public function hasImage()
    {
        if ($this->cover)
        {
            $file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$this->cover->filename;
            return file_exists($file);
        }else{
            return false;
        }
    }

    public function getThumbnailUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/s_' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/s_' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return $this->getImageUrl();
        }
    }

    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/photos/'.$size.$this->cover->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }

    public static function getMainList()
    {
        //return News::model()->findAll(array('condition'=>'','order'=>'date desc','limit'=>6));
    }

    protected function afterDelete()
    {
        //parent::afterDelete();
        //$this->removeImage();
    }

    public static function autoCover(ExposureItems $post)
    {
        if ($post){
            if ($post->cover_id == 0){
                if ($post->some_photo)
                {
                    $post->cover_id = $post->some_photo->id;
                    $post->save();
                    return array(
                        'photo_id' => $post->cover_id,
                        'photo_adress' => $post->getThumbnailUrl(),
                    );
                }
            }
        }
    }

    public static function getExposureAnimals($id=0)
    {
        $condition = '';
        if ($id > 0){
        $condition = 'exposure_id = "'.$id.'"';
        }
        return ExposureItems::model()->findAll(array('condition'=>$condition,'order'=>'cats_id ASC'));
    }
}
