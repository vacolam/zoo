<?php

/**
 * Модель статьи в блоге
 */
class Groups extends CActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'groups';
    }

    public function rules()
    {
        return array(
            array('order_date', 'required'),
            array('id,order_date,name,phone,school_name', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'orders' => array(self::HAS_MANY, 'GroupsOrders', 'groups_id', 'on' => "", 'order' => 'id asc'),
            'photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{photos}}.type = 'groups_orders' and deleted=0", 'order' => 'id'),
            'some_photo' => array(self::HAS_ONE, 'Photos', 'post_id', 'on' => "{{some_photo}}.type = 'groups_orders' and deleted=0", 'order' => 'id'),
            'docs' => array(self::HAS_MANY, 'Docs', 'post_id', 'on' => "{{docs}}.type = 'groups_orders'", 'order' => 'id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'date' => 'Дата',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'short_text' => 'Краткое описание',
        );
    }


}