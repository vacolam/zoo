<?php

/**
 * Модель статьи в блоге
 */
class GroupsOrders extends CActiveRecord
{
    
    const IMAGE_WIDTH = 200;
    const IMAGE_HEIGHT = 100;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'groups_orders';
    }

    public function rules()
    {
        return array(
            array('groups_id', 'required'),
            array('groups_id, afisha_id, date, order_date, name, phone, kolichestvo, time_id, time_text, week_day, status, school_name, vozrast, bus_time, bus_route, bus_need', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'afisha' => array(self::BELONGS_TO, 'Afisha', 'afisha_id', 'on' => "", 'order' => ''),
            'group' => array(self::BELONGS_TO, 'Groups', 'groups_id', 'on' => "", 'order' => ''),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'date' => 'Дата',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'short_text' => 'Краткое описание',
        );
    }

    public function getImageUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return '';
        }
    }
    
    public function hasImage()
    {
        if ($this->cover)
        {
            $file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$this->cover->filename;
            return file_exists($file);
        }else{
            return false;
        }
    }

    public function getThumbnailUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/s_' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/s_' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return $this->getImageUrl();
        }
    }
    
    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/photos/'.$size.$this->cover->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }

}