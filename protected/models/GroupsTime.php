<?php

/**
 * Модель статьи в блоге
 */
class GroupsTime extends CActiveRecord
{
    
    const IMAGE_WIDTH = 200;
    const IMAGE_HEIGHT = 100;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'groups_time';
    }

    public function rules()
    {
        return array(
            array('time', 'required'),
            array('week_day,time,number,groups_id,deleted', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(

        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'week_date' => 'День недели',
            'time' => 'Время',
            'number' => 'Порядковый номер',
            'groups_id' => 'Номер группы',
            'deleted' => 'Удалено',
        );
    }


}