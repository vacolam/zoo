<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class InfoBlocks extends CActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'infoblocks';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('link', 'url'),
            array('title, description', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'link' => 'Ссылка',
            'title' => 'Заголовок',
            'description' => 'Описание',
        );
    }
    
    protected function afterDelete()
    {
    }

    public static function getInfoBlocks()
    {
        $banners = InfoBlocks::model()->findAll(array('condition'=>'','order'=>'id asc'));
        return $banners;
    }
}