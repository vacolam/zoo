<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class Link extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'links';
    }

    public function rules()
    {
        return array(
            array('title, link', 'required'),
            array('link', 'url'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'title' => 'Заголовок',
            'link' => 'Ссылка',
        );
    }

    public static function getLinks()
    {
        $links = Link::model()->findAll();
        return $links;
    }
}