<?php

/**
 * Модель статьи в блоге
 */
class News extends CActiveRecord
{

    const IMAGE_WIDTH = 200;
    const IMAGE_HEIGHT = 100;

    public $image;

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'news';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            /*array('date', 'date', 'format' => 'yyyy-MM-dd'),*/
            array('short_text, filename, pre_text,tags,cover_id, exposureitem_id, is_closed, closed_till, closed_till_time', 'safe'),
            array('image', 'file', 'types' => 'jpg', 'allowEmpty' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{photos}}.type = 'news' and deleted=0", 'order' => 'id'),
            'some_photo' => array(self::HAS_ONE, 'Photos', 'post_id', 'on' => "{{some_photo}}.type = 'news' and deleted=0", 'order' => 'id'),
            'docs' => array(self::HAS_MANY, 'Docs', 'post_id', 'on' => "{{docs}}.type = 'news'", 'order' => 'id'),
            'cover' => array(self::BELONGS_TO, 'Photos', 'cover_id'),
            'animal' => array(self::BELONGS_TO, 'ExposureItems', 'exposureitem_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'date' => 'Дата',
            'title' => 'Заголовок',
            'text' => 'Текст новости',
            'short_text' => 'Краткое описание',
            'pre_text' => 'Текст новости',
            'image' => 'Обложка',
            'filename' => 'Имя файла',
            'exposureitem_id' => 'Животное',
            'is_closed' => 'Публикация',
        );
    }

    public function getImageUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return '';
        }
    }

    public function hasImage()
    {
        if ($this->cover)
        {
            $file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$this->cover->filename;
            return file_exists($file);
        }else{
            return false;
        }
    }

    public function getThumbnailUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/s_' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/s_' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return $this->getImageUrl();
        }
    }

    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/photos/'.$size.$this->cover->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }

    public static function getMainList()
    {
        return News::model()->findAll(array('condition'=>'is_closed=0','order'=>'date DESC, id DESC','limit'=>6));
    }

    protected function afterDelete()
    {
        //parent::afterDelete();
        //$this->removeImage();
    }

    public static function autoCover(News $post)
    {
        if ($post){
            if ($post->cover_id == 0){
                if ($post->some_photo)
                {
                    $post->cover_id = $post->some_photo->id;
                    $post->save();
                    return array(
                        'photo_id' => $post->cover_id,
                        'photo_adress' => $post->getThumbnailUrl(),
                    );
                }
            }
        }
    }
}
