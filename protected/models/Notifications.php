<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class Notifications extends CActiveRecord
{

    CONST WIDTH = 1100;
    CONST HEIGHT = 600;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'notifications';
    }

    public function rules()
    {
        return array(
            array('order_id', 'required'),
            array('order_id, user_id, id,type', 'safe'),
        );
    }


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'yarmarka' => array(self::BELONGS_TO, 'Yarmarka', 'order_id'),
            'groups' => array(self::BELONGS_TO, 'GroupsOrders', 'order_id'),
            'confwork' => array(self::BELONGS_TO, 'ConfWorks', 'order_id'),
            'festival' => array(self::BELONGS_TO, 'ConfWorks', 'order_id'),
            'konkurs' => array(self::BELONGS_TO, 'ConfWorks', 'order_id'),
            'opros' => array(self::BELONGS_TO, 'ConfWorks', 'order_id'),
            'ocenka' => array(self::BELONGS_TO, 'Ocenka', 'order_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'name' => 'Имя',
            'email' => 'e-mail',
            'phone' => 'Телефон',
            'nom_id' => 'Номинация',
            'conf_id' => 'Конференция',
            'filename' => 'Имя файла',
            'mark' => 'Сумма оценок',
            'date' => 'Дата',
        );
    }

    public static function deleteNotification($type,$order_id){
        if ($type != '' and $order_id != ''){
		$query = 'DELETE FROM {{notifications}} where type="'.$type.'" and order_id="'.$order_id.'" and user_id="'.Yii::app()->user->id.'"';
		Yii::app()->db->createCommand($query)->execute();
        }
    }

    public static function countNotification(){
        $notifications = Notifications::model()->findAll(array('condition'=>'user_id="'.Yii::app()->user->id.'"'));
        return count($notifications);
    }
}