<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class Ocenka extends CActiveRecord
{

    CONST WIDTH = 300;
    CONST HEIGHT = 150;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'ocenka';
    }

    public function rules()
    {
        return array(
            array('result', 'required'),
            array('date,name,city,phone,email,id,result', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'link' => 'Ссылка',
            'image' => 'Картинка',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'filename' => 'Имя файла',
            'type' => 'Тип партнерства',
            'animals_id' => 'Каких животных опекает',
        );
    }
}