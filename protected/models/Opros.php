<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class Opros extends CActiveRecord
{

    CONST WIDTH = 600;
    CONST HEIGHT = 300;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'opros';
    }

    public function rules()
    {
        return array(
            array('question', 'required'),
            array('image', 'file', 'types' => 'jpg', 'on' => 'insert', 'allowEmpty' => true),
            array('image', 'file', 'types' => 'jpg', 'on' => 'update', 'allowEmpty' => true),
            array('question,right_answer,wrong_answer, filename,number,description', 'safe'),
        );
    }

   /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(

        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'image' => 'Картинка',
            'question' => 'Вопрос',
            'right_answer' => 'Правильный ответ',
            'wrong_answer' => 'Неправильный ответ',
            'number' => 'Порядковый номер',
            'description' => 'Описание правильного ответа',
        );
    }
    
    public function getImageUrl()
    {
        return Yii::app()->getBaseUrl() . '/upload/opros/' . $this->filename;
    }

    public function getFileUrl()
    {
        return Yii::getPathOfAlias('webroot').'/upload/opros/'.$this->filename;
    }

    protected function afterDelete()
    {

    }

    public static function deleteQuestion(Opros $question)
    {
        if ($question->filename != '')
        {
            $file = Yii::getPathOfAlias('webroot').'/upload/opros/'.$question->filename;
            if (file_exists($file)) {
                unlink($file);
            }
        }

        $question->delete();
    }
}