<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class Orders extends CActiveRecord
{

    CONST WIDTH = 1100;
    CONST HEIGHT = 600;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'orders';
    }

    public function rules()
    {
        return array(
            array('order_id', 'required'),
            array('id,date,type,order_id,time', 'safe'),
        );
    }


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'yarmarka' => array(self::BELONGS_TO, 'Yarmarka', 'order_id'),
            'group' => array(self::BELONGS_TO, 'Groups', 'order_id'),
            'confwork' => array(self::BELONGS_TO, 'ConfWorks', 'order_id'),
            'festival' => array(self::BELONGS_TO, 'ConfWorks', 'order_id'),
            'konkurs' => array(self::BELONGS_TO, 'ConfWorks', 'order_id'),
            'opros' => array(self::BELONGS_TO, 'ConfWorks', 'order_id'),
            'ocenka' => array(self::BELONGS_TO, 'Ocenka', 'order_id'),
            'shopOrder' => array(self::BELONGS_TO, 'ShopOrders', 'order_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'name' => 'Имя',
            'email' => 'e-mail',
            'phone' => 'Телефон',
            'nom_id' => 'Номинация',
            'conf_id' => 'Конференция',
            'filename' => 'Имя файла',
            'mark' => 'Сумма оценок',
            'date' => 'Дата',
        );
    }

    public static function createOrder($type,$order_id){

            //создаем заявку
            $order = new Orders();
            $order->date = new CDbExpression('NOW()');
            $order->time = new CDbExpression('DATE_ADD(NOW(), INTERVAL 8 HOUR)');
            $order->type = $type;
            $order->order_id = $order_id;
            $order->save();

            //создаем уведомление
            $users = User::model()->findAll(array('condition'=>'(role="manager" or role="admin") AND notify like "%'.$type.'%"'));     // 
            if ($users){
                foreach($users as $user){
                    $notification = new Notifications();
                    $notification->user_id = $user->id;
                    $notification->order_id = $order_id;
                    $notification->type = $type;
                    $notification->save();

                $titleMessage = Orders::messageType($type);

                $link = 'https://sakhalinzoo.ru/zoo.php?r=dashboard/index&type='.$type; //Yii::app()->createAbsoluteUrl('dashboard/index',array('type'=>$type));

                $query = "INSERT INTO {{webpush_emails}} (user_id, message, title, link)"
    						." VALUES (:user, :message, :title, :link)";
    					$params = array(
    						'user' => $user->id,
    						'message' => $titleMessage['message'],
    						'title' => $titleMessage['title'],
    						'link' => $link,
    					);
    					Yii::app()->db->createCommand($query)->execute($params);
                }
            }

            //создаем веб-пуш
            $query = "SELECT
                        distinct user_id as user_id
                    FROM {{webpush_users}}
                    WHERE
                        id > 0
                    ";
            $params = array();
            $webpush_users = Yii::app()->db->createCommand($query)->queryAll();

            foreach($webpush_users as $index => $user)
            {
                $usr = User::model()->findByPk($user['user_id']);
                if($usr){
                $notify_array = User::getNotifications($usr);
                }
                if (in_array($type,$notify_array))
                {
                $titleMessage = Orders::messageType($type);

                $link = 'https://sakhalinzoo.ru/zoo.php?r=dashboard/index&type='.$type; //Yii::app()->createAbsoluteUrl('dashboard/index',array('type'=>$type));

                $query = "INSERT INTO {{webpush_messages}} (user_id, message, title, link)"
    						." VALUES (:user, :message, :title, :link)";
    					$params = array(
    						'user' => $user['user_id'],
    						'message' => $titleMessage['message'],
    						'title' => $titleMessage['title'],
    						'link' => $link,
    					);
    					Yii::app()->db->createCommand($query)->execute($params);
                }
            }
    }


    public static function messageType($type)
    {
                $title = 'Заявка';
                $message = 'На сайте новая заявка. посмотрите.';

                if ($type == 'yarmarka'){
                    $title = 'Ярмарка';
                    $message = 'Новая заявка на участие в ярмарке.';

                }
                if ($type == 'groups'){
                    $title = 'Группы';
                    $message = 'Новая заявка на групповое мероприятие.';

                }
                if ($type == 'conf'){
                    $title = 'Конференции';
                    $message = 'Новая работа для конференции';

                }
                if ($type == 'festival'){
                    $title = 'Фестиваль';
                    $message = 'Новая работа для фестиваля';

                }
                if ($type == 'konkurs'){
                    $title = 'Конкурс';
                    $message = 'Новая работа для конкурса';

                }
                if ($type == 'opros'){
                    $title = 'Опрос';
                    $message = 'Пользователь прошел опрос';

                }
                if ($type == 'ocenka'){
                    $title = 'Оценка';
                    $message = 'На сайте оценили работу зоопарка';
                }
                if ($type == 'shopOrder'){
                    $title = 'Заказ в магазине';
                    $message = 'Новый заказ в магазине';
                }

                return array(
                    'title'     => $title,
                    'message'   => $message,
                );
    }


}