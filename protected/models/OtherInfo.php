<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class OtherInfo extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'other_info';
    }

    public function rules()
    {
        return array(
            array('link', 'required'),
            array('link,title', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'title' => 'Заголовок',
            'link' => 'Ссылка',
        );
    }

    public static function getInfo()
    {
        $info = OtherInfo::model()->findAll();
        return $info;
    }

    public static function getTimetable()
    {
        $info = OtherInfo::model()->findByPk(1);
        return $info;
    }

    public static function getPrice()
    {
        $info = OtherInfo::model()->findByPk(2);
        return $info;
    }

    public static function getContacts()
    {
        $info = OtherInfo::model()->findByPk(3);
        return $info;
    }
}