<?php

/**
 * Модель страницы
 */
class Page extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'pages';
    }

    public function rules()
    {
        return array(
            array('id', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'content' => 'Содержание',
        );
    }

    public static function getContatcs()
    {
        $contacts_array = array('company_name','company_phone_1','company_phone_2','company_email','company_city','company_index','company_adress','company_social_fb','company_social_vk','company_social_ok','company_social_tw','company_social_inst','company_social_youtube','company_social_telegram','company_social_whatsapp','metrics','company_license_number','company_license_link','company_shortname','company_founder_site','company_founder_email','company_founder_adress','company_founder_ceo','company_founder_name');
        $contacts = Page::model()->findAll(array('condition'=>'id IN ("'.implode('","',$contacts_array).'")'));

        $result = array();
        if ($contacts){
            foreach($contacts as $contact)
            {
                $result[$contact['id']] = $contact['content'];
            }
        }

        return $result;
    }

    public static function getEngContatcs()
    {
        $contacts_array = array('company_name','company_phone_1','company_phone_2','company_email','company_city','company_index','company_adress','company_social_fb','company_social_vk','company_social_ok','company_social_tw','company_social_inst','company_social_youtube','company_social_telegram','company_social_whatsapp','metrics','company_license_number','company_license_link','company_shortname','company_founder_site','company_founder_email','company_founder_adress','company_founder_ceo','company_founder_name');
        $contacts = Page::model()->findAll(array('condition'=>'id IN ("'.implode('","',$contacts_array).'")'));

        $result = array();
        if ($contacts){
            foreach($contacts as $contact)
            {
                $result[$contact['id']] = $contact['content_eng'];
            }
        }

        return $result;
    }

    public static function getRaspisanie()
    {
        $contacts_array = array('raspis_time','raspis_description','raspis_children_1','raspis_children_2','raspis_children_3','raspis_children_3_text','raspis_vzrosliy_1','raspis_vzrosliy_2','raspis_vzrosliy_3','raspis_vzrosliy_3_text','raspis_pens_1','raspis_pens_2','raspis_pens_3','raspis_pens_3_text','winter_color','spring_color','summer_color','fall_color','buy_ticket_name');
        $contacts = Page::model()->findAll(array('condition'=>'id IN ("'.implode('","',$contacts_array).'")'));

        $result = array();
        if ($contacts){
            foreach($contacts as $contact)
            {
                $result[$contact['id']] = $contact['content'];
            }
        }

        return $result;
    }


    public static function getEngRaspisanie()
    {
        $contacts_array = array('raspis_time','raspis_description','raspis_children_1','raspis_children_2','raspis_children_3','raspis_children_3_text','raspis_vzrosliy_1','raspis_vzrosliy_2','raspis_vzrosliy_3','raspis_vzrosliy_3_text','raspis_pens_1','raspis_pens_2','raspis_pens_3','raspis_pens_3_text','buy_ticket_name');
        $contacts = Page::model()->findAll(array('condition'=>'id IN ("'.implode('","',$contacts_array).'")'));

        $result = array();
        if ($contacts){
            foreach($contacts as $contact)
            {
                $result[$contact['id']] = $contact['content_eng'];
            }
        }

        return $result;
    }

    public static function getRaspisanieColors()
    {
        $contacts_array = array('winter_color','spring_color','summer_color','fall_color');
        $contacts = Page::model()->findAll(array('condition'=>'id IN ("'.implode('","',$contacts_array).'")'));

        $result = array();
        if ($contacts){
            foreach($contacts as $contact)
            {
                $result[$contact['id']] = $contact['content'];
            }
        }

        return $result;
    }

    public static function getTickets()
    {
        $contacts_array = array('buy_ticket_code','buy_ticket_script','buy_ticket_show');
        $contacts = Page::model()->findAll(array('condition'=>'id IN ("'.implode('","',$contacts_array).'")'));

        $result = array();
        if ($contacts){
            foreach($contacts as $contact)
            {
                $result[$contact['id']] = $contact['content'];
            }
        }

        return $result;
    }

    public static function getRaspisanieWords($lang = 'ru')
    {
        if ($lang == 'eng')
        {
            $words = array(
                '1'=>'TIMETABLE',
                '2'=>'CHILDREN',
                '3'=>'ADULTS',
                '4'=>'PENSIONER',
                '5'=>'1-day pass',
                '6'=>'membership',
                '7'=>'CONTACTS',
                '8'=>'Watch on the map',
                '9'=>'RUB',
            );
        }else{
            $words = array(
                '1'=>'РАСПИСАНИЕ',
                '2'=>'ДЕТИ',
                '3'=>'ВЗРОСЛЫЕ',
                '4'=>'ПЕНСИОНЕРЫ',
                '5'=>'Входной билет',
                '6'=>'Абонемент',
                '7'=>'КАК НАС НАЙТИ?',
                '8'=>'Посмотреть на карте',
                '9'=>'РУБ',
            );
        }

        return $words;
    }
}
