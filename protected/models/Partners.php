<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class Partners extends CActiveRecord
{

    CONST WIDTH = 300;
    CONST HEIGHT = 150;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'partners';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('link', 'url'),
            array('image', 'file', 'types' => 'jpg,png,gif,jpeg', 'on' => 'insert', 'allowEmpty' => true),
            array('image', 'file', 'types' => 'jpg,png,gif,jpeg', 'on' => 'update', 'allowEmpty' => true),
            array('title, description, filename, type, animals_id', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'animal' => array(self::BELONGS_TO, 'ExposureItems', 'animals_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'link' => 'Ссылка',
            'image' => 'Картинка',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'filename' => 'Имя файла',
            'type' => 'Тип партнерства',
            'animals_id' => 'Каких животных опекает',
        );
    }
    
    public function getImageUrl()
    {
        return Yii::app()->getBaseUrl() . '/upload/partners/' . $this->filename;
    }

    protected function afterDelete()
    {
        parent::afterDelete();
        $file = Yii::getPathOfAlias('webroot').'/upload/partners/'.$this->filename;
        if (file_exists($file)) {
            unlink($file);
        }
    }

    public static function getMain()
    {
        $banners = Partners::model()->findAll(array('condition'=>'type="partner" and filename !=""','limit'=>6,'order'=>'rand()'));
        return $banners;
    }

    public static function getPartners()
    {
        $banners = Partners::model()->findAll(array('condition'=>'type="partner"'));
        return $banners;
    }

    public static function getOpekuns()
    {
        $banners = Partners::model()->findAll(array('condition'=>'type="custodian"'));
        return $banners;
    }
}