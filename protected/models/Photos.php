<?php

/**
 * Модель статьи в блоге
 */
class Photos extends CActiveRecord
{
    
    const IMAGE_WIDTH = 200;
    const IMAGE_HEIGHT = 100;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'photos';
    }

    public function rules()
    {
        return array(
            array('post_id', 'required'),
            array('id, filename, date, type, subdir,deleted', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'post_id' => 'ID поста',
            'filename' => 'Имя файла',
            'date' => 'дата добавления',
            'type' => 'тип',
        );
    }

    public function getImageUrl()
    {
        return Yii::app()->getBaseUrl() . '/upload/photos/' . $this->filename;
    }

    public function getThumbnailUrl()
    {
        $path = Yii::getPathOfAlias('webroot') . '/upload/photos/s_' . $this->filename;
        if (file_exists($path))
        {
            return Yii::app()->getBaseUrl() . '/upload/photos/s_' . $this->filename;;
        }else{
            return $this->getImageUrl();
        }
    }
    
    public function hasImage()
    {
        $file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$this->filename;
        return file_exists($file);
    }
    
    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/photos/'.$size.$model->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }

    protected function afterDelete()
    {
        parent::afterDelete();
        $this->removeImage();
    }

    public static function deleteImage(Photos $model)
    {
        foreach(array('','s_') as $size)
        {
            $deleted_path = Yii::getPathOfAlias('webroot').'/upload/photos/'.$size.$model->filename;
            if (file_exists($deleted_path)) {
                unlink($deleted_path);
            }
        }
        $model->delete();
    }
}