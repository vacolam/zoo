<?php

/**
 * Модель статьи в блоге
 */
class RewardsCats extends CActiveRecord
{
    
    const IMAGE_WIDTH = 200;
    const IMAGE_HEIGHT = 100;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'rewards_cats';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            /*array('date', 'date', 'format' => 'yyyy-MM-dd'),*/
            array('id,title,number', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{photos}}.type = 'rewards'", 'order' => 'id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'title' => 'Заголовок',
            'number' => 'Порядковый номер',

        );
    }

    public static function getPhotos($cat_id = 0){

        $docs = new CActiveDataProvider('Photos', array(
            'criteria' => array(
                'condition' => 'type = "rewards" And post_id = "'.$cat_id.'"',
                'order' => 'date, id',
            ),
            'pagination' => array(
                'pageSize' => 500,
            ),
        ));

        return $docs;
    }

    public function getImageUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return '';
        }
    }
    
    public function hasImage()
    {
        if ($this->cover)
        {
            $file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$this->cover->filename;
            return file_exists($file);
        }else{
            return false;
        }
    }

    public function getThumbnailUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/s_' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/s_' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return $this->getImageUrl();
        }
    }
    
    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/photos/'.$size.$this->cover->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }
}