<?php

/**
 * Настройка
 */
class Setting extends CActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'settings';
    }

    public function rules()
    {
        return array(
            array('value', 'required'),
        );
    }
}