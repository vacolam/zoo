<?php

/**
 * Модель статьи в блоге
 */
class ShopCats extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'shop_cats';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('id,title,text,parent_id,cover_id,sort', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id'=>'номер',
            'title'=>'Название',
            'text'=>'Описание',
            'parent_id'=>'Родительская Категория',
            'sort'=>'Порядковый номер',
        );
    }

     public static function getSubmenu(){
        $submenu = array();
        $cats = ShopCats::model()->findAll(array('condition'=>'parent_id=0','order'=>'sort asc, id asc'));
        foreach($cats as $cat)
        {
            $submenu[$cat->id] = array(
                'title'=>$cat->title,
                'sub'=>array(),
            );
            $sub_cats = ShopCats::model()->findAll(array('condition'=>'parent_id='.$cat->id,'order'=>'sort asc, id asc'));
            if ($sub_cats)
            {
                foreach($sub_cats as $sub){
                    $submenu[$cat->id]['sub'][$sub->id] = array(
                        'title'=>$sub->title,
                    );
                }
            }
        }
        return $submenu;
    }




    public static function getArray()
    {
        $array = array();
        $AllowedShopArray = User::getAllowedShop();
        $cats = ShopCats::model()->findAll(array('condition'=>'parent_id=0','order'=>'id asc, parent_id asc'));
        $array[0] = 'Нет категорий';
        foreach($cats as $cat)
        {
            if (User::checkAllowedShop($cat->id,$AllowedShopArray))
            {
                $array[$cat->id] = $cat->title;
                $sub_cats = ShopCats::model()->findAll(array('condition'=>'parent_id='.$cat->id,'order'=>'id asc, parent_id asc'));
                if ($sub_cats)
                {
                    foreach($sub_cats as $sub){
                        $array[$sub->id] = '---'.$sub->title;
                    }
                }
            }
        }


        return $array;
    }






}
