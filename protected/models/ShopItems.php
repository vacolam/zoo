<?php

/**
 * Модель статьи в блоге
 */
class ShopItems extends CActiveRecord
{

    const IMAGE_WIDTH = 200;
    const IMAGE_HEIGHT = 100;

    public $image;

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'shop_item';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('id,title,text,cover_id,short_text,cats_id,price, old_price, quontity, order, deleted,hidden', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'photos' => array(self::HAS_MANY, 'Photos', 'post_id', 'on' => "{{photos}}.type = 'shop' and deleted=0", 'order' => 'id'),
            'some_photo' => array(self::HAS_ONE, 'Photos', 'post_id', 'on' => "{{some_photo}}.type = 'shop' and deleted=0", 'order' => 'id'),
            'docs' => array(self::HAS_MANY, 'Docs', 'post_id', 'on' => "{{docs}}.type = 'shop'", 'order' => 'id'),
            'cover' => array(self::BELONGS_TO, 'Photos', 'cover_id'),
            'shopcats' => array(self::BELONGS_TO, 'ShopCats', 'cats_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id'=>'номер',
            'title'=>'Название',
            'text'=>'Описание',
            'cats_id'=>'Категория',
            'price'=>'Цена',
            'old_price'=>'Старая цена',
            'quontity'=>'Доступное количество',
            'order'=>'Порядковый номер',
            'hidden'=>'Скрыть товар из магазина',
        );
    }

    public function getImageUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return '';
        }
    }

    public function hasImage()
    {
        if ($this->cover)
        {
            $file = Yii::getPathOfAlias('webroot').'/upload/photos/'.$this->cover->filename;
            return file_exists($file);
        }else{
            return false;
        }
    }

    public function getThumbnailUrl()
    {
        $cover_exists = false;
        if ($this->cover)
        {
            $path = Yii::getPathOfAlias('webroot') . '/upload/photos/s_' . $this->cover->filename;
            if (file_exists($path))
            {
                $cover_exists = true;
                return Yii::app()->getBaseUrl() . '/upload/photos/s_' . $this->cover->filename;
            }
        }

        if ($cover_exists == false){
           return $this->getImageUrl();
        }
    }

    public function removeImage()
    {
        if ($this->hasImage()) {
            foreach(array('','s_') as $size)
            {
                $deleted_path = Yii::getPathOfAlias('webroot').'/upload/photos/'.$size.$this->cover->filename;
                if (file_exists($deleted_path)) {
                    unlink($deleted_path);
                }
            }
        }
    }


    protected function afterDelete()
    {
    }

    public static function autoCover(ShopItems $post)
    {
        if ($post){
            if ($post->cover_id == 0){
                if ($post->some_photo)
                {
                    $post->cover_id = $post->some_photo->id;
                    $post->save();
                    return array(
                        'photo_id' => $post->cover_id,
                        'photo_adress' => $post->getThumbnailUrl(),
                    );
                }
            }
        }
    }

    public static function getItems(ShopCats $model, $id=0)
    {
        $condition = 'hidden=0 AND deleted=0';
        $condition_array = array();
        $condition_array[] = $id;

        if ($model->parent_id == 0){
            $sub_cats = ShopCats::model()->findAll(array('condition'=>'parent_id='.$id,'order'=>'id asc, parent_id asc'));
            if ($sub_cats)
            {
                foreach($sub_cats as $sub){
                    $condition_array[] = $sub->id;
                }
            }
        }

        $condition = 'hidden=0 AND deleted=0 AND cats_id IN ('.implode($condition_array,',').') ';

        return ShopItems::model()->findAll(array('condition'=>$condition,'order'=>'cats_id ASC, id DESC'));
    }



}
