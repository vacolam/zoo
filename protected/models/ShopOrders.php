<?php

/**
 * Модель статьи в блоге
 */
class ShopOrders extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'shop_orders';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('id,name,phone,date,adress,deliverytype,status,deleted, comment', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'orderitems' => array(self::HAS_MANY, 'ShopOrdersitems', 'order_id', 'on' => ""),
            'ordersms' => array(self::HAS_MANY, 'ShopSms', 'order_id', 'on' => ""),
            'lastsms' => array(self::HAS_ONE, 'ShopSms', 'order_id', 'order' => "id desc"),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id'=>'номер',
            'name'=>'Имя',
            'phone'=>'Телефон',
            'date'=>'Дата',
            'adress'=>'Адресс',
            'deliverytype'=>'Спосбо доставки',
        );
    }

    public static function countOrders()
    {
        $allowedOrders = ShopOrders::getAllowedOrdersIds();

        $condition = '';
        if (count($allowedOrders)>0)
        {
            $allowedOrders = array_unique($allowedOrders);
            $condition = ' AND id IN ('.implode(',',$allowedOrders).')';
        }
        $orders = ShopOrders::model()->findAll(array('condition'=>'status<1000'.$condition));

        return count($orders);
    }


    public static function getAllowedOrdersIds()
    {
        $AllowedShopArray = User::getAllowedShop();

        $allowedProudcts = array(0);
        if (count($AllowedShopArray)>0)
        {
            $shopItemsAll = ShopItems::model()->findAll(array('condition'=>'cats_id IN ('.implode(',',$AllowedShopArray).')'));


            //print_r($shopItemsAll);
            if ($shopItemsAll)
            {
                $itemslist= CHtml::listData($shopItemsAll,'id','title');

                if (count($shopItemsAll) > 0)
                {
                    $allowedProudcts = array_keys($itemslist);
                }
            }
        }

        $allowedOrders = array(0);
        if (count($allowedProudcts) > 0){
           $ShopOrdersitems = ShopOrdersitems::model()->findAll(array('condition'=>'item_id IN ('.implode(',',$allowedProudcts).')'));
           if ($ShopOrdersitems){
               foreach($ShopOrdersitems as $ordr)
               {
                 $allowedOrders[] = $ordr->order_id;
               }
           }
        }


        return $allowedOrders;
    }

}
