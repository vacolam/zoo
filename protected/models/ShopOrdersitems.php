<?php

/**
 * Модель статьи в блоге
 */
class ShopOrdersitems extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'shop_orders_items';
    }

    public function rules()
    {
        return array(
            array('order_id', 'required'),
            array('id,order_id,item_id,quontity,price,status,deleted', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'product' => array(self::BELONGS_TO, 'ShopItems', 'item_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id'=>'номер',
            'name'=>'Имя',
            'phone'=>'Телефон',
            'date'=>'Дата',
            'adress'=>'Адресс',
            'deliverytype'=>'Спосбо доставки',
        );
    }
}
