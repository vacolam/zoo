<?php

/**
 * Модель статьи в блоге
 */
class ShopSms extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'shop_sms';
    }

    public function rules()
    {
        return array(
            array('order_id', 'required'),
            array('id,date,order_id,phone, order_status, text, send_status, send', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id'=>'номер',
            'date'=>'Название',
            'order_id'=>'Описание',
            'phone'=>'Родительская Категория',
            'order_status'=>'Порядковый номер',
            'text'=>'Порядковый номер',
            'send_status'=>'Порядковый номер',
            'send'=>'Порядковый номер',
        );
    }

    public static function addSms(ShopOrders $order, $new_status)
    {
        if ($order->phone != '')
        {
            $status_text = array(
                0 => 'Принят',
                1 => 'Готов к выдаче',
                2 => 'Частично готов к выдаче',
                1000 => 'Оплачен и выдан',
                10000 => 'Отменен продавцом',
                10001 => 'Отменен покупателем',
            );

            $text = '';

            if ($new_status == 0        ){$text     = 'Привет. Это сахалинский Зоопарк. Ваш заказ № '.$order->id.' принят';}
            if ($new_status == 1        ){$text     = 'Привет. Это сахалинский Зоопарк. Ваш заказ № '.$order->id.' Готов к выдаче';}
            if ($new_status == 2        ){$text     = 'Привет. Это сахалинский Зоопарк. Ваш заказ № '.$order->id.' Готов к выдаче';}
            if ($new_status == 1000     ){$text     = 'Привет. Это сахалинский Зоопарк. Ваш заказ № '.$order->id.' Выдан. Спасибо за покупку. ';}
            if ($new_status == 10000    ){$text     = 'Привет. Это сахалинский Зоопарк. Ваш заказ № '.$order->id.' отменен';}
            if ($new_status == 10001    ){$text     = 'Привет. Это сахалинский Зоопарк. Ваш заказ № '.$order->id.' отменен';}

            $new_sms                = new ShopSms();
            $new_sms->date          = new CDbExpression('NOW()');
            $new_sms->order_id      = $order->id;
            $new_sms->phone         = $order->phone;
            $new_sms->text          = $text;
            $new_sms->order_status  = $new_status;
            $new_sms->send          = 0;
            $new_sms->save();
        }
    }

    public static function getSmsByStatus(ShopOrders $order, $status)
    {
        $result = false;
        $sms = ShopSms::model()->findAll(array('condition'=>'order_id="'.$order->id.'" and order_status="'.$status.'" order by id asc limit 1'));
        if (isset($sms[0]))
        {
        $result  = $sms[0];
        }
        return $result;
    }



}
