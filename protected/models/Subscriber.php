<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class Subscriber extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'subscribers';
    }

    public function rules()
    {
        return array(
            array('email', 'required'),
            array('email', 'unique', 'message' => 'Адрес {value} уже подписан на рассылку'),
            array('email', 'email'),
        );
    }
}