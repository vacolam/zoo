<?php
class User extends CActiveRecord implements UserInterface
{

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return array(
            array('email', 'required'),
            array('name,role,password,accesss,role_id,notify', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'nominations' => array(self::HAS_MANY, 'ConfNomJury', 'user_id', 'on' => "", 'order' => ''),
            'role' => array(self::BELONGS_TO, 'UsersRoles', 'role_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
        );
    }

	/**
	 * @return int айди пользователя
	 */
	public function getId()
	{
		return $this->id;
	}

    public static function userNomPermission($nom_id,$user_id){
         $result = false;
         $user = User::model()->findByPk($user_id);
         if ($user)
         {
             $nominations = $user->nominations;
             if ($nominations){
                 foreach($nominations as $nomination)
                 {
                     if ($nomination->nom_id == $nom_id){$result = true;}
                 }
             }

             if($user->role == 'admin'){$result = true;}
         }

         return $result;
    }

    public static function getAllowedCats(){
        $result = array();
        $result = json_decode(Yii::app()->user->accesss,true);
        if (Yii::app()->user->role == 'admin' OR Yii::app()->user->role == 'user')
        {
        $result['all'] = array();
        }
        return $result;
    }

    public static function getActiveCatID($active_controller){
        $active_cats_id = 0;
        if ($active_controller != '')
        {
            $cat_one = false;
            if ($active_controller != 'pages'){
                $cat_one = Cats::model()->findAll(array('condition'=>'plugin="'.$active_controller.'"'));
            }else{
                if (isset($_GET['id']))
                {
                    $cat_one = Cats::model()->findAll(array('condition'=>'id="'.(int)$_GET['id'].'"'));
                }
            }

            if ( isset($cat_one[0]) )
            {
                if ($cat_one[0]->cats_id == 0){
                    $active_cats_id = $cat_one[0]->id;
                }else{
                    $active_cats_id = $cat_one[0]->cats_id;
                }
            }

        }
        return $active_cats_id;
    }

    public static function checkCat($active_cats_id,$allowed_cats){
        if (array_key_exists($active_cats_id,$allowed_cats) OR array_key_exists('all',$allowed_cats))
        {
            return true;
        }else{
            return false;
        }
    }

    public static function openAllowedCat($active_controller,$parameter = 0){
        $allowed_cats      =   User::getAllowedCats();
        $active_cats_id     =   User::getActiveCatID($active_controller);;

        if (User::checkCat($active_cats_id,$allowed_cats) == true OR in_array(Yii::app()->controller->id,array('photos','cats','dashboard','site')))
        {
            return true;
        }else{
            return false;
        }
    }


    public static function checkSubCat($allowed_cats, $cat,$checking_subid){

        $allowed_subcats = array();

        if (isset($allowed_cats[$cat]))
        {
            $allowed_subcats = $allowed_cats[$cat];
        }


        if ( in_array($checking_subid,$allowed_subcats) )
        {
            return true;
        }else{
            return false;
        }
    }

    public static function getAllowedPlugins(){
        $allowed_cats      =   User::getAllowedCats();
        $plugins  =  array();
        $shop_cat_id = Cats::getCatIdByPlugin('shop');
        foreach($allowed_cats as $index => $sub_array)
        {
            if (count($sub_array)>0)
            {
                foreach($sub_array as $sub)
                {
                    $u ='';
                    if ($index == $shop_cat_id){$u='shop_';}
                    $plugins[] = $u.$sub;
                }
            }
        }
        if(User::checkCat($shop_cat_id,$allowed_cats)){$plugins[] = 'shop';$plugins[]='shopOrder';}


       return $plugins;
    }

    public static function checkAllowedPlugins($allowed_plugins, $plugin){

        if (is_array($allowed_plugins))
        {
            if (count($allowed_plugins)>0)
            {
                if (in_array($plugin,$allowed_plugins)){
                    return true;
                }
                else{
                    return false;
                }
            }
        }
    }

    public static function getNotifications($user){
        $result = array();
        $result = json_decode($user->notify,true);
        if (!is_array($result)){
            $result = array();
        }
        return $result;
    }

   public static function getAllowedShop(){
       //1.найти номер категории магазина
       //2.получить массив доступов
       //3.найти в массиве доступов  значение магазина
       //4. выделить его в качестве массива
       //5. создать массив всех категорий и их подкатегорий доступных для работы
       //6. передать

       //1.
       $cat_one = false;
       $shop_id = 0;
       $cat_one = Cats::model()->findAll(array('condition'=>'plugin="shop" and cats_id=0'));
       if ($cat_one){
           if (isset($cat_one[0])){
                $shop_id = $cat_one[0]->id;
           }
       }

       //2.
       $allowed_cats      =   User::getAllowedCats();

       //3.
       $sub_array = array();
       if (array_key_exists($shop_id,$allowed_cats))
       {
            $sub_array    =   $allowed_cats[$shop_id];
       }

       //4.
       $array = array();
       if (is_array($sub_array)){
           if (count($sub_array)>0)
           {
                $array = $sub_array;
           }
       }

       //5.
       $result = array();
       $cats = ShopCats::model()->findAll();
       if ($cats){
           foreach($cats as $cat)
           {
                if (in_array('all',$array) OR in_array($cat->id,$array) OR in_array($cat->parent_id,$array))
                {
                    $result[] = $cat->id;
                }
           }
       }

       //6.
       return $result;
    }

     public static function checkAllowedShop($active_cats_id,$allowed_cats){
        if (in_array($active_cats_id,$allowed_cats) OR in_array('all',$allowed_cats) OR Yii::app()->user->role == 'admin')
        {
            return true;
        }else{
            return false;
        }
    }

    public static function getParentShopID(){
        $parent_cats_id = 0;
        $current_catid=0;
        if( isset($_GET['cats_id']) )
        {
            $current_catid = (int)$_GET['cats_id'];
        }
        if ($current_catid > 0)
        {
            $cat_one = ShopCats::model()->findByPk($current_catid);
            if ($cat_one){
                if ($cat_one->parent_id == 0){
                    $parent_cats_id = $current_catid;
                }else{
                    $parent_cats_id = $cat_one->parent_id;
                }
            }
        }
        return $parent_cats_id;
    }

    public static function openAllowedShop(){
        $allowed_cats      =   User::getAllowedShop();
        $parent_cats_id    =   User::getParentShopID();;

        $current_catid=0;
        if( isset($_GET['cats_id']) )
        {
            $current_catid = (int)$_GET['cats_id'];
        }

        if (User::checkAllowedShop($parent_cats_id,$allowed_cats) OR User::checkAllowedShop($current_catid,$allowed_cats)  OR $current_catid == 0  )
        {
            return true;
        }else{
            if( !in_array(Yii::app()->controller->id,array('photos')) )
            {
            return false;
            }
        }
    }


    public static function openAllowedProduct()
    {
        $allowed_cats      =   User::getAllowedShop();

        $current_productid=0;
        if( isset($_GET['id']) )
        {
            $current_productid = (int)$_GET['id'];
        }

        $product = false;
        if ($current_productid > 0)
        {
            $product = ShopItems::model()->findByPk($current_productid);
        }


        if ( User::checkAllowedShop($product->cats_id,$allowed_cats)  )
        {
            return true;
        }else{
            return false;
        }
    }





}