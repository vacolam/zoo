<?php
class UsersRoles extends CActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'users_roles';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name,id', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(

        );
    }


    public function attributeLabels()
    {
        return array(
        );
    }

    public static function getArray()
    {
        $result = array();

        $sql = UsersRoles::model()->findAll();
        if ($sql)
        {
            $result= CHtml::listData($sql,'id','name');
            $result[0]  = 'Без отдела';
            ksort($result);
        }
        return $result;
    }





}