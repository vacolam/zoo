<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class WebpushUsers extends CActiveRecord
{

    CONST WIDTH = 1100;
    CONST HEIGHT = 600;
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'webpush_users';
    }

    public function rules()
    {
        return array(
            array('user_id', 'required'),
            array('user_id,token_id,id', 'safe'),
        );
    }


    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'name' => 'Имя',
            'email' => 'e-mail',
            'phone' => 'Телефон',
            'nom_id' => 'Номинация',
            'conf_id' => 'Конференция',
            'filename' => 'Имя файла',
            'mark' => 'Сумма оценок',
            'date' => 'Дата',
        );
    }


}