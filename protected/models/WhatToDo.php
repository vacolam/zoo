<?php

/**
 * Модель ссылка на статью о сезонном товаре на главной
 */
class WhatToDo extends CActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'what_to_do';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('link', 'url'),
            array('title, color, backgroundcolor', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'link' => 'Ссылка',
            'title' => 'Заголовок',
            'color' => 'Цвет текста',
            'backgroundcolor' => 'Цвет фона',
        );
    }
    
    protected function afterDelete()
    {
    }

    public static function getWhatToDo()
    {
        $banners = WhatToDo::model()->findAll(array('condition'=>'','order'=>'id asc'));
        return $banners;
    }
}