<?
if (count($docs) > 0)
{
    ?>
    <div style='margin-top:-10px; margin-bottom:10px;'>
        <style>
        a.doc_item{
            color:#000;
        }
        a.doc_item:hover{
            color:#3397ff;
        }
        </style>
        <div style='border-bottom:1px dotted #ccc; padding-bottom:15px; '></div>
        <table style='width:100%;'>
        <?
        foreach ($docs as $doc)
        {
            $name = $doc->filetitle;
            if ($name == ''){$name = $doc->filename;}
            ?>
            <tr style=''>
                <td style='border-bottom:1px dotted #ccc; padding-bottom:15px; padding-top:15px; width:35px; height:45px; padding-right:30px; vertical_align:middle;'>
                    <div style='width:35px; height:45px; background:url(/css_tool/doc_icon.png); background-size:cover;'></div>
                </td>
                <td style='vertical_align:middle; border-bottom:1px dotted #ccc; padding-bottom:15px; padding-top:15px; '>
                    <a href="<?=$doc->getDocUrl();?>" style='font-size:16px; font-weight:200;  text-decoration:none; line-height:1.5em' target='_blank' class='doc_item'><?=$name;?></a>
                </td>
            </tr>
            <?
        }
        ?>
        </table>
        </div>
        <?
}
?>



