<style>
.month_menu_item a{padding:5px 15px; border:1px solid #ccc; color:#606060; border-radius:3px; margin-right:5px; margin-bottom:5px; float:left; font-size:14px;}
.month_menu_item a:hover{background:#FBFBFB; cursor:pointer;}
.month_menu_item.active a{background:rgb(83,149,42); border:1px solid rgb(83,149,42); color:#fff;}
</style>
<ul style='padding:0px; margin:0px; list-style:none;'>
<?
    $active = '';
    $active_tag = '';
    if (!isset($_GET['tag'])){$active = 'active';}else{
        if ((int)$_GET['tag'] == 0){$active = 'active';}
    }
?>
<li class='month_menu_item <?=$active;?> open-s'><a href="<? echo Yii::app()->createUrl('afisha/index',array('tag'=>0,'month'=>$month)); ?>">Все</a></li>
<?
foreach ($tags as $tag)
{
    $active = '';
    if (isset($_GET['tag'])){
        if ((int)$_GET['tag'] == $tag['id']){$active = 'active'; $active_tag = (int)$_GET['tag'];}
    }

    $month_filter = array();
    if ((int)$month > 0){
        $month_filter = array('month'=>$month);
    }

    $filter_array = array_merge(array('tag'=>$tag['id']), $month_filter);
    ?>
    <li class='month_menu_item <?=$active;?> open-s'><a href="<? echo Yii::app()->createUrl('afisha/index',$filter_array); ?>"><?=$tag['name'];?></a></li>
    <?
}
?>
<?

$plugins = User::getAllowedPlugins();
if (User::checkAllowedPlugins($plugins,'groups')){
$active = '';
if ($this->route == 'afisha/orders'){
    $active = 'active';
}
?>
<li class='month_menu_item <?=$active;?> open-s'><a href="<? echo Yii::app()->createUrl('afisha/orders'); ?>">Заявки</a></li>
<?
}
?>
<li style='clear:both;'></li>
</ul>



<?

$menu = array(
            '0' => 'Все',
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        );
?>

<ul style='padding:0px; margin:0px; list-style:none; margin-top:20px;'>
<?
foreach ($menu as $index => $item)
{
    $active = '';
    if ($month == $index){$active = 'active';}


    $tag_filter = array();
    if ((int)$active_tag > 0){
        $tag_filter = array('tag'=>$active_tag);
    }

    $filter_array = array_merge(array('month'=>$index),$tag_filter);

    ?>
    <li class='month_menu_item <?=$active;?> open-s'><a href="<? echo Yii::app()->createUrl('afisha/index',$filter_array); ?>"><?=$item;?></a></li>
    <?
}
?>

<li style='clear:both;'></li>
</ul>