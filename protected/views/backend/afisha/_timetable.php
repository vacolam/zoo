<div style='font-size:16px; margin-top:30px; margin-bottom:10px;'><b>Расписание</b></div>
<div  class=''  style='width:900px; height:auto; overflow-x: scroll;'>
    <div class="" style='width:1540px; min-height:300px;'>
        <?
        $title = array(
            'mn'=>'Понедельник',
            'tu'=>'Вторник',
            'we'=>'Среда',
            'th'=>'Четверг',
            'fr'=>'Пятница',
            'sa'=>'Суббота',
            'su'=>'Воскресенье',
        );
        foreach ($timetable as $index => $week_day)
        {
        ?>
        <div style='width:200px; float:left; margin-right:20px; min-height:300px; background:#fff; margin-bottom:30px;' class='week_day' rel="<?=$index;?>">
        <div style='padding:10px;'>
            <div style='height:30px; border-bottom:2px solid #000; padding-bottom:10px; margin-bottom:10px; font-size:16px; text-transform:uppecase;'>
                <? if (isset($title[$index])){echo $title[$index];};?>
            </div>

        <div>

        <div style='' class='tags_block'>
            <ul style='list-style:none; padding:0px; margin:0px;' class='tags_ul'>
            <?
                if ($week_day){
                    foreach($week_day as $i => $day)
                    {
                    ?>
                    <li style='padding-bottom:2px; border-bottom:1px dotted #ccc; padding-bottom:5px; margin-bottom:5px;' class='tag_li' rel='<?=$day->id;?>'>
                        <input type="text" value='<?=$day->time;?>'id='tag_<?=$day->id;?>' class='time_input' rel='<?=$day->id;?>' style='width:150px; font-size:12px; height:25px; line-height:25px;'/> <div class='btn btn-outline-secondary' style='float:right; width:25px; height:25px; margin-left:5px; font-size:16px; padding:0px; line-height:22px; text-align:center;' title='Удалить' onclick='delete_tagTime(<?=$day->id;?>);'>x</div>
                        <div style='clear:both;'></div>
                    </li>
                    <?
                    }
                }
            ?>
            </ul>
        </div>

        </div>

        <div style='margin-bottom:5px;'>
            <input type="text" class='add_tag' style='border:1px solid #606060; border-radius:3px; padding:0px 10px; height:25px; line-heigth:25px; width:150px; color:#222; font-size:12px; float:left;' placeholder='Время' />
            <div style='float:right; width:25px; height:25px; margin-left:5px; font-size:20px; padding:0px; line-height:25px; text-align:center;' class='btn btn-outline-secondary' onclick='addTagTime("<?=$index;?>")'><b>+</b></div>
            <div style='clear:both;'></div>
        </div>
        </div>
        </div>
        <?
        }
        ?>
        <div style='clear:both;'></div>
    </div>
</div>

        <script>
        function addTagTime(week_day){
            var week = $('.week_day[rel="'+week_day+'"]');
            var time = week.find('.add_tag').val();
            if (time != ''){
                week.find('.add_tag').val('');
                var html = "<li  style='padding-bottom:2px;' class='tag_li new_tag'>"+
                            "<div style='padding:2px; background:#f0f0f0; font-size:14px;'>"+time+"</div>"+
                            "</li>";
                week.find('.tags_ul').append(html);
                $.ajax({
                    type: "GET",
                    url: '<?=Yii::app()->createUrl('afisha/addtime',array());?>',
                    dataType: 'json',
                    data: {
                        'time':time,
                        'week_day':week_day,
                        'groups_id':<?=$model->id;?>,
                    },
                    success: function(response){
                        if (response != false){
                            var html = "<input type='text' value='"+time+"'id='tag_"+response+"' class='time_input' rel='"+response+"' style='width:150px; font-size:12px; height:25px; line-height:25px;'/> <div class='btn btn-outline-secondary' style='float:right; width:25px; height:25px; margin-left:5px; font-size:16px; padding:0px; line-height:22px; text-align:center;' title='Удалить' onclick='delete_tagTime("+response+");'>x</div><div style='clear:both;'></div>";
                            $('.new_tag').html(html);
                            $('.new_tag').attr('rel',response);
                            $('.new_tag').removeClass('new_tag');

                            $('.time_input[rel="'+response+'"]').blur(function(){
                                change_tagTime(response);
                            });
                        }else{
                            //show_ozon('yandex_rtb_R-A-400319-2');
                        }
                    },
                    error: function(response){
                    }
                });
            }
        }


        function delete_tagTime(id){
            if (id != ''){
                $('.tag_li[rel="'+id+'"]').remove();
                $.ajax({
                    type: "GET",
                    url: '<?=Yii::app()->createUrl('afisha/deletetime',array());?>',
                    dataType: 'json',
                    data: {
                        'id':id
                    },
                    success: function(response){
                        if (response != false){
                        }else{
                        }
                    },
                    error: function(response){
                    }
                });
            }
        }


        function change_tagTime(id){
            if (id != ''){
                var time = $('.time_input[rel="'+id+'"]').val();
                $.ajax({
                    type: "GET",
                    url: '<?=Yii::app()->createUrl('afisha/changetime',array());?>',
                    dataType: 'json',
                    data: {
                        'id':id,
                        'time':time,
                    },
                    success: function(response){
                        if (response != false){
                        }else{
                        }
                    },
                    error: function(response){
                    }
                });
            }
        }


        $(function(){
            $('.time_input').blur(function(){
            var t = $(this).attr('rel');
            change_tagTime(t);
            });
        })
        </script>