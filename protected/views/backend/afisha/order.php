<style>
.content_one a{
    color:#000;
}
a {
    color:#222;
}
</style>
<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:60%; height:100px; text-align:left; ' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                        </div>
                    </td>
                    <td style='width:40%; height:100px; text-align:right;' valign=middle>

                    </td>
            </tr>
            </table>
        </div>
    </div>

<div class='content_one'>
<div style='margin-top:30px;'>
<style>
.table td{
    border:none; padding-top:7px; padding-bottom:7px; border-bottom:1px dotted #ccc; vertical-align:middle;
}
</style>
<div style='padding-bottom:15px; margin-bottom:20px; border-bottom:2px solid #000; text-transform:uppercase; font-size:18px;'>
<b>Заявка № <?=$group->id;?>.</b>
</div>
<div style='width:100%;'>
    <table style='width:100%; border-collapse:collapse;' class='table'>

        <tr><td style='text-align:left;'><b>Дата заявки:</b></td><td style='text-align:left;'><?=Dates::getName($group->order_date);?></td></tr>
        <tr><td style='text-align:left;'><b>Организация:</b></td><td style='text-align:left;'><?=$group->school_name;?></td></tr>
        <tr><td style='text-align:left;'><b>Имя:</b></td><td style='text-align:left;'><?=$group->name;?></td></tr>
        <tr><td style='text-align:left;'><b>Телефон:</b></td><td style='text-align:left;'><?=$group->phone;?></td></tr>
        <tr>
            <td colspan=2 style='padding:0px;'>
                <div style='margin-top:40px; width:100%; margin-bottom:60px;'>
                <div style='padding-bottom:2px; padding-top:6px; font-size:14px; text-align:left; text-transform:uppercase;'><b>Файлы</b></div>
                <?
                $docs = $group->docs;
                $this->renderPartial('_documents', array('docs'=>$docs));

                $photos = $group->photos;
                $this->renderPartial('_photos', array('photos'=>$photos,'model'=>$group,'margin_top'=>'40'));
                ?>
                </div>
            </td>
        </tr>

        <?
        $orders = $group->orders;
        foreach ($orders as $index => $order)
        {
        ?>
        <tr><td style='text-align:left; width:400px;'><b>Меропиятие:</b></td><td style='text-align:left;'><? if (isset($order->afisha->title)){echo $order->afisha->title;}?></td></tr>
        <tr>
            <td style='text-align:left;'><b>Статус:</b></td><td style='text-align:right;'>
            <?
            $active = 'secondary';
            $name = 'Статус';
            if($order->status == 0){$name = 'Без статуса'; $active = 'outline-secondary';}
            if($order->status == 1){$name = 'Согласовано'; $active='success';}
            if($order->status == 2){$name = 'Отказано'; }
            ?>
            <div class="dropdown" style=''>
                <button class="btn btn-<?=$active;?> dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style='line-height:1em;'>
                    <?=$name;?>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a href="<?= Yii::app()->createUrl('afisha/orderstatus',array('status'=>0,'id'=>$order->id)); ?>" class="dropdown-item" style=''>Без статуса</a>
                    <div class="dropdown-divider"></div>
                    <a href="<?= Yii::app()->createUrl('afisha/orderstatus',array('status'=>1,'id'=>$order->id)); ?>" class="dropdown-item" style=''>Согласовано</a>
                    <a href="<?= Yii::app()->createUrl('afisha/orderstatus',array('status'=>2,'id'=>$order->id)); ?>" class="dropdown-item" style=''>Отказано</a>
                </div>
            </div>


        </td></tr>


        <tr><td style='text-align:left;'><b>Дата мероприятия:</b></td><td style='text-align:left;'><?=Dates::getName($order->date);?></td></tr>
        <tr><td style='text-align:left;'><b>Время записи:</b></td><td style='text-align:left;'><?=$order->time_text;?></td></tr>
        <tr><td style='text-align:left;'><b>Количество участников:</b></td><td style='text-align:left;'><?=$order->kolichestvo;?></td></tr>
        <tr><td style='text-align:left;'><b>Возраст детей:</b></td><td style='text-align:left;'><?=$order->vozrast;?></td></tr>
        <?
        if ($order->bus_need == 1)
        {
        ?>
        <tr><td style='text-align:left;'><span style='background:#FFCC33; border-radius:3px; padding:3px 10px;'><b>Нужен автобус</b></span></td><td style='text-align:right;'></td></tr>
        <tr><td style='text-align:left;'><b>Время поездки:</b></td><td style='text-align:left;'><?=$order->bus_time;?></td></tr>
        <tr><td style='text-align:left;'><b>Маршрут:</b></td><td style='text-align:left;'><?=$order->bus_route;?></td></tr>
        <?
        }
        ?>
        <tr><td style='text-align:left;' colspan=2><div style='height:50px;'>&nbsp;</div></td></tr>
        <?
        }
        ?>
    </table>
</div>


<div style='clear:both;'></div>
</div>
</div>
</div>

<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({});
});
</script>
