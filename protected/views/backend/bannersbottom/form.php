<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <a href="<?= Yii::app()->createUrl('home/index',array()); ?>" style='font-size:40px; color:#000;'>Главная</a>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'><a href="<?=Yii::app()->createUrl('bannersbottom/index',array());?>">Баннеры внизу сайта</a> - <?= $model->isNewRecord ? 'добавление' : 'редактирование'; ?></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить баннер', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<div class="form" style='padding-top:20px;'>
<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <?php
        if (!$model->isNewRecord) {
            ?>
            <div style=' border:1px solid rgba(0,0,0,.2);width:150px;margin-bottom:40px;'>
                <img src='<?= $model->getImageUrl(); ?>' style='width:150px;'/>
            </div>
            <?
        }
    ?>

    <div class="form-group<?= $model->hasErrors('image') ? ' has-error' : ''; ?>" style='padding:10px; border:1px solid #ccc; background:#fff; border-radius:3px 3px 0px 0px; margin-bottom:0px; border-bottom:none;'>
        <?= CHtml::activeLabel($model, 'image', array('class' => 'control-label')); ?>
        <?= CHtml::activeFileField($model, 'image'); ?>
        <?= CHtml::error($model, 'image', array('class' => 'help-block')); ?>
    </div>

    <div style='padding:10px; border:1px solid rgba(0,0,0,.1); background:#0099FF; color:#fff; border-radius:0px 0px 3px 3px;  margin-bottom:20px;'><b>Min: ширина 148px / высота 148px</b>.</div>

    <div class="form-group<?= $model->hasErrors('position') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'position', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'position', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'position', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('title') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'title', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'title', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'title', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('link') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'link', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'link', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'link', array('class' => 'help-block')); ?>
    </div>





    <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

            <div style='with:100px; float:right;'>
                <?
                if ($this->route == 'bannersbottom/update'){
                ?>
                <a href='<?= Yii::app()->createUrl('bannersbottom/delete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем баннер?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>
</div>
</div>
</div>
