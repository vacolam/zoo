<style>
.content_one a {
    color:#222;
}
.content_one a:hover {
    color:#0099CC;
}
</style>

<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; '>
                            <a href="<?= Yii::app()->createUrl('home/index',array()); ?>" style='font-size:40px; color:#000;'>Главная</a>
                        </div>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'><a href="<?=Yii::app()->createUrl('bannersbottom/index',array());?>">Баннеры внизу сайта</a></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить баннер', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<?php
foreach ($banners as $banner){
    ?>
    <div style='padding-bottom:20px; margin-bottom:20px; border-bottom:1px solid #ccc;'>
                    <a href="<?= Yii::app()->createUrl('bannersbottom/update',array('id'=>$banner->id)); ?>" style='display:block;'>
                        <div style='width:150px; float:left;' alt="" class="slide-img">
                            <img src='<?= $banner->getImageUrl(); ?>' style='width:150px;'/>
                        </div>
                        <div style='width: 500px; float:left; padding-left:40px; '>
                            <div style='font-size:16px; font-weight:600;' class='open-s'><?=$banner->title;?></div>
                            <div style='font-size:14px; line-height:1.3em; padding-top:10px;' class='open-s'><?=$banner->link;?></div>
                        </div>
                        <div style='clear:both;'></div>
                    </a>

    </div>
    <?
}
?>
</div>
</div>
