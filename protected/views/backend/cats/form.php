<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:90%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl($model->link,array());?>" style='color:#000; padding-top:0px; font-size:40px;'>
                                <? if ($model->isNewRecord){ echo "Новый раздел";}else{echo $model->name;}?>
                            </a>
                        </div>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'><?= $model->isNewRecord ? 'добавление' : 'редактирование'; ?></div>
                    </td>
                    <td style='width:10%; height:100px; text-align:right;' valign=middle>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <div class="form-group<?= $model->hasErrors('name') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'name', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'name', array('class' => 'form-control','style'=>'height:100px; width:100%;')); ?>
        <?= CHtml::error($model, 'name', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('number') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'number', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'number', array('class' => 'form-control','style'=>'')); ?>
        <?= CHtml::error($model, 'number', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group">
    <label for='checkbox'>
        <?
        $checked = '';
        if ($model->visible == 1){$checked = 'checked="checked"';}
        ?>
        <input type="checkbox" name='Cats[visible]' value='1' <?=$checked;?> id='checkbox'/> Отображать раздел в меню
    </label>
    </div>





 <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

            <div style='with:100px; float:right;'>
                <?
                if (!$model->isNewRecord AND $model->link == ''){
                ?>
                <a href='<?= Yii::app()->createUrl(Yii::app()->controller->id.'/delete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
                <?
                }
                ?>
            </div>

            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>
</div>
</div>