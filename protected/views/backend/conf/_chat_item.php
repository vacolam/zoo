<?
$bg = 'none;';
$him_or_mine = '';
if (Yii::app()->user->id == $receiver_id){
$bg = '#FBFBFB';
}

if (Yii::app()->user->id == $creator_id){
$him_or_mine = 'mine';
}
?>
<div style='' class='chat_item <?=$him_or_mine;?>'>
<div class='chat_item_in'>
<table style='width:100%;'>
	<tr>
    	<td style='width:45px;' valign=top align=left class='chat_item_photo'>
        	<div style='width:35px; height:35px; border-radius:50%; background:url(<?= $user_photo; ?>) #FFFFCC center; background-size:cover;'></div>
		</td>
		<td valign=top align=left>
				<div style='font-size:12px; color:#202020;' class='open-s'>
                    <?
						echo "<a href='".Yii::app()->createAbsoluteUrl('users/view',array('id'=>$user_id))."' target='_blank' class='open-s' style='font-size:12px; color:#202020;'><b>".$user_name."</b></a>";
						echo "<span style='margin-left:10px; color:#a0a0a0; font-size:10px;' class='open-s'>".Dates::getName($date)."</span>";
					?>
				</div>
				<div style='padding-top:10px; font-size:13px; color:#202020;' class='open-s'>
                	<?
                    echo nl2br($text);
					?>
				</div>
		</td>
	</tr>
</table>
</div>
</div>
<div style='clear:both;'></div>