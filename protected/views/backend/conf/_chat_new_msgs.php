	<?php
		//Скрипт для автоматического увеличения textarea
	Yii::app()->clientScript->registerScriptFile("/js/autoresize.js", CClientScript::POS_HEAD);
	?>
		<?
        $parent_id = 0;
		?>
		<div class='comments_formContainer' style='z-index:999;' rel='comment_<?=$parent_id;?>'>
		<div style='' class='comments_formContainer_in'>


        <table style='width:100%;'>
            <tr>
                <td align=left valign=top style=''>
                		<form class="comment_form" rel='comment_<?=$parent_id;?>' style=''>
                			<div style='margin:0px; padding:0px;' class='textarea_placement'>
                				<table style='width:100%;'>
                                	<tr>
                						<td class='textarea_placement_photo' style='width:50px;' valign=top align=left><div style='width:35px; height:35px; border-radius:50%; background:url() #E0E0E0; background-size:cover;'></div></td>
                						<td style=''>
                							<div class='textarea_placement_textarea_block' style=''>
                                            	<textarea name="Comment[text]" id='comment_text' class='comment_text main_commentbox open-s' rel='' style='' placeholder='Напишите сообщение'></textarea>
                							</div>
                						</td>
                					</tr>
                				</table>
                            </div>

                			<div class='link-add-0'  style='text-align:right; padding-left:15px; padding-right:15px; padding-top:15px; display:none;'>
                               <div class='link-add-1'>

                			   </div>

                			   <input type='hidden' value='' class='link_url'>
                			   <input type='hidden' value='' class='link_page_link'>
                			   <input type='hidden' value='' class='link_page_title'>
                			   <input type='hidden' value='' class='link_page_description'>

                			</div>
                		</form>

            </td>
            <td align=right valign=bottom style='width:50px;' class='chat_mobile_button'>
                        <div style='width:40px;'>
        					<div style='position:inherit;display:inline-block; width:40px; height:40px; background:url(/css_tool/msgs_send.png) #F4F4F4 center; background-size:cover; border-radius:50%;' class='mobile_comments_submit' rel='comment_<?=$parent_id;?>' onclick='submit_comment("comment_<?=$parent_id;?>")'></div>
        					<div style='height:40px; font-size:20px; line-height:40px; color:#808080; width:40px; text-align:center; ' class='open-s mobile_comments_submited'>&bull;&bull;&bull;</div>
        				</div>
            </td>
        </tr>
        </table>

        <div style='text-align:right;' class='chat_pc_button'>
                <div style='width:120px; float:right; margin-right:2px; margin-top:5px;'>
					<div style='position:inherit;display:inline-block; width:110px; height:35px; line-height:35px; ' class='comments_submit' rel='comment_<?=$parent_id;?>' onclick='submit_comment("comment_<?=$parent_id;?>")'>Отправить</div>
					<div style='position:inherit;display:none; width:120px; height:35px; line-height:35px; ' class='comments_submited'>Отправляется...</div>
				</div>
				<div style='clear:both;'></div>
		</div>
		</div>
		</div>

        <script>
        //В этой функции написать ограничения с применением rel = 'comment_' + $comment_id
		function submit_comment(comment_rel){
			var form = $('.comments_formContainer[rel="'+comment_rel+'"]');
			if (form.find('#comment_text').val() != ''){
					$('.comments_submit').css('display','none');
					$('.comments_submited').css('display','block');

                    $('.mobile_comments_submit').css('display','none');
					$('.mobile_comments_submited').css('display','block');

					//scrollToBottom();
					$.ajax({
						url: '<?= Yii::app()->createUrl('conf/chatsend', array()); ?>',
						dataType: 'json',
						type: 'post',
						data: {
	                    	'text': form.find('#comment_text').val(),
	                    	'work_id': <?=$work->id;?>,
						},
						success: function(response) {
							if (response.result)
							{
								$('.messages').append(response.text);
								$('.comments_submited').css('display','none');
								$('.comments_submit').css('display','block');

                                $('.mobile_comments_submited').css('display','none');
								$('.mobile_comments_submit').css('display','block');

								form.find('#comment_text').val('');
								form.find('#comment_text').css('height','40px');
							    scrollToBottom();
							}
						}
					});
				}
		}
	$(function(){
    	$('textarea').not('#note').autosize();
	});
</script>