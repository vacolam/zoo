<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:100%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                            <div style='margin-top:0px; font-size:14px; text-transform:uppercase; color:#A0A0A0;'>
                                <a href="<?=Yii::app()->createUrl('conf/index');?>"><? echo $this->razdel['name']; ?></a> /
                                <a href="<?=Yii::app()->createUrl('conf/confview',array('id'=>$conf->id));?>"><? echo mb_substr($conf->title, 0, 50,'utf-8'); ?>...</a> /
                                <a href="<?=Yii::app()->createUrl('conf/confview',array('id'=>$conf->id));?>">НАПРАВЛЕНИЕ <? if (isset($nom)){ echo mb_substr($nom->title, 0, 30,'utf-8'); } ?>...</a> /
                                <a href="<?=Yii::app()->createUrl('conf/work',array('id'=>$work->id));?>">РАБОТА № <? echo $work->id; ?></a>
                            </div>
                        </div>
                    </td>
            </tr>
            </table>
        </div>
    </div>

<div class='content_one'>

<style>
a {
    color:#222;
}
</style>

<div style='text-align:center; font-size:26px; font-weight:400;'>Работа № <?=$work->id;?></div>
<h3 style='text-align:center;'><b><?=$work->work_name;?></b></h3>
<h3 style='text-align:center;'><b><? if (isset($nom)){ echo $nom->title; } ?></b></h3>
<div style='margin-top:-5px;text-align:center; text-transform:uppercase;'><a href="<?=Yii::app()->createUrl('conf/confview',array('id'=>$conf->id));?>" style='font-size:14px;'><? echo $conf->title; ?></a></div>




<div style='margin-top:30px;'>
<style>
.table td{
    padding-top:7px; padding-bottom:7px; border-bottom:1px dotted #ccc; vertical-align:middle;
}
</style>
<div style='width:450px; float:left;'>
<div style='padding-bottom:7px; font-size:13px; text-align:center; text-transform:uppercase;'><b>Данные работы</b></div>
    <table style='width:100%; border-collapse:collapse;' class='table'>
        <tr><td style='width:150px; text-align:left;'><b>Название:</b></td><td style='text-align:right;'><?=$work->work_name;?></td></tr>
        <tr><td style='width:150px; text-align:left;'><b>Направление:</b></td><td style='text-align:right;'><? if (isset($nom)){ echo $nom->title; } ?></td></tr>
        <tr><td style='text-align:left;'><b>Дата работы:</b></td><td style='text-align:right;'><?=Dates::getName($work->date);?></td></tr>

        <tr><td style='text-align:left;'><b>Сумма оценок:</b></td><td style='text-align:right;'><b><?=$work->mark;?></b></td></tr>
        <tr><td style='text-align:left;'><b>Количество голосов:</b></td><td style='text-align:right;'><?=$work->marks_count;?></td></tr>
        <tr><td style='text-align:left;'><b>Ваша оценка:</b></td>
            <td style='text-align:right;'>
                <?
                if (isset($your_mark['mark'])){
                    echo "<b>".$your_mark['mark']."</b>";
                }
                if (isset($your_mark['date'])){
                    echo ", ".Dates::getName($your_mark['date']);
                }
                ?>
            </td>
        </tr>
        <tr><td style='width:130px; text-align:left;'><b>Сообщений в чате:</b></td><td style='text-align:right;'><?=$work->chat_messages;?></td></tr>
    </table>

<?
    if (User::userNomPermission($work->nom_id,Yii::app()->user->id)){
        ?>
        <style>
            .mark_li{
                float:left; width:30px; margin-left:5px;
            }
            .mark_li a{
                width:30px; height:30px; display:block; background:#B0B0B0; border-radius:50%; text-align:center; line-height:30px; font-size:16px;
            }
            .mark_li a:hover{
                background:#28A745; color:#fff;
            }

            .mark_li.active a{
                background:#28A745; color:#fff;
            }

        </style>
        <div style='padding-bottom:7px; font-size:13px; text-align:left; text-transform:uppercase; margin-top:20px;'><b>Ваша оценка</b></div>
        <ul style='margin:0px; padding:0px; list-style:none; margin-bottom:20px; width:350px;'>
            <?
            for ($i=1; $i < 11; $i++)
            {
                $active = '';
                $m = '';
                if (isset($your_mark['mark']))
                {
                    if ($i == $your_mark['mark']){$active = 'active';}
                }
                ?>
                <li class='mark_li <?=$active;?>'><a href="<?=Yii::app()->createUrl('conf/workmark',array('id'=>$work->id,'mark'=>$i));?>"><?=$i;?></a></li>
                <?
            }
            ?>
            <li style='clear:both;'></li>
        </ul>
        <?
    }
    ?>

</div>

<div style='width:350px; float:right;'>
<div style='padding-bottom:7px; font-size:13px; text-align:center; text-transform:uppercase;'><b>Файлы</b></div>
<?
$docs = $work->docs;
$this->renderPartial('_documents', array('docs'=>$docs));

$photos = $work->photos;
$this->renderPartial('_photos', array('photos'=>$photos,'model'=>$work,'margin_top'=>'40'));
?>

<div style='padding-top:20px;'>
<div><b>Ссылка на облако с фотографиями:</b></div>
<?
$link = '';
if (ConfWorks::checkSiteAdress($work->photos_link) != ''){$link = ConfWorks::checkSiteAdress($work->photos_link);}
if ($link != ''){
?>
<a href="<?=$link;?>" style='color:#3399FF; text-decoration:underline;' target='_blank'><?=$link;?></a>
<?
}
?>
</div>
</div>
<div style='clear:both;'></div>

<div style='width:100%; margin-top:30px;'>
<div style='font-size:16px;'><b>Анкета участника</b></div>
<table style='width:100%; border-collapse:collapse;' class='table'>
        <tr><td style='text-align:left;'><b>ФИО автора:</b></td><td style='text-align:right; min-width:400px;'><?=$work->name;?></td></tr>
        <?
        if (Yii::app()->user->role != 'user')
        {
        ?>

        <tr><td style='text-align:left;'><b>Город (село) проживания, район:</b></td><td style='text-align:right; min-width:400px;'><?=$work->city;?></td></tr>
        <tr><td style='text-align:left;'><b>Дата рождения:</b></td><td style='text-align:right; min-width:400px;'><?=$work->birth;?></td></tr>
        <tr><td style='text-align:left;'><b>Полное официальное наименование учебного заведения (школы, клуба, дома творчества, и т.д.):</b></td><td style='text-align:right; min-width:400px;'><?=$work->shcool;?></td></tr>
        <tr><td style='text-align:left;'><b>Класс, буква класса, секция:</b></td><td style='text-align:right; min-width:400px;'><?=$work->class;?></td></tr>
        <tr><td style='text-align:left; padding-top:30px; padding-left:0px;'><b>Научный руководитель</b></td><td style='text-align:right; min-width:400px;'>&nbsp;</td></tr>
        <tr><td style='text-align:left;'><b>ФИО научного руководителя:</b></td><td style='text-align:right; min-width:400px;'><?=$work->teachers_name;?></td></tr>
        <tr><td style='text-align:left;'><b>E-mail научного руководителя:</b></td><td style='text-align:right; min-width:400px;'><?=$work->teachers_email;?></td></tr>
        <tr><td style='text-align:left;'><b>Должность, полное официальное наименование места работы:</b></td><td style='text-align:right; min-width:400px;'><?=$work->teachers_occupation;?></td></tr>
        <tr><td style='text-align:left;'><b>Контактный телефон научного руководителя:</b></td><td style='text-align:right; min-width:400px;'><?=$work->teachers_cell;?></td></tr>

        <?
        }
        ?>
    </table>

</div>

<div style='width:500px; float:left;  margin-top:50px;'>
            <div style='padding-bottom:10px; margin-bottom:10px; border-bottom:2px solid #222;'>
                <div style='width:250px; float:left;'><b>Жюри</b></div>
                <div style='width:150px; float:left;'><b>Дата</b></div>
                <div style='width:100px; float:right;'><b>Оценка</b></div>
                <div style='clear:both;'></div>
            </div>
        <?
        if (isset ($work->nomination))
        {
            $jury_array = array();
            $juries = $work->nomination->juries;
            if ($juries)
            {
                foreach($juries as $jury){
                    if (isset($jury->user)){
                    $mark = array();
                    if (isset($marks_array[$jury->user->id])){
                        $mark = $marks_array[$jury->user->id];
                    }
                ?>
                <div style='padding-bottom:10px; margin-bottom:10px; border-bottom:1px dotted #ccc; <? if ($jury->user->id == Yii::app()->user->id){echo "font-weight:bold;";}?>'>
                    <div style='width:250px; float:left;'><?=$jury->user->name;?></div>
                        <div style='width:150px; float:left;'><? if (isset($mark['date'])){echo Dates::getName($mark['date']);} ?></div>
                        <div style='width:100px; float:right;'><? if (isset($mark['date'])){echo $mark['mark'];} ?></div>
                        <div style='clear:both;'></div>
                </div>
                <?
                }
                }
            }
        }
        ?>


</div>

<div style='width:350px; float:right;  margin-top:50px; background:#F8F8F8; border:1px solid #ccc; border-radius:5px; box-shadow:0px 0px rgba(0,0,0,.2); position:relative;'>
<?
$this->renderPartial('work_chat', array(
    'work' => $work,
));
?>


</div>
<div style='clear:both;'></div>
</div>
</div>
</div>

<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({});
});
</script>
