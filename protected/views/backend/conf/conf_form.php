<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:90%; height:100px; text-align:left;' valign=middle>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                            <div style='margin-top:0px; font-size:14px; text-transform:uppercase; color:#A0A0A0;'>
                                <? if ($model->isNewRecord){?><a href="<?=Yii::app()->createUrl('conf/confcreate');?>">Добавление</a><?}?>
                                <? if (!$model->isNewRecord){?><a href="<?=Yii::app()->createUrl('conf/confview',array('id'=>$model->id));?>"><? echo mb_substr($model->title, 0, 30,'utf-8'); ?>...</a> / <a href="<?=Yii::app()->createUrl('conf/confupdate',array('id'=>$model->id));?>">Редактировать</a><?}?>
                            </div>
                    </td>
                    <td style='width:10%; height:100px; text-align:right;' valign=middle>

                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one form'>

<script src="js/bootstrap-select-1.13.14.min.js"></script>
<link href="js/bootstrap-select-1.13.14.min.css" rel="stylesheet">


<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
    'class' => 'form_news',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <div style='width:550px; float:left; margin-top:-5px;'>

    <div class="form-group<?= $model->hasErrors('title') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'title', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'title', array('class' => 'form-control','style'=>'height:100px; width:100%;')); ?>
        <?= CHtml::error($model, 'title', array('class' => 'help-block')); ?>
    </div>

        <div class="form-group">
        <label for='checkbox'>
            <?
            $checked = '';
            if ($model->form_is_open == 1){$checked = 'checked="checked"';}
            ?>
            <input type="checkbox" name='Conf[form_is_open]' value='1' <?=$checked;?> id='checkbox'/> Разрешить присылать работы
        </label>
        </div>


        <?
        if ($model->type == 'konkurs'){
        ?>
        <div class="form-group">
        <label for='checkbox2'>
            <?
            $checked = '';
            if ($model->golosovanie_is_closed == 1){$checked = 'checked="checked"';}
            ?>
            <input type="checkbox" name='Conf[golosovanie_is_closed]' value='1' <?=$checked;?> id='checkbox2'/> Голосование закрыто
        </label>
        </div>
        <?
        }
        ?>

    <div class="form-group<?= $model->hasErrors('date') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'date', array('class' => 'control-label')); ?>
        <?php
		$date = new DateTime($model->date);
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'Conf[date]',
            'htmlOptions' => array(
                'class' => 'form-control',
            ),
            'language' => 'ru',
            'value' => $date->format('d.m.Y'),
        ));
        ?>
        <style>
            .ui-datepicker {
                z-index: 999 !important;
            }
        </style>
        <?= CHtml::error($model, 'date', array('class' => 'help-block')); ?>
    </div>

    <?

    ?>
    <div class="form-group<?= $model->hasErrors('type') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'type', array('class' => 'control-label')); ?>
        <?=CHtml::activeDropDownList($model,'type',$typeList, array('class' => 'form-control dropdown', 'data-live-search'=>'true', 'style'=>'width:100%;')); ?>
        <?= CHtml::error($model, 'type', array('class' => 'help-block')); ?>
    </div>

    </div>



    <div style='clear:both;'></div>

        <?
        $this->renderPartial('//modules/_redactor', array(
            'model' => $model,
            'text_input_name' => 'text',
        ));
        ?>

    <div class="form-group<?= $model->hasErrors('short_text') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'short_text', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'short_text', array('class' => 'form-control', 'style'=>'height:120px;')); ?>
        <?= CHtml::error($model, 'short_text', array('class' => 'help-block')); ?>
    </div>

    <div style=' margin-top:20px; margin-bottom:20px; border-top:1px dotted #ccc;'></div>

 <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

            <div style='with:100px; float:right;'>
                <?
                if ($this->route == Yii::app()->controller->id.'/confupdate'){
                ?>
                <a href='<?= Yii::app()->createUrl(Yii::app()->controller->id.'/confdelete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>
<?
if ($this->route == 'conf/confupdate' and $model->type != 'opros')
{

?>
<div style='width:100%; border:1px solid #ccc; border-radius:3px; margin-top:50px; background:#fff;'>
<table style='width:100%;'>
    <tr>
        <td style='width:60%; text-align:left; vertical-align:top; border-right:1px solid #ccc;'>
        <div style='padding:10px;'>
        <?
        $this->renderPartial('//modules/_f_photos', array(
            'photos' => $photos,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('conf/deletephotos', array()),
            'link_setcover'=>'',
        ));

        ?>
        </div>
        </td>
        <td style='text-align:left; vertical-align:top;'>
        <div style='padding:10px;'>
        <?
        $this->renderPartial('//modules/_f_docs', array(
            'docs' => $docs,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('conf/deletedocs', array()),
        ));

        ?>
        </div>
        </td>
    </tr>
</table>
</div>
<?

$this->renderPartial('//modules/_f_upload', array(
    'model' => $model,
    'link_add' => Yii::app()->createUrl('conf/filesadd', array('post_id'=>$model->id,'type'=>'conf')),
));


}
?>
</div>
</div>