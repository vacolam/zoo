<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:80%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <?
                            $link = Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());
                            if (Yii::app()->user->role == 'user'){
                            $link = Yii::app()->createUrl('conf/jury',array());
                            }
                            ?>

                            <a href="<?=$link;?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                            <div style='margin-top:0px; font-size:14px; text-transform:uppercase; color:#A0A0A0;'>
                                <a href="<?=$link;?>"><? echo $this->razdel['name']; ?></a> /
                                <a href="<?=Yii::app()->createUrl('conf/confview',array('id'=>$model->id));?>"><? echo mb_substr($model->title, 0, 50,'utf-8'); ?>...</a>
                            </div>
                        </div>
                    </td>
                    <td style='width:10%; height:100px; text-align:right;' valign=middle>

                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<style>
a {
    color:#222;
}
</style>
<?
if (Yii::app()->user->role != 'user'){
?>
<div style='background:#fff; box-shadow:0px 0px 2px rgba(0,0,0,.1); border-radius:5px; margin-bottom:50px;'>
    <div style='padding:20px; font-size:16px; line-height:1.4em;'>
    <div style='font-size:22px; margin-bottom:15px;'>
            <b><?=$model->title;?></b>
            <?
            if ($model->form_is_open == 1){
                echo " (Разрешено присылать работы)";
            }
            ?>
    </div>
    <div style='padding-bottom:7px;'><i>Дата: <?=Dates::getName($model->date);?></i></div>
    <?=$this->crop_str_word($model->text, 30 );?>

        <?
        $photos = $model->some_photos;
        if ($photos){
            ?>
            <div style='padding-top:0px;'>
            <?
            foreach($photos as $index => $photo){
                $margin='margin-right:7px;';
                if ($index == 6){$margin='';}
                ?>
                <div style='width:80px; height:50px; display:block; border-radius:3px; float:left; <?=$margin;?> background:url(<?=$photo->getThumbnailUrl();?>); background-size:cover;'></div>
                <?
            }
            ?>
            <div style='clear:both;'></div>
            </div>
            <?
        }
        ?>


    <div style='margin-top:30px;'>
    <a href='<?= Yii::app()->createUrl('conf/confupdate',array('id'=>$model->id)); ?>' style='line-height:1em;' class='btn btn-success'>Редактировать</a>
    </div>
    </div>
</div>
<?
}
?>

<div style=''>
<?
if (Yii::app()->user->role != 'user'){
?>
<div style='padding-bottom:10px; margin-bottom:20px; border-bottom:2px solid #000;'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%;text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:30px;'>
                            <b>Номинации</b>
                        </div>
                    </td>
                    <td style='width:50%; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить номинацию', array('nomcreate','conf_id'=>$model->id), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
</div>
<?
}
?>
<?
if ($nominations)
{
    foreach($nominations as $nomination)
    {
        $jury_array = array();
        $juries = $nomination->juries;
        if ($juries)
        {
            foreach($juries as $jury)
            {
                $jury_array[] = $jury->user->name;
            }
        }

        $link = Yii::app()->createUrl('conf/nomupdate',array('id'=>$nomination->id));
        if (Yii::app()->user->role == 'user'){
        $link = '';
        }

        ?>
        <div style='padding-bottom:0px; '>
        <a href="<?= $link; ?>" style='font-size:18px; '><b><?=$nomination->title;?></b></a>
        <div style='padding-bottom:25px; padding-top:0px;'>
        <?
        if (count($jury_array) > 0){
            echo "<span style='color:#8A8A8A; font-size:14px;'>Жюри номинации: ".implode(', ',$jury_array)."</span>";
        }
        ?>
        </div>
        <?
        /*
        $works = $nomination->works;
        if ($works){
            foreach($works as $work)
            {
                $bg = '';
                if (isset($your_marks[$work->id])){ $bg = 'background:#FFFFFF'; }
                ?>
                <a href="<?= Yii::app()->createUrl('conf/work',array('id'=>$work->id)); ?>" style='padding-bottom:10px; padding-top:10px; display:block; border-bottom:1px dotted #ccc; <?=$bg;?>; position:relative; padding-left:10px; padding-right:10px;'>
                    <?
                            if (isset($your_marks[$work->id])){
                                ?>
                                <div style='position:absolute; width:30px; height:30px; border-radius:50%; top:50%; margin-top:-15px; right:-40px; background:url(css_tool/added.png) #28A745 center; box-shadow:0px 0px 2px rgba(0,0,0,.1);'></div>
                                <?
                            }
                        ?>

                    <div style='position:absolute; width:30px; height:30px; border-radius:50%; top:50%; margin-top:-15px; left:-35px;'><b><?=$work->id;?>.</b></div>
                     <div style='width:300px; float:left;'>
                        <div><b><?=$work->work_name;?></b></div>
                        <div style='font-size:12px;'><i>Автор: <b><?=$work->name;?></b></i></div>
                    </div>
                    <div style='width:150px; float:left; font-size:12px;'>
                        <div><b><?=Dates::getName($work->date);?></b></div>
                    </div>
                    <div style='width:200px; float:left; font-size:12px;'>
                        <div>Сумма оценок: <b><?=$work->mark;?></b></div>
                        <div>Количество голосов: <b><?=$work->marks_count;?></b></div>
                        <div>Ваша оценка: <b>
                            <?
                            if (isset($your_marks[$work->id])){echo $your_marks[$work->id];}
                            ?>
                        </b></div>
                    </div>

                    <div style='width:150px; float:right; text-align:right; font-size:12px;'>
                        <div>Сообщений: <b><?=$work->chat_messages;?></b></div>
                    </div>
                    <div style='clear:both;'></div>
                </a>
                <?
            }
        }
        */
        ?>
        <div style='clear:both;'></div>
        </div>
        <?
    }
}
?>
</div>

<?
if (Yii::app()->user->role != 'user'){
?>
<div style='padding-top:50px; padding-bottom:10px; margin-bottom:20px; border-bottom:2px solid #000;'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%;text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:30px;'>
                            <b>Заявки</b>
                        </div>
                    </td>
                    <td style='width:50%; text-align:right;' valign=middle>

                    </td>
            </tr>
            </table>
</div>

<div style=''>

    <div style='width:1060px; padding-bottom:10px; padding-top:10px; display:block; border-bottom:1px solid #ccc; font-weight:bold; position:relative; padding-left:10px; padding-right:10px;'>
        <div style=' float:left; width:150px; padding-right:10px; padding-left:0px; font-size:13px;' class='zaya'>
            Дата
        </div>
         <div style='width:250px; float:left; font-size:13px;'>
             Работа
        </div>
        <div style='width:150px;  float:left; font-size:13px;' class='zaya'>
            <div>Номинации</div>
        </div>
        <div style='width:150px; margin-right:20px; float:left; font-size:13px;' class='zaya'>
            <table style='width:100%; padding:0px; margin:0px;'>
                <tr>
                    <td valign=top>
                        <div style=' font-size:13px;'><b>Школа</b></div>
                    </td>
                    <td style='width:30px; text-align:right;' valign=top>
                        <?
                        $ar = array();
                        $ar['id'] = $model->id;
                        if ($nomination_filter != 0 ){$ar['nomination_filter'] = $nomination_filter;          }
                        if ($city != ''             ){$ar['city'] = $city;          }
                        if ($school != ''           ){$ar['school'] = $school;      }
                        if ($sort == '' || !in_array($sort,array('school_up','school_down'))             ){$ar['sort'] = 'school_down';  }
                        if ($sort == 'school_down'  ){$ar['sort'] = 'school_up';    }
                        //if ($sort == 'school_up'    ){$ar[] = '';                   }
                        $arrow = '&darr;&uarr;';
                        if ($sort == 'school_down'  ){ $arrow = '&darr;'; }
                        if ($sort == 'school_up'  ){ $arrow = '&uarr;'; }
                        ?>
                        <a href="<?=Yii::app()->createUrl('conf/confview',$ar);?>"style='font-size:13px;'><?=$arrow;?></a>
                    </td>
                </tr>
            </table>
        </div>

        <div style='width:150px; float:left; text-align:right; font-size:13px; padding-right:0px;'>
            <table style='width:100%; padding:0px; margin:0px;'>
                <tr>
                    <td valign=top>
                        <div style=' font-size:13px;'><b>Город</b></div>
                    </td>
                    <td style='width:30px; text-align:right;' valign=top>
                        <?
                        $ar = array();
                        $ar['id'] = $model->id;
                        if ($nomination_filter != 0 ){$ar['nomination_filter'] = $nomination_filter;          }
                        if ($city != ''             ){$ar['city'] = $city;          }
                        if ($school != ''           ){$ar['school'] = $school;      }
                        if ($sort == '' || !in_array($sort,array('city_up','city_down'))             ){$ar['sort'] = 'city_down';  }
                        if ($sort == 'city_down'  ){$ar['sort'] = 'city_up';    }
                        $arrow = '&darr;&uarr;';
                        if ($sort == 'city_down'  ){ $arrow = '&darr;'; }
                        if ($sort == 'city_up'  ){ $arrow = '&uarr;'; }
                        ?>
                        <a href="<?=Yii::app()->createUrl('conf/confview',$ar);?>"style='font-size:13px;'><?=$arrow;?></a>
                    </td>
                </tr>
            </table>
        </div>
        <div style='clear:both;'></div>
    </div>

    <div style='width:1060px; padding-bottom:0px; padding-top:10px; display:block; border-bottom:2px solid #ccc; font-weight:bold; position:relative; padding-left:10px; padding-right:10px; background:#f0f0f0;'>
        <form class='s_form' method="post" onsubmit='return false;'>

        <div style=' float:left; width:150px; padding-right:10px; padding-left:0px; font-size:13px;' class='zaya'>
            &nbsp;
        </div>
         <div style='width:250px; float:left; font-size:13px;'>
&nbsp;
        </div>
        <div style='width:150px;  float:left; font-size:13px;' class='zaya'>
                            <select class='nomination_filter' style='width:130px; height:30px; line-height:30px; border:1px solid #ccc; border-radius:3px;'>
                                <option value="0" selected="selected">Номинации</option>
                                <?
                                if ($nominations){
                                    foreach($nominations as $nomination)
                                    {
                                        $selected = '';
                                        if ($nomination->id == $nomination_filter){
                                        $selected = 'selected="selected"';
                                        }

                                        ?>
                                        <option value="<?=$nomination->id;?>" <?=$selected;?>><?=$nomination->title;?></option>
                                        <?
                                    }
                                }
                                ?>
                            </select>
        </div>
        <div style='width:150px; margin-right:20px;  float:left; font-size:13px; position:relative;' class='zaya'>
            <input type='text' class='school_input' style='width:150px; height:30px; line-height:30px; border:1px solid #ccc; border-radius:3px;' value='<?=$school;?>'/>
            <div onclick='search_clear("school_input")' title='Очистить' style='cursor:pointer; position:absolute; z-index:444; top:0px; right:0px; width:30px; height:30px; line-height:30px; text-align:center; font-size:14px;'>
                x
            </div>
        </div>

        <div style='width:150px; float:left; text-align:right; font-size:13px; padding-right:0px; position:relative;'>
            <input type='text' class='city_input' style='width:150px; height:30px; line-height:30px; border:1px solid #ccc; border-radius:3px;' value='<?=$city;?>'/>
            <div onclick='search_clear("city_input")'  title='Очистить' style='cursor:pointer; position:absolute; z-index:444; top:0px; right:0px; width:30px; height:30px; line-height:30px; text-align:center; font-size:14px;'>
                x
            </div>
        </div>

        <div style='width:150px; float:left; text-align:right; font-size:13px; padding-right:0px;'>
            <div onclick='search_start()' style='width:100px; line-height:1em;' class='btn btn-success'>Найти</div>
        </div>
        <div style='clear:both;'></div>

        </form>
    </div>
    <script>
     function search_start(){
         var city = $('.city_input').val();
         var school = $('.school_input').val();
         var search = '';
         var nomination = $('.nomination_filter option:selected').val();


         if (city != ''){
            search = search + '&city='+city;
         }
         if (school != ''){
            search = search + '&school='+school;
         }

         if (nomination != 0){
            search = search + '&nomination_filter='+nomination;
         }

         if (search != ''){
            $('.s_form').attr('onsubmit','');
            $('.s_form').attr('action','<?=Yii::app()->createUrl('conf/confview');?>&id=<?=$model->id;?>'+ search);
            $('.s_form').submit();
         }

         if (city == '' && school == '' && nomination == 0){
             document.location = '<?=Yii::app()->createUrl('conf/confview');?>&id=<?=$model->id;?>';
         }
     }


     function search_clear(input_class){
         $('.'+input_class).val('');
         search_start();
     }

     $(function()
     {
         $('.nomination_filter').change(function(){
             search_start();
         });
     });
    </script>
<?php
$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$provider,
'itemView'=>'_conf_order',
'viewData'=>array(

),
'template'=>"\n{items}\n<div style='padding-top:20px;'>{pager}</div>",
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/site_pages.css?v=1', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));
?>
</div










<?
}
?>




</div>
</div>
