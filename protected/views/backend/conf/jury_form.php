<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                           <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                            <div style='margin-top:0px; font-size:14px; text-transform:uppercase; color:#A0A0A0;'>
                                <a href="<?=Yii::app()->createUrl('conf/index');?>"><? echo $this->razdel['name']; ?></a> / <a href="<?=Yii::app()->createUrl('conf/jurylist');?>">Жюри</a>
                            </div>
                        </div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Создать нового члена Жюри', array('jurycreate'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<style>
.infoError{
    background:#FF9966; color:#fff; text-align:left; padding:8px 10px; border-radius:3px; margin-bottom:0px; margin-top:15px;
}

.infoSuccess{
    background:#33CC66; color:#fff; text-align:left; padding:8px 10px; border-radius:3px; margin-bottom:20px; margin-top:15px;
}

.optionsUserlist{
    font-size:14px; padding-bottom:10px; border-bottom: 1px dotted #ccc; padding-top:10px;
}
</style>

<div style='' class="form">

<div style=''>
<?
$form = $this->beginWidget('CActiveForm', array(
	    'id'=>'user-form',
	    'enableAjaxValidation'=>false,
		));
?>
<div>
<?php
  	  if(Yii::app()->user->hasFlash('userUpdateError')){
	  	echo '<div class="infoError">'.Yii::app()->user->getFlash('userUpdateError').'</div>';
	}

	if(Yii::app()->user->hasFlash('userUpdateSuccess')){
	  	echo '<div class="infoSuccess">'.Yii::app()->user->getFlash('userUpdateSuccess').'</div>';
	}


        if(Yii::app()->user->hasFlash('newUserError')){
            	echo '<div class="infoError" style="">'.Yii::app()->user->getFlash('newUserError').'</div>';
        }

?>
</div>
<div class="">
	<div class='title'>Имя</div>
	<div class='input'><?php echo $form->textField($users,'name',array('class' => 'form-control')); ?></div>
	<div style='clear:both;'></div>
</div>

<div class="" style='padding-top:15px;'>
	<div class='title'>E-mail</div>
	<div class='input'><?php echo $form->textField($users,'email',array('class' => 'form-control')); ?></div>
    <div style='clear:both;'></div>
</div>

<?
if ($this->route == 'conf/jurycreate'){
?>
<div class="" style='padding-top:15px;'>
    <div class='title' style='font-weight:normal;'>Пароль</div>
    <div class='input'><?php echo $form->textField($users,'password', array('class' => 'form-control')); ?></div>
    <div style='clear:both;'></div>
</div>
<?
}
?>

<div class="" style='padding-top:15px;'>
	<div>
		<?php echo CHtml::submitButton('Сохранить изменения', array('class'=>'btn btn-success','style'=>'')); ?>
		<div style='clear:both;'></div>
	</div>
</div>
<?php $this->endWidget();?>
</div>


<?
if ($this->route == 'conf/juryupdate'){

        $this->renderPartial('jury_update_noms', array(
            'nom_list' => $nom_list,
             'users'			=>	$users,
        ));
?>




<div style='padding-top:30px; padding-bottom: 30px; border-bottom: 1px dotted #ccc;'>
<div>
<?php
  	  if(Yii::app()->user->hasFlash('passError')){
	  	echo '<div class="infoError">'.Yii::app()->user->getFlash('passError').'</div>';
	}

	if(Yii::app()->user->hasFlash('passSuccess')){
	  	echo '<div class="infoSuccess">'.Yii::app()->user->getFlash('passSuccess').'</div>';
	}
?>
</div>
<?
$form = $this->beginWidget('CActiveForm', array(
	    'id'=>'user-form',
	    'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('conf/jurypass',array('id'=>$users->id))
		));
?>

<div class="" style='padding-top:15px;'>
	<div class='title'>Новый пароль</div>
	<div class='input'>
	<?php echo $form->textField($users,'password',array('class' => 'form-control','value'=>'')); ?>
	</div>
	<div style='clear:both;'></div>
</div>
<div class="" style='padding-top:15px;'>
	<div style='width: 400px;'>
		<?php echo CHtml::submitButton('Изменить пароль', array('class'=>'btn btn-success','style'=>'')); ?>
	</div>
	<div style='clear:both;'></div>
</div>
<?php $this->endWidget();?>
</div>


<?

if (Yii::app()->user->role == 'admin'){
?>
<div style='padding-top:20px; padding-bottom: 40px;'>
	<?
	$form = $this->beginWidget('CActiveForm', array(
		    'id'=>'user-form',
		    'enableAjaxValidation'=>false,
			'action' => Yii::app()->createUrl('conf/jurydelete',array('id'=>$users->id))
			));
	?>
	<?php $this->endWidget();?>

</div>
<?
}

}
?>
</div>
</div>
</div>