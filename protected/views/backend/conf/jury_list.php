<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                            <div style='margin-top:0px; font-size:14px; text-transform:uppercase; color:#A0A0A0;'>
                                <a href="<?=Yii::app()->createUrl('conf/index');?>"><? echo $this->razdel['name']; ?></a> / <a href="<?=Yii::app()->createUrl('conf/jurylist');?>">Жюри</a>
                            </div>
                        </div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('+ Нового члена Жюри', array('jurycreate'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>
<?
$class = 'btn-outline-success';
if ($this->route == 'conf/index'){$class = 'btn-success';}
?>
<a href="<?=Yii::app()->createUrl('conf/index',array());?>" class='btn <?=$class;?>'>Список проектов</a>

<?
$class = 'btn-outline-success';
if ($this->route == 'conf/jurylist'){$class = 'btn-success';}
?>
<a href="<?=Yii::app()->createUrl('conf/jurylist',array());?>" class='btn <?=$class;?>'>Список жюри</a>


<style>
a {
    color:#222;
}
</style>


<div style='padding-top:50px;'>

<div style='width:850px;'>
            <div style='padding-bottom:10px; margin-bottom:10px; border-bottom:2px solid #222;'>
                <div style='width:250px; float:left;'><b>Жюри</b></div>
                <div style='width:250px; float:left;'><b>E-mail</b></div>
                <div style='width:350px; float:right;'><b>Номинации</b></div>
                <div style='clear:both;'></div>
            </div>
        <?
        if ($jury_list)
        {
            foreach($jury_list as $jury){
            $nominations_array = array();
            $nominations = $jury->nominations;

            if ($nominations)
            {
                $t = 0;
                foreach($nominations as $nomination)
                {
                    $t = $t + 1;
                    if ($t == 6){$nominations_array[] = '...';}
                    if ($t < 6)
                    {
                        if (isset($nominations_list[$nomination->nom_id]))
                        {
                        $nominations_array[] = $nominations_list[$nomination->nom_id];
                        }
                    }
                    if ($t> 6){continue;}
                }
            }

            ?>
            <a href="<?=Yii::app()->createUrl('conf/juryupdate',array('id'=>$jury->id));?>"  style='display:block; padding-bottom:10px; margin-bottom:10px; border-bottom:1px dotted #ccc;'>
                    <div style='width:250px; float:left;'><?=$jury->name;?></div>
                    <div style='width:250px; float:left;'><?=$jury->email;?></div>
                    <div style='width:350px; float:right;'>
                        <?
                        if (count($nominations_array) > 0){
                            echo "<span style='color:#8A8A8A; font-size:14px;'>".implode(', ',$nominations_array)."</span>";
                        }
                        ?>
                    </div>
                    <div style='clear:both;'></div>
            </a>
            <?
            }
        }
        ?>


</div>
</div>
</div>
</div>
