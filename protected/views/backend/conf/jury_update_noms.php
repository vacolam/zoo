<div  class=''  style='margin-top:30px; padding-top:30px; border-top:1px dotted #ccc; margin-bottom:30px; '>
    <div class="">
        <div style='margin-bottom:5px;'>
                    <select class='form-nomination'style='width:250px; padding:0px 10px; height:40px; line-height:40px; border:1px solid #ccc; border-radius:4px; float:left;'>
                        <option value="0" selected="selected">Номинации</option>
                        <?
                        if ($nom_list){
                            foreach($nom_list as $index => $nomination)
                            {
                                ?>
                                <option value="<?=$index;?>"><?=$nomination;?></option>
                                <?
                            }
                        }
                        ?>
                    </select>

            <div style='float:left; width:38px; height:38px; margin-top:1px; margin-left:5px; font-size:20px; padding:0px; line-height:38px; text-align:center;' class='btn btn-outline-secondary' onclick='addNomination()'><b>+</b></div>
            <div style='clear:both;'></div>
        </div>


        <div style='' class='tags_block'>
            <ul style='list-style:none; padding:0px; margin:0px;' class='tags_ul'>
            <?
                $user_nominations = $users->nominations;
                if ($user_nominations){
                    foreach($user_nominations as $index => $user_nomination)
                    {
                        $nom = $user_nomination->nom;
                        $conf = $user_nomination->conf;

                        if ($nom and $conf)
                        {
                            ?>
                            <li style='padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;' class='tag_li' rel='<?=$nom->id;?>'>
                                <span><?=$conf->title;?>:: <?=$nom->title;?></span><span style='cursor:pointer;' title='Удалить' onclick='delete_nomination(<?=$nom->id;?>);'> [x]</span>
                            </li>
                            <?
                        }
                    }
                }
            ?>
            </ul>
        </div>
    </div>
</div>

        <script>
        function addNomination(){
            var nomination_id = $('.form-nomination option:selected').val();
            var nomination_title = $('.form-nomination option:selected').text();
            if (nomination_id > 0){
            var html = "<li  style='padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc; background:#f0f0f0;' class='tag_li new_tag'>"+
                        "<div style='font-size:14px;'>"+nomination_title+"</div>"+
                        "</li>";
            $('.tags_ul').append(html);

                $.ajax({
                    type: "GET",
                    url: '<?=Yii::app()->createUrl('conf/juryaddnom',array());?>',
                    dataType: 'json',
                    data: {
                        'id':nomination_id,
                        'user_id':<?=$users->id;?>,
                    },
                    success: function(response){
                        if (response != false){
                            var html = "<span>"+nomination_title+"</span><span style='cursor:pointer;' title='Удалить' onclick='delete_nomination("+response+");'> [x]</span>";
                            $('.new_tag').html(html);
                            $('.new_tag').prop('rel',response);
                            $('.new_tag').removeClass('new_tag');
                        }else{

                        }
                    },
                    error: function(response){
                    }
                });
            }
        }



        function delete_nomination(id){
            if (id != ''){
                $('.tag_li[rel="'+id+'"]').remove();
                $.ajax({
                    type: "GET",
                    url: '<?=Yii::app()->createUrl('conf/jurydeletenom',array());?>',
                    dataType: 'json',
                    data: {
                        'nom_id':id,
                        'user_id':<?=$users->id;?>,
                    },
                    success: function(response){
                        if (response != false){
                        }else{
                        }
                    },
                    error: function(response){
                    }
                });
            }
        }
        </script>