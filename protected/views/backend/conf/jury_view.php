<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:80%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><?=$model->title;?></a>
                        </div>
                    </td>
                    <td style='width:20%; height:100px; text-align:right;' valign=middle>
                        <a href="<?=Yii::app()->createUrl('conf/confupdate',array('id'=>$model->id));?>" class='btn btn-outline-success' style=''>
                            <svg class="bi bi-gear" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8.837 1.626c-.246-.835-1.428-.835-1.674 0l-.094.319A1.873 1.873 0 014.377 3.06l-.292-.16c-.764-.415-1.6.42-1.184 1.185l.159.292a1.873 1.873 0 01-1.115 2.692l-.319.094c-.835.246-.835 1.428 0 1.674l.319.094a1.873 1.873 0 011.115 2.693l-.16.291c-.415.764.42 1.6 1.185 1.184l.292-.159a1.873 1.873 0 012.692 1.116l.094.318c.246.835 1.428.835 1.674 0l.094-.319a1.873 1.873 0 012.693-1.115l.291.16c.764.415 1.6-.42 1.184-1.185l-.159-.291a1.873 1.873 0 011.116-2.693l.318-.094c.835-.246.835-1.428 0-1.674l-.319-.094a1.873 1.873 0 01-1.115-2.692l.16-.292c.415-.764-.42-1.6-1.185-1.184l-.291.159A1.873 1.873 0 018.93 1.945l-.094-.319zm-2.633-.283c.527-1.79 3.065-1.79 3.592 0l.094.319a.873.873 0 001.255.52l.292-.16c1.64-.892 3.434.901 2.54 2.541l-.159.292a.873.873 0 00.52 1.255l.319.094c1.79.527 1.79 3.065 0 3.592l-.319.094a.873.873 0 00-.52 1.255l.16.292c.893 1.64-.902 3.434-2.541 2.54l-.292-.159a.873.873 0 00-1.255.52l-.094.319c-.527 1.79-3.065 1.79-3.592 0l-.094-.319a.873.873 0 00-1.255-.52l-.292.16c-1.64.893-3.433-.902-2.54-2.541l.159-.292a.873.873 0 00-.52-1.255l-.319-.094c-1.79-.527-1.79-3.065 0-3.592l.319-.094a.873.873 0 00.52-1.255l-.16-.292c-.892-1.64.902-3.433 2.541-2.54l.292.159a.873.873 0 001.255-.52l.094-.319z" clip-rule="evenodd"/>
                                <path fill-rule="evenodd" d="M8 5.754a2.246 2.246 0 100 4.492 2.246 2.246 0 000-4.492zM4.754 8a3.246 3.246 0 116.492 0 3.246 3.246 0 01-6.492 0z" clip-rule="evenodd"/>
                            </svg>
                        </a>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<style>
a {
    color:#222;
}
</style>

<div style='background:#fff; box-shadow:0px 0px 2px rgba(0,0,0,.1); border-radius:5px;'>
    <div style='padding:20px; font-size:16px; line-height:1.4em;'>
    <div style='font-size:19px; margin-bottom:15px;'><b>Описание</b></div>
    <div style='padding-bottom:7px;'><i>Дата: <?=Dates::getName($model->date);?></i></div>
    <?=$this->crop_str_word($model->text, 30 );?>
    <div>
    <a href='<?= Yii::app()->createUrl('conf/confupdate',array('id'=>$model->id)); ?>' style='line-height:1em;' class='btn btn-success'>Редактировать</a>
    </div>
    </div>
</div>

<div style='padding-top:50px;'>
<div style='padding-bottom:10px; margin-bottom:20px; border-bottom:2px solid #000;'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%;text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:30px;'>
                            <b>Номинации</b>
                        </div>
                    </td>
                    <td style='width:50%; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить номинацию', array('nomcreate','conf_id'=>$model->id), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
</div>
<?
if ($nominations)
{
    foreach($nominations as $nomination)
    {
        $jury_array = array();
        $juries = $nomination->juries;
        if ($juries)
        {
            foreach($juries as $jury)
            {
                $jury_array[] = $jury->user->name;
            }
        }
        ?>
        <div style='padding-bottom:40px; '>
        <a href="<?= Yii::app()->createUrl('conf/nomupdate',array('id'=>$nomination->id)); ?>" style='font-size:18px; '><b><?=$nomination->title;?></b></a>
        <div style='padding-bottom:25px; padding-top:0px;'>
        <?
        if (count($jury_array) > 0){
            echo "<span style='color:#8A8A8A; font-size:14px;'>Жюри номинации: ".implode(', ',$jury_array)."</span>";
        }
        ?>
        </div>
        <?
        $works = $nomination->works;
        if ($works){
            foreach($works as $work)
            {
                $bg = '';
                if (isset($your_marks[$work->id])){ $bg = 'background:#FFFFFF'; }
                ?>
                <a href="<?= Yii::app()->createUrl('conf/work',array('id'=>$work->id)); ?>" style='padding-bottom:10px; padding-top:10px; display:block; border-bottom:1px dotted #ccc; <?=$bg;?>; position:relative;'>
                    <?
                            if (isset($your_marks[$work->id])){
                                ?>
                                <div style='position:absolute; width:30px; height:30px; border-radius:50%; top:50%; margin-top:-15px; right:-40px; background:url(css_tool/added.png) #28A745 center; box-shadow:0px 0px 2px rgba(0,0,0,.1);'></div>
                                <?
                            }
                        ?>
                    <div style='width:100px; height:100px; border-radius:3px; display:block; float:left; margin-right:20px; margin-bottom:2px; background:url(<?=$work->getThumbnailUrl();?>); background-size:cover; position:relative;'></div>
                    <div style='width:200px; float:left;'>
                        <div><i>Имя: <b><?=$work->name;?></b></i></div>
                        <div><i>Дата: <b><?=Dates::getName($work->date);?></b></i></div>
                    </div>
                    <div style='width:200px; float:left;'>
                        <i>Сумма оценок: <b><?=$work->mark;?></b></i>
                        <i>Количество голосов: <b><?=$work->marks_count;?></b></i>
                    </div>
                    <div style='width:200px; float:left;'>
                        <i>Ваша оценка: <b>
                            <?
                            if (isset($your_marks[$work->id])){echo $your_marks[$work->id];}
                            ?>
                        </b></i>
                    </div>
                    <div style='width:150px; float:right;'>
                        <i>Сообщений в чате: <b><?=$work->chat_messages;?></b></i>
                    </div>
                    <div style='clear:both;'></div>
                </a>
                <?
            }
        }
        ?>
        <div style='clear:both;'></div>
        </div>
        <?
    }
}
?>
</div>
</div>
</div>
