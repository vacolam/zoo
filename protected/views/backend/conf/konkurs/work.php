<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:100%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                            <div style='margin-top:0px; font-size:14px; text-transform:uppercase; color:#A0A0A0;'>
                                <a href="<?=Yii::app()->createUrl('conf/index');?>"><? echo $this->razdel['name']; ?></a> /
                                <a href="<?=Yii::app()->createUrl('conf/confview',array('id'=>$conf->id));?>"><? echo mb_substr($conf->title, 0, 50,'utf-8'); ?>...</a> /
                                <a href="<?=Yii::app()->createUrl('conf/confview',array('id'=>$conf->id));?>">НОМИНАЦИЯ <? if (isset($nom)){ echo mb_substr($nom->title, 0, 30,'utf-8'); } ?>...</a> /
                                <a href="<?=Yii::app()->createUrl('conf/work',array('id'=>$work->id));?>">РАБОТА № <? echo $work->id; ?></a>
                            </div>
                        </div>
                    </td>
            </tr>
            </table>
        </div>
    </div>

<div class='content_one'>

<style>
a {
    color:#222;
}
</style>

<div style='text-align:center; font-size:26px; font-weight:400;'>Работа № <?=$work->id;?></div>
<h3 style='text-align:center;'><b><?=$work->work_name;?></b></h3>
<h3 style='text-align:center;'><b><? if (isset($nom)){ echo $nom->title; }?></b></h3>
<div style='margin-top:-5px;text-align:center; text-transform:uppercase;'><a href="<?=Yii::app()->createUrl('conf/confview',array('id'=>$conf->id));?>" style='font-size:14px;'><? echo $conf->title; ?></a></div>




<div style='margin-top:30px;'>
<style>
.table td{
    padding-top:7px; padding-bottom:7px; border-bottom:1px dotted #ccc; vertical-align:middle;
}
</style>
<div style='width:450px; float:left;'>
<div style='padding-bottom:7px; font-size:13px; text-align:center; text-transform:uppercase;'><b>Данные работы</b></div>
    <table style='width:100%; border-collapse:collapse;' class='table'>
        <tr><td style='width:150px; text-align:left;'><b>Название:</b></td><td style='text-align:right;'><?=$work->work_name;?></td></tr>
        <tr><td style='width:150px; text-align:left;'><b>Номинация:</b></td><td style='text-align:right;'><? if (isset($nom)){ echo $nom->title; } ?></td></tr>
        <tr><td style='text-align:left;'><b>Дата работы:</b></td><td style='text-align:right;'><?=Dates::getName($work->date);?></td></tr>

        <!--
        <tr><td style='text-align:left;'><b>Сумма оценок:</b></td><td style='text-align:right;'><b><?=$work->mark;?></b></td></tr>
        <tr><td style='text-align:left;'><b>Количество голосов:</b></td><td style='text-align:right;'><?=$work->marks_count;?></td></tr>
        -->
    </table>
</div>

<div style='width:350px; float:right;'>
<div style='padding-bottom:7px; font-size:13px; text-align:center; text-transform:uppercase;'><b>Файлы</b></div>
<?
$docs = $work->docs;
$this->renderPartial('_documents', array('docs'=>$docs));

$photos = $work->photos;
$this->renderPartial('_photos', array('photos'=>$photos,'model'=>$work,'margin_top'=>'40'));
?>
</div>
<div style='clear:both;'></div>

<div style='width:100%; margin-top:30px;'>
<div style='font-size:16px;'><b>Анкета участника</b></div>
<table style='width:100%; border-collapse:collapse;' class='table'>
        <tr><td style='text-align:left;'><b>ФИО автора:</b></td><td style='text-align:right;'><?=$work->name;?></td></tr>
        <tr><td style='text-align:left;'><b>Дата рождения:</b></td><td style='text-align:right;'><?=$work->birth;?></td></tr>
        <tr><td style='text-align:left;'><b>Населенный пункт:</b></td><td style='text-align:right;'><?=$work->city;?></td></tr>
        <tr><td style='text-align:left;'><b>Школа, клуб, станция юннатов, дом культуры:</b></td><td style='text-align:right;'><?=$work->shcool;?></td></tr>
        <tr><td style='text-align:left;'><b>Класс:</b></td><td style='text-align:right;'><?=$work->class;?></td></tr>
        <tr><td style='text-align:left; padding-top:30px; padding-left:0px;'><b>Педагог</b></td><td style='text-align:right;'>&nbsp;</td></tr>
        <tr><td style='text-align:left;'><b>ФИО педагога:</b></td><td style='text-align:right;'><?=$work->teachers_name;?></td></tr>
        <tr><td style='text-align:left;'><b>Должность и место работы педагог:</b></td><td style='text-align:right;'><?=$work->teachers_occupation;?></td></tr>
        <tr><td style='text-align:left;'><b>E-mail педагога:</b></td><td style='text-align:right;'><?=$work->teachers_email;?></td></tr>
        <tr><td style='text-align:left;'><b>Мобильный телефон педагога:</b></td><td style='text-align:right;'><?=$work->teachers_cell;?></td></tr>
    </table>

</div>

<div style='width:500px; float:left;  margin-top:50px; display:none;'>
            <div style='padding-bottom:10px; margin-bottom:10px; border-bottom:2px solid #222;'>
                <div style='width:250px; float:left;'><b>№</b></div>
                <div style='width:150px; float:left;'><b>Дата</b></div>
                <div style='width:100px; float:right;'><b>Оценка</b></div>
                <div style='clear:both;'></div>
            </div>
        <?
            if (count($marks) > 0)
            {
                $t = 0;
                foreach($marks as $index => $mark)
                {
                    $t = $t + 1;
                ?>
                <div style='padding-bottom:10px; margin-bottom:10px; border-bottom:1px dotted #ccc;'>
                        <div style='width:250px; float:left;'><?=$t;?></div>
                        <div style='width:150px; float:left;'><? echo Dates::getName($mark->date); ?></div>
                        <div style='width:100px; float:right;'><? echo $mark->mark; ?></div>
                        <div style='clear:both;'></div>
                </div>
                <?
                }
            }
        ?>


</div>

<div style='clear:both;'></div>
</div>
</div>
</div>

<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({});
});
</script>
