<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:100%; height:100px; text-align:left;' valign=middle>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                            <div style='margin-top:0px; font-size:14px; text-transform:uppercase; color:#A0A0A0;'>
                                <a href="<?=Yii::app()->createUrl('conf/index');?>"><? echo $this->razdel['name']; ?></a> / <a href="<?=Yii::app()->createUrl('conf/confview',array('id'=>$conf->id));?>"><? echo mb_substr($conf->title, 0, 50,'utf-8'); ?>...</a> /
                                <? if ($model->isNewRecord){?><a href="<?=Yii::app()->createUrl('conf/nomcreate',array('id'=>$conf->id));?>">Добавить номинацию</a><?}?>
                                <? if (!$model->isNewRecord){?><a href="<?=Yii::app()->createUrl('conf/nomupdate',array('id'=>$model->id));?>">НОМИНАЦИЯ <? echo mb_substr($model->title, 0, 30,'utf-8'); ?>...</a> / <span>Редактировать</span><?}?>
                            </div>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one form'>

<script src="js/bootstrap-select-1.13.14.min.js"></script>
<link href="js/bootstrap-select-1.13.14.min.css" rel="stylesheet">


<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
    'class' => 'form_news',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <div style='width:550px; float:left; margin-top:-5px;'>

    <div class="form-group<?= $model->hasErrors('title') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'title', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'title', array('class' => 'form-control','style'=>'height:100px; width:100%;')); ?>
        <?= CHtml::error($model, 'title', array('class' => 'help-block')); ?>
    </div>
 <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

            <div style='with:100px; float:right;'>
                <?
                if ($this->route == Yii::app()->controller->id.'/nomupdate'){
                ?>
                <a href='<?= Yii::app()->createUrl(Yii::app()->controller->id.'/nomdelete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>


<?
if ($conf->type == 'conf' || $conf->type == 'festival' )
{
if ($this->route == 'conf/nomupdate')
{

        $this->renderPartial('noms_add_jury', array(
            'users_list' =>  $users_list,
            'model' =>  $model,
        ));
}
}
?>

</div>
</div>
</div>