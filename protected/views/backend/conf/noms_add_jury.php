<?
$user_nominations = $model->juries;
$added_users = array();
$added_users[] = 0;
if ($user_nominations){
    foreach($user_nominations as $user_nomination)
    {
    $added_users[] = $user_nomination->user_id;
    }
}
?>
<div  class=''  style='margin-top:30px; padding-top:30px; border-top:1px dotted #ccc; margin-bottom:30px; '>
    <div class="">
        <div style='margin-bottom:5px;'>
                    <select class='form-nomination'style='width:250px; padding:0px 10px; height:40px; line-height:40px; border:1px solid #ccc; border-radius:4px; float:left;'>
                        <option value="0" selected="selected">Жюри</option>
                        <?
                        if ($users_list){
                            foreach($users_list as $index => $user)
                            {
                                if (in_array($user->id,$added_users)){continue;}
                                ?>
                                <option value="<?=$user->id;?>"><?=$user->id;?>. <?=$user->name;?> <?=$user->email;?></option>
                                <?
                            }
                        }
                        ?>
                    </select>

            <div style='float:left; width:38px; height:38px; margin-top:1px; margin-left:5px; font-size:20px; padding:0px; line-height:38px; text-align:center;' class='btn btn-outline-secondary' onclick='addUser()'><b>+</b></div>
            <div style='clear:both;'></div>
        </div>


        <div style='' class='tags_block'>
            <ul style='list-style:none; padding:0px; margin:0px;' class='tags_ul'>
            <?

                if ($user_nominations){
                    foreach($user_nominations as $index => $user_nomination)
                    {
                        $user = $user_nomination->user;

                        if ($user)
                        {
                            ?>
                            <li style='padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;' class='tag_li' rel='<?=$user->id;?>'>
                                <span><?=$user->id;?>. <?=$user->name;?> - <?=$user->email;?></span><span style='cursor:pointer;' title='Удалить' onclick='delete_user(<?=$user->id;?>);'> [x]</span>
                            </li>
                            <?
                        }
                    }
                }
            ?>
            </ul>
        </div>
    </div>
</div>

        <script>
        function addUser(){
            var user_id = $('.form-nomination option:selected').val();
            var user_title = $('.form-nomination option:selected').text();
            if (user_id > 0){
            var html = "<li  style='padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc; background:#f0f0f0;' class='tag_li new_tag'>"+
                        "<div style='font-size:14px;'>"+user_title+"</div>"+
                        "</li>";
            $('.tags_ul').append(html);

                $.ajax({
                    type: "GET",
                    url: '<?=Yii::app()->createUrl('conf/nomaddjury',array());?>',
                    dataType: 'json',
                    data: {
                        'nom_id':<?=$model->id?>,
                        'user_id':user_id,
                    },
                    success: function(response){
                        if (response != false){
                            var html = "<span>"+user_title+"</span><span style='cursor:pointer;' title='Удалить' onclick='delete_user("+response+");'> [x]</span>";
                            $('.new_tag').html(html);
                            $('.new_tag').prop('rel',response);
                            $('.new_tag').removeClass('new_tag');
                        }else{

                        }
                    },
                    error: function(response){
                    }
                });
            }
        }



        function delete_user(id){
            if (id != ''){
                $('.tag_li[rel="'+id+'"]').remove();
                $.ajax({
                    type: "GET",
                    url: '<?=Yii::app()->createUrl('conf/jurydeletenom',array());?>',
                    dataType: 'json',
                    data: {
                        'nom_id':<?=$model->id?>,
                        'user_id':id,
                    },
                    success: function(response){
                        if (response != false){
                        }else{
                        }
                    },
                    error: function(response){
                    }
                });
            }
        }
        </script>