<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:80%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <?
                            $link = Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());
                            if (Yii::app()->user->role == 'user'){
                            $link = Yii::app()->createUrl('conf/jury',array());
                            }
                            ?>

                            <a href="<?=$link;?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                            <div style='margin-top:0px; font-size:14px; text-transform:uppercase; color:#A0A0A0;'>
                                <a href="<?=$link;?>"><? echo $this->razdel['name']; ?></a> /
                                <a href="<?=Yii::app()->createUrl('conf/confview',array('id'=>$model->id));?>"><? echo mb_substr($model->title, 0, 50,'utf-8'); ?>...</a>
                            </div>
                        </div>
                    </td>
                    <td style='width:10%; height:100px; text-align:right;' valign=middle>

                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<style>
a {
    color:#222;
}
</style>

<div style='background:#fff; box-shadow:0px 0px 2px rgba(0,0,0,.1); border-radius:5px; margin-bottom:50px;'>
    <div style='padding:20px; font-size:16px; line-height:1.4em;'>
    <div style='font-size:22px; margin-bottom:15px;'>
            <b><?=$model->title;?></b>
            <?
            if ($model->form_is_open == 1){
                echo " (Разрешено присылать анкеты)";
            }
            ?>
    </div>
    <div style='padding-bottom:7px;'><i>Дата: <?=Dates::getName($model->date);?></i></div>
    <?=$this->crop_str_word($model->text, 30 );?>
    <div style='margin-top:30px;'>
    <a href='<?= Yii::app()->createUrl('conf/confupdate',array('id'=>$model->id)); ?>' style='line-height:1em;' class='btn btn-success'>Редактировать</a>
    </div>
    </div>
</div>


<div style=''>
<div style='padding-bottom:10px; margin-bottom:20px; border-bottom:2px solid #000;'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%;text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:30px;'>
                            <b>Вопросы</b>
                        </div>
                    </td>
                    <td style='width:50%; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить вопрос', array('oproscreate','conf_id'=>$model->id), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
</div>

<?
if ($model->opros)
{
    foreach($model->opros as $question)
    {
    ?>
    <div style='padding-bottom:20px; margin-bottom:20px; border-bottom:1px solid #ccc;'>
                    <a href="<?= Yii::app()->createUrl('conf/oprosupdate',array('id'=>$question->id)); ?>" style='display:block;'>
                        <div style='width:150px; height:100px; float:left; <? if ($question->filename != ''){?> background: url(<?= $question->getImageUrl(); ?>) #F8F8F8; <?} ?>background-size:cover; border-radius:2px;' alt="" class="slide-img">
                        </div>
                        <div style='width: 500px; float:left; padding-left:40px; '>
                            <div style='font-size:16px; font-weight:600;' class='open-s'><?=$question->question;?></div>
                            <div style='font-size:14px; line-height:1.3em; padding-top:10px;' class='open-s'><?=$question->right_answer;?></div>
                            <div style='font-size:14px; line-height:1.3em; padding-top:10px;' class='open-s'><?=$question->wrong_answer;?></div>
                        </div>
                        <div style='clear:both;'></div>
                    </a>

    </div>
    <?
    }
}
?>

<div style='padding-bottom:10px; margin-bottom:20px; border-bottom:2px solid #000; margin-top:50px;'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%;text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:30px;'>
                            <b>Анкеты</b>
                        </div>
                    </td>
                    <td style='width:50%; text-align:right;' valign=middle>

                    </td>
            </tr>
            </table>
</div>
<?
if ($nominations)
{
    foreach($nominations as $nomination)
    {
        $jury_array = array();
        $juries = $nomination->juries;
        if ($juries)
        {
            foreach($juries as $jury)
            {
                $jury_array[] = $jury->user->name;
            }
        }

        ?>
        <div style='padding-bottom:40px; '>
        <?
        $works = $nomination->works;
        if ($works){
            foreach($works as $work)
            {
                ?>
                <div style='padding-bottom:10px; padding-top:10px; display:block; border-bottom:1px dotted #ccc; position:relative; padding-left:10px; padding-right:10px;'>
                    <div style='position:absolute; width:30px; height:30px; border-radius:50%; top:50%; margin-top:-15px; left:-35px;'><b><?=$work->id;?>.</b></div>
                     <div style='width:300px; float:left;'>
                        <div><b><?=$work->name;?></b></div>
                    </div>
                    <div style='width:150px; float:left; font-size:12px;'>
                        <div><?=Dates::getName($work->date);?></div>
                    </div>
                    <div style='width:200px; float:left; font-size:12px;'>
                        <div><?=$work->phone;?></div>
                    </div>
                    <div style='clear:both;'></div>
                </div>
                <?
            }
        }
        ?>
        <div style='clear:both;'></div>
        </div>
        <?
    }
}
?>

</div>
</div>
</div>
