<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:100%; height:100px; text-align:left;' valign=middle>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                            <div style='margin-top:0px; font-size:14px; text-transform:uppercase; color:#A0A0A0;'>
                                <a href="<?=Yii::app()->createUrl('conf/index');?>"><? echo $this->razdel['name']; ?></a> / <a href="<?=Yii::app()->createUrl('conf/confview',array('id'=>$conf->id));?>"><? echo mb_substr($conf->title, 0, 50,'utf-8'); ?>...</a> /
                                <? if ($model->isNewRecord){?><a href="<?=Yii::app()->createUrl('conf/oproscreate',array('id'=>$conf->id));?>">Добавить вопрос</a><?}?>
                                <? if (!$model->isNewRecord){?><a href="<?=Yii::app()->createUrl('conf/oprosupdate',array('id'=>$model->id));?>">Вопрос <? echo mb_substr($model->question, 0, 30,'utf-8'); ?>...</a> / <span>Редактировать</span><?}?>
                            </div>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one form'>

<div class="form" style='padding-top:20px;'>
<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <?php
        if (!$model->isNewRecord) {
            if ($model->filename != '')
            {
            ?>
            <div style='width:600px; height: 300px; margin-bottom:50px; background:url(<?=$model->getImageUrl();?>) #f0f0f0 center; background-size:cover; border-radius:5px; border:1px solid rgba(0,0,0,.2);'>
            </div>
            <?
            }
        }
    ?>

    <div class="form-group<?= $model->hasErrors('image') ? ' has-error' : ''; ?>" style='padding:10px; border:1px solid #ccc; background:#fff; border-radius:3px 3px 0px 0px; margin-bottom:0px; border-bottom:none;'>
        <?= CHtml::activeLabel($model, 'image', array('class' => 'control-label')); ?>
        <?= CHtml::activeFileField($model, 'image'); ?>
        <?= CHtml::error($model, 'image', array('class' => 'help-block')); ?>
    </div>


    <div style='padding:10px; border:1px solid rgba(0,0,0,.1); background:#0099FF; color:#fff; border-radius:0px 0px 3px 3px;  margin-bottom:20px;'><b>Min: ширина 600px / высота 300px</b>. Ширина должна быть в два раза больше высоты.</div>

    <div class="form-group<?= $model->hasErrors('number') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'number', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'number', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'number', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('question') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'question', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'question', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'question', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('right_answer') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'right_answer', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'right_answer', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'right_answer', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('wrong_answer') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'wrong_answer', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'wrong_answer', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'wrong_answer', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('description') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'description', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'description', array('class' => 'form-control','style'=>'height:100px; width:100%;')); ?>
        <?= CHtml::error($model, 'description', array('class' => 'help-block')); ?>
    </div>




    <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

                    <div style='with:100px; float:right;'>
                <?
                if ($this->route == 'conf/oprosupdate'){
                ?>
                <a href='<?= Yii::app()->createUrl('conf/oprosdelete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем вопрос?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>
    
<?php echo CHtml::endForm(); ?>
</div>
</div>
</div>
