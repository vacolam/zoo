<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                            <div style='margin-top:0px; font-size:14px; text-transform:uppercase; color:#A0A0A0;'>
                                <a href="<?=Yii::app()->createUrl('conf/index');?>"><? echo $this->razdel['name']; ?></a> / <a href="<?=Yii::app()->createUrl('conf/orders');?>">Список заявок</a>
                            </div>
                        </div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
            <div class="dropdown" style=''>
                <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=''>
                    Добавить
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?

                    foreach($typeList as $index => $type)
                    {
                        if ($index == '0'){continue;}
                        ?>
                        <a href="<?= Yii::app()->createUrl('conf/confcreate',array('type'=>$index)); ?>" class="dropdown-item" style=''><?=$type;?></a>
                        <?
                    }
                    ?>
                </div>
            </div>

                        <a href="<?=Yii::app()->createUrl('cats/update',array('id'=>$this->razdel['id']));?>" class='btn btn-outline-success' style='display:none;'>
                            <svg class="bi bi-gear" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8.837 1.626c-.246-.835-1.428-.835-1.674 0l-.094.319A1.873 1.873 0 014.377 3.06l-.292-.16c-.764-.415-1.6.42-1.184 1.185l.159.292a1.873 1.873 0 01-1.115 2.692l-.319.094c-.835.246-.835 1.428 0 1.674l.319.094a1.873 1.873 0 011.115 2.693l-.16.291c-.415.764.42 1.6 1.185 1.184l.292-.159a1.873 1.873 0 012.692 1.116l.094.318c.246.835 1.428.835 1.674 0l.094-.319a1.873 1.873 0 012.693-1.115l.291.16c.764.415 1.6-.42 1.184-1.185l-.159-.291a1.873 1.873 0 011.116-2.693l.318-.094c.835-.246.835-1.428 0-1.674l-.319-.094a1.873 1.873 0 01-1.115-2.692l.16-.292c.415-.764-.42-1.6-1.185-1.184l-.291.159A1.873 1.873 0 018.93 1.945l-.094-.319zm-2.633-.283c.527-1.79 3.065-1.79 3.592 0l.094.319a.873.873 0 001.255.52l.292-.16c1.64-.892 3.434.901 2.54 2.541l-.159.292a.873.873 0 00.52 1.255l.319.094c1.79.527 1.79 3.065 0 3.592l-.319.094a.873.873 0 00-.52 1.255l.16.292c.893 1.64-.902 3.434-2.541 2.54l-.292-.159a.873.873 0 00-1.255.52l-.094.319c-.527 1.79-3.065 1.79-3.592 0l-.094-.319a.873.873 0 00-1.255-.52l-.292.16c-1.64.893-3.433-.902-2.54-2.541l.159-.292a.873.873 0 00-.52-1.255l-.319-.094c-1.79-.527-1.79-3.065 0-3.592l.319-.094a.873.873 0 00.52-1.255l-.16-.292c-.892-1.64.902-3.433 2.541-2.54l.292.159a.873.873 0 001.255-.52l.094-.319z" clip-rule="evenodd"/>
                                <path fill-rule="evenodd" d="M8 5.754a2.246 2.246 0 100 4.492 2.246 2.246 0 000-4.492zM4.754 8a3.246 3.246 0 116.492 0 3.246 3.246 0 01-6.492 0z" clip-rule="evenodd"/>
                            </svg>
                        </a>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>
<?
$class = 'btn-outline-success';
if ($this->route == 'conf/index'){
    if (!isset($_GET['type'])){
    $class = 'btn-success';
    }
}
?>
<a href="<?=Yii::app()->createUrl('conf/index',array());?>" class='btn <?=$class;?>' style='font-size:13px;'>Весь список</a>
<?
foreach($typeList as $index => $type)
{
    if ($index == '0'){continue;}
    $class='btn-outline-success';
    if ($this->route == 'conf/index')
    {
        if (isset($_GET['type'])){
            if ($_GET['type'] == $index){$class='btn-success'; }
        }
    }
    ?>
    <a href="<?=Yii::app()->createUrl('conf/index',array('type'=>$index));?>" class='btn <?=$class;?>'  style='font-size:13px;'><?=$type;?></a>
    <?
}
?>

<?
$class = 'btn-outline-success';
if ($this->route == 'conf/jurylist'){$class = 'btn-success';}
?>
<a href="<?=Yii::app()->createUrl('conf/jurylist',array());?>" class='btn <?=$class;?>'  style='font-size:13px;'>Список жюри</a>
<a href="<?=Yii::app()->createUrl('conf/orders',array());?>" class='btn btn-success'  style='font-size:13px;'>Все заявки</a>

<style>
a {
    color:#222;
}
</style>


<div style='padding-top:50px;'>

    <div style='width:1060px; padding-bottom:10px; padding-top:10px; display:block; border-bottom:1px solid #ccc; font-weight:bold; position:relative; padding-left:10px; padding-right:10px;'>
        <div style=' float:left; width:150px; padding-right:10px; padding-left:0px; font-size:13px;' class='zaya'>
            Дата
        </div>
         <div style='width:250px; float:left; font-size:13px;'>
             Работа
        </div>
        <div style='width:150px;  float:left; font-size:13px;' class='zaya'>
            <div>Имя участника</div>
        </div>
        <div style='width:150px; margin-right:20px; float:left; font-size:13px;' class='zaya'>
            <table style='width:100%; padding:0px; margin:0px;'>
                <tr>
                    <td valign=top>
                        <div style=' font-size:13px;'><b>Школа</b></div>
                    </td>
                    <td style='width:30px; text-align:right;' valign=top>
                        <?
                        $ar = array();
                        if ($city != ''             ){$ar['city'] = $city;          }
                        if ($school != ''           ){$ar['school'] = $school;      }
                        if ($sort == '' || !in_array($sort,array('school_up','school_down'))             ){$ar['sort'] = 'school_down';  }
                        if ($sort == 'school_down'  ){$ar['sort'] = 'school_up';    }
                        //if ($sort == 'school_up'    ){$ar[] = '';                   }
                        $arrow = '&darr;&uarr;';
                        if ($sort == 'school_down'  ){ $arrow = '&darr;'; }
                        if ($sort == 'school_up'  ){ $arrow = '&uarr;'; }
                        ?>
                        <a href="<?=Yii::app()->createUrl('conf/orders',$ar);?>"style='font-size:13px;'><?=$arrow;?></a>
                    </td>
                </tr>
            </table>
        </div>

        <div style='width:150px; float:left; text-align:right; font-size:13px; padding-right:0px;'>
            <table style='width:100%; padding:0px; margin:0px;'>
                <tr>
                    <td valign=top>
                        <div style=' font-size:13px;'><b>Город</b></div>
                    </td>
                    <td style='width:30px; text-align:right;' valign=top>
                        <?
                        $ar = array();
                        if ($city != ''             ){$ar['city'] = $city;          }
                        if ($school != ''           ){$ar['school'] = $school;      }
                        if ($sort == '' || !in_array($sort,array('city_up','city_down'))             ){$ar['sort'] = 'city_down';  }
                        if ($sort == 'city_down'  ){$ar['sort'] = 'city_up';    }
                        $arrow = '&darr;&uarr;';
                        if ($sort == 'city_down'  ){ $arrow = '&darr;'; }
                        if ($sort == 'city_up'  ){ $arrow = '&uarr;'; }
                        ?>
                        <a href="<?=Yii::app()->createUrl('conf/orders',$ar);?>"style='font-size:13px;'><?=$arrow;?></a>
                    </td>
                </tr>
            </table>
        </div>
        <div style='clear:both;'></div>
    </div>

    <div style='width:1060px; padding-bottom:0px; padding-top:10px; display:block; border-bottom:2px solid #ccc; font-weight:bold; position:relative; padding-left:10px; padding-right:10px; background:#f0f0f0;'>
        <form class='s_form' method="post" onsubmit='return false;'>

        <div style=' float:left; width:150px; padding-right:10px; padding-left:0px; font-size:13px;' class='zaya'>
            &nbsp;
        </div>
         <div style='width:250px; float:left; font-size:13px;'>
&nbsp;
        </div>
        <div style='width:150px;  float:left; font-size:13px;' class='zaya'>
&nbsp;
        </div>
        <div style='width:150px; margin-right:20px;  float:left; font-size:13px; position:relative;' class='zaya'>
            <input type='text' class='school_input' style='width:150px; height:30px; line-height:30px; border:1px solid #ccc; border-radius:3px;' value='<?=$school;?>'/>
            <div onclick='search_clear("school_input")' title='Очистить' style='cursor:pointer; position:absolute; z-index:444; top:0px; right:0px; width:30px; height:30px; line-height:30px; text-align:center; font-size:14px;'>
                x
            </div>
        </div>

        <div style='width:150px; float:left; text-align:right; font-size:13px; padding-right:0px; position:relative;'>
            <input type='text' class='city_input' style='width:150px; height:30px; line-height:30px; border:1px solid #ccc; border-radius:3px;' value='<?=$city;?>'/>
            <div onclick='search_clear("city_input")'  title='Очистить' style='cursor:pointer; position:absolute; z-index:444; top:0px; right:0px; width:30px; height:30px; line-height:30px; text-align:center; font-size:14px;'>
                x
            </div>
        </div>

        <div style='width:150px; float:left; text-align:right; font-size:13px; padding-right:0px;'>
            <div onclick='search_start()' style='width:100px; line-height:1em;' class='btn btn-success'>Найти</div>
        </div>
        <div style='clear:both;'></div>

        </form>
    </div>
    <script>
     function search_start(){
         var city = $('.city_input').val();
         var school = $('.school_input').val();
         var search = '';
         if (city != ''){
            search = search + '&city='+city;
         }
         if (school != ''){
            search = search + '&school='+school;
         }

         if (search != ''){
            $('.s_form').attr('onsubmit','');
            $('.s_form').attr('action','<?=Yii::app()->createUrl('conf/orders');?>'+ search);
            $('.s_form').submit();
         }

         if (city == '' && school == ''){
             document.location = '<?=Yii::app()->createUrl('conf/orders');?>';
         }
     }


     function search_clear(input_class){
         $('.'+input_class).val('');
         search_start();
     }
    </script>
<?php
$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$provider,
'itemView'=>'_order',
'viewData'=>array(

),
'template'=>"\n{items}\n<div style='padding-top:20px;'>{pager}</div>",
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/site_pages.css?v=1', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));
?>
</div>
</div>
</div>
