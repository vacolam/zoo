<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <a href="<?=Yii::app()->createUrl('cats/update',array('id'=>$this->razdel['id']));?>" class='btn btn-outline-success' style=''>
                            <svg class="bi bi-gear" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8.837 1.626c-.246-.835-1.428-.835-1.674 0l-.094.319A1.873 1.873 0 014.377 3.06l-.292-.16c-.764-.415-1.6.42-1.184 1.185l.159.292a1.873 1.873 0 01-1.115 2.692l-.319.094c-.835.246-.835 1.428 0 1.674l.319.094a1.873 1.873 0 011.115 2.693l-.16.291c-.415.764.42 1.6 1.185 1.184l.292-.159a1.873 1.873 0 012.692 1.116l.094.318c.246.835 1.428.835 1.674 0l.094-.319a1.873 1.873 0 012.693-1.115l.291.16c.764.415 1.6-.42 1.184-1.185l-.159-.291a1.873 1.873 0 011.116-2.693l.318-.094c.835-.246.835-1.428 0-1.674l-.319-.094a1.873 1.873 0 01-1.115-2.692l.16-.292c.415-.764-.42-1.6-1.185-1.184l-.291.159A1.873 1.873 0 018.93 1.945l-.094-.319zm-2.633-.283c.527-1.79 3.065-1.79 3.592 0l.094.319a.873.873 0 001.255.52l.292-.16c1.64-.892 3.434.901 2.54 2.541l-.159.292a.873.873 0 00.52 1.255l.319.094c1.79.527 1.79 3.065 0 3.592l-.319.094a.873.873 0 00-.52 1.255l.16.292c.893 1.64-.902 3.434-2.541 2.54l-.292-.159a.873.873 0 00-1.255.52l-.094.319c-.527 1.79-3.065 1.79-3.592 0l-.094-.319a.873.873 0 00-1.255-.52l-.292.16c-1.64.893-3.433-.902-2.54-2.541l.159-.292a.873.873 0 00-.52-1.255l-.319-.094c-1.79-.527-1.79-3.065 0-3.592l.319-.094a.873.873 0 00.52-1.255l-.16-.292c-.892-1.64.902-3.433 2.541-2.54l.292.159a.873.873 0 001.255-.52l.094-.319z" clip-rule="evenodd"/>
                                <path fill-rule="evenodd" d="M8 5.754a2.246 2.246 0 100 4.492 2.246 2.246 0 000-4.492zM4.754 8a3.246 3.246 0 116.492 0 3.246 3.246 0 01-6.492 0z" clip-rule="evenodd"/>
                            </svg>
                        </a>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<div class="form">
    <?php echo CHtml::beginForm(
        array('contacts/save')
    ); ?>

<div style='width:430px; float:left;'>

    <div class="form-group">
        <label for="map" class="control-label"><b>Полное название организации</b></label>
        <input type="text" class="form-control" id="company_name" name="COMPANY[company_name]" value='<?=$company_contacts['company_name'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Краткое название организации</label>
        <input type="text" class="form-control" id="company_shortname" name="COMPANY[company_shortname]" value='<?=$company_contacts['company_shortname'];?>' placeholder=''>
    </div>
    <div class="form-group" style='padding-top:20px;'>
        <label for="map" class="control-label"><b>Индекс организации</b></label>
        <input type="text" class="form-control" id="company_index" name="COMPANY[company_index]" value='<?=$company_contacts['company_index'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Город организации</label>
        <input type="text" class="form-control" id="company_city" name="COMPANY[company_city]" value='<?=$company_contacts['company_city'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Адрес организации</label>
        <input type="text" class="form-control" id="company_adress" name="COMPANY[company_adress]" value='<?=$company_contacts['company_adress'];?>' placeholder=''>
    </div>

    <div class="form-group"  style='padding-top:20px;'>
        <label for="map" class="control-label"><b>Номер Лицензии</b></label>
        <input type="text" class="form-control" id="company_adress" name="COMPANY[company_license_number]" value='<?=$company_contacts['company_license_number'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Ссылка на документ с лицензией</label>
        <input type="text" class="form-control" id="company_adress" name="COMPANY[company_license_link]" value='<?=$company_contacts['company_license_link'];?>' placeholder=''>
    </div>

    <div class="form-group"  style='padding-top:20px;'>
        <label for="map" class="control-label"><b>Телефон организации 1</b></label>
        <input type="text" class="form-control" id="company_phone_1" name="COMPANY[company_phone_1]" value='<?=$company_contacts['company_phone_1'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Телефон организации 2</label>
        <input type="text" class="form-control" id="company_phone_2" name="COMPANY[company_phone_2]" value='<?=$company_contacts['company_phone_2'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Email организации</label>
        <input type="text" class="form-control" id="company_email" name="COMPANY[company_email]" value='<?=$company_contacts['company_email'];?>' placeholder=''>
    </div>


    <div class="form-group"  style='padding-top:20px;'>
        <label for="map" class="control-label"><b>Название учредителя</b></label>
        <input type="text" class="form-control" id="company_founder_name" name="COMPANY[company_founder_name]" value='<?=$company_contacts['company_founder_name'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Главный у учредителя</label>
        <input type="text" class="form-control" id="company_founder_ceo" name="COMPANY[company_founder_ceo]" value='<?=$company_contacts['company_founder_ceo'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Адрес и телефон учредителя</label>
        <input type="text" class="form-control" id="company_founder_adress" name="COMPANY[company_founder_adress]" value='<?=$company_contacts['company_founder_adress'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">E-mail учредителя</label>
        <input type="text" class="form-control" id="company_founder_email" name="COMPANY[company_founder_email]" value='<?=$company_contacts['company_founder_email'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Сайт учредителя</label>
        <input type="text" class="form-control" id="company_founder_site" name="COMPANY[company_founder_site]" value='<?=$company_contacts['company_founder_site'];?>' placeholder=''>
    </div>




    <div class="form-group"  style='padding-top:20px;'>
        <label for="map" class="control-label"><b>Facebook</b></label>
        <input type="text" class="form-control" id="company_social_fb" name="COMPANY[company_social_fb]" value='<?=$company_contacts['company_social_fb'];?>' placeholder=''>
    </div>

        <div class="form-group">
        <label for="map" class="control-label">Вконтакте</label>
        <input type="text" class="form-control" id="company_social_vk" name="COMPANY[company_social_vk]" value='<?=$company_contacts['company_social_vk'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Одноклассники</label>
        <input type="text" class="form-control" id="company_social_ok" name="COMPANY[company_social_ok]" value='<?=$company_contacts['company_social_ok'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Twitter</label>
        <input type="text" class="form-control" id="company_social_tw" name="COMPANY[company_social_tw]" value='<?=$company_contacts['company_social_tw'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Instagram</label>
        <input type="text" class="form-control" id="company_social_inst" name="COMPANY[company_social_inst]" value='<?=$company_contacts['company_social_inst'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Youtube</label>
        <input type="text" class="form-control" id="company_social_youtube" name="COMPANY[company_social_youtube]" value='<?=$company_contacts['company_social_youtube'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Telegram</label>
        <input type="text" class="form-control" id="company_social_telegram" name="COMPANY[company_social_telegram]" value='<?=$company_contacts['company_social_telegram'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Whatsapp</label>
        <input type="text" class="form-control" id="company_social_whatsapp" name="COMPANY[company_social_whatsapp]" value='<?=$company_contacts['company_social_whatsapp'];?>' placeholder=''>
    </div>

    <div class="form-group"  style='padding-top:20px;'>
        <label for="map" class="control-label"><b>Код счетчиков статистики</b></label>
        <textarea class="form-control" id="metrics" name="COMPANY[metrics]"><?=$company_contacts['metrics'];?></textarea>
    </div>

</div>

<div style='width:400px; float:right;'>

    <div class="form-group">
        <label for="map" class="control-label">Company Name</label>
        <input type="text" class="form-control" id="company_eng_name" name="COMPANY_ENG[company_name]" value='<?=$company_contacts_eng['company_name'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Post Index</label>
        <input type="text" class="form-control" id="company_eng_index" name="COMPANY_ENG[company_index]" value='<?=$company_contacts_eng['company_index'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">City</label>
        <input type="text" class="form-control" id="company_eng_city" name="COMPANY_ENG[company_city]" value='<?=$company_contacts_eng['company_city'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Company Adress</label>
        <input type="text" class="form-control" id="company_eng_adress" name="COMPANY_ENG[company_adress]" value='<?=$company_contacts_eng['company_adress'];?>' placeholder=''>
    </div>

</div>
<div style='clear:both;'></div>



<div>



    <div class="form-group">
        <label for="map" class="control-label">Код карты</label>
        <textarea id="map" class="form-control" name="Map"><?= $map->content; ?></textarea>
    </div>

    <?php echo CHtml::errorSummary($contacts, null, null, array(
        'class' => 'bg-danger info',
    )); ?>

    <?
        $this->renderPartial('//modules/_redactor', array(
            'model' => $contacts,
            'text_input_name' => 'content',
        ));
    ?>



     <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>
            <div style='clear:both;'></div>
        </div>
    </div>


    <?php echo CHtml::endForm(); ?>
</div>
</div>  
</div>
