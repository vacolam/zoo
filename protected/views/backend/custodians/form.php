<script src="js/bootstrap-select-1.13.14.min.js"></script>
<link href="js/bootstrap-select-1.13.14.min.css" rel="stylesheet">

<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:60%; height:100px; text-align:left; ' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'><?= $model->isNewRecord ? 'добавление' : 'редактирование'; ?></div>
                        </div>
                    </td>
                    <td style='width:40%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>
<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <?php
        if (!$model->isNewRecord) {
            ?>
            <div style='width:300px; height: 150px; margin-bottom:30px; background:url(<?=$model->getImageUrl();?>) #f0f0f0 center; background-size:cover;'>
            </div>
            <?
        }
    ?>
    
    <div class="form-group<?= $model->hasErrors('image') ? ' has-error' : ''; ?>" style='padding:10px; border:1px solid #ccc; background:#fff; border-radius:3px 3px 0px 0px; margin-bottom:0px; border-bottom:none;'>
        <?= CHtml::activeLabel($model, 'image', array('class' => 'control-label')); ?>
        <?= CHtml::activeFileField($model, 'image'); ?>
        <?= CHtml::error($model, 'image', array('class' => 'help-block')); ?>
    </div>

    <div style='padding:10px; border:1px solid rgba(0,0,0,.1); background:#0099FF; color:#fff; border-radius:0px 0px 3px 3px;  margin-bottom:20px;'><b>Min: ширина 300px / высота 150px</b>. Ширина должна быть в два раза больше высоты.</div>
    
    <div class="form-group<?= $model->hasErrors('link') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'link', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'link', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'link', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('title') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'title', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'title', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'title', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('description') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'description', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'description', array('class' => 'form-control', 'style'=>'height:120px;')); ?>
        <?= CHtml::error($model, 'description', array('class' => 'help-block')); ?>
    </div>

    <?
    $type_array = array();
    $type_array['custodian'] = 'Опекун';
    $type_array['partner'] = 'Партнер';
    ?>
    <div class="form-group<?= $model->hasErrors('type') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'type', array('class' => 'control-label')); ?>
        <?=CHtml::activeDropDownList($model,'type',$type_array, array('class' => 'form-control', 'data-live-search'=>'true', 'style'=>'width:100%; background:#fff;')); ?>
        <?= CHtml::error($model, 'type', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('animals_id') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'animals_id', array('class' => 'control-label')); ?>
        <?=CHtml::activeDropDownList($model,'animals_id',$exposureItemsList, array('class' => 'form-control dropdown', 'data-live-search'=>'true', 'style'=>'width:100%;')); ?>
        <?= CHtml::error($model, 'animals_id', array('class' => 'help-block')); ?>
    </div>



     <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

                    <div style='with:100px; float:right;'>
                <?
                if ($this->route == 'custodians/update'){
                ?>
                <a href='<?= Yii::app()->createUrl('custodians/delete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>
    
<?php echo CHtml::endForm(); ?>
</div>
</div>
<script>


$(function(){
    $('.dropdown').selectpicker();
})

</script>