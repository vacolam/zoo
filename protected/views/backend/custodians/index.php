<style>
.content_one a{
    color:#000;
}

</style>
<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:60%; height:100px; text-align:left; ' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                        </div>
                    </td>
                    <td style='width:40%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить', array('create'), array('class' => 'btn btn-outline-success')); ?>
                        <a href="<?=Yii::app()->createUrl('cats/update',array('id'=>$this->razdel['id']));?>" class='btn btn-outline-success' style=''>
                            <svg class="bi bi-gear" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8.837 1.626c-.246-.835-1.428-.835-1.674 0l-.094.319A1.873 1.873 0 014.377 3.06l-.292-.16c-.764-.415-1.6.42-1.184 1.185l.159.292a1.873 1.873 0 01-1.115 2.692l-.319.094c-.835.246-.835 1.428 0 1.674l.319.094a1.873 1.873 0 011.115 2.693l-.16.291c-.415.764.42 1.6 1.185 1.184l.292-.159a1.873 1.873 0 012.692 1.116l.094.318c.246.835 1.428.835 1.674 0l.094-.319a1.873 1.873 0 012.693-1.115l.291.16c.764.415 1.6-.42 1.184-1.185l-.159-.291a1.873 1.873 0 011.116-2.693l.318-.094c.835-.246.835-1.428 0-1.674l-.319-.094a1.873 1.873 0 01-1.115-2.692l.16-.292c.415-.764-.42-1.6-1.185-1.184l-.291.159A1.873 1.873 0 018.93 1.945l-.094-.319zm-2.633-.283c.527-1.79 3.065-1.79 3.592 0l.094.319a.873.873 0 001.255.52l.292-.16c1.64-.892 3.434.901 2.54 2.541l-.159.292a.873.873 0 00.52 1.255l.319.094c1.79.527 1.79 3.065 0 3.592l-.319.094a.873.873 0 00-.52 1.255l.16.292c.893 1.64-.902 3.434-2.541 2.54l-.292-.159a.873.873 0 00-1.255.52l-.094.319c-.527 1.79-3.065 1.79-3.592 0l-.094-.319a.873.873 0 00-1.255-.52l-.292.16c-1.64.893-3.433-.902-2.54-2.541l.159-.292a.873.873 0 00-.52-1.255l-.319-.094c-1.79-.527-1.79-3.065 0-3.592l.319-.094a.873.873 0 00.52-1.255l-.16-.292c-.892-1.64.902-3.433 2.541-2.54l.292.159a.873.873 0 001.255-.52l.094-.319z" clip-rule="evenodd"/>
                                <path fill-rule="evenodd" d="M8 5.754a2.246 2.246 0 100 4.492 2.246 2.246 0 000-4.492zM4.754 8a3.246 3.246 0 116.492 0 3.246 3.246 0 01-6.492 0z" clip-rule="evenodd"/>
                            </svg>
                        </a>
                    </td>
            </tr>
            </table>
        </div>
    </div>



<?
        $this->renderPartial('form_pages', array(
            'model'             => $model,
            'docs'              => $docs,
            'photos'            => $photos,
        ));
?>

<div class='content_one' style='padding-top:40px;'>
<table style='width:100%; padding-bottom:20px;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:26px;'><b>Опекуны</b></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
<?php
foreach ($custodians as $custodian){
    ?>
    <div style='padding-bottom:20px; margin-bottom:20px; border-bottom:1px solid #ccc; color:#000;'>
                    <a href="<?= Yii::app()->createUrl('custodians/update',array('id'=>$custodian->id)); ?>" style='display:block;'>
                        <div style='width:200px; height:100px; float:left; background: url(<?= $custodian->getImageUrl(); ?>) center #F8F8F8; background-size:cover; border-radius:2px;' alt="" class="slide-img">
                        </div>
                        <div style='width: 400px; float:left; padding-left:40px; '>
                            <div style='font-size:16px;' class='open-s'><b><?=$custodian->title;?></b></div>
                            <div style='font-size:14px; line-height:1.3em; padding-top:4px;' class='open-s'><i><?=$custodian->description;?></i></div>
                                     <div style='padding-top:0px; font-size:14px; color:#53952A; margin-top:15px;'>
                                        <?
                                         if ($exposureItemsList){
                                                    if (isset($exposureItemsList[$custodian->animals_id])){
                                                    echo "<span><b>".$exposureItemsList[$custodian->animals_id]."</b> | </span>";
                                                    }
                                            }
                                        ?>
                                    </div>
                        </div>
                        <?
                        if ($custodian->animal){
                        ?>
                        <a href="<?= $custodian->animal->getImageUrl(); ?>" class='fancybox-media' rel='animals' style='width:100px; height:100px; display:block; box-shadow:0px 0px 5px rgba(0,0,0,.1); float:right; background: url(<?= $custodian->animal->getThumbnailUrl(); ?>) center #53952A; background-size:cover; border-radius:2px;'>
                        </a>
                        <?
                        }
                        ?>
                        <div style='clear:both;'></div>
                    </a>

    </div>
    <?
}
?>
</div>
</div>