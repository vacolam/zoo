<?
if (isset($data->shopOrder))
{
$order = $data->shopOrder;
$isNew = '';
$link = Yii::app()->createUrl('shop/order',array('id'=>$order->id));
if (in_array($data->type.'_'.$data->order_id,$notifications))
{
    $isNew = 'isNew';
    $link = Yii::app()->createUrl('dashboard/redirectnotify',array('type'=>$data->type, 'order_id'=>$data->order_id, 'redirect'=>$link ));
}
?>

<div style='padding-bottom:10px; padding-top:10px; border-bottom:1px solid #ccc; color:#000; font-size:14px; display:block; position:relative;' class='<?=$isNew;?> order' rel='<?=$data->type.'_'.$data->order_id;?>'>

<?
if ($isNew == 'isNew'){
    ?>
    <div style='position:absolute; width:16px; height:16px; background:#28A745; border-radius:50%; left:-32px; top:50%; margin-top:-10px; cursor:pointer;' title='Просмотрено' onclick='is_viewed("<?=$data->type;?>",<?=$data->order_id;?>)' class='not_dot'></div>
    <?
}
?>

<a href='<?= $link; ?>' target='_blank' style='display:block; color:#000;'>
        <div style=' float:left; width:50px; padding-left:10px; font-size:13px;' class='zaya'>
&nbsp;
        </div>
        <div style=' float:left; width:100px; padding-left:10px;' class='zaya'><b><?=Dates::getName($order->date);?></b>&nbsp;<br></div>
        <div style='width:150px;  float:left;' class='zaya'><div class='btn btn-success' style='font-size:12px; line-height:1em;'>Магазин</div></div>
        <div style=' float:left; width:250px;' class='zaya'>
        <div style='font-size:13px'>
            <div><b>Заказ № <?=$order->id;?></b></div>
            <div>
            <span style='font-size:14px;' >
                                   <?
                                   $delivery_type = array(
                                    1 => 'Самовывоз из Зоопарка',
                                    2 => 'Доставка курьером',
                                   );

                                   if (isset($delivery_type[$order->deliverytype]))
                                   {
                                       echo $delivery_type[$order->deliverytype];
                                   }
                                   ?>
                                   </span>
                                   <?
                                   if ($order->deliverytype == 2){
                                   ?>
                                   <span style='padding-left:10px; font-size:14px; color:#404040;'><i><?=$order->adress;?></i></span>
                                   <?
                                   }
                                   ?>

            </div>
            <div>
            <?
                $summ = 0;
                $count = 0;
                $orderItems = $order->orderitems;
                if ($orderItems)
                {
                    foreach($orderItems as $item)
                    {
                        $summ = $summ + ($item->price*$item->quontity);
                        $count = $count + $item->quontity;
                    }
                }
                echo $count." шт; ".$summ." руб;";
                ?>
            </div>
        </div>
        </div>

        <div style='width:250px;  float:left;' class='zaya'>
            <div><?=$order->name;?></div>
            <div>Телефон: <?=$order->phone;?></div>
        </div>
        <div style='width:90px;  text-align:right;  float:right; padding-right:10px;' class='zaya'>
<?
   $status_text = array(
        0 => 'Принят',
        1 => 'Готов к выдаче',
        2 => 'Частично готов к выдаче',
        1000 => 'Оплачен и выдан',
        10000 => 'Отменен продавцом',
        10001 => 'Отменен покупателем',
    );
    $status = array
    (
        0 => 'bg-warning',
        1 => 'bg-primary',
        2 => 'bg-primary',
        1000 => 'bg-success',
        10000 => 'bg-danger',
        10001 => 'bg-danger',
    );
    $status_title = '';
    if (isset($status_text[$order->status]))
    {
        $status_title = $status_text[$order->status];
    }
    $status_class = 'bg-primary';
    if (isset($status[$order->status]))
    {
        $status_class = $status[$order->status];
    }


    echo "<span class='".$status_class."' style='color:#fff; padding:1px 6px; border-radius:3px;'>".$status_title."</span>";
?>
        </div>
        <div style='clear:both;'></div>
</a>
</div>
<?
}
?>

