<?
if (isset($data->yarmarka))
{
$yarmarka = $data->yarmarka;

$isNew = '';
$link = Yii::app()->createUrl('yarmarka/view',array('id'=>$data->order_id));
if (in_array($data->type.'_'.$data->order_id,$notifications))
{
    $isNew = 'isNew';
    $link = Yii::app()->createUrl('dashboard/redirectnotify',array('type'=>$data->type, 'order_id'=>$data->order_id, 'redirect'=>$link ));
}
?>

<div style='padding-bottom:10px; padding-top:10px; border-bottom:1px solid #ccc; color:#000; font-size:14px; display:block; position:relative;' class='<?=$isNew;?> order' rel='<?=$data->type.'_'.$data->order_id;?>'>

<?
if ($isNew == 'isNew'){
    ?>
    <div style='position:absolute; width:16px; height:16px; background:#28A745; border-radius:50%; left:-32px; top:50%; margin-top:-10px; cursor:pointer;' title='Просмотрено' onclick='is_viewed("<?=$data->type;?>",<?=$data->order_id;?>)' class='not_dot'></div>
    <?
}
?>

<a href='<?= $link; ?>' target='_blank' style='display:block; color:#000;'>
        <div style=' float:left; width:50px; padding-left:10px; font-size:13px;' class='zaya'>
            <b><?=$yarmarka->id;?></b>
        </div>
        <div style=' float:left; width:100px; padding-left:10px; font-size:13px;' class='zaya'>
            <b><?=Dates::getName($yarmarka->date);?></b>&nbsp;<br>
            <?
            if ($yarmarka->time !='00:00:00')
            {
                ?>
                <div style='font-size:12px; color:#707070'>время <?=$yarmarka->time;?></div>
                <?
            }
            ?>
        </div>
        <div style='width:150px;  float:left;' class='zaya'><div class='btn btn-success' style='font-size:12px; line-height:1em;'>Ярмарка</div></div>
        <div style=' float:left; width:250px;' class='zaya'>
        <div style='font-size:13px'>
            <div><b>Заявка на ярмарку № <?=$yarmarka->id;?></b></div>
            <div>Место № <?=$yarmarka->mesto;?></div>
        </div>
        </div>

        <div style='width:280px;  float:left;' class='zaya'>
            <div><?=$yarmarka->name;?></div>
            <div>Телефон: <?=$yarmarka->phone;?></div>
        </div>
        <div style='width:100px;  text-align:right;  float:right; padding-right:10px;' class='zaya'>
            <?
            $active = 'secondary';
            $name = 'Статус';
            if($yarmarka->status == 0){$name = 'Без статуса'; $active = 'outline-secondary';}
            if($yarmarka->status == 1){$name = 'Согласовано'; $active='success';}
            if($yarmarka->status == 2){$name = 'Отказано'; }
            ?>
            <div class='btn btn-<?=$active;?>' style='line-height:1em; font-size:11px;'><?=$name;?></div>
        </div>
        <div style='clear:both;'></div>
</a>
</div>
<?
}
?>

