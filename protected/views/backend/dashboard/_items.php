<?
if ($data->type == 'yarmarka'){
        $this->renderPartial('_item_yarmarka', array(
            'data' => $data,
            'notifications'=>$notifications
        ));
}

if ($data->type == 'groups'){
        $this->renderPartial('_item_groups', array(
            'data'          =>  $data,
            'notifications' =>  $notifications
        ));
}

if ($data->type == 'conf'){
        $this->renderPartial('_item_conf', array(
            'data' => $data,
            'notifications'=>$notifications
        ));
}

if ($data->type == 'festival'){
        $this->renderPartial('_item_festival', array(
            'data' => $data,
            'notifications'=>$notifications
        ));
}

if ($data->type == 'konkurs'){
        $this->renderPartial('_item_konkurs', array(
            'data' => $data,
            'notifications'=>$notifications
        ));
}

if ($data->type == 'opros'){
        $this->renderPartial('_item_opros', array(
            'data' => $data,
            'notifications'=>$notifications
        ));
}

if ($data->type == 'ocenka'){
        $this->renderPartial('_item_ocenka', array(
            'data' => $data,
            'notifications'=>$notifications
        ));
}

if ($data->type == 'shopOrder'){
        $this->renderPartial('_item_shop', array(
            'data' => $data,
            'notifications'=>$notifications
        ));
}
?>
