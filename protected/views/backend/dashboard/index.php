<script type="text/javascript" src="//www.gstatic.com/firebasejs/3.6.8/firebase.js"></script>
<script type="text/javascript" src="/firebase_subscribe.js?v=14"></script>

<style>
#subscribe .sbscrb{ width:30px; height:30px; fill:currentColor;}
#subscribe .sbscrb:hover{ width:30px; height:30px; fill:#28A745;}
#subscribe.red .sbscrb{fill:#DC3545;}
#subscribe.red .sbscrb:hover{fill:#D02536;}
#subscribe .subscribed{ width:30px; height:30px;  fill:#28A745; display:none;}
#subscribe .subscribed:hover{ width:30px; height:30px;  fill:#269C41;}
</style>

<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                       <div class='title_three' style='font-size:30px;'><b>Заявки, Анкеты, Работы</b></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <span id="subscribe"   class='' style='cursor:pointer;'>
                            <svg viewBox="0 0 16 16" class="bi bi-bell sbscrb" fill="" xmlns="http://www.w3.org/2000/svg" title='Включить уведомления'>
                                <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2z"/>
                                <path fill-rule="evenodd" d="M8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z"/>
                            </svg>

                            <svg viewBox="0 0 16 16" class="bi bi-bell-fill subscribed" fill="currentColor" xmlns="http://www.w3.org/2000/svg" title='Уведомления включены'>
    <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z"/>
</svg>
                        </span>
                    </td>
            </tr>
            </table>
        </div>
    </div>




    <div class='content_one'>
    <div class="form">

    <div style='padding-top:0px;'>

<?
$plugins = User::getAllowedPlugins();



$class = 'btn-outline-success';
if ($type == 'none'){$class = 'btn-success';}
?>
<a href="<?=Yii::app()->createUrl('dashboard/index',array());?>" class='btn <?=$class;?>'  style='font-size:13px;'>Все</a>

<?
//checkAllowedPlugins

$plugins_array = array(
'yarmarka'=>'Ярмарки',
'groups'=>'Группы',
'conf'=>'Конференции',
'festival'=>'Фестивали',
'konkurs'=>'Конкурсы',
'opros'=>'Опросы',
'ocenka'=>'Оценка качества услуг',
'shopOrder'=>'Магазин',
);

foreach($plugins_array as $index => $plugin_title)
{
    if (User::checkAllowedPlugins($plugins,$index) == false){continue;}
$class = 'btn-outline-success';
if ($type == $index){$class = 'btn-success';}
?>
<a href="<?=Yii::app()->createUrl('dashboard/index',array('type'=>$index));?>" class='btn <?=$class;?>'  style='font-size:13px;'><?=$plugin_title;?></a>
<?

}


?>


<div style='border-bottom:2px solid #000; padding-bottom:20px; margin-bottom:20px; margin-top:60px;'>
        <div style='width:50px;  float:left;' class='zaya'><b>№</b></div>
        <div style='width:100px;  float:left;' class='zaya'><b>Дата</b></div>
        <div style='width:150px;  float:left; ' class='zaya'><b>Куда</b></div>
        <div style='width:250px;  float:left;' class='zaya'><b>Что</b></div>
        <div style='width:280px;   float:left;' class='zaya'><b>Кто</b></div>
        <div style='width:100px;  text-align:right;  float:right; padding-right:0px;' class='zaya'><b>Статус</b></div>
        <div style='clear:both;'></div>
</div>
<style>
.isNew{background:#fff;}
</style>

    <?php
    $this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_items',
    'viewData'=>array(
        'notifications'=>$notifications
    ),
    'template'=>"\n{items}\n<div style='padding-top:20px;'>{pager}</div>",
    'ajaxUpdate'=>false,
        'pager'=>array(
            'nextPageLabel' => '<span>&raquo;</span>',
            'prevPageLabel' => '<span>&laquo;</span>',
            'lastPageLabel' => false,
            'firstPageLabel' => false,
            'class'=>'CLinkPager',
            'header'=>false,
            'cssFile'=>'/css_tool/site_pages.css?v=1', // устанавливаем свой .css файл
            'htmlOptions'=>array('class'=>'pager'),
        ),
    ));
    ?>
    </div>


    </div>
    </div>
</div>
<script>
function is_viewed(type,order_id){
    var order = $('.order[rel="'+type+'_'+order_id+'"]');
    order.find('.not_dot').css('display','none');
    order.removeClass('isNew');

    $.ajax({
        type: "GET",
        url: '<?= Yii::app()->createUrl('dashboard/deletenotify', array()); ?>',
        dataType: 'json',
        data: {
            'type':type,
            'order_id':order_id,
        },
        success: function(response){
                    if (response){
                    }
        },
        error: function(response){
        }
    });
}
</script>