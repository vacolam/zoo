<div id='module_docs_<?=$model->id;?>'>
<table style='width:100%;'>
<?
$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$docs,
'itemView'=>'_f_documents_item',
'template'=>"\n{items}\n{pager}",
'emptyText'=>'',
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/pages.css', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));
?>

<tr>
    <td colspan="3" style='padding-bottom:70px; padding-top:20px; border-top:1px dotted #ccc;'>
        <div class='delete_docs_button' style="display:none;background:#C9302C; width:100%; cursor:pointer; margin-bottom:5px;"  onclick="delete_checked_docs(<?=$model->id;?>)"><div style="padding:20px; text-align:center; font-size:14px; color:#fff;">Удалить файлы</div></div>
        <div style='background:#fff; box-shadow:0px 0px 2px rgba(0,0,0,.1); cursor:pointer; width:100%;' onclick="add_photos_and_files('docs',<?=$model->id;?>)"><div style="padding:20px; text-align:center; font-size:14px;">Добавить Файлы</div></div>
    </td>
</tr>
</table>
</div>