<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'>ENG</a></div>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'><?= $model->isNewRecord ? 'добавление' : $this->crop_str_word( $model->name, 6 ); ?></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<?php
/* @var Page $model */
?>

<div class="form" style=''>
<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <div style='width:100%; float:left;'>

    <div class="form-group<?= $model->hasErrors('name') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'name', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'name', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'name', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('number') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'number', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'number', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'number', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group">
    <label for='checkbox'>
        <?
        $checked = '';
        if ($model->show_raspisanie == 1){$checked = 'checked="checked"';}
        ?>
        <input type="checkbox" name='Eng[show_raspisanie]' value='1' <?=$checked;?> id='checkbox'/> Отображать расписание на этой странице
    </label>
    </div>

    </div>


    <div style='width:250px; float:right; display:none;'>
    <?
    $bg = "background:#f0f0f0;";
    if (!$model->isNewRecord && $model->hasImage()) {
        $getThumbnailUrl = $model->getThumbnailUrl();
        $bg = "background:url(".$getThumbnailUrl.") #f0f0f0 center; background-size:cover;";
    }
    ?>
    <div style='width:250px; height: 200px; position:relative; <?=$bg;?>'>
        <?php
        if (!$model->isNewRecord && $model->filename != '')
        {
            ?>
            <div style='width:250px; height:40px; line-height:40px; background:rgba(255,255,255,0.8); overflow:hidden; position:absolute; top:0px; left:0px;'>
                <div style='padding:0px 20px;'>
                <input id="remove-image" name="Eng[removeImage]" type="checkbox"><label for="remove-image">&nbsp;&nbsp;Удалить обложку</label>
                </div>
            </div>
            <?php
        }
        ?>
    </div>

    <div style='width:250px; background:#f0f0f0; margin-top:15px; overflow:hidden;'>
        <div style='padding:10px;'>
        <div class="form-group<?= $model->hasErrors('image') ? ' has-error' : ''; ?>">
            <?= CHtml::activeLabel($model, 'image', array('class' => 'control-label')); ?>
            <?= CHtml::activeFileField($model, 'image'); ?>
            <?= CHtml::error($model, 'image', array('class' => 'help-block')); ?>
        </div>
        </div>
    </div>

    </div>

    <div style='clear:both;'></div>
    <div style=' margin-top:20px; margin-bottom:20px; border-top:1px dotted #ccc;'></div>



    <?
        $this->renderPartial('//modules/_redactor', array(
            'model' => $model,
            'text_input_name' => 'text',
        ));
    ?>



 <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

                    <div style='with:100px; float:right;'>
                <?
                if ($this->route == Yii::app()->controller->id.'/update'){
                ?>
                <a href='<?= Yii::app()->createUrl(Yii::app()->controller->id.'/delete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>
</div>



<?
if ($this->route == 'eng/update')
{
?>
<div style='width:100%; border:1px solid #ccc; border-radius:3px; margin-top:50px; background:#fff;'>
<table style='width:100%;'>
    <tr>
        <td style='width:60%; text-align:left; vertical-align:top; border-right:1px solid #ccc;'>
        <div style='padding:10px;'>
        <?
       $this->renderPartial('//modules/_f_photos', array(
            'photos' => $photos,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('eng/deletephotos', array()),
            'link_setcover'=>Yii::app()->createUrl('eng/setcover', array()),
        ));
        ?>
        </div>
        </td>
        <td style='text-align:left; vertical-align:top;'>
        <div style='padding:10px;'>
        <?
        $this->renderPartial('//modules/_f_docs', array(
            'docs' => $docs,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('eng/deletedocs', array()),
        ));
        ?>
        </div>
        </td>
    </tr>
</table>
</div>
<?
$this->renderPartial('//modules/_f_upload', array(
    'model' => $model,
    'link_add' => Yii::app()->createUrl('eng/filesadd', array('cats_id'=>$model->id)),
));
?>

<?
}
?>

</div>
</div>
