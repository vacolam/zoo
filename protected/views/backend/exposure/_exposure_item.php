
    <?
        $bg =  "background:none; ";
        if ($data->hasImage()) {
            $bg_url = $data->getThumbnailUrl();
            $bg =  'background:url("'.$bg_url.'") #f0f0f0 center; background-size:cover;';
        }
    ?>
<a href="<?=Yii::app()->createUrl('exposure/update',array('id' => $data->id));?>" style='display:block; width:140px; margin-right:5px; height:210px; border-radius:4px; float:left;   <?=$bg;?>'>
    <div style='position:relative; width:100%; height:100%; background-image: linear-gradient(-180deg, rgba(0,0,0,0) 38%, rgba(0,0,0,0.65) 79%); position:relative; border-radius:4px;'>
        <div style='width:100%; position:absolute; left:0px; bottom:10px;'>
            <div style='padding:20px; text-align:center;'>
                <div style='font-size:14px; color:#fff; text-transform:uppercase; font-weight:500; word-wrap:break-word;' class='open-s'><?=$data->title;?></div>
            </div>
        </div>
    </div>
</a>
