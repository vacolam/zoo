<div style='border:1px solid #ccc; background:#FCFCFC; border-radius:5px; margin-bottom:0px;'>
    <div style='display:block; padding:20px;'>

        <div style='padding-bottom:10px; font-size:16px;'><a href="<?=Yii::app()->createUrl('exposure/list');?>" style='color:#000;'><b>Список экспозиций</b></a></div>

            <?php
            $expo = Exposure::getAll();
            $t = 0;
            if ($expo) {
                foreach ($expo as $item) {
                    $t++;
                    if ($t > 5){continue;}
                    $this->renderPartial('_exposure_item', array(
                                'data'=>$item,
                            ));
            }
            }
            ?>
            <a href="<?=Yii::app()->createUrl('exposure/list');?>" style='display:block; width:120px; height:210px; border-radius:4px; float:left; background:#F3F3F3;  border-radius:4px;' title='Все экспозиции'>
                <div style='width:120px; height:210px;  border-radius:4px; text-align:center; line-height:210px; font-size:40px;'>&bull;&bull;&bull;</div>
            </a>

            <div style='clear:both;'></div>
    </div>
</div>

