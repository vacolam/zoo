<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <ul style='list-style:none; padding:0px; margin:0px;'>
                            <li style='float:right; '>
                                <a href="<?=Yii::app()->createUrl('cats/update',array('id'=>$this->razdel['id']));?>" class='btn btn-outline-success' style=''>
                                    <svg class="bi bi-gear" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M8.837 1.626c-.246-.835-1.428-.835-1.674 0l-.094.319A1.873 1.873 0 014.377 3.06l-.292-.16c-.764-.415-1.6.42-1.184 1.185l.159.292a1.873 1.873 0 01-1.115 2.692l-.319.094c-.835.246-.835 1.428 0 1.674l.319.094a1.873 1.873 0 011.115 2.693l-.16.291c-.415.764.42 1.6 1.185 1.184l.292-.159a1.873 1.873 0 012.692 1.116l.094.318c.246.835 1.428.835 1.674 0l.094-.319a1.873 1.873 0 012.693-1.115l.291.16c.764.415 1.6-.42 1.184-1.185l-.159-.291a1.873 1.873 0 011.116-2.693l.318-.094c.835-.246.835-1.428 0-1.674l-.319-.094a1.873 1.873 0 01-1.115-2.692l.16-.292c.415-.764-.42-1.6-1.185-1.184l-.291.159A1.873 1.873 0 018.93 1.945l-.094-.319zm-2.633-.283c.527-1.79 3.065-1.79 3.592 0l.094.319a.873.873 0 001.255.52l.292-.16c1.64-.892 3.434.901 2.54 2.541l-.159.292a.873.873 0 00.52 1.255l.319.094c1.79.527 1.79 3.065 0 3.592l-.319.094a.873.873 0 00-.52 1.255l.16.292c.893 1.64-.902 3.434-2.541 2.54l-.292-.159a.873.873 0 00-1.255.52l-.094.319c-.527 1.79-3.065 1.79-3.592 0l-.094-.319a.873.873 0 00-1.255-.52l-.292.16c-1.64.893-3.433-.902-2.54-2.541l.159-.292a.873.873 0 00-.52-1.255l-.319-.094c-1.79-.527-1.79-3.065 0-3.592l.319-.094a.873.873 0 00.52-1.255l-.16-.292c-.892-1.64.902-3.433 2.541-2.54l.292.159a.873.873 0 001.255-.52l.094-.319z" clip-rule="evenodd"/>
                                        <path fill-rule="evenodd" d="M8 5.754a2.246 2.246 0 100 4.492 2.246 2.246 0 000-4.492zM4.754 8a3.246 3.246 0 116.492 0 3.246 3.246 0 01-6.492 0z" clip-rule="evenodd"/>
                                    </svg>
                                </a>
                            </li>

                            <li style='float:right; margin-right:10px;'>
                            <?= CHtml::link('+ экспозицию', array('create'), array('class' => 'btn btn-outline-success')); ?>
                            </li>

                            <li style='float:right; margin-right:10px;'>
                            <a href="<?= Yii::app()->createUrl('exposureitem/create'); ?>" class="btn btn-outline-success">+ животное</a>
                            </li>
                            <li style='clear:both;'></li>
                        </ul>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>



        <?
        $this->renderPartial('_nav_bar', array(
        ));
        ?>

<ul style='list-style:none; padding:0px; margin:0px; margin-top:30px;'>
<li style='float:left; margin-right:20px; margin-bottom:10px;'>
<div class="dropdown">
    <?
    $name = 'Фильтр по Категориям';

    if ($cats_id > 0){
        if (isset($this->cats_list[$cats_id])){
            $name =  $this->cats_list[$cats_id];
        }
    }
    ?>
    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?=$name;?>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

        <a class="dropdown-item" href="<?= Yii::app()->createUrl('exposure/index',array()); ?>">Все категории</a>
        <div class="dropdown-divider"></div>

        <?
        foreach ($this->cats_list as $index => $value)
        {
            $color='';
            if ($cats_id == $index){
                $color = 'color:#ff0000;';
            }
            ?>
            <a href="<?= Yii::app()->createUrl('exposure/index',array('cats_id'=>$index)); ?>" class="dropdown-item" style='<?=$color;?>'><?=$value;?></a>
            <?
        }
        ?>
    </div>
</div>
</li>

<li style='float:left; margin-right:20px; margin-bottom:10px;'>
<div class="dropdown">
    <?
    $name = 'Фильтр по Экспозициям';

    if ($exposure_id > 0){
        if (isset($exposurelist[$exposure_id])){
            $name =  $exposurelist[$exposure_id];
        }
    }
    ?>
    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?=$name;?>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" href="<?= Yii::app()->createUrl('exposure/index',array()); ?>">Все экспозиции</a>
        <div class="dropdown-divider"></div>
        <?
        foreach ($exposurelist as $index => $value)
        {
            $color='';
            if ($exposure_id == $index){
                $color = 'color:#ff0000;';
            }
            ?>
            <a href="<?= Yii::app()->createUrl('exposure/index',array('exposure_id'=>$index)); ?>" class="dropdown-item" style='<?=$color;?>'><?=$value;?></a>
            <?
        }
        ?>
    </div>
</div>
</li>



<li style='clear:both;'></li>
</ul>



<style>
a {
    color:#222;
}
</style>

<div style='padding-top:0px;'>

<?
if ($current_exposure){
            ?>
            <div style='padding-top:20px;'>
            <?
            $this->renderPartial('_exposure_preview', array(
            'data' => $current_exposure,
            ));
            ?>
            </div>
            <?
}
/*
if (!$current_exposure and $cats_id == 0 and $exposure_id == 0){
?>
            <div style='padding-top:20px;'>
            <?
            $this->renderPartial('_exposure_list', array(
            ));
            ?>
            </div>

<?
}
*/
?>
<div style='width:100%; padding-top:50px;'>
<?
$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$provider,
'itemView'=>'_item',
'viewData'=>array(
    'exposurelist' => $exposurelist,
),
'template'=>"\n{items}\n{pager}",
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/site_pages.css', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));

?>
</div>
 </div>
 </div>
 </div>
