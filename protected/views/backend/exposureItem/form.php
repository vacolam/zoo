<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:60%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl('exposure/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a></div>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'>
                            <?
                            if ($model->isNewRecord){
                                echo 'Животное - Добавление';
                            }else{
                                if ($model->exposure)
                                {
                                ?>
                                <a href="<?=Yii::app()->createUrl('exposure/index',array('exposure_id'=>$model->exposure_id));?>"><? echo $model->exposure->title;?></a> -  <? echo $this->crop_str_word( $model->title, 3 ); ?>
                                <?
                                }
                            }
                            ?>
                        </div>
                    </td>
                    <td style='width:40%; height:100px; text-align:right;' valign=middle>
                        <ul style='list-style:none; padding:0px; margin:0px;'>
                            <li style='float:right; margin-right:0px;'>
                            <?= CHtml::link('+ Экспозицию', array('create'), array('class' => 'btn btn-outline-success')); ?>
                            </li>

                            <li style='float:right; margin-right:20px;'>
                            <a href="<?= Yii::app()->createUrl('exposureitem/create'); ?>" class="btn btn-outline-success">+ Животное</a>
                            </li>
                            <li style='clear:both;'></li>
                        </ul>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>


<div class="form" style='padding-top:0px;'>
<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <div style='width:550px; float:left; margin-top:-5px;'>

    <div class="form-group<?= $model->hasErrors('title') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'title', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'title', array('class' => 'form-control','style'=>'height:100px; width:100%;')); ?>
        <?= CHtml::error($model, 'title', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('cats_id') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'cats_id', array('class' => 'control-label')); ?>
        <?=CHtml::activeDropDownList($model,'cats_id',$this->cats_list, array('class' => 'form-control','style'=>'width:100%;')); ?>
        <?= CHtml::error($model, 'cats_id', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('exposure_id') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'exposure_id', array('class' => 'control-label')); ?>
        <?=CHtml::activeDropDownList($model,'exposure_id',$exposurelist, array('class' => 'form-control','style'=>'width:100%;')); ?>
        <?= CHtml::error($model, 'exposure_id', array('class' => 'help-block')); ?>
    </div>
    </div>


    <div style='width:200px; float:right; '>
    <?
    $bg = "background:#f0f0f0;";

    if (!$model->isNewRecord && $model->hasImage()) {
        $getThumbnailUrl = $model->getThumbnailUrl();
        $bg = "background:url(".$getThumbnailUrl.") #f0f0f0 center; background-size:cover;";
    }

    ?>
    <div style='width:200px; height: 180px; margin-top:20px; border-radius:3px; position:relative; <?=$bg;?>' class='cover_background'>
    </div>

    </div>

    <div style='clear:both;'></div>


        <?
        $this->renderPartial('//modules/_redactor', array(
            'model' => $model,
            'text_input_name' => 'text',
        ));
        ?>

    <div class="form-group<?= $model->hasErrors('short_text') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'short_text', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'short_text', array('class' => 'form-control', 'style'=>'height:120px;')); ?>
        <?= CHtml::error($model, 'short_text', array('class' => 'help-block')); ?>
    </div>


    <div style='width:350px; float:left;'>
        <?
        $this->renderPartial('//modules/_tags', array(
            'model' => $model,
            'tags' => $tags,
            'checked_tags' => $checked_tags,
            'link_add' => Yii::app()->createUrl('exposureitem/addtag'),
            'link_delete' => Yii::app()->createUrl('exposureitem/deletetag'),
        ));
        ?>
    </div>
    <div style='width:300px; float:left;'>
            <?= CHtml::activeCheckBox($model,'opeka'); ?>&nbsp;&nbsp;<label for='ExposureItems_opeka'>Опека</label>
            <div style='clear:both;'></div>
            <div style='padding-top:5px; margin-bottom:5px; border-top:1px dotted #ccc;'></div>
            <?= CHtml::activeCheckBox($model,'redbook'); ?>&nbsp;&nbsp;<label for='ExposureItems_redbook'>Красная книга</label>
            <div style='clear:both;'></div>
    </div>
    <div style='clear:both;'></div>

    <div style=' margin-top:20px; margin-bottom:20px; border-top:1px dotted #ccc;'></div>


 <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

                    <div style='with:100px; float:right;'>
                <?
                if ($this->route == Yii::app()->controller->id.'/update'){
                ?>
                <a href='<?= Yii::app()->createUrl(Yii::app()->controller->id.'/copy',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-warning' >Копия</a>
                <a href='<?= Yii::app()->createUrl(Yii::app()->controller->id.'/delete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>
</div>



<?
if ($this->route == 'exposureitem/update')
{
?>
<div style='width:100%; border:1px solid #ccc; border-radius:3px; margin-top:50px; background:#fff;'>
<table style='width:100%;'>
    <tr>
        <td style='width:60%; text-align:left; vertical-align:top; border-right:1px solid #ccc;'>
        <div style='padding:10px;'>
        <?

        $this->renderPartial('//modules/_f_photos', array(
            'photos' => $photos,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('exposureitem/deletephotos', array()),
            'link_setcover'=>Yii::app()->createUrl('exposureitem/setcover', array()),
        ));

        ?>
        </div>
        </td>
        <td style='text-align:left; vertical-align:top;'>
        <div style='padding:10px;'>
        <?

        $this->renderPartial('//modules/_f_docs', array(
            'docs' => $docs,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('exposureitem/deletedocs', array()),
        ));

        ?>
        </div>
        </td>
    </tr>
</table>
</div>
<?

$this->renderPartial('//modules/_f_upload', array(
    'model' => $model,
    'link_add' => Yii::app()->createUrl('exposureitem/filesadd', array('post_id'=>$model->id)),
));

?>
<?
}
?>
</div>
</div>

<script src="/js/croppic/croppic.js"></script>
<link href="/js/croppic/croppic.css" rel="stylesheet">
