<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:70%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a></div>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'><?= $model->isNewRecord ? 'добавление' : $this->crop_str_word( $model->title, 6 ); ?></div>
                    </td>
                    <td style='width:30%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one form'>

<script src="js/bootstrap-select-1.13.14.min.js"></script>
<link href="js/bootstrap-select-1.13.14.min.css" rel="stylesheet">


<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
    'class' => 'form_news',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <div style='width:550px; float:left; margin-top:-5px;'>

    <div class="form-group<?= $model->hasErrors('title') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'title', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'title', array('class' => 'form-control','style'=>'height:100px; width:100%;')); ?>
        <?= CHtml::error($model, 'title', array('class' => 'help-block')); ?>
    </div>

    <div style='display:none;' class="form-group<?= $model->hasErrors('date') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'date', array('class' => 'control-label')); ?>
        <?php
		$date = new DateTime($model->date);
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'News[date]',
            'htmlOptions' => array(
                'class' => 'form-control',
            ),
            'language' => 'ru',
            'value' => $date->format('d.m.Y'),
        ));
        ?>
        <style>
            .ui-datepicker {
                z-index: 10 !important;
            }
        </style>
        <?= CHtml::error($model, 'date', array('class' => 'help-block')); ?>
    </div>

    </div>


    <div style='width:200px; float:right; '>
    <?
    $bg = "background:#f0f0f0;";

    if (!$model->isNewRecord && $model->hasImage()) {
        $getThumbnailUrl = $model->getThumbnailUrl();
        $bg = "background:url(".$getThumbnailUrl.") #f0f0f0 center; background-size:cover;";
    }

    ?>
    <div style='width:200px; height: 180px; margin-top:20px; border-radius:3px; position:relative; <?=$bg;?>' class='cover_background'>
    </div>

    </div>

    <div style='clear:both;'></div>

        <?
        $this->renderPartial('//modules/_redactor', array(
            'model' => $model,
            'text_input_name' => 'text',
        ));
        ?>

    <div class="form-group<?= $model->hasErrors('short_text') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'short_text', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'short_text', array('class' => 'form-control', 'style'=>'height:120px;')); ?>
        <?= CHtml::error($model, 'short_text', array('class' => 'help-block')); ?>
    </div>


    <div style=' margin-top:20px; margin-bottom:20px; border-top:1px dotted #ccc;'></div>


 <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

                    <div style='with:100px; float:right;'>
                <?
                if ($this->route == Yii::app()->controller->id.'/update'){
                ?>
                <a href='<?= Yii::app()->createUrl(Yii::app()->controller->id.'/delete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>




<?
if ($this->route == 'groups/update')
{





?>
<div style='width:100%; border:1px solid #ccc; border-radius:3px; margin-top:50px; background:#fff;'>
<table style='width:100%;'>
    <tr>
        <td style='width:60%; text-align:left; vertical-align:top; border-right:1px solid #ccc;'>
        <div style='padding:10px;'>
        <?

        $this->renderPartial('//modules/_f_photos', array(
            'photos' => $photos,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('groups/deletephotos', array()),
            'link_setcover'=>Yii::app()->createUrl('groups/setcover', array()),
        ));

        ?>
        </div>
        </td>
        <td style='text-align:left; vertical-align:top;'>
        <div style='padding:10px;'>
        <?

        $this->renderPartial('//modules/_f_docs', array(
            'docs' => $docs,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('groups/deletedocs', array()),
        ));

        ?>
        </div>
        </td>
    </tr>
</table>
</div>
<?

$this->renderPartial('//modules/_f_upload', array(
    'model' => $model,
    'link_add' => Yii::app()->createUrl('groups/filesadd', array('post_id'=>$model->id)),
));

?>


<?
}
?>



</div>
</div>

