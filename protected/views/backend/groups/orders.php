<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:70%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a>
                        </div>
                    </td>
                    <td style='width:30%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить', array('create'), array('class' => 'btn btn-outline-success')); ?>
                        <a href="<?=Yii::app()->createUrl('cats/update',array('id'=>$this->razdel['id']));?>" class='btn btn-outline-success' style=''>
                            <svg class="bi bi-gear" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8.837 1.626c-.246-.835-1.428-.835-1.674 0l-.094.319A1.873 1.873 0 014.377 3.06l-.292-.16c-.764-.415-1.6.42-1.184 1.185l.159.292a1.873 1.873 0 01-1.115 2.692l-.319.094c-.835.246-.835 1.428 0 1.674l.319.094a1.873 1.873 0 011.115 2.693l-.16.291c-.415.764.42 1.6 1.185 1.184l.292-.159a1.873 1.873 0 012.692 1.116l.094.318c.246.835 1.428.835 1.674 0l.094-.319a1.873 1.873 0 012.693-1.115l.291.16c.764.415 1.6-.42 1.184-1.185l-.159-.291a1.873 1.873 0 011.116-2.693l.318-.094c.835-.246.835-1.428 0-1.674l-.319-.094a1.873 1.873 0 01-1.115-2.692l.16-.292c.415-.764-.42-1.6-1.185-1.184l-.291.159A1.873 1.873 0 018.93 1.945l-.094-.319zm-2.633-.283c.527-1.79 3.065-1.79 3.592 0l.094.319a.873.873 0 001.255.52l.292-.16c1.64-.892 3.434.901 2.54 2.541l-.159.292a.873.873 0 00.52 1.255l.319.094c1.79.527 1.79 3.065 0 3.592l-.319.094a.873.873 0 00-.52 1.255l.16.292c.893 1.64-.902 3.434-2.541 2.54l-.292-.159a.873.873 0 00-1.255.52l-.094.319c-.527 1.79-3.065 1.79-3.592 0l-.094-.319a.873.873 0 00-1.255-.52l-.292.16c-1.64.893-3.433-.902-2.54-2.541l.159-.292a.873.873 0 00-.52-1.255l-.319-.094c-1.79-.527-1.79-3.065 0-3.592l.319-.094a.873.873 0 00.52-1.255l-.16-.292c-.892-1.64.902-3.433 2.541-2.54l.292.159a.873.873 0 001.255-.52l.094-.319z" clip-rule="evenodd"/>
                                <path fill-rule="evenodd" d="M8 5.754a2.246 2.246 0 100 4.492 2.246 2.246 0 000-4.492zM4.754 8a3.246 3.246 0 116.492 0 3.246 3.246 0 01-6.492 0z" clip-rule="evenodd"/>
                            </svg>
                        </a>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<style>
a {
    color:#222;
}
</style>

<a href="<?=Yii::app()->createUrl('groups/index',array());?>" class='btn btn-outline-success'  style='font-size:13px;'>Список мероприятий</a>
<a href="<?=Yii::app()->createUrl('groups/orders',array());?>" class='btn btn-success'  style='font-size:13px;'>Все заявки</a>

<div style='padding-top:50px;'>
<style>
.zaya{
padding:0px 10px; color:#000; font-size:14px; cursor:pointer;
}

</style>
<div style='width:100%; padding-bottom:20px;' >
<div style='border-bottom:2px solid #000; margin-bottom:10px;'>
        <div style='width:30px;  float:left; padding-left:0px;' class='zaya'><b>№</b></div>
        <div style='width:100px;  float:left;' class='zaya'><b>Дата заказа</b></div>
        <div style='float:left; width:250px;' class='zaya'><b>Имя</b></div>
        <div style='width:300px;  float:left;' class='zaya'><b>Дата мероприятия</b></div>
        <div style='width:50px;  float:left;' class='zaya'><b>Файлы</b></div>
        <div style='width:150px;  text-align:right;  float:right; padding-right:0px;' class='zaya'><b>Статус</b></div>
        <div style='clear:both;'></div>
</div>
<?php
$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$provider,
'itemView'=>'_order_item',
'viewData'=>array(
),
'template'=>"\n{items}\n{pager}",
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/site_pages.css?v=1', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));
?>

</div>
</div>
</div>
</div>
