<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:80%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'>Групповые мероприятия</a>
                            <div style='margin-top:0px; font-size:14px; text-transform:uppercase; color:#A0A0A0;'>
                                <a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>">Групповые мероприятия</a> /
                                <a href="<?=Yii::app()->createUrl('groups/view',array('id'=>$model->id));?>"><? echo mb_substr($model->title, 0, 50,'utf-8'); ?>...</a>
                            </div>
                        </div>
                    </td>
                    <td style='width:10%; height:100px; text-align:right;' valign=middle>

                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<style>
a {
    color:#222;
}
</style>
<div style='background:#fff; box-shadow:0px 0px 2px rgba(0,0,0,.1); border-radius:5px; margin-bottom:50px;'>
    <div style='padding:20px; font-size:16px; line-height:1.4em;'>
    <div style='font-size:22px; margin-bottom:15px;'><b><?=$model->title;?></b></div>
    <?=$this->crop_str_word($model->text, 30 );?>

        <?
        $photos = $model->photos;

        if ($photos){
            ?>
            <div style='padding-top:0px;'>
            <?
            foreach($photos as $index => $photo){
                $margin='margin-right:7px;';
                if ($index == 6){$margin='';}
                if ($index > 6){continue;}
                ?>
                <div style='width:80px; height:50px; display:block; border-radius:3px; float:left; <?=$margin;?> background:url(<?=$photo->getThumbnailUrl();?>); background-size:cover;'></div>
                <?
            }
            ?>
            <div style='clear:both;'></div>
            </div>
            <?
        }
        ?>


    <div style='margin-top:30px;'>
    <a href='<?= Yii::app()->createUrl('groups/update',array('id'=>$model->id)); ?>' style='line-height:1em;' class='btn btn-success'>Редактировать</a>
    </div>
    </div>
</div>


<div style=''>
<?
        $this->renderPartial('_timetable', array(
            'model' => $model,
            'timetable' => $timetable,
        ));
?>


<table style='width:100%; padding-bottom:20px;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:26px;'><b>Заявки</b></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>

                    </td>
            </tr>
            </table>
<style>
.zaya td{
padding:8px 5px; border-bottom:1px solid #ccc; color:#000; font-size:14px; cursor:pointer;
}

</style>
<div style='width:100%; padding-bottom:20px;' >
<div style='border-bottom:2px solid #000; margin-bottom:10px;'>
        <div style='width:30px;  float:left; padding-left:0px;' class='zaya'><b>№</b></div>
        <div style='width:100px;  float:left;' class='zaya'><b>Дата заказа</b></div>
        <div style='float:left; width:250px;' class='zaya'><b>Имя</b></div>
        <div style='width:300px;  float:left;' class='zaya'><b>Дата мероприятия</b></div>
        <div style='width:50px;  float:left;' class='zaya'><b>Файлы</b></div>
        <div style='width:150px;  text-align:right;  float:right; padding-right:0px;' class='zaya'><b>Статус</b></div>
        <div style='clear:both;'></div>
</div>
<?php
$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$dataProvider,
'itemView'=>'_order_item',
'viewData'=>array(),
'template'=>"\n{items}\n{pager}",
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/site_pages.css?v=1', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));
?>
</div>
</div>
</div>
</div>
