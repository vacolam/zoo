<style>
.chart_redactor_table td{padding:15px 5px;border-bottom:1px solid #ccc; }
</style>

<?
$data = CJSON::decode($chart->data);


//Выясняем чем заполнять ось Х
$data_x = array();
if (is_array($data))
{
    //Это мы вытаскиваем линию X
    if (isset($data[0])){
        if (isset($data[0]['data'])){
            if (is_array($data[0]['data'])){
                if (count($data[0]['data']) > 0){
                    $data_x = $data[0]['data'];
                }
            }
        }
    }
}

$main_X_array = array();
if (count($data_x) > 0)
{
    $main_X_array = $data_x;
}else{
    $main_X_array = array(0,0,0,0,0);
}


//ВЫЯСНЯЕМ ЧЕМ ЗАПОЛНЯТЬ ОСЬ Y
$data_y = false;
if (is_array($data))
{
    //Это мы вытаскиваем линию X
    if (isset($data[1])){
        if (isset($data[1]['data'])){
            if (is_array($data[1]['data'])){
                if (count($data[1]['data']) > 0){
                    $data_y = true;
                }
            }
        }
    }
}
$main_Y_array = array();
if ($data_y == true)
{
    $main_Y_array = $data;
}else{
    $main_Y_array = array();
    $main_Y_array[0] = array('type'=>'x');
    $main_Y_array[1] = array('type'=>'y','name'=>'','data'=>array(0,0,0,0,0));
}




?>


<div>
<table class='chart_redactor_table' style='border-collapse: collapse; width:100%;'>
    <tr>
        <td class='' align=left valign=middle style='padding-right:20px;'>
            <div style='font-size:16px; margin-top:40px;'><b>ОСЬ X</b></div>
            <div style='font-size:12px;'>укажите число или год</div>
        </td>

        <?
        foreach($main_X_array as $index => $value){
        ?>
        <td class=''>
            <div style='margin-bottom:15px;cursor:pointer; width:80px; height:30px; line-height:30px; color:#707070; text-align:center; border: 1px solid #ccc; background:#F6F6F6;  border-radius:15px;' title='Удалить столбик'  onclick='delete_stolbik(this)' class='delete_stolbik'>x</div>
            <input type="text" style='width:80px; height:40px; line-height:40px; padding:10px; font-size:12px; text-align:center;' name='data_x[]' value='<?=$value;?>'/>
        </td>
        <?
        }
        ?>
        <td>
            <div onclick='add_stolbik()' style='margin-top:45px; cursor:pointer; width:80px; height:40px; line-height:40px; color:#707070; text-align:center; border: 1px solid #ccc; background:#F6F6F6; font-size:30px; border-radius:3px;'>
            <b>+</b>
            </div>
        </td>

    </tr>


<?
foreach($main_Y_array as $line_index => $line_array)
{
    if ($line_array['type']=='x'){continue;}
    if (isset($line_array['data']))
    {
    $name = '';
    if (isset($line_array['name']))
    {
    $name = $line_array['name'];
    }
    ?>
    <tr>
        <td class='' align=left valign=middle style='padding-right:20px;'>
            <div style='font-size:16px; padding-bottom:5px;'>ЛИНИЯ ПО <b>ОСИ Y</b></div>
            <input type="text" class='line_name' placeholder='Название линии' style='width:250px; height:40px; line-height:40px; padding:10px; font-size:12px;' name='data_y[<?=$line_index;?>][name]' value='<?=$name;?>'/>
        </td>
        <?
        if (count($line_array['data']) > 0)
        {
        $line = $line_array['data'];
        }else{
        $line = array(0,0,0,0,0);
        }


        foreach($line as $i => $val)
        {
        ?>
        <td class=''>
            <input type="text" style='margin-top:26px;width:80px; height:40px; line-height:40px; padding:10px; font-size:12px;' placeholder='число' name='data_y[<?=$line_index;?>][data][]' value='<?=$val;?>'/>
        </td>
        <?
        }
        ?>
        <td>
            <div style='margin-top:26px; cursor:pointer; width:80px; height:40px; line-height:40px; color:#707070; text-align:center; border: 1px solid #ccc; background:#F6F6F6;  border-radius:3px;' title='удалить строку' onclick='delete_row(this)'>x</div>
        </td>
    </tr>
    <?
    }
}
?>

</table>

<div  style='margin-top:20px; cursor:pointer; width:100%; height:40px; line-height:40px; color:#707070; text-align:center; border: 1px solid #ccc; background:#F6F6F6; border-radius:3px;' onclick='add_row()'>Добавить еще одну линию по оси Y</div>
</div>

<script>
function delete_row(obj){
    if (confirm('Точно удаляем линию?') == true){
    var t = $(obj).closest('tr').index();
    $('.chart_redactor_table tr').eq(t).remove();
    }
}

function add_row(){

    $('.chart_redactor_table tr:last').clone().appendTo(".chart_redactor_table");

    var last_obj = $('.chart_redactor_table tr:last');

    var row_index = last_obj.index();

    $(last_obj).find('input').each(function(index, obj){
          $(obj).val('');

          if (index == 0){
              var name = 'data_y['+row_index+'][name]';
          }else{
              var name = 'data_y['+row_index+'][data][]';
          }

          $(obj).prop('name',name);
    })

}

function add_stolbik(){
    $('.chart_redactor_table tr').each(function(index, obj){
        if (index == 0){
            $(obj).find('td:last').before("<td><div style='margin-bottom:15px;cursor:pointer; width:80px; height:30px; line-height:30px; color:#707070; text-align:center; border: 1px solid #ccc; background:#F6F6F6;  border-radius:15px;' title='Удалить столбик'  onclick='delete_stolbik(this)' class='delete_stolbik'>x</div><input type='text' style='width:80px; height:40px; line-height:40px; padding:10px; font-size:12px; text-align:center;' name='data_x[]' value=''/> </td>");
        }
        else{
           $(obj).find('td:last').before("<td><input type='text' style='margin-top:26px;width:80px; height:40px; line-height:40px; padding:10px; font-size:12px;' placeholder='число' name='data_y["+index+"][data][]' value=''/> </td>");
        }
    })
}

function delete_stolbik(obj){
    if (confirm('Точно удаляем столбик?') == true){
    var t = $(obj).closest('td').index();
    $('.chart_redactor_table tr').each(function(index, obj){
        $(obj).find('td').eq(t).remove();
    });
    }
}

</script>