<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <div class='title_three'>Главная</div>
        </div>
    </div>

<div class='content_one'>
<style>
.cont a {
    color:#222;
}
</style>

<?
$home_menu = array(
                array(
                    'label' => 'Баннеры вверху',
                    'url' => Yii::app()->createUrl('banners/index'),
                    'active' => $this->id === 'banners',
                    'visible'  => 1
                ),
                array(
                            'label' => 'Расписание, стоимость билетов, Кнопка "Купить билет"',
                            'url' => Yii::app()->createUrl('raspisanie/index'),
                            'active' => $this->id === 'raspisanie',
                            'visible'  => 1
                ),
                array(
                    'label' => 'Блоки справа',
                    'url' => Yii::app()->createUrl('infoblocks/index'),
                    'active' => $this->id === 'infoblocks',
                    'visible'  => 1
                ),
             array(
                    'label' => 'Чем заняться',
                    'url' => Yii::app()->createUrl('whattodo/index'),
                    'active' => $this->id === 'whattodo',
                    'visible'  => 1
                ),
                array(
                            'label' => 'Ссылки внизу сайта',
                            'url' => Yii::app()->createUrl('links/index'),
                            'active' => $this->id === 'links',
                    'visible'  => 1
                ),
                array(
                            'label' => 'Баннеры внизу сайта',
                            'url' => Yii::app()->createUrl('bannersbottom/index'),
                            'active' => $this->id === 'bannersbottom',
                    'visible'  => 1
                ),



                );
?>

<div style='padding-top:0px;'>
<?php

foreach ($home_menu as $menu){
    ?>
    <div style='padding-bottom:20px; margin-bottom:20px; border-bottom:1px solid #ccc;'>
                    <a href="<?= $menu['url']; ?>" style='display:block;'>
                        <div style=''>
                             <div style='font-size:16px;' class='open-s'><b><?=$menu['label'];?></b></div>
                        </div>
                        <div style='clear:both;'></div>
                    </a>
    </div>

    <?
}

?>
</div>
</div>
