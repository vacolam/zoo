<?
$allowed_cats      =   User::getAllowedCats();
?>
    <style>
    ul.side_menu{
        list-style:none; margin:0px; padding:0px;
    }
    ul.side_menu li{
        width:100%;
    }

    .menu_item{cursor:pointer; }
    .menu_item a{color: #989eb3; font-size:14px; line-height:14px; width:100%; padding-top:12px; padding-bottom:12px; display:block;}
    .menu_item a:hover{background:rgba(0,0,0,.1); color: #fff;}
    .menu_item.active a{background:rgba(0,0,0,.1); color: #fff;}
    .menu_item.notvisible a{color:#B0B0B0 !important;}
    .menu_item a .bull{color: #989eb3; padding-right:10px; font-size:18px; line-height:14px;}
    .menu_item a:hover .bull, .menu_item.active a .bull{color: #536BE2;}

    </style>
    <div style='width:100%; background:rgba(0,0,0,.2); padding:15px 0px;'>
        <div style='padding-left:35px; padding-right:10px;'>
            <table style='width:100%;'>
                <tr>
                    <td valign=top>
                        <?
                        $link = Yii::app()->createUrl('options/userlist',array());
                        if (Yii::app()->user->role == 'user'){
                        $link = '';
                        }
                        ?>
                        <a href="<?=$link;?>" style='color:#6B7FE6 !important;'><?=Yii::app()->user->name;?></a>
                    </td>
                    <td style='width:30px; padding-top:5px;' valign=top>
                        <?
                        if (Yii::app()->user->role != 'user')
                        {
                        ?>
                        <a href="<?=Yii::app()->createUrl('search/index',array());?>">
                        <svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="#fff" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                        </svg>
                        </a>
                        <?
                        }
                        ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <ul style="margin-top:20px;" class='side_menu'>
        <?
        if (Yii::app()->user->role != 'user')
        {
        $nots = Notifications::countNotification();
        ?>
        <li class="menu_item <? if (Yii::app()->controller->id == 'dashboard'){echo "active";}?>" style='margin-bottom:15px;'>
                <a href="<?=Yii::app()->createUrl('dashboard/index',array());?>">
                    <div style='padding:0px 40px;'>
                    <span class='bull'>&nbsp;</span>
                    <span>Заявки</span>
                    <?
                    if ($nots > 0){
                        ?>
                        <span style='background:#28A745; color:#fff; padding:7px 12px; border-radius:8px; margin-left:5px; font-size:12px;'><?=$nots;?></span>
                        <?
                    }
                    ?>
                    </div>
                </a>
        </li>
        <?
        }
        ?>

        <?php
        foreach ($this->menu as $item) {

            if ($item['plugin'] == 'eng'){continue;}

            $active = '';
            if ($this->active_menu['id'] == $item['id']){$active = 'active';}
            $notvisible = $item['visible'] ? '' : ' notvisible';
            ?>
            <li class="menu_item <?= $active; ?><?= $notvisible; ?>" style='margin-right:20px;'>
                <a href="<?=$item['url'];?>">
                    <div style='padding:0px 40px;'>
                    <span class='bull'>&bull;</span>
                    <span><?=$item['label'];?></span>
                    </div>
                </a>
            </li>
            <?php

        }
        ?>

        <?
        $dashed_color = '#989eb3';
        if ($this->route == 'cats/create'){$dashed_color = '#fff';}

        if (Yii::app()->user->role == 'admin')
        {
        ?>
        <li class="menu_item <? if ($this->route == 'cats/create'){echo "active";}?>" style='margin-bottom:15px;'>
                <a href="<?=Yii::app()->createUrl('cats/create',array());?>">
                    <div style='padding:0px 40px;'>
                    <span class='bull'>&nbsp;</span>
                    <span style='border-bottom:1px dashed <?=$dashed_color;?>;'>Добавить раздел</span>
                    </div>
                </a>
        </li>
        <?
        }


        if (User::checkCat('82',$allowed_cats) == true AND Yii::app()->user->role != 'user')
        {
        ?>
        <li class="menu_item <? if (Yii::app()->controller->id == 'eng'){echo "active";}?>" style='margin-top:15px;'>
                <a href="<?=Yii::app()->createUrl('eng/index',array());?>">
                    <div style='padding:0px 40px;'>
                    <span class='bull'>&bull;</span>
                    <span>ENG</span>
                    </div>
                </a>
        </li>
        <?
        }

        if (Yii::app()->user->role == 'admin'){
        ?>
        <li class="menu_item <? if (Yii::app()->controller->id == 'options'){echo "active";}?>" style='margin-top:15px;'>
                <a href="<?=Yii::app()->createUrl('options/userlist',array());?>">
                    <div style='padding:0px 40px;'>
                    <span class='bull'>&nbsp;</span>
                    <span>Настройки</span>
                    </div>
                </a>
        </li>
        <?
        }
        ?>


        <li class="menu_item" style=''>
                <a href="<?=Yii::app()->createUrl('site/logout',array());?>">
                    <div style='padding:0px 40px;'>
                    <span class='bull'>&nbsp;</span>
                    <span>Выход</span>
                    </div>
                </a>
        </li>
        <li style='clear:both;'></li>
    </ul>