<html>
<head>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/bootstrap/popper.min.js"></script>
    <script src="js/bootstrap/js/bootstrap.min.js"></script>
    <title>Административная панель. Сахалинский Зоопарк.</title>

    <link href="js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" media="all">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="css_tool/fancy.css?id=2" rel="stylesheet">
	<style>
    html,body{
        font-family:Open Sans, Verdana, Tahoma, Arial; font-weight:400;
    }
    .open-s{font-family:Open Sans, Verdana, Tahoma, Arial; font-weight:400;
    }
    a, a:hover{
        text-decoration:none;
    }

    .title_one{width:100%; height:100px; background:#fff; border-bottom:1px solid #ccc;}
    .title_two{width:950px; padding-left:60px;}
    .title_three{line-height:100px; font-size:40px;}
    .content_one{width:950px; padding-left:60px; padding-top:60px;}
    </style>



	<script>
	function strToNum(str){
	   	var stringNum = str+'';
	       stringNum = stringNum.replace(",",".");
	       var numFloat = parseFloat(stringNum);
		if (isNaN(numFloat))
			{
				numFloat = 0;
			}
		return numFloat;
	}
	</script>
	<?
	    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $site_controllers_array = array('news','home','pages','banners','otherinfo','links','afisha','projects','contacts','exposure','exposureitem','eng','raspisanie','infoblocks','whattodo','partners','','','','');
	?>
</head>
<body style=''>
<div style='width:100%;height:100%;'>
<?php
if (!Yii::app()->user->isGuest) {
$this->menu = $this->getSiteMenu();
?>

    <div style='position:fixed; top:0px; left:0px; height:100%; width:260px; background:#242939; z-index:9999;'>
        <div style=''>
                        <div style='' valign=top align=left>
                        <?php
                            $this->renderPartial('//layouts/_left_menu', array());
                        ?>
                        </div>
        </div>
    </div>
<?php
}
?>
<?
$bg = '#F2F3F8';
if (Yii::app()->user->isGuest) {
$bg = '#fff';
}
?>
    <div class="" style='min-height:100%; width:100%; background:<?=$bg;?>;'>
    <div style='margin-left:260px; padding-bottom:200px;'>
        <div style=''>
        <?php
        if(Yii::app()->user->hasFlash('success'))
        {
            ?>
            <div style='width:400px;  min-height:100px; height:auto; text-align:center; position:absolute; left:50%; bottom:10px; margin-left:-200px; ' class='success-message'>
            <div class="alert alert-success" style='box-shadow:0px 0px 50px rgba(0,0,0,.35); border:1px solid rgba(0,0,0,.1); background:#28A745; color:#fff;'>
                <?= Yii::app()->user->getFlash('success'); ?>
            </div>
            </div>
            <script>
            $(function(){
                setTimeout(function(){
                    $('.success-message').fadeOut(300);
                },2000)
            });
            </script>
            <?php
        }


        if(Yii::app()->user->hasFlash('warning'))
        {
            ?>
            <div style='width:400px;  min-height:100px; height:auto; text-align:center; position:absolute; left:50%; bottom:10px; margin-left:-200px; ' class='warning-message'>
            <div class="alert alert-danger" style='box-shadow:0px 0px 50px rgba(0,0,0,.35); border:1px solid rgba(0,0,0,.1); background:#DC3545; color:#fff;'>
                <?= Yii::app()->user->getFlash('warning'); ?>
            </div>
            </div>
            <script>
            $(function(){
                setTimeout(function(){
                    $('.warning-message').fadeOut(300);
                },3000)
            });
            </script>
            <?php
        }
        ?>
        <div class='cont'>
            <?
            //echo 'options';
            ?>
        <?= $content; ?>
        </div>
        </div>
        <div style='clear:both;'></div>
    </div>
    </div>
<div style='clear:both;'></div>
</div>


<div style='clear:both;'></div>
<div style='height:50px; background:#FAFAFA; border-top:1px solid #ccc; width:100%; display:none;'></div>
</body>
</html>
