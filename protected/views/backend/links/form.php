<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <a href="<?= Yii::app()->createUrl('home/index',array()); ?>" style='font-size:40px; color:#000;'>Главная</a>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'><a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>">Ссылки в подвале сайта</a> - <?= $model->isNewRecord ? 'добавление' : 'редактирование'; ?></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<?php echo CHtml::beginForm(); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <div class="form-group<?= $model->hasErrors('title') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'title', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'title', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'title', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('link') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'link', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'link', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'link', array('class' => 'help-block')); ?>
    </div>



  <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

                    <div style='with:100px; float:right;'>
                <?
                if ($this->route == 'links/update'){
                ?>
                <a href='<?= Yii::app()->createUrl('links/delete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>
    
<?php echo CHtml::endForm(); ?>
</div>
</div>
