<div id='module_docs_<?=$model->id;?>'>
<div style='font-size:26px; font-weight:600; padding-bottom:20px;'>Файлы</div>
<table style='width:100%;'>
<?
$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$docs,
'itemView'=>'//modules/_f_docs_item',
'template'=>"\n{items}\n{pager}",
'emptyText'=>'',
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/pages.css', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));
?>

<tr>
    <td colspan="3">
        <div class='delete_docs_button' style="display:none;background:#C9302C; width:100%; cursor:pointer; margin-bottom:5px;"  onclick="delete_checked_docs()"><div style="padding:20px; text-align:center; font-size:14px; color:#fff;">Удалить файлы</div></div>
        <div style='background:#f0f0f0; cursor:pointer; width:100%;' onclick="add_photos_and_files('docs')"><div style="padding:20px; text-align:center; font-size:14px;">Добавить Файлы</div></div>
    </td>
</tr>
</table>
<script>
function deleteDocs(id){
            $('.doc[rel="'+id+'"]').remove();
            $.ajax({
                type: "GET",
                url: '<?= $link_delete; ?>',
                dataType: 'json',
                data: {
                    'id':id
                },
                success: function(response){
                    if (response != false){

                    }else{
                        //show_ozon('yandex_rtb_R-A-400319-2');
                    }
                },
                error: function(response){
                }
            });
}

function delete_docs(){
    var count = $('#module_docs_<?=$model->id;?>').find('.delete_docs:checked').length;
    if (count >0){
        $('#module_docs_<?=$model->id;?>').find('.delete_docs_button').css('display','block');
    }else{
        $('#module_docs_<?=$model->id;?>').find('.delete_docs_button').css('display','none');
    }
}

function delete_checked_docs(){
    $(".delete_docs:checked").each(function(indx, element){
        var id = $(element).attr('rel');
        deleteDocs(id);
    });
    delete_docs();
}
</script>
</div>