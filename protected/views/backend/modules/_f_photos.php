<style>
.photo{
    border:3px solid #fff
}

.photo.isCover{
    border:3px solid #f60000
}

</style>
<div style='padding-top:0px;'>
<div style='font-size:26px; font-weight:600; padding-bottom:20px;'>Фотографии</div>
<?php
$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$photos,
'viewData'=>array(
    'model' => $model,
),
'itemView'=>'//modules/_f_photos_item',
'template'=>"\n{items}\n{pager}",
'emptyText'=>'',
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/pages.css', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));

?>
<div style='clear:both;'></div>
<div class='delete_photos_button' style="display:none;background:#C9302C; width:100%; cursor:pointer; margin-bottom:5px;"  onclick="delete_checked_photos()"><div style="padding:20px; text-align:center; font-size:14px; color:#fff;">Удалить фотографии</div></div>
<div style="background:#f0f0f0; width:100%; cursor:pointer;"  onclick="add_photos_and_files('photos')"><div style="padding:20px; text-align:center; font-size:14px;">Добавить фотографии</div></div>

<script>
function deletePhotos(id){
            $('.photo[rel="'+id+'"]').remove();
            $.ajax({
                type: "GET",
                url: '<?=$link_delete;?>',
                dataType: 'json',
                data: {
                    'id':id,
                },
                success: function(response){
                    if (response != false){
                       if (response.photo_id > 0){
                           $('.photo').removeClass('isCover');
                           $('.photo[rel="'+response.photo_id+'"]').addClass('isCover');
                        }
                        if (response.photo_adress != ''){
                            $('.cover_background').css('background','url("'+response.photo_adress+'") center');
                        }
                    }else{
                        //show_ozon('yandex_rtb_R-A-400319-2');
                    }
                },
                error: function(response){
                }
            });
}

function delete_photos(){
    var count = $('.delete_photos:checked').length;
    if (count >0){
        $('.delete_photos_button').css('display','block');
    }else{
        $('.delete_photos_button').css('display','none');
    }
}

function delete_checked_photos(){
    $(".delete_photos:checked").each(function(indx, element){
        var id = $(element).attr('rel');
        deletePhotos(id);
    });
    delete_photos();
}

function set_cover(id){
            $('.photo').removeClass('isCover');
            $('.photo[rel="'+id+'"]').addClass('isCover');

            $.ajax({
                type: "GET",
                url: '<?=$link_setcover; ?>',
                dataType: 'json',
                data: {
                    'news_id':<?=$model->id?>,
                    'cover_id':id,
                },
                success: function(response){
                    if (response != false){
                        //alert(response);
                        $('.cover_background').css('background','url("'+response+'") center');
                    }else{
                        //show_ozon('yandex_rtb_R-A-400319-2');
                    }
                },
                error: function(response){
                }
            });


}
</script>
<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>
</div>