<style>
        .redactor textarea{
            width:100%; height:400px;
        }

    </style>

    <div class='redactor'>
    <div class="form-group">

        <?php echo CHtml::activeLabel($model, $text_input_name, array('class' => 'control-label')); ?>
        <?php echo CHtml::activeTextArea($model, $text_input_name, array('class' => 'form-control', 'style'=>'height:400px; width:100%; font-size:12px;', 'id'=>'content')); ?>

<!------------------------------------------------->

        <script src="/js/imperavi3/redactor.js"></script>
        <script src="/js/imperavi3/ru.js"></script>
        <script src="/js/imperavi3/plugins/table.min.js"></script>
        <script src="/js/imperavi3/plugins/alignment.min.js"></script>
        <script src="/js/imperavi3/plugins/fontsize.min.js"></script>
        <script src="/js/imperavi3/plugins/fontcolor.min.js"></script>
        <script src="/js/imperavi3/plugins/fontfamily.min.js"></script>
        <script src="/js/imperavi3/plugins/filemanager.min.js"></script>
        <script src="/js/imperavi3/plugins/video.min.js"></script>
        <script src="/js/imperavi3/plugins/fullscreen.js"></script>
        <script src="/js/imperavi3/plugins/widget.min.js"></script>
        <link href="/js/imperavi3/redactor.css?v=2" rel="stylesheet" type="text/css" media="all">
        <!--<link href="/js/imperavi3/filemanager.min.css" rel="stylesheet" type="text/css" media="all">-->
        <script>
            $R('#content', {
                lang: 'ru',
                minHeight: 400,
                plugins: ['table','alignment','fontsize','fontcolor','fontfamily','filemanager','video','fullscreen','widget'],
                buttonsAdd: ['underline', 'indent', 'outdent',],
                imageUpload: '<?=Yii::app()->createUrl('redactor/uploadimages');?>',
                imageResizable: true,
                imagePosition: true,
                fileUpload: '<?=Yii::app()->createUrl('redactor/uploadfiles');?>',

            });
         </script>
<!------------------------------------------------->
    </div>
    </div>