<div  class=''  style=''>
    <div class="">
        <div style='margin-bottom:5px;'>
            <input type="text" class='add_tag' style='border:1px solid #ccc; border-radius:3px; padding:0px 10px; height:30px; line-heigth:30px; width:200px; color:#222; font-size:14px; float:left;' placeholder='Теги' />
            <div style='float:left; width:30px; height:30px; margin-left:5px; font-size:20px; padding:0px; line-height:30px; text-align:center;' class='btn btn-outline-secondary' onclick='addTag()'><b>+</b></div>
            <div style='clear:both;'></div>
        </div>

        <div style='padding-top:0px; font-size:12px; color:#a0a0a0; margin-bottom:10px;'>
            <?
             if ($tags){
                    foreach($tags as $index => $tag)
                    {
                        if (in_array($tag['id'],$checked_tags)){
                        echo "<span>".$tag['name']." | </span>";
                        }
                    }
                }
            ?>
        </div>

        <?
        $height = '';
        if ($tags){
            if (count($tags) > 3){
                $height = '85px;';
            }
        }
        ?>
        <div style='height:<?=$height;?>; overflow:hidden;' class='tags_block'>
            <ul style='list-style:none; padding:0px; margin:0px;' class='tags_ul'>
            <?
                $tags_array = array();
                if ($tags){
                    foreach($tags as $index => $tag)
                    {
                        $checked = '';
                        if (in_array($tag['id'],$checked_tags)){
                        $checked = 'checked="checked"';
                        }
                    ?>
                    <li style='padding-bottom:2px;' class='tag_li' rel='<?=$tag['id'];?>'>
                        <input type="checkbox" name='Tags[]' value='<?=$tag['id'];?>' <?=$checked;?> id='tag_<?=$tag['id'];?>'/> <label for='tag_<?=$tag['id'];?>'><?=$tag['name'];?></label><span style='cursor:pointer;' title='Удалить' onclick='delete_tag(<?=$tag['id'];?>);'> [x]</span>
                    </li>
                    <?
                    }
                }
            ?>
            </ul>
        </div>
        <?
                if ($tags){
                    if (count($tags) > 3){
                        ?>
                            <div style='width:300px; cursor:pointer; margin-top:10px; padding:3px 10px; background:#f0f0f0; border-radius:3px;' class='open_tags_button' onclick='open_tags()'>
                                Раскрыть список тегов
                            </div>

                        <?
                    }
                }
        ?>
    </div>
</div>

        <script>
        function addTag(){
            var tag_name = $('.add_tag').val();
            $('.add_tag').val('');
            var html = "<li  style='padding-bottom:2px;' class='tag_li new_tag'>"+
                        "<div style='padding:2px; background:#f0f0f0; font-size:14px;'>"+tag_name+"</div>"+
                        "</li>";
            $('.tags_ul').append(html);
            open_tags();

            if (tag_name != ''){
                $.ajax({
                    type: "GET",
                    url: '<?=$link_add;?>',
                    dataType: 'json',
                    data: {
                        'tag_name':tag_name
                    },
                    success: function(response){
                        if (response != false){
                            var html = "<input type='checkbox' name='Tags[]' value='"+response+"' id='tag_"+response+"'/> <label for='tag_"+response+"'>"+tag_name+"</label><span style='cursor:pointer;' title='Удалить' onclick='delete_tag("+response+");'> [x]</span>";
                            $('.new_tag').html(html);
                            $('.new_tag').prop('rel',response);
                            $('.new_tag').removeClass('new_tag');
                        }else{
                            //show_ozon('yandex_rtb_R-A-400319-2');
                        }
                    },
                    error: function(response){
                    }
                });
            }
        }

        function open_tags(){
            $('.tags_block').css('height','auto');
            $('.open_tags_button').css('display','none');
        }

        function delete_tag(id){
            if (confirm('Точно удаляем этот тег?')){
                if (id != ''){
                $('.tag_li[rel="'+id+'"]').remove();
                $.ajax({
                    type: "GET",
                    url: '<?=$link_delete;?>',
                    dataType: 'json',
                    data: {
                        'id':id
                    },
                    success: function(response){
                        if (response != false){
                        }else{
                        }
                    },
                    error: function(response){
                    }
                });
            }
            }
        }
        </script>