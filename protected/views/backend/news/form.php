<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a></div>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'><?= $model->isNewRecord ? 'добавление' : $this->crop_str_word( $model->title, 6 ); ?></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one form'>

<script src="js/bootstrap-select-1.13.14.min.js"></script>
<link href="js/bootstrap-select-1.13.14.min.css" rel="stylesheet">


<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
    'class' => 'form_news',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <div style='width:550px; float:left; margin-top:-5px;'>

    <div class="form-group<?= $model->hasErrors('title') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'title', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'title', array('class' => 'form-control','style'=>'height:100px; width:100%;')); ?>
        <?= CHtml::error($model, 'title', array('class' => 'help-block')); ?>
    </div>

    <?
    $closedList = array(
        '0' => 'Опубликовано',
        '1' => 'Не опубликовано',
        '2' => 'Не опубликовано до',
    );
    ?>
    <div class="form-group<?= $model->hasErrors('is_closed') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'is_closed', array('class' => 'control-label')); ?>
        <?=CHtml::activeDropDownList($model,'is_closed',$closedList, array('class' => 'form-control is_closed_list', 'style'=>'width:100%;')); ?>
        <?= CHtml::error($model, 'is_closed', array('class' => 'help-block')); ?>
    </div>

    <script>
        $('.is_closed_list').on('change',function(){
            var selected =  $(this).find('option:selected').prop('value');

            if (selected == 2){
                $('.form-group.closed_till').css('display','block');
            }else{
                $('.form-group.closed_till').css('display','none');
            }
        })
    </script>

    <div class="form-group closed_till" style='<? if ($model->is_closed != 2){echo "display:none;";} ?>'>
        <label class="control-label" for="News_closed_till">Дата, когда надо опубликовать новость</label>
        <?php
        $date_value = '';
        if ($model->closed_till != '0000-00-00')
        {
    		$date = new DateTime($model->closed_till);
            $date_value = $date->format('d.m.Y');
        }
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'News[closed_till]',
            'htmlOptions' => array(
                'class' => 'form-control',
                'style' => 'width:370px; float:left;',
            ),
            'language' => 'ru',
            'value' => $date_value,
        ));
        ?>
        <style>
            .ui-datepicker {
                z-index: 999 !important;
            }
        </style>
        <?= CHtml::error($model, 'closed_till', array('class' => 'help-block')); ?>



        <?
        $close_time_hour = '00';
        $close_time_minute = '00';
        $c = explode(":",$model->closed_till_time);
        $close_time_hour = $c[0];
        $close_time_minute = $c[1];
        ?>
        <div style='width:60px; float:right; margin-left:0px;'>

            <select class='closed_till_minute' name='News[closed_till_minute]' style='width:60px; height:40px; line-height:40px; border:1px solid #ccc; border-radius:3px;'>
                                <?
                                    for($t = 0; $t <= 59 ; $t++)
                                    {
                                        $selected = '';

                                        $time = $t;
                                        if ($t < 10){$time = '0'.$t;}

                                        if ($time == $close_time_minute){
                                        $selected = 'selected="selected"';
                                        }
                                        ?>
                                        <option value="<?=$t;?>" <?=$selected;?>><?=$time;?></option>
                                        <?
                                    }
                                ?>
                            </select>

        </div>

        <div style='width:20px; float:right; text-align:center; height:40px; line-height:40px; '>
        &nbsp;:&nbsp;
        </div>

        <div style='width:60px; float:right; margin-left:0px;'>
            <select class='closed_till_hour' name='News[closed_till_hour]' style='width:60px; height:40px; line-height:40px; border:1px solid #ccc; border-radius:3px;'>
                                <?
                                    for($t = 0; $t <= 23; $t++)
                                    {
                                        $selected = '';
                                        $time = $t;
                                        if ($t < 10){$time = '0'.$t;}
                                        if ($time == $close_time_hour){
                                        $selected = 'selected="selected"';
                                        }
                                        ?>
                                        <option value="<?=$t;?>" <?=$selected;?>><?=$time;?></option>
                                        <?
                                    }
                                ?>
                            </select>

        </div>

        <div style='clear:both;'></div>
    </div>

    <div class="form-group<?= $model->hasErrors('date') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'date', array('class' => 'control-label')); ?>
        <?php
		$date = new DateTime($model->date);
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'News[date]',
            'htmlOptions' => array(
                'class' => 'form-control',

            ),
            'language' => 'ru',
            'value' => $date->format('d.m.Y'),
        ));
        ?>
        <style>
            .ui-datepicker {
                z-index: 999 !important;
            }
        </style>
        <?= CHtml::error($model, 'date', array('class' => 'help-block')); ?>
    </div>

    </div>


    <div style='width:200px; float:right; '>
    <?
    $bg = "background:#f0f0f0;";

    if (!$model->isNewRecord && $model->hasImage()) {
        $getThumbnailUrl = $model->getThumbnailUrl();
        $bg = "background:url(".$getThumbnailUrl.") #f0f0f0 center; background-size:cover;";
    }

    ?>
    <div style='width:200px; height: 180px; margin-top:20px; border-radius:3px; position:relative; <?=$bg;?>' class='cover_background'>
    </div>

    </div>

    <div style='clear:both;'></div>

        <?
        $this->renderPartial('//modules/_redactor', array(
            'model' => $model,
            'text_input_name' => 'pre_text',
        ));
        ?>


    <?
            if ($this->route == 'news/update'){
            ?>
            <div style='width:100%; position:relative;'>
            <div style='width:100px; margin:0px auto; margin-top:-35px; '>
            <?php echo CHtml::button('Предпросмотр', array('type' => 'button', 'class' => 'btn btn-outline-secondary', 'style'=>'background:#FCFCFC; color:#222;', 'onclick'=>'prevew('.$model->id.')')); ?>
            </div>
            </div>
            <?
            }
            ?>

    <div class="form-group<?= $model->hasErrors('short_text') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'short_text', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'short_text', array('class' => 'form-control', 'style'=>'height:120px;')); ?>
        <?= CHtml::error($model, 'short_text', array('class' => 'help-block')); ?>
    </div>


        <?
        $this->renderPartial('//modules/_tags', array(
            'model' => $model,
            'tags' => $tags,
            'checked_tags' => $checked_tags,
            'link_add' => Yii::app()->createUrl('news/addtag'),
            'link_delete' => Yii::app()->createUrl('news/deletetag'),
        ));
        ?>

    <div style=' margin-top:20px; margin-bottom:20px; border-top:1px dotted #ccc;'></div>

    <div class="form-group<?= $model->hasErrors('exposureitem_id') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'exposureitem_id', array('class' => 'control-label')); ?>
        <?=CHtml::activeDropDownList($model,'exposureitem_id',$exposureItemsList, array('class' => 'form-control dropdown', 'data-live-search'=>'true', 'style'=>'width:100%;')); ?>
        <?= CHtml::error($model, 'exposureitem_id', array('class' => 'help-block')); ?>
    </div>



    <div style=' margin-top:20px; margin-bottom:20px; border-top:1px dotted #ccc;'></div>


 <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

                    <div style='with:100px; float:right;'>
                <?
                if ($this->route == Yii::app()->controller->id.'/update'){
                ?>
                <a href='<?= Yii::app()->createUrl(Yii::app()->controller->id.'/delete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>




<?
if ($this->route == 'news/update')
{
?>
<div style='width:100%; border:1px solid #ccc; border-radius:3px; margin-top:50px; background:#fff;'>
<table style='width:100%;'>
    <tr>
        <td style='width:60%; text-align:left; vertical-align:top; border-right:1px solid #ccc;'>
        <div style='padding:10px;'>
        <?

        $this->renderPartial('//modules/_f_photos', array(
            'photos' => $photos,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('news/deletephotos', array()),
            'link_setcover'=>Yii::app()->createUrl('news/setcover', array()),
        ));

        ?>
        </div>
        </td>
        <td style='text-align:left; vertical-align:top;'>
        <div style='padding:10px;'>
        <?

        $this->renderPartial('//modules/_f_docs', array(
            'docs' => $docs,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('news/deletedocs', array()),
        ));

        ?>
        </div>
        </td>
    </tr>
</table>
</div>
<?

$this->renderPartial('//modules/_f_upload', array(
    'model' => $model,
    'link_add' => Yii::app()->createUrl('news/filesadd', array('post_id'=>$model->id)),
));

?>

</div>
</div>
<script>

function prevew(){
    var old_action = $('.form_news').prop('action');
    var preview_action = '<?=Yii::app()->createUrl('news/preview',array('id'=>$model->id))?>';
    $('.form_news').prop('action',preview_action);
     $('.form_news').submit();
}

$(function(){
    $('.dropdown').selectpicker();
})

</script>

<?
}
?>
