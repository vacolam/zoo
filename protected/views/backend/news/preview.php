<link href="css_tool/redactor-styles.css" rel="stylesheet">
<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:60%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'>
                            <a href="<?=Yii::app()->createUrl('news/update',array('id'=>$model->id));?>" style='color:#000; padding-top:0px; font-size:40px;'>Предпросмотр Новости</a>
                        </div>
                    </td>
                    <td style='width:40%; height:100px; text-align:right;' valign=middle>
                        <a href='<?= Yii::app()->createUrl('news/update',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-success'>Назад</a>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>
<div style='width: 810px; margin-top:30px;'>
    <?
        $bg =  "background:none; ";
         if ($model->hasImage()) {
            $bg_url = $model->getThumbnailUrl();
            $bg =  'background:url("'.$bg_url.'") #f0f0f0 center; background-size:cover;';
        }
    ?>

    <a href="<?=Yii::app()->createUrl('news/view',array('id' => $model->id));?>" style='display:block; width:200px; height:150px; float:left; border-radius:5px; margin-right:30px; <?=$bg;?>'></a>


    <div style='width:578px; float:left; margin-top:-10px;'>

<h2 style='font-size:25px; line-height:1.2em;'><b><?= $model->title ?></b></h2>
<div  style='padding-top:20px; color:#53952A;'>
    <?
    echo Dates::getDatetimeName($model->date);
    ?>
</div>

<div style='padding-top: 40px; ' class='redactor-styles'>
    <? echo $model->pre_text; ?>
</div>

<?
$photos = $model->photos;
if (count($photos) > 0)
{
    ?>
    <div style='margin-top:40px;'>
        <div style='font-size:16px; font-weight:600; padding-bottom:5px;'>Фотографии</div>
        <?
    foreach ($photos as $photo)
    {
        ?>
        <a href="<?=$photo->getImageUrl();?>" class='fancybox-media' rel='photos' style='width:115px; height:80px; display:block; float:left; margin-right:2px; margin-bottom:2px; background:url(<?=$photo->getThumbnailUrl();?>); background-size:cover;'></a>
        <?
    }
    ?>
    </div>
    <?
}
?>
<div style='clear:both;'></div>
<?
$docs = $model->docs;
if (count($docs) > 0)
{
    ?>
    <div style='margin-top:40px;'>
        <div style='font-size:16px; font-weight:600; padding-bottom:5px;'>Документы</div>
        <?
        foreach ($docs as $doc)
        {
            ?>
            <a href="<?=$doc->getDocUrl();?>" target='_blank' style=''><?=$doc->filetitle;?></a><br>
            <?
        }
        ?>
        </div>
        <?

}
?>
<div style='clear:both;'></div>
</div>
<div style='clear:both;'></div>


</div>



<div style='border:1px solid #ccc; background:#fff; border-radius:5px; margin-top:40px;'>
    <div style='padding:25px;'>
        <a href='<?= Yii::app()->createUrl('news/update',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-success'>Назад</a>
    </div>
</div>

</div>
</div>

<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>