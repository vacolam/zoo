<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl('options/userlist',array());?>" style='color:#000; padding-top:0px; font-size:40px;'>Настройки</a></div>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'>
                            <a href="<?=Yii::app()->createUrl('options/userlist',array());?>">Пользователи</a> -
                            добавление
                        </div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>
<style>
.infoError{
    background:#FF9966; color:#fff; text-align:left; padding:8px 10px; border-radius:3px; margin-bottom:0px; margin-top:15px;
}

.infoSuccess{
    background:#33CC66; color:#fff; text-align:left; padding:8px 10px; border-radius:3px; margin-bottom:20px; margin-top:15px;
}

.optionsUserlist{
    font-size:14px; padding-bottom:10px; border-bottom: 1px dotted #ccc; padding-top:10px;
}
</style>
<div style='padding-top:0px;'>
<?
    if(Yii::app()->user->hasFlash('newUserError')){
      	echo '<div class="infoError" style="">'.Yii::app()->user->getFlash('newUserError').'</div>';
    }
?>
</div>
<?
$form = $this->beginWidget('CActiveForm', array(
        'id'=>'user-form',
        'enableAjaxValidation'=>false,
        ));
?>
<div class="form-group" style=''>
    <div class='title' style='font-weight:normal;'>Имя пользователя</div>
    <div class='input'><?php echo $form->textField($new,'name', array('class' => 'form-control')); ?></div>
    <div style='clear:both;'></div>
</div>

<?
if ($new->role != 'admin'){
    $roleList = array('manager'=>'Пользователь','user'=>'Жюри');
?>
    <div class="form-group" >
        <div class='title'>Тип пользователя</div>
        <?=CHtml::activeDropDownList($new,'role',$roleList, array('class' => 'form-control dropdown', 'data-live-search'=>'true', 'style'=>'width:100%;')); ?>
        <?= CHtml::error($new, 'role', array('class' => 'help-block')); ?>
    </div>
<?
}

$List = UsersRoles::getArray();
?>
    <div  class="form-group">
        <div class='title'>Отдел</div>
        <?=CHtml::activeDropDownList($new,'role_id',$List, array('class' => 'form-control dropdown', 'data-live-search'=>'true', 'style'=>'width:100%;')); ?>
        <?= CHtml::error($new, 'role_id', array('class' => 'help-block')); ?>
    </div>

<div class="form-group" style=''>
    <div class='title' style='font-weight:normal;'>E-mail</div>
    <div class='input'><?php echo $form->textField($new,'email', array('class' => 'form-control')); ?></div>
    <div style='clear:both;'></div>
</div>

<div class="form-group" style=''>
    <div class='title' style='font-weight:normal;'>Пароль</div>
    <div class='input'><?php echo $form->textField($new,'password', array('class' => 'form-control')); ?></div>
    <div style='clear:both;'></div>
</div>



 <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>
            <div style='clear:both;'></div>
        </div>
    </div>
<?php $this->endWidget();?>


</div>
</div>
