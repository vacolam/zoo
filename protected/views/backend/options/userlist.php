<style>
.content_one a {
    color:#000;
}

</style>
<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl('options/userlist',array());?>" style='color:#000; padding-top:0px; font-size:40px;'>Настройки</a></div>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'>
                            <a href="<?=Yii::app()->createUrl('options/userlist',array());?>">Пользователи</a>
                        </div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить пользователя', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>




    <div class='' style='margin:0px; padding-top:0px; '>
    <?php
      	if(Yii::app()->user->hasFlash('deleteUserError')){
    	  	echo '<div class="infoError" style="">'.Yii::app()->user->getFlash('deleteUserError').'</div>';
    	}

    	if(Yii::app()->user->hasFlash('deleteUserSuccess')){
    	  	echo '<div class="infoSuccess" style="">'.Yii::app()->user->getFlash('deleteUserSuccess').'</div>';
    	}

    	if(Yii::app()->user->hasFlash('newUserSuccess')){
    	  	echo '<div class="infoSuccess" style="">'.Yii::app()->user->getFlash('newUserSuccess').'</div>';
    	}
    ?>
    </div>


	<div style=''>
	        <?
			$r = 0;
	  		foreach($users as $index => $user){
                $this->renderPartial('_userlist_item', array(
                    'data' => $user,
                ));
	        }
			?>
	</div>



</div>
</div>
