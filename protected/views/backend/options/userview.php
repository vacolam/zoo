<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl('options/userlist',array());?>" style='color:#000; padding-top:0px; font-size:40px;'>Настройки</a></div>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'>
                            <a href="<?=Yii::app()->createUrl('options/userlist',array());?>">Пользователи</a> -
                            <?= $users->isNewRecord ? 'добавление' : $this->crop_str_word( $users->name, 6 ); ?>
                        </div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить пользователя', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<style>
.infoError{
    background:#FF9966; color:#fff; text-align:left; padding:8px 10px; border-radius:3px; margin-bottom:0px; margin-top:15px;
}

.infoSuccess{
    background:#33CC66; color:#fff; text-align:left; padding:8px 10px; border-radius:3px; margin-bottom:20px; margin-top:15px;
}

.optionsUserlist{
    font-size:14px; padding-bottom:10px; border-bottom: 1px dotted #ccc; padding-top:10px;
}
</style>

<div style='' class="form">

<div style=''>
<?
$form = $this->beginWidget('CActiveForm', array(
	    'id'=>'user-form',
	    'enableAjaxValidation'=>false,
		));
?>
<div>
<?php
  	  if(Yii::app()->user->hasFlash('userUpdateError')){
	  	echo '<div class="infoError">'.Yii::app()->user->getFlash('userUpdateError').'</div>';
	}

	if(Yii::app()->user->hasFlash('userUpdateSuccess')){
	  	echo '<div class="infoSuccess">'.Yii::app()->user->getFlash('userUpdateSuccess').'</div>';
	}
?>
</div>
<div class="">
	<div class='title'>Имя</div>
	<div class='input'><?php echo $form->textField($users,'name',array('class' => 'form-control')); ?></div>
	<div style='clear:both;'></div>
</div>

<div class="" style='padding-top:15px;'>
	<div class='title'>E-mail</div>
	<div class='input'><?php echo $form->textField($users,'email',array('class' => 'form-control')); ?></div>
    <div style='clear:both;'></div>
</div>

<?
if ($users->role != 'admin'){
    $roleList = array('manager'=>'Пользователь','user'=>'Жюри');
?>
    <div style='padding-top:15px;'>
        <div class='title'>Тип пользователя</div>
        <?=CHtml::activeDropDownList($users,'role',$roleList, array('class' => 'form-control dropdown', 'data-live-search'=>'true', 'style'=>'width:100%;')); ?>
        <?= CHtml::error($users, 'role', array('class' => 'help-block')); ?>
    </div>
<?
}
?>

<?

    $List = UsersRoles::getArray();
?>
    <div style='padding-top:15px;'>
        <div class='title'>Отдел</div>
        <?=CHtml::activeDropDownList($users,'role_id',$List, array('class' => 'form-control dropdown', 'data-live-search'=>'true', 'style'=>'width:100%;')); ?>
        <?= CHtml::error($users, 'role_id', array('class' => 'help-block')); ?>
    </div>

<div class="" style='padding-top:15px;'>
	<div>
		<?php echo CHtml::submitButton('Сохранить изменения', array('class'=>'btn btn-success','style'=>'')); ?>
		<div style='clear:both;'></div>
	</div>
</div>
<?php $this->endWidget();?>
</div>



<div style='padding-top:30px;'>
<div style='font-size:16px; padding-bottom:20px;'><b>Доступы и уведомления</b></div>
<?
$form = $this->beginWidget('CActiveForm', array(
	    'id'=>'user-form',
	    'enableAjaxValidation'=>false,
		));
?>
<table style='border-collapse:collapse;' >
    <tr>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; width:300px;'>
                <b>Раздел</b>
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <b>Доступно</b>
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center; width:200px;'>
                <b>Уведомления</b>
            </td>
        </tr>
<?
$submenu = ShopCats::getSubmenu();
$notify = User::getNotifications($users);
$allowed_cats = array(0);
$allowed_cats = json_decode($users->accesss,true);
if(count($allowed_cats) == 0){
    $allowed_cats[]=0;
}

//echo "<pre>";
//print_r($allowed_cats);
//echo "</pre>";

$yarmarka_parent_cat_id = Cats::getParentCatIdByPlugin('yarmarka');
$ocenka_parent_cat_id = Cats::getParentCatIdByPlugin('ocenka');

foreach ($this->sitemenu as $item)
{

        //КАТЕГОРИИ
        $checked = '';
        if (array_key_exists($item['id'],$allowed_cats))
        {
            $checked = 'checked';
        }
        ?>
        <tr>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; width:300px;'>
                <span><?=$item['label'];?></span> &nbsp;
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='<?=$item['id'];?>' name='accesss_array[]' <?=$checked;?>  style='width:20px; height:20px;'/>
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center; '>

            </td>
        </tr>
        <?

        //////////
        //yarmarka

        if ($item['id'] == $yarmarka_parent_cat_id)
        {
        $checked = ''; if (User::checkSubCat($allowed_cats,$yarmarka_parent_cat_id,'yarmarka')){$checked = 'checked';}
        $checked2 = ''; if (in_array('yarmarka',$notify)){$checked2 = 'checked';}
        ?>
        <tr>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; width:300px;'>
                <span style='padding-left:40px; font-size:14px;'><i>Ярмарка</i></span> &nbsp;
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='yarmarka' name='accesss_array[subcats][<?=$yarmarka_parent_cat_id;?>][]'  <?=$checked;?> style='width:20px; height:20px;'/>
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='yarmarka' name='notifications[]'  <?=$checked2;?> style='width:20px; height:20px;'/>
            </td>
        </tr>
        <?
        }

        //////////
        //ocenka
        if ($item['id'] == $ocenka_parent_cat_id)
        {
        $checked = ''; if (User::checkSubCat($allowed_cats,$yarmarka_parent_cat_id,'ocenka')){$checked = 'checked';}
        $checked2 = ''; if (in_array('ocenka',$notify)){$checked2 = 'checked';}
        ?>
        <tr>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; width:300px;'>
                <span style='padding-left:40px; font-size:14px;'><i>Оценка</i></span> &nbsp;
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='ocenka' name='accesss_array[subcats][<?=$ocenka_parent_cat_id;?>][]'  <?=$checked;?> style='width:20px; height:20px;'/>
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='ocenka' name='notifications[]'  <?=$checked2;?> style='width:20px; height:20px;'/>
            </td>
        </tr>
        <?
        }


        //////////
        //Конференции
        if ($item['plugin'] == 'conf')
        {
        $cat_id = Cats::getParentCatIdByPlugin('conf');
        $checked = ''; if (User::checkSubCat($allowed_cats,$cat_id,'conf')){$checked = 'checked';}
        $checked2 = ''; if (in_array('conf',$notify)){$checked2 = 'checked';}
        ?>
        <tr>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; width:300px;'>
                <span style='padding-left:40px; font-size:14px;'><i>Конференции</i></span> &nbsp;
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='conf' name='accesss_array[subcats][<?=$cat_id;?>][]' <?=$checked;?>  style='width:20px; height:20px;'/>
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='conf' name='notifications[]' <?=$checked2;?>  style='width:20px; height:20px;'/>
            </td>
        </tr>

        <?
        $checked = ''; if (User::checkSubCat($allowed_cats,$cat_id,'festival')){$checked = 'checked';}
        $checked2 = ''; if (in_array('festival',$notify)){$checked2 = 'checked';}
        ?>
        <tr>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; width:300px;'>
                <span style='padding-left:40px; font-size:14px;'><i>Фестивали</i></span> &nbsp;
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='festival' name='accesss_array[subcats][<?=$cat_id;?>][]' <?=$checked;?>  style='width:20px; height:20px;'/>
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='festival' name='notifications[]' <?=$checked2;?>  style='width:20px; height:20px;'/>
            </td>
        </tr>

        <?
        $checked = ''; if (User::checkSubCat($allowed_cats,$cat_id,'konkurs')){$checked = 'checked';}
        $checked2 = ''; if (in_array('konkurs',$notify)){$checked2 = 'checked';}
        ?>
        <tr>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; width:300px;'>
                <span style='padding-left:40px; font-size:14px;'><i>Конкурсы</i></span> &nbsp;
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='konkurs' name='accesss_array[subcats][<?=$cat_id;?>][]' <?=$checked;?>  style='width:20px; height:20px;'/>
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='konkurs' name='notifications[]' <?=$checked2;?>  style='width:20px; height:20px;'/>
            </td>
        </tr>

        <?
        $checked = ''; if (User::checkSubCat($allowed_cats,$cat_id,'opros')){$checked = 'checked';}
        $checked2 = ''; if (in_array('opros',$notify)){$checked2 = 'checked';}
        ?>
        <tr>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; width:300px;'>
                <span style='padding-left:40px; font-size:14px;'><i>Опросы</i></span> &nbsp;
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='opros' name='accesss_array[subcats][<?=$cat_id;?>][]'  <?=$checked;?> style='width:20px; height:20px;'/>
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='opros' name='notifications[]'  <?=$checked2;?> style='width:20px; height:20px;'/>
            </td>
        </tr>
        <?
        }

        //////////
        //Конференции
        if ($item['plugin'] == 'afisha')
        {
        $cat_id = Cats::getCatIdByPlugin('afisha');
        $checked = ''; if (User::checkSubCat($allowed_cats,$cat_id,'groups')){$checked = 'checked';}
        $checked2 = ''; if (in_array('groups',$notify)){$checked2 = 'checked';}
        ?>
        <tr>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; width:300px;'>
                <span style='padding-left:40px; font-size:14px;'><i>Группы</i></span> &nbsp;
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='groups' name='accesss_array[subcats][<?=$cat_id;?>][]' <?=$checked;?> style='width:20px; height:20px;'/>
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                <input type="checkbox" value='groups' name='notifications[]' <?=$checked2;?> style='width:20px; height:20px;'/>
            </td>
            <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center; display:none;'>
                <input type="checkbox" value='something' name='notifications[]' checked style='width:20px; height:20px;'/>
            </td>
        </tr>
        <?
        }



        //////////
        //Магазин
        if ($item['plugin'] == 'shop')
        {
            $cat_id = Cats::getCatIdByPlugin('shop');
            foreach($submenu as $shop_cat_index => $shop_cat)
            {

            $checked = ''; if (User::checkSubCat($allowed_cats,$cat_id,$shop_cat_index)){$checked = 'checked';}
            $checked2 = ''; if (in_array('shop_'.$shop_cat_index,$notify)){$checked2 = 'checked';}
            ?>
            <tr>
                <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; width:300px;'>
                    <span style='padding-left:40px; font-size:14px;'><i><?=$shop_cat['title'];?></i></span> &nbsp;
                </td>
                <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                    <input type="checkbox" value='<?=$shop_cat_index;?>' name='accesss_array[subcats][<?=$cat_id;?>][]' <?=$checked;?>  style='width:20px; height:20px;'/>
                </td>
                <td style='padding-bottom:10px; padding-top:10px; border-bottom:1px dotted #ccc; text-align:center;'>
                    <input type="checkbox" value='shop_<?=$shop_cat_index;?>' name='notifications[]' <?=$checked2;?>  style='width:20px; height:20px;'/>
                </td>
            </tr>
            <?
            }
        }
        ?>

    <?php
}

?>
</table>



<div class="" style='padding-top:15px;'>
	<div>
		<?php echo CHtml::submitButton('Сохранить', array('class'=>'btn btn-success','style'=>'')); ?>
		<div style='clear:both;'></div>
	</div>
</div>
<?php $this->endWidget();?>
</div>



<div style='padding-top:30px; padding-bottom: 30px; border-bottom: 1px dotted #ccc;'>
<div>
<?php
  	  if(Yii::app()->user->hasFlash('passError')){
	  	echo '<div class="infoError">'.Yii::app()->user->getFlash('passError').'</div>';
	}

	if(Yii::app()->user->hasFlash('passSuccess')){
	  	echo '<div class="infoSuccess">'.Yii::app()->user->getFlash('passSuccess').'</div>';
	}
?>
</div>
<?
$form = $this->beginWidget('CActiveForm', array(
	    'id'=>'user-form',
	    'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('options/userpass',array('id'=>$users->id))
		));
?>

<div class="" style='padding-top:15px;'>
	<div class='title'>Новый пароль</div>
	<div class='input'>
	<?php echo $form->textField($users,'password',array('class' => 'form-control','value'=>'')); ?>
	</div>
	<div style='clear:both;'></div>
</div>
<div class="" style='padding-top:15px;'>
	<div style='width: 400px;'>
		<?php echo CHtml::submitButton('Изменить пароль', array('class'=>'btn btn-success','style'=>'')); ?>
	</div>
	<div style='clear:both;'></div>
</div>
<?php $this->endWidget();?>
</div>


<?
if ($users->role != 'admin'){
?>
<div style='padding-top:20px; padding-bottom: 40px;'>
	<?
	$form = $this->beginWidget('CActiveForm', array(
		    'id'=>'user-form',
		    'enableAjaxValidation'=>false,
			'action' => Yii::app()->createUrl('options/userdelete',array('id'=>$users->id))
			));
	?>
	<?php echo CHtml::submitButton('Удалить пользователя',array('confirm'=>'Удалить пользователя?','class'=>'btn btn-danger','style'=>'')); ?>
	<?php $this->endWidget();?>

</div>
<?
}
?>
</div>
</div>
</div>