<div style='padding:0px; padding-bottom:0px; padding-top:0px; '>

        <table style='padding:0px; margin:0px; width:100%; margin-bottom:10px;' cellpadding=0 cellspacing=0 >
				<tr>
					<td class='productLine' style='width: 20px;text-align:center;padding-right:20px'><b>x</b></td>
					<td class='productLine' colspan=2  style=' padding-right:40px; font-size:12px;'><b>Выбранные товары</b></td>
					<td class='productLine' style='width: 70px; text-align:center;  font-size:12px;'><b>Цена без доставки</b></td>
					<td class='productLine' style='width: 70px; text-align:center;  font-size:12px;'><b>Доставка</b></td>
					<td class='productLine' style='width: 70px; text-align:center;  font-size:12px;'><b>Цена с доставкой</b></td>
					<td class='productLine' style='width: 70px; text-align:center;  font-size:12px;'><b>Кол-во</b></td>
					<td class='productLine' style='width: 80px; text-align:center;  font-size:12px;'><b>Сумма</b></td>
				</tr>

		<?  $total_summ = 0;
		    $marja = 0;
			foreach ($orderItems as $index => $product)
			{

				$deleted = 0;
				$lineThrough = '';
				if ($product->deleted == 1){
		          $deleted = 1;
				  $lineThrough = 'text-decoration:line-through;';
				}
                $price = $product->contractor_price + $product->price;
				$summ = $price * $product->count;
				if ($deleted==0){
			        $total_summ = $total_summ + $summ;
					//Считаем прибыль
					$marja = 0;//$marja + (($product->price - $product->product->postavshik_price)*$product->count);
				}



				?>
					<tr class='cart-row' style='background: '>
						<td class='productLine' valign=top style='width: 20px; text-align:left;  color: ; min-height:40px; padding-right:20px; border-bottom: 1px solid #ccc; padding-top:20px; padding-bottom: 15px; <?=$lineThrough;?>'>
							<?=CHtml::checkBox('productInfo['.$product->id.'][deleted]', $product->deleted, array('class'=>'textInput delete_check', 'rel'=>$product->id));?>
						</td>
						<td class='productLine'  valign=top style='width: 50px; text-align:left;  color:; min-height:40px; padding-right:20px; border-bottom: 1px solid #ccc; padding-bottom: 15px; padding-top:20px; <?=$lineThrough;?>'>
							<div style='width:50px;height:50px; background:url(<? echo Yii::app()->baseUrl.'/files/images/product/'.$product->product->thumb['id'].'.'.$product->product->thumb['extension']; ?>) center no-repeat; background-size:cover; border: 1px solid #ccc;'></div>
						</td>
						<td class='productLine' valign=top style='<?=$lineThrough;?> text-align:left; color: #1F1F1F; min-height:40px; padding-right:40px; width:300px; border-bottom: 1px solid #ccc; padding-top:15px; padding-bottom: 15px;'>
							<?
							if ($product->products_id > 0)
							{
							?>
							<a href="#" target='_blank' onclick='logistic_show(<?=$product->id;?>); return false;'><?=$product->name; ?>, арт.: <?=$product->art; ?></a>
                            <div style='padding-top:4px; font-size:11px;'>
								Бренд: <a href="admin.php?r=brands/item&id=<?=$product->product->brand['id']; ?>" target='_blank' style='font-size:11px; text-decoration:underline;'><?=$product->product->brand['name'];?></a>&nbsp;&nbsp;
	                            <a href="admin.php?r=catalog/product&id=<?=$product->products_id; ?>" target='_blank' style='font-size:11px; text-decoration:underline;'>admin_link</a>&nbsp;&nbsp;
	                            <a href="index.php?r=site/product&id=<?=$product->products_id; ?>" target='_blank' style='font-size:11px; text-decoration:underline;' >shop_link</a>&nbsp;&nbsp;
							</div>
							<?
                            }else
							{
							?>
							<?=$product->name; ?>
							<?
                            }
							?>
						</td>


						<td class='productLine cart-cost' valign=top  style='<?=$lineThrough;?> width: 70px;  text-align:center;  min-height:40px; text-align:center; color: #A0A0A0; border-bottom: 1px solid #ccc; padding-bottom: 15px;'>
							<?
							echo CHtml::textField('productInfo['.$product->id.'][price]', $product->price, array('class'=>'project_input item_price','style'=>'background: #fff; width:90%; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin:0px auto; margin-top:20px;'));
							?>
						</td>
						<td class='productLine cart-cost' valign=top  style='<?=$lineThrough;?> width: 70px;  text-align:center;  min-height:40px; text-align:center; color: #A0A0A0; border-bottom: 1px solid #ccc; padding-bottom: 15px;'>
	  						<?
							echo CHtml::textField('productInfo['.$product->id.'][contractor_price]', $product->contractor_price, array('class'=>'project_input item_dostavka','style'=>'background: #fff; width:90%; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin:0px auto; margin-top:20px;'));
							?>
						</td>

						<td class='productLine cart-cost' valign=top  style='<?=$lineThrough;?> width: 70px;  text-align:center;  min-height:40px; text-align:center; color: #A0A0A0; border-bottom: 1px solid #ccc; padding-bottom: 15px;'>
							<div class='item_summ' style='background: #FFFFE0; width:90%; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin:0px auto; margin-top:20px;'>
							<?
							echo "&nbsp;".number_format ($price,0,'.',' ')."&nbsp;";
							?>
							</div>

						</td>
						<td class='productLine'  valign=top style='<?=$lineThrough;?> width:70px; text-align:center;  min-height:40px; text-align:center; border-bottom: 1px solid #ccc; padding-bottom: 15px;'>
							<?
							echo CHtml::textField('productInfo['.$product->id.'][count]', $product->count, array('class'=>'project_input item_count','style'=>'background: #fff; width:80%; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin:0px auto; margin-top:20px;'));
							?>
						</td>
	                	<td class="productLine cart-total-item-price"  valign=top style='border-bottom: 1px solid #ccc; padding-bottom: 15px; <?=$lineThrough;?> text-align:center; color: #25D0AE;'>
                        	<div class='item_koplate_text' style='background: #FFFFE0; width:90%; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin:0px auto; margin-top:20px; color: #222; font-weight:bold; '>
								<? echo "&nbsp;".number_format ($summ,0,'.',' '); ?>
							</div>
							<input type='hidden' value='<?=$summ;?>' class='item_koplate'>
						</td>
					</tr>

				<?php
			}
			?>
            </table>


    <?
		$total_summ = 0;
    	foreach ($orderItems as $index => $product)
		{
				if ($product->deleted == 0){
	                $price = $product->contractor_price + $product->price;
					$summ = $price * $product->count;
			        $total_summ = $total_summ + $summ;
				}
		}
	?>

	<table style='width:100%; ' class='cart'>
	    <tr>
	    	<td style='padding-right:20px; padding-bottom:6px; padding-top:6px;' valign=middle align=right>Сумма за товары</td>
	    	<td valign=middle align=right style='width:90px; padding-bottom:6px; padding-top:6px; border-bottom:1px dotted #ccc;'>
	            <div style='background: #FFFFE0; width:80px; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin-right:5px; color: #222; font-weight:bold;' class='total_summ'>
					<? echo $total_summ; ?>
				</div>
			</td>
		</tr>
		<tr>
	    	<td style='padding-right:20px; padding-bottom:6px; padding-top:6px;' valign=middle align=right>Скидка в рублях</td>
	    	<td align=right valign=middle style='width:90px; padding-bottom:6px; padding-top:6px; border-bottom:1px dotted #ccc;'><? echo CHtml::textField('skidka',$models->skidka, array('style'=>'background: #FFFFE0; width:80px; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin-right:5px; color: #222; font-weight:bold;','class'=>'form-control'));?></td>
			<td class="productLine" style='width:110px;'>&nbsp;</td>
		</tr>
		<tr>
	    	<td style='padding-right:20px; padding-bottom:6px; padding-top:6px;' valign=middle align=right>Доставка</td>
	    	<td align=right valign=middle style='width:90px; padding-bottom:6px; padding-top:6px; border-bottom:1px dotted #ccc;'><? echo CHtml::textField('contractor_price',$models->contractor_price, array('style'=>'background: #FFFFE0; width:80px; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin-right:5px; color: #222; font-weight:bold;','class'=>'form-control'));?></td>
			<td class="productLine" style='width:110px;'>&nbsp;</td>
		</tr>
		<tr>
	    	<td style='padding-right:20px; padding-bottom:6px; padding-top:6px;' valign=middle align=right>К оплате</td>
	    	<td align=right valign=middle style='width:90px; padding-bottom:6px; padding-top:6px; border-bottom:1px dotted #ccc;'>
			<div style='background: #FFFFE0; width:80px; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin-right:5px; color: #222; font-weight:bold;' class='k_oplate'>
				<? echo $total_summ + $models->contractor_price - $models->skidka; ?>
			</div>
			</td>
			<td class="productLine" style='width:110px;'>&nbsp;</td>
		</tr>
	</table>

</div>

<script>
	$(function() {
		$('.cart input').keyup(function(){
			row_recount();
	        cart_recount();
		});

		$('.cart2 input').keyup(function(){
			row_recount();
	        cart_recount();
		});
	});

function itemshow2(model_id){
  	var checked_products = new Array();
	$('.delete_check:checked').each(function(index,value){
	    checked_products.push($(this).attr('rel'));
	});


	if (checked_products.length > 1){
       change_all(checked_products);
	}

	if (checked_products.length <= 1){
       logistic_show(model_id);
	}

}


</script>


