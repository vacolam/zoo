
<div style="background: #FBFBFB; padding: 35px 0px; border: 1px solid #E0E0E0; border-top:6px solid #11a8ab;text-align: center">
<table style='width:100%;'>
	<tr>
    	<td style='border-bottom:1px solid #ccc; padding-bottom:10px; font-size:12px;' valign=top><b>Заказ</b></td>
    	<td style='border-bottom:1px solid #ccc; padding-bottom:10px;padding-left:10px; font-size:12px;' valign=top align=left><b>Дата поступления</b></td>
    	<td style='border-bottom:1px solid #ccc; padding-bottom:10px; padding-left:10px; font-size:12px;' valign=top align=left><b>Покупатель</b></td>
    	<td style='border-bottom:1px solid #ccc; padding-bottom:10px; font-size:12px;' valign=top align=left>&nbsp;</td>
    	<td style='border-bottom:1px solid #ccc; padding-bottom:10px;  padding-left:10px; font-size:12px;' valign=top align=left><b>Товары</b></td>
		<td style='border-bottom:1px solid #ccc; padding-bottom:10px; line-height:1em; font-size:11px;' valign=top><b>Ист-к<br>заказа</b></td>
    	<td style='border-bottom:1px solid #ccc; padding-bottom:10px; line-height:1em; font-size:11px;' valign=top><b>Дата<br>выдачи план</b></td>
    	<td style='border-bottom:1px solid #ccc; padding-bottom:10px; line-height:1em; font-size:11px;' valign=top><b>Дата<br>выдачи факт</b></td>
    	<td style='border-bottom:1px solid #ccc; padding-bottom:10px; text-align:left; font-size:12px;' valign=top><b>Сумма</b></td>
    	<td style='border-bottom:1px solid #ccc; padding-bottom:10px; text-align:right; font-size:12px; padding-right:15px;' valign=top><b>Статус</b></td>
	</tr>
	<?
		$date_longName = array(
		    "01" => "Январь",
		    "02" => "Февраль",
		    "03" => "Март",
		    "04" => "Апрель",
		    "05" => "Май",
		    "06" => "Июнь",
		    "07" => "Июль",
		    "08" => "Август",
		    "09" => "Сентябрь",
		    "10" => "Октябрь",
		    "11" => "Ноябрь",
		    "12" => "Декабрь"
		);

		$month = 0;
		$new_month = '';
		foreach ($models as $index => $model)
		{
			$total_summ = 0;
            foreach ($model->orders_info as $index => $order_info)
			{

				$months = explode('-',$model->date);
				$new_month = $months[1];

				if ($order_info->deleted == 0){
                    $price = $order_info->contractor_price + $order_info->price;
					$summ = $price * $order_info->count;
				}
				$total_summ = $total_summ + $summ;
			}
			$total_summ = $total_summ + $model->contractor_price - $model->skidka;

            $bg = '';
			if ($model->order_status == 1){
              $bg = '#FFFFD6';
			}


			if ($month != $new_month){
            	$month = $new_month;

         		echo 	"
						<tr>
						<td colspan=10>
							<div style='padding-top:70px; padding-bottom:20px; text-align:center; border-bottom: 2px solid #999999;'><span style='padding:6px 10px; background: #3399FF; border-radius:4px; color: #fff;'>".$date_longName[$month]."</span></div>
						</td>
						</tr>
						";
    		}
        	?>
			<tr style='background: <?=$bg;?>;'>
                    <td valign=top style='width: 50px; padding-top: 10px; padding-bottom:10px; border-bottom: 1px dotted #ccc;'><a href='admin.php?r=orders/item&id=<? echo $model->id; ?>' style=' font-size:12px;  padding-left:10px;'><? echo $model->id; ?></a></td>
                    <td valign=top align=left  style='width: 120px; padding-top: 10px; padding-bottom:10px; border-bottom: 1px dotted #ccc;'>
						<a href='admin.php?r=orders/item&id=<? echo $model->id; ?>' style=' font-size:12px;  padding-left:10px;'><? echo $this->date_makeLongStringName($model->date); ?></a>
					</td>
                    <td valign=top align=left  style='width: 120px;padding-top: 10px; padding-bottom:10px; border-bottom: 1px dotted #ccc; font-size:12px;  padding-left:10px;'>
					<div><b><? echo $model->user_name; ?></b> - <span title='Количество заказов'><?=$ordersCount[$model->id];?></span></div>
                    <div style='font-size:10px; margin-top:-2px; margin-bottom:2px;'><? echo $model->region; ?></div>
					<div style='font-size:12px; padding-top:4px;'><? echo $model->user_email; ?>, <? echo $this->phoneFormat($model->user_phone); ?></div></td>
                    <td valign=top align=left  style='width: 30px;padding-top: 20px; padding-bottom:10px; border-bottom: 1px dotted #ccc;'><span style='background: #D0D0D0; border-radius:3px; padding:8px;'><? echo count($orderQuontity[$model->id]);?></span></td>
                    <td valign=top align=left  style='padding-top: 10px; padding-left:10px; padding-bottom:10px; border-bottom: 1px dotted #ccc;'><? foreach($orderQuontity[$model->id] as $index => $value){echo "<div style='font-size:11px; padding-top;2px;'>".$value->name."</div>";} ?></td>
                    <td valign=top  style='width: 80px;padding-top: 10px; padding-bottom:10px; border-bottom: 1px dotted #ccc; font-size:11px;'>&nbsp;</td>
                    <td valign=top  style='width: 120px;padding-top: 10px; padding-bottom:10px; border-bottom: 1px dotted #ccc; font-size:11px;'><? echo $this->date_makeLongStringName($model->dostavka_date_plan); ?></td>
                    <td valign=top  style='width: 120px;padding-top: 10px; padding-bottom:10px; border-bottom: 1px dotted #ccc;  font-size:11px;'><? echo $this->date_makeLongStringName($model->dostavka_date_fact); ?></td>
                    <td valign=top  style='width: 80px;padding-top: 10px; padding-bottom:10px; border-bottom: 1px dotted #ccc; text-align:left;  font-size:12px;'><?=number_format ($total_summ,0,'.',' ');?> руб.</td>
                    <td valign=top  style='width: 180px; text-align:right;padding-top: 20px; padding-bottom:10px; border-bottom: 1px dotted #ccc; padding-right:15px;'><span style='padding:7px 12px; font-size:12px; background: <? echo Yii::app()->params->orders_colors[$model->order_status]; ?>; color: #fff; border-radius:4px;'><? echo $status[$model->order_status]; ?></span></td>
			</tr>
			<?
		}
	?>
</table>
</div>