<?
$this->pageTitle = 'Заказ №'.$models->id." в магазине ИдиЛесом.com";
?>
<div style='padding-bottom:15px;'>
<a href="admin.php?r=orders/index" style=''>&laquo;&nbsp;Назад в Список Заказов</a>
</div>

<div style='background: #FBFBFB; border: 1px solid #E0E0E0;'>
<form action='admin.php?r=orders/update&id=<?=$models->id;?>' method='post'>
<table style='width:100%;'>
<tr>
<td valign=top>
<div style='width:100%; height:60px; background:#526f87; line-height:60px; padding-left: 35px; color: #fff; position:relative; '>
	Заказ <span style='color: #fff;'>№ <?=$models->id;?></span>&nbsp;&nbsp;от&nbsp;&nbsp;<span style='color: #fff;'><? echo $this->date_makeLongStringName($models->date);?></span>
</div>

<div style=''>
<div style='padding:30px; padding-top:0px; min-height:500px; margin-bottom: 50px;'>

	<div class="cart projects_orderTab active" rel='1' style='padding-top:30px;'>
	<?
		if (empty($orderItems))
		{
		?>
			<div style='height:200px; line-height:200px; width:200px; border-radius:150px; background: #F0F0F0; text-align:center; font-size:16px; color: #707070; margin:0px auto; margin-top: 80px; '>ПУСТОТА</div>
		<?php
		}
		else
		{
		?>
			<div>
				<?
				$this->renderPartial('_order', array(
					'models' 		=>	$models,
					'orderItems' 	=>	$orderItems,
					'contractors'	=>	$contractors,
					'ProductsArr'	=>	$ProductsArr,
				));
				?>
			</div>
		<?php
		}
	?>
	</div>

</div>
</div>


</td>
<td valign=top style='width:230px; background: #505050;' >
      <?
      	$feedback_type = array();
      	$feedback_type['phone'] = 'Электронная почта и телефон';
      	$feedback_type['email'] = 'Только электронная почта';

	  ?>
	<div style='padding:20px; padding-bottom:20px; padding-top:10px; border-bottom: 1px solid #2E2E2E;'>

		<div align=center valign=middle style='padding-bottom:4px; border-bottom: 1px dotted #EDEDED; line-height:1.1em; font-size:13px; color: #F7F7F7; margin-bottom:4px; margin-top:10px;'><b>Данные покупателя в заявке</b></div>
		<div style='color: #E3E3E3; font-size:11px;'>
        	<ul style='margin-left:30px; list-style: disc; line-height:1.2em;'>
            	<li><?=$models->user_name;?></li>
            	<li><?=$this->phoneFormat($models->user_phone);?></li>
            	<li><?=$models->user_email;?></li>
            	<li>Способ связи: <?=$feedback_type[$models->feedback_type];?></li>
			</ul>
		</div>
		<div align=left valign=middle style=' line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; margin-top:15px;'>Адрес доставки</div>
		<div style='width:100%; height:28px; position:relative;'>
		   			<?
					echo CHtml::textField('user_adress', $models->user_adress, array('class'=>'form-control','style'=>'width: 197px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>
		 <div style='margin-top:5px; color: #fff; font-size:11px;'>
		 			Другие заказы (<?=count($orders_from_this_person);?>):&nbsp;&nbsp;
					<?
					if ($orders_from_this_person)
					{
						foreach($orders_from_this_person as $index => $order){
                            echo "<a href='admin.php?r=orders/item&id=".$order->id."' style='color: #fff; text-decoration:underline; font-size:11px;'>".$order->id."</a>, ";
						}
					}
					?>
		</div>
		</div>


       	<div style='padding:20px; padding-top:20px; border-top: 1px solid #606060;  border-bottom: 1px solid #2E2E2E;'>
    	<div align=left valign=middle style=' line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; '>Статус заказа</div>
		<div style='width:100%; height:28px; position:relative;'>
		   			<?
						echo CHtml::dropDownList('order_status', $models->order_status, $status, array('class'=>'project_dropDownList print_type selectpicker','data-width'=>'200px','style'=>'width: 200px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>

		<div align=left valign=middle style=' line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; margin-top:13px;'>Тип оплаты</div>
		<div style='width:100%; height:28px; position:relative;'>
		   			<?
					$payment_type = Yii::app()->params['payment_type'];
					echo CHtml::dropDownList('payment_type', $models->payment_type, $payment_type, array('class'=>'project_dropDownList print_type selectpicker','data-width'=>'200px','style'=>'width: 200px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>

    	<div align=left valign=middle style=' line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; margin-top:15px;'>Информация об оплате</div>
		<div style='width:100%; height:28px; position:relative;'>
		   		   <?
					echo CHtml::textField( 'payment_info', $models->payment_info,  array('class'=>'form-control','style'=>'width: 197px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>

		</div>

		<div style='padding:20px; padding-bottom:25px; border-top: 1px solid #606060;'>
		<div align=left style='width: 140px; line-height:1.1em; font-size:13px; color: #A8A8A8;margin-bottom:4px;'>Обещаем сдать заказ:</div>
		<div style='width:100%;height:28px; position:relative;'>
              	<input type="text" id='dateButton2' style='width: 197px; font-size:13px; border: 1px solid #394E60; height: 28px; background:#FFFFCC;' class='form-control datepicker' value='<? echo $this->date_makeLongStringName($models->dostavka_date_plan); ?>' />
               	<div style='width:10px;height:10px; position:absolute; top:12px; right:5px; background: url(css_tool/down.gif) no-repeat; z-index:888;'></div>
				<? echo CHtml::hiddenField('dostavka_date_plan', $models->dostavka_date_plan, array('style'=>'width: 100%; font-size:13px; border: 1px solid #394E60; height: 28px; ','class'=>'form-control'));?>
        </div>

		<div align=left style='line-height:1.1em; font-size:13px; color: #A8A8A8;margin-bottom:4px; margin-top:6px;'>Фактическая дата сдачи:</div>
		<div style='width:100%;height:28px; position:relative;'>
              	<input type="text" id='dateButton' style='width: 197px; font-size:13px; border: 1px solid #394E60; height: 28px; background:#FFFFCC;' class='form-control datepicker' value='<? echo $this->date_makeLongStringName($models->dostavka_date_fact); ?>' />
               	<div style='width:10px;height:10px; position:absolute; top:12px; right:5px; background: url(css_tool/down.gif) no-repeat; z-index:888;'></div>
				<? echo CHtml::hiddenField('dostavka_date_fact', $models->dostavka_date_fact, array('style'=>'width: 100%; font-size:13px; border: 1px solid #394E60; height: 28px; ','class'=>'form-control'));?>
        </div>
	</div>
</td>
</tr>
</table>

<div style='padding-top:10px; padding-bottom:20px; border-top: 1px solid #ccc; background: url(css_tool/noise-f1-34223af06da6ee10a0b27723268a1f95.gif)'>
	<input type="submit" class='button green' style='float:right; margin-right:20px; width: 220px;' value='Сохранить'/>

	<div style='float:left; margin-left:30px; width: 200px;'>
       	<a href="index.php?r=order/index&order_id=<?=$models->hash;?>" class='button orange' target='_blank' style='text-align:center; width:200px;'>Страница заказа&nbsp;&raquo;</a>
	</div>
	<div style='clear:both;'></div>
</div>

</form>
</div>

<script>
function cart_recount(){
	var skidka_summ = strToNum($('#skidka').val());
    var dostavka = strToNum($('#contractor_price').val());

	var total_summ = 0;
	var summ = 0;

	$('.cart-row').each(function(){
		var item_itog = strToNum($(this).find('.item_koplate').val());
	    	summ = summ + item_itog;
	});

	total_summ = summ - skidka_summ + dostavka;
	$('.k_oplate').text(total_summ);
}

function row_recount(){
	var total_summ = 0;
	$('.cart-row')
	.each(function(){
		var price = strToNum($(this).find('.item_price').val());
		var dostavka = strToNum($(this).find('.item_dostavka').val());
		var count = strToNum($(this).find('.item_count').val());

		$(this).find('.item_summ').text((price+dostavka).toFixed(0));

		var item_itog = ((price+dostavka)*count).toFixed(0);
            $(this).find('.item_koplate_text').text(item_itog);
            $(this).find('.item_koplate').val(item_itog);
			total_summ = total_summ + strToNum(item_itog);
	});

    $('.total_summ').text(total_summ);
}

function skidka_recount(){
   	var skidka = strToNum($('#sale_id option:selected').attr('rel'));
   		$('.skidka_procent').text(skidka);
	var summ = 0;
   	$('.cart-row').each(function(){
		var item_itog = strToNum($(this).find('.item_koplate').val());
	    	summ = summ + item_itog;
	});
   	var skidka_summ = (summ/100*skidka).toFixed(0);
   	$('#skidka').val(skidka_summ);
}

		$( "#dateButton" ).datepicker(
        {
        	altField: "#dostavka_date_fact",
      		altFormat: "yy-mm-dd"
		});

		$( "#dateButton2" ).datepicker(
        {
        	altField: "#dostavka_date_plan",
      		altFormat: "yy-mm-dd"
		});

		$.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&laquo;',
                nextText: '&raquo;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd MM yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['ru']);

</script>

<script>
function logistic_show(id){
    showLoader();
	$.ajax({
		type: "POST",
		url: "/admin.php?r=logistic/item&controller=orders&order_info="+id,
		dataType: 'json',
		data: {
			features: new Array(),
			},
		success: function(response){
			hideLoader();
			popupShow(response.form,response.title,980);
		},
		error: function(response){
			setTimeout(function(){
				$('#searchResultMessage').fadeOut(500);
			},1000);
		}
	});

}

function change_all(checked_products){
	if (!checked_products){
		var checked_products = new Array();
		$('.order_checked:checked').each(function(index,value){
		        checked_products.push($(this).val());
		});
	}

    //Обновляем информацию о проекте
	showLoader();

	$.ajax({
		type: "GET",
		url: "/admin.php?r=logistic/ItemsArray&controller=orders",
		dataType: 'json',
		data: {
				'checked_products': checked_products,
				},
		success: function(response){
			hideLoader();
			popupShow(response.form,response.title,980);
		},
		error: function(response){
			setTimeout(function(){
				$('#searchResultMessage').fadeOut(500);
			},1000);
		}
	});

}

function order_tabs(tabId){
	$('.project_menu li').removeClass('active');
	$('.project_menu li[rel="'+tabId+'"]').addClass('active');

	$('.projects_orderTab').removeClass('active');
	$('.projects_orderTab[rel="'+tabId+'"]').addClass('active');
}

function docsCreate(id){
      			showLoader();
				$.ajax({
					type: "GET",
					url: "/admin.php?r=docs/create&id="+id+"&controller=projects",
					dataType: 'json',
					data: {
						features: new Array(),
 					},
					success: function(response){
                    	hideLoader();
						popupShow(response.form,response.title);
					},
					error: function(response){
						setTimeout(function(){
						},1000);
					}
				});
	}
</script>