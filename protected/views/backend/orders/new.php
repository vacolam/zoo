<?
$this->pageTitle = "Новый заказ";
?>
<div style='padding-bottom:15px;'>
<a href="admin.php?r=orders/index" style=''>&laquo;&nbsp;Назад в Список Заказов</a>
</div>

<div style='background: #FBFBFB; border: 1px solid #E0E0E0;'>
<form action='admin.php?r=orders/add' method='post'>


<table style='width:100%;'>
<tr>
<td valign=top>
<div style='width:100%; height:60px; background:#526f87; line-height:60px; padding-left: 35px; color: #fff; position:relative; '>
	Новый заказ
</div>
<div style=''>
<div style='padding:30px; padding-top:0px; min-height:500px; margin-bottom: 50px;'>

<div class='cart'>
		<table style='width:100%; margin-top:30px; margin-bottom:10px;' class='newProducts'>
				<tr>
					<td class='' style='text-align:left; font-size:11px;  padding-bottom: 15px; border-bottom: 1px solid #ccc; '><b>ID</b></td>
					<td class='productLine' style='font-size:11px; border-bottom: 1px solid #ccc; padding-bottom: 15px; '><b>Название</b></td>
					<td class='productLine' style='font-size:11px;  text-align:center; border-bottom: 1px solid #ccc; padding-bottom: 15px; '><b>Склад</b></td>
					<td class='productLine' style='font-size:11px;  text-align:center; border-bottom: 1px solid #ccc; padding-bottom: 15px; '><b>Цена без доставки</b></td>
					<td class='productLine' style='font-size:11px;  text-align:center; border-bottom: 1px solid #ccc; padding-bottom: 15px; '><b>Доставка</b></td>
					<td class='productLine' style='font-size:11px;  text-align:center; border-bottom: 1px solid #ccc; padding-bottom: 15px; '><b>Цена с доставкой</b></td>
					<td class='productLine' style='font-size:11px;  text-align:center; border-bottom: 1px solid #ccc; padding-bottom: 15px; '><b>Кол-во</b></td>
					<td class='productLine' style='font-size:11px;  text-align:center; border-bottom: 1px solid #ccc; padding-bottom: 15px; '><b>Сумма</b></td>
				</tr>
            <?
			for($i = 1; $i <= 3; $i++)
			{
			?>
            <tr class='cart-row' style='background: <?=$bg;?>;'>
						<td class='productLine' style='width: 50px; text-align:left; vertical-align: middle; color: <?=$color;?>; min-height:40px; padding-right:20px; border-bottom: 1px solid #ccc; padding-bottom: 15px; '>
							<div style='padding-top:20px;'>
								<select class='btn-info form-control  selectpicker' name='NewProduct[<?=$i;?>][products_id]' data-width='150' data-live-search='true'  style='background: #fff; width:150px; height:30px; line-height:30px; border: 1px solid #ccc; text-align:left; font-size:12px; padding:0px; margin:0px auto; margin-top:20px;'>
									<option value='0'>Выберите товар</option>
									<option data-divider="true"></option>
									<?
					                foreach($ProductsArr as $index => $value)
									{
					 	            	echo "<option value='".$index."'>".$value."</option>";
									}
									?>
								</select>
							</div>
						</td>
						<td class='productLine' style='text-align:left; vertical-align: middle; color: #1F1F1F; min-height:40px; padding-right:20px; border-bottom: 1px solid #ccc; padding-bottom: 15px;'>
							<?
							echo CHtml::textField('NewProduct['.$i.'][name]', '', array('class'=>'project_input','style'=>'background: #fff; width:100%; min-width:200px; height:30px; line-height:30px; border: 1px solid #ccc; text-align:left; font-size:12px; padding:0px; margin:0px auto; margin-top:20px; padding-left:10px;'));
							?>
						</td>

						<td class='productLine cart-cost'  style='width: 120px;  text-align:center; vertical-align: middle; min-height:40px; text-align:center; color: #A0A0A0; border-bottom: 1px solid #ccc; padding-bottom: 15px;'>
							<?
							echo CHtml::dropDownList('NewProduct['.$i.'][contractor_company]', '', $contractors, array('class'=>'project_dropDownList print_type','style'=>'background: #fff; width:90%; height:30px; line-height:30px; border: 1px solid #ccc; text-align:left; font-size:12px; padding:0px; margin:0px auto; margin-top:20px;'));
							?>
						</td>

						<td class='productLine cart-cost'  style=' width: 70px;  text-align:center; vertical-align: middle; min-height:40px; text-align:center; color: #A0A0A0; border-bottom: 1px solid #ccc; padding-bottom: 15px;'>
							<?
							echo CHtml::textField('NewProduct['.$i.'][price]', '', array('class'=>'project_input item_price','style'=>'background: #fff; width:90%; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin:0px auto; margin-top:20px;'));
							?>
						</td>
						<td class='productLine cart-cost'  style=' width: 70px;  text-align:center; vertical-align: middle; min-height:40px; text-align:center; color: #A0A0A0; border-bottom: 1px solid #ccc; padding-bottom: 15px;'>
	  						<?
							echo CHtml::textField('NewProduct['.$i.'][contractor_price]', '', array('class'=>'project_input item_dostavka','style'=>'background: #fff; width:90%; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin:0px auto; margin-top:20px;'));
							?>
						</td>

						<td class='productLine cart-cost'  style=' width: 70px;  text-align:center; vertical-align: middle; min-height:40px; text-align:center; color: #A0A0A0; border-bottom: 1px solid #ccc; padding-bottom: 15px;'>
							<div class='item_summ' style='background: #FFFFE0; width:90%; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin:0px auto; margin-top:20px;'>
							<?

							?>
							</div>

						</td>
						<td class='productLine' style='width:70px; text-align:center; vertical-align: middle; min-height:40px; text-align:center; border-bottom: 1px solid #ccc; padding-bottom: 15px;'>
							<?
							echo CHtml::textField('NewProduct['.$i.'][count]', '', array('class'=>'project_input item_count','style'=>'background: #fff; width:80%; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin:0px auto; margin-top:20px;'));
							?>
						</td>
	                	<td class="productLine cart-total-item-price" style='width:80px; border-bottom: 1px solid #ccc; padding-bottom: 15px; <?=$lineThrough;?> text-align:center; color: #25D0AE;'>
                        	<div class='item_koplate_text' style='background: #FFFFE0; width:90%; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin:0px auto; margin-top:20px; color: #222; font-weight:bold; '>
                              &nbsp;
							</div>
							<input type='hidden' value='' class='item_koplate'>
						</td>
					</tr>
					<?
                    }
					?>
            </table>
</div>

<div class='cart2'>
	<table style='width:100%;' class='cart'>
	    <tr>
	    	<td style='padding-right:20px; padding-bottom:6px; padding-top:6px;' valign=middle align=right>Сумма за товары</td>
	    	<td valign=middle align=right style='width:90px; padding-bottom:6px; padding-top:6px; border-bottom:1px dotted #ccc;'>
	            <div style='background: #FFFFE0; width:80px; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin-right:5px; color: #222; font-weight:bold;' class='total_summ'>
					&nbsp;
				</div>
			</td>
		</tr>
		<tr>
	    	<td style='padding-right:20px; padding-bottom:6px; padding-top:6px;' valign=middle align=right>
				<select class='selectpicker' name="sale_id" id="sale_id" data-width='150' data-live-search='true'  style='width: 150px; font-size:13px; height: 28px; border:none;'>
	                   <?
					$sale_name_array = $SaleArr['name'];
					$sale_discount_array = $SaleArr['discount'];
	                foreach($sale_name_array as $index => $value)
					{
		            	echo "<option value='".$index."' rel='".$sale_discount_array[$index]."' >".$value." - ".$sale_discount_array[$index]."%</option>";
					}
					?>
				</select>
				<script>
	               	$('#sale_id').on('change',function(){
						skidka_recount();
	                    cart_recount();
	               	});
				</script>
			</td>
	    	<td align=right valign=middle style='width:90px; padding-bottom:6px; padding-top:6px; border-bottom:1px dotted #ccc;'>
				<div style='background: #FFFFE0; width:80px; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin-right:5px; color: #222; font-weight:bold;' class='skidka_procent'>&nbsp;</div>
			</td>
		</tr>
		<tr>
	    	<td style='padding-right:20px; padding-bottom:6px; padding-top:6px;' valign=middle align=right>Скидка в рублях</td>
	    	<td align=right valign=middle style='width:90px; padding-bottom:6px; padding-top:6px; border-bottom:1px dotted #ccc;'><? echo CHtml::textField('skidka','', array('style'=>'background: #FFFFE0; width:80px; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin-right:5px; color: #222; font-weight:bold;','class'=>'form-control'));?></td>
		</tr>
		<tr>
	    	<td style='padding-right:20px; padding-bottom:6px; padding-top:6px;' valign=middle align=right>Доставка</td>
	    	<td align=right valign=middle style='width:90px; padding-bottom:6px; padding-top:6px; border-bottom:1px dotted #ccc;'><? echo CHtml::textField('contractor_price','', array('style'=>'background: #FFFFE0; width:80px; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin-right:5px; color: #222; font-weight:bold;','class'=>'form-control'));?></td>
		</tr>
		<tr>
	    	<td style='padding-right:20px; padding-bottom:6px; padding-top:6px;' valign=middle align=right>К оплате</td>
	    	<td align=right valign=middle style='width:90px; padding-bottom:6px; padding-top:6px; border-bottom:1px dotted #ccc;'>
			<div style='background: #FFFFE0; width:80px; height:30px; line-height:30px; border: 1px solid #ccc; text-align:center; font-size:12px; padding:0px; margin-right:5px; color: #222; font-weight:bold;' class='k_oplate'>
				&nbsp;
			</div></td>
		</tr>
	</table>

</div>

<script>
	$(function() {
		$('.cart input').keyup(function(){
			row_recount();
	        cart_recount();
		});

		$('.cart2 input').keyup(function(){
			row_recount();
	        cart_recount();
		});
	});


</script>



<div style='padding-top:20px; margin-top:20px; border-top: 1px solid #ccc;'>
	<input type="submit" value='Создать заказ' style='float:right;' class='button green'/>
	<div style='clear:both;'></div>
</div>
</div>
</div>


</td>
<td valign=top style='width:230px; background: #505050;' >
    <div style='padding:20px; padding-top:10px;'>


		<div align=left valign=middle style=' line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; margin-top:10px;'>Статуст заказа</div>
		<div style='width:100%; height:28px; position:relative;'>
		   			<?
						echo CHtml::dropDownList('order_status', '', $status, array('class'=>'project_dropDownList print_type selectpicker','data-width'=>'200px','style'=>'width: 200px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>

		<div align=left valign=middle style=' line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; margin-top:13px;'>Тип оплаты</div>
		<div style='width:100%; height:28px; position:relative;'>
		   			<?
					$payment_type = Yii::app()->params['payment_type'];
					echo CHtml::dropDownList('payment_type', '', $payment_type, array('class'=>'project_dropDownList print_type','style'=>'width: 197px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>

    	<div align=left valign=middle style=' line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; margin-top:10px;'>Информация об оплате</div>
		<div style='width:100%; height:28px; position:relative;'>
		   		   <?
					echo CHtml::textField( 'payment_info', '',  array('class'=>'form-control','style'=>'width: 197px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>

		<div align=left style='width: 140px; line-height:1.1em; font-size:13px; color: #A8A8A8;margin-bottom:4px; margin-top:25px;'>Обещаем сдать заказ:</div>
		<div style='width:100%;height:28px; position:relative;'>
              	<input type="text" id='dateButton2' style='width: 197px; font-size:13px; border: 1px solid #394E60; height: 28px; background:#FFFFCC;' class='form-control datepicker' value='<? echo $this->date_makeLongStringName($models->dostavka_date_plan); ?>' />
               	<div style='width:10px;height:10px; position:absolute; top:12px; right:5px; background: url(css_tool/down.gif) no-repeat; z-index:888;'></div>
				<? echo CHtml::hiddenField('dostavka_date_plan', '', array('style'=>'width: 100%; font-size:13px; border: 1px solid #394E60; height: 28px; ','class'=>'form-control'));?>
        </div>

		<div align=left style='line-height:1.1em; font-size:13px; color: #A8A8A8;margin-bottom:4px; margin-top:6px;'>Фактическая дата сдачи:</div>
		<div style='width:100%;height:28px; position:relative;'>
              	<input type="text" id='dateButton' style='width: 197px; font-size:13px; border: 1px solid #394E60; height: 28px; background:#FFFFCC;' class='form-control datepicker' value='<? echo $this->date_makeLongStringName($models->dostavka_date_fact); ?>' />
               	<div style='width:10px;height:10px; position:absolute; top:12px; right:5px; background: url(css_tool/down.gif) no-repeat; z-index:888;'></div>
				<? echo CHtml::hiddenField('dostavka_date_fact', '', array('style'=>'width: 100%; font-size:13px; border: 1px solid #394E60; height: 28px; ','class'=>'form-control'));?>
        </div>

		<Div style='clear:both; width: 200px; margin-top:35px;'>
			<div style='width:200px; float:left; margin-left:0px; margin-top:-2px;'>
				<select class='btn-info form-control  selectpicker' name="clients_id" id="clients_id" data-style='btn-info' data-width='200' data-live-search='true'  style='height:34px; font-size:14px; color: #fff; text-align:left; width:200px; background: #5bc0de; border:none; padding-left:9px;'>
					<option value='0'>Выберите клиента</option>
					<option data-divider="true"></option>
					<?
	                foreach($ClientsArr as $index => $value)
					{
		            	echo "<option value='".$index."'>".$value."</option>";
					}
					?>
				</select>
			</div>
			<div style='width:20px; margin-left:3px; margin-top:-2px; float:left; display:none;' class='clients_addButton' onclick='clientsCreate()'>+</div>
			<div style='clear:both;'></div>
		</Div>

      	<div align=left valign=middle style='width: 120px; line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; margin-top:10px;'>Имя покупателя</div>
		<div style='width:100%; height:28px; position:relative;'>
		   			<?
					echo CHtml::textField('user_name', '', array('class'=>'form-control','style'=>'width: 197px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>

		<div align=left valign=middle style=' line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; margin-top:10px;'>Телефон покупателя</div>
		<div style='width:100%; height:28px; position:relative;'>
		   			<?
					echo CHtml::textField('user_phone', '', array('class'=>'form-control','style'=>'width: 197px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>

		<div align=left valign=middle style=' line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; margin-top:10px;'>E-mail покупателя</div>
		<div style='width:100%; height:28px; position:relative;'>
		   			<?
					echo CHtml::textField('user_email', '', array('class'=>'form-control','style'=>'width: 197px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>

		<div align=left valign=middle style=' line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; margin-top:10px;'>Адрес доставки</div>
		<div style='width:100%; height:28px; position:relative;'>
		   			<?
					echo CHtml::textField('user_adress', '', array('class'=>'form-control','style'=>'width: 197px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>

		<div align=left valign=middle style=' line-height:1.1em; font-size:13px; color: #A8A8A8; margin-bottom:4px; margin-top:10px;'>Срок доставки (в заказе)</div>
		<div style='width:100%; height:28px; position:relative;'>
		   			<?
					echo CHtml::textField('srok', '', array('class'=>'form-control','style'=>'width: 197px; font-size:13px; height: 28px; border:none;'));
					?>
		</div>
	</div>
</td>
</tr>
</table>
</form>
</div>

<script>
function cart_recount(){
	var skidka_summ = strToNum($('#skidka').val());
    var dostavka = strToNum($('#contractor_price').val());

	var total_summ = 0;
	var summ = 0;

	$('.cart-row').each(function(){
		var item_itog = strToNum($(this).find('.item_koplate').val());
	    	summ = summ + item_itog;
	});

	total_summ = summ - skidka_summ + dostavka;
	$('.k_oplate').text(total_summ);
}

function row_recount(){
	var total_summ = 0;
	$('.cart-row')
	.each(function(){
		var price = strToNum($(this).find('.item_price').val());
		var dostavka = strToNum($(this).find('.item_dostavka').val());
		var count = strToNum($(this).find('.item_count').val());

		$(this).find('.item_summ').text((price+dostavka).toFixed(0));

		var item_itog = ((price+dostavka)*count).toFixed(0);
            $(this).find('.item_koplate_text').text(item_itog);
            $(this).find('.item_koplate').val(item_itog);
			total_summ = total_summ + strToNum(item_itog);
	});

    $('.total_summ').text(total_summ);
}

function skidka_recount(){
   	var skidka = strToNum($('#sale_id option:selected').attr('rel'));
   		$('.skidka_procent').text(skidka);
	var summ = 0;
   	$('.cart-row').each(function(){
		var item_itog = strToNum($(this).find('.item_koplate').val());
	    	summ = summ + item_itog;
	});
   	var skidka_summ = (summ/100*skidka).toFixed(0);
   	$('#skidka').val(skidka_summ);
}

		$( "#dateButton" ).datepicker(
        {
        	altField: "#dostavka_date_fact",
      		altFormat: "yy-mm-dd"
		});

		$( "#dateButton2" ).datepicker(
        {
        	altField: "#dostavka_date_plan",
      		altFormat: "yy-mm-dd"
		});

		$.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&laquo;',
                nextText: '&raquo;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd MM yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['ru']);

</script>