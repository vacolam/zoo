<?php
Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget');

?>
<h1><?= $model->isNewRecord ? 'Добавление информации' : 'Редактирование информации'; ?></h1>
<div class="form" style='padding-top:50px;'>
<?php echo CHtml::beginForm(); ?>


<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <div class="form-group<?= $model->hasErrors('title') ? ' has-error' : ''; ?>">

        <?
        if ($this->route == 'otherinfo/create')
        {
            ?>
            <?= CHtml::activeLabel($model, 'title', array('class' => 'control-label')); ?>
            <?= CHtml::activeTextField($model, 'title', array('class' => 'form-control')); ?>
            <?= CHtml::error($model, 'title', array('class' => 'help-block')); ?>
            <?
        }else{
            echo "<b>".$model->title."</b>";
        }
        ?>
    </div>

    <div class="form-group<?= $model->hasErrors('link') ? ' has-error' : ''; ?>">
        <?
        if ($this->route == 'otherinfo/create')
        {
            ?>
            <?= CHtml::activeLabel($model, 'link', array('class' => 'control-label')); ?>
            <?
        }
        ?>
        <?= CHtml::activeTextArea($model, 'link', array('class' => 'form-control', 'style'=>'height:300px;')); ?>
        <?= CHtml::error($model, 'link', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group">
        <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-default')); ?>
        <?= CHtml::link('Назад', array('index'), array('class' => 'btn btn-default')); ?>
    </div>
    
<?php echo CHtml::endForm(); ?>
</div>
