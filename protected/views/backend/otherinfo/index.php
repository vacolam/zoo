<h1>Время работы, стоимость билетов, контакты</h1>
<?//= CHtml::link('Добавить информацию', array('create'), array('class' => 'btn btn-default')); ?>

<style>
a {
    color:#222;
}
</style>

<div style='padding-top:50px;'>
<?php
$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$provider,
'itemView'=>'_list_item',
'template'=>"\n{items}\n{pager}",
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/pages.css', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));
?>
</div>