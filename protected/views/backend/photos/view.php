<script src="/js/cropperjs/dist/cropper.js"></script><!-- Cropper.js is required -->
<link  href="/js/cropperjs/dist/cropper.css" rel="stylesheet">
<script src="/js/cropper-jquery/dist/jquery-cropper.min.js"></script>

<style>
.content_one a {
    color:#222;
}
.content_one a:hover {
    color:#0099CC;
}

img {
  max-width: 100%; /* This rule is very important, please do not ignore this! */
}
</style>

<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; '>
                            <a href="<?= Yii::app()->createUrl('home/index',array()); ?>" style='font-size:40px; color:#000;'>Редактор фотографии</a>
                        </div>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'><a href="<?=Yii::app()->createUrl('bannersbottom/index',array());?>"></a></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>


<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link <? if ($crop_type=='image'){echo "active";}?>" href="<?=Yii::app()->createUrl('photos/view',array('id'=>$photo->id,'crop_type'=>'image'));?>">Редактировать фото и превью</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <? if ($crop_type=='preview'){echo "active";}?>" href="<?=Yii::app()->createUrl('photos/view',array('id'=>$photo->id,'crop_type'=>'preview'));?>">Редактировать только превью</a>
  </li>
  <li style='clear:both;'>

  </li>
</ul>


<?php
if ($photo)
{
    $img = $photo->getImageUrl();
    if($crop_type=='preview'){
    $img = $photo->getThumbnailUrl();
    }
    ?>
    <div style='position:relative; margin-top:50px;'>
        <img src='<?=$img;?>'  style='width:auto; max-width:900px; margin:0px auto;' id="image"/>
        <div class='modalSaving' style='display:none; width:200px; height:60px; background:rgba(0,0,0,.8); position:absolute; top:50%; left:50%; margin-left:-100px; margin-top:-30px; border-radius:5px;text-align:center; line-height:60px; color:#fff;'>Сохранение...</div>
    </div>
    <?
}
?>
<div class="col-md-3" style='display:none;'>
   <!-- <h3>Preview:</h3> -->
   <div class="docs-preview clearfix">
     <div class="img-preview preview-lg"></div>
     <div class="img-preview preview-md"></div>
     <div class="img-preview preview-sm"></div>
     <div class="img-preview preview-xs"></div>
   </div>

   <!-- <h3>Data:</h3> -->
   <div class="docs-data">
     <div class="input-group input-group-sm">
       <span class="input-group-prepend">
         <label class="input-group-text" for="dataX">X</label>
       </span>
       <input type="text" class="form-control" id="dataX" placeholder="x">
       <span class="input-group-append">
         <span class="input-group-text">px</span>
       </span>
     </div>
     <div class="input-group input-group-sm">
       <span class="input-group-prepend">
         <label class="input-group-text" for="dataY">Y</label>
       </span>
       <input type="text" class="form-control" id="dataY" placeholder="y">
       <span class="input-group-append">
         <span class="input-group-text">px</span>
       </span>
     </div>
     <div class="input-group input-group-sm">
       <span class="input-group-prepend">
         <label class="input-group-text" for="dataWidth">Width</label>
       </span>
       <input type="text" class="form-control" id="dataWidth" placeholder="width">
       <span class="input-group-append">
         <span class="input-group-text">px</span>
       </span>
     </div>
     <div class="input-group input-group-sm">
       <span class="input-group-prepend">
         <label class="input-group-text" for="dataHeight">Height</label>
       </span>
       <input type="text" class="form-control" id="dataHeight" placeholder="height">
       <span class="input-group-append">
         <span class="input-group-text">px</span>
       </span>
     </div>
     <div class="input-group input-group-sm">
       <span class="input-group-prepend">
         <label class="input-group-text" for="dataRotate">Rotate</label>
       </span>
       <input type="text" class="form-control" id="dataRotate" placeholder="rotate">
       <span class="input-group-append">
         <span class="input-group-text">deg</span>
       </span>
     </div>
     <div class="input-group input-group-sm">
       <span class="input-group-prepend">
         <label class="input-group-text" for="dataScaleX">ScaleX</label>
       </span>
       <input type="text" class="form-control" id="dataScaleX" placeholder="scaleX">
     </div>
     <div class="input-group input-group-sm">
       <span class="input-group-prepend">
         <label class="input-group-text" for="dataScaleY">ScaleY</label>
       </span>
       <input type="text" class="form-control" id="dataScaleY" placeholder="scaleY">
     </div>
   </div>
   </div>


<div class="row" style='padding-top:10px;'>
  <div class="col-md-12 docs-buttons">
    <!-- <h3>Toolbar:</h3> -->

    <div class="btn-group">
      <button type="button" class="btn btn-primary" data-method="rotate" data-option="-90"  style='height:40px;'>
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Повернуть влево" style='height:40px;'>
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-counterclockwise" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M12.83 6.706a5 5 0 0 0-7.103-3.16.5.5 0 1 1-.454-.892A6 6 0 1 1 2.545 5.5a.5.5 0 1 1 .91.417 5 5 0 1 0 9.375.789z"/>
                <path fill-rule="evenodd" d="M7.854.146a.5.5 0 0 0-.708 0l-2.5 2.5a.5.5 0 0 0 0 .708l2.5 2.5a.5.5 0 1 0 .708-.708L5.707 3 7.854.854a.5.5 0 0 0 0-.708z"/>
              </svg>
        </span>
      </button>
      <button type="button" class="btn btn-primary" data-method="rotate" data-option="90"  style='height:40px;'>
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Повернуть вправо" style='height:40px;'>
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-clockwise" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M3.17 6.706a5 5 0 0 1 7.103-3.16.5.5 0 1 0 .454-.892A6 6 0 1 0 13.455 5.5a.5.5 0 0 0-.91.417 5 5 0 1 1-9.375.789z"/>
                <path fill-rule="evenodd" d="M8.147.146a.5.5 0 0 1 .707 0l2.5 2.5a.5.5 0 0 1 0 .708l-2.5 2.5a.5.5 0 1 1-.707-.708L10.293 3 8.147.854a.5.5 0 0 1 0-.708z"/>
              </svg>
        </span>
      </button>
    </div>

    <div class="btn-group">
      <button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1"  style='height:40px;'>
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Отразить горизонтально" style='height:40px;'>
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-bar-left" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M5.854 4.646a.5.5 0 0 0-.708 0l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L3.207 8l2.647-2.646a.5.5 0 0 0 0-.708z"/>
                <path fill-rule="evenodd" d="M10 8a.5.5 0 0 0-.5-.5H3a.5.5 0 0 0 0 1h6.5A.5.5 0 0 0 10 8zm2.5 6a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 1 0v11a.5.5 0 0 1-.5.5z"/>
              </svg>
        </span>
      </button>
      <button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1"  style='height:40px;'>
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Отразить вертикально" style='height:40px;'>
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-bar-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M11.354 5.854a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L8 3.207l2.646 2.647a.5.5 0 0 0 .708 0z"/>
                <path fill-rule="evenodd" d="M8 10a.5.5 0 0 0 .5-.5V3a.5.5 0 0 0-1 0v6.5a.5.5 0 0 0 .5.5zm-4.8 1.6c0-.22.18-.4.4-.4h8.8a.4.4 0 0 1 0 .8H3.6a.4.4 0 0 1-.4-.4z"/>
              </svg>
        </span>
      </button>
    </div>

    <button type="button" class="btn btn-primary" data-method="crop"  style='height:40px;'>
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Обрезать" style='height:40px;'>
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-crop" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                 <path fill-rule="evenodd" d="M3.5.5A.5.5 0 0 1 4 1v13h13a.5.5 0 0 1 0 1H3.5a.5.5 0 0 1-.5-.5V1a.5.5 0 0 1 .5-.5z"/>
                 <path fill-rule="evenodd" d="M.5 3.5A.5.5 0 0 1 1 3h2.5a.5.5 0 0 1 0 1H1a.5.5 0 0 1-.5-.5zm5.5 0a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0V4H6.5a.5.5 0 0 1-.5-.5zM14.5 14a.5.5 0 0 1 .5.5V17a.5.5 0 0 1-1 0v-2.5a.5.5 0 0 1 .5-.5z"/>
               </svg>
        </span>
    </button>


    <div class="btn-group" style='display:none;'>
      <button type="button" class="btn btn-primary" data-method="disable" title="Disable">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;disable&quot;)">
          <span class="fa fa-lock"></span>
        </span>
      </button>
      <button type="button" class="btn btn-primary" data-method="enable" title="Enable">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;enable&quot;)">
          <span class="fa fa-unlock"></span>
        </span>
      </button>
    </div>

    <div class="btn-group" style='display:none;'>

      <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
        <input type="file" class="sr-only" id="inputImage" name="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Import image with Blob URLs">
          <span class="fa fa-upload"></span>
        </span>
      </label>
      <button type="button" class="btn btn-primary" data-method="destroy" title="Destroy">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;destroy&quot;)">
          <span class="fa fa-power-off"></span>
        </span>
      </button>
    </div>

    <div class="btn-group-crop" style='float:right; height:40px;'>

        <button type="button" class="btn btn-danger cropClear" data-method="clear" style='display:none;'>
          <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" >
            Отменить обрезку
          </span>
        </button>




      <button style='display:none;' type="button" class="btn btn-success" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 160, &quot;height&quot;: 90 }">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getCroppedCanvas&quot;, { width: 160, height: 90 })">
          160&times;90
        </span>
      </button>
      <button style='display:none;'  type="button" class="btn btn-success" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 320, &quot;height&quot;: 180 }">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getCroppedCanvas&quot;, { width: 320, height: 180 })">
          320&times;180
        </span>
      </button>
    </div>

    <!-- Show the cropped image in modal -->
    <div style='display:none;' class="modal fade docs-cropped" id="getCroppedCanvasModal" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" role="dialog" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="getCroppedCanvasTitle">Результат</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style='position:relative;'>
              <div class="modal-body-second"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg" style='display:none;'>Download</a>

          </div>
        </div>
      </div>
    </div><!-- /.modal -->

<div style='display:none;'>




    <button type="button" class="btn btn-secondary" data-method="getData" data-option data-target="#putData">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getData&quot;)">
        Get Data
      </span>
    </button>
    <button type="button" class="btn btn-secondary" data-method="setData" data-target="#putData">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;setData&quot;, data)">
        Set Data
      </span>
    </button>
    <button type="button" class="btn btn-secondary" data-method="getContainerData" data-option data-target="#putData">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getContainerData&quot;)">
        Get Container Data
      </span>
    </button>
    <button type="button" class="btn btn-secondary" data-method="getImageData" data-option data-target="#putData">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getImageData&quot;)">
        Get Image Data
      </span>
    </button>
    <button type="button" class="btn btn-secondary" data-method="getCanvasData" data-option data-target="#putData">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getCanvasData&quot;)">
        Get Canvas Data
      </span>
    </button>
    <button type="button" class="btn btn-secondary" data-method="setCanvasData" data-target="#putData">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;setCanvasData&quot;, data)">
        Set Canvas Data
      </span>
    </button>
    <button type="button" class="btn btn-secondary" data-method="getCropBoxData" data-option data-target="#putData">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getCropBoxData&quot;)">
        Get Crop Box Data
      </span>
    </button>
    <button type="button" class="btn btn-secondary" data-method="setCropBoxData" data-target="#putData">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;setCropBoxData&quot;, data)">
        Set Crop Box Data
      </span>
    </button>
    <button type="button" class="btn btn-secondary" data-method="moveTo" data-option="0">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="cropper.moveTo(0)">
        Move to [0,0]
      </span>
    </button>
    <button type="button" class="btn btn-secondary" data-method="zoomTo" data-option="1">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="cropper.zoomTo(1)">
        Zoom to 100%
      </span>
    </button>
    <button type="button" class="btn btn-secondary" data-method="rotateTo" data-option="180">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="cropper.rotateTo(180)">
        Rotate 180°
      </span>
    </button>
    <button type="button" class="btn btn-secondary" data-method="scale" data-option="-2" data-second-option="-1">
      <span class="docs-tooltip" data-toggle="tooltip" title="cropper.scale(-2, -1)">
        Scale (-2, -1)
      </span>
    </button>
    <textarea type="text" class="form-control" id="putData" placeholder="Get data to here or set data with this value"></textarea>
  </div><!-- /.docs-buttons -->

  <div style='display:none;' class="col-md-3 docs-toggles">
    <!-- <h3>Toggles:</h3> -->
    <div class="btn-group d-flex flex-nowrap" data-toggle="buttons">
      <label class="btn btn-primary active">
        <input type="radio" class="sr-only" id="aspectRatio0" name="aspectRatio" value="1.7777777777777777">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 16 / 9">
          16:9
        </span>
      </label>
      <label class="btn btn-primary">
        <input type="radio" class="sr-only" id="aspectRatio1" name="aspectRatio" value="1.3333333333333333">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 4 / 3">
          4:3
        </span>
      </label>
      <label class="btn btn-primary">
        <input type="radio" class="sr-only" id="aspectRatio2" name="aspectRatio" value="1">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 1 / 1">
          1:1
        </span>
      </label>
      <label class="btn btn-primary">
        <input type="radio" class="sr-only" id="aspectRatio3" name="aspectRatio" value="0.6666666666666666">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 2 / 3">
          2:3
        </span>
      </label>
      <label class="btn btn-primary">
        <input type="radio" class="sr-only" id="aspectRatio4" name="aspectRatio" value="NaN">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: NaN">
          Free
        </span>
      </label>
    </div>

    <div class="btn-group d-flex flex-nowrap" data-toggle="buttons">
      <label class="btn btn-primary active">
        <input type="radio" class="sr-only" id="viewMode0" name="viewMode" value="0" checked>
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="View Mode 0">
          VM0
        </span>
      </label>
      <label class="btn btn-primary">
        <input type="radio" class="sr-only" id="viewMode1" name="viewMode" value="1">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="View Mode 1">
          VM1
        </span>
      </label>
      <label class="btn btn-primary">
        <input type="radio" class="sr-only" id="viewMode2" name="viewMode" value="2">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="View Mode 2">
          VM2
        </span>
      </label>
      <label class="btn btn-primary">
        <input type="radio" class="sr-only" id="viewMode3" name="viewMode" value="3">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="View Mode 3">
          VM3
        </span>
      </label>
    </div>

    <div class="dropdown dropup docs-options">
      <button type="button" class="btn btn-primary btn-block dropdown-toggle" id="toggleOptions" data-toggle="dropdown" aria-expanded="true">
        Toggle Options
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" aria-labelledby="toggleOptions" role="menu">
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="responsive" type="checkbox" name="responsive" checked>
            <label class="form-check-label" for="responsive">responsive</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="restore" type="checkbox" name="restore" checked>
            <label class="form-check-label" for="restore">restore</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="checkCrossOrigin" type="checkbox" name="checkCrossOrigin" checked>
            <label class="form-check-label" for="checkCrossOrigin">checkCrossOrigin</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="checkOrientation" type="checkbox" name="checkOrientation" checked>
            <label class="form-check-label" for="checkOrientation">checkOrientation</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="modal" type="checkbox" name="modal" checked>
            <label class="form-check-label" for="modal">modal</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="guides" type="checkbox" name="guides" checked>
            <label class="form-check-label" for="guides">guides</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="center" type="checkbox" name="center" checked>
            <label class="form-check-label" for="center">center</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="highlight" type="checkbox" name="highlight" checked>
            <label class="form-check-label" for="highlight">highlight</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="background" type="checkbox" name="background" checked>
            <label class="form-check-label" for="background">background</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="autoCrop" type="checkbox" name="autoCrop" checked>
            <label class="form-check-label" for="autoCrop">autoCrop</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="movable" type="checkbox" name="movable" checked>
            <label class="form-check-label" for="movable">movable</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="rotatable" type="checkbox" name="rotatable" checked>
            <label class="form-check-label" for="rotatable">rotatable</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="scalable" type="checkbox" name="scalable" checked>
            <label class="form-check-label" for="scalable">scalable</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="zoomable" type="checkbox" name="zoomable" checked>
            <label class="form-check-label" for="zoomable">zoomable</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="zoomOnTouch" type="checkbox" name="zoomOnTouch" checked>
            <label class="form-check-label" for="zoomOnTouch">zoomOnTouch</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="zoomOnWheel" type="checkbox" name="zoomOnWheel" checked>
            <label class="form-check-label" for="zoomOnWheel">zoomOnWheel</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="cropBoxMovable" type="checkbox" name="cropBoxMovable" checked>
            <label class="form-check-label" for="cropBoxMovable">cropBoxMovable</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="cropBoxResizable" type="checkbox" name="cropBoxResizable" checked>
            <label class="form-check-label" for="cropBoxResizable">cropBoxResizable</label>
          </div>
        </li>
        <li class="dropdown-item">
          <div class="form-check">
            <input class="form-check-input" id="toggleDragModeOnDblclick" type="checkbox" name="toggleDragModeOnDblclick" checked>
            <label class="form-check-label" for="toggleDragModeOnDblclick">toggleDragModeOnDblclick</label>
          </div>
        </li>
      </ul>
    </div><!-- /.dropdown -->

    <a class="btn btn-success btn-block" data-toggle="tooltip" data-animation="false" href="https://fengyuanchen.github.io/cropperjs" title="JavaScript image cropper">Cropper.js</a>


</div>
  </div><!-- /.docs-toggles -->
</div>

 <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;' class='docs-buttons'>
          <button type="button" class="btn btn-success" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" style='width:700px;'>
            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="">
              Применить и Сохранить
            </span>
          </button>

        <div style='with:100px; float:right;'>
                <button type="button" class="btn btn-primary" data-method="reset">
                    <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="Сбросить изменения">
                      Сбросить изменения
                    </span>
                </button>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>

<script>
$(function () {
  'use strict';

  var console = window.console || { log: function () {} };
  var URL = window.URL || window.webkitURL;
  var $image = $('#image');
  var $download = $('#download');
  var $dataX = $('#dataX');
  var $dataY = $('#dataY');
  var $dataHeight = $('#dataHeight');
  var $dataWidth = $('#dataWidth');
  var $dataRotate = $('#dataRotate');
  var $dataScaleX = $('#dataScaleX');
  var $dataScaleY = $('#dataScaleY');
  var options = {
    aspectRatio: 'free',
    preview: '.img-preview',
    autoCrop: false,
    viewMode: 2,
    movable: false,
    crop: function (e) {
      $dataX.val(Math.round(e.detail.x));
      $dataY.val(Math.round(e.detail.y));
      $dataHeight.val(Math.round(e.detail.height));
      $dataWidth.val(Math.round(e.detail.width));
      $dataRotate.val(e.detail.rotate);
      $dataScaleX.val(e.detail.scaleX);
      $dataScaleY.val(e.detail.scaleY);
    }
  };
  var originalImageURL = $image.attr('src');
  var uploadedImageName = 'cropped.jpg';
  var uploadedImageType = 'image/jpeg';
  var uploadedImageURL;

  // Tooltip
  $('[data-toggle="tooltip"]').tooltip();

  // Cropper
  $image.on({
    ready: function (e) {
      console.log(e.type);
    },
    cropstart: function (e) {
      console.log(e.type, e.detail.action);
    },
    cropmove: function (e) {
      console.log(e.type, e.detail.action);
    },
    cropend: function (e) {
      $('.cropClear').css('display','inline');
    },
    crop: function (e) {
      console.log(e.type);
    },
    zoom: function (e) {
      console.log(e.type, e.detail.ratio);
    }
  }).cropper(options);

  // Buttons
  if (!$.isFunction(document.createElement('canvas').getContext)) {
    $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
  }

  if (typeof document.createElement('cropper').style.transition === 'undefined') {
    $('button[data-method="rotate"]').prop('disabled', true);
    $('button[data-method="scale"]').prop('disabled', true);
  }

  // Download
  if (typeof $download[0].download === 'undefined') {
    $download.addClass('disabled');
  }

  // Options
  $('.docs-toggles').on('change', 'input', function () {
    var $this = $(this);
    var name = $this.attr('name');
    var type = $this.prop('type');
    var cropBoxData;
    var canvasData;

    if (!$image.data('cropper')) {
      return;
    }

    if (type === 'checkbox') {
      options[name] = $this.prop('checked');
      cropBoxData = $image.cropper('getCropBoxData');
      canvasData = $image.cropper('getCanvasData');

      options.ready = function () {
        $image.cropper('setCropBoxData', cropBoxData);
        $image.cropper('setCanvasData', canvasData);
      };
    } else if (type === 'radio') {
      options[name] = $this.val();
    }

    $image.cropper('destroy').cropper(options);
  });

  // Methods
  $('.docs-buttons').on('click', '[data-method]', function () {
    var $this = $(this);
    var data = $this.data();
    var cropper = $image.data('cropper');
    var cropped;
    var $target;
    var result;

    if ($this.prop('disabled') || $this.hasClass('disabled')) {
      return;
    }

    if (cropper && data.method) {


        if (data.method == 'clear'){
        $('.cropClear').css('display','none');
        }

        if (data.method == 'crop'){
        $('.cropClear').css('display','inline');
        }

      data = $.extend({}, data); // Clone a new one

      if (typeof data.target !== 'undefined') {
        $target = $(data.target);

        if (typeof data.option === 'undefined') {
          try {
            data.option = JSON.parse($target.val());
          } catch (e) {
            console.log(e.message);
          }
        }
      }

      cropped = cropper.cropped;

      switch (data.method) {
        case 'rotate':
          if (cropped && options.viewMode > 0) {
            $image.cropper('clear');
          }

          break;

        case 'getCroppedCanvas':
          if (uploadedImageType === 'image/jpeg') {
            if (!data.option) {
              data.option = {};
            }

            data.option.fillColor = '#fff';
          }

          break;
      }

      result = $image.cropper(data.method, data.option, data.secondOption);

      switch (data.method) {
        case 'rotate':
          if (cropped && options.viewMode > 0) {
            $image.cropper('crop');
          }

          break;

        case 'scaleX':
        case 'scaleY':
          $(this).data('option', -data.option);
          break;

        case 'getCroppedCanvas':
          if (result) {
            // Bootstrap's Modal
            var y = "";
            $('.modal-body-second').html(result);

            if (!$download.hasClass('disabled')) {
              download.download = uploadedImageName;
              var href = result.toDataURL(uploadedImageType);
              $download.attr('href', result.toDataURL(uploadedImageType));
              save_photo(href);

              //var g = $image.cropper('getData');
              //$image.cropper('setData',g);
              //$image.setData(g);
              //alert(g);
              //$('#image').attr('src',href);
            }
          }

          break;

        case 'destroy':
          if (uploadedImageURL) {
            URL.revokeObjectURL(uploadedImageURL);
            uploadedImageURL = '';
            $image.attr('src', originalImageURL);
          }

          break;
      }

      if ($.isPlainObject(result) && $target) {
        try {
          $target.val(JSON.stringify(result));
        } catch (e) {
          console.log(e.message);
        }
      }
    }
  });

  // Keyboard
  $(document.body).on('keydown', function (e) {
    if (e.target !== this || !$image.data('cropper') || this.scrollTop > 300) {
      return;
    }

    switch (e.which) {
      case 37:
        e.preventDefault();
        $image.cropper('move', -1, 0);
        break;

      case 38:
        e.preventDefault();
        $image.cropper('move', 0, -1);
        break;

      case 39:
        e.preventDefault();
        $image.cropper('move', 1, 0);
        break;

      case 40:
        e.preventDefault();
        $image.cropper('move', 0, 1);
        break;
    }
  });

  // Import image
  var $inputImage = $('#inputImage');

  if (URL) {
    $inputImage.change(function () {
      var files = this.files;
      var file;

      if (!$image.data('cropper')) {
        return;
      }

      if (files && files.length) {
        file = files[0];

        if (/^image\/\w+$/.test(file.type)) {
          uploadedImageName = file.name;
          uploadedImageType = file.type;

          if (uploadedImageURL) {
            URL.revokeObjectURL(uploadedImageURL);
          }

          uploadedImageURL = URL.createObjectURL(file);
          $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
          $inputImage.val('');
        } else {
          window.alert('Please choose an image file.');
        }
      }
    });
  } else {
    $inputImage.prop('disabled', true).parent().addClass('disabled');
  }


function save_photo(href){
        if (href != '')
        {
            $('.modalSaving').css('display','block');
            $.ajax({
              type: "POST",
              url: '<?= Yii::app()->createUrl('photos/save',array('id'=>$photo->id,'crop_type'=>$crop_type)); ?>',
              dataType: 'json',
              data: {
                  'canvas':href
              },
              success: function(response){
                  //alert(response);
                  //alert(window.location);
                  window.location = window.location;
                  //alert('ok');
              },
              error: function(response){
              }
            });
        }
  };
});

</script>



</div>
</div>
