<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <a href="<?= Yii::app()->createUrl('home/index',array()); ?>" style='font-size:40px; color:#000;'>Главная</a>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'><a href="<?=Yii::app()->createUrl('raspisanie/index',array());?>">Расписание и билеты</a></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

    <?php echo CHtml::beginForm(); ?>
<div style='width:430px; float:left;'>

    <div class="form-group">
        <label for="map" class="control-label">Режим работы</label>
        <input type="text" class="form-control" id="raspis_time" name="COMPANY[raspis_time]" value='<?=$raspisanie['raspis_time'];?>' placeholder='10:00 - 18:00'>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Надпись под режимом работы</label>
        <input type="text" class="form-control" id="raspis_description" name="COMPANY[raspis_description]" value='<?=$raspisanie['raspis_description'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label"><b>Дети</b> входной билет</label>
        <input type="text" class="form-control" id="raspis_children_1" name="COMPANY[raspis_children_1]" value='<?=$raspisanie['raspis_children_1'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Дети абонемент</label>
        <input type="text" class="form-control" id="raspis_children_2" name="COMPANY[raspis_children_2]" value='<?=$raspisanie['raspis_children_2'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Дети другое</label>
        <input type="text" class="form-control" id="raspis_children_3" name="COMPANY[raspis_children_3]" value='<?=$raspisanie['raspis_children_3'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Дети другое описание</label>
        <input type="text" class="form-control" id="raspis_children_3_text" name="COMPANY[raspis_children_3_text]" value='<?=$raspisanie['raspis_children_3_text'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label"><b>Взрослые</b> входной билет</label>
        <input type="text" class="form-control" id="raspis_vzrosliy_1" name="COMPANY[raspis_vzrosliy_1]" value='<?=$raspisanie['raspis_vzrosliy_1'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Взрослые абонемент</label>
        <input type="text" class="form-control" id="raspis_vzrosliy_2" name="COMPANY[raspis_vzrosliy_2]" value='<?=$raspisanie['raspis_vzrosliy_2'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Взрослые другое</label>
        <input type="text" class="form-control" id="raspis_vzrosliy_3" name="COMPANY[raspis_vzrosliy_3]" value='<?=$raspisanie['raspis_vzrosliy_3'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Взрослые другое описание</label>
        <input type="text" class="form-control" id="raspis_vzrosliy_3_text" name="COMPANY[raspis_vzrosliy_3_text]" value='<?=$raspisanie['raspis_vzrosliy_3_text'];?>' placeholder=''>
    </div>



    <div class="form-group">
        <label for="map" class="control-label"><b>Пенсия</b> входной билет</label>
        <input type="text" class="form-control" id="raspis_pens_1" name="COMPANY[raspis_pens_1]" value='<?=$raspisanie['raspis_pens_1'];?>' placeholder=''>
    </div>

    <!--
    <div class="form-group" style='display:none;'>
        <label for="map" class="control-label">Пенсия будни (не используется)</label>
        <input type="text" class="form-control" id="raspis_pens_2" name="COMPANY[raspis_pens_2]" value='<?=$raspisanie['raspis_pens_2'];?>' placeholder=''>
    </div>
    -->

    <div class="form-group">
        <label for="map" class="control-label">Пенсия другое</label>
        <input type="text" class="form-control" id="raspis_pens_3" name="COMPANY[raspis_pens_3]" value='<?=$raspisanie['raspis_pens_3'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Пенсия другое описание</label>
        <input type="text" class="form-control" id="raspis_pens_3_text" name="COMPANY[raspis_pens_3_text]" value='<?=$raspisanie['raspis_pens_3_text'];?>' placeholder=''>
    </div>


</div>



<div style='width:400px; float:right;'>
    <div class="form-group">
        <label for="map" class="control-label">Time</label>
        <input type="text" class="form-control" id="raspis_time" name="COMPANY_ENG[raspis_time]" value='<?=$raspisanie_eng['raspis_time'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Text under time</label>
        <input type="text" class="form-control" id="raspis_description" name="COMPANY_ENG[raspis_description]" value='<?=$raspisanie_eng['raspis_description'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label"><b>Children</b> 1-DAY PASS</label>
        <input type="text" class="form-control" id="raspis_children_1" name="COMPANY_ENG[raspis_children_1]" value='<?=$raspisanie_eng['raspis_children_1'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Children MEMBERSHIP</label>
        <input type="text" class="form-control" id="raspis_children_2" name="COMPANY_ENG[raspis_children_2]" value='<?=$raspisanie_eng['raspis_children_2'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Children other</label>
        <input type="text" class="form-control" id="raspis_children_3" name="COMPANY_ENG[raspis_children_3]" value='<?=$raspisanie_eng['raspis_children_3'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Children other text</label>
        <input type="text" class="form-control" id="raspis_children_3_text" name="COMPANY_ENG[raspis_children_3_text]" value='<?=$raspisanie_eng['raspis_children_3_text'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label"><b>Parents</b> 1-DAY PASS</label>
        <input type="text" class="form-control" id="raspis_vzrosliy_1" name="COMPANY_ENG[raspis_vzrosliy_1]" value='<?=$raspisanie_eng['raspis_vzrosliy_1'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Parents MEMBERSHIP</label>
        <input type="text" class="form-control" id="raspis_vzrosliy_2" name="COMPANY_ENG[raspis_vzrosliy_2]" value='<?=$raspisanie_eng['raspis_vzrosliy_2'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Parents other</label>
        <input type="text" class="form-control" id="raspis_vzrosliy_3" name="COMPANY_ENG[raspis_vzrosliy_3]" value='<?=$raspisanie_eng['raspis_vzrosliy_3'];?>' placeholder=''>
    </div>
    <div class="form-group">
        <label for="map" class="control-label">Parents other text</label>
        <input type="text" class="form-control" id="raspis_vzrosliy_3_text" name="COMPANY_ENG[raspis_vzrosliy_3_text]" value='<?=$raspisanie_eng['raspis_vzrosliy_3_text'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label"><b>Adults</b> 1-DAY PASS</label>
        <input type="text" class="form-control" id="raspis_pens_1" name="COMPANY_ENG[raspis_pens_1]" value='<?=$raspisanie_eng['raspis_pens_1'];?>' placeholder=''>
    </div>

    <!--
    <div class="form-group" style='display:none;'>
        <label for="map" class="control-label">Adults week</label>
        <input type="text" class="form-control" id="raspis_pens_2" name="COMPANY_ENG[raspis_pens_2]" value='<?=$raspisanie_eng['raspis_pens_2'];?>' placeholder=''>
    </div>
    -->

    <div class="form-group">
        <label for="map" class="control-label">Adults other</label>
        <input type="text" class="form-control" id="raspis_pens_3" name="COMPANY_ENG[raspis_pens_3]" value='<?=$raspisanie_eng['raspis_pens_3'];?>' placeholder=''>
    </div>

    <div class="form-group">
        <label for="map" class="control-label">Adults other text</label>
        <input type="text" class="form-control" id="raspis_pens_3_text" name="COMPANY_ENG[raspis_pens_3_text]" value='<?=$raspisanie_eng['raspis_pens_3_text'];?>' placeholder=''>
    </div>


</div>

<div style='clear:both; padding-top:30px; border-bottom:1px solid #ccc; margin-bottom:20px;'></div>

<div class="form-group">
    <label for="map" class="control-label"><b>Покупка билетов</b></label>
    <div style='padding-top:10px;'>
        <input id="buy_ticket_show" name="COMPANY[buy_ticket_show]" type="radio" <? if ($tickets['buy_ticket_show'] == 'show'){?>checked="checked"<?}?> value='show'> ВКЛ
        <input id="buy_ticket_notshow" name="COMPANY[buy_ticket_show]" type="radio" <? if ($tickets['buy_ticket_show'] == 'not_show'){?>checked="checked"<?}?> value='not_show'> ВЫКЛ
    </div>

    <div style='width:430px; float:left; padding-top:30px;'>
        <div class="form-group">
            <label for="map" class="control-label"><b>Надпись на кнопке "Купить билет"</b></label>
            <input type="text" class="form-control" id="buy_ticket_name" name="COMPANY[buy_ticket_name]" value='<?=$raspisanie['buy_ticket_name'];?>' placeholder=''>
        </div>
    </div>

    <div style='width:400px; float:right; padding-top:30px;'>

            <div class="form-group">
            <label for="map" class="control-label"><b>Button Buy tickets name</b></label>
            <input type="text" class="form-control" id="raspis_pens_3_text" name="COMPANY_ENG[buy_ticket_name]" value='<?=$raspisanie_eng['buy_ticket_name'];?>' placeholder=''>
            </div>
    </div>

<div style='clear:both;'></div>

    <div style='padding-top:30px;'>
    <label for="map" class="control-label"><b>Код для покупки билетов</b></label>
    <textarea type="text" class="form-control" id="buy_ticket_script" name="COMPANY[buy_ticket_script]"  placeholder='' style='min-height:120px;'><?=$tickets['buy_ticket_script'];?></textarea>
    </div>

    <label for="map" class="control-label" style='padding-top:30px;'><b>Функция на кнопке для запуска виджета покупки билетов</b></label>
    <input type="text" class="form-control" id="buy_ticket_code" name="COMPANY[buy_ticket_code]" value='<?=$tickets['buy_ticket_code'];?>' placeholder=''>

</div>

<div style='padding-top:20px; border-bottom:1px solid #ccc; margin-bottom:40px;'></div>

<div class="form-group">
    <label for="map" class="control-label">Цвет расписания зимой</label> <span style='border-radius:4px; padding:5px 15; margin-left:10px; <?=$raspisanie['winter_color'];?>'></span>
    <input type="text" class="form-control" id="winter_color" name="COMPANY[winter_color]" value='<?=$raspisanie['winter_color'];?>' placeholder=''>
</div>

<div class="form-group">
    <label for="map" class="control-label">Цвет расписания весной</label> <span style='border-radius:4px; padding:5px 15; margin-left:10px;  <?=$raspisanie['spring_color'];?>'></span>
    <input type="text" class="form-control" id="spring_color" name="COMPANY[spring_color]" value='<?=$raspisanie['spring_color'];?>' placeholder=''>
</div>

<div class="form-group">
    <label for="map" class="control-label">Цвет расписания летом</label> <span style='border-radius:4px; padding:5px 15; margin-left:10px;  <?=$raspisanie['summer_color'];?>'></span>
    <input type="text" class="form-control" id="summer_color" name="COMPANY[summer_color]" value='<?=$raspisanie['summer_color'];?>' placeholder=''>
</div>

<div class="form-group">
    <label for="map" class="control-label">Цвет расписания осенью</label> <span style='border-radius:4px; padding:5px 15; margin-left:10px;  <?=$raspisanie['fall_color'];?>'></span>
    <input type="text" class="form-control" id="fall_color" name="COMPANY[fall_color]" value='<?=$raspisanie['fall_color'];?>' placeholder=''>
</div>



    <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>

    <div style='padding-left:320px;'>
        <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

        <div style='clear:both;'></div>
    </div>
    </div>

<?php

?>


    <?php echo CHtml::endForm(); ?>
</div>
</div>
