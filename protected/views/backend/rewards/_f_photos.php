<style>
.photo{
    border:3px solid #fff
}

.photo.isCover{
    border:3px solid #f60000
}

</style>
<div id='module_photos_<?=$model->id;?>'>
<?php
$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$photos,
'viewData'=>array(
    'model' => $model,
),
'itemView'=>'_f_photos_item',
'template'=>"\n{items}\n{pager}",
'emptyText'=>'',
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/pages.css', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));

?>
<div style='clear:both;'></div>
<div style='padding-bottom:70px; padding-top:20px;'>
<div class='delete_photos_button' style="display:none;background:#C9302C; width:100%; cursor:pointer; margin-bottom:5px;"  onclick="delete_checked_photos(<?=$model->id;?>)"><div style="padding:20px; text-align:center; font-size:14px; color:#fff;">Удалить фотографии</div></div>
<div style="background:#fff; box-shadow:0px 0px 2px rgba(0,0,0,.1); width:100%; cursor:pointer;"  onclick="add_photos_and_files('photos',<?=$model->id;?>)"><div style="padding:20px; text-align:center; font-size:14px;">Добавить фотографии</div></div>
 </div>

</div>