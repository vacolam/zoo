<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:100%; height:100px; text-align:left;' valign=middle>
                        <div style='width:100%; height:50px; position:relative;'>
                            <div style='width:120px; background:#28A745; border-radius: 3px; height:38px; text-align:center; cursor:pointer; line-height:38px; font-size:16px; color:#fff; position:absolute; top:6px; right:6px; z-index:99;' onclick='search_start()'>Найти</div>
                            <form class='s_form' method="post" onsubmit='return false;'>
                            <?
                                $s='';
                                if (isset($_GET['search_text'])){
                                    $s = $_GET['search_text'];
                                }
                            ?>
                            <input type="text" value='<?=$s;?>' class='search_text' name='search_text' style='width:900px; padding-left:20px; padding-right:140px; height:50px; line-height:50px; border:1px solid #ccc; border-radius:3px; font-size:16px; outline:none;' placeholder='Поиск'  />
                            </form>
                        </div>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>

<div style='padding-top:0px;'>
<?php
if ($news){
    foreach($news as $item){
        $this->renderPartial('_news', array('data'=>$item));
    }
}

if ($afisha){
    foreach($afisha as $item){
        $this->renderPartial('_afisha', array('data'=>$item));
    }
}

if ($pages){
    foreach($pages as $item){
        $this->renderPartial('_pages', array('data'=>$item));
    }
}

if ($conf){
    foreach($conf as $item){
        $this->renderPartial('_conf', array('data'=>$item));
    }
}

if ($exposure){
    foreach($exposure as $item){
        $this->renderPartial('_exposure', array('data'=>$item));
    }
}

if ($exposure_items){
    foreach($exposure_items as $item){
        $this->renderPartial('_exposure_item', array('data'=>$item));
    }
}
?>
</div>
</div>
</div>


<script>
 function search_start(){
     var search = $('.search_text').val();
     if (search != ''){
        $('.s_form').attr('onsubmit','');
        $('.s_form').attr('action','<?=Yii::app()->createUrl('search/index');?>'+ '&search_text='+search);
        $('.s_form').submit();
     }
 }
</script>