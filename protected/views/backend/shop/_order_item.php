    <div style='padding-bottom:20px; margin-bottom:20px; border-bottom:1px solid #ccc;'>
                    <a href="<?= Yii::app()->createUrl('shop/order',array('id'=>$data->id)); ?>" style='display:block;'>

                        <div style='width:100px; float:left; margin-top:-5px; '>
                             <div style='font-size:14px;' class='open-s'>
                                 <?
                                 echo Dates::getName($data->date);
                                 ?>&nbsp;
                             </div>
                        </div>

                        <div style='width:800px; float:left; margin-top:-5px; '>
                               <div class='open-s' style='' >
                                   <span style='color:#007BFF;' ><b>ЗАКАЗ № <?=$data->id;?></b></span>
                                   <span style='padding-left:10px; font-size:14px; text-transform:uppercase;' >
                                   |&nbsp;&nbsp;&nbsp;<?
                                   $delivery_type = array(
                                    1 => 'Самовывоз из Зоопарка',
                                    2 => 'Доставка курьером',
                                   );

                                   if (isset($delivery_type[$data->deliverytype]))
                                   {
                                       echo $delivery_type[$data->deliverytype];
                                   }
                                   ?>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                                   </span>
                                   <span style='padding-left:0px; font-size:14px; color:#404040;'><i><?=$data->name;?>,</i></span>
                                   <span style='padding-left:10px; font-size:14px; color:#404040;'><i><?=$this->phoneFormat($data->phone);?>,</i></span>
                                   <span style='padding-left:10px; font-size:14px; color:#404040;'><i><?=$data->adress;?></i></span>
                               </div>
                               <div style='font-size:13px; line-height:1.7em; padding-top:10px;' class='open-s'>
                                    <?
                                    $summ = 0;
                                    $count = 0;
                                    $orderItems = $data->orderitems;
                                    $ar = array();
                                    if ($orderItems)
                                    {
                                        foreach($orderItems as $item)
                                        {
                                            //<span style='background:#E0E0E0; border-radius:3px; padding:1px 6px; display:none;'>".$item->quontity."шт.</span>
                                            $ar[] = "<span style='color:#808080;'>".$item->product->title.";</span>";
                                            $summ = $summ + ($item->price*$item->quontity);
                                            $count = $count + $item->quontity;
                                        }
                                    }
                                    if (count($ar) > 0){
                                        echo implode(" ",$ar);
                                    }
                                    ?>
                                </div>
                        </div>

                        <div style='width:100px; float:left; text-align:center; font-size:13px; margin-top:10px;'>
                        <span style='padding:12px 16px; background:#E0E0E0; border-radius:3px;'><?
                        echo $count.' шт';
                        ?></span>
                        </div>

                        <div style='width:100px; float:left; text-align:center; font-size:13px; margin-top:10px; '>
                       <span style='padding:12px 16px; background:#E0E0E0; border-radius:3px; '><?
                        echo $summ.' РУБ';
                        ?></span>
                        </div>

                        <div style='width:200px; float:right; text-align:right; font-size:15px;'>
                            <?
                            $status_text = array(
                                0 => 'Принят',
                                1 => 'Готов к выдаче',
                                2 => 'Частично готов к выдаче',
                                1000 => 'Оплачен и выдан',
                                10000 => 'Отменен продавцом',
                                10001 => 'Отменен покупателем',
                            );
                            $status = array
                            (
                                0 => 'bg-warning',
                                1 => 'bg-primary',
                                2 => 'bg-primary',
                                1000 => 'bg-success',
                                10000 => 'bg-danger',
                                10001 => 'bg-danger',
                            );

                            $status_label = 'Статус заказа';
                            if (isset($status_text[$data->status]))
                            {
                                $status_label = $status_text[$data->status];
                            }

                            $status_class = 'bg-primary';
                            if (isset($status[$data->status]))
                            {
                                $status_class = $status[$data->status];
                            }
                            ?>
                            <div style=''>
                                 <span style='padding:12px 16px; font-weight:normal; color:#fff;' class="badge <?=$status_class;?>"><b><?=$status_label;?></b></span>
                            </div>
                            <?
                            $status_sms = ShopSms::getSmsByStatus($data,$data->status);
                            if($status_sms){
                                $send_status_text = '';
                                if ($status_sms->send_status == '' AND $status_sms->send == 0){$send_status_text = 'Ожидает отправки оператору';}
                                elseif ($status_sms->send_status == '' AND $status_sms->send == 1){$send_status_text = 'СМС Отправлено оператору';}
                                elseif($status_sms->send_status == 'moderation'){$send_status_text = 'СМС На модерации';}
                                elseif($status_sms->send_status == 'delivery'){$send_status_text = 'СМС Доставлено';}
                                elseif($status_sms->send_status == 'reject'){$send_status_text = 'СМС Отклонено';}
                                else{$send_status_text = $status_sms->send_status;}
                                echo "<b> СМС: ".$send_status_text."</b>";
                            }
                            ?>
                        </div>


                        <div style='clear:both;'></div>
                    </a>


    </div>

