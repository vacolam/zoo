<style>
.submenu{padding-top:11px; padding-bottom:11px; display:block;  font-size:14px;}
.submenu:hover{color:#53952A;}
.submenu.active{font-weight:bold;  color:#53952A;}
.submenu_li{border-bottom:1px dotted #ccc;}
.submenu_ul{padding:0px;margin:0px;list-style:none;  padding-top:20px;}
.submenu_ul :last-child{border-bottom:none;}
.layouts_submenu_block{background:#F9FAFB; border-radius:5px; border:1px solid #DDE3E9; border-radius:5px;}
.layouts_submenu_padding{padding:40px; padding-top:0px;}
.layouts_submenu_title{font-size:22px; font-weight:600; text-transform:uppercase; color:#303030;text-align:left;}

</style>
<div style='' class='layouts_submenu_block'>
<div style='' class='layouts_submenu_padding'>

<ul style='' class='submenu_ul'>
<?
$active = false;

$AllowedShopArray = User::getAllowedShop();



?>
<li style='' class='submenu_li'><a href="<?=Yii::app()->createUrl('shop/index',array());?>" class='open-s submenu <? echo $active === true ? 'active' : ''; ?>' style=' '><b>Все</b></a></li>
<?

    foreach($submenu as $index => $item)
    {
        if (User::checkAllowedShop($index,$AllowedShopArray))
        {
            $active = '';
            $link = Yii::app()->createUrl('shop/index',array('cats_id'=>$index));
            if (isset($active_cat))
            {
                if ($index == $active_cat){
                    $active = true;
                }
            }
            elseif (isset($_GET['cats_id']))
            {
                if (in_array((int)$_GET['cats_id'], array($index))){
                    $active = true;
                }
            }

        ?>
        <li style='' class='submenu_li'><a href="<?=$link;?>" class='open-s submenu <? echo $active === true ? 'active' : ''; ?>' style=' '><b><?=$item['title'];?></b></a></li>
        <?
        }

        //СабМеню
            if (isset($item['sub']))
            {
                foreach($item['sub'] as $index_sub => $item_sub)
                {
                    if (User::checkAllowedShop($index,$AllowedShopArray) OR User::checkAllowedShop($index_sub,$AllowedShopArray))
                    {
                        $active_s = '';
                        if (isset($_GET['cats_id']))
                        {
                            if (in_array((int)$_GET['cats_id'],array($index_sub))){
                                $active_s = true;
                            }
                        }
                        ?>
                            <li style='padding-left:20px;' class='submenu_li'><a href="<?=Yii::app()->createUrl('shop/index',array('cats_id'=>$index_sub));?>" class='open-s submenu <? echo $active_s === true ? 'active' : ''; ?>' style=' '><?=$item_sub['title'];?></a></li>
                        <?
                    }
                }


                if (User::checkAllowedShop($index,$AllowedShopArray))
                {
                ?>
                <li style='padding-left:20px;' class='submenu_li'><a href="<?=Yii::app()->createUrl('shop/catscreate',array('cats_id'=>$index));?>" class='open-s submenu' style=''>+ Подкатегорию</a></li>
                <?

                }
            }
    }
?>
<?
$active = false;

if (Yii::app()->user->role == 'admin')
{
    ?>
    <li style='margin-top:20px;' class='submenu_li'><a href="<?=Yii::app()->createUrl('shop/catscreate',array());?>" class='open-s submenu <? echo $active === true ? 'active' : ''; ?>' style=' '><b>+ Добавить новую категорию</b></a></li>
    <?
}
?>
</ul>
</div>
</div>