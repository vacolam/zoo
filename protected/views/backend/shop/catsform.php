<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two' style='width: 1400px;'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <ul style='list-style:none; padding:0px; margin:0px;'>
                            <li style='float:right; '>
                                <a href="<?=Yii::app()->createUrl('cats/update',array('id'=>$this->razdel['id']));?>" class='btn btn-outline-success' style=''>
                                    <svg class="bi bi-gear" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M8.837 1.626c-.246-.835-1.428-.835-1.674 0l-.094.319A1.873 1.873 0 014.377 3.06l-.292-.16c-.764-.415-1.6.42-1.184 1.185l.159.292a1.873 1.873 0 01-1.115 2.692l-.319.094c-.835.246-.835 1.428 0 1.674l.319.094a1.873 1.873 0 011.115 2.693l-.16.291c-.415.764.42 1.6 1.185 1.184l.292-.159a1.873 1.873 0 012.692 1.116l.094.318c.246.835 1.428.835 1.674 0l.094-.319a1.873 1.873 0 012.693-1.115l.291.16c.764.415 1.6-.42 1.184-1.185l-.159-.291a1.873 1.873 0 011.116-2.693l.318-.094c.835-.246.835-1.428 0-1.674l-.319-.094a1.873 1.873 0 01-1.115-2.692l.16-.292c.415-.764-.42-1.6-1.185-1.184l-.291.159A1.873 1.873 0 018.93 1.945l-.094-.319zm-2.633-.283c.527-1.79 3.065-1.79 3.592 0l.094.319a.873.873 0 001.255.52l.292-.16c1.64-.892 3.434.901 2.54 2.541l-.159.292a.873.873 0 00.52 1.255l.319.094c1.79.527 1.79 3.065 0 3.592l-.319.094a.873.873 0 00-.52 1.255l.16.292c.893 1.64-.902 3.434-2.541 2.54l-.292-.159a.873.873 0 00-1.255.52l-.094.319c-.527 1.79-3.065 1.79-3.592 0l-.094-.319a.873.873 0 00-1.255-.52l-.292.16c-1.64.893-3.433-.902-2.54-2.541l.159-.292a.873.873 0 00-.52-1.255l-.319-.094c-1.79-.527-1.79-3.065 0-3.592l.319-.094a.873.873 0 00.52-1.255l-.16-.292c-.892-1.64.902-3.433 2.541-2.54l.292.159a.873.873 0 001.255-.52l.094-.319z" clip-rule="evenodd"/>
                                        <path fill-rule="evenodd" d="M8 5.754a2.246 2.246 0 100 4.492 2.246 2.246 0 000-4.492zM4.754 8a3.246 3.246 0 116.492 0 3.246 3.246 0 01-6.492 0z" clip-rule="evenodd"/>
                                    </svg>
                                </a>
                            </li>

                            <li style='float:right; margin-right:10px;'>
                            <?
                            $link = Yii::app()->createUrl('shopitem/create');
                            if ($cats_id > 0){
                            $link = Yii::app()->createUrl('shopitem/create',array('cats_id'=>$cats_id));
                            }
                            ?>
                            <a href="<?=$link; ?>" class="btn btn-outline-success">+ товар</a>
                            </li>
                            <li style='clear:both;'></li>
                        </ul>
                    </td>
            </tr>
            </table>
        </div>
    </div>




    <div class='content_one' style=' width: 1400px;'>
    <?
    $this->renderPartial('_nav_bar', array());
    ?>
    <Div style='width:300px; float:left;'>
    <?
     $this->renderPartial('_submenu', array(
                'submenu' => $submenu,
            ));
    ?>

    </Div>

    <div style='width:980px; float:right;'>


<?
$title = '';
if (Yii::app()->controller->action->id == 'catscreate' AND $cats_id == 0){
    $title = 'Добавить категорию';
}

if (Yii::app()->controller->action->id == 'catscreate' AND $cats_id > 0){
    $title = 'Добавить подкатегорию';
}

if (Yii::app()->controller->action->id == 'catsupdate' AND $model->parent_id == 0){
    $title = 'Редактировать категорию '.$model->title;
}

if (Yii::app()->controller->action->id == 'catsupdate' AND $model->parent_id > 0){
    $title = 'Редактировать подкатегорию "'.$model->title.'"';
}



?>

<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <div style='width:550px; float:left;'>

    <div style='font-size:16px; padding-bottom:10px;'>
        КАТЕГОРИИ
    </div>
    <div style='font-size:24px; padding-bottom:40px;'>
        <b><? echo $title; ?></b>
    </div>
    <div class="form-group<?= $model->hasErrors('title') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'title', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'title', array('class' => 'form-control')); ?>
        <?= CHtml::error($model, 'title', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('parent_id') ? ' has-error' : ''; ?>">
        <?

        if (Yii::app()->controller->action->id == 'catscreate')
        {
            if (isset($cats_id)){
                        if ($cats_id > 0){
                            $model->parent_id = $cats_id;
                        }
            }
        }

        /*
        if (Yii::app()->controller->action->id == 'catsupdate')
        {
            if (isset($cats_id)){
                        if ($cats_id > 0){
                            $model->parent_id = $cats_id;
                        }
            }
        }
        */


        ?>
        <?= CHtml::activeLabel($model, 'parent_id', array('class' => 'control-label')); ?>
        <?=CHtml::activeDropDownList($model,'parent_id',$catslist, array('class' => 'form-control is_closed_list', 'style'=>'width:100%;')); ?>
        <?= CHtml::error($model, 'parent_id', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('sort') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'sort', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'sort', array('class' => 'form-control','style'=>'')); ?>
        <?= CHtml::error($model, 'sort', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('text') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'text', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'text', array('class' => 'form-control','style'=>'height:100px;')); ?>
        <?= CHtml::error($model, 'text', array('class' => 'help-block')); ?>
    </div>



    </div>

    <div style='clear:both;'></div>


    <div style=' margin-top:20px; margin-bottom:20px; border-top:1px dotted #ccc;'></div>




 <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

                    <div style='with:100px; float:right;'>
                <?
                if ($this->route == Yii::app()->controller->id.'/catsupdate'){
                ?>
                <a href='<?= Yii::app()->createUrl(Yii::app()->controller->id.'/catsdelete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>
</div>

<div style='clear:both;'></div>

</div>
</div>
