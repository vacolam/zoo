<div style='width:100%; '>
    <div class='title_one'>
        <div class='title_two' style='width: 1400px; '>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl(Yii::app()->controller->id.'/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'><? echo $this->razdel['name']; ?></a></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <ul style='list-style:none; padding:0px; margin:0px;'>
                            <li style='float:right; '>
                                <a href="<?=Yii::app()->createUrl('cats/update',array('id'=>$this->razdel['id']));?>" class='btn btn-outline-success' style=''>
                                    <svg class="bi bi-gear" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M8.837 1.626c-.246-.835-1.428-.835-1.674 0l-.094.319A1.873 1.873 0 014.377 3.06l-.292-.16c-.764-.415-1.6.42-1.184 1.185l.159.292a1.873 1.873 0 01-1.115 2.692l-.319.094c-.835.246-.835 1.428 0 1.674l.319.094a1.873 1.873 0 011.115 2.693l-.16.291c-.415.764.42 1.6 1.185 1.184l.292-.159a1.873 1.873 0 012.692 1.116l.094.318c.246.835 1.428.835 1.674 0l.094-.319a1.873 1.873 0 012.693-1.115l.291.16c.764.415 1.6-.42 1.184-1.185l-.159-.291a1.873 1.873 0 011.116-2.693l.318-.094c.835-.246.835-1.428 0-1.674l-.319-.094a1.873 1.873 0 01-1.115-2.692l.16-.292c.415-.764-.42-1.6-1.185-1.184l-.291.159A1.873 1.873 0 018.93 1.945l-.094-.319zm-2.633-.283c.527-1.79 3.065-1.79 3.592 0l.094.319a.873.873 0 001.255.52l.292-.16c1.64-.892 3.434.901 2.54 2.541l-.159.292a.873.873 0 00.52 1.255l.319.094c1.79.527 1.79 3.065 0 3.592l-.319.094a.873.873 0 00-.52 1.255l.16.292c.893 1.64-.902 3.434-2.541 2.54l-.292-.159a.873.873 0 00-1.255.52l-.094.319c-.527 1.79-3.065 1.79-3.592 0l-.094-.319a.873.873 0 00-1.255-.52l-.292.16c-1.64.893-3.433-.902-2.54-2.541l.159-.292a.873.873 0 00-.52-1.255l-.319-.094c-1.79-.527-1.79-3.065 0-3.592l.319-.094a.873.873 0 00.52-1.255l-.16-.292c-.892-1.64.902-3.433 2.541-2.54l.292.159a.873.873 0 001.255-.52l.094-.319z" clip-rule="evenodd"/>
                                        <path fill-rule="evenodd" d="M8 5.754a2.246 2.246 0 100 4.492 2.246 2.246 0 000-4.492zM4.754 8a3.246 3.246 0 116.492 0 3.246 3.246 0 01-6.492 0z" clip-rule="evenodd"/>
                                    </svg>
                                </a>
                            </li>

                            <li style='float:right; margin-right:10px;'>
                            <?
                            $link = Yii::app()->createUrl('shopitem/create');
                            ?>
                            <a href="<?=$link; ?>" class="btn btn-outline-success">+ товар</a>
                            </li>
                            <li style='clear:both;'></li>
                        </ul>
                    </td>
            </tr>
            </table>
        </div>
    </div>

<div class='content_one' style=' width: 1400px;'>

    <?
    $this->renderPartial('_nav_bar', array());
    ?>


    <div style='width:1340px; '>

<style>
a {
    color:#222;
}
</style>
<?
    $status_text = array(
        0 => 'Принят',
        1 => 'Готов к выдаче',
        2 => 'Частично готов к выдаче',
        1000 => 'Оплачен и выдан',
        10000 => 'Отменен продавцом',
        10001 => 'Отменен покупателем',
    );
    $status = array
    (
        0 => 'bg-warning',
        1 => 'bg-primary',
        2 => 'bg-primary',
        1000 => 'bg-success',
        10000 => 'bg-danger',
        10001 => 'bg-danger',
    );

    $status_class = 'bg-primary';
    if (isset($status[$data->status]))
    {
        $status_class = $status[$data->status];
    }

    $status_title = '';
    if (isset($status_text[$data->status]))
    {
        $status_title = $status_text[$data->status];
    }
?>
<div>
<div style='font-size:22px; padding-bottom:0px;'>
    <b>ЗАКАЗ № <?=$data->id;?></b>
    <span class="badge <?=$status_class;?>" style='font-size:16px; margin-left:20px; '><?=$status_title;?></span>
</div>
</div>

<?php

echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
    'class' => 'form_news',
));

?>

<div style='width:880px; float:left; padding-top:0px;'>
<style>
.table td{
padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;
}
.product_title{
color:#3366FF;
}

.product_title:hover{
text-decoration:underline;
}

.product_line.deleted{
text-decoration:line-through; background:#E0E0E0; color:#707070;
}

.product_line .cart_delete{
display:block;
}

.product_line .cart_redelete{
display:none;
}

.product_line.deleted .cart_delete{
display:none;
}

.product_line.deleted .cart_redelete{
display:block;
}

.product_line.deleted .product_title{
color:#808080;
}

.product_line.deleted input{
background:#EAEAEA; color:#808080;  border:1px solid #ccc;
}

.shop_order_input{
    display:none;
}

</style>
<table style='width:100%; font-size:14px; border-collapse:collapse;' class='table'>
<tr>
    <td style='border:none;'>&nbsp;</td>
    <td style='border:none;'>Цена</td>
    <td style='border:none;'>Кол-во</td>
    <td style='border:none;'>Сумма</td>
    <td style='border:none;'>&nbsp;</td>
</tr>
<?
$summ = 0;
$quontity=0;
$items = $data->orderitems;

if ($items)
{
foreach ($items as $order_item){
    $product = $order_item->product;
    ?>
    <tr style='' class='product_line <? if ($order_item->deleted == 1 OR $product->deleted == 1){echo "deleted";} ?>' rel='<?=$order_item->id;?>'>
        <td style='vertical-align:middle; border-right:1px solid #D0D0D0; padding:0px;'>
            <input type="hidden" value='<?=$order_item->id;?>' name='ShopOrdersitem[<?=$order_item->id;?>][id]'/>
            <?=$product->id;?>.
            <a href="<?= Yii::app()->createUrl('shopitem/update',array('id'=>$product->id)); ?>" style='' class='product_title'>
                <?=$product->title;?>
                <? if ($product->deleted == 1){echo " (Товар удален)";}?>
            </a>
        </td>
        <td style='width:130px; border-right:1px solid #D0D0D0; vertical-align:middle;'>
            <input type="text" value='<?=$order_item->price;?>' name='ShopOrdersitem[<?=$order_item->id;?>][price]' class='cart_price' style=' width:70px; text-align:center;'/>
            <span style='font-size:11px;'>&nbsp;РУБ.</span>
        </td>
        <td style='width:130px; border-right:1px solid #D0D0D0; vertical-align:middle;'>
            <?
            $count = $order_item->quontity;

            if ($order_item->deleted != 1)
            {
            $quontity = $quontity + $count;
            }
            ?>
            <input type="text" value='<?=$count;?>' class='cart_quontity' style='width:70px; text-align:center;' name='ShopOrdersitem[<?=$order_item->id;?>][quontity]'/>
            <span style='font-size:11px;'>&nbsp;шт.</span>
        </td>
        <td style='width:120px; border-right:1px solid #D0D0D0; vertical-align:middle;'>
            <span class='cart_sum_text'>
            <?
            $curent_summ =  $count*$order_item->price;
            if ($order_item->deleted != 1)
            {
            $summ =  $summ+$curent_summ;
            }
            echo $curent_summ;
            ?>
            </span>
            <span style='font-size:11px;'>&nbsp;РУБ.</span>
        </td>

        <td style='width:40px; cursor:pointer; text-align:center; vertical-align:middle; border-right:1px solid #D0D0D0;'>
            <?
            if ($order_item->product->deleted == 0)
            {
                ?>
                <div style='' onclick='cart_delete(<?=$order_item->id;?>)' class='cart_delete'><b>X</b></div>
                <div style='text-decoration:none; color:#000;' onclick='cart_redelete(<?=$order_item->id;?>)' class='cart_redelete' ><b>&larr;</b></div>
                <?
            }
            ?>
            <input type="hidden" value='<?=$order_item->deleted;?>' class='cart_redelete_input' name='ShopOrdersitem[<?=$order_item->id;?>][deleted]'/>
        </td>
    </tr>
    <?
}
}
?>

    <tr style='padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc; font-size:14px;'>
        <td style='' colspan=2>&nbsp;</td>
        <td style='width:30px;' class='total_count'><b><?=$quontity;?> шт.</b></td>
        <td style='width:30px;' class='total_sum'><b><?=$summ;?> РУБ.</b></td>
        <td>&nbsp;</td>
    </tr>
</table>
</div>

<div style='width:400px; float:right;'>
<table style='width:100%; font-size:14px; border-collapse:collapse;'>
                                <tr>
                                    <td style='width:100px; text-align:left; padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;' valign=top>
                                        <b>Статус:</b>
                                    </td>
                                    <td style='padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'>
                                    <?
                                        foreach($status_text as $index => $text)
                                        {
                                            $status_class = 'bg-primary';
                                            if (isset($status[$index]))
                                            {
                                                $status_class = $status[$index];
                                            }
                                            ?>
                                            <div class="form-check <? if ($index == $data->status){echo "checked";} ?>" style='margin-bottom:6px;'>
                                                <input class="form-check-input" type="radio" name="ShopOrders[status]" id="exampleRadios_<?=$index;?>" value="<?=$index;?>" <? if ($index == $data->status){echo "checked";} ?>>
                                                <label class="form-check-label" for="exampleRadios_<?=$index;?>" style=''>
                                                    <span class="badge <?=$status_class;?>" style='font-size:16px; '><?=$text;?></span>
                                                </label>
                                            </div>
                                            <?
                                        }
                                    ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:100px; text-align:left; padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                        <b>Имя:</b>
                                    </td>
                                    <td style='padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                        <div class='shop_order_text'><i><?=$data->name;?></i></div>
                                        <input type='text' value='<?=$data->name;?>' class='ShopOrders_name shop_order_input' name='ShopOrders[name]' style='height:40px; line-height:40px; width:100%;'>
                                   </td>
                                </tr>

                                <tr>
                                    <td style='width:100px; text-align:left;  padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                        <b>Телефон:</b>
                                    </td>
                                    <td style=' padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                        <div class='shop_order_text'><i><?=$this->phoneFormat($data->phone);?></i></div>
                                        <input type='text' value='<?=$data->phone;?>' class='ShopOrders_phone shop_order_input' name='ShopOrders[phone]'  style='height:40px; line-height:40px; width:100%;'>
                                    </td>
                                </tr>

                                <tr>
                                    <td style='width:100px; text-align:left; padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;' >

                                    </td>
                                    <td style=' padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'>
                                        <div style='text-transform:uppercase; font-weight:bold; ' class='shop_order_text'>
                                          <?
                                           $delivery_type = array(
                                            1 => 'Самовывоз из Зоопарка',
                                            2 => 'Доставка курьером',
                                           );

                                           if (isset($delivery_type[$data->deliverytype]))
                                           {
                                               echo $delivery_type[$data->deliverytype];
                                           }
                                           ?>
                                        </div>
                                        <select name="ShopOrders[deliverytype]" size="1" class='deliverytype shop_order_input' style='height:40px; line-height:40px; width:100%; border:2px solid #ccc; border-radius:4px;'>
                                            <option value="1" <? if ($data->deliverytype == 1){ ?>selected="selected"<? } ?> >Заберу сам в зоопарке</option>
                                            <option value="2" <? if ($data->deliverytype == 2){ ?>selected="selected"<? } ?>>Привезите курьером</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                   <td style='width:100px; text-align:left; padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                        <b>Адрес:</b>
                                   </td>
                                   <td style=' padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                   <div class='shop_order_text'><i><?=$data->adress;?></i></div>
                                   <input type='text' value='<?=$data->adress;?>' class='ShopOrders_adress shop_order_input' name='ShopOrders[adress]' style='height:40px; line-height:40px; width:100%;'>
                                   </td>
                                </tr>

                               <tr>
                                   <td style=' padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc; ' colspan=2  valign=top>
                                   <div style='padding-bottom:3px;'><b>Комментарий к заказу</b></div>
                                   <textarea  class='' name='ShopOrders[comment]' style='width:100%; height:120px; display:block; border:1px solid #ccc; border-radius:3px; font-size:12px; line-height:1.4em;'><?=$data->comment;?></textarea>
                                   </td>
                                </tr>


                                                                        <input type='text' value='<?=$data->phone;?>' class='ShopOrders_phone shop_order_input' name='ShopOrders[phone]'  style='height:40px; line-height:40px; width:100%;'>

                            </table>

                            <div onclick='edit_input_show()' style='color:#3366FF; text-decoration:underline; cursor:pointer; font-size:14px; padding-top:10px; text-align:right;'><i>Редактировать</i></div>

<!--СТАТУСЫ СМС-->
<?
$smses = $data->ordersms;
if ($smses)
{
    ?>
    <div style='margin-top:20px; width:100%; background:#fff; border:1px solid #ccc; border-radius:3px; '>
    <div style='padding:20px;'>

    <div style='padding-bottom:10px; margin-bottom:10px; border-bottom:1px dotted #ccc; font-size:14px;'>
    <b>Статусы СМС</b>
    </div>
    <?
    foreach ($smses as $sms)
    {
    ?>
    <div style='padding-bottom:10px; margin-bottom:10px; border-bottom:1px dotted #ccc; font-size:12px;'>
    <?
    $status_title = '';
    if (isset($status_text[$sms->order_status]))
    {
        $status_title = $status_text[$sms->order_status];
    }
    $status_class = 'bg-primary';
    if (isset($status[$sms->order_status]))
    {
        $status_class = $status[$sms->order_status];
    }


    echo "<div><span class='".$status_class."' style='color:#fff; padding:1px 6px; border-radius:3px;'>".$status_title."</span></div>";
    echo "<div style='padding-top:3px;'><b>".Dates::showDatetime($sms->date)."</b> &nbsp;&nbsp; ";
    echo $sms->text." | ";

    $send_status_text = '';
    if ($sms->send_status == '' AND $sms->send == 0){$send_status_text = 'Ожидает отправки оператору';}
    elseif ($sms->send_status == '' AND $sms->send == 1){$send_status_text = 'СМС Отправлено оператору';}
    elseif($sms->send_status == 'moderation'){$send_status_text = 'СМС На модерации';}
    elseif($sms->send_status == 'delivery'){$send_status_text = 'СМС Доставлено';}
    elseif($sms->send_status == 'reject'){$send_status_text = 'СМС Отклонено';}
    else{$send_status_text = $sms->send_status;}
    echo "<b>".$send_status_text."</b>";
    echo "</div>";
    ?>
    </div>
    <?


    }
    ?>
    </div>
    </div>
    <?
}
?>

</div>
<div style='clear:both;'></div>



    <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:730px; height:40px;')); ?>
            <div style='with:150px; float:right;'>
                <a href='<?= Yii::app()->createUrl('shop/orderdelete',array('id'=>$data->id)); ?>' style='width:150px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>


 </div>
 </div>

 <div style='clear:both;'></div>

 </div>




<script>
function edit_input_show(){
    if ( $('.shop_order_text').is(':visible') )
    {
        $('.shop_order_text').css('display','none');
        $('.shop_order_input').css('display','block');
    }else{
        $('.shop_order_input').css('display','none');
        $('.shop_order_text').css('display','block');
    }
}

function cart_update(){
    //пересчитываем цифры в корзине
    var count = 0;
    var summ = 0;
    $('.product_line').not('.deleted').each(function(index,obj){
        var id = $(obj).attr('rel');

        var quontity = parseInt( $(obj).find('.cart_quontity').val() );
        count = count + quontity;

        var current_price = parseInt( $(obj).find('.cart_price').val() );
        var current_sum = current_price*quontity;

        $(obj).find('.cart_sum_text').text(current_sum);

        summ = summ + current_sum;
    });

    //записываем новые конечные значения
    $('.total_count').text(count + " шт.");
    $('.total_sum').text(summ + " РУБ.");
}


function cart_delete(id)
{
    $('.product_line[rel="'+id+'"]').addClass('deleted');
    $('.product_line[rel="'+id+'"]').find('.cart_redelete_input').val(1);
    cart_update();
}

function cart_redelete(id)
{
    $('.product_line[rel="'+id+'"]').removeClass('deleted');
    $('.product_line[rel="'+id+'"]').find('.cart_redelete_input').val(0);
    cart_update();
}

$(function(){
    $('.cart_quontity,.cart_price').on('keyup',function(){
        cart_update();
    })
})

</script>