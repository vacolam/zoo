<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two' style='width: 1400px;'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <div style='padding-top:0px; font-size:40px;'><a href="<?=Yii::app()->createUrl('shop/index',array());?>" style='color:#000; padding-top:0px; font-size:40px;'>Магазин</a></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <ul style='list-style:none; padding:0px; margin:0px;'>
                            <li style='float:right; '>
                            </li>

                            <li style='float:right; margin-right:10px;'>
                            <?
                            $link = Yii::app()->createUrl('shopitem/create');
                            if ($cats_id > 0){
                            $link = Yii::app()->createUrl('shopitem/create',array('cats_id'=>$cats_id));
                            }
                            ?>
                            <a href="<?=$link; ?>" class="btn btn-outline-success">+ товар</a>
                            </li>
                            <li style='clear:both;'></li>
                        </ul>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>
    <?
    $this->renderPartial('//shop/_nav_bar', array());
    ?>

<div class="form" style='padding-top:0px;'>
<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

    <div style='width:550px; float:left; margin-top:-5px;'>

    <div class="form-group<?= $model->hasErrors('title') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'title', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'title', array('class' => 'form-control','style'=>'height:100px; width:100%;')); ?>
        <?= CHtml::error($model, 'title', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('cats_id') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'cats_id', array('class' => 'control-label')); ?>
        <?
        if (isset($cats_id)){
            if ($cats_id > 0){
                $model->cats_id = $cats_id;
            }
        }
        ?>
        <?=CHtml::activeDropDownList($model,'cats_id',$catslist, array('class' => 'form-control','style'=>'width:100%;')); ?>
        <?= CHtml::error($model, 'cats_id', array('class' => 'help-block')); ?>
    </div>




    </div>


    <div style='width:200px; float:right; '>
    <?
    $bg = "background:#f0f0f0;";

    if (!$model->isNewRecord && $model->hasImage()) {
        $getThumbnailUrl = $model->getThumbnailUrl();
        $bg = "background:url(".$getThumbnailUrl.") #f0f0f0 center; background-size:cover;";
    }

    ?>
    <div style='width:200px; height: 180px; margin-top:20px; border-radius:3px; position:relative; <?=$bg;?>' class='cover_background'>
    </div>

    </div>

    <div style='clear:both;'></div>


    <div class="form-group<?= $model->hasErrors('price') ? ' has-error' : ''; ?>" style='width:160px; float:left; margin-right:20px;'>
        <?= CHtml::activeLabel($model, 'price', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'price', array('class' => 'form-control','style'=>'width:160px;')); ?>
        <?= CHtml::error($model, 'price', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('old_price') ? ' has-error' : ''; ?>" style='width:160px; float:left;margin-right:20px;'>
        <?= CHtml::activeLabel($model, 'old_price', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'old_price', array('class' => 'form-control','style'=>'width:160px;')); ?>
        <?= CHtml::error($model, 'old_price', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group<?= $model->hasErrors('quontity') ? ' has-error' : ''; ?>" style='width:190px; float:left;'>
        <?= CHtml::activeLabel($model, 'quontity', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextField($model, 'quontity', array('class' => 'form-control','style'=>'width:190px;')); ?>
        <?= CHtml::error($model, 'quontity', array('class' => 'help-block')); ?>
    </div>

    <div style='clear:both;'></div>

    <div style='padding-bottom:10px; padding-top:10px;  border-bottom:1px dotted #ccc; margin-bottom:20px;'>
        <input type="checkbox" value='1' <? if ($model->hidden == 1){echo "checked='checked'";}?> id='hidden_checkbox' name='ShopItems[hidden]'/>
        <label for='hidden_checkbox'>Не показывать в магазине</label>
    </div>

        <?
        $this->renderPartial('//modules/_redactor', array(
            'model' => $model,
            'text_input_name' => 'text',
        ));
        ?>

    <div class="form-group<?= $model->hasErrors('short_text') ? ' has-error' : ''; ?>">
        <?= CHtml::activeLabel($model, 'short_text', array('class' => 'control-label')); ?>
        <?= CHtml::activeTextArea($model, 'short_text', array('class' => 'form-control', 'style'=>'height:120px;')); ?>
        <?= CHtml::error($model, 'short_text', array('class' => 'help-block')); ?>
    </div>




    <div style=' margin-top:20px; margin-bottom:20px; border-top:1px dotted #ccc;'></div>


 <div style='width:100%; padding:15px 0px; position:fixed; border-top:1px solid #ccc; bottom:0px; left:0px; z-index:888; background:#FDFDFD;'>
        <div style='margin-left:320px; width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'float:left; width:650px; height:40px;')); ?>

                    <div style='with:100px; float:right;'>
                <?
                if ($this->route == Yii::app()->controller->id.'/update'){
                ?>
                <a href='<?= Yii::app()->createUrl(Yii::app()->controller->id.'/copy',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-warning' >Копия</a>
                <a href='<?= Yii::app()->createUrl(Yii::app()->controller->id.'/delete',array('id'=>$model->id)); ?>' style='width:100px;' class='btn btn-danger' onclick="return confirm('Точно удаляем?');">Удалить</a>
                <?
                }
                ?>
            </div>
            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>
</div>



<?
if ($this->route == 'shopitem/update')
{
?>

<div style=" margin-top:50px; padding:10px; border:1px solid rgba(0,0,0,.1); background:#0099FF; color:#fff; border-radius:0px 0px 3px 3px;  margin-bottom:20px;">Добавляйте большие фотографии. Min ширина 600px</div>
<div style='width:100%; border:1px solid #ccc; border-radius:3px;background:#fff;'>
<table style='width:100%;'>
    <tr>
        <td style='width:60%; text-align:left; vertical-align:top; border-right:1px solid #ccc;'>
        <div style='padding:10px;'>
        <?

        $this->renderPartial('//modules/_f_photos', array(
            'photos' => $photos,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('shopitem/deletephotos', array()),
            'link_setcover'=>Yii::app()->createUrl('shopitem/setcover', array()),
        ));

        ?>
        </div>
        </td>
        <td style='text-align:left; vertical-align:top;'>
        <div style='padding:10px;'>
        <?

        $this->renderPartial('//modules/_f_docs', array(
            'docs' => $docs,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('shopitem/deletedocs', array()),
        ));

        ?>
        </div>
        </td>
    </tr>
</table>
</div>
<?

$this->renderPartial('//modules/_f_upload', array(
    'model' => $model,
    'link_add' => Yii::app()->createUrl('shopitem/filesadd', array('post_id'=>$model->id)),
));

?>
<?
}
?>
</div>
</div>

<script src="/js/croppic/croppic.js"></script>
<link href="/js/croppic/croppic.css" rel="stylesheet">
