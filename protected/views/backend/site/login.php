<style>
.passError, .emailError, .help-block{ display:none;color:#ff0000; font-size:12px; padding-top:3px;}
.input_box{ position:relative; width:300px; padding-bottom:5px;}
</style>
<div class="login" style='width:300px; height:400px; position:absolute; top:50%; left:50%; margin-left:-150px; margin-top:-250px;'>
    <div style='width:160px; height:160px; margin:0px auto; background:url(/css_tool/zoo_logo.png) center; background-size:cover;'></div>
    <form method="post" id='registration-form'>

        <div class="form-group">

            <label for="email">Введите E-mail: </label>
            <div class='input_box'>
            <input class="form-control email" type="text" name="emailing">
            <div style='width:30px; height:30px; position:absolute; top:6px; right:6px; display:none; z-index:889; font-size:10px;' class='regEmail_loading'><img src='/css_tool/d299207f-7503-4e5b-bd47-772245490bb9.gif' style='width:30px; height:30px; padding:0px; margin:0px;'/></div>
            </div>
            <div class='emailError'></div>

                <div class='input_for_pass'></div>
                <div class='passError' style=''>Ошибка: Вы не ввели пароль</div>

            <?php if ($error) { ?>
            <div class="help-block" style='display:block;'><?= $error; ?></div>
            <?php } ?>

        </div>
        <div class='btn btn-success checkEmailButton' style='width:100%;'>Продолжить</div>
        <!--
        <div class="form-group<?= $error ? ' has-error' : ''; ?>">
            <label for="password">Пароль: </label>
            <input class="form-control" type="password" name="password">
        </div>
        <button type="submit" class='btn btn-success' style='width:100%;'>Войти</button>
        -->


    </form>

</div>




<!--\\\\\\\\\\\\\\\\\\\\\\\\Регистрация\\\\\\\\\\\\\\\\\\\\\\\\\\-->
<script type="text/javascript">
show_subscribe();

function show_subscribe(){
    var formObj = $('.login');

    //Навешиваем имена на форму
    formObj.find('.checkEmailButton').on('click',function(){check_registrated_email();});
    formObj.find('.email').prop('name','RegistrationForm[email]');
    formObj.find('.email').focus();
}

function check_registrated_email(){
    var Error = false;
    var formObj = $('.login');
    var submitForm = $('#registration-form');

    //Прячем ошибки, над полями если есть.
    if (formObj.find('.emailError').css('display')=='block'){
        formObj.find('.emailError').css({'display':'none'});
    }
    if (formObj.find('.passError').css('display')=='block'){
        formObj.find('.passError').css({'display':'none'});
    }
    if (formObj.find('.help-block').css('display')=='block'){
        formObj.find('.help-block').css({'display':'none'});
    }

    //Валидация e-mail с помощью javascript
    var email = jsCodeHtmlChars(formObj.find('.email').val());
    if (mail(email)==true){
    }else{
      formObj.find('.emailError').text('Ошибка: неправильный формат электронной почты');
      formObj.find('.emailError').fadeIn(500);
      Error = true;
    }

    //если ошибок нет, отправляем e-mail на проверку
    if (Error == false){
            formObj.find('.regEmail_loading').css('display','block');
            formObj.find('.checkEmailButton').text('Проверяем e-mail...');
           	$.ajax({
                type: "GET",
                url: 'zoo.php?r=site/checkemail',
                dataType: 'json',
                data: {
                        email	:	email,
          			},
                success: function(response){
                    if(response == false)
                    {
                        $('.regEmail_loading').css('display','none');
                        formObj.find('.emailError').text('Ошибка: такой адрес не существует');
                        formObj.find('.emailError').fadeIn(500);
                        formObj.find('.checkEmailButton').html('Продолжить  &rarr;');
                    }else{
                        //Добавляем поле для ввода пароля в верстку
                        var pass_html = '';
        				pass_html = pass_html + "<div style='' class='placeholder'>Введите пароль</div>";
                        pass_html = pass_html + "<div style='' class='input_box passInput'>";
                        pass_html = pass_html + "<input type='hidden' name='RegistrationForm[tkn]' class='' value='"+response+"'>";
                        pass_html = pass_html + "<input type='text' name='RegistrationForm[password]' class='form-control regpass' rel='Введите пароль' value='' autocomplete='off'>";
                        pass_html = pass_html + "<div style='width:30px; height:30px; position:absolute; top:6px; right:6px; display:none; z-index:889; font-size:10px;' class='regPass_loading'><img src='/css_tool/d299207f-7503-4e5b-bd47-772245490bb9.gif' style='width:30px; height:30px; padding:0px; margin:0px;'/></div>";
                        pass_html = pass_html + "</div>";

                        //Плавно его открываем и активируем.
                        formObj.find('.input_for_pass').css('display','none').html(pass_html);
                        formObj.find('.input_for_pass').slideToggle();
                        formObj.find('.regpass').focus();

                        //Меняем обработчик на кнопке и текст
                        formObj.find('.checkEmailButton').html('Войти  &rarr;');
                        formObj.find('.checkEmailButton').off('click');
                        formObj.find('.checkEmailButton').on('click',function(){submitRegistration();});
                        formObj.find('.regEmail_loading').css('display','none');
                    }
                },
                error: function(response){
                }
            });

    }
}

var registrationSubmited = false;

function submitRegistration(){
    var Error = false;
    var formObj = $('.login');
    var submitForm = $('#registration-form');

    //Прячем ошибки, над полями если есть.
    if (formObj.find('.emailError').css('display')=='block'){
        formObj.find('.emailError').css({'display':'none'});
    }
    if (formObj.find('.passError').css('display')=='block'){
        formObj.find('.passError').css({'display':'none'});
    }
    if (formObj.find('.help-block').css('display')=='block'){
        formObj.find('.help-block').css({'display':'none'});
    }

    var email = jsCodeHtmlChars(formObj.find('.email').val());
    if (mail(email)==true){
    }else{
      formObj.find('.emailError').fadeIn(500);
      Error = true;
    }

    if (formObj.find('.regpass').val() == '' || formObj.find('.regpass').val() == formObj.find('.regpass').attr('rel')){
      formObj.find('.passError').text('Ошибка: Вы не ввели пароль');
      formObj.find('.passError').fadeIn(500);
      Error = true;
    }

    if (formObj.find('.regpass').val() != '' && formObj.find('.regpass').val() != formObj.find('.regpass').attr('rel') && formObj.find('.regpass').val().length < 5){
      formObj.find('.passError').text('Ошибка: минимальная длина пароля - 5 знаков');
      formObj.find('.passError').fadeIn(500);
      Error = true;
    }

    if (Error == false)
    {
        if (registrationSubmited == false)
        {
            $('.regPass_loading').css('display','block');
            formObj.find('.checkEmailButton').html('Входим...');
            submitForm.submit();
            registrationSubmited = true;
        }
    }
}


function mail(str){
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(str);
}

function numeric(str){return /^[0-9]+z/.test(str + "z");}

function jsCodeHtmlChars(r)
{
r=jsStringReplace(r,"&","&amp;");
r=jsStringReplace(r,"<","&lt;");
r=jsStringReplace(r,">","&gt;");

r=jsStringReplace(r,"&#039","'");
r=jsStringReplace(r,"&#039&#039","&quot;");
return (r);
}
function jsStringReplace(text,searchString, replaceString)
{
lengthSearchString=searchString.length;
lengthReplaceString=replaceString.length;
rezultText=text;
start_poz=0;//начальная позиция с которой начинаем поиск заданной подстроки
while ((poz=rezultText.indexOf(searchString,start_poz))!=-1)
{
firstPart=rezultText.substring(0,poz);
lengthRezultText=rezultText.length;
endPart=rezultText.substring(poz+lengthSearchString, lengthRezultText );
rezultText=firstPart+replaceString+endPart;
start_poz=poz+lengthReplaceString;
}
return (rezultText);
}

function jsStringReplace(text,searchString, replaceString)
{
lengthSearchString=searchString.length;
lengthReplaceString=replaceString.length;
rezultText=text;
start_poz=0;//начальная позиция с которой начинаем поиск заданной подстроки
while ((poz=rezultText.indexOf(searchString,start_poz))!=-1)
{
firstPart=rezultText.substring(0,poz);
lengthRezultText=rezultText.length;
endPart=rezultText.substring(poz+lengthSearchString, lengthRezultText );
rezultText=firstPart+replaceString+endPart;
start_poz=poz+lengthReplaceString;
}
return (rezultText);
}

</script>