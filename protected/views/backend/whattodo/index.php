<style>
.content_one a {
    color:#222;
}
.content_one a:hover {
    color:#0099CC;
}
</style>
<div style='width:100%;'>
    <div class='title_one'>
        <div class='title_two'>
            <table style='width:100%;'>
                <tr>
                    <td style='width:50%; height:100px; text-align:left;' valign=middle>
                        <a href="<?= Yii::app()->createUrl('home/index',array()); ?>" style='font-size:40px; color:#000;'>Главная</a>
                        <div style='margin-top:-2px; font-size:16px; text-transform:uppercase; color:#A0A0A0;'><a href="<?=Yii::app()->createUrl('whattodo/index',array());?>">Блоки Чем заняться</a></div>
                    </td>
                    <td style='width:50%; height:100px; text-align:right;' valign=middle>
                        <?= CHtml::link('Добавить блок', array('create'), array('class' => 'btn btn-outline-success')); ?>
                    </td>
            </tr>
            </table>
        </div>
    </div>

    <div class='content_one'>
<?php
foreach ($banners as $banner){

        $bg = 'background:#fff; border:1px solid #ccc;';
        $fontcolor = 'color:#000;';
        $border = '';
        if ($banner->backgroundcolor != ''){
            $bg = 'background-color:'.$banner->backgroundcolor.';';
            $border = 'border:1px solid '.$banner->backgroundcolor.';';
            $fontcolor = 'color:#fff;';
        }

    ?>
    <div style='border-radius:5px; padding-bottom:20px; margin-bottom:20px; <?=$bg;?>'>
        <div style='padding:20px 30px;'>
                    <a href="<?= Yii::app()->createUrl('whattodo/update',array('id'=>$banner->id)); ?>" style='display:block; <?=$fontcolor;?>'>
                        <div style='width: 500px;'>
                            <div style='font-size:16px; font-weight:600; text-transform:uppercase;' class='open-s'><?=$banner->title;?></div>
                            <div style='font-size:14px; line-height:1.3em; padding-top:4px;' class='open-s'><?=$banner->link;?></div>
                            <?
                            if ($banner->backgroundcolor == ''){
                            ?>
                            <div style='font-size:12px; line-height:1.3em; padding-top:10px;' class='open-s'><span style='padding:4px 10px; border-radius:3px; color:#fff; background:<?=$banner->color;?>;'><?=$banner->color;?></span></div>
                            <?
                            }
                            ?>
                        </div>
                        <div style='clear:both;'></div>
                    </a>
        </div>
    </div>
    <?
}
?>
</div>
</div>