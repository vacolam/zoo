<a href='<?=Yii::app()->createUrl('yarmarka/view',array('id'=>$data->id));?>' style='padding-bottom:20px; margin-bottom:20px; border-bottom:1px solid #ccc; color:#000; font-size:14px; display:block;'>
        <div style='width:30px; float:left; margin-right:5px;'><b><?=$data->id;?></b></div>
        <div style='width:100px; float:left;margin-right:5px; font-size:13px;'>
            <?=Dates::getName($data->date);?>
            <?
            if ($data->time !='00:00:00')
            {
                ?>
                <div style='font-size:12px; color:#707070'>время <?=$data->time;?></div>
                <?
            }
            ?>
            &nbsp;
        </div>
        <div style='width:250px; float:left;margin-right:5px;'><?=$data->name;?>&nbsp;</div>
        <div style='width:150px; float:left;margin-right:5px;'><?=$data->phone;?>&nbsp;</div>
        <div style='width:150px; float:left;margin-right:5px; word-wrap: break-word;'><?=$data->email;?>&nbsp;</div>
        <div style='width:50px; float:left;margin-right:5px;'><?=count($data->docs)+count($data->photos);?>&nbsp;</div>
        <div style='width:100px; float:right; text-align:right;'>
            <?
            $active = 'secondary';
            $name = 'Статус';
            if($data->status == 0){$name = 'Без статуса'; $active = 'outline-secondary';}
            if($data->status == 1){$name = 'Активно'; $active='success';}
            if($data->status == 2){$name = 'Неактивно'; }
            ?>
            <div class='btn btn-<?=$active;?>' style='line-height:1em; font-size:11px;'><?=$name;?></div>
        </div>
        <div style='clear:both;'></div>
</a>