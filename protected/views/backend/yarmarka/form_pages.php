
<div class='content_one'>


<div class="form" style=''>
<?php echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
)); ?>

<?php echo CHtml::errorSummary($model, null, null, array(
    'class' => 'bg-danger info',
)); ?>

       <?
        $this->renderPartial('//modules/_redactor', array(
            'model' => $model,
            'text_input_name' => 'text',
        ));
        ?>

 <div style=''>
        <div style='width:900px;'>
            <?php echo CHtml::button('Сохранить', array('type' => 'submit', 'class' => 'btn btn-success', 'style'=>'width:100%; height:40px;')); ?>
            <div style='clear:both;'></div>
        </div>
    </div>

<?php echo CHtml::endForm(); ?>
</div>

<div style='width:100%; border:1px solid #ccc; border-radius:3px; margin-top:50px; background:#fff;'>
<table style='width:100%;'>
    <tr>
        <td style='width:60%; text-align:left; vertical-align:top; border-right:1px solid #ccc;'>
        <div style='padding:10px;'>
        <?

        $this->renderPartial('//modules/_f_photos', array(
            'photos' => $photos,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('pages/deletephotos', array()),
            'link_setcover'=>Yii::app()->createUrl('pages/setcover', array()),
        ));

        ?>
        </div>
        </td>
        <td style='text-align:left; vertical-align:top;'>
        <div style='padding:10px;'>
        <?
        $this->renderPartial('//modules/_f_docs', array(
            'docs' => $docs,
            'model' => $model,
            'link_delete'=>Yii::app()->createUrl('pages/deletedocs', array()),
        ));
        ?>
        </div>
        </td>
    </tr>
</table>
</div>
<?
$this->renderPartial('//modules/_f_upload', array(
    'model' => $model,
    'link_add' => Yii::app()->createUrl('pages/filesadd', array('cats_id'=>$model->id)),
));

?>
</div>
