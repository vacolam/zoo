<style>
                .photos_add_button{width:100%; height:50px; margin-top:10px; border: 1px solid #ccc; border-radius:2px; line-height:50px; text-align:center; cursor:pointer; position:relative;}
                .photos_add_button:hover{background:#F7F7F7;}

                .upload canvas {
                    display:block;
                }

            </style>
// TODO: сделать что-нибудь. это просто туду для тоого чтобы посмотреть как работает плагин для туду
<div class='upload' style='display:none; width:100%;z-index:99999; backgrond:#f49d9d'>
<div style='width:100%; position:relative;'>
    <div style=''>
        <div style='position:relative;'>
        <div class='comments_formContainer' style='color:#2b0da8'>
                    <form class="comment_fileupload" rel='comment_' id='fileupload' action=''>

                    <div class='photos_add_button' style=''>
                        Добавить еще
                       	<input class="fileupload fileupload_input" id='fileupload_input' type="file" rel='comment_' name="files[]" multiple data-url="" style='position:absolute; top:0px; left:0px; font-size:50px; opacity:0;  cursor:pointer !important; background:#ff0000;'>
                    </div>

                    <div style='padding-left:1px; padding-right:1px; margin-bottom: 0px; display:none; height:auto; min-height:10px; padding-top:10px;' class="previews_box" rel='comment_'>
                        <div class="comment-previews" rel='comment_' style='padding-bottom:0px; '>
                        	<div class="files" style='opacity:1;'></div>
                            <div style='clear:both;'></div>
                            <div class="error_count font_sub" style='text-align:center; background:#E7ECF3; height:35px; line-height:35px; border-radius:3px; margin-bottom:5px; margin-top:10px; font-size:12px; display:none;' >За один раз можно добавлять только <b>10 файлов</b></div>
                        </div>
                    </div>

                    <div class="fileupload-buttonbar" style='display:none; width:100%; height:40px; margin-top:10px;'><button type="submit" class="start upload-button btn btn-success" style='width:100%;'>Загрузить</button></div>

                    </form>
        </div>
        </div>
    </div>
</div>
</div>

<script>
function add_photos_and_files(type){
    $('#fileupload_input').click();

}

function upload_close(){
  $('.upload').css('display','none');
}

function upload_open(){
    $('.upload').css('display','block');
}

var addedCount = 0;
var doneCount = 0;


$(function () {
     'use strict';
    // Initialize the jQuery File Upload widget:
    $('.comment_fileupload').fileupload({
        previewMaxWidth: 50,
        previewMaxHeight: 50,
        previewCrop: true,
        imageMaxWidth: 1400,
        imageMaxHeight: 800,
        disableImageResize: false,
        maxNumberOfFiles: 10,
        acceptFileTypes: /(png)|(jpe?g)|(gif)|(txt)|(pdf)|(doc)|(docx)|(ppt)|(pptx)|(xls)|(xlsx)$/i,
        change : function (e, data) {

        },
    })
    .bind('fileuploadadd', function (e, data) {
        //Функция срабатывает для КАЖДОЙ ФОТКИ
        //в момент добавления ее в интерфейс
        //$.each(data.files, function (index, file) {
            //console.log('Added file in queue: ' + file.name);
            //number_of_files = number_of_files + 1;
            //sessionStorage.setItem('number_of_files', number_of_files);
        //});
        //Подсчитываем количество фоток, которые будет закачивать.
        addedCount++;

            var form = $('.comments_formContainer');

            var photos_count = form.find('.comment-previews .files .fade').length;

            if(photos_count >= 10){
                form.find('.error_count').css('display','block');
                return false;
            }
    })
    .bind('fileuploadprocessstart', function (e) {

        //Функция срабатывает ОДИН РАЗ в самом начале, как выбрали фотки
        //Отображаем поле, в котором показываются превьюшки.
        //В обычном режиме оно скрыто.
        upload_open();
        var form = $('.comments_formContainer');
        form.find('.previews_box').css('display','block');

    })
    .bind('fileuploadprocessstop', function (e) {
      //comment_add_process();
      //alert('d');

    })
    .bind('fileuploadsend', function (e,data) {

        //Функция начинает работать Для Каждой Фотки
        //когда  пошел запрос на отправку фотки
        var form = $('.comments_formContainer');
        form.find('.cancel').css('display','none');
        form.find('.load_process').css('display','block');
    })
    .bind('fileuploadalways', function (e, data) {

        //Срабатывает каждый раз, как загрузилась или нет очередная фотка
        //Как все загрузилось, обновляем страницу.


        var form = $('.comments_formContainer');
        addedCount = form.find('.comment-previews .files .fade').length;
        doneCount++;
        if (doneCount >= addedCount) {
           window.location.href = window.location.href + '&success=ok';
        }
        //alert('done');
    })
    .bind('fileuploadstopped', function (e, data) {
       		//var comment_id = $('.comment-post[rel="new"]').find('.comment-post_in').attr('rel');
       		//getNewComment(comment_id);
    });

    // Enable iframe cross-domain access via redirect option:
    $('.comment_fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
 });


</script>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="template-upload fade" style="padding-bottom:10px; border-bottom:1px dotted #ccc; margin-bottom:10px; opacity:1;">
        <div style="width:50px; height:50px; float:left; background:#f0f0f0; margin-right:5px;">
            <span class="preview">

            </span>
        </div>
        <div style="width:195px; float:left; height:50px; overflow:hidden;">
            <div style="width:1000px; height:50px; line-height:50px; font-size:11px;" class="name">
                {%=file.name%}
            </div>
        </div>

        <div style="width:40px; height:40px; margin-top:5px; float:right; display:none;" class="load_process">
        	<div style="width:40px; height:40px; background:rgba(0,0,0,.6); border-radius:50%; z-index:991;">
            <img src="/css_tool/balls.gif" style='width:80px; height:80px; z-index:992; margin-top:-20px; margin-left:-20px;' />
            </div>
        </div>


            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled style="display:none;">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel" style="cursor:pointer; border:none; background:none; height:50px; width:30px; float:right;" title='Удалить фотографию'>
                    <div style="width:30px; height:30px; background:url(/css_tool/cross.png) center; background-size:cover;"></div>
                </button>
            {% } %}

        <div style="clear:both;"></div>
        <div style="display:none;">
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </div>
        <div style="display:none;">
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </div>
    </div>
{% }
%}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="template-download fade" style="padding-bottom:10px; border-bottom:1px dotted #ccc; margin-bottom:10px; opacity:1;">
        <div style="width:50px; height:50px; float:left; background:#f0f0f0; margin-right:5px;">
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <img src="{%=file.thumbnailUrl%}">
                {% } %}
            </span>
        </div>
        <div style="width:195px; float:left; height:50px; overflow:hidden;">
            <div style="width:1000px; height:50px; line-height:50px; font-size:11px;" class="name">
                {%=file.name%}
            </div>
        </div>
        <div style="width:20px; height:20px; float:right; margin-top:15px;">
                    <div style="width:18px; height:18px; background:url(/css_tool/checkIcon.png) center; background-size:cover; margin-top:2px;"></div>
        </div>

        <div style="display:none;">
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </div>
        <div style="display:none;">
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </div>
        <div style="display:none;">
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </div>
        <div style="clear:both;"></div>
    </div>
{% } %}
</script>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="/js/image-upload/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="/js/image-upload/misc/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="/js/image-upload/misc/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="/js/image-upload/misc/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/js/image-upload/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/js/image-upload/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="/js/image-upload/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="/js/image-upload/jquery.fileupload-image.js"></script>
<!-- The File Upload validation plugin -->
<script src="/js/image-upload/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/js/image-upload/jquery.fileupload-ui.js"></script>
<!-- The main application script -->

<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="/js/image-upload/cors/jquery.xdr-transport.js"></script>
<![endif]-->
