<?
$detect2 = new Mobile_Detect;
?>
<link rel="stylesheet" href="css_tool/jquery.ui.datepicker.css?v=4" type="text/css">
<style>
.error{
    padding:7px 12px; background:#EA8690; color:#fff; font-size:12px; margin-top:4px; border-radius:5px; display:none;
}

input[type=text], select{
    height:40px; font-size:16px; line-height:40px; width:460px; padding: 0px 15px;
    border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}

</style>

<?
$form_display = 'block';
$success_display = 'none';


if(isset($_GET['success'])) {
    $form_display = 'none';
    $success_display = 'block';
}


?>

<div class='ocenka_preview' style='display:<?=$form_display;?>;'>
    <div class='form_padding'>
    <div style='width:150px; height:120px; background:url(/css_tool/great_idea_monochromatic.png) center; background-size:cover; float:left; margin-left:20px; margin-right:40px; margin-top:20px;' class='form_photo'></div>
    <div style='' class='form_description'>
        <div class='form_title' style=''>Записаться</div>
        <div class='form_text' style=''>Вы можете записать группу на удобное для Вас время. Внимательно заполните заявку и укажите удобное время.</div>
        <div class='form_button' onclick='show_ocenka()' style='background:#007BFF;'>Начать</div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>

<?
if($detect2->isMobile())
{
?>
<div class='ocenka ocenka_preview'  style='display:none;'>
    <div class='form_padding'>
        <div class='form_description'>
            <div class='form_title'>Зайдите с компьютера</div>
            <div class='form_text'>В мобильной версии отправка заявки не работает. Зайдите на эту страницу с компьютера.</div>
        </div>
    </div>
</div>
<?
}



if(!$detect2->isMobile())
{
?>
<div class='ocenka' style='display:none; width:100%; margin-bottom:20px;'>
    <div style='padding:0px; width:100%; ' class='work_form'>

                <div style='background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); padding:40px;'>
                <form class='form_conf'>
                    <div style="" class='input-block-school_name'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Название учреждения<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="Groups[school_name]" placeholder="" class="school_name" />
                            <div class='error'>Заполните название</div>
                        </div>
                    </div>

                    <div style="margin-top:40px;width:310px;" class='input-block-name'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Имя контактного лица<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="Groups[name]" placeholder="" class="name" />
                            <div class='error'>Заполните имя</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>

                    <div style="width:310px; margin-top:40px;" class='input-block-phone'>
                    <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                               Телефон контактного лица<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="Groups[phone]" placeholder="" class="phone" />
                            <div class='error'>Заполните Телефон</div>
                        </div>
                    </div>

                </form>

                    <div style='clear:both;'></div>

                    <div style="margin-top:40px;" class='input-block-image'>
                        <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Прикрепите Файл с реквизитами организации<span style='color:#DC3545;'>*</span>
                            </div>
                    </div>



                    <div style="margin-top:10px;" class='input-block-image'>
                            <div style="background:#f0f0f0; width:495px; cursor:pointer;"  onclick="add_photos_and_files()"><div style="padding:20px; text-align:center; font-size:14px;">Выберите файлы</div></div>
                            <?

                               $this->renderPartial('//layouts/_f_upload', array(
                                                                'files_number'=>5
                                                        ));


                            ?>
                            <div class='error'>Добавьте документы</div>

                            <div></div>
                    </div>

                    <div style='clear:both;'></div>
                </div>


                <div style='' class='afishas_groups'>
                            <?
                                $this->renderPartial('_form_item', array(
                                                                'active_afisha'=>$model->id,
                                                                'groups_array' => $groups_array,
                                                        ));
                            ?>
                </div>



                    <div style='cursor:pointer; width:100%; border:none; height:80px; border-radius:4px; margin-top:20px; color:#222; line-height:80px; font-size:14px; text-align:center; background:#E9E9E9; border:1px solid #E3E3E3;   box-shadow:0px 0px 8px rgba(0,0,0,.1);' onclick='group_add()' class='new_group_add_button'>Добавить еще одно мероприятие в заявку</div>
                    <div style='cursor:pointer; width:100%; border:none; height:40px; border-radius:4px; margin-top:40px; color:#fff; line-height:40px; font-size:14px; text-align:center; background:#007BFF;  box-shadow:0px 0px 8px rgba(0,0,0,.2);' onclick='form_submit()' class='sbmt_btn'>Отправить заявку</div>
        </div>
</div>

<div class='ocenka_thnx' style='width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px; display:<?=$success_display;?>;'>
    <div style='padding:30px 20px;'>
    <div style='width:150px; height:110px; background:url(/css_tool/asset.png) center; background-size:cover; float:left; margin-left:20px; margin-right:40px; margin-top:20px;'></div>
    <div style='width:300px; float:left;'>
        <div style='font-size:36px; font-weight:200;'>Спасибо!!!</div>
        <div style='font-size:15px; line-height:1.5em; font-weight:200; padding-top:15px;'><i>Ваша заявка на мероприятие принята. Мы свяжемся с Вами для уточнения деталей.</i></div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>
<?
}
?>

<script>
function show_ocenka(){
    $('.ocenka_preview').css('display','none');
    $('.ocenka').css('display','block');
}

function check_form_errors(){
    var t = 0;
    if ($('.name').val() == ''){t = t + 1; $('.input-block-name').find('.error').css('display','block');}
    if ($('.school_name').val() == ''){t = t + 1; $('.input-block-school_name').find('.error').css('display','block');}
    if ($('.phone').val() == ''){t = t + 1; $('.input-block-phone').find('.error').css('display','block');}

//    if ($('.teachers_name').val() == ''){t = t + 1; $('.input-block-teachers_name').find('.error').css('display','block');}
//    if ($('.teachers_email').val() == ''){t = t + 1; $('.input-block-teachers_email').find('.error').css('display','block');}
//    if ($('.teachers_occupation').val() == ''){t = t + 1; $('.input-block-teachers_occupation').find('.error').css('display','block');}
//    if ($('.teachers_cell').val() == ''){t = t + 1; $('.input-block-teachers_cell').find('.error').css('display','block');}
//    if ($('.work_name').val() == ''){t = t + 1; $('.input-block-work_name').find('.error').css('display','block');}


    var photos_count = $('.files .fade').length;
    if (photos_count == 0){t = t + 1; $('.input-block-image').find('.error').css('display','block');}

    $('.group_item').each(function(e,k)
    {
            var form_id = $(this).attr('id');
            var form_rel = $(this).attr('rel');
            var form = $('#'+form_id);

            if (form.find('.kolichestvo').val() == ''){t = t + 1; form.find('.input-block-kolichestvo').find('.error').css('display','block');}
            if (form.find('.vozrast').val() == ''){t = t + 1; form.find('.input-block-vozrast').find('.error').css('display','block');}
            if (form.find('.time_id option:selected').val() == 0 || form.find('.time_id').length == 0){t = t + 1; form.find('.input-block-time_id').find('.error').css('display','block');}
               var bus_need_status = form.find('.bus_need').is(':checked');
               if (bus_need_status == true)
               {
                    if (form.find('.bus_time').val() == ''){t = t + 1; form.find('.input-block-bus_time').find('.error').css('display','block');}
                    if (form.find('.bus_route').val() == ''){t = t + 1; form.find('.input-block-bus_route').find('.error').css('display','block');}
               }else{

               }
    });


    return t;
}

function form_submit(){
    $('.error').css('display','none');
    var t = 0;
    t = check_form_errors();

    if (t == 0){
    $('.sbmt_btn').text('Отправляется...');
        var photos_count = $('.files .fade').length;
        //var form = $('.form_conf').serialize();

        var school_name = $('.school_name').val();
        var name        = $('.name').val();
        var phone       = $('.phone').val();
        var count       = $('.group_item').length;

        var Orders = new Array();

        $('.group_item').each(function(e,k){
            var form_id = $(this).attr('id');
            var form_rel = $(this).attr('rel');

            var bus_need        = $(this).find('.bus_need').is(':checked');
            if (bus_need == true){bus_need = 1;}else{bus_need = 0;}

            var y = {
            'afisha_id'     : $(this).find('.afisha_group option:selected').val(),
            'start_date'    : $(this).find('#start_date_' + form_rel).val(),
            'time_id'       : $(this).find('.time_id option:selected').val(),
            'kolichestvo'   : $(this).find('.kolichestvo').val(),
            'vozrast'       : $(this).find('.vozrast').val(),
            'week_day'      : $(this).find('.week_day').val(),
            'bus_need'      : bus_need,
            'bus_time'      : $(this).find('.bus_time').val(),
            'bus_route'     : $(this).find('.bus_route').val(),
            };

            Orders.push(y);
        });

        $.ajax({
            type: "POST",
            url: '<?= Yii::app()->createUrl('afisha/addorder', array()); ?>',
            dataType: 'json',
            data: {
                school_name     : school_name,
                name            : name,
                phone           : phone,
                Orders          : Orders,
            },
            success: function(response){
                if (response){
                            if (photos_count > 0){
                                var url = '<?=Yii::app()->createUrl('afisha/filesadd', array());?>&post_id=' + response.id;
                                $('.comment_fileupload').fileupload({
                                    url:  url
                                });
                               	$('.upload-button').trigger('click');
                            }
                }else{
                    //show_ozon('yandex_rtb_R-A-400319-2');
                }
            },
            error: function(response){
            }
        });

        console.log(Orders);
    }else{
        $('.sbmt_btn').text('Ошибка. Заполните нужную информацию...').css('background','#DC3545');
        setTimeout(function(){
        $('.sbmt_btn').text('Отправить заявку').css('background','#007BFF');
        },5000);
    }
}


$(function(){
    $('.ocenka').on('change', '.bus_need', function(){

        var form = $(this).closest('.group_item');
        //var form_id = $(this).attr('id');
        //var form_rel = $(this).attr('rel');
       var bus_need_status = form.find('.bus_need').is(':checked');
       if (bus_need_status == true)
       {
            form.find('.need_bus_form').css('display','block');
       }else{
            form.find('.need_bus_form').css('display','none');
       }
    });
})



function getTimeTable(groups_id, date, form_id){
    var form = $('#group_item_'+form_id);
    form.find('.date_select').html('<div style="width:161px; padding:0px 10px; height:36px; line-height:36px; border:2px solid #ccc; border-radius:4px; font-size:13px; background:#FDFDFD;">Ищем расписание...</div>');
    $.ajax({
        type: "GET",
        url: '<?=Yii::app()->createUrl('afisha/gettimetable',array());?>',
        dataType: 'json',
        data: {
            'groups_id':    groups_id,
            'date':         date,
        },
        success: function(response){
            if (response != false){
                form.find('.date_select').html(response);
            }else{
            }
        },
        error: function(response){
        }
    });
}

function change_group(obj){
    var form_id = $(obj).closest('.group_item').attr('id');
    var form = $('#'+form_id);
    var uniqid =  $(obj).closest('.group_item').attr('rel');
    var chosen_date = form.find('#start_date_'+uniqid).val();
    var chosen_groups = form.find('.afisha_group option:selected').val();
    getTimeTable(chosen_groups,chosen_date,uniqid);
}

var add_button_is_active = false;

function group_add(){

    if (add_button_is_active == false)
    {

        $('.new_group_add_button').text('Загрузка...');
        add_button_is_active = true;

        $.ajax({
            type: "GET",
            url: '<?=Yii::app()->createUrl('afisha/getafishaitem',array());?>',
            dataType: 'json',
            data: { },
            success: function(response){
                if (response != false){
                     $(".afishas_groups").append(response);
                     var last_group_item = $('.group_item').last();
                     var last_form_rel =  last_group_item.attr('rel');
                     var chosen_date = last_group_item.find('#start_date_' + last_form_rel).val();
                     var chosen_groups = last_group_item.find('.afisha_group option:selected').val();

                     getTimeTable(chosen_groups,chosen_date, last_form_rel);

                     $('.new_group_add_button').text('Добавить еще одно мероприятие в заявку');
                     add_button_is_active = false;
                }else{
                }
            },
            error: function(response){
            }
        });

    }
}

$(function(){
    var active_form_id =  $('.group_item').attr('rel');
    getTimeTable("<?=$model->id;?>","<?=date("Y-m-d");?>", active_form_id);
});

</script>

