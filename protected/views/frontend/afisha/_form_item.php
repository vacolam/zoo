<?
$uniq_id = uniqid();
if (!isset($active_afisha)){
    $active_afisha = 0;
}
?>
<form class='form_zayvka'>
<div style='background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); padding:40px; margin-top:20px;' id='group_item_<?=$uniq_id;?>' class='group_item' rel='<?=$uniq_id;?>'>
    <div class="form-group">
        <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
        Мероприятие<span style='color:#DC3545;'>*</span>
        </div>
        <select name="Groups[groups_id]" style='width:495px;' class='afisha_group' onchange='change_group(this);'>
            <?
            foreach($groups_array as $index => $id){
                $selected = '';
                if ($index == $active_afisha){
                    $selected = 'selected="selected"';
                }
                ?>
                <option value="<?=$index;?>" <?=$selected;?>><?=$id;?></option>
                <?
            }
            ?>
        </select>
    </div>
                     <script>
                    $( function() {
                   		$( "#datepicker_<?=$uniq_id;?>" ).datepicker(
                        {
                      		altFormat: "yy-mm-dd",
                            altField: '#start_date_<?=$uniq_id;?>',
                            closeText: 'Закрыть',
                            prevText: '&laquo;',
                            nextText: '&raquo;',
                            currentText: 'Сегодня',
                            monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                            monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                            'Июл','Авг','Сен','Окт','Ноя','Дек'],
                            dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                            dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                            dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                            weekHeader: 'Не',
                            dateFormat: 'dd MM yy',
                            firstDay: 1,
                            isRTL: false,
                            showMonthAfterYear: false,
                            yearSuffix: '',
                            onSelect: function(dateText, inst) {
                                    var form_id = $(this).closest('.group_item').attr('id');
                                    var form = $('#'+form_id);
                                    //alert(form);
                                    var chosen_date = form.find('#start_date_<?=$uniq_id;?>').val();
                                    var chosen_groups = form.find('.afisha_group option:selected').val();
                                    //var chosen_group =
                                    //var date = $(this).datepicker('getDate');
                                    //var dayOfWeek = date.getUTCDay();
                                    getTimeTable(chosen_groups,chosen_date,'<?=$uniq_id;?>');
                            }
                        });


                    });

                    </script>

                    <div style="width:310px; margin-top:40px;  float:left;" >
                        <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Выберите дату<span style='color:#DC3545;'>*</span>
                            </div>
                        <div id="datepicker_<?=$uniq_id;?>"></div>
                        <input type="hidden" name='Groups[date]' value='' id='start_date_<?=$uniq_id;?>' />
                    </div>
                    <div style="width:185px;margin-top:40px; float:right; " >
                        <div style="width:185px;" class='input-block-time_id'>
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Выберите время<span style='color:#DC3545;'>*</span>
                            </div>
                            <div class='date_select'>

                            </div>
                            <div class='error'>Нужно выбрать время</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>

                    <div style="width:310px; margin-top:30px;" class='input-block-kolichestvo'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Количество человек в группе<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="Groups[kolichestvo]" placeholder="" class="kolichestvo" />
                            <div class='error'>Заполните количество</div>
                        </div>

                    </div>

                    <div style="width:310px; margin-top:30px;" class='input-block-vozrast'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Возраст детей<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="Groups[vozrast]" placeholder="" class="vozrast" />
                            <div class='error'>Заполните возраст</div>
                        </div>

                    </div>


                    <div style='padding-top:0px; margin-top:40px; border-top:1px dotted #ccc;'></div>
                        <div style="width:310px; margin-top:30px;" class='input-block-vozrast'>
                            <div class="">
                                <label style="font-size:16px; font-weight:600; padding-bottom:7px; cursor:pointer;" for='bus_need_<?=$uniq_id;?>' >
                                    <input type="checkbox" id='bus_need_<?=$uniq_id;?>' class='bus_need' name='Groups[bus_need]' value='1'/><span >&nbsp;&nbsp;&nbsp;<b>Хотим заказать автобус</b></span>
                                </label>
                            </div>
                        </div>

                        <div class='need_bus_form' style='display:none;'>
                            <div class='input-block-bus_time'>
                                <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                    <br>Время поездки<span style='color:#DC3545;'>*</span>
                                </div>
                                <input type="text" name="Groups[bus_time]" placeholder="" class="bus_time" />
                                <div class='error'>Укажите время</div>
                            </div>

                            <div class='input-block-bus_route'>
                                <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                    <br>Маршрут поездки (адрес от пункта А до Б и обратно с указанием адреса)<span style='color:#DC3545;'>*</span>
                                </div>
                                <textarea name="Groups[bus_route]" class='bus_route' style='padding:20px; height:100px; width:460px; border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;'/></textarea>
                                <div class='error'>Укажите маршрут</div>
                            </div>

                        </div>
                </div>
    </form>


