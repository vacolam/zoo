<style>
.month_menu_item{ float:left; border:1px solid #ccc; color:#606060; border-radius:3px; margin-right:5px; margin-bottom:5px;}
.month_menu_item a{display:block; padding:5px 15px;  font-size:14px;}
.month_menu_item:hover {background:#FBFBFB; cursor:pointer;}
.month_menu_item.active {background:rgb(83,149,42); border:1px solid rgb(83,149,42); color:#fff;}
.month_menu_item.active a{color:#fff;}
.afisha_iscroll.m_2{ margin-top:30px;}
@media screen and (max-width:800px) {

.afisha_iscroll{width:320px; height:40px; overflow:hidden; padding-top:10px; padding-left:5px;  }
.afisha_iscroll_in{ height:40px; width:2500px;}
.afisha_iscroll.m_2{ margin-top:5px; }
.afisha_iscroll.m_1 .month_menu_item { background:#fff;  border: 1px solid #fff; color:#606060; border-radius:3px; margin-right:5px; margin-bottom:5px; font-size:14px; box-shadow:0px 0px 3px rgba(0,0,0,0.4);}
.afisha_iscroll.m_1 .month_menu_item a{padding:8px 15px; }

.afisha_iscroll.m_1 .month_menu_item.active{background:rgb(83,149,42); border:1px solid rgb(83,149,42); color:#fff;}
.afisha_iscroll.m_1 .month_menu_item.active a{color:#fff;}

.afisha_iscroll.m_2 .month_menu_item {margin-right:2px;}
.afisha_iscroll.m_2 .month_menu_item a{padding:3px 10px; }

}
</style>
<?
if($detect2->isMobile())
{
?>
<script>
$(function(){
var width = 0;
$('.afisha_iscroll.m_1 li.month_menu_item').each(function(e)
{
    width = width + $(this).innerWidth() + 5;
});

$('.afisha_iscroll.m_1 .afisha_iscroll_in').css('width',width + 'px');
t1 = new IScroll('.afisha_iscroll.m_1', { scrollX: true, scrollY: false, mouseWheel: true,eventPassthrough:true });

var width = 0;
$('.afisha_iscroll.m_2 li.month_menu_item').each(function(e)
{
    width = width + $(this).innerWidth() + 5;
});
$('.afisha_iscroll.m_2 .afisha_iscroll_in').css('width',width + 'px');
t2 = new IScroll('.afisha_iscroll.m_2', { scrollX: true, scrollY: false, mouseWheel: true,eventPassthrough:true });
})
</script>
<?
}
?>
<div class='afisha_iscroll m_1'>
<div class='afisha_iscroll_in'>

<ul style='padding:0px; margin:0px; list-style:none;' class='afisha_menu_1'>
<?
    $active = '';
    $active_tag = '';
    if (!isset($_GET['tag'])){$active = 'active';}else{
        if ((int)$_GET['tag'] == 0){$active = 'active';}
    }
?>
<li class='y1 month_menu_item <?=$active;?> open-s'><a href="<? echo Yii::app()->createUrl('afisha/index',array('tag'=>0,'month'=>$month)); ?>">Все</a></li>
<?
foreach ($tags as $tag)
{
    $active = '';
    if (isset($_GET['tag'])){
        if ((int)$_GET['tag'] == $tag['id']){$active = 'active'; $active_tag = (int)$_GET['tag'];}
    }

    $month_filter = array();
    if ((int)$month > 0){
        $month_filter = array('month'=>$month);
    }

    $filter_array = array_merge(array('tag'=>$tag['id']), $month_filter);
    ?>
    <li class='month_menu_item <?=$active;?> open-s'><a href="<? echo Yii::app()->createUrl('afisha/index',$filter_array); ?>"><?=$tag['name'];?></a></li>
    <?
}
?>

<li style='clear:both;'></li>
</ul>
</div>
</div>


<?

$menu = array(
            '0' => 'Все',
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        );
?>

<div class='afisha_iscroll m_2'>
<div class='afisha_iscroll_in'>
<ul style='padding:0px; margin:0px; list-style:none;'>
<?
foreach ($menu as $index => $item)
{
    $active = '';
    if ($month == $index){$active = 'active';}


    $tag_filter = array();
    if ((int)$active_tag > 0){
        $tag_filter = array('tag'=>$active_tag);
    }

    $filter_array = array_merge(array('month'=>$index),$tag_filter);

    ?>
    <li class='month_menu_item <?=$active;?> open-s'><a href="<? echo Yii::app()->createUrl('afisha/index',$filter_array); ?>"><?=$item;?></a></li>
    <?
}
?>

<li style='clear:both;'></li>
</ul>
</div>
</div>