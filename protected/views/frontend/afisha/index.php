<?
$this->pageTitle =  $this->razdel['name'].". Сахалинский зооботанический парк.";
?>
<?
$detect2 = new Mobile_Detect;
?>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left'>
<?php

$this->renderPartial('_menu', array(
    'tags' => $tags,
    'month' => $month,
    'detect2' => $detect2,
));

?>
<div style='' class='afisha_block'>
<?

$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$provider,
'itemView'=>'_view',
'viewData'=>array(
'detect2'=>$detect2
),
'template'=>"\n{items}\n{pager}",
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/site_pages.css?v=2', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));

?>
</div>
</div>


<div style='width: 300px; margin-top:0px; float:right;'  class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

    <div style='margin-top:50px;'>
        <div style='padding-bottom:5px; text-transform:uppercase; font-size:16px; '><b>Партнеры</b></div>
        <?
        $this->renderPartial('//layouts/_partners', array());
        ?>
    </div>
</div>

<div style='clear:both;'></div>



</div>
</div>