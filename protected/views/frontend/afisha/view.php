<?
$this->pageTitle =  $model->title.'. '.$this->razdel['name']." - cахалинский зооботанический парк.";

$detect2 = new Mobile_Detect;
?>
<style>
.main_container_left.news{width: 830px;}
@media screen and (max-width:800px) {
.main_container_left.news{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left news'>
<?
if(!$detect2->isMobile())
{
        $bg =  "background:none; ";
        $bg_url = '';
        if ($model->hasImage()) {
            $bg_url = $model->getThumbnailUrl();
            $bg =  '';//background:url("'.$bg_url.'") #f0f0f0 center; background-size:cover;';
        }
    ?>

    <a href="<?=Yii::app()->createUrl('afisha/view',array('id' => $model->id));?>" style='<?=$bg;?>' class='news_view_photo'>
    <?
    if ($bg_url != '')
    {
        ?>
        <img src="<?=$bg_url;?>" alt="" style='width:200px; border-radius:5px;'/>
        <?
    }
    ?>
    </a>
<?
}else{
if ($model->hasImage()) {
            $bg_url = $model->getThumbnailUrl();
            $bg =  '';//background:url("'.$bg_url.'") #f0f0f0 center; background-size:cover;';
?>
    <a href="<?=Yii::app()->createUrl('afisha/view',array('id' => $model->id));?>" style='<?=$bg;?>' class='news_view_photo'>
<?
    if ($bg_url != '')
    {
        ?>
        <img src="<?=$bg_url;?>" alt="" style='width:320px; border-radius:5px;'/>
        <?
    }
    ?>

    </a>
<?
}
}
?>


    <div class='news_view_description'>

<h2 class='news_view_title'><?= $model->title ?></h2>
<div  class='news_view_date'>
<?

                                $t = '';
                                if ($model->end_date != '0000-00-00'){$t = 'c&nbsp;&nbsp;';}
                                echo $t.Dates::getName($model->date);

                                if ($model->end_date != '0000-00-00'){
                                    echo '&nbsp;&nbsp;до&nbsp;&nbsp;'.Dates::getName($model->end_date);
                                }

                                if ($model->time != ''){
                                    echo '&nbsp;&nbsp;'.$model->time;
                                }
                                ?>
</div>

<?
if ($model->has_groups == 1)
{
?>
<div style='margin-top:40px;'>
<?
     $this->renderPartial('_form', array(
            'model'=>$model,
            'groups_array' => $groups_array,  
     ));
?>
</div>
<?
}
?>

<div style='' class='news_view_text  redactor-styles'>
    <? echo $model->text; ?>
</div>

<?
$docs = $model->docs;
$this->renderPartial('//layouts/_documents', array('docs'=>$docs));

$photos = $model->photos;
$this->renderPartial('//layouts/_photos', array('photos'=>$photos,'model'=>$model,'margin_top'=>'40'));

$this->renderPartial('//layouts/_socials', array('margin_top'=>'40'));
?>
</div>
<div style='clear:both;'></div>


</div>


<div style='width: 300px; margin-top:0px; float:right;'  class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

</div>

<div style='clear:both;'></div>
</div>
</div>



<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>