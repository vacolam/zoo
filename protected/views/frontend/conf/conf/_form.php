<?
$detect2 = new Mobile_Detect;
?>
<style>
.error{
    padding:7px 12px; background:#EA8690; color:#fff; font-size:12px; margin-top:4px; border-radius:5px; display:none;
}
</style>
<style>
input[type=text] {
    height:40px; font-size:16px; line-height:40px; width:310px; padding: 0px 15px;
    border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}

textarea.shcool, textarea.teachers_occupation {
    height:100px; font-size:16px; line-height:1.3em; width:310px; padding: 10px 15px;
    border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}
</style>

<?
$form_display = 'block';
$success_display = 'none';


if(isset($_GET['success'])) {
    $form_display = 'none';
    $success_display = 'block';
}


?>

<div class='ocenka_preview' style='width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px; display:<?=$form_display;?>;'>
    <div style='padding:30px;'>
    <div style='width:200px; height:147px; background:url(/css_tool/great_idea_monochromatic.png) center; background-size:cover; float:left; margin-left:30px; margin-right:90px; margin-top:20px;' class='form_photo'></div>
    <div style='' class='form_confa_description'>
        <div class='form_title'>Отправить работу</div>
        <div class='form_text'>Для участия в конференции заполните анкету и не забудьте прикрепить свою работу.</div>
        <div onclick='show_ocenka()' class='form_button' style='background:#007BFF;'>Начать</div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>

<?
if($detect2->isMobile())
{
?>
<div class='ocenka ocenka_preview'  style='display:none;'>
    <div class='form_padding'>
        <div class='form_description'>
            <div class='form_title'>Зайдите с компьютера</div>
            <div class='form_text'>В мобильной версии отправка заявки не работает. Зайдите на эту страницу с компьютера.</div>
        </div>
    </div>
</div>
<?
}


if(!$detect2->isMobile())
{
?>

<div class='ocenka' style='display:none; width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px;'>
    <div style='padding:70px;' class='work_form'>
            <form class='form_conf'>

                    <div style="float:left; width:310px;" class='input-block-name'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                ФИО автора<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="ConfWorks[name]" placeholder="" class="name" />
                            <div class='error'>Заполните ФИО</div>
                        </div>
                    </div>

                    <div style="float:right; width:310px;" class='input-block-city'>
                    <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Город (село) проживания, район<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="ConfWorks[city]" placeholder="" class="city" />
                            <div class='error'>Заполните название</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>

                    <div style="float:left; width:310px; margin-top:40px;" class='input-block-birth'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                &nbsp;<br>Дата рождения автора<span style='color:#DC3545;'>*</span>
                                <div style='font-size:14px; line-height:1.5em; font-weight:200; padding-top:5px; '>день &bull; месяц &bull; год</div>
                            </div>
                            <input type="text" name="ConfWorks[birth]" placeholder="" class="birth" />
                            <div class='error'>Заполните дату</div>
                        </div>

                    </div>

                    <div style="float:right; width:310px; margin-top:40px;" class='input-block-shcool'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Полное официальное наименование учебного заведения (школы, клуба, дома творчества, и т.д.), <span style='color:#DC3545;'>*</span>
                            </div>
                            <textarea type="text" name="ConfWorks[shcool]" placeholder="" class="shcool" style=''/></textarea>
                            <div class='error'>Заполните название</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>

                    <div style="float:right; width:310px; margin-top:40px;" class='input-block-class'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Класс, буква класса, секция<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="ConfWorks[class]" placeholder="" class="class" />
                            <div class='error'>Заполните это поле</div>
                        </div>
                    </div>
                    <div style='clear:both;'></div>

                    <div style='padding-top:0px; margin-top:40px; border-top:1px dotted #ccc;'></div>

                    <div style="float:left; width:310px; margin-top:40px;" class='input-block-teachers_name'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                ФИО научного руководителя<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="ConfWorks[teachers_name]" placeholder="" class="teachers_name" />
                            <div class='error'>Заполните ФИО</div>
                        </div>
                    </div>

                    <div style="float:right; width:310px; margin-top:40px;" class='input-block-teachers_email'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                E-mail руководителя<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="ConfWorks[teachers_email]" placeholder="" class="teachers_email" />
                            <div class='error'>Заполните e-mail</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>

                    <div style="float:left; width:310px; margin-top:40px;" class='input-block-teachers_occupation'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                            Должность, полное официальное наименование места работы Научного руководителя<span style='color:#DC3545;'>*</span>
                            </div>
                            <textarea type="text" name="ConfWorks[teachers_occupation]" placeholder="" class="teachers_occupation" /></textarea>
                            <div class='error'>Заполните данные</div>
                        </div>
                    </div>

                    <div style="float:right; width:310px; margin-top:40px;" class='input-block-teachers_cell'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Мобильный телефон<br>руководителя <span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="ConfWorks[teachers_cell]" placeholder="" class="teachers_cell" />
                            <div class='error'>Заполните мобильный телефон</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>

                    <div style='padding-top:0px; margin-top:40px; border-top:1px dotted #ccc;'></div>

                    <div style="float:left; width:310px; margin-top:40px;" >
                        <div class='input-block-work_name'>
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                               Название работы<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="ConfWorks[work_name]" placeholder="" class="work_name" />
                            <div class='error'>Заполните название</div>
                        </div>

                        <div style="margin-top:40px;" class='input-block-nom_id'>
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Направление<span style='color:#DC3545;'>*</span>
                            </div>
                            <select name="ConfWorks[nom_id]" size="1" class='nom_id' style='width:310px; padding:0px 10px; height:40px; line-height:40px; border:2px solid #ccc; border-radius:4px;'>
                                <option value="0" selected="selected">Выберите направление</option>
                                <?
                                if ($nominations){
                                    foreach($nominations as $nomination)
                                    {
                                        ?>
                                        <option value="<?=$nomination->id;?>"><?=$nomination->title;?></option>
                                        <?
                                    }
                                }
                                ?>
                            </select>
                            <div class='error'>Заполните Направление</div>
                        </div>


                    </div>


                        <div class='input-block-photos_link' style="float:right; width:310px;" >
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px; padding-top:40px;">
                                Фотографии к работе<span style='color:#DC3545;'>*</span>
                                <div style='font-size:14px; line-height:1.5em; font-weight:200; padding-top:5px; '>ссылку на диск в облаке с фотографиями</div>
                            </div>
                            <input type="text" name="ConfWorks[photos_link]" placeholder="" class="photos_link" />
                            <div class='error'>Заполните ссылку</div>
                            </div>
                    </form>
                        <div class='input-block-image' style="float:right; width:310px; margin-top:40px;" >
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                               Файлы работы<span style='color:#DC3545;'>*</span>
                            </div>
                            <div style='font-size:15px; line-height:1.5em; font-weight:200; padding-top:5px; padding-bottom:20px;'>
                                Загрузите
                                <ul>
                                    <li>исследовательскую работу в печатном виде с соблюдением правил оформления</li>
                                    <li>тезисы</li>
                                    <li>дневник наблюдений</li>
                                    <li>графики (если имеются)</li>
                                </ul>

                                <div style='padding-top:20px;'>Файлы в формате: doc, docx, xls, xlsx, rar, zip, pdf</div>
                            </div>

                            <div style="background:#f0f0f0; width:100%; cursor:pointer;"  onclick="add_photos_and_files()"><div style="padding:20px; text-align:center; font-size:14px;">Загрузить файлы</div></div>
                            <?

                             $this->renderPartial('//layouts/_f_upload', array(
                                'files_number'=>15
                            ));

                            ?>
                            <div class='error'>Добавьте файлы работы</div>
                            </div>




                    <div style='clear:both;'></div>

                    <div style='padding-top:0px; margin-top:40px; border-top:1px dotted #ccc;'></div>


                    <div style='cursor:pointer; width:100%; border:none; height:40px; border-radius:4px; margin-top:40px; color:#fff; line-height:40px; font-size:14px; text-align:center; background:#007BFF;' onclick='form_submit()' class='sbmt_btn'>Отправить</div>
        </div>
</div>

<div class='ocenka_thnx' style='width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px; display:<?=$success_display;?>;'>
    <div style='padding:40px;'>
    <div style='width:200px; height:147px; background:url(/css_tool/asset.png) center; background-size:cover; float:left; margin-left:0px; margin-right:80px;'></div>
    <div style='width:400px; float:left;'>
        <div style='font-size:36px; font-weight:200;'>Спасибо!!!</div>
        <div style='font-size:15px; line-height:1.5em; font-weight:200; padding-top:15px;'><i>Спасибо, что прислали свою работу на конференцию <?=$model->title; ?>. Мы ее внимательно изучим и сообщим результаты Вашему научному руководителю.</i></div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>
<?
}
?>

<script>
function show_ocenka(){
    $('.ocenka_preview').css('display','none');
    $('.ocenka').css('display','block');
}

function check_form_errors(){
    var t = 0;
    if ($('.name').val() == ''){t = t + 1; $('.input-block-name').find('.error').css('display','inline-block');}
    if ($('.city').val() == ''){t = t + 1; $('.input-block-city').find('.error').css('display','inline-block');}
    if ($('.birth').val() == ''){t = t + 1; $('.input-block-birth').find('.error').css('display','inline-block');}
    if ($('.shcool').val() == ''){t = t + 1; $('.input-block-shcool').find('.error').css('display','inline-block');}
    if ($('.class').val() == ''){t = t + 1; $('.input-block-class').find('.error').css('display','inline-block');}
    if ($('.teachers_name').val() == ''){t = t + 1; $('.input-block-teachers_name').find('.error').css('display','inline-block');}
    if ($('.teachers_email').val() == ''){t = t + 1; $('.input-block-teachers_email').find('.error').css('display','inline-block');}
    if ($('.teachers_occupation').val() == ''){t = t + 1; $('.input-block-teachers_occupation').find('.error').css('display','inline-block');}
    if ($('.teachers_cell').val() == ''){t = t + 1; $('.input-block-teachers_cell').find('.error').css('display','inline-block');}
    if ($('.work_name').val() == ''){t = t + 1; $('.input-block-work_name').find('.error').css('display','inline-block');}
    if ($('.photos_link').val() == ''){t = t + 1; $('.input-block-photos_link').find('.error').css('display','inline-block');}
    //if ($('.image').val() == ''){t = t + 1; $('.input-block-image').find('.error').css('display','block');}

    if ($('.nom_id option:selected').val() == 0){t = t + 1; $('.input-block-nom_id').find('.error').css('display','inline-block');}

    var photos_count = $('.files .fade').length;
    if (photos_count == 0){t = t + 1; $('.input-block-image').find('.error').css('display','inline-block');}
    return t;
}

function form_submit(){
    $('.error').css('display','none');
    var t = 0;
    t = check_form_errors();

    if (t == 0){
    $('.sbmt_btn').text('Отправляется...');
    var photos_count = $('.files .fade').length;
        var form = $('.form_conf').serialize();
            $.ajax({
                type: "POST",
                url: '<?= Yii::app()->createUrl('conf/addwork', array('id'=>$model->id)); ?>',
                dataType: 'json',
                data: form,
                success: function(response){
                    if (response){
                                if (photos_count > 0){
                                    var url = '<?=Yii::app()->createUrl('conf/filesadd', array());?>&post_id=' + response.id;
                                    $('.comment_fileupload').fileupload({
                                        url:  url
                                    });
                                   	$('.upload-button').trigger('click');
                                }
                    }else{
                        //show_ozon('yandex_rtb_R-A-400319-2');
                    }
                },
                error: function(response){
                }
            });
    }
}
</script>

