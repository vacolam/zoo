
<style>
.main_container_left.news{width: 830px;}
@media screen and (max-width:800px) {
.main_container_left.news{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left news'>

<div style=''>

<div  class='pages_view_title'><?=$model->title;?></div>
<div  style='padding-top:20px; color:#53952A; display:none; '>
    <?
    echo Dates::getDatetimeName($model->date);
    ?>
</div>

<?
if ($model->form_is_open == 1)
{
?>
<div style='padding-top: 40px; '>
<?
     $this->renderPartial('_form', array(
            'nominations'=>$nominations,
            'model'=>$model,
            ));
?>
</div>
<?
}
?>
<div style='padding-top: 30px;' class='redactor-styles'>
    <? echo $model->text; ?>

</div>

<?
$docs = $model->docs;
$this->renderPartial('//layouts/_documents', array('docs'=>$docs));

$photos = $model->photos;
$this->renderPartial('//layouts/_photos', array('photos'=>$photos,'model'=>$model,'margin_top'=>'40'));

$this->renderPartial('//layouts/_socials', array('margin_top'=>'40'));
?>

<?php
/*
if ($nominations)
{
    foreach($nominations as $nomination)
    {
        ?>
        <div style='padding-bottom:40px;'>
        <div style='font-size:20px; padding-bottom:15px; font-weight:400;'><?=$nomination->title;?></div>
        <?
        $works = $nomination->works;
        if ($works){
            foreach($works as $work)
            {
                ?>
                <a href="<?=$work->getImageUrl();?>" class='fancybox-media' rel='photos' style='width:115px; height:80px; display:block; float:left; margin-right:2px; margin-bottom:2px; background:url(<?=$work->getThumbnailUrl();?>); background-size:cover;'></a>
                <?
            }
        }
        ?>
        <div style='clear:both;'></div>
        </div>
        <?
    }
}
*/
?>
</div>
<div style='clear:both;'></div>
</div>


<div style='width: 300px; margin-top:0px; float:right; ' class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

</div>

<div style='clear:both;'></div>

</div>
</div>



<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>