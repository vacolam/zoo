<?
$this->pageTitle =  $this->razdel['name'].". Сахалинский зооботанический парк.";
?>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left'>
<?php

$this->renderPartial('_menu', array(
    'active_type' => $active_type,
));

?>
<?php

$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$provider,
'itemView'=>'_view',
'template'=>"\n{items}<div style='margin-top:40px;'></div>\n{pager}",
'ajaxUpdate'=>false,
    'pager'=>array(
        'nextPageLabel' => '<span>&raquo;</span>',
        'prevPageLabel' => '<span>&laquo;</span>',
        'lastPageLabel' => false,
        'firstPageLabel' => false,
        'class'=>'CLinkPager',
        'header'=>false,
        'cssFile'=>'/css_tool/site_pages.css?v=3', // устанавливаем свой .css файл
        'htmlOptions'=>array('class'=>'pager'),
    ),
));


?>
<?php

?>

<div style='clear:both;'></div>
</div>

<div style='width: 300px; margin-top:0px; float:right;'  class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

    <div style='margin-top:50px;'>
        <div style='padding-bottom:5px; text-transform:uppercase; font-size:16px; '><b>Партнеры</b></div>
        <?
        $this->renderPartial('//layouts/_partners', array());
        ?>
    </div>
</div>

<div style='clear:both;'></div>


</div>
</div>