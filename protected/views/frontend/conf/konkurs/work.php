<?

$description_arra = array();

if ($work->name != ''){$description_arra[] = 'Автор '.$work->name;}
if ($work->city != ''){$description_arra[] = 'город '.$work->city;}
if ($work->shcool != ''){$description_arra[] = 'учебное заведение '.$work->shcool;}
if ($work->class != ''){$description_arra[] = 'класс '.$work->class;}
if ($work->teachers_name != ''){$description_arra[] = 'педагог '.$work->teachers_name;}


$description_title = '';
if (count($description_arra) > 0)
{
$description_title = implode(', ',$description_arra);
}



$title = $this->pageTitle =  'Работа '.$work->name." для конкурса ".$conf->title.". Cахалинский зооботанический парк.";
?>
    <meta property="og:url"           content="<?=Yii::app()->createAbsoluteUrl('conf/work',array('id' => $work->id))?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="<?=$title;?>" />
    <meta property="og:description"   content="Голосование за работу. <?=$description_title;?>" />
    <meta property="og:image"         content="<?=$work->one_photo->getImageUrl();?>" />

<style>
.main_container_left.news{width: 830px;}
@media screen and (max-width:800px) {
.main_container_left.news{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left news'>

<div style=''>

<div class='pages_view_title'><?=$conf->title;?></div>
<div style='font-size:16px; font-weight:200;  text-align:left; margin-top:0px;  text-transform:uppercase;'><? if (isset($nom)){ echo $nom->title; } ?></div>

<div style='padding-top: 30px; '>
<?
if (isset($work->one_photo)){
    ?>
    <div style='width:100%; border-radius:3px; box-shadow:0px 0px 4px rgba(0,0,0,.2); background:#EFF2F5;'>
        <img src="<?=$work->one_photo->getImageUrl();?>" alt="" style='width:100%;  border-radius:3px 3px 0px 0px;'/>
        <div style='padding:20px 20px; text-align:center; font-size:16px; font-weight:400; line-height:1.4em; border-radius: 0px 0px 3px 3px; color:#404040'>
            <div style='padding-bottom:20px;'>
            Работа №:<? echo $work->id;?>,
            "<? echo $work->work_name;?>",
            </div>
            <table style='width:100%; border-collapse:collapse;' class='table'>
                <tr><td style='text-align:left;'><b>Автор:</b></td><td style='text-align:right;'><?=$work->name;?></td></tr>
                <tr><td style='text-align:left;'><b>Населенный пункт:</b></td><td style='text-align:right;'><?=$work->city;?></td></tr>
                <tr><td style='text-align:left;'><b>Школа, клуб, станция юннатов, дом культуры:</b></td><td style='text-align:right;'><?=$work->shcool;?></td></tr>
                <tr><td style='text-align:left;'><b>Класс:</b></td><td style='text-align:right;'><?=$work->class;?></td></tr>
                <tr><td style='text-align:left;'><b>ФИО педагога:</b></td><td style='text-align:right;'><?=$work->teachers_name;?></td></tr>
            </table>

        </div>
    </div>
    <div style='padding-top:20px;'>

    <?
    if ($conf->golosovanie_is_closed == 0){
    ?>
        <div style='text-align:center; margin-top:40px;'>
        <div style='font-size:22px; text-transform:uppercase; '><b>Проголосовать</b></div>
        <div style='font-weight:400; text-transform:uppercase; font-size:14px; padding-top:10px; width:70%; margin:0px auto;'>ставьте лайки. Один лайк - один голос. Победит та работа, которая получит больше голосов</div>



        <ul style='list-style:none; padding:0px; margin:0px; width:auto; display:inline-block; margin-top:40px;'>

        <li style='float:left; width:200px; height:40px; background:#5181B8; border-radius:3px; text-align:center; margin-right:8px;'>
            <div style='padding-top:5px;'>
                <!-- Put this script tag to the <head> of your page -->
                <script type="text/javascript" src="https://vk.com/js/api/openapi.js?168"></script>

                <script type="text/javascript">
                    VK.init({apiId: 7489543, onlyWidgets: true});
                </script>

                <!-- Put this div tag to the place, where the Like block will be -->
                <div id="vk_like" style='display:inline-block;'></div>
                <script type="text/javascript">
                VK.Widgets.Like("vk_like", {type: "button", height: 30, pageTitle : '<?=$title;?>', pageImage : '<?=$work->one_photo->getImageUrl();?>'});
                </script>
            </div>
        </li>


        <li style='float:left; width:200px; height:40px; background:#1877F2; border-radius:3px; text-align:center; margin-right:8px; display:none;'>
            <div style='padding-top:5px;'>
                 <!-- Load Facebook SDK for JavaScript -->
                <div id="fb-root"></div>
                <!--< script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v7.0&appId=448181171937022&autoLogAppEvents=1"></script>-->

                  <!-- Your like button code -->
                <div class="fb-like" data-href="<?=Yii::app()->createAbsoluteUrl('conf/work',array('id' => $work->id))?>" data-width="" data-layout="button_count" data-action="like" data-size="large" data-share="false"></div>
            </div>
        </li>

        <li style='float:left; width:200px; height:40px; background:#ED8207; border-radius:3px; text-align:center; margin-right:8px;'>
            <div style='padding-top:3px;'>
          <div id="ok_shareWidget"></div>
            <script>
            !function (d, id, did, st, title, description, image) {
              function init(){
                OK.CONNECT.insertShareWidget(id,did,st, title, description, image);
              }
              if (!window.OK || !OK.CONNECT || !OK.CONNECT.insertShareWidget) {
                var js = d.createElement("script");
                js.src = "https://connect.ok.ru/connect.js";
                js.onload = js.onreadystatechange = function () {
                if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                  if (!this.executed) {
                    this.executed = true;
                    setTimeout(init, 0);
                  }
                }};
                d.documentElement.appendChild(js);
              } else {
                init();
              }
            }(document,"ok_shareWidget","<?=Yii::app()->createAbsoluteUrl('conf/work',array('id' => $work->id))?>",'{"sz":30,"st":"rounded","ck":3,"bgclr":"ED8207","txclr":"FFFFFF"}',"","","");
            </script>
            </div>
        </li>
        <li style='clear:both;'></li>
        </ul>

        <div style='padding-top:20px; text-align:center; font-size:14px; line-height:1.7em; width:300px; margin:0px auto;'><span style='color:#fff; background:#FF0033; padding:4px 10px; border-radius:4px;'>ВНИМАНИЕ</span> В браузере Safari на iPhone и iOS голосование может не работать из-за новой политики безопасности компании Apple. Используйте для этого браузер Google Chrome.</div>

         </div>
        <?
        }
        ?>
    </div>
    <?
}
?>

</div>

<?
//$docs = $model->docs;
//$this->renderPartial('//layouts/_documents', array('docs'=>$docs));

//$photos = $model->photos;
//$this->renderPartial('//layouts/_photos', array('photos'=>$photos,'model'=>$model,'margin_top'=>'40'));

//$this->renderPartial('//layouts/_socials', array('margin_top'=>'0'));
?>

<div style='padding-top:50px;'>

</div>
</div>
<div style='clear:both;'></div>
</div>


<div style='width: 300px; margin-top:0px; float:right;'  class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

</div>

<div style='clear:both;'></div>

</div>
</div>



<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>