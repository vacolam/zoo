
        <?
        $visible = 'none';
        if ($t == 1){
        $visible = 'block';
        }
        ?>
        <div style='padding-bottom:40px; display:<?=$visible;?>' class='question' rel='<?=$question->id;?>'>
        <div style='font-size:16px; text-align:center; padding-bottom:20px;'>
            <b><?=$t;?>/<?=$count;?></b>
        </div>

        <div style='' class='question_title'><b><?=$question->question;?></b></div>

        <? if ($question->filename != ''){ ?>
        <div style='background: url(<?= $question->getImageUrl(); ?>) center; background-size:cover; ' class='description_image'>
            <div class='right'>
                <div style='width:100%; height:100%; position:relative;'>
                    <div style='' class='right_image'></div>
                </div>
            </div>
            <div class='wrong'>
                <div style='width:100%; height:100%; position:relative;'>
                    <div style='' class='wrong_image'></div>
                </div>
            </div>
        </div>
        <?
        }
        ?>

        <div style='margin-top:10px; border:1px solid #ccc; border-radius:3px; box-shadow:0px 0px 2px rgba(0,0,0,.1); '>
            <?
            $first_question = ''; $first_class='';
            $second_question = ''; $second_class='';

            $rand_number = rand(1,100);

            if($rand_number % 2 === 0){
            $first_question = $question->right_answer;
            $first_class='r';
            $second_question = $question->wrong_answer;
            $second_class='l';
            }else{
            $first_question = $question->wrong_answer;
            $first_class='l';
            $second_question = $question->right_answer;
            $second_class='r';

            }


            ?>
        <div style='border-bottom:1px solid #ccc; border-radius:3px 3px 0px 0px;' class='question_button <?=$first_class;?>'  onclick='answer(this)'><?=$first_question;?></div>
        <div style='border-radius:0px 0px 3px 3px' class='question_button <?=$second_class;?>'  onclick='answer(this)'><?=$second_question;?></div>
        <div style='clear:both;'></div>
        </div>

        <div class='description'>
           <div style='font-size:15px; line-height:1.7em; font-weight:200; text-align:left;' class='description_text'><?=$question->description;?></div>
           <div onclick='next_question(this)' class='next'>Дальше</div>
        </div>

        </div>
