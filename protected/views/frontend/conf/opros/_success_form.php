<style>
.error{
    padding:7px 12px; background:#EA8690; color:#fff; font-size:12px; margin-top:4px; border-radius:5px; display:none;
}

input[type=text] {
    height:40px; font-size:16px; line-height:40px; width:460px; padding: 0px 15px;
    border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}
@media screen and (max-width:800px) {
input[type=text] {
    height:40px; font-size:14px; line-height:40px; width:260px; padding: 0px 10px;
    border:1px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}

}

</style>


<div class='ocenka success_form' style=''>
    <div style='' class='work_form success_form_in'>

            <div style='' class='success_form_photo'></div>
            <div class='success_form_title_block' style=''>
                <div style='' class='success_form_title'>Вы победили!!!</div>
                <div class='success_form_description' style=''><i>Вы правильно ответили на все вопросы! Заполните эту форму. Если Вы уложились в правила опроса, мы с Вами свяжемся, чтобы передать подарок. :) </i></div>
            </div>

            <div style='clear:both;'></div>

            <form class='form_conf'>

                    <div style="width:100%; margin-top:50px;" class='input-block-name'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Ваше имя<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="ConfWorks[name]" placeholder="" class="name" style=''/>
                            <div class='error'>Заполните имя</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>

                    <div style="margin-top:0px;" class='input-block-phone'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                &nbsp;<br>Ваш телефон<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="ConfWorks[phone]" placeholder="" class="phone" />
                            <div class='error'>Заполните телефон</div>
                        </div>

                    </div>

            </form>


            <div style='cursor:pointer; width:100%; border:none; height:40px; border-radius:4px; margin-top:40px; color:#fff; line-height:40px; font-size:14px; text-align:center; background:#007BFF;' onclick='form_submit()' class='sbmt_btn'>Отправить</div>

    </div>
</div>

<div class='ocenka_thnx' style=' '>
    <div style='' class='ocenka_thnx_in'>
    <div style='' class='ocenka_thnx_photo'></div>
    <div class='ocenka_thnx_title_block' style=''>
        <div class='ocenka_thnx_title' style=''>Спасибо!!!</div>
        <div class='ocenka_thnx_description' style=''><i>Вы прошли опрос и отправили нам свои данные. Если все правильно, мы свяжемся с Вами, чтобы поздравить!</i></div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>


<script>
function show_ocenka(){
    $('.ocenka_preview').css('display','none');
    $('.ocenka').css('display','block');
}

function check_form_errors(){
    var t = 0;
    if ($('.name').val() == ''){t = t + 1; $('.input-block-name').find('.error').css('display','block');}
    if ($('.phone').val() == ''){t = t + 1; $('.input-block-phone').find('.error').css('display','block');}
    return t;
}

function form_submit(){
    $('.error').css('display','none');
    var t = 0;
    t = check_form_errors();

    if (t == 0){
    $('.sbmt_btn').text('Отправляется...');
    var photos_count = $('.files .fade').length;
        var form = $('.form_conf').serialize();
            $.ajax({
                type: "POST",
                url: '<?= Yii::app()->createUrl('conf/addopros', array('id'=>$model->id)); ?>',
                dataType: 'json',
                data: form,
                success: function(response){
                    if (response){
                        $('.ocenka').css('display','none');
                        $('.ocenka_thnx').css('display','block');
                    }else{
                        //show_ozon('yandex_rtb_R-A-400319-2');
                    }
                },
                error: function(response){
                }
            });
    }
}
</script>

