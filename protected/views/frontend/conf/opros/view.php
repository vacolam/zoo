
<style>
.main_container_left.news{width: 830px;}
@media screen and (max-width:800px) {
.main_container_left.news{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left news'>

<div style=''>

<div class='pages_view_title'><?=$model->title;?></div>
<div  style='padding-top:20px; color:#53952A; display:none; '>
    <?
    echo Dates::getDatetimeName($model->date);
    ?>
</div>

<div style='padding-top: 30px; ' class='redactor-styles'>
    <? echo $model->text; ?>

</div>

<?
//$docs = $model->docs;
//$this->renderPartial('//layouts/_documents', array('docs'=>$docs));

//$photos = $model->photos;
//$this->renderPartial('//layouts/_photos', array('photos'=>$photos,'model'=>$model,'margin_top'=>'40'));

//$this->renderPartial('//layouts/_socials', array('margin_top'=>'40'));
?>
<div style='' class='opros_block'>
<div class='opros_block_in'>
<style>

</style>
<?php

if ($model->opros)
{
    $t = 0;
    $count = count($model->opros);
    foreach($model->opros as $question)
    {
        $t = $t + 1;
        $this->renderPartial('opros/_question', array('question'=>$question,'model'=>$model, 't'=>$t, 'count'=>$count));
    }

?>
<div style='display:none;' class='result'>
<?
     $count = count($model->opros);
     $this->renderPartial('opros/_fail_form', array(
            'model'=>$model,
             'count'=>$count
     ));
?>
</div>

<?
if ($model->form_is_open == 1){
?>
<div style='display:none;' class='success'>
<?
     $this->renderPartial('opros/_success_form', array(
            'model'=>$model,
     ));
?>
</div>
<?
}
?>

<script>

function answer(answer){
    var a = $(answer);
    var question = a.closest('.question');

    if (question.hasClass('.checked') == false)
    {
        var question_number = question.attr('rel');
        var right = false;
        if (a.hasClass('r')){right = true;}

        a.addClass('checked');

        question.find('.right').css('display','none');
        question.find('.wrong').css('display','none');

        if (right == true){
            question.find('.right').css('display','block');

            if (question.find('.description_image').length == 0){
                question.find('.question_title').css('color','#53952A');
            }
        }else{
            question.find('.wrong').css('display','block');

            if (question.find('.description_image').length == 0){
                question.find('.question_title').css('color','#CC0000;');
            }
        }
        question.addClass('.checked');
        show_result(question_number);
    }
}

function show_result(question_number){
    var question = $('.question[rel="'+question_number+'"]');
    var text_length = question.find('.description_text').text().length;

    //if (text_length > 1){
    question.find('.description').css('display','block');
    //}else{
    //    setTimeout(function(){
    //        next_question_go(question_number);
    //    },1000);
    //}
}

function next_question(this_question){
    var question = $(this_question).closest('.question');
    var question_number = question.attr('rel');

    next_question_go(question_number);
}

function next_question_go(question_number){
    var question = $('.question[rel="'+question_number+'"]');
    var max_count = $('.question').length;
    var eq = question.index() - 1;
    var next_eq = eq + 1;

    //1. скрываем текущий ответ
    question.css('display','none');

    //2. выбираем что показывать. конец или следующий вопрос
    if (next_eq >= max_count){
        result_show();
    }else{
        var next_question = $('.question').eq(next_eq);
        next_question.css('display','block');
    }
}


function result_show(){
    //1. подсчитать количество правильных ответов
    var count_questions = $('.question').length;
    var right_answers = 0;
    var count_right_answers = 0;
    count_right_answers = $('.question_button.r.checked').length;
    $('.real_result').text(count_right_answers);
    if (count_questions == count_right_answers){
        $('.success').css('display','block');
    }else{
        $('.result').css('display','block');
    }
}

</script>
<?
}
?>
</div>
</div>
</div>
<div style='clear:both;'></div>
</div>


<div style='width: 300px; margin-top:0px; float:right; ' class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

</div>

<div style='clear:both;'></div>

</div>
</div>



<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>