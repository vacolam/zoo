<div style='padding-bottom:20px; border-bottom:1px dotted #ccc; margin-bottom:20px;;'>

<div style=''>


    <a href="<?=Yii::app()->createUrl('conf/view',array('id' => $data->id));?>" style='font-size:17px; font-weight:600; line-height:1.2em; text-transform:uppercase;'><?=$data->title;?></a>
    <?
    if (isset($this->typeList[$data->type])){
    ?>
    <span style='padding:3px 8px; background:rgb(83,149,42);  color:#fff; border-radius:3px; margin-left:5px; font-size:12px;'><?=$this->typeList[$data->type];?></span>
    <?
    }
    ?>


    <div style='' class='news_view_date'>
        <?
        echo Dates::getDatetimeName($data->date);
        ?>
    </div>


    <div style='' class='news_view_text'>
        <a href="<?=Yii::app()->createUrl('conf/view',array('id' => $data->id));?>">
    	<p><?= $data->short_text; ?></p>


        <?
        $photos = $data->some_photos;
        if ($photos){
            ?>
            <div style='padding-top:15px; display:none;'>
            <?
            foreach($photos as $index => $photo){
                $margin='margin-right:7px;';
                if ($index == 6){$margin='';}
                ?>
                <div style='width:115px; height:80px; display:block; float:left; <?=$margin;?> background:url(<?=$photo->getThumbnailUrl();?>); background-size:cover;'></div>
                <?
            }
            ?>
            <div style='clear:both;'></div>
            </div>
            <?
        }
        ?>
        </a>
    </div>
</div>

</div>