<?
$this->pageTitle =  $this->razdel['name'].". Сахалинский зооботанический парк.";
?>
<style>
.main_container_left.contacts{width: 1200px; float:none;}
@media screen and (max-width:800px) {
.main_container_left.contacts{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left'>
    <div style='' class='redactor-styles'>

    <div style='padding-bottom:20px;' class='pages_view_title'><?=$this->razdel['name'];?></div>
    <?

    echo $consult->content;

    ?>
   </div> 
<?
$this->renderPartial('//layouts/_documents', array('docs'=>$docs));

$this->renderPartial('//layouts/_photos', array('photos'=>$photos,'model'=>$consult,'margin_top'=>'40'));

$this->renderPartial('//layouts/_socials', array('margin_top'=>'40'));
?>

</div>

<div style='width: 300px; margin-top:0px; float:right;' class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

    <div style='margin-top:50px;'>
        <div style='padding-bottom:5px; text-transform:uppercase; font-size:16px; '><b>Партнеры</b></div>
        <?
        $this->renderPartial('//layouts/_partners', array());
        ?>
    </div>
</div>

<div style='clear:both;'></div>

</div>
</div>
