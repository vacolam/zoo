<?
$this->pageTitle =  $this->razdel['name'].". Сахалинский зооботанический парк.";
?>
<style>
.main_container_left.contacts{width: 1200px; float:none;}
@media screen and (max-width:800px) {
.main_container_left.contacts{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left contacts'>

<div style='' class='contacts_left_block'>
<?
$company_contacts = Page::getContatcs();
?>
        <div style='font-size:13px; text-transform:uppercase;'><b>Полное название:</b></div>
        <h2 style='color:#202020; font-weight:400;'><?=$company_contacts['company_name'];?></h2>

        <div style='padding-top:40px;font-size:13px; text-transform:uppercase;'><b>Краткое название:</b></div>
        <div style='font-size:16px; padding-top:2px; text-transform:uppercase; color:#222; font-weight:400;'><?=$company_contacts['company_shortname'];?></div>

        <div style='padding-top:40px; font-size:16px; ' class='open-s'>
            <div style='font-size:13px; text-transform:uppercase;'><b>Адрес:</b></div>
            <div style='font-size:16px; padding-top:2px; text-transform:uppercase; color:#222; font-weight:400;'><?=$company_contacts['company_city'];?>, <? echo  "<b>".$company_contacts['company_adress']."</b>, индекс ".$company_contacts['company_index'];?></div>


            <div style='padding-top:40px;font-size:13px; text-transform:uppercase;'><b>Телефоны:</b></div>
            <?
            echo "<div style='font-size:16px; padding-top:2px; text-transform:uppercase; color:#222; font-weight:400;'>";
            $phones_array = array($company_contacts['company_phone_1'],$company_contacts['company_phone_2']);
            echo implode(', ',$phones_array);
            echo "</div>";

            echo "<div style='font-size:14px; padding-top:40px;'>";
            echo "<div style='font-size:13px; text-transform:uppercase;'><b>email:</b></div>";
            echo "<div style='font-size:16px; padding-top:2px; color:#222; font-weight:400;'>";
            echo $company_contacts['company_email'];
            echo "</div>";
            echo "</div>";
            ?>

        </div>

</div>
<div class='contacts_right_block' style=''>
<?= $map; ?>
</div>

<div style='clear:both;'></div>

<div style='margin-top:20px; width:100%; background:#CEEBB7; border-radius:4px; margin-bottom:40px; color:#000; line-height:1.6em;'>
    <div style='padding:20px;'>
        <?
            echo  "<b>Учредитель:</b> ".$company_contacts['company_founder_name'].",  ".$company_contacts['company_founder_ceo'].". ".$company_contacts['company_founder_adress'].".";

            if ($company_contacts['company_founder_email'] != ''){
            ?>
            <span style='padding-left:10px;'><a href='mailto:<?=$company_contacts['company_founder_email'];?>' target="_blank" style='color:#1E98FF; border-bottom:1px solid #1E98FF;'><?=$company_contacts['company_founder_email'];?></a></span>
            <?
            }

            if ($company_contacts['company_founder_site'] != ''){
            ?>
            <span style='padding-left:10px;'><a href='<?=$company_contacts['company_founder_site'];?>' target="_blank" style='color:#1E98FF; border-bottom:1px solid #1E98FF;'><?=$company_contacts['company_founder_site'];?></a></span>
            <?
            }
        ?>
    </div>
</div>

<div style='' class='contacts_description redactor-styles'>
<?= $contacts; ?>
</div>
</div>
</div>
</div>
