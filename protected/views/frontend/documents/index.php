<?
$this->pageTitle =  $model->name.'. Сахалинский зооботанический парк.';
?>

<style>
.main_container.news{width: 830px;}
@media screen and (max-width:800px) {
.main_container.news{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container news'>
<?php
/**
 * Представление простых страниц
 *
 * @var string $text содержание текстовой страницы
 */
?>
<style>
a.doc_item{
    color:#000;
}
a.doc_item:hover{
    color:#3397FF;
}

.doc_td{
     border-bottom:1px dotted #ccc; padding-bottom:20px; padding-top:20px;
}

.doc_table{ margin-bottom:50px; }


</style>
<div style=''>
<?php
if ($cats){
    $count = count($cats);
    foreach($cats as $cat)
    {
        ?>
        <div style=''>
        <div class='documents_title'><b><?=$cat->title;?></b></div>
        <table style='width:100%; border-collapse: collapse;' class='doc_table'>
        <?
        $docs = DocumentsCats::getDocs($cat->id);
        $docs_array = $docs->getData();
        $count = count($docs_array);
        $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$docs,
        'itemView'=>'_item',
        'viewData'=>array(
            'count' => $count,
        ),
        'template'=>"\n{items}\n{pager}",
        'emptyText'=>'',
        'ajaxUpdate'=>false,
            'pager'=>array(
                'nextPageLabel' => '<span>&raquo;</span>',
                'prevPageLabel' => '<span>&laquo;</span>',
                'lastPageLabel' => false,
                'firstPageLabel' => false,
                'class'=>'CLinkPager',
                'header'=>false,
                'cssFile'=>'/css_tool/pages.css', // устанавливаем свой .css файл
                'htmlOptions'=>array('class'=>'pager'),
            ),
        ));
    ?>
    </table>
    </div>
    <?
    }
}
?>

</div>


</div>



<div style='' class='main_right_submenu'>

<?
if (count($submenu)>0){
?>
    <?
     $this->renderPartial('//pages/_submenu', array(
                'submenu' => $submenu,
            ));
    ?>

<?
}
?>
</div>

<div style='clear:both;'></div>

</div>
</div>

