<?
$detect2 = new Mobile_Detect;
?>

<style>
.main_container_left.news{width: 830px;}
@media screen and (max-width:800px) {
.main_container_left.news{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>

    <?
    if($model->show_raspisanie == 1){
        ?>
        <div style='padding-bottom:50px;'>
        <?
        if($detect2->isMobile())
        {
        $this->renderPartial('//home/_raspisanie_mobile', array('lang'=>'eng'));
        }else{
        ?>
        <div style='margin-top:-50px;'>
        <?
        $this->renderPartial('//home/_raspisanie', array('lang'=>'eng'));
        ?>
        </div>
        <?
        }
        ?>
        </div>
        <?
    }
    ?>

<div style='' class='main_container_left news'>



    <div style=''>
<?php
if ($model->filename != ''){
if ($model->hasImage()) {
    echo "<img src='".$model->getImageUrl()."' style='width:100%;'>";
}
}
?>
</div>
<h2 class='pages_view_title'>
<?
    echo $model->name;
?>
</h2>

<div style='padding-top:50px;' class='redactor-styles'>
<?
    echo $model->text;
?>
</div>


<?
$docs = $model->docs;
$this->renderPartial('//layouts/_documents', array('docs'=>$docs));

$photos = $model->photos;
$this->renderPartial('//layouts/_photos', array('photos'=>$photos,'model'=>$model,'margin_top'=>'40'));

$this->renderPartial('//layouts/_socials', array('margin_top'=>'40'));
?>

</div>



<div style='width: 300px; margin-top:0px; float:right;' class='main_right'>



    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_partners', array());
        ?>
    </div>

</div>

<div style='clear:both;'></div>

</div>
</div>

<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>
