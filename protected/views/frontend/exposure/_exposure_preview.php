    <div style='' class='expo_pr_block'>
                    <div style='display:block; padding:0px; padding-left:0px; padding-right:0px;'>
                        <div class='expo_pr_photo_block' style=''>
                            <div style='background-size:cover; border-radius:5px;' class='expo_pr_photo'>
                            <?
                            if ($data->getImageUrl()){
                            ?>
                            <img src="<?=$data->getImageUrl();?>" alt="" class='expo_pr_photo_img'/>
                            <?
                            }
                            ?>
                            </div>

                            <div style='width:250px; margin:0px auto; text-align:left; margin-top:5px;' class='expo_pr_photo_info'>
                            <div style='width:auto; display:inline-block;'>
                            <?
                            if ($data->panorama_link != ''){
                                ?>
                                <a href="<?=$data->panorama_link;?>" title='Панорама' style='width:40px; height:40px; float:left; text-align:center; margin-left:0px; margin-right:0px;'>
                                        <div style='width:40px; height:40px; margin:0px auto; border-radius:50%;'>
                                            <div style='padding-top:8px; width:25px; margin:0px auto;'>
                                            <img src="/css_tool/noun_panorama_547467.png" alt="" style='width:25px;'/>
                                            </div>
                                        </div>
                                </a>
                                <?
                            }
                            ?>

                            <?
                            if ($data->webcam_link != ''){
                                ?>
                                <a href="<?=$data->webcam_link;?>" title='Веб-камеры' style='width:40px; height:40px; float:left; text-align:center; margin-left:0px; margin-right:0px;'>
                                        <div style='width:40px; height:40px; margin:0px auto; border-radius:50%;'>
                                            <div style='padding-top:8px; width:25px; margin:0px auto;'>
                                            <img src="/css_tool/noun_webcam_2820411.png" alt="" style='width:25px;'/>
                                            </div>
                                        </div>
                                </a>
                                <?
                            }
                            ?>
                            <div style='clear:both;'></div>
                            </div>
                            </div>
                        </div>


                        <div class='expo_pr_description' style=''>
                             <div class='expo_pr_title' style='' class='open-s'><?=$data->title;?></div>


                                <?
                                    $subtitle = array();

                                    if ($data->exposure_year != '')
                                    {
                                        $subtitle[] = "<span>Экспозиция работает с ".$data->exposure_year." г.</span>";
                                    }

                                    if ($animals){
                                        if (count($animals) > 0){
                                            $subtitle[] = "<span>Виды животных: ".count($animals)."</span>";
                                        }
                                    }

                                    if (count($subtitle) > 0)
                                    {
                                        ?>
                                        <div class='expo_pr_subtitle' style=''>
                                        <?
                                        echo implode("&nbsp;&bull;&nbsp;",$subtitle);
                                        ?>
                                        </div>
                                        <?
                                    }
                                ?>

                            <?
                            $show_more = false;
                            $count = 0;
                            if ($data->text != '')
                            {
                                $count = count(explode(' ', $data->text));
                            }
                            if ($count > 30){$show_more = true;}

                            if ( $data->text != ''){
                            ?>
                            <div style='' class='open-s text-short expo_pr_shortext'>
                                <? echo $this->crop_str_word( $data->text, 30 ).""; ?>
                                <?
                                if ($show_more == true){
                                ?>
                                <span onclick='show_full()' style='cursor:pointer; color:#3397ff; text-decoration:underline;'>Подробнее</span>
                                <?
                                }
                                ?>
                            </div>
                            <div style=' display:none;' class='open-s text-full expo_pr_text'>
                                <?=$data->text;?>
                            </div>

                            <script>
                            function show_full(){
                                $('.text-short').css('display','none');
                                $('.text-full').css('display','block');
                            }
                            </script>
                            <?
                            }


                            if (count($data->photos) > 1)
                            {
                                $photos = $data->photos;
                            }else{
                                $photos = Exposure::getSomeAnimalsPhotos($data->id);
                            }

                            if (count($photos) > 0){
                            ?>
                                <div style='' class='expo_pr_somephotos_block'>
                                <?
                                $t=0;
                                $last_image = '';
                                foreach ($photos as $photo)
                                {
                                    if ($photo->id == $data->cover_id){continue;}
                                    $t++;
                                    $display = '';
                                    if ($t > 5){$display = 'display:none;';}
                                    if ($t == 6){
                                        $last_image = "<a href='".$photo->getImageUrl()."'  rel='photos' style='' class='expo_pr_photos_more fancybox-media'>&bull;&bull;&bull;</a>";
                                    }
                                    ?>
                                    <a href="<?=$photo->getImageUrl();?>" class='fancybox-media expo_pr_photos_item' rel='photos' style='  background:url(<?=$photo->getThumbnailUrl();?>) center #53952A; background-size:cover; <?=$display;?>'></a>
                                    <?
                                }
                                echo $last_image;
                                ?>
                                <div style='clear:both;'></div>
                                </div>
                                <?
                            }
                            ?>

                            <div style='clear:both;'></div>
                        </div>
                        <div style='clear:both;'></div>
                    </div>
    </div>

<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>