<?
$detect2 = new Mobile_Detect;
$exposure_name = '';

if ($exposure){
$exposure_name = " ".$exposure->title;
}
$this->pageTitle =  $model->title.". Экспозиция".$exposure_name.". Сахалинский зооботанический парк.";
?>
<style>
.main_container_left.news{width: 830px;}
@media screen and (max-width:800px) {
.main_container_left.news{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left news'>
    <div style='' >



<?
$r = array();
if ($model->exposure)
{
$r[] = $model->exposure->title;
}

if ($model->exposurecats)
{
$r[] = $model->exposurecats->title;
}

?>


    <?
         $bg =  "background:#53952A; ";
         $bg_url = '';
         if ($model->hasImage())
         {
            $bg_url = $model->getImageUrl();
            $bg =  "";//'background:url("'.$bg_url.'") #53952A center; background-size:cover;';
         }
         ?>
            <div style='' class='animal_photo_block'>

                    <div style='' class='animal_photo_bg'>
                    <div style='' class='animal_photo_bg_relative'>
                        <div class='animal_photo_absolute' style=''>
                            <div class='animal_title' style=''><?= $model->title ?></div>
                            <?
                                if (count($r)>0){
                                    echo "<div>".implode('&nbsp;&bull;&nbsp;',$r)."</div>";
                                }
                            ?>

                            <div>
                                <ul style='list-style:none; padding:0px; margin:0px;'>
                                <?
                                if ($model->redbook == 1){
                                    echo "<li class='animal_redbook_icon' style='' title='Занесен в красную книгу'><img src='/css_tool/noun_Book_3542.png'  style='' class='animal_redbook_icon_img'/></li>";
                                }

                                if ($model->opeka == 1){
                                    echo "<li class='animal_opeka_icon' style='' title='Находится под опекой'><img src='/css_tool/noun_care_2757905.png'  style='' class='animal_opeka_icon_img'/></li>";
                                }
                                ?>
                                <li style='clear:both;'></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <img src="<?=$bg_url;?>" alt="" class='animal_photo_block_img'/>
            </div>


<?
                            $photos = $model->photos;
                            if (count($photos) > 1)
                            {
                                ?>
                                <div style='margin-top:15px;'>
                                <?
                                $t=0;
                                $last_image = '';
                                foreach ($photos as $photo)
                                {
                                    if ($photo->id == $model->cover_id){continue;}
                                    $t++;
                                    $display = '';
                                    if ($t > 5){$display = 'display:none;';}
                                    if ($t == 6){
                                        $last_image = "<a href='".$photo->getImageUrl()."' class='animal_photos_more fancybox-media' rel='photos' style=''>&bull;&bull;&bull;</a>";
                                    }
                                    ?>
                                    <a href="<?=$photo->getImageUrl();?>" class='fancybox-media animal_photo_item' rel='photos' style='  background:url(<?=$photo->getThumbnailUrl();?>) #53952A center; background-size:cover; <?=$display;?>'></a>
                                    <?
                                }
                                echo $last_image;
                                ?>
                                <div style='clear:both;'></div>
                                </div>
                                <?
                            }
                            ?>

<?
$custodian = $model->custodian;
if ($custodian)
{
$this->renderPartial('custodian', array('custodian'=>$custodian));
}
?>

<div style='' class='animal_description redactor-styles'>
    <?
    if ($model->text != ''){
        echo $model->text;
    }else{
        echo nl2br($model->short_text);
    }
    ?>

</div>

<?
$docs = $model->docs;
$this->renderPartial('//layouts/_documents', array('docs'=>$docs));

$this->renderPartial('//layouts/_socials', array('margin_top'=>'40'));
?>
<div style='clear:both;'></div>
</div>


<?
$news = $model->news;
if (count($news) > 0)
{
    ?>
    <div style='' class='animal_news'>
        <div class='animal_news_title' style=''><b>Новости</b></div>
        <?
        $t = 0;
        foreach ($news as $item)
        {
            $t++;
                        $this->renderPartial('_news_item', array(
                                'data'=>$item,
                                't'=>$t,
                                'detect2'=>$detect2,
                            ));
        }
        ?>
        </div>
        <?

}
?>


<div style='clear:both;'></div>


</div>


<div style='width: 300px; margin-top:0px; float:right; ' class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

</div>

<div style='clear:both;'></div>

</div>
</div>



<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>