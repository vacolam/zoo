<?
$this->pageTitle =  $this->razdel['name'].". Сахалинский зооботанический парк.";
?>
<?
$detect2 = new Mobile_Detect;
?>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left'>
<div style='padding-top:0px;'>
        <div>
            <!--БЛОК ПАНОРАМЫ-->
            <a href="<?=Yii::app()->createUrl('pano/index',array());?>" style='display:block;  margin-right:10px;   background:url(/upload/pano/terra-sm.jpg) #53952A center; background-size:cover; ' class='home_expoitem_block'>
                <div style='position:relative; width:100%; height:100%; background-image: linear-gradient(-180deg, rgba(0,0,0,0) 38%, rgba(0,0,0,0.65) 79%); border-radius:4px'>
                    <div style='width:100%; position:absolute; left:0px; bottom:10px;'>
                        <div style='' class='home_expoitem_padding'>
                            <div style='' class='home_expoitem_title open-s'>Панорамы</div>
                        </div>
                    </div>
                </div>
            </a>
            <!--/////////-->
            <?php
            $expo = Exposure::getAll();
            $t = 0; //счетчик блоков в ряду
            $r = 0; //счетчик рядов
            if ($expo) {
                foreach ($expo as $item) {
                    $t++;
                    $r++;
                    $max_t = 5;
                    if($detect2->isMobile())
                    {
                    $max_t = 3;
                    }
                    if ($t==$max_t){$t=1;}
                    if ($t==1 AND $r==1){$t=2;}
                    $this->renderPartial('_exposure_item', array(
                                'data'=>    $item,
                                't'=>       $t,
                                'max_t'=>   $max_t,
                            ));
            }
                ?>
                <div style='clear:both;'></div>
                <?php
            }
            ?>
            <div style='clear:both;'></div>
        </div>
</div>
</div>


<div style='width: 300px; margin-top:0px; float:right;' class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

    <div style='margin-top:50px;'>
        <div style='padding-bottom:5px; text-transform:uppercase; font-size:16px; '><b>Партнеры</b></div>
        <?
        $this->renderPartial('//layouts/_partners', array());
        ?>
    </div>
</div>

<div style='clear:both;'></div>



</div>
</div>