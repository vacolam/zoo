<?
$this->pageTitle =  $model->title.". Сахалинский зооботанический парк.";
?>

<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left'>
    <div style=''>
    <?
    $this->renderPartial('_exposure_preview', array(
    'data' => $model,
    'animals' => $animals,
    ));
    ?>
    </div>

    <div>
        <?
            if ($animals){
                $previous_CatID = 0;
                $t = 0;
                foreach($animals as $animal)
                {
                    $current_CatID = $animal->cats_id;
                    $t ++;
                    if ($t==3){$t=1;}

                    if ($previous_CatID != $current_CatID)
                    {
                        ?>
                        <div style='clear:both;'></div>
                        <?
                        if (isset($catslist[$current_CatID]))
                        {
                            ?><div style='' class='expo_type_title'></div><?
                        }

                        $t = 1;
                    }



                    $this->renderPartial('_animal_item2', array(
                    'data'      => $animal,
                    'catslist'  => $catslist,
                    't'         => $t,
                    ));

                    $previous_CatID = $current_CatID;
                }
            }
        ?>

        <div style='clear:both;'></div>
    </div>



</div>


<div style='width: 300px; margin-top:0px; float:right;' class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

    <div style='margin-top:50px;'>
        <div style='padding-bottom:5px; text-transform:uppercase; font-size:16px; '><b>Партнеры</b></div>
        <?
        $this->renderPartial('//layouts/_partners', array());
        ?>
    </div>
</div>

<div style='clear:both;'></div>



</div>
</div>