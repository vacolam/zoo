<link rel="stylesheet" href="css_tool/jquery.ui.datepicker.css?v=4" type="text/css">
<style>
.error{
    padding:7px 12px; background:#EA8690; color:#fff; font-size:12px; margin-top:4px; border-radius:5px; display:none;
}
</style>
<style>
input[type=text] {
    height:40px; font-size:16px; line-height:40px; width:310px; padding: 0px 15px;
    border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}

</style>

<?
$form_display = 'block';
$success_display = 'none';


if(isset($_GET['success'])) {
    $form_display = 'none';
    $success_display = 'block';
}


?>

<div class='ocenka_preview' style='width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px; display:<?=$form_display;?>;'>
    <div style='padding:30px;'>
    <div style='width:200px; height:147px; background:url(/css_tool/great_idea_monochromatic.png) center; background-size:cover; float:left; margin-left:30px; margin-right:90px; margin-top:20px;'></div>
    <div style='width:400px; float:left;'>
        <div style='font-size:30px; font-weight:500;'>Записаться</div>
        <div style='font-size:16px; font-weight:200; padding-top:20px;'>Вы можете записать группу на удобное для Вас время. Внимательно заполните заявку и укажите удобное время.</div>
        <div onclick='show_ocenka()' style='cursor:pointer; width:150px; height:40px; border-radius:4px; margin-top:20px; color:#fff; line-height:40px; font-size:14px; text-align:center; background:#007BFF;'>Начать</div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>

<div class='ocenka' style='display:none; width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px;'>
    <div style='padding:70px;' class='work_form'>
            <form class='form_conf'>

                    <div style="float:left; width:310px;" class='input-block-school_name'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Название учреждения<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="GroupsOrders[school_name]" placeholder="" class="school_name" />
                            <div class='error'>Заполните название</div>
                        </div>
                    </div>

                    <div style="float:right; width:310px;" class='input-block-name'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Имя контактного лица<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="GroupsOrders[name]" placeholder="" class="name" />
                            <div class='error'>Заполните имя</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>

                    <div style="float:left; width:310px; margin-top:40px;" class='input-block-phone'>
                    <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                               Телефон контактного лица<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="GroupsOrders[phone]" placeholder="" class="phone" />
                            <div class='error'>Заполните Телефон</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>

                    <div style='padding-top:0px; margin-top:40px; border-top:1px dotted #ccc;'></div>


                    <div style="float:left; width:310px; margin-top:20px;" class='input-block-kolichestvo'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                &nbsp;<br>Количество человек в группе<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="GroupsOrders[kolichestvo]" placeholder="" class="kolichestvo" />
                            <div class='error'>Заполните количество</div>
                        </div>

                    </div>

                    <div style="float:right; width:310px; margin-top:20px;" class='input-block-vozrast'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                &nbsp;<br>Возраст детей<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="GroupsOrders[vozrast]" placeholder="" class="vozrast" />
                            <div class='error'>Заполните возраст</div>
                        </div>

                    </div>

                    <div style='clear:both;'></div>

                    <div style='padding-top:0px; margin-top:40px; border-top:1px dotted #ccc;'></div>
                    <script>
                    $( function() {
                   		$( "#datepicker" ).datepicker(
                        {
                      		altFormat: "yy-mm-dd",
                            altField: '#start_date',
                            closeText: 'Закрыть',
                            prevText: '&laquo;',
                            nextText: '&raquo;',
                            currentText: 'Сегодня',
                            monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                            monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                            'Июл','Авг','Сен','Окт','Ноя','Дек'],
                            dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                            dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                            dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                            weekHeader: 'Не',
                            dateFormat: 'dd MM yy',
                            firstDay: 1,
                            isRTL: false,
                            showMonthAfterYear: false,
                            yearSuffix: '',
                            onSelect: function(dateText, inst) {
                                    var chosen_date = $('#start_date').val();
                                    //var date = $(this).datepicker('getDate');
                                    //var dayOfWeek = date.getUTCDay();
                                    getTimeTable(chosen_date);
                            }
                        });


                    });

                    function getTimeTable(date){
                        $('.date_select').html('<div style="width:286px; padding:0px 10px; height:36px; line-height:36px; border:2px solid #ccc; border-radius:4px; font-size:13px; background:#FDFDFD;">Ищем свободное время...</div>');
                        $.ajax({
                            type: "GET",
                            url: '<?=Yii::app()->createUrl('groups/gettimetable',array());?>',
                            dataType: 'json',
                            data: {
                                'groups_id':    <?=$model->id;?>,
                                'date':         date,
                            },
                            success: function(response){
                                if (response != false){
                                    $('.date_select').html(response);
                                }else{
                                }
                            },
                            error: function(response){
                            }
                        });
                    }
                    $(function(){
                        getTimeTable("<?=date("Y-m-d");?>");
                    });
                    </script>

                    <div style="float:left; width:310px; margin-top:40px;" >
                        <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Выберите дату<span style='color:#DC3545;'>*</span>
                            </div>
                        <div id="datepicker"></div>
                        <input type="hidden" name='GroupsOrders[date]' value='' id='start_date' />
                    </div>
                    <div style="float:right; width:310px;" >
                        <div style="margin-top:40px;" class='input-block-time_id'>
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Выберите время<span style='color:#DC3545;'>*</span>
                            </div>
                            <div class='date_select'>

                            </div>
                            <div class='error'>Нужно выбрать время</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>

                     </form>

                    <div style='padding-top:0px; margin-top:40px; border-top:1px dotted #ccc;'></div>

                    <div style="float:left; width:310px; margin-top:40px;" class='input-block-image'>
                        <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Прикрепите документы<span style='color:#DC3545;'>*</span>
                            </div>

                            <div style='padding-top:20px;'>
                                <ul style='padding:0px; margin:0px; '>
                                    <li style='margin-left:20px;'>Файл с реквизитами предприятия</li>
                                </ul>
                            </div>
                    </div>

                    <div style="float:right; width:310px; margin-top:40px;" class='input-block-image'>
                            <div style="background:#f0f0f0; width:100%; cursor:pointer;"  onclick="add_photos_and_files()"><div style="padding:20px; text-align:center; font-size:14px;">Выберите файлы</div></div>
                            <?

                            $this->renderPartial('_f_upload', array());

                            ?>
                            <div class='error'>Добавьте документы</div>

                            <div></div>
                    </div>

                    <div style='clear:both;'></div>

                    <div style='padding-top:0px; margin-top:40px; border-top:1px dotted #ccc;'></div>


                    <div style='cursor:pointer; width:100%; border:none; height:40px; border-radius:4px; margin-top:40px; color:#fff; line-height:40px; font-size:14px; text-align:center; background:#007BFF;' onclick='form_submit()' class='sbmt_btn'>Отправить</div>
        </div>
</div>

<div class='ocenka_thnx' style='width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px; display:<?=$success_display;?>;'>
    <div style='padding:40px;'>
    <div style='width:200px; height:147px; background:url(/css_tool/asset.png) center; background-size:cover; float:left; margin-left:0px; margin-right:80px;'></div>
    <div style='width:400px; float:left;'>
        <div style='font-size:36px; font-weight:200;'>Спасибо!!!</div>
        <div style='font-size:15px; line-height:1.5em; font-weight:200; padding-top:15px;'><i>Ваша заявка на мероприятие <?=$model->title; ?> принята. Мы свяжемся с Вами для уточнения деталей.</i></div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>


<script>
function show_ocenka(){
    $('.ocenka_preview').css('display','none');
    $('.ocenka').css('display','block');
}

function check_form_errors(){
    var t = 0;
    if ($('.name').val() == ''){t = t + 1; $('.input-block-name').find('.error').css('display','block');}
    if ($('.kolichestvo').val() == ''){t = t + 1; $('.input-block-kolichestvo').find('.error').css('display','block');}
    if ($('.school_name').val() == ''){t = t + 1; $('.input-block-school_name').find('.error').css('display','block');}
    if ($('.vozrast').val() == ''){t = t + 1; $('.input-block-vozrast').find('.error').css('display','block');}
    if ($('.phone').val() == ''){t = t + 1; $('.input-block-phone').find('.error').css('display','block');}
    if ($('.teachers_name').val() == ''){t = t + 1; $('.input-block-teachers_name').find('.error').css('display','block');}
    if ($('.teachers_email').val() == ''){t = t + 1; $('.input-block-teachers_email').find('.error').css('display','block');}
    if ($('.teachers_occupation').val() == ''){t = t + 1; $('.input-block-teachers_occupation').find('.error').css('display','block');}
    if ($('.teachers_cell').val() == ''){t = t + 1; $('.input-block-teachers_cell').find('.error').css('display','block');}
    if ($('.work_name').val() == ''){t = t + 1; $('.input-block-work_name').find('.error').css('display','block');}

    if ($('.time_id option:selected').val() == 0 || $('.time_id').length == 0){t = t + 1; $('.input-block-time_id').find('.error').css('display','block');}

    var photos_count = $('.files .fade').length;
    if (photos_count == 0){t = t + 1; $('.input-block-image').find('.error').css('display','block');}
    return t;
}

function form_submit(){
    $('.error').css('display','none');
    var t = 0;
    t = check_form_errors();

    if (t == 0){
    $('.sbmt_btn').text('Отправляется...');
    var photos_count = $('.files .fade').length;
        var form = $('.form_conf').serialize();
            $.ajax({
                type: "POST",
                url: '<?= Yii::app()->createUrl('groups/addorder', array('id'=>$model->id)); ?>',
                dataType: 'json',
                data: form,
                success: function(response){
                    if (response){
                                if (photos_count > 0){
                                    var url = '<?=Yii::app()->createUrl('groups/filesadd', array());?>&post_id=' + response.id;
                                    $('.comment_fileupload').fileupload({
                                        url:  url
                                    });
                                   	$('.upload-button').trigger('click');
                                }
                    }else{
                        //show_ozon('yandex_rtb_R-A-400319-2');
                    }
                },
                error: function(response){
                }
            });
    }
}
</script>

