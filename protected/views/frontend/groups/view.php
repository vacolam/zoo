<?
$this->pageTitle =  $model->title.". Групповые мероприятия сахалинского Зоопарка.";
?>


<style>
.post_text a{
    color:#3397ff; text-decoration:underline;
}
</style>
<div style='width:100%; background:rgba(253, 253, 253, 1);'>
<div style='width: 1200px; margin:0px auto;  padding-top:90px; padding-bottom:90px;'>
<div style='width: 830px; float:right;'  class='post_text'>



    <div style='width:100%; margin-top:-10px;'>

<h2 style='font-size:25px; line-height:1.2em;'><?= $model->title ?></h2>
<div  style='padding-top:20px; color:#53952A; display:none;'>
    <?
    echo Dates::getDatetimeName($model->date);
    ?>
</div>

    <?
        $bg =  "background:none; ";
         if ($model->hasImage()) {
            $bg_url = $model->getImageUrl();
            $bg =  'background:url("'.$bg_url.'") #f0f0f0 center; background-size:cover;';
    ?>
<div style='display:None; margin-top:40px;  width:830px; height:450px; border-radius:5px;  <?=$bg;?>'></div>
<?
}
?>

<div style='margin-top:40px;'>
<?
     $this->renderPartial('_form', array(
            'model'=>$model,
     ));
?>
</div>

<div style='padding-top: 20px; '>
    <style>
    p {
        padding:0px; padding-bottom:35px; line-height:1.5em; font-size:16px; margin:0px;
    }

    </style>
    <? echo $model->text; ?>

</div>

<?
$docs = $model->docs;
$this->renderPartial('//layouts/_documents', array('docs'=>$docs));

$photos = $model->photos;
$this->renderPartial('//layouts/_photos', array('photos'=>$photos,'model'=>$model,'margin_top'=>'40'));

$this->renderPartial('//layouts/_socials', array('margin_top'=>'40'));
?>
</div>
<div style='clear:both;'></div>


</div>


<div style='width: 300px; margin-top:0px; float:left; border-radius:5px;'>

<?
if (count($submenu)>0){
?>
    <?
     $this->renderPartial('//pages/_submenu', array(
                'submenu' => $submenu,
            ));
    ?>

<?
}
?>

</div>

<div style='clear:both;'></div>

</div>
</div>



<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>