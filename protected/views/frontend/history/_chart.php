<?

//Это массив данных
$data = CJSON::decode($chart->data);


//Это мы вытаскиваем линию X
$data_x = array();
if (isset($data[0])){
    if (isset($data[0]['data'])){
        if (is_array($data[0]['data'])){
            if (count($data[0]['data']) > 0){
                $data_x = $data[0]['data'];
            }
        }
    }
}


$colors = array('#CC00CC','#FFCC66','#6600FF','#66CC00','#330099','#CC3366','#FF3399','#CC00CC','#FFCC66','#6600FF','#66CC00','#330099','#CC3366','#FF3399');

//А сейчас формируем линии
?>
<script>
        var Lines<?=$chart->id;?> = [
         <?
            foreach($data as $line_index => $line_array)
            {
                if ($line_array['type']=='x'){continue;}
                if (isset($line_array['data']))
                {
                if (count($line_array['data']) > 0)
                {
                $line = $line_array['data'];

                $color = '#3399FF';
                if (isset($colors[$line_index])){$color = $colors[$line_index];}
                ?>

                {
                    type: "line",
                    showInLegend: true,
                    name: "<?=$line_array['name'];?>",
                    markerType: "square",
                    color: "<?=$color;?>",
                    xValueFormatString:"####",
                    yValueFormatString:"####",
                    dataPoints: [
                    <?
                    //тут соединяем координаты X и Y
                    foreach($data_x as $index => $value)
                    {
                            $isset_value = false;
                            if (isset($line[$index])){
                                if ($line[$index] != ''){
                                    echo "{x:".$value.",y:".$line[$index]."},";
                                    $isset_value = true;
                                }
                            }

                            if ($isset_value == false)
                            {
                                echo "{x:0,y:0},";
                            }
                    }
                    ?>
                    ]
                },

            <?
            }
            }
            }
         ?>
        ];




$(function() {

    var options = {
        animationEnabled: true,
        theme: "light2",
        title:{
        },
        axisX:{
            valueFormatString:"####"
        },
        axisY: {
            title: "",
            suffix: "",
            minimum: 0,
            valueFormatString:"####"
        },
        toolTip:{
            shared:true
        },
        legend:{
            cursor:"pointer",
            verticalAlign: "bottom",
            horizontalAlign: "left",
            dockInsidePlotArea: true,
            itemclick: toogleDataSeries
        },
        data: Lines<?=$chart->id;?>
    };
    $("#chartContainer<?=$chart->id;?>").CanvasJSChart(options);



});
</script>
<div style='padding-bottom:50px;'>
<div style='font-size:20px; margin-bottom:35px;'><b><?=$chart->name;?></b></div>
<div id="chartContainer<?=$chart->id;?>" style="height: 370px; width: 100%; "></div>
<div style=' padding-top:25px;'>
    <?=$chart->description;?>
</div>
</div>

