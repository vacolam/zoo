<?
$this->pageTitle =  $model->name.'. Сахалинский Зооботанический парк.';
?>
<style>
.main_container.news{width: 830px;}
@media screen and (max-width:800px) {
.main_container.news{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container news'>

<div class='pages_view_title'><?=$model->name;?></div>

<div style='padding-top:30px;' class='redactor-styles'>
<?
    echo $model->text;
?>
</div>



<?
$docs = $model->docs;
$this->renderPartial('//layouts/_documents', array('docs'=>$docs));

$photos = $model->photos;
$this->renderPartial('//layouts/_photos', array('photos'=>$photos,'model'=>$model,'margin_top'=>'40'));


?>



<script src="js/jquery.canvasjs.min.js"></script>
<script>
function toogleDataSeries(e){
    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    } else{
        e.dataSeries.visible = true;
    }
    e.chart.render();
}
</script>
<?

if (count($charts)>0)
{
?>
<div style='margin-top:30px;'>
<?
foreach ($charts as $chart)
{
        $this->renderPartial('_chart', array(
            'chart'     => $chart,
        ));

}
?>
</div>
<?
}
?>

<?
$this->renderPartial('//layouts/_socials', array('margin_top'=>'40'));
?>
</div>



<div style='' class='main_right_submenu'>

<?
if (count($submenu)>0){
?>
    <?
     $this->renderPartial('//pages/_submenu', array(
                'submenu' => $submenu,
            ));
    ?>

<?
}
?>
</div>

<div style='clear:both;'></div>

</div>
</div>
<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>
