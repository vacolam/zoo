<div id="block-for-slider">
        <div id="viewport" style=''>
            <ul id="slidewrapper" style='margin:0px; left:0px; list-style:none;'>
                <?php
                if (count($banners) > 0)
                {
                foreach ($banners as $index => $banner)
                {
                    $active ='';
                    $index = $index + 1;
                    if ($index == 1){$active = 'active';}
                    ?>
                    <li class="slide <?=$active;?>" rel='<?=$index;?>'>
                        <a href="<?= $banner->link; ?>" style='' class='slide_a'>
                            <div style='background: url(<?= $banner->getImageUrl(); ?>) center; background-size:cover; ' alt="" class="slide-img">
                                <div class="slide-bg" style=''>  <!--background-image: linear-gradient(-180deg, rgba(0,0,0,0) 38%, rgba(0,0,0,0.65) 79%);-->
                                <div class='slide-width' style=''>
                                <div class='slide-text-size' style=''>
                                    <div class='slide-text-height' style=' '>
                                        <div style='text-align:left;'>
                                        <div class='slide-text-flag' style='' class='open-s'><span class='slide-text-flag-2' style=''>САМОЕ АКТУАЛЬНОЕ</span></div>
                                        <div  class='slide-text-title open-s'><b><?=$banner->title;?></b></div>
                                        <div class='slide-text-description' style='font-size:16px; line-height:1.6em; color:#fff; padding-top:5px;' class='open-s'><? echo $this->crop_str_word($banner->description, 25 ); ?></div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <?php
                }
                }
                ?>
            </ul>
        </div>

                <ul id="nav-btns" style=''>
                <?
                foreach ($banners as $index => $banner)
                {
                    $index = $index + 1;
                    $active = '';
                    if ($index == 1){$active = 'active';}
                    ?>
                    <li class="slide-nav-btn <?=$active;?>" rel="<?=$index;?>" onclick='banner_button_click(<?=$index;?>)'></li>
                    <?
                }
                ?>
            </ul>
</div>
<script>
var banner_max_slides = <?=count($banners);?>;
var current_slide = 1;

var slideTime = 15000;
var bannder_slide_circling;

function bannder_slide_circle(){
    bannder_slide_circling = setInterval(function(){banner_slide(current_slide)},slideTime);
}

function banner_slide(slide_id){
    var next_slide =  slide_id + 1;
    if (next_slide > banner_max_slides){next_slide = 1;}

    $('.slide[rel="'+slide_id+'"]').animate({left: "-100%"}, 1000);
    $('.slide[rel="'+next_slide+'"]').css('left','100%').animate({left: "0"}, 1000);

    $('.slide-nav-btn').removeClass('active');
    $('.slide-nav-btn[rel="'+next_slide+'"]').addClass('active');
    current_slide = next_slide;
}

function banner_button_click(slide_id){
    $('.slide[rel="'+current_slide+'"]').animate({left: "-100%"}, 1000);
    $('.slide[rel="'+slide_id+'"]').css('left','100%').animate({left: "0"}, 1000);

    $('.slide-nav-btn').removeClass('active');
    $('.slide-nav-btn[rel="'+slide_id+'"]').addClass('active');
    current_slide = slide_id;
}

$(function(){
    bannder_slide_circle();

    $('#block-for-slider').on('mousemove',function(){
        clearInterval(bannder_slide_circling);

    });
    $('#block-for-slider').on('mouseleave',function(){
        bannder_slide_circle();
    });
})

</script>