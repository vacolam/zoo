
    <?
        $bg =  "background:#53952A; ";
        if ($data->hasImage()) {
            $bg_url = $data->getThumbnailUrl();
            $bg =  'background:url("'.$bg_url.'") #53952A center; background-size:cover;';
        }

        $marginRight = ' margin-right:11px;';
        if ($t == ($max_t-1)){$marginRight = 'margin-right:0px;';}
    ?>
<a href="<?=Yii::app()->createUrl('exposure/view',array('id' => $data->id));?>" style='display:block; <?=$marginRight;?>    <?=$bg;?>' class='home_expoitem_block'>
    <div style='position:relative; width:100%; height:100%; background-image: linear-gradient(-180deg, rgba(0,0,0,0) 38%, rgba(0,0,0,0.65) 79%); border-radius:4px;'>
        <div style='width:100%; position:absolute; left:0px; bottom:10px;'>
            <div style='' class='home_expoitem_padding'>
                <div style='' class='home_expoitem_title open-s'><?=$data->title;?></div>
            </div>
        </div>
    </div>
</a>
<?
if ($t == ($max_t-1)){?><div style='clear:both;'></div><?}
?>
