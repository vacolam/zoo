<div class='home_newsitem_block'>
    <?
        $bg =  "background:none; ";
        if ($data->hasImage()) {
            $bg_url = $data->getThumbnailUrl();
            $bg =  'background:url("'.$bg_url.'") #f0f0f0 center; background-size:cover;';
        }
    ?>
<a href="<?=Yii::app()->createUrl('news/view',array('id' => $data->id));?>" style=' <?=$bg;?>' class='home_newsitem_photo'></a>


<div style='' class='home_newsitem_description'>
<div style='' class='home_newsitem_mobiledate'>
        <?
        echo Dates::getDatetimeName($data->date);
        ?>
    </div>
    <a href="<?=Yii::app()->createUrl('news/view',array('id' => $data->id));?>" style='' class='home_newsitem_title'><?=$data->title;?></a>
    <div style='' class='home_newsitem_date'>
        <?
        echo Dates::getDatetimeName($data->date);
        ?>
    </div>

    <div style='' class='home_newsitem_text'>
        <a href="<?=Yii::app()->createUrl('news/view',array('id' => $data->id));?>" style='' class='home_newsitem_text2'>
        <? echo $data->short_text; ?>
        </a>
    </div>
</div>

<div style='clear:both;'></div>

</div>