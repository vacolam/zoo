<?

//ЭТО ПРЕДСТАВЛЕНИЕ ЕЩЕ ИСПОЛЬЗУЕТСЯ В ENG

if (!isset($lang)){$lang = 'RU';}

if ($lang == 'RU'){
    $company_contacts = Page::getContatcs();
    $raspisanie = Page::getRaspisanie();
    $words = Page::getRaspisanieWords('ru');
}else{
    $company_contacts = Page::getEngContatcs();
    $raspisanie = Page::getEngRaspisanie();
    $words = Page::getRaspisanieWords('eng');
}
$company_socials = Page::getContatcs();
$raspisanie_colors = Page::getRaspisanieColors();
$tickets = Page::getTickets();

$season_bg = 'background-image: linear-gradient(-45deg, #FFCC00 0%, #53952A 50%);';
$season = Dates::getSeason();

//$season = 'Spring';
if ($season == 'Summer'){
    if ($raspisanie_colors['summer_color'] != ''){
        $season_bg = $raspisanie_colors['summer_color'];
    }
}
if ($season == 'Fall'){
    if ($raspisanie_colors['fall_color'] != ''){
        $season_bg = $raspisanie_colors['fall_color'];
    }
}
if ($season == 'Winter'){
    if ($raspisanie_colors['winter_color'] != ''){
        $season_bg = $raspisanie_colors['winter_color'];
    }
}
if ($season == 'Spring'){
    if ($raspisanie_colors['spring_color'] != ''){
        $season_bg = $raspisanie_colors['spring_color'];
    }
}

?>
<div style='width:1200px; margin:0px auto; margin-top:20px; position:relative; <?=$season_bg;?>  border-radius:5px; ' class='home_raspisanie_block'>

<div style='z-index:888; height:390px; width:315px; float:left; border-right:5px solid #fff;'>
        <div style='padding:25px 20px; '>
            <div style='padding-top:10px; text-align:center; color:#fff;'>
                <div style='font-size:13px; font-weight:200; text-align:center; padding-bottom:15px;'><b><?=$words[1];?></b></div>
                <div style='font-size:50px;  font-weight:600; letter-spacing:-1px; padding-top:60px;'>
                    <?=$raspisanie['raspis_time'];?>
                </div>
                <div style='font-size:15px; font-weight:200; text-align:center; padding-top:25px; width:240px; margin:0px auto;'><?=$raspisanie['raspis_description'];?></div>
            </div>
            <?
            if ($tickets['buy_ticket_show'] == 'show')
            {
            ?>
            <style>
            .buy_ticket_pay_type{text-decoration:none;}
            .buy_ticket_pay_type:hover{text-decoration:underline;}
            .buy_ticket{height:50px; background:#FFDB4D; border-radius:4px; line-height:50px; margin:0px auto; margin-top:25px; text-align:center; font-size:16px; color:#222; text-shadow:0px 1px 0px #FFF3C2; box-shadow:0px 0px 3px rgba(0,0,0,0.15);}
            .buy_ticket:hover{background:#FFD633; cursor:pointer;}

            </style>
            <div style='text-align:center;'>
            <?=$tickets['buy_ticket_script'];?>
            <div style='width:240px;' onclick='<?=$tickets['buy_ticket_code'];?>' class='open-s buy_ticket'><?=$raspisanie['buy_ticket_name'];?></div>
            <a href="https://sakhalinzoo.ru/index.php?r=pages/view&id=60" target='_blank' class='buy_ticket_pay_type' style='text-align:center; color:#fff; font-size:12px;'>Способы и условия оплаты</a>
            </div>
            <?
            }
            ?>
        </div>
</div>


<div style='z-index:881; height:390px;  width:560px; float:left;'>
    <div style='width:100%: height:100%;'>

    <div style='width:180px; height:370px;  float:left; color:#fff; border-right:1px dotted rgba(255,255,255,0.7);'>
        <div style='padding:25px;'>
            <div style='padding-top:10px; text-align:center; color:#fff;'>
                <div style='text-align:left;'>
                <div style='font-size:13px;  font-weight:600; color:#fff; padding-bottom:15px; margin-bottom:15px; border-bottom:2px solid rgba(255,255,255,0.6); border-radius:1px;'>
                    <?=$words[2];?>
                </div>
                <div style='font-size:32px;  font-weight:600; letter-spacing:-1px; '>
                    <?=$raspisanie['raspis_children_1'];?> <SPAN style='font-size:20px;'><?=$words[9];?></SPAN>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'><?=$words[5];?></div>

                <div style='font-size:32px;  font-weight:600; letter-spacing:-1px; padding-top:30px;'>
                    <?=$raspisanie['raspis_children_2'];?> <SPAN style='font-size:20px;'><?=$words[9];?></SPAN>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'><?=$words[6];?></div>




                <div style='font-size:18px;  font-weight:600; letter-spacing:-1px; padding-top:30px; text-transform:uppercase;'>
                    <?=$raspisanie['raspis_children_3'];?>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'><?=$raspisanie['raspis_children_3_text'];?></div>

                </div>
            </div>
        </div>
    </div>
    <div style='width:180px;height:370px;  float:left; margin-right:1px; color:#fff; border-right:1px dotted rgba(255,255,255,0.7);'>
        <div style='padding:25px;'>
            <div style='padding-top:10px; text-align:center; color:#fff;'>
                <div style='text-align:left;'>
                <div style='font-size:13px;  font-weight:600;  color:#fff; padding-bottom:15px; margin-bottom:15px; border-bottom:2px solid rgba(255,255,255,0.6); border-radius:1px;'>
                    <?=$words[3];?>
                </div>


                <div style='font-size:32px;  font-weight:600; letter-spacing:-1px;'>
                    <?=$raspisanie['raspis_vzrosliy_1'];?> <SPAN style='font-size:20px;'><?=$words[9];?></SPAN>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'><?=$words[5];?></div>

                <div style='font-size:32px;  font-weight:600; letter-spacing:-1px;  padding-top:30px;'>
                    <?=$raspisanie['raspis_vzrosliy_2'];?> <SPAN style='font-size:20px;'><?=$words[9];?></SPAN>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'><?=$words[6];?></div>

                <div style='font-size:18px;  font-weight:600; letter-spacing:-1px; padding-top:30px; text-transform:uppercase;'>
                    <?=$raspisanie['raspis_vzrosliy_3'];?>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'><?=$raspisanie['raspis_vzrosliy_3_text'];?></div>
                </div>
            </div>
        </div>
    </div>
    <div style='width:180px; height:370px;  float:left; margin-right:5px; color:#fff;'>
        <div style='padding:25px;'>
            <div style='padding-top:10px; text-align:center; color:#fff;'>
                <div style='text-align:left;'>
                <div style='font-size:13px;  font-weight:600; color:#fff; padding-bottom:15px; margin-bottom:15px; border-bottom:2px solid rgba(255,255,255,0.6); border-radius:1px;'>
                    <?=$words[4];?>
                </div>
                <div style='font-size:32px;  font-weight:600; letter-spacing:-1px; '>
                    <?=$raspisanie['raspis_pens_1'];?> <SPAN style='font-size:20px;'><?=$words[9];?></SPAN>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'><?=$words[5];?></div>


                <div style='font-size:18px;  font-weight:600; letter-spacing:-1px; padding-top:30px; text-transform:uppercase;'>
                    <?=$raspisanie['raspis_pens_3'];?>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'><?=$raspisanie['raspis_pens_3_text'];?></div>
                </div>
            </div>
        </div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>

    <div style='width:315px; height:390px; float:right; border-left:5px solid #fff; border-radius:0px 5px 5px 0px; position:relative;'>
       <div style='padding:25px 10px;'>
            <div style='padding-top:10px; color:#fff;'>
                <div style='text-align:center;'>
                <div style='font-size:13px;  font-weight:600; color:#fff; padding-bottom:15px; margin-bottom:15px; text-align:center; text-shadow:1px 1px 2px rgba(0,0,0,.2);'>
                    <?=$words[7];?>
                </div>
                <div style='font-size:28px; font-weight:600; text-align:center; padding-top:25px; text-shadow:1px 1px 2px rgba(0,0,0,.2); text-transform:uppercase;'><?=$company_contacts['company_adress'];?></div>
                <div style='font-size:16px; font-weight:200; text-align:center; padding-top:2px; text-shadow:1px 1px 2px rgba(0,0,0,.2);'><?=$company_contacts['company_city'];?></div>
                <div style='font-size:16px; font-weight:200; text-align:center; padding-top:20px; text-shadow:1px 1px 2px rgba(0,0,0,.2);'><?=$company_socials['company_phone_1'];?>
                <br>
                <?=$company_socials['company_phone_2'];?>
                </div>

                <a href='<?=Yii::app()->createUrl('contacts/index');?>' style='font-size:14px; font-weight:200; text-align:center; padding-top:10px; color:#fff;'><b><?=$words[8];?>&nbsp;&rarr;</b></a>
                <div><a href='<?=Yii::app()->createUrl('pano/index');?>' style='font-size:14px; font-weight:200; text-align:center; padding-top:10px; color:#fff;'><b>Панорамы&nbsp;&rarr;</b></a></div>

                <div style='width:100%; height:auto;position:absolute; left:0px; bottom:20px;'>
                    <div style='width:100%; text-align:center;'>
<?
                    $this->renderPartial('//layouts/_social_logos',array('size'=>30,'company_contacts'=>$company_socials));
?>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
<div style='clear:both;'></div>
</div>
