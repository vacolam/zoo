<?

//ЭТО ПРЕДСТАВЛЕНИЕ ЕЩЕ ИСПОЛЬЗУЕТСЯ В ENG

if (!isset($lang)){$lang = 'RU';}

if ($lang == 'RU'){
    $company_contacts = Page::getContatcs();
    $raspisanie = Page::getRaspisanie();
    $words = Page::getRaspisanieWords('ru');
}else{
    $company_contacts = Page::getEngContatcs();
    $raspisanie = Page::getEngRaspisanie();
    $words = Page::getRaspisanieWords('eng');
}
$company_socials = Page::getContatcs();
$raspisanie_colors = Page::getRaspisanieColors();
$tickets = Page::getTickets();

$season_bg = 'background-image: linear-gradient(-45deg, #FFCC00 0%, #53952A 50%);';
$season = Dates::getSeason();

if ($season == 'Summer'){
    if ($raspisanie_colors['summer_color'] != ''){
        $season_bg = $raspisanie_colors['summer_color'];
    }
}
if ($season == 'Fall'){
    if ($raspisanie_colors['fall_color'] != ''){
        $season_bg = $raspisanie_colors['fall_color'];
    }
}
if ($season == 'Winter'){
    if ($raspisanie_colors['winter_color'] != ''){
        $season_bg = $raspisanie_colors['winter_color'];
    }
}
if ($season == 'Spring'){
    if ($raspisanie_colors['spring_color'] != ''){
        $season_bg = $raspisanie_colors['spring_color'];
    }
}
?>
<div style='margin-top:-10px;' class='home_raspisanieMobile_block'>

<div style='<?=$season_bg;?> border-radius:5px; box-shadow:0px 0px 4px rgba(0,0,0,.2); width:100%;'>
        <div style='padding:25px 10px; '>
            <div style='padding-top:10px; text-align:center; color:#fff;'>
                <div style='font-size:12px; font-weight:200; text-align:center; padding-bottom:5px; text-shadow:1px 1px 2px rgba(0,0,0,.2);'><b><?=$words[1];?></b></div>
                <div style='font-size:26px;  font-weight:600; letter-spacing:-1px; padding-top:20px;'>
                    <?=$raspisanie['raspis_time'];?>
                </div>
                <div style='font-size:12px; font-weight:200; text-align:center; padding-top:20px; width:280px; margin:0px auto;'><?=$raspisanie['raspis_description'];?></div>
            </div>

            <?
            if ($tickets['buy_ticket_show'] == 'show')
            {
            ?>
            <style>
            .buy_ticket_pay_type{text-decoration:none;}
            .buy_ticket_pay_type:hover{text-decoration:underline;}
            .buy_ticket{height:40px; background:#FFDB4D; border-radius:4px; line-height:40px; margin:0px auto; margin-top:20px; text-align:center; font-size:16px; color:#222; text-shadow:0px 1px 0px #FFF3C2; box-shadow:0px 0px 3px rgba(0,0,0,0.15);}
            .buy_ticket:hover{background:#FFD633; cursor:pointer;}

            </style>
            <div style='text-align:center;'>
            <?=$tickets['buy_ticket_script'];?>
            <div style='width:240px;' onclick='<?=$tickets['buy_ticket_code'];?>' class='open-s buy_ticket'><?=$raspisanie['buy_ticket_name'];?></div>
            <a href="https://sakhalinzoo.ru/index.php?r=pages/view&id=60" target='_blank' class='buy_ticket_pay_type' style='text-align:center; color:#fff; font-size:12px;'>Способы и условия оплаты</a>
            </div>
            <?
            }
            ?>
        </div>
</div>


<script type="text/javascript">
$(function(){
t1 = new IScroll('.home_raspisanie_bilety_iscroll', { scrollX: true, scrollY: false, mouseWheel: true,eventPassthrough:true });
})
</script>
<div class='home_raspisanie_bilety_iscroll' style=''>
<div class='home_raspisanie_bilety_iscroll_in' style='<?=$season_bg;?> border-radius:5px; box-shadow:0px 0px 4px rgba(0,0,0,.2);'>

    <div style='width:180px; height:280px; float:left; color:#fff; border-right:1px dotted rgba(255,255,255,0.7);'>
        <div style='padding:25px;'>
            <div style='padding-top:0px; text-align:center; color:#fff;'>
                <div style='text-align:left;'>
                <div style='font-size:12px;  font-weight:600; color:#fff; padding-bottom:15px; margin-bottom:15px; border-bottom:2px solid rgba(255,255,255,0.6); border-radius:1px;'>
                    <?=$words[2];?>
                </div>
                <div style='font-size:22px;  font-weight:600; letter-spacing:-1px; '>
                    <?=$raspisanie['raspis_children_1'];?> <SPAN style='font-size:20px;'><?=$words[9];?></SPAN>
                </div>
                <div style='font-size:12px; font-weight:200; text-align:left; padding-top:0px;'><?=$words[5];?></div>

                <div style='font-size:22px;  font-weight:600; letter-spacing:-1px; padding-top:20px;'>
                    <?=$raspisanie['raspis_children_2'];?> <SPAN style='font-size:20px;'><?=$words[9];?></SPAN>
                </div>
                <div style='font-size:12px; font-weight:200; text-align:left; padding-top:0px;'><?=$words[6];?></div>

                <div style='font-size:16px;  font-weight:600; letter-spacing:-1px; padding-top:20px; text-transform:uppercase;'>
                    <?=$raspisanie['raspis_children_3'];?>
                </div>
                <div style='font-size:12px; font-weight:200; text-align:left; padding-top:0px;'><?=$raspisanie['raspis_children_3_text'];?></div>

                </div>
            </div>
        </div>
    </div>

    <!--ВЗРОСЛЫЕ-->
    <div style='width:180px;height:280px;  float:left; margin-right:1px; color:#fff; border-right:1px dotted rgba(255,255,255,0.7);'>
        <div style='padding:25px;'>
            <div style='padding-top:0px; text-align:center; color:#fff;'>
                <div style='text-align:left;'>
                <div style='font-size:12px;  font-weight:600;  color:#fff; padding-bottom:15px; margin-bottom:15px; border-bottom:2px solid rgba(255,255,255,0.6); border-radius:1px;'>
                    <?=$words[3];?>
                </div>


                <div style='font-size:22px;  font-weight:600; letter-spacing:-1px;'>
                    <?=$raspisanie['raspis_vzrosliy_1'];?> <SPAN style='font-size:20px;'><?=$words[9];?></SPAN>
                </div>
                <div style='font-size:12px; font-weight:200; text-align:left; padding-top:0px;'><?=$words[5];?></div>

                <div style='font-size:22px;  font-weight:600; letter-spacing:-1px;  padding-top:20px;'>
                    <?=$raspisanie['raspis_vzrosliy_2'];?> <SPAN style='font-size:20px;'><?=$words[9];?></SPAN>
                </div>
                <div style='font-size:12px; font-weight:200; text-align:left; padding-top:0px;'><?=$words[6];?></div>


                <div style='font-size:16px;  font-weight:600; letter-spacing:-1px; padding-top:20px; text-transform:uppercase;'>
                    <?=$raspisanie['raspis_vzrosliy_3'];?>
                </div>
                <div style='font-size:12px; font-weight:200; text-align:left; padding-top:0px;'><?=$raspisanie['raspis_vzrosliy_3_text'];?></div>
                </div>
            </div>
        </div>
    </div>

    <!--PENSI-->
    <div style='width:180px; height:280px;  float:left; margin-right:5px; color:#fff;'>
        <div style='padding:25px;'>
            <div style='padding-top:0px; text-align:center; color:#fff;'>
                <div style='text-align:left;'>
                <div style='font-size:12px;  font-weight:600; color:#fff; padding-bottom:15px; margin-bottom:15px; border-bottom:2px solid rgba(255,255,255,0.6); border-radius:1px;'>
                    <?=$words[4];?>
                </div>
                <div style='font-size:22px;  font-weight:600; letter-spacing:-1px; '>
                    <?=$raspisanie['raspis_pens_1'];?> <SPAN style='font-size:20px;'><?=$words[9];?></SPAN>
                </div>
                <div style='font-size:12px; font-weight:200; text-align:left; padding-top:0px;'><?=$words[5];?></div>


                <div style='font-size:16px;  font-weight:600; letter-spacing:-1px; padding-top:20px; text-transform:uppercase;'>
                    <?=$raspisanie['raspis_pens_3'];?>
                </div>
                <div style='font-size:12px; font-weight:200; text-align:left; padding-top:0px;'><?=$raspisanie['raspis_pens_3_text'];?></div>
                </div>
            </div>
        </div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>

    <div style='position:relative; <?=$season_bg;?> border-radius:5px; box-shadow:0px 0px 4px rgba(0,0,0,.2); width:100%; margin-top:0px;  padding-bottom:50px;'>
       <div style='padding:25px 10px;'>
            <div style='padding-top:10px; color:#fff;'>
                <div style='text-align:center;'>
                <div style='font-size:12px;  font-weight:600; color:#fff; padding-bottom:10px; margin-bottom:15px; text-align:center; text-shadow:1px 1px 2px rgba(0,0,0,.2);'>
                    <?=$words[7];?>
                </div>
                <div style='font-size:22px; font-weight:600; text-align:center; padding-top:15px; text-shadow:1px 1px 2px rgba(0,0,0,.2); text-transform:uppercase;'><?=$company_contacts['company_adress'];?></div>
                <div style='font-size:12px; font-weight:200; text-align:center; padding-top:2px; text-shadow:1px 1px 2px rgba(0,0,0,.2);'><?=$company_contacts['company_city'];?></div>
                <div style='font-size:12px; font-weight:200; text-align:center; padding-top:20px; text-shadow:1px 1px 2px rgba(0,0,0,.2);'><?=$company_socials['company_phone_1'];?>
                <br>
                <?=$company_socials['company_phone_2'];?>
                </div>

                <a href='<?=Yii::app()->createUrl('contacts/index');?>' style='font-size:10px; font-weight:200; text-align:center; padding-top:10px; color:#fff;'><b><?=$words[8];?>&nbsp;&rarr;</b></a>

                <div style='width:100%; height:auto;position:absolute; left:0px; bottom:20px;'>
                    <div style='width:100%; text-align:center;'>
                    <?
                    $this->renderPartial('//layouts/_social_logos',array('size'=>26,'company_contacts'=>$company_socials));
                    ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
<div style='clear:both;'></div>
</div>
