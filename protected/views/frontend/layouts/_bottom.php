<div class="footer"  style='clear:both;width:100%; margin-top:0px; background:#FAFAFA; border-top:1px solid #FEFEFE;'>
<div style='clear:both; width:1200px; margin:0px auto; padding:30px 0px; padding-bottom:60px;'>
<?

?>



<table style='list-style:none; padding:0px; margin:0px; width:100%; border:none;'>
<tr>
    <td style='width:50%;' valign=top align=left>
        <?
        if (Yii::app()->controller->id != 'eng')
        {
        ?>
        <div style='width:100px; height:100px; background:url(/css_tool/logo_old.png) center; background-size:cover; float:left; margin-right:30px;'></div>
        <div style=' font-size:18px; float:left; width:460px; margin-top:25px; color:#202020; font-weight:400;'>2008—<?=date("Y");?>, <?=$company_contacts['company_name'];?></div>
        <div style='clear:both;'></div>

        <div style='padding-top:20px; font-size:14px; margin-left:130px; ' class='open-s'>
            <?
            $phones_array = array($company_contacts['company_phone_1'],$company_contacts['company_phone_2']);
            echo implode(', ',$phones_array);
            echo "<br>";
            echo $company_contacts['company_city'].", ".$company_contacts['company_adress']."";
            echo "<br><br>";

            if ($company_contacts['company_license_number'] != ''){
            ?>
            <a href="<?= $company_contacts['company_license_link']; ?>" target='_blank' style=' text-decoration:none; color:#606060;'><b><?= $company_contacts['company_license_number']; ?></b></a>
            <?
            }
            ?>
        </div>

        <div style='width:100%; text-align:left; margin-top:30px; margin-left:128px;'>
                <?
                $this->renderPartial('//layouts/_social_logos',array('is_green'=>true,'size'=>30,'company_contacts'=>$company_contacts));
                ?>
        </div>
        <?
        }else{
            ?>
            <div style='width:100px; height:100px; background:url(/css_tool/summer_logo.png) center; background-size:cover; float:left; margin-right:60px;'></div>
            <div style='float:left; width:360px; margin-top:0px; color:#202020;'>
                <div style='padding-top:0px; font-size:22px;' class='open-s'>
                    PHONES:
                    <ul style='margin-left:20px; font-size:18px; padding-top:20px;'>
                    <li>
                    <?
                    $phones_array = array($company_contacts['company_phone_1'],$company_contacts['company_phone_2']);
                    echo implode('</li><li>',$phones_array);
                    ?>
                    </li>
                    </ul>
                </div>

                <div style='display:inline-block; margin-top:50px;'>
                       <?
                    if ($company_contacts['company_social_vk'] != ''){
                    ?>
                    <a href='<?=$company_contacts['company_social_vk'];?>' target="_blank" style='background:#fff; border-radius:2px; margin-left:2px; margin-right:2px; display:block; height:30px; width:30px; float:left; '><img src="/css_tool/vk_green.png" alt="" style='height:30px; width:30px;'/></a>
                    <?
                    }
                    ?>

                    <?
                    if ($company_contacts['company_social_fb'] != ''){
                    ?>
                    <a href='<?=$company_contacts['company_social_fb'];?>' target="_blank" style='background:#fff; border-radius:2px; margin-left:2px; margin-right:2px;  display:block; height:30px; width:30px; float:left; '><img src="/css_tool/facebook_green.png" alt="" style='width:30px; height:30px;'/></a>
                    <?
                    }
                    ?>

                    <?
                    if ($company_contacts['company_social_inst'] != ''){
                    ?>
                    <a href='<?=$company_contacts['company_social_inst'];?>' target="_blank" style='background:#fff; border-radius:2px; margin-left:2px; margin-right:2px; display:block; height:30px; width:30px; float:left; '><img src="/css_tool/instagram_green.png" alt="" style='width:30px; height:30px;'/></a>
                    <?
                    }
                    ?>

                    <?
                    if ($company_contacts['company_social_ok'] != ''){
                    ?>
                    <a href='<?=$company_contacts['company_social_ok'];?>' target="_blank" style='background:#fff; border-radius:2px; margin-left:2px; margin-right:2px;  display:block; height:30px; width:30px; float:left; '><img src="/css_tool/odnoklassniki_green.png" alt="" style='width:30px; height:30px;'/></a>
                    <?
                    }
                    ?>

                    <?
                    if ($company_contacts['company_social_tw'] != ''){
                    ?>
                    <a href='<?=$company_contacts['company_social_tw'];?>' target="_blank" style='background:#fff; border-radius:2px; margin-left:2px; margin-right:2px; display:block; height:30px; width:30px; float:left; '><img src="/css_tool/twitter_green.png" alt="" style='width:30px; height:30px;'/></a>
                    <?
                    }
                    ?>

                    <?
                    if ($company_contacts['company_social_telegram'] != ''){
                    ?>
                    <a href='<?=$company_contacts['company_social_telegram'];?>' target="_blank" style='background:#fff; border-radius:2px; margin-left:2px; margin-right:2px; display:block; height:30px; width:30px; float:left; '><img src="/css_tool/telegram_green.png" alt="" style='width:30px; height:30px;'/></a>
                    <?
                    }
                    ?>

                    <?
                    if ($company_contacts['company_social_whatsapp'] != ''){
                    ?>
                    <a href='<?=$company_contacts['company_social_whatsapp'];?>' target="_blank" style='background:#fff; border-radius:2px; margin-left:2px; margin-right:2px;  display:block; height:30px; width:30px; float:left; '><img src="/css_tool/whatsapp_green.png" alt="" style='width:30px; height:30px;'/></a>
                    <?
                    }
                    ?>

                    <?
                    if ($company_contacts['company_social_youtube'] != ''){
                    ?>
                    <a href='<?=$company_contacts['company_social_youtube'];?>' target="_blank" style='background:#fff; border-radius:2px; margin-left:2px; margin-right:2px;  display:block; height:30px; width:30px; float:left; '><img src="/css_tool/youtube_green.png" alt="" style='width:30px; height:30px;'/></a>
                    <?
                    }
                    ?>
                    <div style='clear:both;'></div>
                    </div>
            </div>
            <div style='clear:both;'></div>
            <?
        }
        ?>
    </td>
    <td style='padding-left:40px; padding-top:20px;' valign=top align=left>
        <?
        if (Yii::app()->controller->id != 'eng')
        {
        ?>
        <div style='font-size:16px;'><span style='background:#53952A; color:#fff; padding:6px 15px; border-radius:4px;'>Полезные ресурсы</span></div>
        <ul style='padding-top:30px;'>
        <?
        $links = Link::getLinks();
        foreach ($links as $link)
        {
            ?>
            <li style='margin-left:20px;'><a href="<?= $link->link; ?>" style='color: #000; font-size:14px; text-decoration:none;'><?= $link->title; ?></a></li>
            <?
        }
        ?>
        </ul>
        <?
        }
        ?>
    </td>
</tr>
</table>
</div>
</div>