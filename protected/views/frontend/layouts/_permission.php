<?
if($detect2->isMobile())
{
?>
<div style='width:100%; height:auto; background:rgba(202, 212, 204, 1); position:fixed; left:0px; bottom:0px; display:none;' class='permission_popup'>
    <div style='padding:20px;'>

        <div style='font-size:13px; color:#000;' >
            Предоставляя свои персональные данные Пользователь дает согласие на
обработку, хранение и использование своих персональных данных в
соответствии с ФЗ № 152-ФЗ «О персональных данных» от 27.06.2006 г.,
<a href="https://sakhalinzoo.ru/index.php?r=documents/index" target='_blank' style='color:#468122; border-bottom:1px dotted #468122;'>Политикой в отношении обработки персональных данных ГБУК
«Сахалинский зооботанический парк»</a>.

            <div onclick='setPermission()' style='margin-top:10px; cursor:pointer; width:100%; height:30px; line-height:30px; text-align:center; border-radius:4px; background:#53952A; color:#fff;'>
                ПОНЯТНО
            </div>

            <div style='clear:both;'></div>
        </div>



    </div>
</div>

<?
}else{
?>
<div style='width:100%; height:100px; background:rgba(202, 212, 204, 1); position:fixed; left:0px; bottom:0px; display:none;' class='permission_popup'>
    <div style='width:1200px; margin:0px auto;  padding-top:20px; padding-bottom:20px;'>

        <div style='font-size:14px; color:#000; width:1000px; float:left;'>
            Предоставляя свои персональные данные Пользователь дает согласие на
обработку, хранение и использование своих персональных данных в
соответствии с ФЗ № 152-ФЗ «О персональных данных» от 27.06.2006 г.,
<a href="https://sakhalinzoo.ru/index.php?r=documents/index" target='_blank' style='color:#468122; border-bottom:1px dotted #468122;'>Политикой в отношении обработки персональных данных ГБУК
«Сахалинский зооботанический парк»</a>.
        </div>

        <div style='font-size:14px; color:#000; width:150px; float:right;'>
            <div onclick='setPermission()' style='cursor:pointer; width:150px; height:40px; line-height:40px; text-align:center; border-radius:4px; background:#53952A; color:#fff;'>
                ПОНЯТНО
            </div>
        </div>

        <div style='clear:both;'></div>
    </div>
</div>
<?
}
?>

<script>
    function setPermission(){
        Cookies.set('allow_to_use_my_data', 'allow', { expires: 60 });
        $('.permission_popup').css('display','none');
    }

    function checkPermission(){
        var allow_to_use_my_data = Cookies.get('allow_to_use_my_data');
        if (allow_to_use_my_data == 'undefined' || allow_to_use_my_data == undefined)
        {
            $('.permission_popup').css('display','block');
        }
    }

    $(function(){
        checkPermission();
    })
</script>
