<style>
#header-title.open-s {
    background: url(/css_tool/summer_logo.png) no-repeat top left;
    background-size: 100% 100%;
    font-size:20px;

}

#header-title {
    position: absolute;
    height: 9.35em;
    width: 9.68em;
}
#header-title span {
    position: absolute;
    color: #FFFFFF;
    text-transform: uppercase;
}
#sakhalin {
    top: 2.8em;
    left: 1.3em;
    font-size:14px;
}
#zoo {
    top: 1.75em;
    left: .63em;
    font-size: 2.95em;
    letter-spacing: 0.01em;
}
#zoo_2 {
    top: 9em;
    left: 8.3em;
    font-size: .64em;
}
#zoo_3 {
    top: 3.34em;
    left: 2.83em;
    font-size: 1.85em;
    letter-spacing: .03em;
}
</style>

<?
$style_line = 'box-shadow:0px 1px 1px rgba(0,0,0,.1); border-bottom:1px solid #ccc;';
$style_logo = 'width:120px; height:15px; background:#f0f0f0; bottom:-1px; left:-1px; border-radius:0px 0px 5px 5px; box-shadow:1px 1px 1px rgba(0,0,0,.1); z-index:666; border:1px solid #ccc; border-top:none;';


if ($this->route == 'home/index'){
$style_line = 'box-shadow:0px 4px 5px rgba(0,0,0,.3);';
$style_logo = 'width:120px; height:15px; background:#f0f0f0; bottom:0px; left:0px;   border-radius:0px 0px 5px 5px; box-shadow:0px 4px 5px rgba(0,0,0,.3);';
}

?>
<div style='width:100%; height:90px; position:relative;' class='menu_site'>
<div style='width:100%; height:90px; position:absolute; top:0px; left:0px; z-index:666; background:#fff; <?=$style_line;?>'> <!---->
<div style='width:100%; padding:15px 0px; ' >
    <div style='width:1200px; margin:0px auto; text-align:left; position:relative;'>
        <style>
    	.menu_item{cursor:pointer; color: #3F2C35 !important; float:left; padding:2px 0px; height:30px; line-height:30px;}
    	.menu_item a{font-weight:500; color: #707070;  text-transform:uppercase; font-size:13px; line-height:30px}
    	.menu_item.active{font-weight:bold; background:#53952A; padding:2px 10px; border-radius:3px;}
    	.menu_item.active a{font-weight:600; color: #fff;}
    	</style>
        <?
        $d = 'none';
        $s='';
        if ($this->route == 'site/search'){
            $d = 'block';
            if (isset($_GET['search_text'])){
                $s = $_GET['search_text'];
            }
        }
        ?>
        <div style='width:1080px; position:absolute; left:130px; top:-10px; display:<?=$d;?>;' class='search_input'>
            <div style='width:100%; height:50px; position:relative;'>
                <div style='width:50px; background:#FAFAFA; border-right:1px solid #ccc; border-radius: 3px 0px 0px 3px; height:48px; text-align:center; cursor:pointer; line-height:42px; font-size:30px; color:#000; position:absolute; top:1px; left:1px; z-index:99;' onclick='search_hide()'>&larr;</div>
                <div style='width:120px; background:#53952A; border-radius: 3px; height:38px; text-align:center; cursor:pointer; line-height:38px; font-size:16px; color:#fff; position:absolute; top:6px; right:6px; z-index:99;' onclick='search_start()'>Найти</div>
                <form class='s_form' method="post" onsubmit="return false">
                <input type="text" value='<?=$s;?>' class='search_text' name='search_text' style='width:870px; padding-left:70px; padding-right:140px; height:50px; line-height:50px; border:1px solid #ccc; border-radius:3px; font-size:16px; outline:none;' placeholder='Поиск'  />
                </form>
            </div>
        </div>
        <ul style="list-style:none;  width:100%; margin-top:15px;">
            <li style='width:120px; height:105px; margin-top:-30px;  float:left; margin-right:20px; position:relative;'>
                <a href='<?=Yii::app()->createUrl('home/index',array());?>' style='display:block; height:115px; width:120px; position:absolute; bottom:0px; left:0px; background:#fff; margin-top:-10px; border-radius:0px 0px 5px 5px;  z-index:777;'> <!---->
                    <div style='padding:10px;'>
                    <img src="/css_tool/zoo_logo.png" alt="" style='width:100px;'/>
                    </div>
                </a>
                <div style='position:absolute; <?=$style_logo;?>'></div>
            </li>
            <?php
            $i = 0;
            foreach ($this->menu as $item)
            {
                $i++;
                if ($i > 9){continue;}
                if (Yii::app()->controller->id == 'eng'){
                    $class = $item['active'] ? ' class="active menu_item"' : 'class="menu_item"';
                }else{
                    $class = 'class="menu_item"';
                    if ($item['id'] == $this->razdel['id']){
                        $class = ' class="active menu_item"';
                    }
                }
                ?>
                <li <?= $class; ?> style='margin-right:15px; '><?= CHtml::link($item['label'], $item['url'], array('style'=>"")); ?></li>
                <?php
            }

            if ($i > 9)
            {
            ?>
                <li class='menu_item tochki' style='margin-right:15px;' style='border:1px solid #ff0000;'>
                        <div>&bull;&bull;&bull;</div>
                        <div style='position:absolute; display:none;' class='tochki_popup'>
                        <div style='border-radius:3px; width:240px; height:auto; border:1px solid #F2F2F2; background:#fff; box-shadow:0px 12px 15px rgba(0,0,0,.2); position:absolute; top:-2px; left:-30px;'>
                            <div style='padding:20px;'>
                                <?
                                $r = 0;
                                foreach ($this->menu as $item)
                                {
                                    $r++;
                                    if ($r < 10){continue;}
                                    if (Yii::app()->controller->id == 'eng'){
                                        $class = $item['active'] ? ' class="active menu_item"' : 'class="menu_item"';
                                    }else{
                                        $class = 'class="menu_item"';
                                        if ($item['id'] == $this->razdel['id']){
                                            $class = ' class="active menu_item"';
                                        }
                                    }
                                    ?>
                                    <div style='margin-right:15px;'><?= CHtml::link($item['label'], $item['url'], array('style'=>"line-height:1.3em; padding-top:5px; padding-bottom:5px; display:block;")); ?></div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        </div>
                </li>
                <script>
                    $('.tochki').on('mousemove',function(){
                        $('.tochki_popup').css('display','block');
                    });
                    $('.tochki').on('mouseleave',function(){
                        $('.tochki_popup').css('display','none');
                    });
                </script>

            <?
            }
            ?>
            <li style='width:30px; height:30px;  float:right; text-align:right;' class='menu_item'>
                <?
                if (Yii::app()->controller->id == 'eng'){
                    ?>
                    <a href="<?=Yii::app()->createUrl('home/index',array());?>"><b>РУС</b></a>
                    <?
                }else{
                    ?>
                    <a href="<?=Yii::app()->createUrl('eng/index');?>"><b>ENG</b></a>
                    <?
                }
                ?>
            </li>
            <li style='width:28px; height:16px; float:right; margin-right:15px; margin-top:5px;' class='menu_item'>
                <a href="<?=Yii::app()->createUrl('sitemap/index',array());?>" style='display:block; width:30px; height:20px; background:url(/css_tool/sitemap.png) center; background-size:cover;' title='Карта сайта'></a>
            </li>
            <li style='width:50px; height:30px; background:url(/css_tool/noun_Eye_801371.png) center; background-size:cover; float:right; margin-right:5px;' class='menu_item bvi-open'></li>
            <li style='width:30px; height:20px; background:url(/css_tool/noun_Search_1948134.png) center; background-size:cover; float:right; margin-top:4px; margin-right:5px;' class='menu_item' onclick='seach_show()'></li>
    		<li style='clear:both;'></li>
        </ul>
    </div>
</div>
</div>
</div>

<script>
 function seach_show(){
    $('.search_input').css('display','block');
 }

 function search_hide(){
    $('.search_input').css('display','none');
 }

 function search_start(){
     var search = $('.search_text').val();
     if (search != ''){
        $('.s_form').attr('onsubmit','');
        $('.s_form').attr('action','<?=Yii::app()->createUrl('site/search');?>'+ '&search_text='+search);
        $('.s_form').submit();
     }
 }
</script>