    <div style='position:relative; border-bottom:1px solid #ccc;  background:#fff; box-shadow:0px 2px 2px rgba(0,0,0,.1);' class='mobile_top_menu'>

	<table style='width:100%; padding:0px; margin:0px; height:85px;' cellpadding=0 cellspacing=0 >
    	<td style='width:80px;  color:#404040; font-size:11px; text-transform:uppercase; padding:0px; padding-left:20px;' class='open-s' align='left' valign='middle'>

			<div style='width:22px; height:15px; cursor:pointer; padding-top:0px; cursor:pointer; position:relative;' class='menu_mobile_burger'>
        	<div style='width:22px; height:3px; background:#000; margin:0px auto; border-radius:4px;'></div>
        	<div style='width:22px; height:3px; background:#000; margin:0px auto; margin-top:3px; border-radius:4px;'></div>
        	<div style='width:22px; height:3px; background:#000; margin:0px auto; margin-top:3px; border-radius:4px;'></div>
			</div>

		</td>

        <td style='padding:0px;'  align='center' valign='middle'>
            <div style='text-align:center; padding-top:8px;' class='zoo_logo'>

                    <img src="/css_tool/zoo_logo.png" alt="" style='width:75px;'/>

            </div>
        </td>

		<td style='width:80px; padding:0px; text-align:right; padding-right:20px;' valign='middle' align='right'>

		                <?
                if (Yii::app()->controller->id == 'eng'){
                    ?>
                    <a href="<?=Yii::app()->createUrl('home/index',array());?>" style='font-size:14px;'><b>РУС</b></a>
                    <?
                }else{
                    ?>
                    <a href="<?=Yii::app()->createUrl('eng/index');?>" style='font-size:14px;'><b>ENG</b></a>
                    <?
                }
                ?>

		</td>
	</table>

<?
if ($this->route != 'home/index' AND Yii::app()->controller->id != 'shop')
{
if (isset($this->razdel['name']))
{
if ($this->razdel['name'] != '')
{

?>
    <div style='border-top:1px solid #E3E3E3; padding: 10px 0px;' class='zoo_top_subtitle'>
        <div style='padding:0px 20px; text-align:center; font-size:14px; text-transform:uppercase;'><b><?=$this->razdel['name'];?></b></div>
    </div>
<?
}
}
}

if (Yii::app()->controller->id == 'shop')
{
?>
    <a href='<?=Yii::app()->createUrl('shop/index',array());?>' style='display:block; border-top:1px solid #E3E3E3; padding: 10px 0px;' class=''>
        <div style='padding:0px 20px; text-align:center; font-size:14px; text-transform:uppercase;'><b>Категории магазина</b></div>
    </a>
<?

}
?>



<div style='background:rgba(0,0,0,.1); width:100%; height:100%; position:fixed; top:0px; left:0px; z-index:9999998; display:none;' class='mobile_menu_cover'>
<div style='width:90%; height:100%; background:#fff;  position:fixed; top:0px; left:0px; box-shadow: 0px 5px 4px rgba(0,0,0,0.1); z-index:9999999; display:none;' class='mobile_menu_list'>
	<div style='position:relative; height:100%; width:100%; '>
        <div style='width:100%; height:200px; overflow:hidden; z-index:1;' id='vertical_mobile_hidden'>
            <div style='padding:0px 0px; z-index:2;'>

           	<div class='close_mobile_menu' style='position:absolute; top:0px; right:10px; color: #3498db; font-size:12px; cursor:pointer; width:40px;height:40px; background:url(/css_tool/cross.png)'></div>

            <div style='padding:0px 20px; padding-top:60px; padding-bottom:15px; background:#FAFAFA; border-bottom:1px solid #ccc;'>

                    <!--ПОИСК-->
                    <?
                    $s = '';
                    if ($this->route == 'site/search')
                    {
                        if (isset($_GET['search_text']))
                        {
                            $s = $_GET['search_text'];
                        }
                    }
                    ?>
               <form class='s_form' method="post">
                <input type="text" value='<?=$s;?>' class='search_text' name='search_text' style='width:185px; padding-left:10px; padding-right:10px; height:30px; line-height:30px; border:1px solid #ccc; border-radius:3px; font-size:14px; outline:none; float:left;' placeholder='Поиск'  />


                    <div style='width:70px; background:#53952A; border-radius: 3px; height:32px; text-align:center; cursor:pointer; line-height:32px; font-size:12px; color:#fff; float:right;' onclick='search_start()'>Найти</div>

<div style='clear:both;'></div>
                </form>

                        </div>




            <div style='padding:30px 20px; padding-top:10px;'>
    	    	<ul style='padding:0px; margin:0px; list-style:none;' class='font_sub'>
                            <?
                            foreach ($this->menu as $item)
                            {
                                    $class = '';
                                if (Yii::app()->controller->id == 'eng'){
                                    $class = $item['active'] ? 'active' : '';
                                }else{
                                    if ($item['id'] == $this->razdel['id']){
                                        $class = 'active';
                                    }
                                }
                                ?>
                                <li class=''>
                                    <a href="<?=$item['url'];?>" style='padding:8px 0px; border-bottom:1px dotted #ccc; font-size:13px; display:block;' class='mobile_menu_item <?=$class;?>'>                                                      <?=$item['label'];?>
                                    </a>
                                </li>
                                <?php
                                if (isset($item['submenu']))
                                {
                                    foreach($item['submenu'] as $subitem){
                                    ?>
                                    <li class=''>
                                    <a href="<?=$subitem['link'];?>" style='padding:8px 15px; border-bottom:1px dotted #ccc; font-size:13px; display:block;' class='mobile_menu_item'>                                                      <?=$subitem['name'];?>
                                    </a>
                                </li>
                                    <?
                                    }
                                }
                            }
                            ?>
    		    </ul>

            </div>
            </div>
        </div>
	</div>
</div>
</div>
</div>


<script>
function menu_mobile_extra_close(){
	 $('.mobile_menu_list').css('display','none');
   	 $('.mobile_menu_cover').css('display','none');
   	 $('.mobile_menu_list').css('left','-100%');
}
function menu_mobile_close(){
 $('.mobile_menu_list').animate({'left': "-100%"}, 300, function(){
	 $('.mobile_menu_list').css('display','none');
   	 $('.mobile_menu_cover').css('display','none');
     $('html').css('overflow','visible');
 });
}

function menu_mobile_open(){
 $('html').css('overflow','hidden');
 $('.mobile_menu_list').css('left','-100%');
 $('.mobile_menu_cover').css('display','block');
 $('.mobile_menu_list').css('display','block');

 var display_height = $(window).height();
  $('#vertical_mobile_hidden').css('height',display_height+'px');


 $('.mobile_menu_list').animate({'left': "0px"}, 300);
     setTimeout(function(){
         <?
            $detect2 = new Mobile_Detect;
            if($detect2->isMobile())
            {
                if($detect2->is('iOS'))
                {
                    ?>
                    $(function(){
                        	t10 = new IScroll('#vertical_mobile_hidden', {scrollY: true, tap: true, click: true,});
                    })
                    <?
                }else
                {
                    ?>
                    $(function(){
                        	t10 = new IScroll('#vertical_mobile_hidden', {scrollY: true,});
                    })
                    <?
                }
            }
            ?>
     },320);
}

$('.menu_mobile_burger, .zoo_logo, .zoo_top_subtitle').on('click',function(){
	menu_mobile_open();
});

$('.close_mobile_menu').on('click',function(){
menu_mobile_close();
});









 function seach_show(){
    $('.search_input').css('display','block');
 }

 function search_hide(){
    $('.search_input').css('display','none');
 }

 function search_start(){
     var search = $('.search_text').val();
     if (search != ''){
        $('.s_form').attr('action','<?=Yii::app()->createUrl('site/search');?>'+ '&search_text='+search);
        $('.s_form').submit();
     }
 }

</script>