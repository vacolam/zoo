<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi">
<title><?=$this->pageTitle;?></title>
<?
Yii::app()->clientScript->registerMetaTag($this->siteKeywords, 'keywords');
Yii::app()->clientScript->registerMetaTag($this->siteDescription, 'description');
?>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon_apple.png">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="css_tool/jquery.min.js"></script>
	<script type="text/javascript" src="css_tool/jquery-ui.js"></script>
    <link rel="stylesheet" href="js/eye/css/bvi.min.css" type="text/css">

    <link href="css_tool/style_10032016.css?id=65" rel="stylesheet">
	<link href="/css_tool/style_mobile.css?r=63" rel="stylesheet" type="text/css">


        <link href="css_tool/redactor-styles.css?v=12" rel="stylesheet">
    <style>
    html,body{
        font-family:Open Sans, Verdana, Tahoma, Arial; font-weight:400;
    }
    .open-s{font-family:Open Sans, Verdana, Tahoma, Arial; font-weight:400;
    }
    a, a:hover{
        text-decoration:none;
    }
    </style>
    <?
        $detect2 = new Mobile_Detect;
    ?>



</head>
<body style='padding:0px; margin:0px; width:100%;'>
<?
if(!$detect2->isMobile())
{
    $this->renderPartial('//layouts/_top', array());
}else{
    $this->renderPartial('//layouts/_top_mobile');
}
echo $content;



$company_contacts = Page::getContatcs();


//Website bottom
if(!$detect2->isMobile())
{
$this->renderPartial('//layouts/_bottom_logos',array('detect2'=>$detect2,'company_contacts'=>$company_contacts));
$this->renderPartial('//layouts/_bottom',array('detect2'=>$detect2,'company_contacts'=>$company_contacts));
}

//Mobile bottom
if($detect2->isMobile())
{
    if ($this->route != 'home/index' AND Yii::app()->controller->id != 'eng')
    {
        $this->renderPartial('//layouts/_bottom_mobile_blocks',array('detect2'=>$detect2));
    }
    $this->renderPartial('//layouts/_bottom_logos_mobile',array('detect2'=>$detect2,'company_contacts'=>$company_contacts));
    $this->renderPartial('//layouts/_bottom_mobile',array('detect2'=>$detect2,'company_contacts'=>$company_contacts));
}

    $this->renderPartial('//layouts/_permission',array('detect2'=>$detect2));
?>










<div style='text-align:center; font-size:14px; color:#000; background:#fff; width:80px; height:30px; border:1px solid #000; box-shadow: 0px 0px 5px rgba(0,0,0,.1); line-height:30px; cursor:pointer;' class='open-s' id='scrolltotop' onclick='ScrollToTop()'>
Наверх
</div>
<script>
function scrolltotop_display()
{
    var el=$('#scrolltotop');
    if((window.pageYOffset||document.documentElement.scrollTop)>window.innerHeight)
    { if(!el.is(':visible')) { el.stop(true, true).fadeIn(); } }
    else { if(!el.is(':animated')) { el.stop(true, true).fadeOut(); }}
}
function scrolltotop_position()
{
    var el=$('#scrolltotop');
    el.css('top', window.innerHeight-100);
    el.css('left', 40); //window.innerWidth+100
    scrolltotop_display();
}
$(window).on('load', function(){
    $('#scrolltotop').css('display', 'none');
    $('#scrolltotop').css('position', 'fixed');
    scrolltotop_position();
   // $('#scrollToTop a').removeAttr('href');
});
function ScrollToTop(){
    $('html, body').animate({scrollTop:0}, 500);
}
$(window).on('scroll', scrolltotop_display);
$(window).on('resize', scrolltotop_position);

$(function(){
$(".redactor-styles img").click(function() {
    var src = $(this).attr('src');

        $.fancybox.open([
            {
                                        href : src,
                                        title : '<a href="'+src+'" target="_blank" style="display:block; width:30px; height:30px; background:url(css_tool/download.png) center; background-size:cover;" title="Скачать"></a>',
            }
        	], {
            index:0,
            padding:0,
            margin   : [0,0,0,0],
            beforeLoad: function() {
                this.title = this.title;
 		    },
            onUpdate:function(){
            },
    		helpers:  {
                overlay: {
                  locked: false
                },
                 title: {
                        type: ''
                    }
            },
            aspectRatio : true
        });

        return false;


});
});
</script>


<?
if($detect2->isMobile())
{
    ?>
    <script src="/js/touchpunch.js"></script>
    <script src="/js/iscroll.js"></script>
    <script>
    function obertka_tables(){
        $(".redactor-styles table").wrap("<div class='table_iscroll'><div class='table_iscroll_in'></div></div>");
    }

    $(function(){
        obertka_tables();

        if ($('.table_iscroll').length > 0){

        var t = 0;
        $('.table_iscroll').each(function(){
            t = t + 1;
            $(this).addClass('eq_' + t);
            t1 = new IScroll('.table_iscroll.eq_'+t, { scrollX: true, scrollY: false, mouseWheel: true,eventPassthrough:true });
        })


}
    })
    </script>
    <?
}

echo $company_contacts['metrics'];
?>
<script src="js/eye/js/js.cookie.js"></script>
<?
if(!$detect2->isMobile())
{
?>
<script src="js/eye/js/bvi-init.js"></script>
<script src="js/eye/js/bvi.min.js"></script>
<?
}
?>
</body>
</html>
