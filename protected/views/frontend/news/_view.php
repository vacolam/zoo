<div style='padding-bottom:20px; border-bottom:1px dotted #ccc; margin-bottom:20px;' class="news_item">
    <?

if(!$detect2->isMobile())
{
        $bg =  "background:none; ";
        $bg_url = '';
        if ($data->hasImage()) {
            $bg_url = $data->getThumbnailUrl();
            $bg =  '';//background:url("'.$bg_url.'") #f0f0f0 center; background-size:cover;';
        }
    ?>
<a href="<?=Yii::app()->createUrl('news/view',array('id' => $data->id));?>" style=' <?=$bg;?>' class='news_item_photo'>
    <?
    if ($bg_url != '')
    {
        ?>
        <img src="<?=$bg_url;?>" alt="" style='width:200px; border-radius:5px;'/>
        <?
    }
    ?>
</a>
<?
}else{
    if ($data->hasImage()) {
            $bg_url = $data->getThumbnailUrl();
            $bg =  '';//background:url("'.$bg_url.'") #f0f0f0 center; background-size:cover;';
    ?>
    <a href="<?=Yii::app()->createUrl('news/view',array('id' => $data->id));?>" style=' <?=$bg;?>' class='news_item_photo'>
    <?
    if ($bg_url != '')
    {
        ?>
        <img src="<?=$bg_url;?>" alt="" style='width:320px; border-radius:3px;'/>
        <?
    }
    ?>
    </a>
    <?
    }
}
?>

<div style='' class='news_item_description'>

    <a href="<?=Yii::app()->createUrl('news/view',array('id' => $data->id));?>" style='' class='news_item_title'><?=$data->title;?></a>

    <div style='' class='news_item_date'>
        <?
        echo Dates::getDatetimeName($data->date);
        ?>
    </div>


    <div style=' ' class='news_item_shorttext'>
        <a href="<?=Yii::app()->createUrl('news/view',array('id' => $data->id));?>">
    	<p><?= $data->short_text; ?></p>
        </a>
    </div>
</div>

<div style='clear:both;'></div>

</div>