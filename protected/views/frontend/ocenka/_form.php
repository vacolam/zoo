<?
$detect2 = new Mobile_Detect;
?>
<style>
input[type=text] {
    height:40px; font-size:16px; line-height:40px; width:350px; padding: 0px 15px;
    border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}

label{
    display:block; padding-bottom:10px;
}

.error{
    padding:7px 12px; background:#DC3545; color:#fff; font-size:14px; margin-top:5px; border-radius:5px; display:none;
}
</style>

<div class='ocenka_preview' style='width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px; display:block;'>
    <div style='padding:30px;'>
    <div style='width:140px; height:200px; background:url(/css_tool/question.png) center; background-size:cover; float:left; margin-left:40px; margin-right:80px;' class='form_photo'></div>
    <div class='form_long_description'>
        <div class='form_title'>Пройдите опрос</div>
        <div class='form_text'>Нам очень важно знать Ваше мнение о нашей работе. Помогите нам понять, что хорошо, что плохо, что лучше оставить, а что можно изменить.</div>
        <div onclick='show_ocenka()' class='form_button' style='background:#007BFF; width:150px;'>Пройти опрос</div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>


<?
if($detect2->isMobile())
{
?>
<div class='ocenka ocenka_preview'  style='display:none;'>
    <div class='form_padding'>
        <div class='form_description'>
            <div class='form_title'>Зайдите с компьютера</div>
            <div class='form_text'>В мобильной версии отправка заявки не работает. Зайдите на эту страницу с компьютера.</div>
        </div>
    </div>
</div>
<?
}


if(!$detect2->isMobile())
{
?>

<div class='ocenka_thnx' style='width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px; display:none;'>
    <div style='padding:40px;'>
    <div style='width:160px; height:160px; background:url(/css_tool/super_dad.png) center; background-size:cover; float:left; margin-left:0px; margin-right:80px;'></div>
    <div style='width:500px; float:left;'>
        <div style='font-size:36px; font-weight:200;'>Спасибо!!!</div>
        <div style='font-size:15px; line-height:1.5em; font-weight:200; padding-top:15px;'><i>Спасибо, что нашли время пройти этот опрос! Информацию получит наш специалист и внимательно изучит. Если все отлично, то мы рады стараться, если у Вас были замечания, мы постараемся исправить.</i></div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>

<div class='ocenka' style='display:none; width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px;'>
<div style='padding:70px 45px; padding-bottom:85px; '>
<form class='form'>

                    <div style="float:left; width:350px;" class='input-block-1'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Ваше имя<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="Ocenka[name]" placeholder="Ваше имя" class="name" />
                            <div class='error'>Заполните имя</div>
                        </div>
                    </div>

                    <div style="float:right; width:350px;" class='input-block-2'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Город
                            </div>
                            <input type="text" name="Ocenka[city]" placeholder="Город" class="city" />
                        </div>
                    </div>

                    <div style='clear:both;'></div>


                    <div style="float:left; width:350px; margin-top:40px;" class='input-block-3'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Телефон<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="Ocenka[phone]" placeholder="Телефон" class="phone" />
                            <div class='error'>Заполните телефон</div>
                        </div>
                    </div>

                    <div style="float:right; width:350px; margin-top:40px;" class='input-block-4'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Электронная почта
                            </div>
                            <input type="text" name="Ocenka[email]" placeholder="Электронная почта" class="email" />
                        </div>
                    </div>

                    <div style='clear:both;'></div>


                  <div style="margin-top:50px; border-bottom:1px dotted #ccc; padding-bottom:20px;margin-bottom:20px;" class='input-question-1'>

                            <p style="font-weight:600; font-size:18px;">
                                1. Оценить доступность информации об организации<span style='color:#DC3545;'>*</span>
                                <div class='error'>Выберите хотя бы один пункт</div>
                            </p>
                            <input type="hidden" name='Ocenka[question1]' value='1. Оценить доступность информации об организации' />
                            <div style='padding-top:20px;'>
                                <label class="radio">
                                    <input   id="radio11" name="Ocenka[radio1]" type="radio" value="Отлично">
                                     Отлично
                                </label>
                                <label class="radio">
                                    <input   id="radio12" name="Ocenka[radio1]" type="radio" value="Хорошо">
                                     Хорошо
                                </label>
                                <label class="radio">
                                    <input   id="radio13" name="Ocenka[radio1]" type="radio" value="Удовлетворительно">
                                     Удовлетворительно
                                </label>
                              <label class="radio">
                                    <input   id="radio14" name="Ocenka[radio1]" type="radio" value="Ниже среднего">
                                     Ниже среднего
                                </label>
                              <label class="radio">
                                    <input   id="radio15" name="Ocenka[radio1]" type="radio" value="Неудовлетворительно">
                                     Неудовлетворительно
                                </label>
                            </div>
                  </div>


                  <div style=" border-bottom:1px dotted #ccc; padding-bottom:20px;margin-bottom:20px;" class='input-question-2' >
                            <p style="font-weight:600; font-size:18px;">
                                2. Комфортность условий предоставления услуг и доступности их получения<span style='color:#DC3545;'>*</span>
                                <div class='error'>Выберите хотя бы один пункт</div>
                            </p>
                            <input type="hidden" name='Ocenka[question2]' value='2. Комфортность условий предоставления услуг и доступности их получения' />
                            <div style='padding-top:20px;'>
                                <label class="radio">
                                    <input   id="radio21" name="Ocenka[radio2]" type="radio" value="Отлично">
                                     Отлично
                                </label>
                                <label class="radio">
                                    <input   id="radio22" name="Ocenka[radio2]" type="radio" value="Хорошо">
                                     Хорошо
                                </label>
                                <label class="radio">
                                    <input   id="radio23" name="Ocenka[radio2]" type="radio" value="Удовлетворительно">
                                     Удовлетворительно
                                </label>
                              <label class="radio">
                                    <input   id="radio24" name="Ocenka[radio2]" type="radio" value="Ниже среднего">
                                     Ниже среднего
                                </label>
                              <label class="radio">
                                    <input   id="radio25" name="Ocenka[radio2]" type="radio" value="Неудовлетворительно">
                                     Неудовлетворительно
                                </label>
                            </div>

                            
                  </div>


                  <div style="border-bottom:1px dotted #ccc; padding-bottom:20px;margin-bottom:20px;" class='input-question-3'>
                            <p  style="font-weight:600; font-size:18px;">
                                3. Время ожидания предоставления услуги<span style='color:#DC3545;'>*</span>
                                <div class='error'>Выберите хотя бы один пункт</div>
                            </p>
                            <input type="hidden" name='Ocenka[question3]' value='3. Время ожидания предоставления услуги' />
                           <div style='padding-top:20px;'>
                                <label class="radio">
                                    <input   id="radio31" name="Ocenka[radio3]" type="radio" value="Отлично">
                                     Отлично
                                </label>
                                <label class="radio">
                                    <input   id="radio32" name="Ocenka[radio3]" type="radio" value="Хорошо">
                                     Хорошо
                                </label>
                                <label class="radio">
                                    <input   id="radio33" name="Ocenka[radio3]" type="radio" value="Удовлетворительно">
                                     Удовлетворительно
                                </label>
                              <label class="radio">
                                    <input   id="radio34" name="Ocenka[radio3]" type="radio" value="Ниже среднего">
                                     Ниже среднего
                                </label>
                              <label class="radio">
                                    <input   id="radio35" name="Ocenka[radio3]" type="radio" value="Неудовлетворительно">
                                     Неудовлетворительно
                                </label>
                            </div>
                    </div>

                  <div style="border-bottom:1px dotted #ccc; padding-bottom:20px;margin-bottom:20px;" class='input-question-4'>
                            <p  style="font-weight:600; font-size:18px;">
                                4. Доброжелательность, вежливость, компетентность работников организации<span style='color:#DC3545;'>*</span>
                                <div class='error'>Выберите хотя бы один пункт</div>
                            </p>
                            <input type="hidden" name='Ocenka[question4]' value='4. Доброжелательность, вежливость, компетентность работников организации' />
                            <div style='padding-top:20px;'>
                                <label class="radio">
                                    <input   id="radio41" name="Ocenka[radio4]" type="radio" value="Отлично">
                                     Отлично
                                </label>
                                <label class="radio">
                                    <input   id="radio42" name="Ocenka[radio4]" type="radio" value="Хорошо">
                                     Хорошо
                                </label>
                                <label class="radio">
                                    <input   id="radio43" name="Ocenka[radio4]" type="radio" value="Удовлетворительно">
                                     Удовлетворительно
                                </label>
                              <label class="radio">
                                    <input   id="radio44" name="Ocenka[radio4]" type="radio" value="Ниже среднего">
                                     Ниже среднего
                                </label>
                              <label class="radio">
                                    <input   id="radio45" name="Ocenka[radio4]" type="radio" value="Неудовлетворительно">
                                     Неудовлетворительно
                                </label>
                            </div>
                    </div>

                  <div style="border-bottom:1px dotted #ccc; padding-bottom:20px;margin-bottom:20px;" class='input-question-5'>
                            <p  style="font-weight:600; font-size:18px;">
                                5. Удовлетворенность качеством оказания услуг<span style='color:#DC3545;'>*</span>
                                <div class='error'>Выберите хотя бы один пункт</div>
                            </p>
                            <input type="hidden" name='Ocenka[question5]' value='5. Удовлетворенность качеством оказания услуг' />
                            <div style='padding-top:20px;'>
                                <label class="radio">
                                    <input   id="radio51" name="Ocenka[radio5]" type="radio" value="Отлично">
                                     Отлично
                                </label>
                                <label class="radio">
                                    <input   id="radio52" name="Ocenka[radio5]" type="radio" value="Хорошо">
                                     Хорошо
                                </label>
                                <label class="radio">
                                    <input   id="radio53" name="Ocenka[radio5]" type="radio" value="Удовлетворительно">
                                     Удовлетворительно
                                </label>
                              <label class="radio">
                                    <input   id="radio54" name="Ocenka[radio5]" type="radio" value="Ниже среднего">
                                     Ниже среднего
                                </label>
                              <label class="radio">
                                    <input   id="radio55" name="Ocenka[radio5]" type="radio" value="Неудовлетворительно">
                                     Неудовлетворительно
                                </label>
                            </div>
                    </div>

                  <div style="border-bottom:1px dotted #ccc; padding-bottom:20px;margin-bottom:20px;">
                            <p style="font-weight:600; font-size:16px; padding-bottom:7px;">
                                Комментарий
                            </p>
                            <div >
                            <textarea name="Ocenka[comment]" id="comment1" style='width:740px; font-size:16px; line-height:40px;  padding: 0px 15px; border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;'></textarea>
                            </div>
                    </div>


                    <div style='cursor:pointer; width:100%; border:none; height:40px; border-radius:4px; margin-top:0px; color:#fff; line-height:40px; font-size:14px; text-align:center; background:#007BFF;' onclick='form_submit()' class='sbmt_btn'>Отправить</div>
            </form>
            </div>
            </div>

<?
}
?>

<script>
function show_ocenka(){
    $('.ocenka_preview').css('display','none');
    $('.ocenka').css('display','block');
}

function check_form_errors(){
    var t = 0;
    if ($('.input-block-1').find('input').val() == ''){t = t + 1; $('.input-block-1').find('.error').css('display','block');}
    if ($('.input-block-3').find('input').val() == ''){t = t + 1; $('.input-block-3').find('.error').css('display','block');}

    if ($('.input-question-1 input[name="Ocenka[radio1]"]:checked').val() == undefined){t = t + 1; $('.input-question-1').find('.error').css('display','block');}
    if ($('.input-question-2 input[name="Ocenka[radio2]"]:checked').val() == undefined){t = t + 1; $('.input-question-2').find('.error').css('display','block');}
    if ($('.input-question-3 input[name="Ocenka[radio3]"]:checked').val() == undefined){t = t + 1; $('.input-question-3').find('.error').css('display','block');}
    if ($('.input-question-4 input[name="Ocenka[radio4]"]:checked').val() == undefined){t = t + 1; $('.input-question-4').find('.error').css('display','block');}
    if ($('.input-question-5 input[name="Ocenka[radio5]"]:checked').val() == undefined){t = t + 1; $('.input-question-5').find('.error').css('display','block');}

    return t;
}

function form_submit(){
    $('.sbmt_btn').text('Отправляется...');
    $('.error').css('display','none');

    var t = 0;

    t = check_form_errors();

    if (t == 0){
    var form = $('.form').serialize();
            $.ajax({
                type: "POST",
                url: '<?= Yii::app()->createUrl('ocenka/vote', array()); ?>',
                dataType: 'json',
                data: form,
                success: function(response){
                    if (response == true){
                        setTimeout(function(){
                            $('.ocenka').css('display','none');
                            $('.ocenka_thnx').css('display','block');
                        },500);
                    }else{
                        //show_ozon('yandex_rtb_R-A-400319-2');
                    }
                },
                error: function(response){
                }
            });
    }else{
        $('.sbmt_btn').css('background','#DC3545');
        $('.sbmt_btn').text('Ошибки в заполнении');

        setTimeout(function(){
                $('.sbmt_btn').css('background','#007BFF');
                $('.sbmt_btn').text('Отправить');
        },5000);
    }


}
</script>