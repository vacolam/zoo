<?
$this->pageTitle =  $model->name.'. Сахалинский Зооботанический парк.';
?>

<script src="/js/pannellum/pannellum.js" defer></script>
<link rel="stylesheet" href="/js/pannellum/pannellum.css">
<style>

        #app {
            margin: 0 auto;
        }

        .header {
            font-size: 1.5rem;
        }

        #menu {
            list-style: none;
            padding: 0;
            margin: 0 0 1rem 0;
        }

        #menu li {
            display: inline-block;
            margin-right: 2rem;
            cursor: pointer;
        }

        #menu li:hover {
            text-shadow: 1px 1px 2px darkgrey;
        }

        #menu li:last-child {
            margin-right: 0;
        }

        #view {
            width: 100%;
            height: 600px;
        }

        .main_container.news{width: 830px;}
        @media screen and (max-width:800px) {
        .main_container.news{width:auto;}

        #view {
            width: 100%;
            height: 400px;
        }
        }
    </style>
    <script>
        const panoramas = {

             terra: {
                title: "Экзотариум",
                type: "equirectangular",
                panorama: "upload/pano/sm/terra.jpg",
                autoLoad: true
            },
            fish: {
                title: "Аквариум",
                type: "equirectangular",
                panorama: "upload/pano/sm/fish.jpg"
            },
            monkeys: {
                title: "Приматник",
                type: "equirectangular",
                panorama: "upload/pano/sm/monkeys.jpg"
            },

        };

        document.addEventListener('DOMContentLoaded', () => {
            const menuEl = document.getElementById('menu');
            const panoViewer = pannellum.viewer(document.getElementById('view'), {
                default: {
                    "firstScene": "terra",
                    "sceneFadeDuration": 1000
                },
                scenes: panoramas
            });

            const onMenuItemClick = (e) => {
                panoViewer.loadScene(e.target.getAttribute('data-name'));
            };

            for (const [name, options] of Object.entries(panoramas)) {
                let item = document.createElement('li');
                item.innerText = options.title;
                item.onclick = onMenuItemClick;
                item.setAttribute('data-name', name);
                menuEl.appendChild(item);
            }
        });
    </script>
<?
$detect2 = new Mobile_Detect;
?>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left'>
<div style='padding-bottom:20px;' class='pages_view_title'><?=$model->name;?></div>



    <main id="app">
        <ul id="menu"></ul>
        <div id="view"></div>
    </main>
</div>


<div style='width: 300px; margin-top:0px; float:right;' class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

    <div style='margin-top:50px;'>
        <div style='padding-bottom:5px; text-transform:uppercase; font-size:16px; '><b>Партнеры</b></div>
        <?
        $this->renderPartial('//layouts/_partners', array());
        ?>
    </div>
</div>

<div style='clear:both;'></div>



</div>
</div>




