<?
$this->pageTitle =  $model->name.'. Сахалинский Зооботанический парк.';
?>
<style>
.main_container.news{width: 830px;}
@media screen and (max-width:800px) {
.main_container.news{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container news'>
<?php
/**
 * Представление простых страниц
 *
 * @var string $text содержание текстовой страницы
 */
?>

<div class='pages_view_title'><?=$model->name;?></div>

<div style='' class='partners_padding_top'>

<?php
$i = 0;
$count = count($partners);
foreach ($partners as $partner){
    $i++;
    $border =  'border-bottom:1px dotted #ccc; ';
    if ($i == $count){$border='';}
    ?>
    <div style=' <?=$border;?>' class='partners_item_block'>

                        <a href='<?= $partner->link;?>' target='_blank' style=' background: url(<?= $partner->getImageUrl(); ?>) center #F8F8F8; background-size:cover;' alt="" class="partners_item_photo slide-img">
                        </a>
                        <div style='' class='partners_item_description'>
                            <a href='<?= $partner->link;?>' target='_blank' style='' class='partners_item_title open-s'><b><?=$partner->title;?></b></a>
                            <div style='' class='partners_item_text open-s'><i><?=$partner->description;?></i></div>
                        </div>
                        <div style='clear:both;'></div>


    </div>
    <?
}
?>


<div style='' class='partners_custodiand_title'><b>Опекуны</b></div>
<?php
$i = 0;
$count = count($custodians);
foreach ($custodians as $custodian){
    $i++;
    $border =  'border-bottom:1px dotted #ccc; ';
    if ($i == $count){$border='';}
    ?>
    <div style='<?=$border;?>' class='partners_item_block'>

                        <a href='<?= $custodian->link;?>' target='_blank' style='background: url(<?= $custodian->getImageUrl(); ?>) center #F8F8F8; background-size:cover;' class="partners_item_photo slide-img">
                        </a>
                        <div style='' class='custodians_item_description'>
                            <a href='<?= $custodian->link;?>' target='_blank' style='' class='partners_item_title open-s'><b><?=$custodian->title;?></b></a>
                            <div style='' class='partners_item_text open-s'><i><?=$custodian->description;?></i></div>
                                     <div class='custodians_item_animal_block'>
                                        <?
                                         if ($exposureItemsList){
                                                    if (isset($exposureItemsList[$custodian->animals_id])){
                                                    echo "<a href='".Yii::app()->createUrl('exposure/animal',array('id' => $custodian->animals_id))."' class='custodians_item_animal_name'><b>".$exposureItemsList[$custodian->animals_id]."</b></a>";
                                                    }
                                            }
                                        ?>
                                    </div>
                        </div>
                        <?
                        if ($custodian->animal){
                        ?>
                        <a href="<?= $custodian->animal->getImageUrl(); ?>" class='custodians_item_animal_photo fancybox-media' rel='animals' style=' background: url(<?= $custodian->animal->getThumbnailUrl(); ?>) center #53952A; background-size:cover; border-radius:2px;'>
                        </a>
                        <?
                        }
                        ?>
                        <div style='clear:both;'></div>


    </div>
    <?
}
?>
</div>



</div>



<div style='' class='main_right_submenu'>

<?
if (count($submenu)>0){
?>
    <?
     $this->renderPartial('//pages/_submenu', array(
                'submenu' => $submenu,
            ));
    ?>

<?
}
?>
</div>

<div style='clear:both;'></div>

</div>
</div>
<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>
