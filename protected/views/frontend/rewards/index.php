<?
$this->pageTitle =  $this->razdel['name'].". Сахалинский зооботанический парк.";
?>
<style>
.main_container.news{width: 830px;}
@media screen and (max-width:800px) {
.main_container.news{width:auto;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container news'>
<?php
/**
 * Представление простых страниц
 *
 * @var string $text содержание текстовой страницы
 */
?>
<style>
a.doc_item{
    color:#000;
}
a.doc_item:hover{
    color:#3397FF;
}

</style>
<div style='padding-top:0px;'>
<?php
if ($cats){
    foreach($cats as $cat)
    {
        ?>
        <div style='padding-bottom:20px;'>
        <div style='font-size:24px; padding-bottom:20px; text-transform:uppercase;'><b><?=$cat->title;?></b></div>
        <?
        $photos = RewardsCats::getPhotos($cat->id);

        $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$photos,
        'itemView'=>'_item',
        'template'=>"\n{items}\n{pager}",
        'emptyText'=>'',
        'ajaxUpdate'=>false,
            'pager'=>array(
                'nextPageLabel' => '<span>&raquo;</span>',
                'prevPageLabel' => '<span>&laquo;</span>',
                'lastPageLabel' => false,
                'firstPageLabel' => false,
                'class'=>'CLinkPager',
                'header'=>false,
                'cssFile'=>'/css_tool/pages.css', // устанавливаем свой .css файл
                'htmlOptions'=>array('class'=>'pager'),
            ),
        ));
    ?>
    <div style='clear:both;'></div>
    </div>
    <?
    }
}
?>

</div>


</div>



<div style='width: 300px; margin-top:0px; float:left;' class='main_right'>

<?
if (count($submenu)>0){
?>
    <?
     $this->renderPartial('//pages/_submenu', array(
                'submenu' => $submenu,
            ));
    ?>

<?
}
?>
</div>

<div style='clear:both;'></div>

</div>
</div>
<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>
