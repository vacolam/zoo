<div style='padding-bottom:20px; border-bottom:1px dotted #ccc; margin-bottom:20px;;'>
    <h2>
    <a href="<?=Yii::app()->createUrl('exposure/view',array('id' => $data['id']));?>" target='_blank' style='font-size:17px; font-weight:600; line-height:1.4em; text-transform:uppercase;'>
        <span style='padding:3px 10px; background:rgb(83,149,42); border:1px solid rgb(83,149,42); color:#fff; border-radius:3px; margin-right:5px; margin-bottom:5px; font-size:14px;'>Экспозиции</span>
        <?=$data['title'];?>
    </a>
    </h2>

    <div style='padding-top: 10px; font-size:14px; '>
        <a href="<?=Yii::app()->createUrl('exposure/view',array('id' => $data['id']));?>" target='_blank'>
    	<p><?= $data['short_text']; ?></p>
        </a>
    </div>
</div>
