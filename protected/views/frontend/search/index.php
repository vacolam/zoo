<?
$this->pageTitle =  "Результаты поиска. Сахалинский зооботанический парк.";
?>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left'>

<h2 class='pages_view_title'>Результаты поиска</h2>

<div style='padding-top:50px;'>
<?php
if ($news){
    foreach($news as $item){
        $this->renderPartial('//search/_news', array('data'=>$item));
    }
}

if ($afisha){
    foreach($afisha as $item){
        $this->renderPartial('//search/_afisha', array('data'=>$item));
    }
}

if ($pages){
    foreach($pages as $item){
        $this->renderPartial('//search/_pages', array('data'=>$item));
    }
}

if ($conf){
    foreach($conf as $item){
        $this->renderPartial('//search/_conf', array('data'=>$item));
    }
}

if ($exposure){
    foreach($exposure as $item){
        $this->renderPartial('//search/_exposure', array('data'=>$item));
    }
}

if ($exposure_items){
    foreach($exposure_items as $item){
        $this->renderPartial('//search/_exposure_item', array('data'=>$item));
    }
}
?>
</div>

<div style='clear:both;'></div>
</div>

<div style='width: 300px; margin-top:0px; float:right;'  class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

    <div style='margin-top:50px;'>
        <div style='padding-bottom:5px; text-transform:uppercase; font-size:16px; '><b>Партнеры</b></div>
        <?
        $this->renderPartial('//layouts/_partners', array());
        ?>
    </div>
</div>

<div style='clear:both;'></div>


</div>
</div>