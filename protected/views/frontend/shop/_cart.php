<style>
.cart_count_arrow{display:none;}
.cart_buble{display:block;}
</style>

<?
if ($this->route == 'shop/cart')
{
    ?>
    <style>
    @media screen and (max-width:800px)
    {
    .cart_buble{display:none;}
    }
    </style>
    <?
}
?>

<a href='<?=Yii::app()->createUrl('shop/cart');?>' style='width:250px; height:60px;  position:fixed; bottom:40px; left:50%; margin-left:-100px;' class='cart_buble'>
<div style='width:250px; height:60px; position:relative;'>
<div style='background:#F88718; height:60px; box-shadow:0px 0px 10px rgba(0,0,0,0.3); width:60px; border-radius:50%; position:absolute; z-index:22; left:0px; top:0px;'>
    <div style='padding-top:0px; padding-left:0px;'>
    <div style='width:60px; height:60px; background:url(css_tool/cart2.png) center; background-size:cover; display:none;'></div>
    <div style='height:40px; width:40px; margin:0px auto; margin-top:10px; background:url(css_tool/added.png) center;'></div>
    </div>
</div>

<div style='width:200px;  position:absolute; z-index:11; right:10px; top:10px; height:40px; background:#F88718; box-shadow:0px 0px 10px rgba(0,0,0,0.3);  border-radius:0px 20px 20px 0px;'>
    <span style='height:40px; line-height:40px; color:#fff; padding-left:30px;'>Корзина</span>
    <span style='height:40px; line-height:40px; color:#fff; padding-left:5px;'>|</span>
    <span style='height:40px; line-height:40px; color:#fff; padding-left:5px; ' class='cart_count_text'>0 шт</span>
    <span style='height:40px; line-height:40px;  color:#fff; padding-left:25px; ' class='cart_count_arrow'><b>&rarr;</b></span>
</div>
</div>
</a>

<script>
var shop_array = new Object();
var index = 0;
function shop_add_item(id){
    var quontity = $('.shop_quontity').val();
    if (parseInt(quontity) == 0){quontity = 1;}
    cart_add(id,quontity);

    $('.shop_button_add').css('display','none');
    $('.shop_button_process').css('display','block');

    /*setTimeout(function(){
            $('.shop_button_process').text('Добавлено в корзину');
    },800)
      */
    setTimeout(function(){
        $('.shop_button_process').css('display','none');
        $('.shop_button_ok').css('display','block');
    },1800)

}

function shop_list_add_item(id,quontity){
    var item_object = $('.expo_animal_item[rel="'+id+'"]');
        var quontity = $('.shop_quontity').val();
        if (parseInt(quontity) == 0){quontity = 1;}
        cart_add(id,quontity);

        item_object.find('.shop_button_add').css('display','none');
        item_object.find('.shop_button_process').css('display','block');

        setTimeout(function(){
                item_object.find('.shop_button_process').css('display','none');
                item_object.find('.shop_button_ok').css('display','block');
        },1800)

}

function cart_load(){

        var cart_products = Cookies.get('cart_products');
        if (cart_products == 'undefined' || cart_products == undefined)
        {

        }else{
            var loaded_array = $.parseJSON(cart_products);
             shop_array = loaded_array;
        }

        console.log('массив загрузился');
        console.log(shop_array);

        cart_text();
}

function cart_add(id,quontity = 0){
    if (quontity == 0){quontity = 1;}
    var c = parseInt(quontity);
    if(id in shop_array){
       shop_array[id] = shop_array[id] + c;
    }else{
       shop_array[id] = c;
    }
    console.log(shop_array);
    cart_save();
    cart_text();
}

function cart_save(){
    var json_array = JSON.stringify(Object.assign({}, shop_array));

    var cart_products = Cookies.get('cart_products');
    if (cart_products == 'undefined' || cart_products == undefined)
    {
        Cookies.set('cart_products', json_array, { expires: 60 });
    }else{
        Cookies.set('cart_products', json_array, { expires: 60 });
    }
}

function cart_text(){

    var count = 0;
    $.each(shop_array, function(index,value){
        count = count + parseInt(value);
    });
    $('.cart_count_text').text(count + ' шт');

}

function cart_update(){

    // меняем массив и сохраняем его в куки
    var new_cart_array = new Array();
    $('.product_line').each(function(index,obj){
        var id = $(obj).attr('rel');
        var quontity = parseInt( $(obj).find('.cart_quontity').val() );
        new_cart_array[id] = quontity;
    });

    shop_array = new_cart_array;
    cart_save();

    //пересчитываем цифры в корзине
    var count = 0;
    var summ = 0;
    $('.product_line').each(function(index,obj){
        var id = $(obj).attr('rel');

        var quontity = parseInt( $(obj).find('.cart_quontity').val() );
        count = count + quontity;

        var current_price = parseInt( $(obj).find('.cart_price').val() );
        var current_sum = current_price*quontity;

        $(obj).find('.item_sum').text(current_sum + ' РУБ.');

        summ = summ + current_sum;
    });

    //записываем новые конечные значения
    $('.total_count').text(count + " шт.");
    $('.total_sum').text(summ + " РУБ.");
    $('.cart_count_text').text(count + ' шт');
}


function cart_delete(id)
{
    $('.product_line[rel="'+id+'"]').remove();
    cart_update();
    /*
    var myIndex = shop_array.indexOf(id);
    if (myIndex !== -1) {
        myArray.splice(myIndex, 1);
    }
    console.log(myArray);
    cart_text();
    */
}

$(function(){
    cart_load();
    $('.cart_buble')
    .on('mouseover',function(){
        $('.cart_count_text').css('display','none');
        $('.cart_count_arrow').css('display','inline-block');
    })
    .on('mouseleave',function(){
        $('.cart_count_arrow').css('display','none');
        $('.cart_count_text').css('display','inline-block');
    })
})

</script>