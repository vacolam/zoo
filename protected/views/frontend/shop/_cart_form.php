<div class='cart_form'>

<?
$detect2 = new Mobile_Detect;
?>
<style>
.error{
    padding:7px 12px; background:#EA8690; color:#fff; font-size:12px; margin-top:4px; border-radius:5px; display:none;
}

input[type=text] {
    height:40px; font-size:16px; line-height:40px; width:310px; padding: 0px 15px;
    border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}

select {
    height:40px; font-size:16px; line-height:40px; width:340px; padding: 0px 15px;
    border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}

.input-block-name, .input-block-phone, .input-block-deliverytype, .input-block-adress{
width:300px; margin-top:20px;
}

.name, .phone, .deliverytype, .adress{

}

@media screen and (max-width:800px) {
input[type=text] {
    height:40px; font-size:16px; line-height:40px; width:100%; padding: 0px 0px;
    border:1px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}

select {
    height:40px; font-size:16px; line-height:40px; width:100%; padding: 0px 0px;
    border:1px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}

.input-block-name, .input-block-phone, .input-block-deliverytype, .input-block-adress{
width:100%; margin-top:10px;
}

.name, .phone, .deliverytype, .adress{

}

}
</style>

<div class='' style=''>
            <form class='form_conf'>

                    <div style="margin-top:0px;" class='input-block-name'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:4px;">
                                Ваше имя<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="ShopOrders[name]" placeholder="" class="name" />
                            <div class='error'>Заполните имя</div>
                        </div>
                    </div>

                    <div style="" class='input-block-phone'>
                    <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:4px;">
                                Телефон<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="ShopOrders[phone]" placeholder="" class="phone" />
                            <div class='error'>Заполните телефон</div>
                        </div>
                    </div>


                     <div style="" class='input-block-deliverytype'>
                            <div style="font-size:16px; font-weight:600; padding-bottom:4px;">
                                Как доставить?<span style='color:#DC3545;'>*</span>
                            </div>
                            <select name="ShopOrders[deliverytype]" size="1" class='deliverytype' style='padding:0px 10px; height:45px; line-height:45px; border:2px solid #ccc; border-radius:4px;'>
                                <option value="1" selected="selected">Заберу сам в зоопарке</option>
                                <option value="2" >Привезите курьером</option>

                            </select>
                            <div class='error'>Выберите способ доставки</div>
                        </div>

                    <div style="display:none;" class='input-block-adress'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:4px;">
                                Адрес доставки
                            </div>
                            <input type="text" name="ShopOrders[adress]" placeholder="" class="adress" />
                            <div class='error'>Заполните адрес</div>
                        </div>
                    </div>

                    <div style="padding-top:10px;" class='input-block-agreement'>
                        <div class="">
                            <input id="agreement" class='agreement' name="agreement" type="checkbox" value="1" checked="checked">
                            <label for="agreement">Я согласен с предоставлением персональных данных для информирования о статусе заказа</label>

                            <div class='error'>Без этого согласия заказ оформить нельзя</div>
                        </div>
                    </div>

                    <div style="padding-top:10px;" class='input-block-empty'>
                        <div class="">
                            <div class='error'>Пустую корзину нельзя оформлять</div>
                        </div>
                    </div>

        </form>
        <div style='cursor:pointer; width:100%; border:none; height:50px; border-radius:4px; margin-top:20px; color:#fff; line-height:50px; font-size:14px; text-align:center; background:#53952A;' onclick='form_submit()' class='sbmt_btn'>Оформить заказ</div>

</div>

<div class='ocenka_thnx' style='width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px; display:none;'>
    <div style='padding:40px;'>
    <div style='width:200px; height:147px; background:url(/css_tool/asset.png) center; background-size:cover; float:left; margin-left:0px; margin-right:80px;'></div>
    <div style='width:400px; float:left;'>
        <div style='font-size:36px; font-weight:200;'>Спасибо!!!</div>
        <div style='font-size:15px; line-height:1.5em; font-weight:200; padding-top:15px;'><i>Спасибо, что сделали заказ.</i></div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>
<script src="/js/jquery.inputmask.bundle.js?v=1"></script>
<script>
$(document).ready(function(){
     $(".phone").inputmask({"mask": "+7(999) 999-9999"});
});
</script>
<script>
function show_ocenka(){
    $('.ocenka_preview').css('display','none');
    $('.ocenka').css('display','block');
}

function check_form_errors(){

    var t = 0;
    if (  $('.agreement').is(':checked') == false  ){t = t + 1; $('.input-block-agreement').find('.error').css('display','block');}
    if ($('.name').val() == ''){t = t + 1; $('.input-block-name').find('.error').css('display','block');}
    if ($('.phone').val() == ''){t = t + 1; $('.input-block-phone').find('.error').css('display','block');}

    if ($('.deliverytype option:selected').val() == 0)
    {
        t = t + 1;
        $('.input-block-deliverytype').find('.error').css('display','block');
    }

    if ($('.deliverytype option:selected').val() == 2)
    {
        if ($('.adress').val() == ''){t = t + 1; $('.input-block-adress').find('.error').css('display','block');}
    }

    if (Object.keys(shop_array).length == 0) {
        t = t + 1;
        $('.input-block-empty').find('.error').css('display','block');
    }

    return t;
}

function form_submit(){
    $('.error').css('display','none');
    var t = 0;
    t = check_form_errors();

    if (t == 0){
    $('.sbmt_btn').text('Отправляется...');

        var form = $('.form_conf').serialize();
            $.ajax({
                type: "POST",
                url: '<?= Yii::app()->createUrl('shop/createorder', array()); ?>',
                dataType: 'json',
                data: form,
                success: function(response){
                    if (parseInt(response.id) > 0 ){
                        var yy = response.secret;
                        $.ajax({
                            type: "POST",
                            url: '<?= Yii::app()->createUrl('shop/orderitems'); ?>&order_id='+response.id+'&secret='+response.secret,
                            dataType: 'json',
                            data: shop_array,
                            success: function(response){
                                shop_array = new Array();
                                cart_save();
                                window.location = "<?= Yii::app()->createUrl('shop/order', array()); ?>&id="+yy;
                            },
                            error: function(response){
                            }
                        });

                    }
                },
                error: function(response){
                }
            });
    }
}

$(function(){
    $('.deliverytype').on('change',function(){

        if ($('.deliverytype option:selected').val() == 2)
        {
            $('.input-block-adress').css('display','block');
        }else{
            $('.input-block-adress').css('display','none');
        }
    })
})
</script>



</div>