
    <?
        $bg =  "background:#53952A; ";
        if ($data->hasImage()) {
            $bg_url = $data->getThumbnailUrl();
            $bg =  'background:url("'.$bg_url.'") center #53952A; background-size:cover;';
        }
        $marginRight='animal_left';
        if ($t == 1){$marginRight = 'animal_left';}
        if ($t == 2){$marginRight = 'animal_right';}
    ?>
<style>
.shop_button_add, .shop_button_process, .shop_button_ok{
    cursor:pointer; width:340px; height: 50px; background: #FFDB4D; border:1px solid rgba(0,0,0,0.2); border-radius: 4px;line-height: 50px;margin: 0px auto;text-align: center;font-size: 16px;color: #222; text-shadow: 0px 1px 0px #fff3c2;
}
.shop_button_add:hover, .shop_button_process:hover{
 background: #FFD429;
}

.shop_button_process, .shop_button_ok{
display:none;
}

.shop_button_ok{
background:#F88718;
}

.shop_button_ok:hover{
background:#F27D07;
}
</style>
<div style='' class='expo_animal_item <?=$marginRight;?>' rel='<?=$data->id;?>'>
<a href="<?=Yii::app()->createUrl('shop/item',array('id' => $data->id));?>" style='' class=''>
    <div style='<?=$bg;?> ' class='expo_animal_photo'></div>
    <div style='' class='expo_animal_description'>
        <div>
            <?
            if (isset($catslist[$data->cats_id]))
            {
                ?><div style='' class='expo_animal_type'><b><? echo $this->crop_str_word($catslist[$data->cats_id], 2 ); ?></b></div><?
            }
            ?>
        </div>
        <div style='' class='open-s expo_animal_title'><i><? echo $this->crop_str_word($data->title, 5 ); ?></i></div>
        <div style='margin-top:5px;' class='open-s'>
            <?
            if ($data->old_price == 0)
            {
                ?>
                    <span style='background:#53952A; color:#fff; padding:3px 8px; border-radius:3px; font-size:13px;'>
                    <?
                        echo $data->price." РУБ";
                    ?>
                    </span>
                <?
            }


            if ($data->old_price > 0)
            {
                ?>
                <span style='background:#E7E7E7; color:#222; padding:3px 8px; border-radius:3px; font-size:13px; text-decoration:line-through;'>
                    &nbsp;&nbsp;<?
                        echo $data->old_price." РУБ";
                    ?>&nbsp;&nbsp;
                </span>

                <span style='background:#F88718; color:#fff; padding:3px 8px; border-radius:3px; font-size:13px; margin-left:10px;'>
                    <?
                        echo $data->price." РУБ";
                    ?>
                </span>

                <?
            }
            ?>
        </div>
    </div>
    <div style='clear:both;'></div>
</a>
<div class='shop_button_add' onclick='shop_list_add_item(<?=$data->id;?>,1)' style='margin-top:3px; width:100%; border:1px solid #ccc; border-radius:4px; height:30px; line-height:30px; text-align:center; cursor:pointer;'>В корзину</div>
    <div class='shop_button_process' style=''>&bull;&bull;&bull;</div>
    <a href='<?=Yii::app()->createUrl('shop/cart');?>' class='shop_button_ok' style=' border:1px solid rgba(0,0,0,.1); text-align:center;'>
        <ul style='list-style:none; margin:0px;padding:0px; display:inline-block;'>
        <li style='height:40px; width:40px; margin-top:5px; background:url(css_tool/added.png) center; float:left;'></li>
        <li style='color:#fff; text-shadow:none; height:50px; line-height:50px; float:left;'>Оформить заказ &nbsp;&rarr;</li>
        <li style='clear:both;'></li>
        </ul>
    </a>
</div>
<?
if ($t == 2){?><div style='clear:both;'></div><?}
?>
