
    <?
        $bg =  'background:#E0E0E0;';
        if ($data->hasImage()) {
            $bg_url = $data->getThumbnailUrl();
            $bg =  'background:url("'.$bg_url.'") center #E0E0E0; background-size:cover;';
        }
        $marginRight='animal_left';
        if ($t == 1){$marginRight = 'animal_left';}
        if ($t == 3){$marginRight = 'animal_right';}
    ?>
<style>
.shop_button_add, .shop_button_process, .shop_button_ok{
    cursor:pointer; width:340px; height: 50px; background: #FFDB4D; border:1px solid rgba(0,0,0,0.2); border-radius: 4px;line-height: 50px;margin: 0px auto;text-align: center;font-size: 16px;color: #222; text-shadow: 0px 1px 0px #fff3c2;
}
.shop_button_add:hover, .shop_button_process:hover{
 background: #FFD429;
}

.shop_button_process, .shop_button_ok{
display:none;
}

.shop_button_ok{
background:#F88718;
}

.shop_button_ok:hover{
background:#F27D07;
}

.animal_left{
    margin-right:17px;
}
.animal_right{
    margin-right:0px;
}
</style>
<div style='width:270px; height:370px; border:1px solid #cccc; border-radius:3px; margin-bottom:17px;' class='expo_animal_item <?=$marginRight;?>' rel='<?=$data->id;?>'>
<a href="<?=Yii::app()->createUrl('shop/item',array('id' => $data->id));?>" style='width:270px; height:250px; display:block;' class=''>
    <div class='expo_animal_photo' style='border-radius:3px 3px 0px 0px; width:270px; height:250px; <?=$bg;?>'>
    <div style='display:none; position:relative; width:100%; height:100%; background-image: linear-gradient(-180deg, rgba(0,0,0,0) 38%, rgba(0,0,0,0.65) 79%); border-radius:4px;'>
        <div style='width:100%; position:absolute; left:0px; bottom:10px;'>
            <div style='' class='home_expoitem_padding'>
                 <div>
                    <?
                    if (isset($catslist[$data->cats_id]))
                    {
                        ?><div style='' class='expo_animal_type'><b><? echo $this->crop_str_word($catslist[$data->cats_id], 2 ); ?></b></div><?
                    }
                    ?>
                </div>
                <div style='' class='home_expoitem_title open-s'><? echo $this->crop_str_word($data->title, 5 ); ?></div>
            </div>
        </div>
    </div>
    </div>
</a>

    <div style='font-size:12px; color:#606060; font-weight:bold; text-align:center; padding-top:10px; height:20px; line-height:20px; overflow:hidden;' class='home_expoitem_title open-s'><? echo $data->title; ?></div>

<div style='text-align:center; padding-top:5px; float:none;' class='expo_animal_description'>
        <div style='margin-top:5px;' class='open-s'>
            <?
            if ($data->old_price == 0)
            {
                ?>
                    <span style='font-size:15px; font-weight:bold; color:#000;'>
                    <?
                        echo $data->price." РУБ";
                    ?>
                    </span>
                <?
            }


            if ($data->old_price > 0)
            {
                ?>
                <span style='font-size:15px; color:#808080; text-decoration:line-through;'>
                    &nbsp;&nbsp;<?
                        echo $data->old_price." РУБ";
                    ?>&nbsp;&nbsp;
                </span>

                <span style='font-size:15px; font-weight:bold; margin-left:10px; color:#000;'>
                    <?
                        echo $data->price." РУБ";
                    ?>
                </span>

                <?
            }
            ?>
        </div>
    </div>

    <div style='padding-top:10px;'>
    <div class='shop_button_add' onclick='shop_list_add_item(<?=$data->id;?>,1)' style='background: #FFDB4D; color: #222;text-shadow: 0px 1px 0px #fff3c2;  height:30px; line-height:30px; font-size:12px; text-align:center; border-radius:3px; width:180px; margin:0px auto; cursor:pointer; text-transform:uppercase;'><b>В корзину</b></div>
    <div class='shop_button_process' style='background: #FFDB4D; color: #222;text-shadow: 0px 1px 0px #fff3c2;  height:30px; line-height:30px; font-size:12px; text-align:center; border-radius:3px; width:180px; margin:0px auto; cursor:pointer; text-transform:uppercase;'>&bull;&bull;&bull;</div>
    <a href='<?=Yii::app()->createUrl('shop/cart');?>' class='shop_button_ok' style='background: #F88718; color: #fff;   height:30px; line-height:30px; font-size:12px; text-align:center; border-radius:3px; width:180px; margin:0px auto; cursor:pointer; text-transform:uppercase;'>
        Оформить заказ &nbsp;&rarr;
    </a>
    </div>

</div>

<?

if ($t == 3){
    ?><div style='clear:both;'></div><?
    }

?>
