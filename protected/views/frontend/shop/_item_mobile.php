
    <?
        $bg =  'background:#E0E0E0;';
        if ($data->hasImage()) {
            $bg_url = $data->getThumbnailUrl();
            $bg =  'background:url("'.$bg_url.'") center #E0E0E0; background-size:cover;';
        }
        $marginRight='animal_left';
        //if ($t == 1){$marginRight = 'animal_left';}
        //if ($t == 3){$marginRight = 'animal_right';}
    ?>
<style>
.shop_button_add, .shop_button_process, .shop_button_ok{
    cursor:pointer; width:340px; height: 50px; background: #FFDB4D; border:1px solid rgba(0,0,0,0.2); border-radius: 4px;line-height: 50px;margin: 0px auto;text-align: center;font-size: 16px;color: #222; text-shadow: 0px 1px 0px #fff3c2;
}
.shop_button_add:hover, .shop_button_process:hover{
 background: #FFD429;
}

.shop_button_process, .shop_button_ok{
display:none;
}

.shop_button_ok{
background:#F88718;
}

.shop_button_ok:hover{
background:#F27D07;
}

.animal_left{
    margin-right:17px;
}
.animal_right{
    margin-right:0px;
}
</style>
<div style='width:50%; float:left; border:none; margin:0px; padding:0px;  margin-bottom:10px; ' class='expo_animal_item' rel='<?=$data->id;?>'>
<div style='padding:5px;'>
<a href="<?=Yii::app()->createUrl('shop/item',array('id' => $data->id));?>" style='display:block;' class=''>
    <div class='' style='border-radius:4px; width:100%; height:150px;  <?=$bg;?>'></div>

    <div style='text-align:left; padding-top:5px; height:30px; line-height:30px; overflow:hidden;' class=''>
        <div style='margin-top:5px;' class='open-s'>
            <?
            if ($data->old_price == 0)
            {
                ?>
                    <span style='font-size:15px; font-weight:bold; color:#000;'>
                    <?
                        echo $data->price." РУБ";
                    ?>
                    </span>
                <?
            }


            if ($data->old_price > 0)
            {
                ?>
                <span style='font-size:15px; color:#808080; text-decoration:line-through;'>
                    &nbsp;&nbsp;<?
                        echo $data->old_price." РУБ";
                    ?>&nbsp;&nbsp;
                </span>

                <span style='font-size:15px; font-weight:bold; margin-left:10px; color:#000;'>
                    <?
                        echo $data->price." РУБ";
                    ?>
                </span>

                <?
            }
            ?>
        </div>
    </div>

    <div style='font-size:12px; color:#606060; font-weight:bold; text-align:left; height:38px; overflow:hidden;' class=' open-s'><? echo $data->title; ?></div>
</a>



    <div style='margin-top:10px;'>
    <div class='shop_button_add' onclick='shop_list_add_item(<?=$data->id;?>,1)' style='background: #FFDB4D; color: #222;text-shadow: 0px 1px 0px #fff3c2;  height:30px; line-height:30px; font-size:12px; text-align:center; border-radius:3px; width:100%; margin:0px auto; cursor:pointer; text-transform:uppercase;'><b>В корзину</b></div>
    <div class='shop_button_process' style='background: #FFDB4D; color: #222;text-shadow: 0px 1px 0px #fff3c2;  height:30px; line-height:30px; font-size:12px; text-align:center; border-radius:3px; width:100%; margin:0px auto; cursor:pointer; text-transform:uppercase;'>&bull;&bull;&bull;</div>
    <a href='<?=Yii::app()->createUrl('shop/cart');?>' class='shop_button_ok' style='background: #F88718; color: #fff;   height:30px; line-height:30px; font-size:10px; text-align:center; border-radius:3px; width:100%; margin:0px auto; cursor:pointer; text-transform:uppercase;'>
        Оформить заказ &nbsp;&rarr;
    </a>
    </div>

    <div style='clear:both;'></div>
</div>
</div>


