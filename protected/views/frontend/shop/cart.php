<?
$detect2 = new Mobile_Detect;

$this->pageTitle =  "Корзина, магазин Сахалинский зооботанический парк.";
?>
<style>
.main_container_left{width: 730px;}
.oformit{float:right; width:344px;}
.oformit_title{font-size:22px; padding-bottom:30px;}
.animal_photo_item{width:136px; height:136px; margin-left:10px; margin-bottom:10px; margin-right:0px; border:1px solid rgba(0,0,0,.1);}
.shop_bredcrumbs{color:#A0A0A0; font-size:13px;}
.shop_bredcrumbs a:hover{color:#53952A; font-size:13px; text-decoration:underline;}
.shop_button_add, .shop_button_process, .shop_button_ok{
   cursor:pointer; width:340px; height: 50px; background: #FFDB4D; border:1px solid rgba(0,0,0,0.2); border-radius: 4px;line-height: 50px;margin: 0px auto;text-align: center;font-size: 16px;color: #222; text-shadow: 0px 1px 0px #fff3c2;
}
.shop_button_add:hover, .shop_button_process:hover{
 background: #FFD429;
}

.shop_button_process, .shop_button_ok{
display:none;
}

.shop_button_ok{
background:#F88718;
}

.shop_button_ok:hover{
background:#F27D07;
}

.bredcrumbs_block{padding-bottom:30px;}


@media screen and (max-width:800px) {
.main_container.news{width:auto;}
.main_container_left{width: auto; padding:0px 20px;}
.oformit{float:none; width:auto; padding:0px 20px; margin-top:30px;}
.oformit_title{font-size:22px; padding-bottom:10px;}
.bredcrumbs_block{padding:0px 20px; padding-bottom:15px;}
}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>

<div class='bredcrumbs_block' style=''>
    <div class='pages_view_title' style='padding:0xp; margin:0px;'>КОРЗИНА</div>
    <div class='open-s shop_bredcrumbs'>
        <a href="<?=Yii::app()->createUrl('shop/index');?>" class='shop_bredcrumbs'><i>Магазин</i></a>
            /
            <a href="<?=Yii::app()->createUrl('shop/cart');?>" class='shop_bredcrumbs'><i>Корзина</i></a>
    </div>
</div>

<div style='' class='main_container_left'>
    <div style='' class='cart_products'>
<style>
.table td{
padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;
}
.product_title{
color:#3366FF;
}

.product_title:hover{
text-decoration:underline;
}

.product_line.deleted{
text-decoration:line-through; background:#E0E0E0; color:#707070;
}

.product_line .cart_delete{
display:block;
}

.product_line .cart_redelete{
display:none;
}

.product_line.deleted .cart_delete{
display:none;
}

.product_line.deleted .cart_redelete{
display:block;
}

.product_line.deleted .product_title{
color:#808080;
}

.product_line.deleted input{
background:#EAEAEA; color:#808080;  border:1px solid #ccc;
}

.shop_order_input{
    display:none;
}

</style>

<?
if ($detect2->isMobile() == false)
{
?>
<div style='font-size:22px; padding-bottom:30px;' class='open-s'><i>Список товаров</i></div>
<table style='width:100%; font-size:14px; border-collapse:collapse;' class='table' cellpadding=0 cellspacing=0>

<?
$summ = 0;
$quontity=0;

if ($items)
{
foreach ($items as $product){
    ?>
    <tr style='' class='product_line <? if ($product->deleted == 1){echo "deleted";} ?>' rel='<?=$product->id;?>'>
        <td style='vertical-align:middle; padding:0px;'>
            <a href="<?= Yii::app()->createUrl('shop/item',array('id'=>$product->id)); ?>" style='' class='product_title'>
                <?=$product->title;?>
            </a>
        </td>
        <td style='width:100px;  vertical-align:middle; text-align:center;'>
            <?=$product->price;?>
            <input type="hidden" value='<?=$product->price;?>' class='cart_price'/>
            <span style='font-size:11px;'>&nbsp;РУБ.</span>
        </td>
        <td style='width:150px; vertical-align:middle; text-align:center;'>
            <?
            $count = 0;
                    if (isset($cart_array[$product->id])){
                        $count = $cart_array[$product->id];
                    }
                    $quontity = $quontity + $count;
            ?>
            <input type="text" value='<?=$count;?>' class='cart_quontity' style='width:40px; text-align:center; font-size:13px;'/>
            <span style='font-size:11px;'>&nbsp;шт.</span>
        </td>
        <td style='width:100px;  vertical-align:middle; text-align:center;'>
            <span class='item_sum'>
            <?
            $curent_summ =  $count*$product->price;
            $summ =  $summ+$curent_summ;
                    echo $curent_summ;
            ?>    <span style='font-size:11px;'>&nbsp;РУБ.</span>
            </span>

        </td>

        <td style='width:20px; cursor:pointer; text-align:right; vertical-align:middle; '>
            <?
            if ($product->deleted == 0)
            {
                ?>
                <div style='' onclick='cart_delete(<?=$product->id;?>)' class='cart_delete'><b>X</b></div>
                <?
            }
            ?>
        </td>
    </tr>
    <?
}
}
?>

    <tr style='padding-top:10px; padding-bottom:10px; font-size:14px;'>
        <td style='border:none;' colspan=2>&nbsp;</td>
        <td style='border:none; text-align:center;' class='total_count'><b><?=$quontity;?> шт.</b></td>
        <td style='border:none; text-align:center;' class='total_sum'><b><?=$summ;?> РУБ.</b></td>
        <td style='border:none;'>&nbsp;</td>
    </tr>
</table>
<?
}else{

$summ = 0;
$quontity=0;

if ($items)
{
foreach ($items as $product){
    $count = 0;
    if (isset($cart_array[$product->id])){
        $count = $cart_array[$product->id];
    }
    $quontity = $quontity + $count;
    ?>
    <div style='width:100%; border:1px solid #ccc; background:#fff; border-radius:5px; margin-bottom:10px;' class='product_line' rel='<?=$product->id;?>'>
        <div style='width:20px;padding-right:10px; cursor:pointer; float:right;'>
                <div style='height:40px; line-height:40px; text-align:right; ' onclick='cart_delete(<?=$product->id;?>)' class='cart_delete'><b>X</b></div>
        </div>
        <div style=''>
            <div style='padding:10px;'>
            <a href="<?= Yii::app()->createUrl('shop/item',array('id'=>$product->id)); ?>" style='' class='product_title'>
                <?=$product->title;?>
            </a>
            </div>
        </div>

        <div style='clear:both;'></div>

        <div style='border-top:1px solid #ccc;'>

            <input type="hidden" value='<?=$product->price;?>' class='cart_price'/>

            <div style='width:150px; float:left; text-align:left;'>
                <div style='padding:10px;'>
                <span class='item_sum'>
                <?
                $curent_summ =  $count*$product->price;
                $summ =  $summ+$curent_summ;
                        echo $curent_summ;
                ?>    <span style='font-size:11px;'>&nbsp;РУБ.</span>
                </span>
                </div>
            </div>

            <div style='width:130px; float:right; text-align:right;'>
                <div style='padding:10px;'>
                    <?

                    ?>
                    <input type="text" value='<?=$count;?>' class='cart_quontity' style='width:70px; height:20px; line-height:20px; text-align:center; font-size:13px;'/>
                    <span style='font-size:11px;'>&nbsp;шт.</span>
                </div>
            </div>

            <div style='clear:both;'></div>
        </div>

    </div>
    <?
}
}
?>

    <div style='width:100%; border:1px solid #ccc; background:#fff; border-radius:5px;'>
        <div style='padding:10px; font-size:15px;'>
            <div style='width:140px; float:left; ' class='total_sum'><b><?=$summ;?> РУБ.</b></div>
            <div style='width:140px; float:right; text-align:right;' class='total_count'><b><?=$quontity;?> шт.</b></div>
            <div style='clear:both;'></div>
        </div>
    </div>
<?
}
?>
    </div>
</div>


<div style='' class='oformit'>
    <div style='' class='oformit_title open-s'><i>Оформить заказ</i></div>
    <?
        $this->renderPartial('_cart_form', array());
    ?>
</div>

<div style='clear:both;'></div>

</div>
</div>


<?
     $this->renderPartial('_cart', array());
?>

<script>
$(function(){
    $('.cart_quontity').on('keyup',function(){
        cart_update();
    })
})

function show_form(){
    $('.cart_products').css('display','none');
    $('.cart_form').css('display','block');
}
</script>