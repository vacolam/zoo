<?
$detect2 = new Mobile_Detect;
$cat_name = '';

if ($cats){
$cat_name = " ".$cats->title;
}
$this->pageTitle =  $model->title.", ".$cat_name.". Сахалинский зооботанический парк.";
?>
<style>
.price_block{margin-top:25px;}
.price_text{ padding:8px 14px; font-size:16px; font-weight: bold;}
.more_photos_block{margin-top:15px;}
.main_container_left{width: 730px;}
.shop_bredcrumbs{color:#A0A0A0; font-size:13px;}
.shop_bredcrumbs a:hover{color:#53952A; font-size:13px; text-decoration:underline;}
.shop_button_add, .shop_button_process, .shop_button_ok{
   cursor:pointer; width:340px; height: 50px; background: #FFDB4D; border:1px solid rgba(0,0,0,0.2); border-radius: 4px;line-height: 50px;margin: 0px auto;text-align: center;font-size: 16px;color: #222; text-shadow: 0px 1px 0px #fff3c2;
}
.shop_button_add:hover, .shop_button_process:hover{
 background: #FFD429;
}

.shop_button_process, .shop_button_ok{
display:none;
}

.shop_button_ok{
background:#F88718;
}

.shop_button_ok:hover{
background:#F27D07;
}

.animal_photo_block{
width:730px;
}

.animal_photo_block_img{
width:730px;  border-radius:8px; border:1px solid rgba(0,0,0,.1);
}

.main_container_right{
    float:right; width:400px;
}
.bredcrumbs_block{
 padding-bottom:30px;
}

.add_button_block{
float:right; width:340px;
}

@media screen and (max-width:800px) {
.main_container.news{width:auto;}
.main_container_right{float:none; width:auto; margin:0px; padding:0px 20px; padding-top:10px; }
.documents_socials{display:none;}
.expo_animal_type{display:none;}
.news_view_title{padding-top:10px; margin:0px; font-size:20px;}

.bredcrumbs_block{padding:0px 20px; padding-bottom:20px;}
.main_container_left{padding:0px; margin:0px; padding:0px 20px; width:auto; min-height:auto;}
.animal_photo_block{width:100%;}
.animal_photo_block_img{width:100%;  border-radius:8px; border:1px solid rgba(0,0,0,.1);}
.shop_quontity{display:none;}
.add_button_block{float:none; width:100%;}
.shop_button_add, .shop_button_process, .shop_button_ok{cursor:pointer; width:100%; height:40px; background: #FFDB4D; border:1px solid rgba(0,0,0,0.2); border-radius: 4px;line-height: 40px;margin: 0px auto;text-align: center;font-size: 14px;color: #222; text-shadow: 0px 1px 0px #fff3c2;}
.shop_button_ok{background:#F88718;}
.more_photos_block{margin-top:5px;}
.price_block{margin-top:10px;}
.price_text{ padding:4px 10px; font-size:12px; font-weight: bold;}

}
</style>
<div style='' class='C-01'>
<div style='' class='C-02'>

<div style='' class='bredcrumbs_block'>
    <div class='pages_view_title' style='padding:0xp; margin:0px;'>МАГАЗИН</div>
    <div class='open-s shop_bredcrumbs'>
        <a href="<?=Yii::app()->createUrl('shop/index');?>" class='shop_bredcrumbs'><i>Магазин</i></a>

        <?
        if ($active_cat){
            if ($active_cat->id > 0){
            ?>
            /
            <a href="<?=Yii::app()->createUrl('shop/index',array('cats_id'=>$active_cat->id));?>" class='shop_bredcrumbs'><i><?=$active_cat->title;?></i></a>
            <?
            }
        }
        ?>
        <?
        if ($active_subcat){
            if ($active_subcat->id > 0){
            ?>
            /
            <a href="<?=Yii::app()->createUrl('shop/index',array('cats_id'=>$active_subcat->id));?>" class='shop_bredcrumbs'><i><?=$active_subcat->title;?></i></a>
            <?
            }
        }
        ?>
    </div>
</div>

<div style='' class='main_container_left'>
<div style='' >



<?
$r ='';
if ($model->shopcats)
{
$r = $model->shopcats->title;
}

?>


    <?
         $bg =  "background:#53952A; ";
         $bg_url = '';
         if ($model->hasImage())
         {
            $bg_url = $model->getImageUrl();
            $bg =  "";//'background:url("'.$bg_url.'") #53952A center; background-size:cover;';
         }
         ?>


            <div style='' class='animal_photo_block'>
                    <img src="<?=$bg_url;?>" alt="" class='animal_photo_block_img' style=''/>
            </div>

                            <?
                            $photos = $model->photos;
                            if (count($photos) > 1)
                            {
                                ?>
                                <div style='' class='more_photos_block'>
                                <?
                                $t=0;
                                $last_image = '';
                                foreach ($photos as $photo)
                                {
                                    if ($photo->id == $model->cover_id){continue;}
                                    $t++;
                                    $display = '';
                                    if ($t > 4){$display = 'display:none;';}
                                    if ($t == 5){
                                        $last_image = "<a href='".$photo->getImageUrl()."' class='animal_photos_more fancybox-media' rel='photos' style=''>&bull;&bull;&bull;</a>";
                                    }
                                    ?>
                                    <a href="<?=$photo->getImageUrl();?>" class='fancybox-media animal_photo_item' rel='photos' style='  background:url(<?=$photo->getThumbnailUrl();?>) #53952A center; background-size:cover; <?=$display;?>'></a>
                                    <?
                                }
                                echo $last_image;
                                ?>
                                <div style='clear:both;'></div>
                                </div>
                                <?
                            }
                            ?>


<div class='documents_socials'>
<?
$docs = $model->docs;
$this->renderPartial('//layouts/_documents', array('docs'=>$docs));

$this->renderPartial('//layouts/_socials', array('margin_top'=>'40'));
?>
</div>

<div style='clear:both;'></div>
</div>





<div style='clear:both;'></div>


</div>


<div style='' class='main_container_right'>
<?
if ($r != ''){
    echo "<div class='expo_animal_type' style='color:#53952A;'><b>".$r."</b></div>";
}
?>
<h2 class='news_view_title'><?= $model->title ?></h2>

<div style='' class='price_block'>
<?
                                if ($model->old_price == 0)
                                {
                                    ?>
                                        <span class='price_text' style="background:#53952A; color:#fff; border-radius:3px; ">
                                        <?
                                            echo $model->price." РУБ";
                                        ?>
                                        </span>
                                    <?
                                }


                                if ($model->old_price > 0)
                                {
                                    ?>
                                    <span class='price_text' style='background:#E7E7E7; color:#222; border-radius:3px;  text-decoration:line-through; font-weight:normal;'>
                                        &nbsp;&nbsp;<?
                                            echo $model->old_price." РУБ";
                                        ?>&nbsp;&nbsp;
                                    </span>

                                    <span class='price_text' style='background:#F88718; color:#fff;  border-radius:3px;  margin-left:10px;'>
                                        <?
                                            echo $model->price." РУБ";
                                        ?>
                                    </span>

                                    <?
                                }
                                ?>



</div>



<div style='padding-top:30px;'>
<input type="text" class='shop_quontity' name='shop_quontity' value='1' style='height:50px; width:50px; border-radius:3px; border:1px solid #ccc; text-align:center; line-height:50px; font-size:15px; float:left;'/>

<div style='' class='add_button_block'>
    <div onclick='shop_add_item(<?=$model->id;?>)' class='shop_button_add' style=''>Положить в корзину</div>
    <div class='shop_button_process' style=''>&bull;&bull;&bull;</div>
    <a href='<?=Yii::app()->createUrl('shop/cart');?>' class='shop_button_ok' style=' border:1px solid rgba(0,0,0,.1); text-align:center;'>
        <ul style='list-style:none; margin:0px;padding:0px; display:inline-block;'>
        <li style='height:40px; width:40px; margin-top:5px; background:url(css_tool/added.png) center; float:left;'></li>
        <li style='color:#fff; text-shadow:none; height:50px; line-height:50px; float:left;'>Оформить заказ &nbsp;&rarr;</li>
        <li style='clear:both;'></li>
        </ul>
    </a>

    <div style='padding-top:5px; font-size:14px; text-align:center;'>
    <i><?
    $title = '';
    if ($model->quontity >= 1000){$title = 'В наличии: много'; }
    if ($model->quontity < 1000 and $model->quontity >= 500 ){$title = 'В наличии более 500'; }
    if ($model->quontity < 500 and $model->quontity >= 100 ){$title = 'В наличии более 100'; }
    if ($model->quontity < 100 and $model->quontity >= 50 ){$title = 'В наличии более 50'; }
    if ($model->quontity < 50 and $model->quontity >= 10  ){$title = 'В наличии более 10'; }
    if ($model->quontity < 10){$title = 'В наличии меньше 10'; }

    echo $title;
    ?></i>

    </div>

</div>

    <div style='clear:both;'></div>
</div>

<?
if ($model->text != '')
{
    ?>
    <div style='' class='animal_description redactor-styles'>
        <?
           echo $model->text;
        ?>
    </div>
    <?
}
?>

</div>

<div style='clear:both;'></div>

</div>
</div>



<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>


    <?
     $this->renderPartial('_cart', array());
    ?>