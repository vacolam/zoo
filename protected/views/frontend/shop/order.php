<meta name="robots" content="noindex, nofollow" />
<?
$detect2 = new Mobile_Detect;
?>
<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='padding-bottom:200px; float:none;' class='main_container'>
<?
$status_text = array(
        0 => 'Принят',
        1 => 'Готов к выдаче',
        2 => 'Частично готов к выдаче',
        1000 => 'Оплачен и выдан',
        10000 => 'Отменен продавцом',
        10001 => 'Отменен покупателем',
    );
    $status = array
    (
        0 => 'bg-warning',
        1 => 'bg-primary',
        2 => 'bg-primary',
        1000 => 'bg-success',
        10000 => 'bg-danger',
        10001 => 'bg-danger',
    );

    $status_class = 'bg-primary';
    if (isset($status[$data->status]))
    {
        $status_class = $status[$data->status];
    }

    $status_title = '';
    if (isset($status_text[$data->status]))
    {
        $status_title = $status_text[$data->status];
    }
?>

<div style='font-size:22px; padding-bottom:0px;'>
    <span style='background:#53952A; color:#fff; padding:8px 15px; border-radius:4px;'><b>ЗАКАЗ № <?=$data->id;?></b></span>
</div>

<?
if ($data->status == 0){
?>
<div style='margin-top:20px; margin-bottom:20px;  background:#F0F0F0; border:1px solid #ccc; border-radius:4px;'>
    <div style='padding:10px;'>
        <?
            echo "Ваш заказ <b>ПРИНЯТ</b>! Если понадобится уточнение Вашего заказа, с Вами свяжется менеджер. Если в заказе все в порядке, Вам придет СМС, когда заказ будет готов.";
        ?>
    </div>
</div>
<?
}
?>

<style>
.table td{
padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;
}
.product_title{
color:#3366FF;
}

.product_title:hover{
text-decoration:underline;
}

.product_line.deleted{
text-decoration:line-through; background:#E0E0E0; color:#707070;
}

.product_line .cart_delete{
display:block;
}

.product_line .cart_redelete{
display:none;
}

.product_line.deleted .cart_delete{
display:none;
}

.product_line.deleted .cart_redelete{
display:block;
}

.product_line.deleted .product_title{
color:#808080;
}

.product_line.deleted input{
background:#EAEAEA; color:#808080;  border:1px solid #ccc;
}

.shop_order_input{
    display:none;
}

</style>
<?
if ($detect2->isMobile() == false)
{
?>
<table style='width:100%; font-size:14px; border-collapse:collapse;' class='table'>
<tr>
    <td style='border:none;'>&nbsp;</td>
    <td style='border:none;'>Цена</td>
    <td style='border:none;'>Кол-во</td>
    <td style='border:none;'>Сумма</td>
    <td style='border:none;'>&nbsp;</td>
</tr>
<?
$summ = 0;
$quontity=0;
$items = $data->orderitems;

if ($items)
{
foreach ($items as $order_item){
    $product = $order_item->product;
    ?>
    <tr style='' class='product_line <? if ($order_item->deleted == 1 OR $product->deleted == 1){echo "deleted";} ?>' rel='<?=$order_item->id;?>'>
        <td style='vertical-align:middle;  padding:0px;'>
            <a href="<?= Yii::app()->createUrl('shopitem/update',array('id'=>$product->id)); ?>" style='' class='product_title'>
                <?=$product->title;?>
                <? if ($product->deleted == 1){echo " (Товар удален)";}?>
            </a>
        </td>
        <td style='width:130px;  vertical-align:middle;'>
            <?=$order_item->price;?>
            <span style='font-size:11px;'>&nbsp;РУБ.</span>
        </td>
        <td style='width:130px;  vertical-align:middle;'>
            <?
            $count = $order_item->quontity;
            if ($order_item->deleted != 1)
            {
            $quontity = $quontity + $count;
            }
            ?>
            <?=$count;?>
            <span style='font-size:11px;'>&nbsp;шт.</span>
        </td>
        <td style='width:120px;  vertical-align:middle;'>
            <span class='cart_sum_text'>
            <?
            $curent_summ =  $count*$order_item->price;
            if ($order_item->deleted != 1)
            {
            $summ =  $summ+$curent_summ;
            }
            echo $curent_summ;
            ?>
            </span>
            <span style='font-size:11px;'>&nbsp;РУБ.</span>
        </td>

    </tr>
    <?
}
}
?>

    <tr style='padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc; font-size:14px;'>
        <td style='' colspan=2>&nbsp;</td>
        <td style='width:30px;' class='total_count'><b><?=$quontity;?> шт.</b></td>
        <td style='width:30px;' class='total_sum'><b><?=$summ;?> РУБ.</b></td>
    </tr>
</table>
<?
}else{
$summ = 0;
$quontity=0;
$items = $data->orderitems;
if ($items)
{
foreach ($items as $order_item)
{
    $product = $order_item->product;
    $count = 0;
    $count = $order_item->quontity;

    if ($order_item->deleted != 1)
    {
    $quontity = $quontity + $count;
    }
    ?>
    <div style='width:100%; border:1px solid #ccc; background:#fff; border-radius:5px; margin-bottom:10px;' class='product_line <? if ($order_item->deleted == 1 OR $product->deleted == 1){echo "deleted";} ?>' rel='<?=$product->id;?>'>
        <div style=''>
            <div style='padding:10px;'>
            <a href="<?= Yii::app()->createUrl('shop/item',array('id'=>$product->id)); ?>" style='' class='product_title'>
                <?=$product->title;?>
            </a>
            </div>
        </div>

        <div style='clear:both;'></div>

        <div style='border-top:1px solid #ccc;'>

            <input type="hidden" value='<?=$order_item->price;?>' class='cart_price'/>

            <div style='width:150px; float:left; text-align:left;'>
                <div style='padding:10px;'>
                <span class='item_sum'>
                <?
                $curent_summ =  $count*$order_item->price;
                if ($order_item->deleted != 1)
                {
                $summ =  $summ+$curent_summ;
                }
                echo $curent_summ;
                ?>
                <span style='font-size:11px;'>&nbsp;РУБ.</span>
                </span>
                </div>
            </div>

            <div style='width:130px; float:right; text-align:right;'>
                <div style='padding:10px;'>
                    <?=$count;?>
                    <span style='font-size:11px;'>&nbsp;шт.</span>
                </div>
            </div>

            <div style='clear:both;'></div>
        </div>

    </div>
    <?
}
}
?>

    <div style='width:100%; border:1px solid #ccc; background:#fff; border-radius:5px;'>
        <div style='padding:10px; font-size:15px;'>
            <div style='width:140px; float:left; ' class='total_sum'><b><?=$summ;?> РУБ.</b></div>
            <div style='width:140px; float:right; text-align:right;' class='total_count'><b><?=$quontity;?> шт.</b></div>
            <div style='clear:both;'></div>
        </div>
    </div>
<?
}
?>

<table style='width:100%; font-size:14px; border-collapse:collapse;'>
                                <tr>
                                    <td style='width:100px; text-align:left; padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;' valign=top>
                                        <b>Статус:</b>
                                    </td>
                                    <td style='padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'>
                                    <?
                                        foreach($status_text as $index => $text)
                                        {
                                            $status_class = 'bg-primary';
                                            if (isset($status[$index]))
                                            {
                                                $status_class = $status[$index];
                                            }
                                            ?>
                                            <div class="form-check <? if ($index == $data->status){echo "checked";} ?>" style='margin-bottom:6px;'>
                                                <input class="form-check-input" type="radio" name="ShopOrders[status]" id="exampleRadios_<?=$index;?>" value="<?=$index;?>" <? if ($index == $data->status){echo "checked";} ?>>
                                                <label class="form-check-label" for="exampleRadios_<?=$index;?>">
                                                    <span class="badge <?=$status_class;?>" style='font-size:16px; '><?=$text;?></span>
                                                </label>
                                            </div>
                                            <?
                                        }
                                    ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:100px; text-align:left; padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                        <b>Имя:</b>
                                    </td>
                                    <td style='padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                        <div class='shop_order_text'><i><?=$data->name;?></i></div>
                                   </td>
                                </tr>

                                <tr>
                                    <td style='width:100px; text-align:left;  padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                        <b>Телефон:</b>
                                    </td>
                                    <td style=' padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                        <div class='shop_order_text'><i><? echo mb_substr($data->phone,0,3); ?>*-***-*<? echo mb_substr($data->phone,8,3); ?></i></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td style='width:100px; text-align:left; padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;' >

                                    </td>
                                    <td style=' padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'>
                                        <div style='text-transform:uppercase; font-weight:bold; ' class='shop_order_text'>
                                          <?
                                           $delivery_type = array(
                                            1 => 'Самовывоз из Зоопарка',
                                            2 => 'Доставка курьером',
                                           );

                                           if (isset($delivery_type[$data->deliverytype]))
                                           {
                                               echo $delivery_type[$data->deliverytype];
                                           }
                                           ?>
                                        </div>
                                    </td>
                                </tr>

                                <?
                                if ($data->adress != '')
                                {
                                ?>
                                <tr>
                                   <td style='width:100px; text-align:left; padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                        <b>Адрес:</b>
                                   </td>
                                   <td style=' padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;'  valign=top>
                                   <div class='shop_order_text'><i><? echo mb_substr($data->adress,0,10); ?>****</i></div>
                                   </td>
                                </tr>
                                <?
                                }
                                ?>

                                <?
                                if ($data->comment != '')
                                {
                                ?>
                                <tr>

                                   <td style=' padding-top:10px; padding-bottom:10px; border-bottom:1px dotted #ccc;' colspan=2  valign=top>
                                   <div style='padding-bottom:3px;'><b>Комментарий:</b></div>
                                   <div class='shop_order_text'><i><? echo $data->comment; ?></i></div>
                                   </td>
                                </tr>
                                 <?
                                }
                                ?>
                            </table>

</div>
</div>
</div>




