<?
$this->pageTitle =  "Карта сайта. Сахалинский зооботанический парк.";
?>

<div style='' class='C-01'>
<div style='' class='C-02'>
<div style='' class='main_container_left'>
<div style='padding-bottom:20px;' class='pages_view_title'>Карта сайта</div>
<ul style='margin:0px; padding:0px; margin-left:20px; margin-top:-20px;'>
<?

foreach($this->menu as $item){

    if (isset($item['label']))
    {
        ?>
        <li style='margin:0px; padding:0px; padding-top:20px;'><a href="<?=$item['url'];?>"><?=$item['label'];?></a></li>
        <?

        if (in_array($item['id'],array(3,2,5)) == false)
        {
            if (isset($item['submenu']))
            {
                if (count($item['submenu']) > 0)
                {
                    ?>
                    <ul style='margin-left:40px;'>
                        <?
                        foreach($item['submenu'] as $sub)
                        {
                            ?>
                            <li><a href="<?=$sub['link'];?>"><?=$sub['name'];?></a></li>
                            <?
                        }
                        ?>
                    </ul>
                    <?

                }
            }
        }

        //ПРОЕКТЫ
        if ($item['id'] == 3)
        {

            ?>
            <ul style='margin-left:40px;'>
                        <?
                        foreach ($typeList as $conf_index => $conf_item)
                        {
                            ?>
                            <li><a href="<? echo Yii::app()->createUrl('conf/index',array('type'=>$conf_index)); ?>"><?=$conf_item;?></a></li>
                            <?
                        }
                        ?>
            </ul>
            <?
        }

        //Афиша
        if ($item['id'] == 2)
        {
            ?>
            <ul style='margin-left:40px;'>
                        <?
                        foreach ($afisha_tags as $tag)
                        {
                            ?>
                            <li><a href="<? echo Yii::app()->createUrl('afisha/index',array('tag'=>$tag['id'])); ?>"><?=$tag['name'];?></a></li>
                            <?
                        }
                        ?>
            </ul>
            <?
        }


        //Экспозици
        if ($item['id'] == 5)
        {
            ?>
            <ul style='margin-left:40px;'>
                        <?
                        foreach ($expo as $expo_item)
                        {
                            ?>
                            <li><a href="<?=Yii::app()->createUrl('exposure/view',array('id' => $expo_item->id));?>"><?=$expo_item->title;?></a></li>
                            <?
                        }
                        ?>
            </ul>
            <?
        }

    }


}
?>
</ul>
</div>

<div style='width: 300px; margin-top:0px; float:right;' class='main_right'>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

    <div style='margin-top:50px;'>
        <div style='padding-bottom:5px; text-transform:uppercase; font-size:16px; '><b>Партнеры</b></div>
        <?
        $this->renderPartial('//layouts/_partners', array());
        ?>
    </div>
</div>
<div style='clear:both;'></div>

</div>
</div>
