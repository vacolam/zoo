    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="css_tool/jquery.min.js"></script>
    <script type="text/javascript" src="css_tool/jquery-ui.js"></script>
<style>
    html,body{
        font-family:Open Sans, Verdana, Tahoma, Arial; font-weight:400;
    }
    .open-s{font-family:Open Sans, Verdana, Tahoma, Arial; font-weight:400;
    }
    a, a:hover{
        text-decoration:none;
    }

    </style>

<div style='width: 900px; margin: 0px auto; margin-top:150px;' class='open-s'>
    <div><b>Дизайн сайта</b></div>
    <ul>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=1" target='_blank'>1. Главная</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=2" target='_blank'>2. Новости</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=3" target='_blank'>3. Новости. Внутренняя.</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=4" target='_blank'>4. Стандартная текстовая страница</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=5" target='_blank'>5. Документы</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=6" target='_blank'>6. Ярмарка</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=7" target='_blank'>7. Ярмарка. Заявка.</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=8" target='_blank'>8. Оценка качества услуг.</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=9" target='_blank'>9. Оценка качества услуг. Форма оценки.</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=10" target='_blank'>10. Наши партнеры</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=11" target='_blank'>11. Опека над животными</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=13" target='_blank'>13. Конференция. Внутренняя страница</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=14" target='_blank'>14. Конференция. Внутренняя страница. Заявка</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=15" target='_blank'>15. Викторина</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=18" target='_blank'>18. Афиша</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=19" target='_blank'>19. Афиша. Внутренняя страница</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=20" target='_blank'>20. Афиша. Внутренняя страница с заявкой на группу</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=22" target='_blank'>22. Экспозии</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=23" target='_blank'>23. Страница экспозиции</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=24" target='_blank'>24. Страница животного</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=25" target='_blank'>25. Контакты</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=26" target='_blank'>26. Поиск</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&all=all&id=27" target='_blank'>27. Английская версия</a></li>
    </ul>
    <br><br><br><br><br><br><br><br><br><br><br>

    <div><b>Оформление внутренней страницы</b></div>
    <ul>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-11-1" target='_blank'>Вариант 1</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-11-2" target='_blank'>Вариант 2</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-11-3" target='_blank'>Вариант 3</a></li>
    </ul>
    <br><br>
    <div><b>Этап 1. Правки 22 апреля 2020.</b></div>
    <ul>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-etap-1-1" target='_blank'>Главная</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-etap-1-4" target='_blank'>Главная - Экспозиции под новостями</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-etap-1-2" target='_blank'>Новости</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-etap-1-3" target='_blank'>Афиша</a></li>
    </ul>
    <br><br>
    ------------------
    <br><br><br>
    <div><b>Новости</b></div>
    <ul>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-news-1" target='_blank'>Новости 1 - Списком</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-news-2" target='_blank'>Новости 2 - Одинаковые блоки</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-news-3" target='_blank'>Новости 3 - Как на главной</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-news-4" target='_blank'>Новости. Внутренняя страница</a></li>
    </ul>
    <br><br>

    <div><b>Афиша</b></div>
    <ul>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-afisha-1" target='_blank'>Афиша 1 - Списком</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-afisha-2" target='_blank'>Афиша 2 - Одинаковые блоки</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-afisha-3" target='_blank'>Афиша 3 - Меню справа</a></li>
    </ul>
    <br><br>
    ------------------
    <br><br><br><br>


    <div><b>Концепция 3. Доработанный</b></div>
    <ul>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-10.1" target='_blank'>Вариант 4.1 - Доработанный вариант.</a></li>
    </ul>

    <div><b>Концепция 3</b></div>
    <ul>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-9.2" target='_blank'>Вариант 3.1 - Новости. Потом Экспозиции.</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-9.10" target='_blank'>Вариант 3.2 - Экспозиции. Потом Новости</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-9.9" target='_blank'>Вариант 3.3 - Другой цвет расписания</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-9.18" target='_blank'>Вариант 3.5 - Цветное оформление</a></li>
    </ul>
    <br><br>
    ------------------
    <br><br><br><br>
    <div><b>Концепция 1</b></div>
    <ul>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-1.1" target='_blank'>Вариант 1.1 - Блок "Чем заняться" в виде текста</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-1.2" target='_blank'>Вариант 1.2 - Блок "Чем заняться" в виде разноцветных квадратов</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-1.3" target='_blank'>Вариант 1.3 - Другое оформление центрального баннера</a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-1.4" target='_blank'>Вариант 1.4 - Блок "Чем заняться" в виде БЕЛЫХ квадратов + Немного другое расписание</a></li>
        <li><a href="index.php?r=test/view&id=v-1.5" target='_blank'>Вариант 1.5 - Цветное оформление расписания</a></li>
    </ul>
    <br><br>
    <div><b>Концепция 2</b></div>
    <ul>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-2.1" target='_blank'>Вариант 2.1  </a></li>
        <li style='padding-bottom:10px;'><a href="index.php?r=test/view&id=v-2.2" target='_blank'>Вариант 2.2 - Чем заняться в виде фотографий + Новости Текстом + Экспозиции сбоку</a></li>
        <li><a href="index.php?r=test/view&id=v-2.3" target='_blank'>Вариант 2.3 - Тонкое меню</a></li>
    </ul>


</div>