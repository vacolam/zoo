
    <?
        $bg =  "background:none; ";
        if ($data->hasImage()) {
            $bg_url = $data->getThumbnailUrl();
            $bg =  'background:url("'.$bg_url.'") #f0f0f0 center; background-size:cover;';
        }

        $marginRight = ' margin-right:10px;';
        if ($t == 4){$marginRight = 'margin-right:0px;';}
    ?>
<a href="<?=Yii::app()->createUrl('exposure/view',array('id' => $data->id));?>" style='display:block; <?=$marginRight;?> margin-bottom:10px; width:205px; height:300px; border-radius:4px; float:left;   <?=$bg;?>'>
    <div style='position:relative; width:100%; height:100%; background:rgba(0,0,0,.1); border-radius:4px;'>
        <div style='width:100%; position:absolute; left:0px; top:0px;'>
            <div style='padding:20px;'>
                <div style='font-size:18px; color:#fff; font-weight:500; word-wrap:break-word;' class='open-s'><?=$data->title;?></div>
            </div>
        </div>
    </div>
</a>
<?
if ($t == 4){?><div style='clear:both;'></div><?}
?>
