    <div style='padding-bottom:30px; margin-bottom:30px; border-bottom:1px dotted #ccc;'>
                    <div style='display:block; padding:20px;'>
                        <div style='width:250px; height:150px; float:left; background: url(<?= $data->getImageUrl(); ?>) #F8F8F8; background-size:cover; border-radius:5px;'>
                        </div>
                        <div style='width: 500px; float:left; padding-left:40px; '>
                             <div style='font-size:28px; text-transform:uppercase; margin-top:-5px;' class='open-s'><?=$data->title;?></div>

                            <div style='font-size:16px; line-height:1.5em; padding-top:20px; color:#909090;' class='open-s'><?=$data->text;?></div>


                            <?
                            $photos = $data->photos;
                            if (count($photos) > 1)
                            {
                                ?>
                                <div style='margin-top:40px;'>
                                    <?
                                foreach ($photos as $photo)
                                {
                                    if ($photo->id == $data->cover_id){continue;}
                                    ?>
                                    <a href="<?=$photo->getImageUrl();?>" class='fancybox-media' rel='photos' style='width:115px; height:80px; display:block; float:left; margin-right:2px; margin-bottom:2px; background:url(<?=$photo->getThumbnailUrl();?>); background-size:cover;'></a>
                                    <?
                                }
                                ?>
                                </div>
                                <?
                            }
                            ?>
                            <div style='clear:both;'></div>
                            <?
                            $docs = $data->docs;
                            if (count($docs) > 0)
                            {
                                ?>
                                <div style='margin-top:40px;'>
                                    <?
                                    foreach ($docs as $doc)
                                    {
                                        ?>
                                        <a href="<?=$doc->getDocUrl();?>" target='_blank' style=''><?=$doc->filetitle;?></a><br>
                                        <?
                                    }
                                    ?>
                                    </div>
                                    <?

                            }
                            ?>
                            <div style='clear:both;'></div>
                        </div>
                        <div style='clear:both;'></div>
                    </div>
    </div>

<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>