<?
$exposure_name = '';

if ($exposure){
$exposure_name = " ".$exposure->title;
}
$this->pageTitle =  $model->title.". Экспозиция".$exposure_name." сахалинского Зоопарка.";
?>


<style>
.post_text a{
    color:#3397ff; text-decoration:underline;
}
</style>
<div style='width:100%; background:rgba(253, 253, 253, 1);'>
<div style='width: 1200px; margin:0px auto;  padding-top:90px; padding-bottom:90px;'>
<div style='width: 830px; float:left;'  class='post_text'>
    <?
        $bg =  "background:none; ";
         if ($model->hasImage()) {
            $bg_url = $model->getThumbnailUrl();
            $bg =  'background:url("'.$bg_url.'") #f0f0f0 center; background-size:cover;';
        }
    ?>

    <a href="<?=Yii::app()->createUrl('exposure/aimal',array('id' => $model->id));?>" style='display:block; width:200px; height:150px; float:left; border-radius:5px; margin-right:30px; <?=$bg;?>'></a>


    <div style='width:600px; float:left; margin-top:-10px;'>

<h2 style='font-size:25px; line-height:1.2em;'><?= $model->title ?></h2>

<?
if ($exposure){
?>
<div style='padding-top:20px;'>
<a href="<?=Yii::app()->createUrl('exposure/view',array('id' => $exposure->id));?>" style='color:#53952A; font-size:18px;'><b><?=$exposure->title;?></b></a>
</div>
<?
}
?>

<div style='padding-top: 40px; '>
    <style>
    p {
        padding-bottom:35px; line-height:1.5em; font-size:16px;
    }

    </style>
    <?
    if ($model->text != ''){
        echo $model->text;
    }else{
        echo nl2br($model->short_text);
    }
    ?>

</div>

<div style='padding-top:10px; height:25px;'>
    <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="https://yastatic.net/share2/share.js"></script>
<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter,whatsapp,telegram"></div>
</div>

<?
$photos = $model->photos;
if (count($photos) > 1)
{
    ?>
    <div style='margin-top:40px;'>
        <?
    foreach ($photos as $photo)
    {
        if ($photo->id == $model->cover_id){continue;}
        ?>
        <a href="<?=$photo->getImageUrl();?>" class='fancybox-media' rel='photos' style='width:115px; height:80px; display:block; float:left; margin-right:2px; margin-bottom:2px; background:url(<?=$photo->getThumbnailUrl();?>); background-size:cover;'></a>
        <?
    }
    ?>
    </div>
    <?
}
?>
<div style='clear:both;'></div>
<?
$docs = $model->docs;
if (count($docs) > 0)
{
    ?>
    <div style='margin-top:40px;'>
        <div style='font-size:16px; font-weight:600; padding-bottom:5px;'>Документы</div>
        <?
        foreach ($docs as $doc)
        {
            ?>
            <a href="<?=$doc->getDocUrl();?>" target='_blank' style=''><?=$doc->filetitle;?></a><br>
            <?
        }
        ?>
        </div>
        <?

}
?>
<div style='clear:both;'></div>
</div>
<div style='clear:both;'></div>


</div>


<div style='width: 300px; margin-top:0px; float:right; '>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

</div>

<div style='clear:both;'></div>

</div>
</div>



<script src="/css_tool/jquery.fancybox.pack.js"></script>
<script>
$(function(){
   			(function ($, F) {
                F.transitions.resizeIn = function() {
                    var previous = F.previous,
                        current  = F.current,
                        startPos = previous.wrap.stop(true).position(),
                        endPos   = $.extend({opacity : 1}, current.pos);

                    startPos.width  = previous.wrap.width();
                    startPos.height = previous.wrap.height();

                    previous.wrap.stop(true).trigger('onReset').remove();

                    delete endPos.position;

                    current.inner.hide();

                    current.wrap.css(startPos).animate(endPos, {
                        duration : current.nextSpeed,
                        easing   : current.nextEasing,
                        step     : F.transitions.step,
                        complete : function() {
                            F._afterZoomIn();

                            current.inner.fadeIn("fast");
                        }
                    });
                };

            }(jQuery, jQuery.fancybox));
});

$(function(){
            $(".fancybox-media").fancybox({
                padding:0,
                margin   : [0,0,0,0],
    			helpers:  {
                     overlay: {
                          locked: false
                        },
                media : {},
                title:null
                }
            });
});
</script>