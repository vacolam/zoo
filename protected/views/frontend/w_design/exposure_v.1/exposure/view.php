<?
$this->pageTitle =  $model->title.". Экспозиция сахалинского Зоопарка.";
?>

<div style='width:100%; background:rgba(253, 253, 253, 1);'>
<div style='width: 1200px; margin:0px auto;  padding-top:80px; padding-bottom:90px;'>


<div style='width: 850px; padding-top:0px; float:left;'>
    <div style=''>
    <?
    $this->renderPartial('_exposure_preview', array(
    'data' => $model,
    ));
    ?>
    </div>

    <div>
        <?
            if ($animals){
                $previous_CatID = 0;
                foreach($animals as $animal)
                {
                    $current_CatID = $animal->cats_id;

                    if ($previous_CatID != $current_CatID)
                    {
                        ?>
                        <div style='clear:both;'></div>
                        <?
                        if (isset($catslist[$current_CatID]))
                        {
                            ?><div style='padding-top:30px; padding-bottom:15px; font-size:20px; text-transform:uppercase;'><?=$catslist[$current_CatID];?></div><?
                        }
                    }

                    $this->renderPartial('_animal_item', array(
                    'data' => $animal,
                    't' => 1,
                    ));

                    $previous_CatID = $current_CatID;
                }
            }
        ?>
    </div>

<div style='padding-top:50px;'>
<?



?>
</div>
</div>


<div style='width: 300px; margin-top:0px; float:right; '>
    <div style='width:300px; padding-top:0px;'>
        <?
        $this->renderPartial('//layouts/_infoblocks', array());
        ?>
    </div>

    <div style='margin-top:50px;'>
        <div style='padding-bottom:5px; text-transform:uppercase; font-size:16px; '><b>Партнеры</b></div>
        <?
        $this->renderPartial('//layouts/_partners', array());
        ?>
    </div>
</div>

<div style='clear:both;'></div>



</div>
</div>