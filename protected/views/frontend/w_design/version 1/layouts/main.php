<html>
<head>
    <title>Сахалинский Зоопарк.</title>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="css_tool/jquery.min.js"></script>
	<script type="text/javascript" src="css_tool/jquery-ui.js"></script>
    <link href="css_tool/style_10032016.css?id=2" rel="stylesheet">
<style>
    html,body{
        font-family:Open Sans, Verdana, Tahoma, Arial; font-weight:400;
    }
    .open-s{font-family:Open Sans, Verdana, Tahoma, Arial; font-weight:400;
    }
    a, a:hover{
        text-decoration:none;
    }

    </style>
	<?
	$detect = new Mobile_Detect;
	if ( $detect->isMobile() ) {
	    if( $detect->isTablet() ){
		}
		else{?>

		<?}
	}
	?>
	<script>
	var isMobile = <? if ($detect->isMobile()){echo "true";}else{echo "false";};?>;
	</script>

	<script type='text/javascript'>
	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
		}
		return "";
	}
</script>
</head>
<body style='padding:0px; margin:0px; width:100%;'>



        <?
        $this->renderPartial('//layouts/_top', array());
        ?>


<div style='width:1200px; margin:0px auto; margin-top:40px;'>
<?= $content; ?>
</div>


<div class="footer"  style='clear:both;width:100%; margin-top:30px; background:#E0E0E0; border-top:1px solid #ccc;'>
<div style='clear:both; width:1200px; margin:0px auto; padding:30px 0px;'>
<?
        $this->renderPartial('//layouts/_bottom', array());
?>
</div>
</div>


<div style='text-align:center; font-size:14px; color:#000; background:#fff; width:80px; height:30px; border:1px solid #000; box-shadow: 0px 0px 5px rgba(0,0,0,.1); line-height:30px; cursor:pointer;' class='open-s' id='scrolltotop' onclick='ScrollToTop()'>
Наверх
</div>
<script>
function scrolltotop_display()
{
    var el=$('#scrolltotop');
    if((window.pageYOffset||document.documentElement.scrollTop)>window.innerHeight)
    { if(!el.is(':visible')) { el.stop(true, true).fadeIn(); } }
    else { if(!el.is(':animated')) { el.stop(true, true).fadeOut(); }}
}
function scrolltotop_position()
{
    var el=$('#scrolltotop');
    el.css('top', window.innerHeight-100);
    el.css('left', 40); //window.innerWidth+100
    scrolltotop_display();
}
$(window).on('load', function(){
    $('#scrolltotop').css('display', 'none');
    $('#scrolltotop').css('position', 'fixed');
    scrolltotop_position();
   // $('#scrollToTop a').removeAttr('href');
});
function ScrollToTop(){
    $('html, body').animate({scrollTop:0}, 500);
}
$(window).on('scroll', scrolltotop_display);
$(window).on('resize', scrolltotop_position);
</script>


<div class="popup__overlay">
  <div class="popup">
        <div style='' class='popupTop'>
           <div style='width:48px; cursor:pointer; height:48px; position:absolute; top: -1px;right:-1px;' id='popupCloseButton' align='right' onclick='popupClose()'><img src="css_tool/cross_white.png" alt="" style='opacity:0.8;' /></div>
           <div align='left' style='font-size:18px; line-height:1.2em; color: #fff; text-shadow: 0px 1px 0px #713B87; margin-left:30px;  margin-right:170px;' id='popupTitle'></div>
           <Div style='clear:both;'></Div>
        </div>
          <div align='left' class='popupContent'>
          </div>

        <div style='height:17px; width:1000px;   margin-top: 50px; display:none;'>
        </div>
  </div>
  <div style='display:block;'><img src="css_tool/gif-load.gif" alt="" style='height:1px; width:1px; visibility:hidden;'/></div>
</div>


<div style='position:fixed; width:100%; height:100%;  z-index:9999999999;display:none; background:rgba(0,0,0,.1); ' id='mess'>
  <div style='width:100%; height:100%; position: relative;'>
      <div style='background:#5f3165; width:180px; padding: 20px 0px; border-radius:15px; position:absolute; top:50%; left:50%; margin-top:-50px; margin-left:-90px; box- shadow: 0 0 7px rgba(0,0,0,0.4);'>
	  	  <div style='width: 50px; float: left; margin-left: 20px;'>
          	<img src="css_tool/success.png" alt="" style='width:50px;' />
		  </div>
          <div class="spinner" style='float:left; width: 80px; margin-left:5px; color: #fff; font-size:14px; margin-left:2px; margin-top: 16px; '>
			Добавлено
          </div>
		  <div style='clear:both;'></div>
        </div>
  </div>
</div>


<div style='position:fixed; width:100%; height:100%;  z-index:9999999999;display:none; background:rgba(0,0,0,.1); ' id='loaderBlock'>
  <div style='width:100%; height:100%; position: relative;'>
      <div style='background:#fff; box-shadow:0px 0px 5px rgba(0,0,0,0.2); width:90px; height:90px; border-radius:15px; position:absolute; top:50%; left:50%; margin-top:-50px; margin-left:-45px; box- shadow: 0 0 7px rgba(0,0,0,0.4);'>
	  	  <div style='width:100%; height:100%; position:relative;'>
		  		<div id="loaderImage" style='position:absolute; top:50%;left:50%; margin-left:-20px; margin-top:-20px;'></div>
		  </div>
      </div>
  </div>
</div>



</body>
</html>

<!--for menu jquary-->
<!--<script type="text/javascript" src="./css_tool/stickUp.js"></script>-->
<script type="text/javascript">
function numeric(str){return /^[0-9]+z/.test(str + "z");}

function makeOrder(){
 popupClose();
 contactDialogShow();
}

function brand_info(brand_id){
	$('#loaderBlock').fadeIn(300);
	$.ajax({
			type: "GET",
			url: "index.php?r=catalog/brand&id="+brand_id,
			dataType: 'json',
			data: {
 				  },
			success: function(response){
					$('#loaderBlock').css('display','none');
					$('.popup__overlay').find('.popupContent').html(response.content);
					popupOpen();
			},
			error: function(response){
					$('#loaderBlock').css('display','none');
			}
		});
}

function orderConfirm(formId){

    var cellOrder = $('#'+formId).find('.cell').val();
    var nameOrder = $('#'+formId).find('.name').val();
    var cellPlaceholder = $('#'+formId).find('.cell').attr('rel');
    var namePlaceholder = $('#'+formId).find('.name').attr('rel');
    var error = false;
    var errorText = '';

    $('#'+formId).find('.errorText').html('&nbsp;');

    if (cellOrder == '' || numeric(cellOrder) == false || cellOrder==cellPlaceholder){
      error = true;
      errorText = 'Введите Телефон';
    }

    if (nameOrder == '' || nameOrder == namePlaceholder){
      error = true;
      if(errorText != ''){errorText = errorText + ', ';}
      errorText = errorText + 'Введите Имя';
    }

    if (error == true){
        errorText = "<span style='color: #F56D66;'><u>Ошибка</u>:</span>&nbsp;&nbsp;" + errorText;
        $('#'+formId).find('.errorText').html(errorText);
    }else{
         orderSend(formId);
    }
}

function show_loader(){
  if ($("#loaderDialog2").css('display')=='none'){
    $('#wrapper').css('width', $('#wrapper').css('width'));
    $('body').css('overflow','hidden');
    if ($("#contactDialog").css('display')!='none'){
        $("#contactDialog").css('display','none');
    }

   $("#loaderDialog2").find('.loaderCont').html('<img src="css_tool/gif-load.gif" alt="" style="float: left; margin-right:30px;"/>Ваши контакты отправляются специалисту....');
   $("#loaderDialog2").css({"display":"block"});
  }
}

function hide_loader(){
  if ($("#loaderDialog2").css('display')=='block'){
    $("#loaderDialog2").css({"display":"none"});
        $('body').css('overflow','auto');
        $('#wrapper').css('width', 'auto');
  }
}


function contactDialogShow(){
  if ($("#contactDialog").css('display')=='none'){
    $("#contactDialog").fadeIn(700);
  }
}

function contactDialogHide(){
  if ($("#contactDialog").css('display')=='block'){
    $("#contactDialog").fadeOut(700);
  }
}

function orderSend(formId){

            var session_id = Number(new Date());
            var cellOrder = $('#'+formId).find('.cell').val();
            var nameOrder = $('#'+formId).find('.name').val();

            show_loader();
            setTimeout(function(){
                JsHttpRequest.query(
                    'order.php', // backend
                    {
                      'formId': formId,
                      'cellOrder': cellOrder,
                      'nameOrder': nameOrder,
                      'session_id':session_id
                    },
                    function(result, errors) {
                      $('#'+formId).find('.cell').val($('#'+formId).find('.cell').attr('rel'));
                      $('#'+formId).find('.name').val($('#'+formId).find('.name').attr('rel'));
                      $('#'+formId).find('.errorText').html('&nbsp;');
                      $("#loaderDialog2").find('.loaderCont').html('<span style="font-size:16px;">Ваши контакты отправлены.<br /><br />Вам позвонит <b>Запорожец Иван</b>, чтобы назначить встречу.</span>');
                      setTimeout(function(){hide_loader();},3200);
                    },
                false
                );
            },2000);

}



jQuery(function($) {
 $('#contactDialogPole').click(function(event) {
    var e = event || window.event;
    if (e.target == this) {
           contactDialogHide();
    }
});

$(":input")
.focus(function(){
          if ($(this).val() == $(this).attr('rel')){$(this).val('');}
})
.blur(function(){
          if ($(this).val() == ''){$(this).val($(this).attr('rel'));}
})
.keydown(function (e) {
      if (e.keyCode == 13) {
       e.preventDefault();
      }
})
.keypress(function (e) {
      if (e.keyCode == 13) {
       e.preventDefault();
      }
})
.keyup(function(e){
      var formId = $(this).closest('form').attr('id');

      if (e.keyCode == 13) {
          e.preventDefault();
          //alert(formId);
          orderConfirm(formId);
      }
});






});




</script>
<script type="text/javascript">
var menuIsScrolling = false;
$(function(){

//cartMenu cartTitle cartCount

$('.cartMenu')
		.mouseenter(function(){
			$(this).find('.cartTitle').delay(300).stop(true).animate({'background-color':'#8EEBD9;'},400,'swing');
			$(this).find('.cartCount').delay(400).stop(true).animate({'background-color':'#8EEBD9;'},200,'swing');
		})
		.mouseleave(function(){
			if ($(this).hasClass('active')==false){
				$(this).find('.cartTitle').delay(300).stop(true).animate({'background-color':'#fff;','color':'#606060'},300,'swing');
				$(this).find('.cartCount').delay(300).stop(true).animate({'background-color':'#D1D1D1;','color':'#606060'},200,'swing');
			}
		});

var scrollDelta = 0;
var scrollPosition = $(window).scrollTop();
var stopScrolling = false;
var sectionsPosition = new Array();

//Заполняем массив положения секций значениями

/*$('.section').each(function(e){
	var sectionTop = $(this).offset().top;
	sectionsPosition.push(sectionTop);
});*/

//alert(sectionsPosition);
/*
$(window).on( 'DOMMouseScroll mousewheel', function ( event ) {
  slidePosition = 'none';
if (stopScrolling==false){
  if( event.originalEvent.detail > 0 || event.originalEvent.wheelDelta < 0 ) { //alternative options for wheelData: wheelDeltaX & wheelDeltaY
	scrollDelta = 'down';
	jQuery.each(sectionsPosition, function(n,i) {
       if ($(window).scrollTop() >= i-200 && $(window).scrollTop() < i){
         slidePosition = n;
       }
    });

	if (slidePosition != 'none'){
        stopScrolling = true;

		slidePosition = parseInt(slidePosition);
		height = sectionsPosition[slidePosition];
        $('body,html').animate({
			scrollTop: height
		}, 200, 'swing',function(){
			setTimeout(function(){stopScrolling = false;} , 200);
		});
	}

  } else {
	scrollDelta = 'up';
    jQuery.each(sectionsPosition, function(n,i) {
       if ($(window).scrollTop() > i && $(window).scrollTop() <= i+200){
         slidePosition = n;
       }
    });

	if (slidePosition != 'none'){
        stopScrolling = true;

		slidePosition = parseInt(slidePosition);
		height = sectionsPosition[slidePosition];
        $('body,html').animate({
			scrollTop: height
		}, 200, 'swing',function(){
			setTimeout(function(){stopScrolling = false;} , 200);
		});
	}

  }

}
	if (stopScrolling==true){
		event.preventDefault();
	    event.stopPropagation();
		return false;
	}
});
*/



          var windowHeight = $(window).height(); //та часть браузера в которой сайт отображается
          var documentHeight = $(document).height(); //высота самого сайта
          var scroll_count_down = 0;
          var scroll_count_up = 0;
            $(window).scroll(function(){

		});
});

function locationMenu_activate(tabID){
   $('.menuItem').removeClass('active');
   $('.menuItem[rel="'+tabID+'"]').addClass('active');
}

function locationMenu_goTo(goTo){
	menuIsScrolling = true;

	var height = $('.section[rel="'+goTo+'"]').offset().top;
     $('body,html').animate({
         scrollTop: height
    }, 1200, 'swing',function(){
    	menuIsScrolling = false;
    });

	locationMenu_activate(goTo);
}


function popupClose(){
	$('.popup__overlay').fadeOut(500,function(){
		$('body').css('overflow','auto');
	});


	//В меню убираем выделение с корзины
    if ($('.cartMenu').hasClass('active') == true ){
       $('.cartMenu').removeClass('active');

	   $('.cartMenu').find('.cartTitle').css({'background-color':'#fff;','color':'#606060'});
	   $('.cartMenu').find('.cartCount').css({'background-color':'#D1D1D1;','color':'#606060'});
    }

	urlHash('');

}

function popupOpen(){
	$('.popup__overlay').fadeIn(500);
	$('body').css('overflow','hidden');
}
</script>


<script type="text/javascript">


    function catalogTab(tabID){
       $('.subMenu_container').css('display','none');
       $('.subMenu_container[rel="'+tabID+'"]').css('display','block');

       $('.sectionCatatalog_menu .catalog_menu').removeClass('active');
       $('.sectionCatatalog_menu .catalog_menu[rel="'+tabID+'"]').addClass('active');

	   //var subcatID = $('.subMenu_container[rel="'+tabID+'"]').find('.catalog_subMenu:first').attr('rel');
	   var filterID = 0;
	   var activator = 'subcatalog';
       showCatatalogContent(tabID, filterID, activator);

    }

	function subCategoryTab(tabID){
       $('.catalog_subMenu').removeClass('active');
       $('.catalog_subMenu[rel="'+tabID+'"]').addClass('active');
	}

	var load_IsAnimating = false;
	function loadCatalogContent(subcatID, filterID){
	   var filters = new Array();
	   if (filterID != 0){
         filters.push(filterID);
	   }

	   //alert('d');
       load_IsAnimating = true;
       $('.catalogContent').css('display','none');
	   setTimeout(function(){
       		$('.catalogLoader').css('display','block');
	   },10);
	 /*  setTimeout(function(){
          $('.catalogLoader').fadeIn(520,function(){
          	load_IsAnimating = false;
          });
	   },270);
	   */

	   $.ajax({
			type: "POST",
			url: "/index.php?r=catalog/catalog&id="+subcatID,
			dataType: 'json',
			data: {
					filters: filters,
 				  },
			success: function(response){
				$('.sectionCatatalog').find('.catalogContent').html(response.content);
				$('.catalogContent').fadeIn(500);
       			$('.catalogLoader').fadeOut(320);
			},
			error: function(response){
			}
		});
	}

	function goTo_CatalogSection(tabID){
		var sectionCatalog_rel = $('.sectionCatatalog').attr('rel');
        locationMenu_goTo(sectionCatalog_rel);
        catalogTab(tabID);
	}

	function goTo_ServiceSection(){
		var sectionService_rel = $('.sectionService').attr('rel');
        locationMenu_goTo(sectionService_rel);
	}

	function catalogTagsTab(tabID){
		$('.catalogMenu').removeClass('active');
		$('.catalogMenu[rel="'+tabID+'"]').addClass('active');
	}

	function showCatatalogContent(subcatID, filterID, activator){
		if (activator=='subcatalog'){
			//Активирую в боковом меню слево подраздел
            subCategoryTab(subcatID);
			//Загружаю контент
            loadCatalogContent(subcatID, filterID)
		}

		if (activator=='filter'){
			//Определеяеим активное подменю в категориях
			var subcatID = $('.catalog_subMenu.active').attr('rel');
			//Активирую в фильтрах выбанный подраздел
            catalogTagsTab(filterID);
			//Загружаю контент
            loadCatalogContent(subcatID, filterID)
		}
	}

	/*Мобильное меню каталога*/

	function mobileCatalogTab(tabID){
       $('.sectionCatalog_mobileMenu .subMenu_container').css('display','none');
       $('.sectionCatalog_mobileMenu .subMenu_container[rel="'+tabID+'"]').css('display','block');
	   $('.sectionCatalog_mobileMenu .catalog_menu').removeClass('active');
	   $('.sectionCatalog_mobileMenu .catalog_menu[rel="'+tabID+'"]').addClass('active');
    }

	function showMobileCatatalogContent(subcatID, filterID, activator){
			//Активирую в боковом меню слево подраздел
            subCategoryTab(subcatID);
			//Загружаю контент
            loadCatalogContent(subcatID, filterID);
			//Закрываю меню
			mobileCatalogMenu_hide();
			//Заполняю данными mobileTitle
			mobileTitle_edit();
	}

	function mobileCatalogMenu_show(){
        $('.sectionCatalog_mobileMenu').fadeIn(500);
		$('body').css({'overflow':'hidden'});
	}

	function mobileCatalogMenu_hide(){
        $('.sectionCatalog_mobileMenu').fadeOut(500);
		$('body').css({'overflow':'auto'});
	}

	function mobileTitle_edit(){
		var catText = $('.sectionCatalog_mobileMenu .catalog_menu.active').text();
		var subcatText = $('.sectionCatalog_mobileMenu .catalog_subMenu.active').text();
		$('.sectionCatalog_mobileTitle .left').text(catText);
		$('.sectionCatalog_mobileTitle .right:first').text(subcatText);
	}

    	$(function(){
			$('.catalogContent')
			.on('mouseenter','.productItem',function(){
            	var height = $(this).find('.itemName').height() - 10;
				$(this).find('.itemName').css({'display':'block','bottom':'57px'});
				$(this).find('.itemName').delay(300).stop(true).animate({'bottom':'60px','opacity':'1'},200,'swing');
				$(this).find('.photoBig').delay(300).stop(true).animate({'opacity':'0.5'},170,'swing');
				$(this).find('.itemArrow').css({'display':'block','opacity':'0'}).delay(300).stop(true).animate({'opacity':'1'},170,'swing');
			})
			.on('mouseleave','.productItem',function(){
				$(this).find('.itemName').delay(300).stop(true).animate({'bottom':'57px','opacity':'0'},200,'swing');
				$(this).find('.photoBig').delay(300).stop(true).animate({'opacity':'1'},200,'swing');
								$(this).find('.itemArrow').delay(300).stop(true).animate({'opacity':'0'},170,'swing');
			   //	$(this).find('.itemName').css('display','none');
				//$('#'+placeBoxId).find('.placeBox_squareButtons').delay(300).stop().animate({'right':'-93px'},300,'swing');
			});

         });


 function product_show(productID){
 		$('#loaderBlock').fadeIn(300);
 		$.ajax({
			type: "POST",
			url: "index.php?r=catalog/product&id="+productID,
			dataType: 'json',
			data: {
 				  },
			success: function(response){
				if (response.result==true){
					$('#loaderBlock').css('display','none');
					$('.popup__overlay').find('.popupContent').html(response.content);
					popupOpen();
				}else{
					$('#loaderBlock').css('display','none');
				}
			},
			error: function(response){
				$('#loaderBlock').css('display','none');
			}
		});
		urlHash('product_id='+productID);
 }

 	function urlHash(hash){
     	window.location.hash = hash;
	}



  function cart_show(){
  		//В меню выделяем корзину
  		$('.cartMenu').addClass('active');
		$('#loaderBlock').fadeIn(300);
 		$.ajax({
			type: "POST",
			url: "index.php?r=cart/index",
			dataType: 'json',
			data: {
 				  },
			success: function(response){
					$('#loaderBlock').css('display','none');
					$('.popup__overlay').find('.popupContent').html(response.content);
					popupOpen();
			},
			error: function(response){
					$('#loaderBlock').css('display','none');
			}
		});

		urlHash('cart_show');
 }

 function cart_add(productID){

 			if ($('.productInfo[rel="'+productID+'"]').find('.itemAdd').hasClass('added')==false)
			{

				/*
				 ** Делаем анимацию кнопки в корзину
				 ** меняем кнопку "В корзину" на "Оформить покупку"
				 */

				//Добавляем класс-маркер "Добавлено", чтобы кнопка "В корзину" не срабатывала дважды
				$('.productInfo[rel="'+productID+'"]').find('.itemAdd').addClass('added');

				//Меняем блок с ценой
					//Делаем его короче
					$('.productInfo[rel="'+productID+'"]').find('.itemPrice').delay(300).animate({'width':'130px'},200,'swing');

					//Убираем длинный текст с ценой
					$('.productInfo[rel="'+productID+'"]').find('.long_price').delay(200).animate({'opacity':'0'},200,'swing',function(){
	                	$(this).css('display','none');
					});

					//показываем блок с ценой более мелкого размера
					$('.productInfo[rel="'+productID+'"]').find('.short_price').css({'display':'block','opacity':'0'}).delay(310).animate({'opacity':'1'},200,'swing');

				//Меняем блок непосредственно с кнопкой
	                //Меняем длину кнопки "В корзину"
					$('.productInfo[rel="'+productID+'"]').find('.itemAdd').delay(300).animate({'width':'225px'},200,'swing',function(){
	                     $('.productInfo[rel="'+productID+'"] .itemAdd').find('.itemAdd_plusBg').css({'width':'225px'});
					});

					//Прячем надпись "В корзину"
					$('.productInfo[rel="'+productID+'"]').find('.to_the_cart').delay(310).animate({'opacity':'0'},200,'swing',function(){
	                	$(this).css('display','none');
					});

					//Показываем надпись "Оформить покупку"
					$('.productInfo[rel="'+productID+'"]').find('.checkout_button').css({'display':'block','opacity':'0'}).delay(310).animate({'opacity':'1'},200,'swing');

				//Меняем функцию при нажатии на кнопку
				$('.productInfo[rel="'+productID+'"]').find('.productSecondAddButton').click(function(){
                	cart_show();
				});

				$.ajax({
					url: 'index.php?r=cart/add',
					type: 'get',
					data: {
						id: 	productID,
						count: 	1,
					},
					dataType: 'json',
					success: function(res)
					{
						if (res.success)
						{
						  /*
							$('.cart-total-price').html(res.cost); */
	                        $('#mess').fadeIn(300);
							setTimeout(function(){$('#mess').fadeOut(300);   },500);
						    $('.cartCount').html(res.count);
						    $('.cartPrice').html(res.cost);

						}
					}
				});

				//сообщаем яндекс метрике, что добавление в корзину работает

			}
 }

 function cart_remove(productID){
 			//Удаляем визуально
 		    $('.cart-row[rel="'+productID+'"]').fadeOut(500);

			var cart = $('.cart-row[rel="'+productID+'"]').closest('.priceTable');
			setTimeout(function(){
              //Удаляем рельно
              $('.cart-row[rel="'+productID+'"]').remove();
			  //пересчитываем корзину
			  refresh_cart();
			  //Если блок с таблицей цен пуст, удаляем его
			  if (cart.find('.cart-row').length == 0){
			  	cart.css('display','none');
			  }
			  //если надо пишем что пусто
			  if ($('.cart-row').length == 0){
			  	$('.cart').html("<div style='min-height: 400px; width: 100%; padding-top: 140px; '><div style='height:200px; line-height:200px; width:200px; border-radius:150px; background: #F0F0F0; text-align:center; font-size:16px; color: #707070; margin:0px auto; '>ПУСТО</div></div>");
			  }
			},510);

			//Если есть карточка с товаром, то возвращаем ей плюсик
		   /*	if($('.productItem[rel="'+productID+'"]').length > 0){
             if ($('.productItem[rel="'+productID+'"]').find('.itemAdd').hasClass('added')==true){
				 $('.productItem[rel="'+productID+'"]').find('.itemAdd').removeClass('added');
				 $('.productItem[rel="'+productID+'"]').find('.addButton').css({'display':'block','left':'-10px'}).delay(300).animate({'opacity':'1','left':'9px'},200,'swing');
				 $('.productItem[rel="'+productID+'"]').find('.addedButton').delay(300).animate({'opacity':'0','left':'10px'},200,'swing');
			 }
			}
			*/


			$.ajax({
				url: 'index.php?r=cart/remove',
				type: 'get',
				data: {
					id: 	productID,
				},
				dataType: 'json',
				success: function(res)
				{
					if (res.result==true)
					{
                      $('.cartCount').html(res.count);
                      $('.cartPrice').html(res.cost);
					}
				}
			});
 }

 var photoIsAnimated = false;
 function showPhoto(tabID){
	var current_photo = $('.productPhoto.active').attr('rel');
    $('.productPhoto_preview').removeClass('active');
    $('.productPhoto_preview[rel="'+tabID+'"]').addClass('active');
	if (photoIsAnimated == false && current_photo != tabID){
		photoIsAnimated = true;
		$('.productPhoto[rel="'+tabID+'"]').css({'left':'710px','display':'block'});
		$('.productPhoto[rel="'+current_photo+'"]').delay(300).stop(true).animate({'left':'-710px'},300,'swing');
	   $('.productPhoto[rel="'+tabID+'"]').delay(300).stop(true).animate({'left':'0px'},300,'swing',function(){
        	photoIsAnimated = false;
            $('.productPhoto[rel="'+current_photo+'"]').removeClass('active');
            $('.productPhoto[rel="'+tabID+'"]').addClass('active');
	  	});
    }
 }

</script>


<script type="text/javascript">
	var cSpeed=10;
	var cWidth=40;
	var cHeight=40;
	var cTotalFrames=18;
	var cFrameWidth=40;
	var cImageSrc='css_tool/sprites.gif';

	var cImageTimeout=false;
	var cIndex=0;
	var cXpos=0;
	var cPreloaderTimeout=false;
	var SECONDS_BETWEEN_FRAMES=0;

	function startAnimation(){

		document.getElementById('loaderImage').style.backgroundImage='url('+cImageSrc+')';
		document.getElementById('loaderImage').style.width=cWidth+'px';
		document.getElementById('loaderImage').style.height=cHeight+'px';

		//FPS = Math.round(100/(maxSpeed+2-speed));
		FPS = Math.round(100/cSpeed);
		SECONDS_BETWEEN_FRAMES = 1 / FPS;

		cPreloaderTimeout=setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES/1000);

	}

	function continueAnimation(){

		cXpos += cFrameWidth;
		//increase the index so we know which frame of our animation we are currently on
		cIndex += 1;

		//if our cIndex is higher than our total number of frames, we're at the end and should restart
		if (cIndex >= cTotalFrames) {
			cXpos =0;
			cIndex=0;
		}

		if(document.getElementById('loaderImage'))
			document.getElementById('loaderImage').style.backgroundPosition=(-cXpos)+'px 0';

		cPreloaderTimeout=setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES*1000);
	}

	function stopAnimation(){//stops animation
		clearTimeout(cPreloaderTimeout);
		cPreloaderTimeout=false;
	}

	function imageLoader(s, fun)//Pre-loads the sprites image
	{
		clearTimeout(cImageTimeout);
		cImageTimeout=0;
		genImage = new Image();
		genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
		genImage.onerror=new Function('alert(\'Could not load the image\')');
		genImage.src=s;
	}

	//The following code starts the animation
	new imageLoader(cImageSrc, 'startAnimation()');
</script>
