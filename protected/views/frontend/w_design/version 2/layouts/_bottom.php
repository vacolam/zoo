<?
$otherInfo = OtherInfo::getInfo();
?>
<table style='list-style:none; padding:0px; margin:0px; width:100%; border:none; color:#fff;'>
<tr>
    <td style='width:40%;' valign=top align=left>
        <div style=' font-size:16px; '>2008—2020, Государственное бюджетное учреждение культуры «Сахалинский зооботанический парк»</div>

        <div style='padding-top:15px; font-size:14px; color:#BFEBBC;' class='open-s'>
            <?
            echo nl2br($otherInfo[2]->link);
            ?>
        </div>

        <div style='width:100%; text-align:left; margin-top:45px;'>
                <div style='display:inline-block;'>
                <div style='height:30px; float:left; backgro und:#808080; border-radius:7px;'><img src="/css_tool/vk-xxl.png" alt="" style='width:30px;'/></div>
                <div style='height:30px; float:left; backg round:#808080; border-radius:7px; margin-left:5px;'><img src="/css_tool/facebook-6-xxl.png" alt="" style='width:30px;'/></div>
                <div style='height:30px; float:left; backg round:#808080; border-radius:7px; margin-left:5px;'><img src="/css_tool/instagram-xxl.png" alt="" style='width:30px;'/></div>
                <div style='clear:both;'></div>
                </div>
        </div>
    </td>
    <td style='padding-left:40px;' valign=top align=left>
        <div style='font-size:16px;'><b>Полезные ресурсы</b></div>
        <ul style='padding-top:20px;'>
        <?
        $links = Link::getLinks();
        foreach ($links as $link)
        {
            ?>
            <li style='margin-left:20px;'><a href="<?= $link->link; ?>" style='color:#BFEBBC; font-size:14px; text-decoration:none;'><?= $link->title; ?></a></li>
            <?
        }
        ?>
        </ul>
    </td>
</tr>
</table>