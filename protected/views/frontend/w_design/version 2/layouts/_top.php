<style>
#header-title.open-s {
    background: url(/css_tool/summer_logo.png) no-repeat top left;
    background-size: 100% 100%;
    font-size:20px;

}

#header-title {
    position: absolute;
    height: 9.35em;
    width: 9.68em;
}
#header-title span {
    position: absolute;
    color: #FFFFFF;
    text-transform: uppercase;
}
#sakhalin {
    top: 2.8em;
    left: 1.3em;
    font-size:14px;
}
#zoo {
    top: 1.75em;
    left: .63em;
    font-size: 2.95em;
    letter-spacing: 0.01em;
}
#zoo_2 {
    top: 9em;
    left: 8.3em;
    font-size: .64em;
}
#zoo_3 {
    top: 3.34em;
    left: 2.83em;
    font-size: 1.85em;
    letter-spacing: .03em;
}
</style>
<div class="header" style='width:100%; margin:0px; left:0px; border-bottom:1px solid #B0B0B0; border-radius:1px; padding-top:30px; padding-bottom:30px; background:#fff;'>
    <div style='width:1200px; margin:0px auto; position:relative;'>
    <div style='width:300px;margin:0px auto;'>
        <div style='width:80px; height:80px;  float:left; margin-right:15px; margin-top:-4px;'>
                        <div id="header-title" class='open-s' style='width:80px; height:80px; margin:0px auto; float:none;'>
        					<a href="/" style='display:none;'>
        						<span id="sakhalin">Сахалинский</span><span id="zoo">ЗОО</span><span id="zoo_2">ботанический</span><span id="zoo_3">парк</span>
        					</a>
                		</div>
        </div>
        <div style='width:200px; float:left; color:#53952A; padding-top:15px; '>
            <div style='font-weight:400; font-size:22px; line-height:22px;'>САХАЛИНСКИЙ</div>
            <div style='font-weight:400; margin-top:5px;'>
            <ul style='list-style:none; padding:0px; margin:0px;'>
            <li style='font-size:42px; float:left; line-height:42px; margin-top:-5px; font-weight:600;'>ЗОО</li>
            <li style='float:left; margin-left:5px;'>
                <div style='font-size:8px; line-height:8px;'>БОТАНИЧЕСКИЙ</div>
                <div style='font-size:26px; line-height:26px;'>ПАРК</div>
            </li>
            <li style='clear:both;'></li>

            </ul>
            </div>
            <div style='font-weight:400;'></div>
        </div>
        <div style='clear:both;'></div>
    </div>

        <div style='float:right; width:800px; display:none;'>
            <div style='text-align:right;'>
                <div style='font-size:20px; color:#53952A; padding-top:20px;'>8(4242) 72-45-09</div>
                <div style='font-size:12px;  color:#53952A;'>50-56-26, 30-37-47</div>
            </div>
        </div>

        <div style='clear:both;'></div>

        <div style='position:absolute; top:0px; right:0px;'>
            <a href="" style='font-size:14px; color:#a0a0a0; font-weight:200;'>ENGLISH</a>
        </div>
    </div>
</div>
<div style='width:100%; padding:15px 0px; border-bottom:1px solid #B0B0B0;'>
    <div style='width:1200px; margin:0px auto; text-align:left;'>
        <style>
    	.menu_item{cursor:pointer; color: #3F2C35 !important; float:left; padding:2px 0px; height:30px; line-height:30px;}
    	.menu_item a{font-weight:500; color: #707070;  text-transform:uppercase; font-size:14px; line-height:30px}
    	.menu_item.active{font-weight:bold; background:#53952A; padding:2px 10px; border-radius:3px;}
    	.menu_item.active a{font-weight:600; color: #fff;}
    	</style>
        <ul style="list-style:none;  width:100%;">
            <?php
            foreach ($this->getMenu() as $item) {
                $class = $item['active'] ? ' class="active menu_item"' : 'class="menu_item"';
                ?>
                <li <?= $class; ?> style='margin-right:60px;'><?= CHtml::link($item['label'], $item['url'], array('style'=>"")); ?></li>
                <?php
            }
            ?>
            <li style='width:50px; height:30px; background:url(/css_tool/noun_Eye_801371.png) center; background-size:cover; float:right; margin-right:-5px;' class='menu_item'></li>
            <li style='width:35px; height:30px; background:url(/css_tool/noun_Search_1948134.png) center; background-size:cover; float:right;' class='menu_item'></li>
    		<li style='clear:both;'></li>
        </ul>
    </div>
</div>