<style>
#block-for-slider {
    width: 100%;
    height:500px;
    margin:0px auto;
    overflow:hidden;
    position:relative;
}
.slide{
    position:absolute;
    top:0px; left:110%;
    height:500px;
    width:100%;

}
.slide.active{
    left:0px;
}

#nav-btns{
    width:100%; height:auto; position:absolute; bottom:10px; left:0px; text-align:center;
}

.slide-nav-btn{
    width:10px; height:10px; border-radius:50%; background:#fff; border:1px solid #ccc; display:inline-block; margin-left:1px; margin-right:1px; cursor:pointer;
}

.slide-nav-btn:hover{
    background:#D9D9D9;
}

.slide-nav-btn.active{
    background:#ccc;
}

</style>

<?
//$color_array = array(#FFCC00);
?>

<div id="block-for-slider">
        <div id="viewport" style=''>
            <ul id="slidewrapper" style='margin:0px; left:0px; list-style:none;'>
                <?php
                if (count($banners) > 0)
                {
                foreach ($banners as $index => $banner)
                {
                    $active ='';
                    $index = $index + 1;
                    if ($index == 1){$active = 'active';}
                    ?>
                    <li class="slide <?=$active;?>" rel='<?=$index;?>'>
                        <a href="<?= $banner->link; ?>" style='width:678px; height:500px; display:block; position:relative; border-radius:5px;'>
                            <div style='border:1px solid #606060; width:100%; height:450px; background: url(<?= $banner->getImageUrl(); ?>) center; background-size:cover; border-radius:5px; position:relative;' alt="" class="slide-img">
                                <div style='width:100%; height:100%; background-image: linear-gradient(-180deg, rgba(0,0,0,0) 38%, rgba(0,0,0,0.65) 79%);'>
                                <div style='position:absolute; bottom:0px; left:0px; padding:10px;'>
                                    <div style='margin-top:5px; height:130px; border-radius:5px; opacity:1; '>
                                        <div style='text-align:left; padding:10px 20px;'>
                                        <div style='font-size:18px; color:#fff; padding-top:0px;' class='open-s'><b><?=$banner->title;?></b></div>
                                        <div style='font-size:14px; line-height:1.6em; color:#fff; padding-top:15px;' class='open-s'><? echo $this->crop_str_word($banner->description, 15 ); ?></div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <?php
                }
                }
                ?>
            </ul>
        </div>

                <ul id="nav-btns" style='display:none;'>
                <?
                foreach ($banners as $index => $banner)
                {
                    $index = $index + 1;
                    $active = '';
                    if ($index == 1){$active = 'active';}
                    ?>
                    <li class="slide-nav-btn <?=$active;?>" rel="<?=$index;?>" onclick='banner_button_click(<?=$index;?>)'></li>
                    <?
                }
                ?>
            </ul>
</div>
<script>
var banner_max_slides = <?=count($banners);?>;
var current_slide = 1;

var slideTime = 15000;
var bannder_slide_circling;

function bannder_slide_circle(){
    bannder_slide_circling = setInterval(function(){banner_slide(current_slide)},slideTime);
}

function banner_slide(slide_id){
    var next_slide =  slide_id + 1;
    if (next_slide > banner_max_slides){next_slide = 1;}

    $('.slide[rel="'+slide_id+'"]').animate({left: "-100%"}, 1000);
    $('.slide[rel="'+next_slide+'"]').css('left','100%').animate({left: "0"}, 1000);

    $('.slide-nav-btn').removeClass('active');
    $('.slide-nav-btn[rel="'+next_slide+'"]').addClass('active');
    current_slide = next_slide;
}

function banner_button_click(slide_id){
    $('.slide[rel="'+current_slide+'"]').animate({left: "-100%"}, 1000);
    $('.slide[rel="'+slide_id+'"]').css('left','100%').animate({left: "0"}, 1000);

    $('.slide-nav-btn').removeClass('active');
    $('.slide-nav-btn[rel="'+slide_id+'"]').addClass('active');
    current_slide = slide_id;
}

$(function(){
    bannder_slide_circle();

    $('#block-for-slider').on('mousemove',function(){
        clearInterval(bannder_slide_circling);

    });
    $('#block-for-slider').on('mouseleave',function(){
        bannder_slide_circle();
    });
})

</script>