<div style='text-transform:uppercase; font-size:14px; font-weight:600; padding-bottom:10px;'><b>Чем заняться</b></div>
    <div>
        <div style='border-radius:4px; width:150px; height:100px; background:url(/upload/other/7bf1314121b434089383a6f33a2570a3.jpg) center #f0f0f0; background-size:cover; float:left;'></div>
        <div style='margin-left:20px; width: 305px;  float:left; '>
            <a href="" style='font-size:14px; font-weight:600;' class='open-s'>Сходить группой на экскурсию</a>
            <div style='color:#909090; font-size:14px; padding-top:10px; '><i>Отлично подходит школьным организованным группам. Возможен заказ автобуса.</i></div>
        </div>
        <div style='clear:both;'></div>

        <div style='padding-top:12px;'>
            <div style='border-radius:4px;width:150px; height:100px; background:url(/upload/other/c1cc139b9b3cd65e4a5b8b50a3d4ae2f.jpg) center #f0f0f0; background-size:cover; float:left;'></div>
            <div style='margin-left:20px; width: 305px; float:left; '>
                <a href="" style='font-size:14px; font-weight:600;' class='open-s'>Интересные мероприятия</a>
                <div style='color:#909090; font-size:14px; padding-top:10px; '><i>Приходите на наши праздники, дни рождения зверей и просто интересные события</i></div>
            </div>
            <div style='clear:both;'></div>
        </div>

        <div style='padding-top:12px;'>
            <div style='border-radius:4px;width:150px; height:100px; background:url(/upload/other/39c2813b8da5f23d35281f835e694548.jpg) center #f0f0f0; background-size:cover; float:left;'></div>
            <div style='margin-left:20px; width: 305px; float:left; '>
                <a href="" style='font-size:14px; font-weight:600;' class='open-s'>Познавательные кружки</a>
                <div style='color:#909090; font-size:14px; padding-top:10px; '><i>Участвуйте в различных познавательных мероприятиях, где вы узнаете о животных и их привычках</i></div>
            </div>
            <div style='clear:both;'></div>
        </div>

        <div style='padding-top:12px;'>
            <div style='border-radius:4px;width:150px; height:115px; background:url(/upload/afisha/s_5e783e613d75c_1584938593.jpg) center #f0f0f0; background-size:cover; float:left;'></div>
            <div style='margin-left:20px; width: 305px; float:left; '>
                <a href="" style='font-size:14px; font-weight:600;' class='open-s'>Кормление животных</a>
                <div style='color:#909090; font-size:14px; padding-top:10px; '><i>Вы не видели как кормят медведя? Обязательно посмотрите этот познавательный процесс.</i></div>
            </div>
            <div style='clear:both;'></div>
        </div>

        <div style='padding-top:12px; display:none;'>
            <div style='width:150px; height:90px; background:#f0f0f0; float:left;'></div>
            <div style='margin-left:20px; width: 305px; float:left; '>
                <a href="" style='font-size:14px; font-weight:600;' class='open-s'>Деревенское подворье</a>
                <div style='color:#909090; font-size:14px; padding-top:10px; '><i>Здесь можно общаться с животными, напрямую контактируя с ними. Это место отдыха, психологической реабилитации и оздоровления.</i></div>
            </div>
            <div style='clear:both;'></div>
        </div>

        <div style='padding-top:20px; display:none;'>
            <div style='width:170px; height:90px; background:#f0f0f0; float:left;'></div>
            <div style='margin-left:20px; width: 305px; float:left; '>
                <a href="" style='font-size:14px; font-weight:600;' class='open-s'>Сахалинское долголетие</a>
                <div style='color:#909090; font-size:14px; padding-top:10px; '><i>А Вы знали, что на Сахалине действует специальная программа поддержки пенсионеров? Наш Зоопарк с радостью в ней участвует!</i></div>
            </div>
            <div style='clear:both;'></div>
        </div>

        <div style='text-align:right; padding-top:20px; display:none;'>
            <ul style='list-style:none; padding:0px; margin:0px; display:inline-block;'>
                <li style='float:left; margin-right:20px; color:#53952A; font-size:18px;'><span style='padding-bottom:3px; border-bottom:2px solid #53952A;'>1</span></li>
                <li style='float:left; margin-right:20px; color:#53952A; font-size:18px;'>2</li>
                <li style='float:left; margin-right:0px; color:#909090; font-size:18px;'>></li>
                <li style='clear:both;'></li>
            </ul>
        </div>
    </div>