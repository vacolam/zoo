<?
$otherInfo = OtherInfo::getInfo();
?>
<table style='list-style:none; padding:0px; margin:0px; width:100%; border:none;'>
<tr>
    <td style='width:50%;' valign=top align=left>
        <div style='width:100px; height:100px; background:url(/css_tool/summer_logo.png) center; background-size:cover; float:left; margin-right:30px;'></div>
        <div style=' font-size:18px; float:left; width:460px; margin-top:25px; color:#202020; font-weight:400;'>2008—2020, Государственное бюджетное учреждение культуры «Сахалинский зооботанический парк»</div>
        <div style='clear:both;'></div>

        <div style='padding-top:30px; font-size:14px; margin-left:130px; ' class='open-s'>
            <?
            echo nl2br($otherInfo[2]->link);
            ?>
        </div>

        <div style='width:100%; text-align:left; margin-top:10px; margin-left:130px;'>
                <div style='display:inline-block;'>
                <div style='height:30px; float:left; background:#808080; border-radius:7px;'><img src="/css_tool/vk-xxl.png" alt="" style='width:30px;'/></div>
                <div style='height:30px; float:left; background:#808080; border-radius:7px; margin-left:5px;'><img src="/css_tool/facebook-6-xxl.png" alt="" style='width:30px;'/></div>
                <div style='height:30px; float:left; background:#808080; border-radius:7px; margin-left:5px;'><img src="/css_tool/instagram-xxl.png" alt="" style='width:30px;'/></div>
                <div style='clear:both;'></div>
                </div>
        </div>
    </td>
    <td style='padding-left:40px; padding-top:20px;' valign=top align=left>
        <div style='font-size:16px;'><span style='background:#53952A; color:#fff; padding:6px 15px; border-radius:4px;'>Полезные ресурсы</span></div>
        <ul style='padding-top:30px;'>
        <?
        $links = Link::getLinks();
        foreach ($links as $link)
        {
            ?>
            <li style='margin-left:20px;'><a href="<?= $link->link; ?>" style='color: #000; font-size:14px; text-decoration:none;'><?= $link->title; ?></a></li>
            <?
        }
        ?>
        </ul>
    </td>
</tr>
</table>