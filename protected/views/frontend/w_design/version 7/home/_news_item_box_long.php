
    <?
        $bg =  "background:none; ";
        if ($data->hasImage()) {
            $bg_url = $data->getImageUrl();
            $bg =  'background:url("'.$bg_url.'") #f0f0f0 center; background-size:cover;';
        }
    ?>
<a href="<?=Yii::app()->createUrl('news/view',array('id' => $data->id));?>" style='display:block; margin-right:10px; margin-bottom:10px;  width:550px; height:300px; overflow:hidden; float:left; border-radius:4px;  <?=$bg;?>'>
    <div style='border-radius:4px; width:550px; height:300px; background-image: linear-gradient(-180deg, rgba(0,0,0,0) 38%, rgba(0,0,0,0.65) 79%); position:relative;'>
    <div style='position:absolute; padding-bottom:20px; bottom:0px; left:20px; width:510px; height:100px;'>
    <div style='padding-top:10px; font-size:16px; font-weight:600; color:#fff; ' class='open-s'><? echo $this->crop_str_word($data->title, 10 ); ?></div>
    <div style='padding-top:15px; font-size:14px; font-weight:200; color:#fff;' class='open-s'><? echo $this->crop_str_word($data->short_text, 15 ); ?></div>
    </div>
    </div>
</a>
