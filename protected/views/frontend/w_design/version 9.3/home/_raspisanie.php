<!-----
9.3 background-image: linear-gradient(to top, #f5d100 0%, #53952A 130%);
9.4 background-image: linear-gradient(to top, #50cc7f 0%, #f5d100 100%);
9.5 background-image: linear-gradient(to top, #9be15d 0%, #00e3ae 100%);
9.6 background-image: linear-gradient(-20deg, #00cdac 0%, #61CCC5 100%);
9.7 background-image: linear-gradient(-40deg, #53952A 0%, #61CCC5 100%);
9.8 background-image: linear-gradient(-40deg, #61CCC5 0%, #53952A 100%);
9.9 background-image: linear-gradient(-45deg, #FFCC00 0%, #53952A 50%);
9.10 background-image: linear-gradient(-45deg, #00CC00 10%, #53952A 80%);
9.11 background-image: linear-gradient(to top, #0ba360 0%, #3cba92 100%);
9.12 background-image: linear-gradient(to top, #a18cd1 0%, #fbc2eb 100%);
9.14 background-image: linear-gradient(10deg, #00CC00 0%, #53952A 100%);
9.15 background-image: linear-gradient(10deg,  #498325 0%, #62AF31 100%);

-->
<div style='width:1200px; margin:0px auto; margin-top:20px; position:relative;'>
<!---------
background:url(https://note256.files.wordpress.com/2014/01/evernote-background-color.png) center no-repeat; background-size:cover;
background:url(https://zoo.waw.pl/cache/files/11111/background---w-1200.jpg) left center;
---------------->
    <div style='z-index:888; width:320px;  height:360px;  position:absolute; top:0px; left:0px; background:url(/css_tool/photo-1541140134513-85a161dc4a00.jpg) top left; background-size:cover;   border-radius:5px;'>
        <div style='width:100%; height:100%; background:rgba(204, 102, 255, 0.6); border-radius:5px;'>
        <div style='padding:25px 20px; '>
            <div style='padding-top:10px; text-align:center; color:#fff;'>
                <div style='font-size:13px; font-weight:600; text-align:center; padding-bottom:15px; text-shadow:1px 1px 7px rgba(0,0,0,.2);'>РАСПИСАНИЕ</div>
                <div style='font-size:50px;  font-weight:600; letter-spacing:-1px; padding-top:60px;  text-shadow:2px 2px 5px rgba(0,0,0,.2);'>
                    10:00-18:00
                </div>
                <div style='font-size:15px; font-weight:200; text-align:center; padding-top:25px; width:240px; margin:0px auto;'><b>Касса</b> работает <b>до 17:00</b> часов в рабочие дни. В выходные и праздники <b>до 17:30</b></div>
            </div>
        </div>
        </div>
    </div>


<div style='z-index:881;position:absolute; top:0px; left:330px; width:550px; height:360px;  border-radius:5px; background:url(/css_tool/photo-1541140134513-85a161dc4a00.jpg) top left; background-size:cover; '>
    <div style='width:100%: height:360px; background:rgba(111, 199, 56, 0.88); border-radius:5px'>

    <div style='width:180px; height:360px;  float:left; color:#fff; border-right:1px dotted rgba(255,255,255,0.7);'>
        <div style='padding:25px;'>
            <div style='padding-top:10px; text-align:center; color:#fff;'>
                <div style='text-align:left;'>
                <div style='font-size:13px;  font-weight:600; color:#fff; padding-bottom:15px; margin-bottom:15px; border-bottom:2px solid rgba(255,255,255,0.6); border-radius:1px;'>
                    ДЕТИ
                </div>
                <div style='font-size:32px;  font-weight:600; letter-spacing:-1px;'>
                    100 <SPAN style='font-size:20px;'>РУБ</SPAN>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'>Рабочие дни</div>

                <div style='font-size:32px;  font-weight:600; letter-spacing:-1px; padding-top:30px;'>
                    80 <SPAN style='font-size:20px;'>РУБ</SPAN>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'>Праздники и выходные</div>


                <div style='font-size:18px;  font-weight:600; letter-spacing:-1px; padding-top:30px;'>
                    БЕСПЛАТНО
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'>дети до 3-х лет</div>

                </div>
            </div>
        </div>
    </div>
    <div style='width:180px;height:360px;  float:left; margin-right:1px; color:#fff; border-right:1px dotted rgba(255,255,255,0.7);'>
        <div style='padding:25px;'>
            <div style='padding-top:10px; text-align:center; color:#fff;'>
                <div style='text-align:left;'>
                <div style='font-size:13px;  font-weight:600;  color:#fff; padding-bottom:15px; margin-bottom:15px; border-bottom:2px solid rgba(255,255,255,0.6); border-radius:1px;'>
                    ВЗРОСЛЫЕ
                </div>
                <div style='font-size:32px;  font-weight:600; letter-spacing:-1px;'>
                    200 <SPAN style='font-size:20px;'>РУБ</SPAN>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'>Рабочие дни</div>

                <div style='font-size:32px;  font-weight:600; letter-spacing:-1px; padding-top:30px;'>
                    180 <SPAN style='font-size:20px;'>РУБ</SPAN>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'>Праздники и выходные</div>
                </div>
            </div>
        </div>
    </div>
    <div style='width:180px;height:360px;  float:left; margin-right:5px; color:#fff;'>
        <div style='padding:25px;'>
            <div style='padding-top:10px; text-align:center; color:#fff;'>
                <div style='text-align:left;'>
                <div style='font-size:13px;  font-weight:600; color:#fff; padding-bottom:15px; margin-bottom:15px; border-bottom:2px solid rgba(255,255,255,0.6); border-radius:1px;'>
                    ПЕНСИОНЕРЫ
                </div>
                <div style='font-size:32px;  font-weight:600; letter-spacing:-1px;'>
                    50 <SPAN style='font-size:20px;'>РУБ</SPAN>
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'>Рабочие дни</div>

                <div style='font-size:22px;  font-weight:600; letter-spacing:-1px; padding-top:30px;'>
                    БЕСПЛАТНО
                </div>
                <div style='font-size:14px; font-weight:200; text-align:left; padding-top:0px;'>Сахалинское долголетие</div>
                </div>
            </div>
        </div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>

    <div style='width:310px; height:360px; float:right;  border-radius:5px; background:url(/css_tool/photo-1541140134513-85a161dc4a00.jpg) top left; background-size:cover;'>
       <div style='width:100%; height:100%; border-radius:5px;  background:rgba(255, 198, 26, 0.85);'>
       <div style='padding:25px;'>
            <div style='padding-top:10px; color:#fff;'>
                <div style='text-align:center;'>
                <div style='font-size:13px;  font-weight:600; color:#fff; padding-bottom:15px; margin-bottom:15px; text-align:center; text-shadow:1px 1px 2px rgba(0,0,0,.2);'>
                    КАК НАС НАЙТИ?
                </div>
                <div style='font-size:28px; font-weight:600; text-align:center; padding-top:25px; text-shadow:1px 1px 2px rgba(0,0,0,.2);'>УЛ. ДЕТСКАЯ 4А</div>
                <div style='font-size:16px; font-weight:200; text-align:center; padding-top:2px; text-shadow:1px 1px 2px rgba(0,0,0,.2);'>г. Южно-Сахалинск</div>
                <div style='font-size:16px; font-weight:200; text-align:center; padding-top:20px; text-shadow:1px 1px 2px rgba(0,0,0,.2);'>72-45-09<span style='padding-left:5px; padding-right:5px;'>&bull;</span>50-56-26<span style='padding-left:5px; padding-right:5px;'>&bull;</span>30-37-47</div>

                <div style='font-size:14px; font-weight:200; text-align:center; padding-top:10px;'><b>Посмотреть на карте&nbsp;&rarr;</b></div>

                <div style='width:100%; text-align:center; margin-top:65px;'>
                <div style='display:inline-block;'>
                <div style='height:30px; float:left; '><img src="/css_tool/vk-xxl.png" alt="" style='width:30px;'/></div>
                <div style='height:30px; float:left; margin-left:5px;'><img src="/css_tool/facebook-6-xxl.png" alt="" style='width:30px;'/></div>
                <div style='height:30px; float:left; margin-left:5px;'><img src="/css_tool/instagram-xxl.png" alt="" style='width:30px;'/></div>
                <div style='clear:both;'></div>
                </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </div>
<div style='clear:both;'></div>
</div>