<div style='width:100%;'>
<div style='width:100%; height:450px;'>
<?
            $this->renderPartial('_banners', array(
            'banners' => $banners,
        ));
?>
</div>
</div>


<div style='padding-top:40px;'>
    <?
    $this->renderPartial('_raspisanie', array());
    ?>
</div>




<?
$otherInfo = OtherInfo::getInfo();
?>
<div style='width: 1200px; margin:0px auto; overflow:hidden; margin-top:60px; '>
    <?
    $this->renderPartial('_what_to_do', array());
    ?>
</div>


    <div style='display:none;'>
    <?
            echo nl2br($otherInfo[0]->link);
            ?>
    </div>



<div style='width:1200px; margin:0px auto; position:relative; margin-bottom:100px;'>
<div style='width: 850px; margin-top:70px; float:left; margin-right:50px;'>

<div style='width: 850px;'>
    <table style='width:835px;  border-collapse:collapse; margin-bottom:15px; margin-top:-10px;'>
        <tr>
            <td style='text-align:left;' valign=middle>
                <div style=''><span style='background:#FFC61A; color:#fff; font-size:14px; padding:8px 14px; border-radius:3px;'><b>ЭКСПОЗИЦИИ</b></span></div>
            </td>
            <td  valign=middle style='text-align:right; font-weight:200;'>
                <div>ВСЕ</div>
            </td>
        </tr>
    </table>

        <div>
            <?php
            $expo = Exposure::getMainList();
            if ($expo) {
                foreach ($expo as $item) {
                    $this->renderPartial('_exposure_item', array(
                                'data'=>$item,
                            ));
                }
                ?>
                <div style='clear:both;'></div>
                <?php
            }
            ?>
            <div style='clear:both;'></div>
        </div>
    </div>


    <table style='width:835px;  margin-top:50px; border-collapse:collapse; margin-bottom:15px;'>
        <tr>
            <td style='text-align:left;' valign=middle>
                <div style=''><span style='background:rgba(204, 102, 255, 1); color:#fff; font-size:14px; padding:8px 14px; border-radius:3px;'><b>НОВОСТИ</b></span></div>
            </td>
            <td  valign=middle style='text-align:right; font-weight:200;'>
                <div>ВСЕ</div>
            </td>
        </tr>
    </table>
<?php
$news = News::getMainList();
if ($news) {
    $t = 0;
    foreach ($news as $item) {
        $t++;
        if ($t == 8){$t = 1;}
        $pattern = '_news_item_box_small';
        if (in_array($t,array(1,2,3,5,6))){$pattern = '_news_item_box_small';}
        if (in_array($t,array(4,7))){$pattern = '_news_item_box_long';}

        $this->renderPartial($pattern, array(
                    'data'=>$item,
                ));
    }
    ?>
    <div style='clear:both;'></div>

    <?php
}
?>



</div>


<div style='width: 300px; margin-top:50px; float:right; '>
<div style='width:300px; padding-top:25px;'>

            <div style='width:300px; background:#F2F2F2; border-radius:5px;'>
            <div style='padding:30px; padding-top:30px;'>
            <div style='font-size:16px; color:#fff;'><span style='background:#53952A; color:#fff; padding:6px 15px; border-radius:4px;'>Опека над животными</span></div>
            <div style='font-size:15px; padding-top:30px; color:#808080; line-height:1.4em;'><i>Опека над животными позволяет зоопарку улучшить условия содержания животных и дает возможность более полно вести научные наблюдения за их поведением в неволе, а опекунам предоставляет великолепную возможность ощутить свою причастность к живой природе и жизни зоопарка, видеть конкретные результаты своих усилий.</i></div>
            </div>
            </div>

            <div style='width:300px; background:#F2F2F2; border-radius:5px; margin-top:15px;'>
            <div style='padding:30px; padding-top:30px;'>
            <div style='font-size:16px; color:#fff;'><span style='background:#53952A; color:#fff; padding:6px 15px; border-radius:4px;'>Нашли животное?</span></div>
            <div style='font-size:14px; padding-top:20px; color:#808080;'><i>Узнайте как действовать, если Вы обнаружили дикое животное, требующее помощи.</i></div>
            </div>
            </div>

            <div style='width:300px; background:#F2F2F2; border-radius:5px; margin-top:15px;'>
            <div style='padding:30px; padding-top:30px;'>
            <div style='font-size:16px; color:#fff;'><span style='background:#53952A; color:#fff; padding:6px 15px; border-radius:4px;'>Реабилитационный центр</span></div>
            <div style='font-size:14px; padding-top:20px; color:#808080;'><i>Медвежата, оставшиеся без медведицы, лисицы, попавшие в колодец, птицы с поврежденными крыльями и многие другие случаи вынуждают граждан нести животных в зоопарк. С этим помогает специальное учреждение Зоопарка Центр реабилитации диких животных.</i></div>
            </div>
            </div>




    </div>

    <div style='margin-top:50px;'>
        <div style='padding-bottom:5px; text-transform:uppercase; font-size:16px; '><b>Партнеры</b></div>

        <div style='width:300px; height:150px; background:url(http://sakhalinzoo.ru/upload/iblock/ee1/ee1e82caf58a5ea6e58d993803ae8f57.png) #6F524E center no-repeat; backgrou nd-size:cover; margin-bottom:5px;'></div>
        <div style='width:300px; height:150px; background:url(http://sakhalinzoo.ru/upload/iblock/eb0/eb0e5147f7eee97a26195c344a20decd.jpg) #fff center no-repeat; backgrou nd-size:cover; border:1px solid #ccc; margin-bottom:5px;'></div>
        <div style='width:300px; height:150px; background:url(http://img2.loveradio.ru/images/newlogo.png) #fff center no-repeat; border:1px solid #ccc; margin-bottom:5px;'></div>
        <div style='width:300px; height:150px; background:url(http://sakhalinzoo.ru/upload/iblock/c1c/c1c0e4409d7898f34c78c8552b56f474.png) #fff center no-repeat; border:1px solid #ccc; margin-bottom:5px;'></div>
        <div style='width:300px; height:150px; background:url(https://astv.ru/Images/logo/logo_main_new_dis.png) #fff center no-repeat;border:1px solid #ccc; margin-bottom:5px;'></div>
        <!--<div style='width:300px; height:150px; background:url(https://radio.astv.ru/sites/default/files/ASTV-logos-01.png) #fff center no-repeat; border:1px solid #ccc; margin-bottom:5px;'></div>-->
        <div style='width:300px; height:150px; background:url(https://www.мойрейс.рф/verstka/site/pics/logo_hover.png) rgb(4,55,99) center no-repeat; margin-bottom:5px;'></div>
        <!--<div style='width:300px; height:150px; background:url(http://doctor-brand.com/img/logo.jpg) #AC090C center no-repeat; margin-bottom:5px;'></div>
        <div style='width:300px; height:150px; background:url(http://efdesign.ru/wp-content/uploads/2017/07/cropped-EFD-Logo_tr-w-2.png) rgb(4,55,99)  center no-repeat; border:1px solid #ccc; margin-bottom:5px;'></div>
        -->


    </div>












</div>

<div style='clear:both;'></div>

</div>




