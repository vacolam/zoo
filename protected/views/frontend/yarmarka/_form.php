<?
$detect2 = new Mobile_Detect;
?>
<style>
input[type=text] {
    height:40px; font-size:16px; line-height:40px; width:350px; padding: 0px 15px;
    border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;
}

label{
    display:block; padding-bottom:10px;
}

.error{
    padding:7px 12px; background:#DC3545; color:#fff; font-size:14px; margin-top:5px; border-radius:5px; display:none;
}
</style>

<?
$form_display = 'block';
$success_display = 'none';


if(isset($_GET['success'])) {
    $form_display = 'none';
    $success_display = 'block';
}
?>

<div class='Yarmarka_preview' style='width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1);  display:<?=$form_display;?>;'>
    <div style='padding:30px;'>
    <div style='width:150px; height:150px; background:url(/css_tool/flag.png) center; background-size:cover; float:left; margin-left:40px; margin-right:60px; margin-top:10px;' class='form_photo'></div>
    <div style='' class='form_long_description'>
        <div class='form_title'>Отправить заявку</div>
        <div class='form_text'>Заполните все поля заявки и обязательно прикрепите необходимые документы.</div>
        <div onclick='show_Yarmarka()' class='form_button' style='background:#C71A45;'>Начать</div>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>


<?
if($detect2->isMobile())
{
?>
<div class='Yarmarka ocenka_preview'  style='display:none;'>
    <div class='form_padding'>
        <div class='form_description'>
            <div class='form_title'>Зайдите с компьютера</div>
            <div class='form_text'>В мобильной версии отправка заявки не работает. Зайдите на эту страницу с компьютера.</div>
        </div>
    </div>
</div>
<?
}


if(!$detect2->isMobile())
{
        $YarmarkaID = 0;
        if (isset($_GET['yarmarka_id'])){
        $YarmarkaID = (int)$_GET['yarmarka_id'];
        }
?>
<div class='Yarmarka_thnx' style='width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px; display:<?=$success_display;?>;'>
    <div style='padding:40px;'>
    <div style='width:160px; height:160px; background:url(/css_tool/asset.png) center; background-size:cover; float:left; margin-left:0px; margin-right:80px;'></div>
    <div style='width:500px; float:left;'>
        <div style='font-size:32px; font-weight:200;'><b>Заявка <? if ($YarmarkaID>0){echo '№ '.$YarmarkaID.' ';}?>принята!!!</b></div>
        <div style='font-size:15px; line-height:1.5em; font-weight:200; padding-top:15px;'><i>Спасибо, что прислали заявку на ярмарку. Мы с Вами свяжемся, чтобы уточнить детали.</i></div>
        <?


        if ($YarmarkaID > 0){
        ?>
        <div style='font-size:15px; line-height:1.5em; font-weight:200; padding-top:15px;'><i>Номер Вашей заявки - <b><?=$YarmarkaID;?></b>.</i></div>
        <?
        }
        ?>
    </div>
    <div style='clear:both;'></div>
    </div>
</div>
<div class='Yarmarka' style='display:none; width:100%; background:#fff; border-radius:5px; box-shadow:0px 0px 5px rgba(0,0,0,.1); margin-bottom:20px;'>
<div style='padding:70px 45px; padding-bottom:85px; '>
<form class='form'>

                    <div style="float:left; width:350px;" class='input-block-1'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Наименование ИП<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="Yarmarka[name]" placeholder="Ваше имя" class="name" />
                            <div class='error'>Заполните наименование</div>
                        </div>
                    </div>

                    <div style="float:right; width:350px;" class='input-block-2'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Номер места<span style='color:#DC3545;'>*</span>
                            </div>
                            <select name="Yarmarka[mesto]" size="1" class='mesto' style='width:350px; padding:0px 10px; height:40px; line-height:40px; border:2px solid #ccc; border-radius:4px;'>
                                <option value="0" selected="selected">Выберите место</option>
                                <option value="1">Место № 1</option>
                                <option value="2">Место № 2</option>
                                <option value="3">Место № 3</option>
                                <option value="4">Место № 4</option>
                                <option value="5">Место № 5</option>
                                <option value="6">Место № 6</option>
                                <option value="7">Место № 7</option>
                                <option value="8">Место № 8</option>
                                <option value="9">Место № 9</option>
                                <option value="10">Место № 10</option>
                            </select>
                            <div class='error'>Выберите место</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>


                    <div style="float:left; width:350px; margin-top:40px;" class='input-block-3'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Телефон<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="Yarmarka[phone]" placeholder="Телефон" class="phone" />
                            <div class='error'>Заполните телефон</div>
                        </div>
                    </div>

                    <div style="float:right; width:350px; margin-top:40px;" class='input-block-4'>
                        <div class="">
                            <div style="font-size:16px; font-weight:600; padding-bottom:7px;">
                                Электронная почта<span style='color:#DC3545;'>*</span>
                            </div>
                            <input type="text" name="Yarmarka[email]" placeholder="Электронная почта" class="email" />
                            <div class='error'>Заполните e-mail</div>
                        </div>
                    </div>

                    <div style='clear:both;'></div>

                    <div style="border-bottom:1px dotted #ccc; padding-bottom:20px;margin-bottom:20px; margin-top:40px;">
                            <p style="font-weight:600; font-size:16px; padding-bottom:7px;">
                                Сообщение
                            </p>
                            <div >
                            <textarea name="Yarmarka[comment]" class="comment" style='width:740px; font-size:16px; line-height:40px;  padding: 0px 15px; border:2px solid #BDC3C7; background:#fff; border-radius:5px; color:#A0A0A0;'></textarea>
                            </div>
                    </div>
            </form>

                    <div style="margin-top:20px; margin-bottom:20px;" class='input-block-5'>
                            <div class='open-s' style="background:#fff; width:100%; cursor:pointer; height:110px; border:2px dashed #BABABA;"  onclick="add_photos_and_files()">
                                <div style="text-align:center; font-size:24px;  padding-top:18px;">
                                <i>Выберите файлы (до 10 штук)<span style='color:#DC3545;'>*</span></i>
                                </div>

                                <div style='text-align:center; padding-top:3px; font-size:13px; color:#A0A0A0;'>
                                  pdf, docx, doc, xls, xlsx, zip, rar, rtf, txt, gif, png, jpeg
                                </div>

                                <div style='text-align:center; padding-top:3px; font-size:10px; color:#A0A0A0;'>
                                                                    <b>MAX</b> РАЗМЕР ОДНОГО ФАЙЛА -<b> 10МБ</b>
                                </div>

                            </div>
                            <?

                             $this->renderPartial('//layouts/_f_upload', array(
                                'files_number'=>10
                            ));

                            ?>
                            <div class='error'>Добавьте файлы</div>
                    </div>

                    <div style='cursor:pointer; width:100%; border:none; height:40px; border-radius:4px; margin-top:0px; color:#fff; line-height:40px; font-size:14px; text-align:center; background:#007BFF;' onclick='form_submit()' class='sbmt_btn'>Отправить</div>

            </div>
            </div>
<?
}
?>
<script>
function show_Yarmarka(){
    $('.Yarmarka_preview').css('display','none');
    $('.Yarmarka').css('display','block');
}

function check_form_errors(){
    var t = 0;
    if ($('.input-block-1').find('input').val() == ''){t = t + 1; $('.input-block-1').find('.error').css('display','block');}
    if ($('.mesto option:selected').val() == 0){t = t + 1; $('.input-block-2').find('.error').css('display','block');}
    if ($('.input-block-3').find('input').val() == ''){t = t + 1; $('.input-block-3').find('.error').css('display','block');}
    if ($('.input-block-4').find('input').val() == ''){t = t + 1; $('.input-block-4').find('.error').css('display','block');}

    var photos_count = $('.files .fade').length;
    if (photos_count == 0){t = t + 1; $('.input-block-5').find('.error').css('display','block');}
    return t;
}

function form_submit(){
    $('.sbmt_btn').text('Отправляется...');
    $('.error').css('display','none');

    var t = 0;

    t = check_form_errors();

    var photos_count = $('.files .fade').length;
    if (t == 0){
    var form = $('.form').serialize();
            $.ajax({
                type: "POST",
                url: '<?= Yii::app()->createUrl('yarmarka/order', array()); ?>',
                dataType: 'json',
                data: form,
                success: function(response){
                            if (response){
                                YarmarkaID = response.id;
                                if (photos_count > 0){
                                    var url = '<?=Yii::app()->createUrl('yarmarka/filesadd', array());?>&post_id=' + response.id;
                                    $('.comment_fileupload').fileupload({
                                        url:  url
                                    });
                                   	$('.upload-button').trigger('click');
                                }
                            }
                },
                error: function(response){
                }
            });
    }else{
        $('.sbmt_btn').css('background','#DC3545');
        $('.sbmt_btn').text('Ошибки в заполнении');

        setTimeout(function(){
                $('.sbmt_btn').css('background','#007BFF');
                $('.sbmt_btn').text('Отправить');
        },5000);
    }


}
</script>